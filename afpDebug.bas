Attribute VB_Name = "modDebug"
'20131123LC (tutta)
Option Explicit


'Da usare in Excel (es. =xlsdiff(A10;"dip";B10) in A le istruzioni, in B iErr, in C la funzione)
'Poi incollare in Notepad, ed eliminare le ""
Public Function xlsDiff(ByVal sa As String, sb As String, iErr As Long) As String
  Dim i As Long
  Dim s As String
  Dim sInd As String
  
  sa = Trim(sa)
  sInd = Space(4)
  If sa = "" Then
    xlsDiff = ""
  ElseIf Left(sa, 1) = "'" Then
    xlsDiff = sInd & sa
  Else
    i = InStr(sa, " ")
    If i > 0 Then
      s = Left(sa, i - 1)
      xlsDiff = sInd & "ElseIf ." & s & " <> " & sb & "." & s & " Then" & vbCrLf & sInd & "  iErr = " & iErr
    Else
      xlsDiff = "'// " & s
    End If
  End If
End Function


Public Function Dbg_VD1_Diff(a() As Double, a1() As Double) As Long
  Dim iErr As Long
  Dim i1 As Long, n1 As Long, m1 As Long
  
  m1 = LBound(a1, 1)
  n1 = UBound(a1, 1)
  If m1 <> LBound(a, 1) Or n1 <> UBound(a, 1) Then
    iErr = -1
  Else
    For i1 = m1 To n1
      If a1(i1) <> a(i1) Then
        iErr = i1
        GoTo ExitPoint
      End If
    Next i1
  End If
ExitPoint:
  Dbg_VD1_Diff = iErr
End Function


Public Function Dbg_VD2_Diff(a() As Double, a1() As Double) As Long
  Dim iErr As Long
  Dim i1 As Long, n1 As Long, m1 As Long
  Dim i2 As Long, n2 As Long, m2 As Long
  
  m1 = LBound(a1, 1)
  n1 = UBound(a1, 1)
  m2 = LBound(a1, 2)
  n2 = UBound(a1, 2)
  If m1 <> LBound(a, 1) Or n1 <> UBound(a, 1) Then
    iErr = -1
  ElseIf m2 <> LBound(a, 2) Or n2 <> UBound(a, 2) Then
    iErr = -2
  Else
    For i1 = m1 To n1
      For i2 = m2 To n2
        If a1(i1, i2) <> a(i1, i2) Then
          iErr = i1
          GoTo ExitPoint
        End If
      Next i2
    Next i1
  End If
ExitPoint:
  Dbg_VD2_Diff = iErr
End Function


Public Function Dbg_VD3_Diff(a() As Double, a1() As Double) As Long
  Dim iErr As Long
  Dim i1 As Long, n1 As Long, m1 As Long
  Dim i2 As Long, n2 As Long, m2 As Long
  Dim i3 As Long, n3 As Long, m3 As Long
  
  m1 = LBound(a1, 1)
  n1 = UBound(a1, 1)
  m2 = LBound(a1, 2)
  n2 = UBound(a1, 2)
  m3 = LBound(a1, 3)
  n3 = UBound(a1, 3)
  If m1 <> LBound(a, 1) Or n1 <> UBound(a, 1) Then
    iErr = -1
  ElseIf m2 <> LBound(a, 2) Or n2 <> UBound(a, 2) Then
    iErr = -2
  ElseIf m3 <> LBound(a, 3) Or n3 <> UBound(a, 3) Then
    iErr = -3
  Else
    For i1 = m1 To n1
      For i2 = m2 To n2
        For i3 = m3 To n3
          If a1(i1, i2, i3) <> a(i1, i2, i3) Then
            iErr = i1
            GoTo ExitPoint
          End If
        Next i3
      Next i2
    Next i1
  End If
ExitPoint:
  Dbg_VD3_Diff = iErr
End Function


Public Function Dbg_TAppl_Diff(ByRef appl As TAppl, appl1 As TAppl) As Long
  Dim iErr As Long
  
  With appl1
    If .idReport <> appl.idReport Then
      iErr = 1
    ElseIf .nMatr <> appl.nMatr Then
      iErr = 2
    ElseIf .xa <> appl.xa Then
      iErr = 3
    ElseIf .h <> appl.h Then
      iErr = 4
    ElseIf .bEA <> appl.bEA Then
      iErr = 5
    ElseIf .bProb_EA_IV_IB <> appl.bProb_EA_IV_IB Then
      iErr = 6
    ElseIf .bProb_AW <> appl.bProb_AW Then
      iErr = 7
    ElseIf .bProb_PensAtt <> appl.bProb_PensAtt Then
      iErr = 8
    ElseIf .bProb_Superst <> appl.bProb_Superst Then
      iErr = 9
    ElseIf .matr1 <> appl.matr1 Then
      iErr = 10
    ElseIf .matr2 <> appl.matr2 Then
      iErr = 11
    ElseIf .bStop <> appl.bStop Then
      iErr = 12
    ElseIf .t0 <> appl.t0 Then
      iErr = 13
    ElseIf .DataBilancio <> appl.DataBilancio Then
      iErr = 14
    ElseIf .DataInizio <> appl.DataInizio Then
      iErr = 15
    '// *** Eta' ***
    ElseIf .XMIN <> appl.XMIN Then
      iErr = 17
    ElseIf .XMAX <> appl.XMAX Then
      iErr = 18
    ElseIf .xlim <> appl.xlim Then
      iErr = 19
    ElseIf .nx5 <> appl.nx5 Then
      iErr = 20
    '// *** Anzianita' ***
    ElseIf .hmin5 <> appl.hmin5 Then
      iErr = 22
    ElseIf .hmin15 <> appl.hmin15 Then
      iErr = 23
    ElseIf .hmax <> appl.hmax Then
      iErr = 24
    '20080416LC (inizio)
    ElseIf .nMax1 <> appl.nMax1 Then
      iErr = 26
    ElseIf .nMax2 <> appl.nMax2 Then
      iErr = 27
    '20080416LC (fine)
    '--- 20121028LC (inizio)
    ElseIf .nSize1 <> appl.nSize1 Then
      iErr = 30
    ElseIf .nSize2 <> appl.nSize2 Then
      iErr = 31
    ElseIf .nSize3 <> appl.nSize3 Then
      iErr = 32
    ElseIf .nSize4 <> appl.nSize4 Then
      iErr = 33
    '--- 20121028LC (inizio)
    ElseIf .hlim <> appl.hlim Then
      iErr = 35
    ElseIf .nh5 <> appl.nh5 Then
      iErr = 36
    '// *** altro ***
    ElseIf .Qualifica <> appl.Qualifica Then
      iErr = 38
    ElseIf .sesso <> appl.sesso Then
      iErr = 39
    '// *** Nomi di file
    ElseIf .szDef1Input <> appl.szDef1Input Then
      iErr = 41
    ElseIf .szDef1Output <> appl.szDef1Output Then
      iErr = 42
    ElseIf .iDef1Gruppo <> appl.iDef1Gruppo Then
      iErr = 43
    ElseIf .szDef2Input <> appl.szDef2Input Then
      iErr = 44
    ElseIf .szDef2Output <> appl.szDef2Output Then
      iErr = 45
    ElseIf .iDef2Gruppo <> appl.iDef2Gruppo Then
      iErr = 46
    ElseIf .szDef3Input <> appl.szDef3Input Then
      iErr = 47
    ElseIf .szDef3Output <> appl.szDef3Output Then
      iErr = 48
    ElseIf .iDef3Gruppo <> appl.iDef3Gruppo Then
      iErr = 49
    ElseIf .szDef3RenditeM <> appl.szDef3RenditeM Then
      iErr = 50
    ElseIf .szDef3RenditeF <> appl.szDef3RenditeF Then
      iErr = 51
    ElseIf .szDef3TipoFile <> appl.szDef3TipoFile Then
      iErr = 52
    '*********
    'Parametri
    '*********
    ElseIf .parm(299) <> appl.parm(299) Then
      iErr = 56
    '*******
    'Matrici
    '*******
    ElseIf Dbg_VD3_Diff(.BDA(), appl.BDA()) <> 0 Then
      iErr = 60
    ElseIf Dbg_VD3_Diff(.LL1(), appl.LL1()) <> 0 Then
      iErr = 61
    ElseIf Dbg_VD3_Diff(.pf(), appl.pf()) <> 0 Then
      iErr = 62
    'ElseIf Dbg_VTWFD_Diff(.pfamD(), appl.pfamD()) <> 0 Then
    '  iErr = 63
    'ElseIf Dbg_VTWFL_Diff(.pfamL(), appl.pfamL()) <> 0 Then
    '  iErr = 64
    'ElseIf Dbg_VTWFD_Diff(.pfam4(), appl.pfam4()) <> 0 Then
    '  iErr = 65
    ElseIf Dbg_VD3_Diff(.CoeffPA(), appl.CoeffPA()) <> 0 Then
      iErr = 66
    'CoeffRicRis() As Double
    ElseIf Dbg_VD3_Diff(.CoeffRiv(), appl.CoeffRiv()) <> 0 Then
      iErr = 68
    ElseIf Dbg_VD3_Diff(.CoeffVar(), appl.CoeffVar()) <> 0 Then
      iErr = 69
    'ElseIf Dbg_VTCV_Diff(.cv(), appl.cv()) <> 0 Then
    '  iErr = 70
    ElseIf Dbg_VD3_Diff(.CoeffNI(), appl.CoeffNI()) <> 0 Then
      iErr = 71
    ElseIf Dbg_VD3_Diff(.Lsm(), appl.Lsm()) <> 0 Then
      iErr = 72
    ElseIf Dbg_VD3_Diff(.tb1(), appl.tb1()) <> 0 Then
      iErr = 73
    ElseIf Dbg_VD3_Diff(.SuddivRipart(), appl.SuddivRipart()) <> 0 Then
      iErr = 78
    '--- 20080919LC
    ElseIf .nPensMat <> appl.nPensMat Then
      iErr = 80
    ElseIf .nPensMax <> appl.nPensMax Then
      iErr = 81
    '--- 20120210LC
'ElseIf .cv4() <> appl.cv4() Then
'  iErr = 83
    End If
  End With
  Dbg_TAppl_Diff = iErr
End Function
  

Public Function Dbg_TIscr2_Diff(ByRef dip As TIscr2, dip1 As TIscr2) As Long
  Dim iErr As Long
  
  With dip1
    If .mat <> dip.mat Then
      iErr = 1
    ElseIf .gruppo0 <> dip.gruppo0 Then
      iErr = 2
    ElseIf .dElab <> dip.dElab Then
      iErr = 3
    ElseIf .annoIniz <> dip.annoIniz Then
      iErr = 4
    ElseIf .DCan <> dip.DCan Then
      iErr = 5
    ElseIf .bModArt26 <> dip.bModArt26 Then
      iErr = 6
    ElseIf .iQ <> dip.iQ Then
      iErr = 7
    ElseIf .iSQ <> dip.iSQ Then
      iErr = 8
    ElseIf .iSQT <> dip.iSQT Then
      iErr = 9
    ElseIf .iTC <> dip.iTC Then
      iErr = 10
    ElseIf .iTS <> dip.iTS Then
      iErr = 11
    '--- EX TIscr2 (inizio) ---
    ElseIf .Qualifica <> dip.Qualifica Then
      iErr = 13
    ElseIf .Carriera <> dip.Carriera Then
      iErr = 14
    ElseIf .sesso <> dip.sesso Then
      iErr = 15
    ElseIf .DNasc <> dip.DNasc Then
      iErr = 16
    ElseIf .dAssunz <> dip.dAssunz Then
      iErr = 17
    ElseIf .annoAss <> dip.annoAss Then
      iErr = 18
    ElseIf .dIscrFondo <> dip.dIscrFondo Then
      iErr = 19
    ElseIf .xan <> dip.xan Then
      iErr = 20
    ElseIf .xr <> dip.xr Then
      iErr = 21
    ElseIf .dx <> dip.dx Then
      iErr = 22
    '---
    ElseIf .annoFineCR <> dip.annoFineCR Then
      iErr = 24
    ElseIf .n <> dip.n Then
      iErr = 25
    '---
    ElseIf .qab2 <> dip.qab2 Then
      iErr = 27
    ElseIf .qae2 <> dip.qae2 Then
      iErr = 28
    ElseIf .qai2 <> dip.qai2 Then
      iErr = 29
    ElseIf .qaw2 <> dip.qaw2 Then
      iErr = 30
    ElseIf .qad1 <> dip.qad1 Then
      iErr = 31
    ElseIf .qed1 <> dip.qed1 Then
      iErr = 32
    ElseIf .qbd <> dip.qbd Then
      iErr = 33
    ElseIf .qid <> dip.qid Then
      iErr = 34
    ElseIf .qpad <> dip.qpad Then
      iErr = 35
    ElseIf .qpnd <> dip.qpnd Then
      iErr = 36
    '--- 20121115LC (inizio)
    ElseIf .pwDB <> dip.pwDB Then
      iErr = 38
    ElseIf .pwDB_fc <> dip.pwDB_fc Then
      iErr = 39
    '---
    ElseIf .a_tpind <> dip.a_tpind Then
      iErr = 41
    ElseIf .a_hind <> dip.a_hind Then
      iErr = 42
    ElseIf .a_pind <> dip.a_pind Then
      iErr = 43
    ElseIf .a_pind_fc <> dip.a_pind_fc Then
      iErr = 44
    '---
    ElseIf .a_tprev <> dip.a_tprev Then
      iErr = 46
    ElseIf .a_hrev <> dip.a_hrev Then
      iErr = 47
    ElseIf .a_prev <> dip.a_prev Then
      iErr = 48
    ElseIf .a_prev_fc <> dip.a_prev_fc Then
      iErr = 49
    '---
    ElseIf .ea_tpind <> dip.ea_tpind Then
      iErr = 51
    ElseIf .ea_hind <> dip.ea_hind Then
      iErr = 52
    ElseIf .ea_pind <> dip.ea_pind Then
      iErr = 53
    ElseIf .ea_pind_fc <> dip.ea_pind_fc Then
      iErr = 54
    '---
    ElseIf .ea_tprev <> dip.ea_tprev Then
      iErr = 56
    ElseIf .ea_hrev <> dip.ea_hrev Then
      iErr = 57
    ElseIf .ea_prev <> dip.ea_prev Then
      iErr = 58
    ElseIf .ea_prev_fc <> dip.ea_prev_fc Then
      iErr = 59
    '--- 20121115LC (fine)
    ElseIf .irpefNI <> dip.irpefNI Then
      iErr = 61
    ElseIf .ivaNI <> dip.ivaNI Then
      iErr = 62
    ElseIf .FRAZ_PC0 <> dip.FRAZ_PC0 Then
      iErr = 63
    ElseIf .FRAZ_PNA0 <> dip.FRAZ_PNA0 Then
      iErr = 64
    ElseIf .FRAZ_PNV0 <> dip.FRAZ_PNV0 Then
      iErr = 65
    ElseIf .FRAZ_TZ0 <> dip.FRAZ_TZ0 Then
      iErr = 66
    ElseIf .qm <> dip.qm Then
      iErr = 67
    ElseIf .qaS <> dip.qaS Then
      iErr = 68
    ElseIf .NMR <> dip.NMR Then
      iErr = 69
    ElseIf .NTR <> dip.NTR Then
      iErr = 70
    ElseIf .st <> dip.st Then
      iErr = 71
    ElseIf .catPens <> dip.catPens Then
      iErr = 72
    ElseIf .t <> dip.t Then
      iErr = 73
    ElseIf .t0 <> dip.t0 Then
      iErr = 74
    ElseIf .rr_tmin <> dip.rr_tmin Then
      iErr = 75
    ElseIf .rr_tmax <> dip.rr_tmax Then
      iErr = 76
    ElseIf .annoPNAA <> dip.annoPNAA Then
      iErr = 77
    ElseIf .annoPNAV <> dip.annoPNAV Then
      iErr = 78
    ElseIf .c0 <> dip.c0 Then
      iErr = 79
    ElseIf .c1 <> dip.c1 Then
      iErr = 80
    ElseIf .coe0 <> dip.coe0 Then
      iErr = 81
    ElseIf .coe1 <> dip.coe1 Then
      iErr = 82
    ElseIf .d0 <> dip.d0 Then
      iErr = 83
    ElseIf .d1 <> dip.d1 Then
      iErr = 84
    ElseIf .doe0 <> dip.doe0 Then
      iErr = 85
    ElseIf .doe1 <> dip.doe1 Then
      iErr = 86
    '---
    ElseIf .anno0 <> dip.anno0 Then
      iErr = 88
    ElseIf .anno1 <> dip.anno1 Then
      iErr = 89
    ElseIf .bNuovoEntrato <> dip.bNuovoEntrato Then
      iErr = 90
    ElseIf Dbg_TWA_Diff(.n0, dip.n0) <> 0 Then
      iErr = 91
    ElseIf .zd <> dip.zd Then
      iErr = 92
    ElseIf .zs <> dip.zs Then
      iErr = 93
    ElseIf .zs1 <> dip.zs1 Then
      iErr = 94
    ElseIf .ZW <> dip.ZW Then
      iErr = 95
    '---
    ElseIf .ZMAX <> dip.ZMAX Then
      iErr = 97
    '---
    ElseIf .AnniElab1 <> dip.AnniElab1 Then
      iErr = 99
    ElseIf .AnniElab2 <> dip.AnniElab2 Then
      iErr = 100
    ElseIf .TipoElab <> dip.TipoElab Then
      iErr = 101
    '20120214LC (inizio)
    'PRC_PV_ATT As Double     '20080611LC
    'PRC_PA_ATT As Double     '20080611LC
    'PRC_PC_ATT As Double     '20120214LC
    'PRCTTLZ As Double
    '20120214LC (fine)
    '--- 20120210LC
'ElseIf Dbg_TAppl_CV4(.cv4, dip.cv4) <> 0 Then
'  iErr = 109
    '--- 20120112LC (afpnew)
    ElseIf Dbg_V_TIscr2_Stato_Diff(.s(), dip.s()) Then
      iErr = 111
    '---
    ElseIf .nSup <> dip.nSup Then
      iErr = 113
'ElseIf DBg_V_TIscrSup2(.sup(), dip.sup()) <> 0 Then
'  iErr = 114
    '20120114LC (inizio)
    ElseIf Dbg_VD1_Diff(.om_rcU(), dip.om_rcU()) <> 0 Then
      iErr = 128
    ElseIf Dbg_VD1_Diff(.om_rcV(), dip.om_rcV()) <> 0 Then
      iErr = 129
    '20120114LC (fine)
    ElseIf .rr_dhMax <> dip.rr_dhMax Then
      iErr = 131
    ElseIf .rr_dhEff <> dip.rr_dhEff Then
      iErr = 132
    ElseIf .rr_dhEff0 <> dip.rr_dhEff0 Then
      iErr = 133
    ElseIf .rr_crr <> dip.rr_crr Then
      iErr = 134
    '--- 20080812LC ---
    ElseIf .anno <> dip.anno Then
      iErr = 136
    ElseIf .qsd <> dip.qsd Then
      iErr = 137
    ElseIf .tau1 <> dip.tau1 Then
      iErr = 138
    ElseIf .tau0 <> dip.tau0 Then
      iErr = 139
    ElseIf Dbg_V_TWA_Diff(.wa(), dip.wa()) <> 0 Then
      iErr = 140
    ElseIf Dbg_V_TWB_Diff(.wb(), dip.wb()) <> 0 Then
      iErr = 141
    ElseIf Dbg_VD1_Diff(.wc(), dip.wc()) <> 0 Then
      iErr = 142
    '--- 20080813LC ---
    ElseIf .atryPensA <> dip.atryPensA Then
      iErr = 144
    ElseIf .annoPensA <> dip.annoPensA Then
      iErr = 145
    ElseIf .tipoPensA <> dip.tipoPensA Then
      iErr = 146
    ElseIf .stipoPensA <> dip.stipoPensA Then
      iErr = 147
    '---
    'ElseIf wb0.atryPensEA <> dipwb0.atryPensEA Then
    '  iErr = 149
    'ElseIf wb0.annoPensEA <> dipwb0.annoPensEA Then
    '  iErr = 150
    'ElseIf wb0.tipoPensEA <> dipwb0.tipoPensEA Then
    '  iErr = 151
    'ElseIf wb0.stipoPensEA <> dipwb0.stipoPensEA Then
    '  iErr = 152
    '--- 20080912LC ---
    ElseIf .nPens <> dip.nPens Then
      iErr = 154
'ElseIf .lp() <> dip.lp() Then
'  iErr = 155
    '--- 20120110LC ---
    ElseIf .bFalsoEA <> dip.bFalsoEA Then
      iErr = 157
    '--- 20130609LC (riorganizzato per debug)
    ElseIf .Pens1r <> dip.Pens1r Then
      iErr = 159
    ElseIf .Pens1C <> dip.Pens1C Then
      iErr = 160
    ElseIf .ax <> dip.ax Then
      iErr = 161
    ElseIf .ax0 <> dip.ax0 Then
      iErr = 162
    '--- Informazioni per pensione minima
    ElseIf .bPmin <> dip.bPmin Then
      iErr = 164
    ElseIf .nAbbArt25 <> dip.nAbbArt25 Then
      iErr = 165
    ElseIf .nRMPmin <> dip.nRMPmin Then
      iErr = 166
    ElseIf .RMPmin <> dip.RMPmin Then
      iErr = 167
    ElseIf .PminBase <> dip.PminBase Then
      iErr = 168
    ElseIf .PMinAbb <> dip.PMinAbb Then
      iErr = 169
    ElseIf .PMinCalc <> dip.PMinCalc Then
      iErr = 170
    ElseIf .PSost <> dip.PSost Then
      iErr = 171
    End If
  End With
  Dbg_TIscr2_Diff = iErr
End Function


Public Function Dbg_TIscr2_Anz_Diff(anz As TIscr2_Anz, anz1 As TIscr2_Anz) As Long
  Dim iErr As Long
  
  With anz1
    If .h <> anz.h Then
      iErr = 1
    ElseIf .h_retr <> anz.h_retr Then
      iErr = 2
    ElseIf .h_contr <> anz.h_contr Then
      iErr = 3
    ElseIf .h_ante <> anz.h_ante Then
      iErr = 4
    ElseIf .h_retr_ante <> anz.h_retr_ante Then
      iErr = 5
    ElseIf .h0_retr_ante <> anz.h0_retr_ante Then
      iErr = 6
    ElseIf .h1_retr_ante <> anz.h1_retr_ante Then
      iErr = 7
    ElseIf .h_contr_ante <> anz.h_contr_ante Then
      iErr = 8
    ElseIf .h2_contr_ante <> anz.h2_contr_ante Then
      iErr = 9
    ElseIf .h3_contr_ante <> anz.h3_contr_ante Then
      iErr = 10
    ElseIf .h_post <> anz.h_post Then
      iErr = 11
    ElseIf .h_retr_post <> anz.h_retr_post Then
      iErr = 12
    ElseIf .h0_retr_post <> anz.h0_retr_post Then
      iErr = 13
    ElseIf .h1_retr_post <> anz.h1_retr_post Then
      iErr = 14
    ElseIf .h_contr_post <> anz.h_contr_post Then
      iErr = 15
    ElseIf .h2_contr_post <> anz.h2_contr_post Then
      iErr = 16
    ElseIf .h3_contr_post <> anz.h3_contr_post Then
      iErr = 17
    End If
  End With
  Dbg_TIscr2_Anz_Diff = iErr
End Function


Public Function Dbg_V_TIscr2_Stato_Diff(s() As TIscr2_Stato, s1() As TIscr2_Stato) As Long
  Dim iErr As Long
  Dim i1 As Long, n1 As Long, m1 As Long
  
  m1 = LBound(s1, 1)
  n1 = UBound(s1, 1)
  If m1 <> LBound(s, 1) Or n1 <> UBound(s, 1) Then
    iErr = -1
  Else
    For i1 = m1 To n1
      If Dbg_TIscr2_Stato_Diff(s(i1), s1(i1)) <> 0 Then
        iErr = i1
        GoTo ExitPoint
      End If
    Next i1
  End If
ExitPoint:
  Dbg_V_TIscr2_Stato_Diff = iErr
End Function


Public Function Dbg_TIscr2_Stato_Diff(s As TIscr2_Stato, s1 As TIscr2_Stato) As Long
  Dim iErr As Long
  
  With s1
    If .sm <> s.sm Then
      iErr = 1
    ElseIf .sm2ns <> s.sm2ns Then
      iErr = 2
    ElseIf .sm2s <> s.sm2s Then
      iErr = 2
    ElseIf .iva <> s.iva Then
      iErr = 3
    ElseIf .c_sogg0a1 <> s.c_sogg0a1 Then
      iErr = 4
    ElseIf .c_sogg0a2 <> s.c_sogg0a2 Then
      iErr = 5
    ElseIf .c_sogg1a <> s.c_sogg1a Then
      iErr = 6
    ElseIf .c_sogg2a <> s.c_sogg2a Then
      iErr = 7
    ElseIf .c_sogg3a <> s.c_sogg3a Then
      iErr = 8
    ElseIf .c_int0a <> s.c_int0a Then
      iErr = 9
    ElseIf .c_int1a <> s.c_int1a Then
      iErr = 10
    ElseIf .c_int2a <> s.c_int2a Then
      iErr = 11
    ElseIf .c_int3a <> s.c_int3a Then  '20150512LC
      iErr = 11
    ElseIf .c_ass0a <> s.c_ass0a Then
      iErr = 12
    ElseIf .c_ass1a <> s.c_ass1a Then
      iErr = 13
    ElseIf .c_mat <> s.c_mat Then
      iErr = 14
    ElseIf .c_tot <> s.c_tot Then
      iErr = 15
    '--- 20120114LC
    ElseIf .om_rcU(eom.om_max) <> s.om_rcU(eom.om_max) Then
      iErr = 17
    ElseIf .om_rcV(eom.om_max) <> s.om_rcV(eom.om_max) Then
      iErr = 18
    '--- 20121105LC
    ElseIf .bIsArt25 <> s.bIsArt25 Then
      iErr = 20
    ElseIf .bIsMisto <> s.bIsMisto Then
      iErr = 21
    ElseIf .bIsProRata <> s.bIsProRata Then
      iErr = 22
    ElseIf .ic <> s.ic Then
      iErr = 23
    ElseIf .ico <> s.ico Then
      iErr = 24
    ElseIf .ipr <> s.ipr Then
      iErr = 25
    ElseIf .mi <> s.mi Then
      iErr = 26
    ElseIf .ALR <> s.ALR Then
      iErr = 27
    '---
    ElseIf .it <> s.it Then
      iErr = 29
    ElseIf .stato <> s.stato Then
      iErr = 30
    ElseIf .caus <> s.caus Then
      iErr = 31
    ElseIf .gruppo1 <> s.gruppo1 Then
      iErr = 32
    ElseIf .bPensAtt <> s.bPensAtt Then
      iErr = 33
    ElseIf .napa <> s.napa Then
      iErr = 34
    ElseIf .napa0 <> s.napa0 Then
      iErr = 35
    ElseIf .tapa <> s.tapa Then
      iErr = 36
    '--- 20121103LC
    ElseIf .pens1a1 <> s.pens1a1 Then
      iErr = 38
    ElseIf .pens1a2 <> s.pens1a2 Then
      iErr = 39
    '--- Anzianit�
    ElseIf Dbg_TIscr2_Anz_Diff(.anz, s.anz) Then
      iErr = 41
    ElseIf .AnzTot <> s.AnzTot Then
      iErr = 42
    ElseIf .hs <> s.hs Then
      iErr = 43
    '20111213LC (inizio)
    ElseIf .h_retr_ante1 <> s.h_retr_ante1 Then
      iErr = 45
    ElseIf .h_retr_post1 <> s.h_retr_post1 Then
      iErr = 46
    ElseIf .h_contr_ante1 <> s.h_contr_ante1 Then
      iErr = 47
    ElseIf .h_contr_post1 <> s.h_contr_post1 Then
      iErr = 48
    '20111213LC (fine)
    ElseIf .hq <> s.hq Then
      iErr = 50
    ElseIf .hf <> s.hf Then
      iErr = 51
    End If
  End With
  Dbg_TIscr2_Stato_Diff = iErr
End Function


Public Function Dbg_V_TWA_Diff(wa() As TWA, wa1() As TWA) As Long
  Dim iErr As Long
  Dim i1 As Long, n1 As Long, m1 As Long
  
  m1 = LBound(wa1, 1)
  n1 = UBound(wa1, 1)
  If m1 <> LBound(wa, 1) Or n1 <> UBound(wa, 1) Then
    iErr = -1
  Else
    For i1 = m1 To n1
      If Dbg_TWA_Diff(wa(i1), wa1(i1)) <> 0 Then
        iErr = i1
        GoTo ExitPoint
      End If
    Next i1
  End If
ExitPoint:
  Dbg_V_TWA_Diff = iErr
End Function


Public Function Dbg_TWA_Diff(wa As TWA, wa1 As TWA) As Long
  Dim iErr As Long
  
  With wa1
    If .c0a_z <> wa.c0a_z Then
      iErr = 1
    ElseIf .c0v_z <> wa.c0v_z Then  '20150504LC
      iErr = 2
    ElseIf .c0e_z <> wa.c0e_z Then
      iErr = 2
    ElseIf .c1i_z <> wa.c1i_z Then
      iErr = 3
    ElseIf .c1b_z <> wa.c1b_z Then
      iErr = 4
    ElseIf .c1ca_z <> wa.c1ca_z Then
      iErr = 5
    ElseIf .c1cvo_z <> wa.c1cvo_z Then
      iErr = 6
    ElseIf .c1cva_z <> wa.c1cva_z Then
      iErr = 6
    ElseIf .c1cvp_z <> wa.c1cvp_z Then
      iErr = 6
    ElseIf .c1cs_z <> wa.c1cs_z Then
      iErr = 7
    ElseIf .c1ct_z <> wa.c1ct_z Then
      iErr = 8
    ElseIf .c1na_z <> wa.c1na_z Then
      iErr = 9
    ElseIf .c1nvo_z <> wa.c1nvo_z Then
      iErr = 10
    ElseIf .c1nva_z <> wa.c1nva_z Then
      iErr = 10
    ElseIf .c1nvp_z <> wa.c1nvp_z Then
      iErr = 10
    ElseIf .c1ns_z <> wa.c1ns_z Then
      iErr = 11
    ElseIf .c1nt_z <> wa.c1nt_z Then
      iErr = 12
    ElseIf .s1_z <> wa.s1_z Then
      iErr = 13
    ElseIf .sj0_z <> wa.sj0_z Then
      iErr = 14
    ElseIf .sj1_z <> wa.sj1_z Then
      iErr = 15
    ElseIf .sj2_z <> wa.sj2_z Then
      iErr = 16
    ElseIf .sj3_z <> wa.sj3_z Then
      iErr = 17
    ElseIf .sj4_z <> wa.sj4_z Then
      iErr = 18
    ElseIf .sj5_z <> wa.sj5_z Then
      iErr = 19
    ElseIf .sj6_z <> wa.sj6_z Then
      iErr = 20
    ElseIf .sj7_z <> wa.sj7_z Then
      iErr = 21
    ElseIf .sj8_z <> wa.sj8_z Then
      iErr = 22
    ElseIf .sd_z <> wa.sd_z Then
      iErr = 23
    ElseIf .w1b_z <> wa.w1b_z Then
      iErr = 24
    ElseIf .w2b_z <> wa.w2b_z Then
      iErr = 25
    ElseIf .w3b_z <> wa.w3b_z Then
      iErr = 26
    ElseIf .totAtt_z <> wa.totAtt_z Then
      iErr = 27
    ElseIf .tot_z <> wa.tot_z Then
      iErr = 28
    '---
    ElseIf .c1na_p <> wa.c1na_p Then
      iErr = 30
    ElseIf .c1nvo_p <> wa.c1nvo_p Then
      iErr = 31
    ElseIf .c1nva_p <> wa.c1nva_p Then
      iErr = 31
    ElseIf .c1nvp_p <> wa.c1nvp_p Then
      iErr = 31
    ElseIf .c1ns_p <> wa.c1ns_p Then
      iErr = 32
    ElseIf .c1nt_p <> wa.c1nt_p Then
      iErr = 33
    ElseIf .s1_p <> wa.s1_p Then
      iErr = 34
    ElseIf .w1b_p <> wa.w1b_p Then
      iErr = 35
    '--- 20121116LC (inizio)
    ElseIf .w2b_p <> wa.w2b_p Then
      iErr = 37
    ElseIf .w3b_p <> wa.w3b_p Then
      iErr = 38
    '---
    ElseIf .c1na_zp <> wa.c1na_zp Then
      iErr = 40
    ElseIf .c1nvo_zp <> wa.c1nvo_zp Then
      iErr = 41
    ElseIf .c1nva_zp <> wa.c1nva_zp Then
      iErr = 41
    ElseIf .c1nvp_zp <> wa.c1nvp_zp Then
      iErr = 41
    ElseIf .c1ns_zp <> wa.c1ns_zp Then
      iErr = 42
    ElseIf .c1nt_zp <> wa.c1nt_zp Then
      iErr = 43
    ElseIf .s1_zp <> wa.s1_zp Then
      iErr = 44
    ElseIf .w1b_zp <> wa.w1b_zp Then
      iErr = 45
    ElseIf .w2b_zp <> wa.w2b_zp Then
      iErr = 46
    ElseIf .w3b_zp <> wa.w3b_zp Then
      iErr = 47
    '---
    ElseIf .c1na_zm <> wa.c1na_zm Then
      iErr = 49
    ElseIf .c1nvo_zm <> wa.c1nvo_zm Then
      iErr = 50
    ElseIf .c1nva_zm <> wa.c1nva_zm Then
      iErr = 50
    ElseIf .c1nvp_zm <> wa.c1nvp_zm Then
      iErr = 50
    ElseIf .c1ns_zm <> wa.c1ns_zm Then
      iErr = 51
    ElseIf .c1nt_zm <> wa.c1nt_zm Then
      iErr = 52
    ElseIf .s1_zm <> wa.s1_zm Then
      iErr = 53
    ElseIf .w1b_zm <> wa.w1b_zm Then
      iErr = 54
    ElseIf .w2b_zm <> wa.w2b_zm Then
      iErr = 55
    ElseIf .w3b_zm <> wa.w3b_zm Then
      iErr = 56
    '--- 20121116LC (fine)
    End If
  End With
  Dbg_TWA_Diff = iErr
End Function


Public Function Dbg_V_TWB_Diff(wb() As TWB, wb1() As TWB) As Long
  Dim iErr As Long
  Dim i1 As Long, n1 As Long, m1 As Long
  
  m1 = LBound(wb1, 1)
  n1 = UBound(wb1, 1)
  If m1 <> LBound(wb, 1) Or n1 <> UBound(wb, 1) Then
    iErr = -1
  Else
    For i1 = m1 To n1
      If Dbg_TWB_Diff(wb(i1), wb1(i1)) <> 0 Then
        iErr = i1
        GoTo ExitPoint
      End If
    Next i1
  End If
ExitPoint:
  Dbg_V_TWB_Diff = iErr
End Function


Public Function Dbg_TWB_Diff(wb As TWB, wb1 As TWB) As Long
  Dim iErr As Long
  
  With wb1
    '--- 20150504LC (inizio)
    If .vg2 <> wb.vg2 Then
      iErr = 1
    ElseIf .eg2 <> wb.eg2 Then
      iErr = 2
    ElseIf .bg2 <> wb.bg2 Then
      iErr = 3
    ElseIf .ig2 <> wb.ig2 Then
      iErr = 4
    End If
    '--- 20150504LC (fine)
  End With
  Dbg_TWB_Diff = iErr
End Function



