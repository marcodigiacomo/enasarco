VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.MDIForm MdiAfp 
   BackColor       =   &H8000000C&
   Caption         =   "AFP v.2.8"
   ClientHeight    =   4140
   ClientLeft      =   1785
   ClientTop       =   1710
   ClientWidth     =   7065
   LinkTopic       =   "MDIForm1"
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   7065
      _ExtentX        =   12462
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   11
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "New"
            Object.ToolTipText     =   "Nuova analisi"
            Object.Tag             =   ""
            ImageIndex      =   1
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "Open"
            Object.ToolTipText     =   "Apre un'analisi esistente"
            Object.Tag             =   ""
            ImageIndex      =   2
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "Save"
            Object.ToolTipText     =   "Salva i parametri delll'analisi corrente"
            Object.Tag             =   ""
            ImageIndex      =   3
         EndProperty
         BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button6 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "Calc"
            Object.ToolTipText     =   "Effettua l'analisi con i dati specificati"
            Object.Tag             =   ""
            ImageIndex      =   5
         EndProperty
         BeginProperty Button7 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "Stop"
            Object.ToolTipText     =   "Interrompe l'analisi in corso"
            Object.Tag             =   ""
            ImageIndex      =   6
         EndProperty
         BeginProperty Button8 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button9 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "Print"
            Object.ToolTipText     =   "Stampa il report dell'analisi"
            Object.Tag             =   ""
            ImageIndex      =   4
         EndProperty
         BeginProperty Button10 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button11 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "RepOpt"
            Object.ToolTipText     =   "Opzioni report"
            Object.Tag             =   ""
            ImageIndex      =   7
         EndProperty
      EndProperty
   End
   Begin ComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   0
      Top             =   3765
      Width           =   7065
      _ExtentX        =   12462
      _ExtentY        =   661
      Style           =   1
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
         NumPanels       =   1
         BeginProperty Panel1 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   3240
      Top             =   1800
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   1440
      Top             =   1440
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   9
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AfpMdi.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AfpMdi.frx":067A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AfpMdi.frx":0CF4
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AfpMdi.frx":136E
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AfpMdi.frx":19E8
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AfpMdi.frx":1D02
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AfpMdi.frx":2534
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AfpMdi.frx":284E
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AfpMdi.frx":2B68
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuMain 
      Caption         =   "&File"
      Index           =   0
      Begin VB.Menu mnuFile 
         Caption         =   "&Nuova analisi"
         Index           =   0
         Shortcut        =   ^N
      End
      Begin VB.Menu mnuFile 
         Caption         =   "&Apri analisi"
         Index           =   1
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuFile 
         Caption         =   "&Salva analisi"
         Index           =   2
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuFile 
         Caption         =   "-"
         Index           =   3
      End
      Begin VB.Menu mnuFile 
         Caption         =   "Salva para&metri"
         Index           =   4
         Shortcut        =   ^M
      End
      Begin VB.Menu mnuFile 
         Caption         =   "-"
         Index           =   5
      End
      Begin VB.Menu mnuFile 
         Caption         =   "&Tipo report"
         Index           =   6
         Shortcut        =   ^T
      End
      Begin VB.Menu mnuFile 
         Caption         =   "&Ricrea report"
         Index           =   7
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuFile 
         Caption         =   "-"
         Index           =   8
      End
      Begin VB.Menu mnuFile 
         Caption         =   "&Elabora"
         Index           =   9
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuFile 
         Caption         =   "&Interrompi"
         Index           =   10
         Shortcut        =   {F6}
      End
      Begin VB.Menu mnuFile 
         Caption         =   "-"
         Index           =   11
      End
      Begin VB.Menu mnuFile 
         Caption         =   "&Imposta stampante"
         Index           =   12
         Shortcut        =   ^I
      End
      Begin VB.Menu mnuFile 
         Caption         =   "Stam&pa"
         Index           =   13
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuFile 
         Caption         =   "-"
         Index           =   14
      End
      Begin VB.Menu mnuFile 
         Caption         =   "Es&ci"
         Index           =   15
         Shortcut        =   {F12}
      End
   End
   Begin VB.Menu mnuMain 
      Caption         =   "Fi&nestre"
      Index           =   1
      WindowList      =   -1  'True
      Begin VB.Menu mnuWindow 
         Caption         =   "&Sovrapponi"
         Index           =   0
      End
      Begin VB.Menu mnuWindow 
         Caption         =   "Affianca &Orizzontalmente"
         Index           =   1
      End
      Begin VB.Menu mnuWindow 
         Caption         =   "Affianca &Verticalmente"
         Index           =   2
      End
      Begin VB.Menu mnuWindow 
         Caption         =   "&Disponi icone"
         Index           =   3
      End
   End
End
Attribute VB_Name = "MdiAfp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'0) 6L1                        (z514c10: 16803774) 16816973 (4/3)
'1) OPZ_PENS_ATT_FRAZ = 0,5    (z514c10: 16807263)
'2) OPZ_MISTO_INTEGR_MODEL = 4 (z514c10: 16964914)
'3) OPZ_MOD_ART25_MODEL = 1    (z514c10: 16903871)
'4) OPZ_MOD_ART26_MODEL = 2    (z514c10: 16821608) 16921705
'5) OPZ_MOD_ART_RIV_2001 = 1   (z514c10: 16925980)
'6) OPZ_MOD_ART_17_2 = 1       (z514c10: 16922748) (mat=176617)
'7) OPZ_MOD_ART_17_4 = 1       (z514c10: 15927798)
'8) OPZ_MOD_ART_32_6 = 1       (z514c10: 16644736)
'9) OPZ_MOD_X75 = 1            (z514c10: 17107019)
'10) OPZ_PINV_MODEL = 3        (z514c10: 16580533)
'ALL)                          (z514c10: 16674410)
Option Explicit
DefStr A-Z

Private Const FILE_NEW As Integer = 0
Private Const FILE_OPEN As Integer = 1
Private Const FILE_SAVE_AS As Integer = 2
Private Const FILE_SAVE_PRM_AS As Integer = 4
Private Const FILE_REP_OPZ As Integer = 6
Private Const FILE_ELAB_START_ENEA As Integer = 8
Private Const FILE_ELAB_START_AFP As Integer = 9
Private Const FILE_ELAB_STOP As Integer = 10
Private Const FILE_SET_PRINTER As Integer = 12
Private Const FILE_PRINT As Integer = 13
Private Const FILE_EXIT As Integer = 15
'--- 20121010LC (inizio)
Public p_afp As CAfp
'--- 20121010LC (fine)


Public Sub StatusBar(sz$)
  'StatusBar1.Cls
  'StatusBar1.Print sz
  'StatusBar1.Panels(1) = sz
  StatusBar1.SimpleText = sz
  DoEvents
End Sub


Private Sub MDIForm_Load()
  Dim fi&, i&
  Dim a$, b$
  
  '20121010LC (inizio)
  If ON_ERR_RN Then On Error Resume Next
'Call aaaa
  Printer.Orientation = cdlLandscape
  On Error GoTo 0
  ChDrive App.Path
  ChDir App.Path
  'Set p_afp = New i2_Afp.CAFP
  'Set p_afp = CreateObject("i2_Afp.CAfp")
  Set p_afp = New CAfp
  Call p_afp.Init(MI_VB, Me)
  '20121010LC (fine)
End Sub


Private Sub MDIForm_Unload(ByRef Cancel As Integer)
  If Screen.MousePointer = vbHourglass Then
    Cancel = True
    Exit Sub
  End If
  ''''Call xlsChiudi(glo.xlsApp, glo.xlsOutput)
  mnuFile_Click FILE_EXIT
End Sub


Private Sub mnuFile_Click(ByRef Index As Integer)
'20150522LC
  Dim sErr$, sz$
  Dim frmB As frmElab
  
  If Screen.MousePointer = vbHourglass And Index <> FILE_ELAB_STOP Then Exit Sub
    sErr = ""
  Select Case Index
  Case FILE_NEW, FILE_OPEN
    If Index = FILE_NEW Then
      p_afp.nElab = 0 '20121010LC
    Else
      sz = InputBox("Progressivo numerico dell'analisi")
      If sz = "" Then
        sErr = "Operazione annullata dall'utente"
      ElseIf Not IsNumeric(sz) Then
        sErr = "Progressivo non valido"
      Else
        p_afp.nElab = Int(sz) '20121010LC
      End If
    End If
    If sErr <> "" Then
      Call MsgBox(sErr)
    Else
      Set frmB = New frmElab
      frmB.Show
      '20121021LC (inizio)
      If frmB.Tag <> "" Then
        Unload frmB
      End If
      '20121021LC (fine)
    End If
  
  Case FILE_SAVE_AS
    Call MsgBox("L'analisi viene salvata automaticamente")
  
  Case FILE_SAVE_PRM_AS
    If Forms.Count > 2 And Not ActiveForm Is Nothing Then
      Call ActiveForm.FormParametriSalva
    End If
  
  Case FILE_REP_OPZ
    Call frmRep.Show(vbModal)
  
  Case FILE_ELAB_START_ENEA  '20150522LC
    If ON_ERR_RN = True Then On Error Resume Next
    If Not ActiveForm Is Nothing Then
      ''''Call ActiveForm.Elabora(1) '20150616�C
    End If
    On Error GoTo 0
  
  Case FILE_ELAB_START_AFP
    If ON_ERR_RN = True Then On Error Resume Next
    If Not ActiveForm Is Nothing Then
      Call ActiveForm.Elabora(0)
    End If
    On Error GoTo 0
  
  Case FILE_ELAB_STOP
    If ON_ERR_RN = True Then On Error Resume Next
    If Not ActiveForm Is Nothing Then
      Call ActiveForm.Interrompi
    End If
    On Error GoTo 0
    
  Case FILE_SET_PRINTER
    With CommonDialog1
      .Flags = cdlPDPrintSetup
      Call .ShowPrinter
    End With
    
  Case FILE_PRINT
    If ON_ERR_RN = True Then On Error Resume Next
    If Not ActiveForm Is Nothing Then
      Call ActiveForm.Stampa
    End If
    On Error GoTo 0
    
  Case FILE_EXIT
    End
  End Select
  Exit Sub
End Sub


Private Sub mnuWindow_Click(Index As Integer)
  If Screen.MousePointer = vbHourglass Then Exit Sub
  Select Case Index
  Case 0  '&Sovrapponi
    Arrange vbCascade
  Case 1  'Affianca &Orizzontalmente
    Arrange vbTileHorizontal
  Case 2  'Affianca &Verticalmente
    Arrange vbTileVertical
  Case 3  '&Disponi icone
    Arrange vbArrangeIcons
  End Select
End Sub


Private Sub Toolbar1_ButtonClick(ByVal Button As ComctlLib.Button)
  Select Case Button.Key
  Case "New"
    Call mnuFile_Click(FILE_NEW)
  Case "Open"
    Call mnuFile_Click(FILE_OPEN)
  Case "Save"
    Call mnuFile_Click(FILE_SAVE_PRM_AS)
  Case "Calc"
    Call mnuFile_Click(FILE_ELAB_START_AFP)
  Case "Stop"
    Call mnuFile_Click(FILE_ELAB_STOP)
  Case "Print"
    Call mnuFile_Click(FILE_PRINT)
  Case "RepOpt"
    'Call mnuFile_Click(FILE_REP_OPZ)
    Call mnuFile_Click(FILE_ELAB_START_ENEA)
  End Select
End Sub

