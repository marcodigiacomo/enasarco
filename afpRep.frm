VERSION 5.00
Begin VB.Form frmRep 
   Caption         =   "Opzioni report"
   ClientHeight    =   3690
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   7380
   LinkTopic       =   "Form4"
   ScaleHeight     =   3690
   ScaleWidth      =   7380
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      Caption         =   "File di output"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Index           =   2
      Left            =   120
      TabIndex        =   13
      Top             =   3000
      Width           =   6075
      Begin VB.OptionButton Option1 
         Caption         =   "solo file di testo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   2580
         TabIndex        =   15
         Top             =   240
         Width           =   1695
      End
      Begin VB.OptionButton Option1 
         Caption         =   "solo file Excel"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   4440
         TabIndex        =   16
         Top             =   240
         Visible         =   0   'False
         Width           =   1575
      End
      Begin VB.OptionButton Option1 
         Caption         =   "file di testo + file Excel"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   180
         TabIndex        =   14
         Top             =   240
         Value           =   -1  'True
         Width           =   2235
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Chiudi"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6300
      TabIndex        =   17
      Top             =   3180
      Width           =   975
   End
   Begin VB.Frame Frame1 
      Caption         =   "Elementi di stampa"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2895
      Index           =   1
      Left            =   120
      TabIndex        =   0
      Top             =   60
      Width           =   7155
      Begin VB.CheckBox Check1 
         Caption         =   "Evoluzione montanti contributivi"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   7
         Left            =   2580
         TabIndex        =   18
         Top             =   1440
         Value           =   1  'Checked
         Width           =   3195
      End
      Begin VB.CheckBox Check1 
         Caption         =   "File afp_opz_ini"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   1
         Left            =   2580
         TabIndex        =   12
         Top             =   360
         Value           =   1  'Checked
         Width           =   1755
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Bil. tecn. ministeriale"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   11
         Left            =   2580
         TabIndex        =   10
         Top             =   2160
         Value           =   1  'Checked
         Width           =   2175
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Valori capitali"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   12
         Left            =   240
         TabIndex        =   11
         Top             =   2460
         Value           =   1  'Checked
         Width           =   1995
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Coefficienti variabili"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   240
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   720
         Value           =   1  'Checked
         Width           =   2175
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Sviluppo superstiti"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   4
         Left            =   240
         TabIndex        =   4
         Top             =   1080
         Value           =   1  'Checked
         Width           =   2055
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Bilancio tecnico"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Index           =   10
         Left            =   240
         TabIndex        =   9
         Top             =   2160
         Value           =   1  'Checked
         Width           =   1875
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Evoluzione pensionati non contribuenti"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   9
         Left            =   2580
         TabIndex        =   8
         Top             =   1800
         Value           =   1  'Checked
         Width           =   3795
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Evoluzione pensioni"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   8
         Left            =   240
         TabIndex        =   7
         Top             =   1800
         Value           =   1  'Checked
         Width           =   2175
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Evoluzione contributi"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   6
         Left            =   240
         TabIndex        =   6
         Top             =   1440
         Value           =   1  'Checked
         Width           =   2235
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Evoluzione redditi IRPEF e volume d'affari IVA"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   5
         Left            =   2580
         TabIndex        =   5
         Top             =   1080
         Value           =   1  'Checked
         Width           =   4455
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Sviluppo collettivitą"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   2580
         TabIndex        =   3
         Top             =   720
         Value           =   1  'Checked
         Width           =   2055
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Parametri di input"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   1
         Top             =   360
         Value           =   1  'Checked
         Width           =   1935
      End
   End
End
Attribute VB_Name = "frmRep"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private Sub Command1_Click()
  Dim i%
  Dim req As Collection
Call MsgBox("TODO - req")
Stop
  
  Set req = New Collection
  For i = 0 To 12
    req.Add CStr(frmRep.Check1(i).Value), "k2" & i
  Next i
  If frmRep.Option1(2).Value = True Then
    req.Add CStr(2), "r2"
  ElseIf frmRep.Option1(1).Value = True Then
    req.Add CStr(1), "r2"
  Else
    req.Add CStr(0), "r2"
  End If
  Call MdiAfp.p_afp.OpzioniReport(req, 1)
  Unload Me
End Sub


Private Sub Form_Load()
  Static bInit As Boolean
  Dim i&
  Dim req As Collection
Call MsgBox("TODO - req")
Stop
  
  If bInit = True Then
    Call MdiAfp.p_afp.OpzioniReport(req, 0)
    For i = 0 To 12
      Check1(i).Value = req("k2" & i)
    Next i
    For i = 0 To 2
      frmRep.Option1(i).Value = req("r2") = i
    Next i
  End If
  Me.Move (Screen.Width - Me.Width) / 2, (Screen.Height - Me.Height) / 2
  bInit = True
End Sub

