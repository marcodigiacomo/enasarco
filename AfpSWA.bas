Attribute VB_Name = "modSWA"
'****************************************************************************************************
'SCOPO
'  Routine di uso specifico per il prototipo
'STORIA DELLE VERSIONI
'  20140115LC  Prima implementazione
'****************************************************************************************************
Option Explicit


'*********
'Prototipi
'*********
'--- 1) Calcola l'aliquota provvigionale
'Public Function Aliq(tipo As Integer, anTr As Integer) As Double
'--- 2) Calcola il coefficiente di trasformazione del montante in rendita
'Public Function CCR(anno As Integer, x As Double) As Double
'--- 3) Calcola il massimale annuo Mono/Pluri
'Public Function MaxMP(mand As Integer, anTr As Integer) As Double
'--- 4) Calcola il minimale annuo Mono/Pluri
'Public Function MinMP(mand As Integer, anTr As Integer) As Double
'--- 5) Calcola il coefficiente di rivalutazione del montante
'Public Function MontRiv(tipo As Integer, anno As Integer) As Double
'--- 6) Calcola la quota A di pensione
'Public Function PensA(x As Double, h As Double) As Double
'--- 7) Calcola la detrazione sulla quota A di pensione
'Public Function PensA_Detr(x As Double) As Double
'--- 8) Calcola la quota B di pensione
'Public Function PensB(x As Double, h As Double) As Double


'***************
'Implementazione
'***************
Public Function aliq(ByRef tipo As Integer, ByRef anTr As Integer, iop&) As Double
'********************************************************************************
'SCOPO
'  Calcola l'aliquota provvigionale
'ARGOMENTI DI INPUT
'  tipo=0   totale (default se diverso dagli altri valori previsti)
'      =1   previdenziale
'      =2   di solidariet�
'  anTr     Anno*10 + Trimestre
'ARGOMENTI DI OUTPUT O INPUT/OUTPUT
'  Nessuno
'VALORE RESTITUITO
'  Aliquota secondo tipo (non � espressa in percento)
'IMPLEMENTAZIONI
'  20140115LC prima implementazione
'********************************************************************************
  Dim y As Double  'Variabile di appoggio per valore restituito
  
  If tipo = 1 Then  'Previdenziale
    If anTr <= 19783 Then '<= #9/30/1978#
      y = 0.06
    ElseIf anTr <= 19832 Then '<= #6/30/1983#
      y = 0.08
    ElseIf anTr <= 19982 Then '<= #6/30/1998#
      y = 0.1
    ElseIf anTr <= 20044 Then '<= #12/31/2004#
      y = 0.115
    ElseIf anTr <= 20054 Then '<= #12/31/2005#
      y = 0.12
    ElseIf anTr <= 20164 Then '<= #12/31/2016#
      y = 0.125
    ElseIf anTr <= 20174 Then '<= #12/31/2017#
      y = 0.1255
    ElseIf anTr <= 20184 Then '<= #12/31/2018#
      y = 0.13
    ElseIf anTr <= 20194 Then '<= #12/31/2019#
      y = 0.135
    Else
      y = 0.14
    End If
  ElseIf tipo = 2 Then  'Assistenziale
    If anTr <= 20034 Then '<= #12/31/2003#
      y = 0
    ElseIf anTr <= 20124 Then '<= #12/31/2012#
      y = 0.01
    ElseIf anTr <= 20134 Then '<= #12/31/2013#
      y = 0.0125
    ElseIf anTr <= 20144 Then '<= #12/31/2014#
      y = 0.017
    ElseIf anTr <= 20154 Then '<= #12/31/2015#
      y = 0.0215
    ElseIf anTr <= 20164 Then '<= #12/31/2016#
      y = 0.026
    Else
      y = 0.03
    End If
  Else  'Totale
    y = aliq(1, anTr, 0) + aliq(2, anTr, 0)  'Previdenziale+Assistenziale
  End If
  aliq = y
End Function


Public Function CCR8(ByRef anno As Integer, ByRef x As Double, iop&) As Double
'********************************************************************************
'SCOPO
'  Calcola il coefficiente di trasformazione del montante in rendita
'ARGOMENTI DI INPUT
'  anno di riferimento
'  et� in anni commerciali, con parte frazionaria
'ARGOMENTI DI OUTPUT O INPUT/OUTPUT
'  Nessuno
'VALORE RESTITUITO
'  Coefficiente di conversione in rendita (in percento)
'NOTE
'  Gli array hanno indice inferiore pari a 0, per consentire la portabilit� immediata in C++ e .NET
'IMPLEMENTAZIONI
'  20140115LC  prima implementazione
'  20150421LC  modifiche per casi limite (x<xmin o x>xmax)
'  20150521LC  rinominato in CCR8 (come in modEnea)
'********************************************************************************
  '--- Costanti
  Const AMIN = 2004        'Anno minimo
  Const AMAX = 2013        'Anno massimo
  Const XMIN = 40          'Et� minima
  Const XMAX = 80          'Et� massima
  '--- Variabili statiche
  Static bInit As Byte     'Flag che indica se sono stati inizializzati i coefficienti
  Static v2004 As Variant  'Array dei coefficienti in vigore a partire dal 2004
  Static v2009 As Variant  'Array dei coefficienti in vigore a partire dal 2009
  Static v2013 As Variant  'Array dei coefficienti in vigore a partire dal 2013
  Static CR() As Variant   'Array bidimensionale dei coefficienti (per anno, et�)
  '--- Variabili ordinarie
  Dim ia As Integer        'usato come indice di ciclo, e limitato al range [0,AMAX-AMIN]
  Dim ix As Integer        'usata anche come indice di ciclo Et�-XMIN, e limitato al range [0,XMAX-XMIN]
  Dim xi As Integer        'Et� intera (usata anche come indice di ciclo Et�-XMIN)
  Dim mesi As Integer      'Mesi interamente trascorsi
  Dim y0 As Double         'Coefficiente all'et� intera
  Dim y1 As Double         'Coefficiente all'et� intera+1
  
If iop = 0 Then
  Call MsgBox("TODO")
  Stop
End If
  '--- Inizializzazione
  If bInit = 0 Then
    bInit = 1
    '--- Legge i dati nel range [AMIN-AMAX]
    v2004 = Array(3.246, 3.302, 3.361, 3.422, 3.487, 3.554, 3.626, 3.7, 3.779, 3.862, _
                  3.949, 4.041, 4.139, 4.242, 4.352, 4.468, 4.591, 4.72, 4.86, 5.006, _
                  5.163, 5.334, 5.514, 5.706, 5.911, 6.136, 6.379, 6.64, 6.927, 7.232, _
                  7.563, 7.924, 8.319, 8.75, 9.227, 9.751, 10.335, 10.983, 11.701, 12.499, 13.378)
    v2009 = Array(3.0664, 3.1139, 3.1636, 3.2156, 3.27, 3.3268, 3.3864, 3.4488, 3.5142, 3.5829, _
                  3.6551, 3.7324, 3.8139, 3.8999, 3.9906, 4.0868, 4.1887, 4.2968, 4.4116, 4.5339, _
                  4.6639, 4.803, 4.9519, 5.1114, 5.283, 5.4672, 5.6653, 5.8785, 6.1087, 6.3581, _
                  6.6284, 6.9219, 7.2417, 7.5892, 7.9685, 8.3828, 8.8339, 9.327, 9.8678, 10.46, 11.1085)
    v2013 = Array(3.043, 3.089, 3.136, 3.186, 3.238, 3.292, 3.349, 3.409, 3.471, 3.537, _
                  3.605, 3.679, 3.756, 3.837, 3.923, 4.014, 4.111, 4.213, 4.321, 4.436, _
                  4.559, 4.689, 4.828, 4.977, 5.136, 5.306, 5.488, 5.684, 5.896, 6.124, _
                  6.372, 6.642, 6.936, 7.256, 7.603, 7.981, 8.395, 8.847, 9.341, 9.886, 10.484)
    ReDim CR(0 To AMAX - AMIN, 0 To XMAX - XMIN)
    For ix = 0 To XMAX - XMIN
      For ia = 0 To AMAX - AMIN
        If ia >= AMAX - AMIN Then
          CR(ia, ix) = v2013(ix)
        ElseIf ia >= 2009 - AMIN Then
          CR(ia, ix) = v2009(ix)
        Else
          CR(ia, ix) = v2004(ix)
        End If
      Next ia
    Next ix
  End If
  '--- indice dell'anno da considerare (entro i limiti)
  If anno > AMAX Then
    ia = AMAX - AMIN
  ElseIf anno > AMIN Then
    ia = anno - AMIN
  Else
    ia = 0
  End If
  '--- et� intera
  xi = Int(x)
  '--- indice dell'et� intera nell'array / mesi interamente trascorsi
  '--- 20150421LC (inizio)
  mesi = 0
  If xi > XMAX Then
    ix = XMAX - XMIN
  ElseIf xi >= XMIN Then
    ix = xi - XMIN
    If Math.Abs(x - xi) > AA0 Then  '--- mai fidarsi di un floating point esattamente uguale ad un intero
      mesi = Int(x * 12 + AA0) - xi * 12  '--- idem c.s., +AA0 rende "sicuro" l'arrotondamento di floating point
    End If
  Else
    ix = 0
  End If
  '--- 20150421LC (fine)
  '--- Coefficiente di trasformazione all'et� intera
  y0 = CR(ia, ix)
  '--- Coefficiente di trasformazione all'et� intera+1
  'If ix < XMAX - XMIN Then
  If xi >= XMIN And xi < XMAX Then  '20150421LC
    y1 = CR(ia, ix + 1)
  Else
    'y1 = y0  '--- y1 non verr� usato, si commenta l'assegnazione per sottolineare questo fatto
    mesi = 0
  End If
  '--- Restituzione del coefficiente, interpolato alla 3 cifra decimale, eeventualmente interpolato
  If mesi > 0 Then
    '--- Si interpola dapprima sull'incremento mensile (come espressamente scritto nel regolamento) al 3� decimale
    'Round((3.537 - 3.471) / 12, 3) = Round(0.066/12,3) = Round(0.055/12,3) da erroneamente 0.005, usare ARRV
    'CCR8 = Round(y0 + Round((y1 - y0) / 12, 3) * mesi, 3)
    CCR8 = ARR5(y0 + ARR5((y1 - y0) / 12#, 1000#) * mesi, 1000#)
  Else
    'CCR8 = Round(y0, 3)  '--- Non occorre interpolare, y1 non viene usato
    CCR8 = ARR5(y0, 1000#)  '--- Non occorre interpolare, y1 non viene usato
  End If
End Function


Public Function MaxMP(ByRef mand As Double, ByRef anTr As Integer, iop&) As Double
'********************************************************************************
'SCOPO
'  Calcola il massimale annuo Mono/Pluri
'ARGOMENTI DI INPUT
'  mand<=1  mandato MONO (default se diverso dagli altri valori previsti)
'      >=2  mandato PLURI
'  anTr     Anno*10 + Trimestre
'ARGOMENTI DI OUTPUT O INPUT/OUTPUT
'  Nessuno
'VALORE RESTITUITO
'  Massimale in euro
'IMPLEMENTAZIONI
'  20140115LC prima implementazione
'********************************************************************************
  Dim y As Double  'Variabile di appoggio per il valore restituito
  
If iop = 0 Then
  Call MsgBox("TODO")
  Stop
End If
  If mand >= 2 Then  'mandato Pluri
    If anTr <= 19832 Then '<= #9/30/1983#
      y = 3873.43
    ElseIf anTr <= 19874 Then '<= #12/31/1987#
      y = 5164.67
    ElseIf anTr <= 19884 Then '<= #12/31/1988#
      y = 8263.31
    ElseIf anTr <= 19982 Then '<= #06/30/1998#
      y = 10329.14
    ElseIf anTr <= 20014 Then '<= #12/31/2001#  'passaggio all'euro
      y = 12394.97
    ElseIf anTr <= 20034 Then '<= #12/31/2003#
      y = 12395#
    ElseIf anTr <= 20054 Then '<= #12/31/2005#
      y = 14027#
    ElseIf anTr <= 20074 Then '<= #12/31/2007#
      y = 14561#
    ElseIf anTr <= 20094 Then '<= #12/31/2009#
      y = 15202#
    ElseIf anTr <= 20114 Then '<= #12/31/2011#
      y = 15810#
    ElseIf anTr <= 20124 Then '<= #12/31/2012#
      y = 20000#
    ElseIf anTr <= 20134 Then '<= #12/31/2013#
      y = 22000#
    ElseIf anTr <= 20144 Then '<= #12/31/2014#
      y = 23000#
    Else
      y = 25000#
    End If
  Else  'mandato Mono
    If anTr <= 19783 Then '<= #9/30/1978#
      y = 4648.11
    ElseIf anTr <= 19784 Then '<= #12/31/1978#
      y = 6197.48
    ElseIf anTr <= 19832 Then '<= #06/30/1983#
      y = 12394.97
    ElseIf anTr <= 19884 Then '<= #12/31/1988#
      y = 15493.71
    ElseIf anTr <= 19982 Then '<= #06/30/1998#
      y = 17559.53
    ElseIf anTr <= 20014 Then '<= #12/31/2001#  'passaggio all'euro
      y = 21691.19
    ElseIf anTr <= 20034 Then '<= #12/31/2003#
      y = 21691#
    ElseIf anTr <= 20054 Then '<= #12/31/2005#
      y = 24548#
    ElseIf anTr <= 20074 Then '<= #12/31/2007#
      y = 25481#
    ElseIf anTr <= 20094 Then '<= #12/31/2009#
      y = 26603#
    ElseIf anTr <= 20114 Then '<= #12/31/2011#
      y = 27667#
    ElseIf anTr <= 20124 Then '<= #12/31/2012#
      y = 30000#
    ElseIf anTr <= 20134 Then '<= #12/31/2013#
      y = 32500#
    ElseIf anTr <= 20144 Then '<= #12/31/2014#
      y = 35000#
    Else
      y = 37500#
    End If
  End If
  MaxMP = y
End Function


Public Function MinMP(ByRef mand As Double, ByRef anTr As Integer, iop&) As Double
'********************************************************************************
'SCOPO
'  Calcola il minimale annuo Mono/Pluri
'ARGOMENTI DI INPUT
'  mand<=1  mandato MONO (default se diverso dagli altri valori previsti)
'      >=2  mandato PLURI
'  anTr     Anno*10 + Trimestre
'ARGOMENTI DI OUTPUT O INPUT/OUTPUT
'  Nessuno
'VALORE RESTITUITO
'  Minimale annuo in euro
'IMPLEMENTAZIONI
'  20140115LC prima implementazione
'********************************************************************************
  Dim y As Double  'Variabile di appoggio per il valore restituito
  
  If mand >= 2 Then  'mandato Pluri
    If anTr <= 19783 Then '<= #9/30/1978#
      y = 18.59
    ElseIf anTr <= 19982 Then '<= #6/30/1998#
      y = 61.97
    ElseIf anTr <= 20014 Then '<= #12/31/2001#  'passaggio all'euro
      y = 123.95
    ElseIf anTr <= 20044 Then '<= #12/31/2004#
      y = 124
    ElseIf anTr <= 20064 Then '<= #12/31/2006#
      y = 350
    ElseIf anTr <= 20084 Then '<= #12/31/2008#
      y = 364
    ElseIf anTr <= 20104 Then '<= #12/31/2010#
      y = 381
    ElseIf anTr <= 20114 Then '<= #12/31/2011#
      y = 396
    ElseIf anTr <= 20124 Then '<= #12/31/2011#
      y = 400
    Else
      y = 412
    End If
  Else  'mandato Mono
    If anTr <= 19783 Then '<= #9/30/1978#
      y = 30.99
    ElseIf anTr <= 19982 Then '<= #6/30/1998#
      y = 123.95
    ElseIf anTr <= 20014 Then '<= #12/31/2001#  'passaggio all'euro
      y = 247.9
    ElseIf anTr <= 20044 Then '<= #12/31/2004#
      y = 248
    ElseIf anTr <= 20054 Then '<= #12/31/2005#
      y = 700
    ElseIf anTr <= 20074 Then '<= #12/31/2007#
      y = 727
    ElseIf anTr <= 20094 Then '<= #12/31/2009#
      y = 759
    ElseIf anTr <= 20114 Then '<= #12/31/2011#
      y = 789
    ElseIf anTr <= 20124 Then '<= #12/31/2012#
      y = 800
    Else
      y = 824
    End If
  End If
  MinMP = y
End Function


Public Function MontRiv(ByRef tipo As Integer, ByRef anno As Integer, iop&) As Double
'********************************************************************************
'SCOPO
'  Calcola il coefficiente di rivalutazione del montante
'ARGOMENTI DI INPUT
'  tipo=0  restituisce il CoeRiv (default se diverso dagli altri valori definiti(
'       1  restituisce 1+CoeRiv (memorizzato in CR)
'       2  restituisce la produttoria di CoeRiv (memorizzata in PrdCR)
'  anno    anno di riferimento
'ARGOMENTI DI OUTPUT O INPUT/OUTPUT
'  Nessuno
'VALORE RESTITUITO
'  Massimale in euro
'NOTE
'  1) La sequenza C1946 C1947 ... C2002 C2003 C2004 ... C2012 viene estasa per altri 100 anni usando C212
'  2) Da essa si ricava 1+C1946 1+C1947 ... 1+C2002 1+C2003 1+C2004 ... 1+C2012, e questa viene memorizzata in CR()
'     dunque avremo, ad esempio CR(2012)=1+C2012
'  3) Da essa si ricava la produttoria PrdCR(2003) che vale
'     PrdCR(2003)=1
'     PrdCR(anno-1)=PrdCR(anno)/CR(anno) per anni precedenti al 2003
'     PrdCR(anno)=PrdCR(anno-1)*CR(anno) per anni successivi al 2013
'  In realt� CR(anno) viene memorizzato in CR(anno-AMIN), ma questo non cambia la logica di costruzione dei valori
'IMPLEMENTAZIONI
'  20140115LC prima implementazione
'********************************************************************************
  '--- Costanti
  Const AMIN As Integer = 1946   'Anno minimo dei coefficienti
  Const ABASE As Integer = 2003  'Stabilisce l'anno pase, per il quale la produttoria PrdCR(ABASE)=1.
  Const AMAX As Integer = 2012   'Anno massimo dei coefficienti (superato il quale si adotto l'ultimo)
  Const ALIM As Integer = AMAX + 100  'Anno limite di memorizzazione dei coefficienti, oltre il quale si estrapola con fact
  '--- Variabili statiche
  Static bInit As Byte
  Static CR() As Double
  Static PrdCR() As Double
  '--- Variabili ordinarie
  Dim v As Variant    'Array di appoggio per l'inizializzazione di CR()
  Dim ia As Integer   'indice dell'anno
  Dim fact As Double  'fattore per estrapolare oltre AMAX
  
  '--- Inizializzazione
  If bInit = 0 Then
    bInit = 1
    '--- Legge i dati nel range [AMIN-AMAX]
    'I dati dal 2009 al 2011 sono stati presi dai PDF e sono lievemente diversi da quelli avuti in Excel
    'Dal 2012 si usa per ora l'1,5%
    v = Array(0.44984, 0.6798, 0.847691, 0.803377, _
              0.622137, 0.467254, 0.264475, 0.117614, 0.104072, 0.101766, 0.100771, 0.087979, 0.087084, 0.080711, _
              0.079959, 0.076694, 0.081432, 0.09136, 0.105468, 0.111816, 0.110107, 0.104326, 0.099969, 0.087896, _
              0.089733, 0.099558, 0.100769, 0.099769, 0.12137, 0.146567, 0.156004, 0.190509, 0.216775, 0.210426, _
              0.203363, 0.226929, 0.214364, 0.205767, 0.202694, 0.186164, 0.160219, 0.142703, 0.126341, 0.115314, _
              0.105217, 0.101013, 0.097075, 0.088611, 0.07299, 0.065726, 0.062054, 0.055871, 0.053597, 0.056503, _
              0.051781, 0.047781, 0.043698, 0.041614, 0.039272, 0.040506, 0.035386, 0.033937, 0.034625, 0.0332001, _
              0.01866837, 0.01016589, 0.015)
    '--- Inserisce i dati nel range [AMIN-AMAX]
    ReDim CR(0 To ALIM - AMIN)
    For ia = 0 To AMAX - AMIN
      CR(ia) = 1 + v(ia)
    Next ia
    '--- Inserisce i dati nel range [AMAX+1-ALIM]
    For ia = AMAX + 1 - AMIN To ALIM - AMIN
      CR(ia) = CR(ia - 1)
    Next ia
    '--- Inserisce la produttoria dei dati nel range [AMIN-ABASE]
    ReDim PrdCR(0 To ALIM - AMIN)
    PrdCR(ABASE - AMIN) = 1#
    For ia = ABASE - AMIN To AMIN + 1 - AMIN Step -1
      PrdCR(ia - 1) = PrdCR(ia) / CR(ia)
    Next ia
    '--- Inserisce la produttoria dei dati nel range [ABASE-ALIM]
    For ia = ABASE + 1 - AMIN To ALIM - AMIN
      PrdCR(ia) = PrdCR(ia - 1) * CR(ia)
    Next ia
  End If
  '--- Restituzione del valore
  ia = anno - AMIN
  If ia < 0 Then  '--- Sotto LBound, restitusce 0
    MontRiv = 0#
  Else
    If ia > ALIM Then '--- Superato UBound, estrapola l'ultimo valore
      fact = CR(ALIM) ^ (ia - ALIM)
      ia = ALIM
    Else
      fact = 1#
    End If
    If tipo = 1 Then
      MontRiv = CR(ia) * fact
    ElseIf tipo = 2 Then
      MontRiv = PrdCR(ia) * fact
    Else
      MontRiv = CR(ia) * fact - 1#
    End If
  End If
End Function


Public Function PensA(ByRef x As Double, ByRef h As Double) As Double
'********************************************************************************
'SCOPO
'  Calcola la quota A di pensione
'ARGOMENTI DI INPUT
'  x   media delle provvigioni
'  h   anzianit�
'ARGOMENTI DI OUTPUT O INPUT/OUTPUT
'  Nessuno
'VALORE RESTITUITO
'  Pensione in quota A, non arrotondata
'IMPLEMENTAZIONI
'  20140115LC prima implementazione
'********************************************************************************
  Dim y As Double  'Valori di appoggio per il valore restituito
  
  y = x * h / 40# * 0.7
  PensA = y
End Function


Public Function PensA_Detr(ByRef x As Double) As Double
'********************************************************************************
'SCOPO
'  Calcola la detrazione sulla quota A di pensione
'ARGOMENTI DI INPUT
'  x   pensione in quota A
'ARGOMENTI DI OUTPUT O INPUT/OUTPUT
'  Nessuno
'VALORE RESTITUITO
'  Pensione in quota A, non arrotondata
'IMPLEMENTAZIONI
'  20140115LC prima implementazione
'********************************************************************************
  Dim y As Double
  
  If x <= 2582.28 Then
    y = 0#
  ElseIf x <= 3098.74 Then
    'y = 0.1 * (x - 2582.28)
    y = -258.228 + 0.1 * x
  ElseIf x <= 3615.2 Then
    'y = 0.1 * (3098.74 - 2582.28) + 0.12 * (x - 3098.74)
    y = -320.2028 + 0.12 * x
  ElseIf x <= 4131.66 Then
    'y = 0.1 * (3098.74 - 2582.28) + 0.12 * (3615.2 - 3098.74) + 0.14 * (x - 3615.2)
    y = -392.5068 + 0.14 * x
  ElseIf x <= 4648.11 Then
    'y = 0.1 * (3098.74 - 2582.28) + 0.12 * (3615.2 - 3098.74) + 0.14 * (4131.66 - 3615.2) + 0.16 * (x - 4131.66)
    y = -475.14 + 0.16 * x
  ElseIf x <= 5164.57 Then
    'y = 0.1 * (3098.74 - 2582.28) + 0.12 * (3615.2 - 3098.74) + 0.14 * (4131.66 - 3615.2) + 0.16 * (4648.11 - 4131.66) + 0.18 * (x - 4648.11)
    y = 0.1 * (3098.74 - 2582.28) + 0.12 * (3615.2 - 3098.74) + 0.14 * (4131.66 - 3615.2) + 0.16 * (4648.11 - 4131.66) + 0.18 * (-4648.11) + 0.18 * x
  Else
    'y = 0.1 * (3098.74 - 2582.28) + 0.12 * (3615.2 - 3098.74) + 0.14 * (4131.66 - 3615.2) + 0.16 * (4648.11 - 4131.66) + 0.18 * (5164.57 - 4648.11) + 0.2 * (x - 5164.57)
    y = -671.3936 + 0.2 * x
  End If
  PensA_Detr = y
End Function


Public Function PensB(ByRef x As Double, ByRef h As Double) As Double
'********************************************************************************
'SCOPO
'  Calcola la quota B di pensione
'ARGOMENTI DI INPUT
'  x   media delle provvigioni
'  h   anzianit�
'ARGOMENTI DI OUTPUT O INPUT/OUTPUT
'  Nessuno
'VALORE RESTITUITO
'  Pensione in quota B, non arrotondata
'IMPLEMENTAZIONI
'  20140115LC prima implementazione
'********************************************************************************
  Dim y As Double  'Valore di appoggio per il valore restituito
  
  If x <= 6197.48 Then
    y = 0.0175 * x
  ElseIf x <= 9296.22 Then
    'y = 6197.48 * 0.0175 + (x - 6197.48) * 0.0155
    y = 12.39496 + 0.0155 * x
  ElseIf x <= 12394.97 Then
    'y = 6197.48 * 0.0175 + (9296.22 - 6197.48) * 0.0155 + (x - 9296.22) * 0.0135
    y = 30.9874 + 0.0135 * x
  Else
    'y = 6197.48 * 0.0175 + (9296.22 - 6197.48) * 0.0155 + (12394.97 - 9296.22) * 0.0135 + (x - 12394.97) * 0.0115
    y = 55.77734 + 0.0115 * x
  End If
  PensB = y * h
End Function


