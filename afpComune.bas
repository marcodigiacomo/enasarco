Attribute VB_Name = "modComune"
Option Explicit

'--- DATABASE (20121024LC) ---
Public Const DB_ACCESS = 1
Public Const DB_SQL_SERVER = 2
Public Const DB_ORACLE = 3
Public Const DB_MYSQL = 4

Public Const F_PARAM  As Long = 1
Public Const F_TASSI_STIME  As Long = 2
Public Const F_BASI_TECNICHE  As Long = 3
Public Const F_REQUISITI_DI_ACCESSO  As Long = 4
Public Const F_CONTRIBUZIONE  As Long = 5
Public Const F_MISURA_PRESTAZIONI  As Long = 6
Public Const F_NUOVI_ISCRITTI  As Long = 7
Public Const F_RISULTATI  As Long = 8

Public Const MI_BATCH As Long = 0
Public Const MI_VB As Long = 1
Public Const MI_ASP As Long = 2
Public Const MI_ASPX As Long = 3

Public Const NL As String = vbCrLf

Public Const ON_ERR_EH  As Boolean = False       'TRUE  in produzione
Public Const ON_ERR_RN  As Boolean = ON_ERR_EH  'TRUE  in produzione
Public Const ON_ERR_RN1 As Boolean = ON_ERR_EH  'TRUE  in produzione
Public Const ON_ERR_RN2 As Boolean = ON_ERR_EH  'TRUE  in produzione
Public Const ON_ERR_RN3 As Boolean = True       'TRUE  in produzione


Public Type TCV '20121102LC
  '******************************
  'Frame TASSI ED ALTRE STIME (2)
  '******************************
  '--- 2.1 Tassi di valutazione e rivalutazione ---
  Tv As Double          ' 0
  ti As Double          ' 1
  Tev As Double         ' 2
  Trn As Double         ' 3
  '--- 2.2 Tassi di rivalutazione dei montanti contributivi ---
  Trmc0 As Double       ' 4
  Trmc1 As Double       ' 5
  Trmc2 As Double       ' 6
  Trmc3 As Double       ' 7
  Trmc4 As Double       ' 8
  Trmc5 As Double       ' 9
  Trmc6 As Double       ' 10
  '--- 2.3 Tassi di rivalutazione di redditi e volumi d'affari ---
  Ts1a As Double        ' 11
    Ts2a As Double        ' 12
  Ts1p As Double        ' 13
    Ts2p As Double        ' 14
  Ts1n As Double        ' 15
    Ts2n As Double        ' 16
  '--- 2.4 Rivalutazione delle pensioni ---
  Tp1Max As Double      ' 17
  Tp1Al As Double       ' 18
  Tp2Max As Double      ' 19
  Tp2Al As Double       ' 20
  Tp3Max As Double      ' 21
  Tp3Al As Double       ' 22
  Tp4Max As Double      ' ??
  Tp4Al As Double       ' ??
  Tp5Max As Double      ' ??
  Tp5Al As Double       ' ??
  '--- 2.5 Altre stime ----
  PrcPVattM As Double   ' 23
  PrcPVattF As Double   ' 24
  PrcPAattM As Double   ' 25
  PrcPAattF As Double   ' 26
  PrcPCattM As Double   ' 27
  PrcPCattF As Double   ' 28
  PrcPTattM As Double   ' 29
  PrcPTattF As Double   ' 30
  '******************************
  'Frame REQUISITI DI ACCESSO (4)
  '******************************
  '--- 4.1 Pensione di vecchiaia ordinaria ----
  Pveo30M As Long       ' 31
  Pveo30F As Long       ' 32
  Pveo65M As Long       ' 33
  Pveo65F As Long       ' 34
  Pveo98M As Long       ' 35
  Pveo98F As Long       ' 36
  '--- 4.2 Pensione di vecchiaia anticipata ----
  Pvea35M As Long       ' 37
  Pvea35F As Long       ' 38
  Pvea58M As Long       ' 39
  Pvea58F As Long       ' 40
  '--- 4.3 Pensione di vecchiaia anticipata ----
  Pvep70M As Long       ' 41
  Pvep70F As Long       ' 42
  '--- 4.4 Pensione di inabilitā ----
  Pinab2M As Long       ' 43
  Pinab2F As Long       ' 44
  Pinab10M As Long      ' 45
  Pinab10F As Long      ' 46
  Pinab35M As Long      ' 47
  Pinab35F As Long      ' 48
  '--- 4.5 Pensione di invaliditā ----
  Pinv5M As Long        ' 49
  Pinv5F As Long        ' 50
  Pinv70 As Double      ' 51
  '--- 4.6 Pensione indiretta ----
  Pind2M As Long        ' 52
  Pind2F As Long        ' 53
  Pind20M As Long       ' 54
  Pind20F As Long       ' 55
  Pind30M As Long       ' 56
  Pind30F As Long       ' 57
  '--- 4.7 Restituzione dei contributi ----
  RcHminM As Long       ' 58
  RcHminF As Long       ' 59
  RcXminM As Long       ' 60
  RcXminF As Long       ' 61
  RcPrcCon As Double    ' 62
  'RcPrcRiv As Double   ' 63  '20150512LC (commentato)
  '--- 4.8 Totalizzazione ----
  TzHminM As Long       ' 64
  TzHminF As Long       ' 65
  TzXminM As Long       ' 66
  TzXminF As Long       ' 67
  TzRivMin As Double    ' 68
  '***********************
  'Frame CONTRIBUZIONE (5)
  '***********************
  '--- 5.1 Contribuzione ridotta ----
  EtaR As Long          ' 69
  XMaxR As Long         ' 70
  AnniR As Long         ' 71
  '--- 5.2 Soggettivo ----
  Sogg0Min As Double    ' 72
  Sogg0MinR As Double   ' 73
  Sogg1Al As Double     ' 74
  Sogg1AlR As Double    ' 75
  Sogg1Max As Double    ' 76
  Sogg1MaxR As Double   ' 77
  Sogg2Al As Double     ' 78
  Sogg2AlR As Double    ' 79
  Sogg2Max As Double    ' 80
  Sogg2MaxR As Double   ' 81
  Sogg3Al As Double     ' 82
  Sogg3AlR As Double    ' 83
  SoggArt25 As Double   ' 84
  '--- 5.3 Integrativo / FIRR ----
  Int0Min As Double     ' 85
  Int0MinR As Double    ' 86
  Int1Al As Double      ' 87
  Int1AlR As Double     ' 88
  Int1Max As Double     ' 89
  Int1MaxR As Double    ' 90
  Int2Al As Double      ' 91
  Int2AlR As Double     ' 92
  IntArt25 As Double    ' 93
  '--- 5.4 Assistenziale ----
  Ass0Min As Double     ' 94
  Ass0MinR As Double    ' 95
  Ass1Al As Double      ' 96
  Ass1AlR As Double     ' 97
  '--- 5.5 Altro ----
  Mat0Min As Double     ' 98
  Mat0MinR As Double    ' 99
  '****************************
  'Frame MISURA PRESTAZIONI (6)
  '****************************
  '--- 6.1 Scaglioni IRPEF ----
  ScA As Double         ' 100
  AlA As Double         ' 101
  ScB As Double         ' 102
  AlB As Double         ' 103
  ScC As Double         ' 104
  AlC As Double         ' 105
  ScD As Double         ' 106
  AlD As Double         ' 107
  ScE As Double         ' 108
  AlE As Double         ' 109
  ScF As Double         ' 110
  AlF As Double         ' 111
  ScG As Double         ' 112
  AlG As Double         ' 113
  '--- 6.2 Pensione ----
  PMin As Double        ' 114
  NMR As Long           ' 115
  NTR As Long           ' 116
  '--- 6.3 Supplementi di pensione ----
  SpAnni As Long        ' 117
  SpEtaMax As Long      ' 118
  SpPrcCon As Double    ' 119
  'SpPrcRiv As Double   ' 120  '20150512LC  '(commentato)
  '--- 6.4 Reversibilitā superstiti ----
  av As Double          ' 121
  Avo As Double         ' 122
  Avoo As Double        ' 123
  Ao As Double          ' 124
  Aoo As Double         ' 125
  Aooo As Double        ' 126
  '******************************
  'Frame TASSI ED ALTRE STIME (2)
  '******************************
  '--- 2.1 Tassi di valutazione e rivalutazione ---
  Prd_Tv As Double      ' 127
  Prd_Ti As Double      ' 128
  prd_Tev As Double     ' 129
  prd_Trn As Double     ' 130
  '--- 2.2 Tassi di rivalutazione dei montanti contributivi ---
  Prd_Trmc0 As Double   ' 131
  Prd_Trmc1 As Double   ' 132
  Prd_Trmc2 As Double   ' 133
  Prd_Trmc3 As Double   ' 134
  Prd_Trmc4 As Double   ' 135
  Prd_Trmc5 As Double   ' 136
  Prd_Trmc6 As Double   ' 137
  '--- 2.3 Tassi di rivalutazione di redditi e volumi d'affari ---
  Prd_Ts1a As Double    ' 138
  Prd_Ts1p As Double    ' 140
  Prd_Ts1n As Double    ' 142
  '---
  Prd_Ts2a As Double    ' 139
  Prd_Ts2p As Double    ' 141
  Prd_Ts2n As Double    ' 143
End Type


'--- 20170307LC (inizio)
Public Type TCV_R1A
  cvR1Sogg0Min As Double
  cvR1Sogg1Al As Double
  cvR1Sogg1Max As Double
End Type


Public Type TCV_R1
  cvR1Anno As Integer
  cvR1Anni As Byte
  cvR1EtaMax As Byte
  r1a() As TCV_R1A
End Type


Public Type TCV_R2
  cvR2Anno As Integer
  cvR2 As Byte
  cvR1Anni As Byte
  cvR1PercPIL As Double
End Type


Public Type TCV_R3
  cvR3Anno As Integer
  cvR3 As Byte
  cvR1PercENAS As Double
End Type
'--- 20170307LC (fine)


Public Type TWFD
  sg(8) As Double
End Type


Public Type TWFL  '20130302LC
  sg(8) As Long
End Type


Public Enum ecv
  '******************************
  'Frame TASSI ED ALTRE STIME (2)
  '******************************
  '--- 2.1 Tassi di valutazione e rivalutazione ---
  cv_Tv = 0
  cv_Ti = 1
  cv_Tev = 2
  cv_Trn = 3
  '--- 2.2 Tassi di rivalutazione dei montanti contributivi ---
  cv_Trmc0 = 4
  cv_Trmc1 = 5
  cv_Trmc2 = 6
  cv_Trmc3 = 7
  cv_Trmc4 = 8
  cv_Trmc5 = 9
  cv_Trmc6 = 10
  '--- 2.3 Tassi di rivalutazione di redditi e volumi d'affari ---
  cv_Ts1a = 11
  cv_Ts1p = 13
  cv_Ts1n = 15
  '---
  cv_Ts2a = 12
  cv_Ts2p = 14
  cv_Ts2n = 16
  '--- 2.4 Rivalutazione delle pensioni ---
  cv_Tp1Max = 17
  cv_Tp1Al = 18
  cv_Tp2Max = 19
  cv_Tp2Al = 20
  cv_Tp3Max = 21
  cv_Tp3Al = 22
  '--- 20161112LC (inizio)
  cv_Tp4Max = 23
  cv_Tp4Al = 24
  cv_Tp5Max = 25
  cv_Tp5Al = 26
  '--- 2.5 Altre stime ----
  cv_PrcPVattM = 27
  cv_PrcPVattF = 28
  cv_PrcPAattM = 29
  cv_PrcPAattF = 30
  cv_PrcPCattM = 31
  cv_PrcPCattF = 32
  cv_PrcPTattM = 33
  cv_PrcPTattF = 34
  '******************************
  'Frame REQUISITI DI ACCESSO (4)
  '******************************
  '--- 4.1 Pensione di vecchiaia ordinaria ----
  cv_Pveo30M = 35
  cv_Pveo30F = 36
  cv_Pveo65M = 37
  cv_Pveo65F = 38
  cv_Pveo98M = 39
  cv_Pveo98F = 40
  '--- 4.2 Pensione di vecchiaia anticipata ----
  cv_Pvea35M = 41
  cv_Pvea35F = 42
  cv_Pvea58M = 43
  cv_Pvea58F = 44
  '--- 4.3 Pensione di vecchiaia posticipata ----
  cv_Pvep70M = 45
  cv_Pvep70F = 46
  '--- 4.4 Pensione di inabilitā ----
  cv_Pinab2M = 47
  cv_Pinab2F = 48
  cv_Pinab10M = 49
  cv_Pinab10F = 50
  cv_Pinab35M = 51
  cv_Pinab35F = 52
  '--- 4.5 Pensione di invaliditā ----
  cv_Pinv5M = 53
  cv_Pinv5F = 54
  cv_Pinv70 = 55
  '--- 4.6 Pensione indiretta ----
  cv_Pind2M = 56
  cv_Pind2F = 57
  cv_Pind20M = 58
  cv_Pind20F = 59
  cv_Pind30M = 60
  cv_Pind30F = 61
  '--- 4.7 Restituzione dei contributi ----
  cv_RcHminM = 62
  cv_RcHminF = 63
  cv_RcXminM = 64
  cv_RcXminF = 65
  cv_RcPrcCon = 66
  'cv_RcPrcRiv = 67  '20150512LC (commentato)
  '--- 4.8 Totalizzazione ----
  cv_TzHminM = 67
  cv_TzHminF = 68
  cv_TzXminM = 69
  cv_TzXminF = 70
  cv_TzRivMin = 71
  '***********************
  'Frame CONTRIBUZIONE (5)
  '***********************
  '--- 5.1 Contribuzione ridotta ----
  cv_EtaR = 72
  cv_XMaxR = 73
  cv_AnniR = 74
  '--- 5.2 Soggettivo ----
  cv_Sogg0Min = 75
  cv_Sogg0MinR = 76
  cv_Sogg1Al = 77
  cv_Sogg1AlR = 78
  cv_Sogg1Max = 79
  cv_Sogg1MaxR = 80
  cv_Sogg2Al = 81
  cv_Sogg2AlR = 82
  cv_Sogg2Max = 83
  cv_Sogg2MaxR = 84
  cv_Sogg3Al = 85
  cv_Sogg3AlR = 86
  cv_SoggArt25 = 87
  '--- 5.3 Integrativo / FIRR ----
  cv_Int0Min = 88
  cv_Int0MinR = 89
  cv_Int1Al = 90
  cv_Int1AlR = 91
  cv_Int1Max = 92
  cv_Int1MaxR = 93
  cv_Int2Al = 94
  cv_Int2AlR = 95
  '--- 20150512LC (inizio)
  cv_Int2Max = 67   'cv_TzHminM
  cv_Int2MaxR = 68  'cv_TzHminF
  cv_Int3Al = 69    'cv_TzXminM
  cv_Int3AlR = 70   'cv_TzXminF
  '--- 20150512LC (fine)
  cv_IntArt25 = 96
  '--- 5.4 Assistenziale ----
  cv_Ass0Min = 97
  cv_Ass0MinR = 98
  cv_Ass1Al = 99
  cv_Ass1AlR = 100
  '--- 5.5 Altro ----
  cv_Mat0Min = 101
  cv_Mat0MinR = 102
  '****************************
  'Frame MISURA PRESTAZIONI (6)
  '****************************
  '--- 6.1 Scaglioni IRPEF ----
  cv_ScA = 103
  cv_AlA = 104
  cv_ScB = 105
  cv_AlB = 106
  cv_ScC = 107
  cv_AlC = 108
  cv_ScD = 109
  cv_AlD = 110
  cv_ScE = 111
  cv_AlE = 112
  cv_ScF = 113
  cv_AlF = 114
  cv_ScG = 115
  cv_AlG = 116
  '--- 6.2 Pensione ----
  cv_PMin = 117
  cv_NMR = 118
  cv_NTR = 119
  '--- 6.3 Supplementi di pensione ----
  cv_SpAnni = 120
  cv_SpEtaMax = 121
  cv_SpPrcCon = 122
  'cv_SpPrcRiv = 123  '20150512LC (commentato)
  '--- 6.4 Reversibilitā superstiti ----
  cv_Av = 123
  cv_Avo = 124
  cv_Avoo = 125
  cv_Ao = 126
  cv_Aoo = 127
  cv_Aooo = 128
  '******************************
  'Frame TASSI ED ALTRE STIME (2)
  '******************************
  '--- 2.1 Tassi di valutazione e rivalutazione ---
  cv_Prd_Tv = 129
  cv_Prd_Ti = 130
  cv_prd_Tev = 131
  cv_prd_Trn = 132
  '--- 2.2 Tassi di rivalutazione dei montanti contributivi ---
  cv_Prd_Trmc0 = 133
  cv_Prd_Trmc1 = 134
  cv_Prd_Trmc2 = 135
  cv_Prd_Trmc3 = 136
  cv_Prd_Trmc4 = 137
  cv_Prd_Trmc5 = 138
  cv_Prd_Trmc6 = 139
  '--- 2.3 Tassi di rivalutazione di redditi e volumi d'affari ---
  cv_Prd_Ts1a = 140
    cv_Prd_Ts2a = 141
  cv_Prd_Ts1p = 142
    cv_Prd_Ts2p = 143
  cv_Prd_Ts1n = 144
    cv_Prd_Ts2n = 145
  '---
  cv_max = 145
  '--- 20161112LC (fine)
End Enum

'20131122LC  'ex 20120114LC
Public Enum eom
  om_min = 0
  om_2001 = 0
  om_2002 = 1
  om_art25 = 2
  om_pens_att = 3    '4
  om_misto_obbl = 4  '3
  om_misto_fac = 5
  om_misto_int = 6
  om_misto_fig = 7
  om_last = 7  '20150521LC
  om_Sogg0 = 8       '7
  om_Int0 = 9        '8
  om_tot = 10        '9
  om_max = 10
End Enum


Public Enum epf
  pf_ps = 0 'As Double
  '--- v ---
  pf_yv = 1 'As Byte
  pf_pv = 2 'As Double
  '--- vo ---
  pf_yvo = 3 'As Byte
  pf_z1vo = 4 'As Byte
  pf_pvo = 5 'As Double
  pf_pvod = 6 'As Double
  '--- voo ---
  pf_yvoo = 7 'As Byte
  pf_z1voo = 8 'As Byte
  pf_z2voo = 9 'As Byte
  pf_pvoo = 10 'As Double
  pf_pvood = 11 'As Double
  '--- o ---
  pf_z1o = 12 'As Byte
  pf_po = 13 'As Double
  '--- oo ---
  pf_z1oo = 14 'As Byte
  pf_z2oo = 15 'As Byte
  pf_poo = 16 'As Double
  '--- ooo ---
  pf_z1ooo = 17 'As Byte
  pf_z2ooo = 18 'As Byte
  pf_z3ooo = 19 'As Byte
  pf_pooo = 20 'As Double
  '---
  pf_pfra = 21 'As Double
  pf_pgen = 22 'As Double
  pf_per = 23 'As Double
  pf_pfam = 24 'As Double
  pf_afam = 25 'As Double
  pf_max = 25
End Enum


'*******************
'Frame PARAMETRI (1)
'*******************
Public Const P_cv1  As Long = 0
Public Const P_ID   As Long = 1
'--- 1.1 Bilancio tecnico ----
Public Const P_btAnno     As Long = 2
Public Const P_btAnniEl1  As Long = 3
Public Const P_btAnniEl2  As Long = 4
Public Const P_btPatrim   As Long = 5
Public Const P_btSpAmm    As Long = 6
Public Const P_btSpAss    As Long = 7
Public Const P_btPassSIV  As Long = 8
Public Const P_btPassSIP  As Long = 9
Public Const P_btPassPFV  As Long = 10  '
Public Const P_btPassPFP  As Long = 11  '
'--- 1.2 Tipo di calcolo ----
Public Const P_TipoElab   As Long = 12
'--- 1.3 Modalitā di analisi ---
Public Const P_SQL        As Long = 13
Public Const P_FOUT       As Long = 14
'******************************
'Frame TASSI ED ALTRE STIME (2)
'******************************
Public Const P_cv2       As Long = 20
'Public bCv25 As Boolean  '20140605LC  'ex 20120213LC
'--- 2.1 Tassi di valutazione e rivalutazione ---
Public Const P_cvTv      As Long = 21  'Valutazione
Public Const P_cvTi      As Long = 22  'Rendimento dell'attivo
Public Const P_cvTev     As Long = 23 'Tasso di inflazione (elementi variabili)
Public Const P_cvTrn     As Long = 24 'Tasso di rivalutazione dei redditi iniziali dei nuovi iscritti
'--- 2.2 Tassi di rivalutazione dei montanti contributivi ---
Public Const P_cvTrmc0   As Long = 25
Public Const P_cvTrmc1   As Long = 26
Public Const P_cvTrmc2   As Long = 27
Public Const P_cvTrmc3   As Long = 28
Public Const P_cvTrmc4   As Long = 29
Public Const P_cvTrmc5   As Long = 30
Public Const P_cvTrmc6   As Long = 31
'--- 2.3 Tassi di rivalutazione di redditi e volumi d'affari ---
Public Const P_cvTs1a    As Long = 32
Public Const P_cvTs1p    As Long = 34
Public Const P_cvTs1n    As Long = 36
'---
Public Const P_cvTs2a    As Long = 33  '
Public Const P_cvTs2p    As Long = 35  '
Public Const P_cvTs2n    As Long = 37  '
'--- 2.4 Rivalutazione delle pensioni ---
Public Const P_cvTp1Max  As Long = 38
Public Const P_cvTp1Al   As Long = 39
Public Const P_cvTp2Max  As Long = 40
Public Const P_cvTp2Al   As Long = 41
Public Const P_cvTp3Max  As Long = 42
Public Const P_cvTp3Al   As Long = 43
'--- 20161112LC (inizio)
Public Const P_cvTp4Max  As Long = 138
Public Const P_cvTp4Al   As Long = 139
Public Const P_cvTp5Max  As Long = 140
Public Const P_cvTp5Al   As Long = 141
'--- 20161112LC (fine)
'--- 2.5 Altre stime ----
Public Const P_cvPrcPVattM  As Long = 44
Public Const P_cvPrcPVattF  As Long = 45
Public Const P_cvPrcPAattM  As Long = 46
Public Const P_cvPrcPAattF  As Long = 47
Public Const P_cvPrcPCattM  As Long = 48
Public Const P_cvPrcPCattF  As Long = 49
Public Const P_cvPrcPTattM  As Long = 50
Public Const P_cvPrcPTattF  As Long = 51
'***********************
'Frame BASI_TECNICHE (3)
'***********************
Public Const P_cv3  As Long = 70
'--- 3.1 Probabilitā di morte ----
Public Const P_LAD  As Long = 71
Public Const P_LED  As Long = 72
Public Const P_LPD  As Long = 73
Public Const P_LSD  As Long = 74
Public Const P_LBD  As Long = 75
Public Const P_LID  As Long = 76
'--- 3.2 Altre probabilitā ----
Public Const P_LAV  As Long = 277  '20150505LC
Public Const P_LAE  As Long = 77
Public Const P_LAB  As Long = 78
Public Const P_LAI  As Long = 79
Public Const P_LAW  As Long = 80
'--- 3.3 Linee reddituali ----
Public Const P_LRA  As Long = 81
Public Const P_LRP  As Long = 82
Public Const P_LRN  As Long = 83
'--- 3.4 Coefficienti di conversione del montante in rendita ----
Public Const P_CCR  As Long = 84
'--- 3.5 Superstiti ----
Public Const P_PF  As Long = 85
Public Const P_ZMAX  As Long = 86
'******************************
'Frame REQUISITI DI ACCESSO (4)
'******************************
Public Const P_cv4  As Long = 100
'--- 4.1 Pensione di vecchiaia ordinaria ----
Public Const P_cvPveo30M  As Long = 101
Public Const P_cvPveo30F  As Long = 102
Public Const P_cvPveo65M  As Long = 103
Public Const P_cvPveo65F  As Long = 104
Public Const P_cvPveo98M  As Long = 105
Public Const P_cvPveo98F  As Long = 106
'--- 4.2 Pensione di vecchiaia anticipata ----
Public Const P_cvPvea35M  As Long = 107
Public Const P_cvPvea35F  As Long = 108
Public Const P_cvPvea58M  As Long = 109
Public Const P_cvPvea58F  As Long = 110
'--- 4.3 Pensione di vecchiaia poticipata ----
Public Const P_cvPvep70M  As Long = 111
Public Const P_cvPvep70F  As Long = 112
'--- 4.4 Pensione di inabilitā ----
Public Const P_cvPinab2M  As Long = 113
Public Const P_cvPinab2F  As Long = 114
Public Const P_cvPinab10M  As Long = 115
Public Const P_cvPinab10F  As Long = 116
Public Const P_cvPinab35M  As Long = 117
Public Const P_cvPinab35F  As Long = 118
'--- 4.5 Pensione di invaliditā ----
Public Const P_cvPinv5M  As Long = 119
Public Const P_cvPinv5F  As Long = 120
Public Const P_cvPinv70  As Long = 121
'--- 4.6 Pensione indiretta ----
Public Const P_cvPind2M  As Long = 122
Public Const P_cvPind2F  As Long = 123
Public Const P_cvPind20M  As Long = 124
Public Const P_cvPind20F  As Long = 125
Public Const P_cvPind30M  As Long = 126
Public Const P_cvPind30F  As Long = 127
'--- 4.7 Restituzione dei contributi ----
Public Const P_cvRcHminM  As Long = 128
Public Const P_cvRcHminF  As Long = 129
Public Const P_cvRcXminM  As Long = 130
Public Const P_cvRcXminF  As Long = 131
Public Const P_cvRcPrcCon  As Long = 132
'Public Const P_cvRcPrcRiv  As Long = 138  '20150512LC (commentato)
'--- 4.8 Totalizzazione ----
Public Const P_cvTzHminM  As Long = 133
Public Const P_cvTzHminF  As Long = 134
Public Const P_cvTzXminM  As Long = 135
Public Const P_cvTzXminF  As Long = 136
Public Const P_cvTzRivMin  As Long = 137
'***********************
'Frame CONTRIBUZIONE (5)
'***********************
Public Const P_cv5  As Long = 150
'--- 5.1 Contribuzione ridotta ----
Public Const P_cvEtaR  As Long = 151
Public Const P_cvXmaxR  As Long = 152
Public Const P_cvAnniR  As Long = 153
'--- 5.2 Soggettivo ----
Public Const P_cvSogg0Min  As Long = 154
Public Const P_cvSogg0MinR  As Long = 155
Public Const P_cvSogg1Al  As Long = 156
Public Const P_cvSogg1AlR  As Long = 157
Public Const P_cvSogg1Max  As Long = 158
Public Const P_cvSogg1MaxR  As Long = 159
Public Const P_cvSogg2Al  As Long = 160
Public Const P_cvSogg2AlR  As Long = 161
Public Const P_cvSogg2Max  As Long = 162
Public Const P_cvSogg2MaxR  As Long = 163
Public Const P_cvSogg3Al  As Long = 164
Public Const P_cvSogg3AlR  As Long = 165
Public Const P_cvSoggArt25  As Long = 166
'--- 5.3 Integrativo / FIRR ----
Public Const P_cvInt0Min  As Long = 167
Public Const P_cvInt0MinR  As Long = 168
Public Const P_cvInt1Al  As Long = 169
Public Const P_cvInt1AlR  As Long = 170
Public Const P_cvInt1Max  As Long = 171
Public Const P_cvInt1MaxR  As Long = 172
Public Const P_cvInt2Al  As Long = 173
Public Const P_cvInt2AlR  As Long = 174
'--- 20150512LC (inizio)
Public Const P_cvInt2Max  As Long = 175
Public Const P_cvInt2MaxR  As Long = 176
Public Const P_cvInt3Al  As Long = 177
Public Const P_cvInt3AlR  As Long = 178
Public Const P_cvIntArt25  As Long = 179
'--- 5.4 Assistenziale ----
Public Const P_cvAss0Min  As Long = 180
Public Const P_cvAss0MinR  As Long = 181
Public Const P_cvAss1Al  As Long = 182
Public Const P_cvass1AlR  As Long = 183
'--- 5.5 Altro ----
Public Const P_cvMat0Min  As Long = 190
Public Const P_cvMat0MinR  As Long = 191
'--- 20150512LC (fine)
'****************************
'Frame MISURA PRESTAZIONI (6)
'****************************
Public Const P_cv6  As Long = 200
'--- 6.1 Scaglioni IRPEF ----
Public Const P_cvScA  As Long = 201
Public Const P_cvAlA  As Long = 202
Public Const P_cvScB  As Long = 203
Public Const P_cvAlB  As Long = 204
Public Const P_cvScC  As Long = 205
Public Const P_cvAlC  As Long = 206
Public Const P_cvScD  As Long = 207
Public Const P_cvAlD  As Long = 208
Public Const P_cvScE  As Long = 209
Public Const P_cvAlE  As Long = 210
Public Const P_cvScF  As Long = 211
Public Const P_cvAlF  As Long = 212
Public Const P_cvScG  As Long = 213
Public Const P_cvAlG  As Long = 214
'--- 6.2 Pensione ----
Public Const P_cvPMin  As Long = 215
Public Const P_cvNMR  As Long = 216
Public Const P_cvNTR  As Long = 217
'--- 6.3 Supplementi di pensione ----
Public Const P_cvSpAnni  As Long = 218
Public Const P_cvSpEtaMax  As Long = 219
Public Const P_cvSpPrcCon  As Long = 220
'Public Const P_cvSpPrcRiv  As Long = 221
'--- 6.4 Reversibilitā superstiti ----
Public Const P_cvAv  As Long = 222
Public Const P_cvAvo  As Long = 223
Public Const P_cvAvoo  As Long = 224
Public Const P_cvAo  As Long = 225
Public Const P_cvAoo  As Long = 226
Public Const P_cvAooo  As Long = 227
'Public Const P_cvAgen  as long = 228
'Public Const P_cvAfra  as long = 229
'************************
'Frame NUOVI ISCRITTI (7)
'************************
Public Const P_cv7  As Long = 250
Public Const P_niTipo  As Long = 251
Public Const P_niP  As Long = 252
Public Const P_niStat  As Long = 253
Public Const P_neStat  As Long = 254  '20150527LC
Public Const P_nvStat  As Long = 255  '20150527LC
'***************************
'3a colonna della matrice TB
'***************************
'***********
'Popolazione
'***********
'Public Const TB_FIRST  As Long = 0 '20121028LC (inizio)
Public Const TB_ZTOT      As Long = 0   ' 0       (20)
'--- Attivi ed ex-attivi ---
Public Const TB_ZAA       As Long = 1   ' 1  'Na   (2)  ZAA
Public Const TB_ZVA       As Long = 127 ' 2  'Nva  ()   ZVA  '20150504LC
Public Const TB_ZEA       As Long = 2   ' 2  'Nea  (3)  ZEA
'--- Pensionati diretti attivi
Public Const TB_ZPC_VO    As Long = 3   '76  '    (4a)  ZPDCVO +ZPDCVA +ZPDCVP +ZPDCA +ZPDCS +ZPDCT
Public Const TB_ZPC_VA    As Long = 4   '95       (4b)
Public Const TB_ZPC_VP    As Long = 5   '99       (4c)
Public Const TB_ZPC_A     As Long = 6   ' 3       (4d)
Public Const TB_ZPC_S     As Long = 7   '77       (4e)
Public Const TB_ZPC_T     As Long = 8   '78       (4f)
'--- Pensionati diretti non attivi
Public Const TB_ZPN_VO    As Long = 9   ' 6        (5)(14a) ZPDVO
Public Const TB_ZPN_VA    As Long = 10  '92        (6)(14b) ZPDVA
Public Const TB_ZPN_VP    As Long = 11  '96        (7)(14c) ZPDVP
Public Const TB_ZPN_A     As Long = 12  ' 7        (8)(14d) ZPDA
Public Const TB_ZPN_S     As Long = 13  '15        (9)(14e) ZPDS
Public Const TB_ZPN_T     As Long = 14  '14       (10)(14f) ZPDT
Public Const TB_ZPN_B     As Long = 15  ' 4       (11)(14g) ZPB
Public Const TB_ZPN_I     As Long = 16  ' 5       (12)(14h) ZPI
'--- Pensionati uindiretti e superstiti
Public Const TB_ZPS_1     As Long = 17  ' 8       alternativa a (13)(14i)
Public Const TB_ZPS_2     As Long = 18  '59       (13)(14i) ZPS2
Public Const TB_ZPS_V     As Long = 19  '53
Public Const TB_ZPS_VO    As Long = 20  '54
Public Const TB_ZPS_VOO   As Long = 21  '55
Public Const TB_ZPS_O     As Long = 22  '56
Public Const TB_ZPS_OO    As Long = 23  '57
Public Const TB_ZPS_OOO   As Long = 24  '58
'--- Altro
'(14) ZPDVO +ZPDVA +ZPDVP +ZPDA +ZPDS +ZPDT +ZPB +ZPI + ZPS2
'(15) ZAA +ZEA +ZPDCVO +ZPDCVA +ZPDCVP +ZPDCA +ZPDCS +ZPDCT +(14)  <--- non 11
Public Const TB_ZW_1      As Long = 25  '11       (16) ZW1
Public Const TB_ZW_2      As Long = 26  '12       (17) ZW2
Public Const TB_ZW_3      As Long = 27  '13       (18) ZW3
Public Const TB_ZD_S      As Long = 28  '10       (19) ZSD
Public Const TB_ZD        As Long = 29  ' 9
'--- 20140414LC (inizio)
'*************************
'Redditi e volumi d'affari
'*************************
Public Const TB_AA_IRPEF  As Long = 30  '20
Public Const TB_PA_IRPEF  As Long = 31
Public Const TB_AA_IRPEF2 As Long = 32  '75
Public Const TB_PA_IRPEF2 As Long = 33
Public Const TB_AA_IVA    As Long = 34  '26
Public Const TB_PA_IVA    As Long = 35
'*****************
'Contributi totali
'*****************
Public Const TB_AA_ZETOT  As Long = 36  '16
Public Const TB_PA_ZETOT  As Long = 37  '16
Public Const TB_AA_ETOT   As Long = 38  '18
Public Const TB_PA_ETOT   As Long = 39  '18
'*********************
'Contributi soggettivi
'*********************
Public Const TB_AA_ECSR   As Long = 40  '21
Public Const TB_PA_ECSR   As Long = 41  '21
Public Const TB_AA_ECS01  As Long = 42  '22
Public Const TB_PA_ECS01  As Long = 43  '22
Public Const TB_AA_ECS1   As Long = 44  '23
Public Const TB_PA_ECS1   As Long = 45  '23
Public Const TB_AA_ECS2   As Long = 46  '79
Public Const TB_PA_ECS2   As Long = 47  '79
Public Const TB_AA_ECS3   As Long = 48  '24
Public Const TB_PA_ECS3   As Long = 49  '24
'********************
'Contributi Volontari
'********************
Public Const TB_AA_ECS02  As Long = 50  '80  'Facoltativi
Public Const TB_PA_ECS02  As Long = 51  '80  'Facoltativi
'**********************
'Contributi Integrativi
'**********************
Public Const TB_AA_ECIR   As Long = 52  '27
Public Const TB_PA_ECIR   As Long = 53  '27
Public Const TB_AA_ECI0   As Long = 54  '28
Public Const TB_PA_ECI0   As Long = 55  '28
Public Const TB_AA_ECI1   As Long = 56  '29
Public Const TB_PA_ECI1   As Long = 57  '29
Public Const TB_AA_ECI2   As Long = 58  '82
Public Const TB_PA_ECI2   As Long = 59  '82
Public Const TB_AA_ECI3   As Long = 128  '20150512LC
Public Const TB_PA_ECI3   As Long = 129  '20150512LC
'************************
'Contributi Assistenziali
'************************
Public Const TB_AA_ECAR   As Long = 60  '25
Public Const TB_PA_ECAR   As Long = 61  '25
Public Const TB_AA_ECA0   As Long = 62  '30
Public Const TB_PA_ECA0   As Long = 63  '30
Public Const TB_AA_ECA1   As Long = 64  '35
Public Const TB_PA_ECA1   As Long = 65  '35
'***********************
'Contributi di Maternitā
'***********************
Public Const TB_AA_EMAT   As Long = 66  '36
Public Const TB_PA_EMAT   As Long = 67  '36
'*********************
'Contributi Figurativi
'*********************
Public Const TB_AA_EFS0   As Long = 72  '86  'Figurativo soggettivo acconto
Public Const TB_PA_EFS0   As Long = 73  '86  'Figurativo soggettivo acconto
Public Const TB_AA_EFS1   As Long = 74  '87  'Figurativo soggettivo saldo
Public Const TB_PA_EFS1   As Long = 75  '87  'Figurativo soggettivo saldo
Public Const TB_AA_EFI0   As Long = 76  '88  'Figurativo integrativo acconto
Public Const TB_PA_EFI0   As Long = 77  '88  'Figurativo integrativo acconto
Public Const TB_AA_EFI1   As Long = 78  '89  'Figurativo integrativo saldo
Public Const TB_PA_EFI1   As Long = 79  '89  'Figurativo integrativo saldo
'*************************************
'Contributi Integrativi non retrocessi
'*************************************
Public Const TB_AA_EAI0   As Long = 68  '84  'Integrativo non retrocesso acconto
Public Const TB_PA_EAI0   As Long = 69  '84  'Integrativo non retrocesso acconto
Public Const TB_AA_EAI1   As Long = 70  '85  'Integrativo non retrocesso saldo
Public Const TB_PA_EAI1   As Long = 71  '85  'Integrativo non retrocesso saldo
'*************************
'Contributi di Solidarietā
'*************************
Public Const TB_AA_ECSP   As Long = 80  '81  'Solidarietā
Public Const TB_PA_ECSP   As Long = 81  '81  'Solidarietā
'*************************
'Ricongiunzione e riscatti
'*************************
Public Const TB_ECRR      As Long = 82  '74
'********
'Montanti
'********
Public Const TB_OMRC      As Long = 83  '37
Public Const TB_OMRC_0    As Long = 130
Public Const TB_OMRC_1    As Long = 131
Public Const TB_OMRC_2    As Long = 132
Public Const TB_OMRC_3    As Long = 133
Public Const TB_OMRC_4    As Long = 134
Public Const TB_OMRC_5    As Long = 135
Public Const TB_OMRC_6    As Long = 136
'******
'Uscite
'******
'--- Spesa pensionistica
'--- Pensionati diretti attivi
Public Const TB_SPC_VO    As Long = 84  '63
Public Const TB_SPC_VA    As Long = 85
Public Const TB_SPC_VP    As Long = 86
Public Const TB_SPC_A     As Long = 87
Public Const TB_SPC_S     As Long = 88
Public Const TB_SPC_T     As Long = 89
'--- Pensionati diretti non attivi
Public Const TB_SPN_VO    As Long = 90  '64
Public Const TB_SPN_VA    As Long = 91  '94
Public Const TB_SPN_VP    As Long = 92  '98
Public Const TB_SPN_A     As Long = 93  '65
Public Const TB_SPN_S     As Long = 94  '66
Public Const TB_SPN_T     As Long = 95  '67
Public Const TB_SPN_B     As Long = 96  '61
Public Const TB_SPN_I     As Long = 97  '62
'--- Pensionati indiretti e superstiti
Public Const TB_SPS       As Long = 98  '68
'--- Altro
Public Const TB_SPW_1     As Long = 99  '70
Public Const TB_SPW_2     As Long = 100 '71
Public Const TB_SPW_3     As Long = 101 '72
Public Const TB_SP_TOT    As Long = 102 '69  '(ZPDCTOT) +(ZPDVOTOT+ZPDVATOT+ZPDVPTOT+ZPDATOT+ZPDTTOT+ZPDSTOT+ZPBTOT+ZPITOT) +ZPSTOT
Public Const TB_SU_TOT    As Long = 103 '73  'ZPTOT +(ZISOS1+ZISOS2+ZISOS3)
Public Const TB_PASS1     As Long = 104 '90  'Prestazione assistenziale da integrazione al minimo
Public Const TB_PASS2     As Long = 105 '91  'Prestazione assistenziale da contributi figurativi
'--- Medie pensioni e restituzione
'--- Pensionati diretti attivi
Public Const TB_MPC_VO    As Long = 106 '40
Public Const TB_MPC_VA    As Long = 107
Public Const TB_MPC_VP    As Long = 108
Public Const TB_MPC_A     As Long = 109
Public Const TB_MPC_S     As Long = 110
Public Const TB_MPC_T     As Long = 111
Public Const TB_MPC_U     As Long = 111  '20181108LC
'--- Pensionati diretti non attivi
Public Const TB_MPN_VO    As Long = 112 '41
Public Const TB_MPN_VA    As Long = 113 '93
Public Const TB_MPN_VP    As Long = 114 '97
Public Const TB_MPN_A     As Long = 115 '42
Public Const TB_MPN_S     As Long = 116 '43
Public Const TB_MPN_T     As Long = 117 '44
Public Const TB_MPN_B     As Long = 118 '38
Public Const TB_MPN_I     As Long = 119 '39
'--- Pensionati indiretti e superstiti
Public Const TB_MPS       As Long = 120 '45
'--- Altro
Public Const TB_MPW_1     As Long = 121 '47
Public Const TB_MPW_2     As Long = 122 '48
Public Const TB_MPW_3     As Long = 123 '49
Public Const TB_MP_TOT    As Long = 124 '46
Public Const TB_MU_TOT    As Long = 125 '50
'*****
'Saldi
'*****
Public Const TB_STOT      As Long = 126 '51
'---
Public Const TB_FIRR_USC  As Long = 137  '20160612LC
Public Const TB_FIRR_SSUP As Long = 138  '20160613LC
Public Const TB_LAST      As Long = 138  '20160613LC
'--- 20140414LC (fine)
'Public Const TB_PFAM      As Long = 52  '52
'Public Const TB_ZFAM      As Long = 25  '60

'20080709LC
'20120203LC
Public Sub ComboBox_Set(cb As ComboBox, b As Boolean)
  With cb
    If b = True And .ListIndex < 0 Then
      .ListIndex = 0
    End If
    .Enabled = b
    .BackColor = IIf(b, &H80000005, &H8000000F)
  End With
End Sub


Public Sub ParametriNI(i%, frm As Object)
'20130307LC
  '20120319LC (inizio)
  frm.optNI_0.Value = (i = 0)
  frm.optNI_1.Value = (i = 1)
  frm.optNI_2.Value = (i = 2)
  frm.optNI_3.Value = (i = 3)
  frm.Label1(P_niP).Enabled = (i >= 2)          '20140422LC
  Call ComboBox_Set(frm.cbxNip, i >= 2)  '20140422LC
  Call ComboBox_Set(frm.cbxNiStat, i >= 2)
  '20120319LC (fine)
  '--- 20150527LC (inizio)
  Call ComboBox_Set(frm.cbxNeStat, True)
  Call ComboBox_Set(frm.cbxNvStat, True)
  '--- 20150527LC (fine)
End Sub


Public Sub ParametriTipoElab(Index%, frm As Object)  '20130307LC
  Select Case Index
  Case 0, 1
    frm.optTipoElab(Index).Value = True
    frm.Text1(P_TipoElab) = ""
    frm.Text1(P_TipoElab).Enabled = False
    frm.Text1(P_TipoElab).BackColor = &H8000000F
  Case 2
    frm.optTipoElab(2).Value = True
    frm.Text1(P_TipoElab) = Year(frm.Text1(P_btAnno)) + 1
    frm.Text1(P_TipoElab).Enabled = True
    frm.Text1(P_TipoElab).BackColor = &H80000005
  Case Else
    frm.optTipoElab(2).Value = True
    frm.Text1(P_TipoElab) = Index
    frm.Text1(P_TipoElab).Enabled = True
    frm.Text1(P_TipoElab).BackColor = &H80000005
  End Select
End Sub


'20120212LC
Public Sub TextBox_Set(tb As TextBox, b As Boolean)
  With tb
    .BackColor = IIf(b, &H80000005, &HFFFFC0)    '&H8000000F
    '.Enabled = b
    .ForeColor = IIf(b, &H80000008, &HFF0000)
    .Locked = Not b
  End With
End Sub
