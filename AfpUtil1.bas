Attribute VB_Name = "modAfpUtil1"
'****************************************************************************************************
'SCOPO
'  Routine di uso generale, in particolare per sostituire le funzioni di Excel non esportabili
'STORIA DELLE VERSIONI
'  01.00 Prima implementazione (20140115LC)
'****************************************************************************************************
Option Explicit


'*********
'Prototipi
'*********
'--- 1) Arrotonda un numero ad un valore di peso specificato
'Public Function ARR(x As Double, y As Double, z As Double)
'--- 2) Arrotonda un numero per difetto
'Public Function ARR0(y As Double, z As Double) As Double
'--- 3) Arrotonda un numero al valore pi� vicino
'Public Function ARR5(y As Double, z As Double) As Double
'--- 4) Arrotonda un numero per eccesso
'Public Function ARR9(y As Double, z As Double) As Double
'--- 5) Arrotonda un numero al centesimo pi� vicino
'Public Function ARRV(y As Double, z As Double) As Double
'--- 6) Calcola i giorni commerciali trascorsi dal 31/12/1899 fino data specificata
'Public Function Data360(d As Date) As Long
'--- 7) Calcola i giorni commerciali trascorsi tra due date
'Public Function Giorni360(d1 As Date, d2 As Date) As Long


'***************
'Implementazione
'***************
Public Const AA0 As Double = 0.000000002  'Numero molto piccolo per arrotondare all'intero inferiore (non scendere sotto 2E-9)
Public Const AA5 As Double = 0.5 + AA0    'Numero per arrotondare valori all'intero pi� vicino
Public Const AA9 As Double = 1 - AA0      'Numero per arrontondare all'intero superiore


Public Function ARR(x As Double, y As Double, z As Double) As Double
'****************************************************************************************************
'SCOPO
'  Arrotonda un numero ad un valore di peso specificato
'ARGOMENTI DI INPUT
'  x   numero da sommare prima di arrotondare
'  y   numero da arrotondare
'  z   numero da usare come divisore prima di arrotondare (peso)
'ARGOMENTI DI OUTPUT O INPUT/OUTPUT
'  Nessuno
'VALORE RESTITUITO
'  Numero arrotondato
'NOTE
'  1) Questa funzione sostituisce la ROUND (ARROTONDA) di VBA, non sempre precise
'La funzione INT non funziona con x=AA0, y=309101.91, z=100
'INT(309101.91*100+AA0)=30910190!!!
'IMPLEMENTAZIONI
'  20140115LC prima implementazione
'****************************************************************************************************
  Dim c As Double
  
  If z <= 0 Or z >= 1E+20 Then  '--- Peso non previsto, non si arrotonda
    ARR = y
  ElseIf y >= 0 Then            '--- Arrotondamento per numeri positivi
    ARR = Int(x + y * z) / z
  Else                          '--- Arrotondamento per numeri negativi
    ARR = -Int(x - y * z) / z
  End If
End Function


Public Function ARR0(y As Double, z As Double) As Double
'****************************************************************************************************
'SCOPO
'  Arrotonda un numero per difetto
'ARGOMENTI DI INPUT
'  y   numero da arrotondare
'  z   numero da usare come divisore prima di arrotondare (peso)
'ARGOMENTI DI OUTPUT O INPUT/OUTPUT
'  Nessuno
'VALORE RESTITUITO
'  Numero arrotondato per difetto
'NOTE
'IMPLEMENTAZIONI
'  20140115LC prima implementazione
'****************************************************************************************************
  ARR0 = ARR(AA0, y, z)
End Function


Public Function ARR5(y As Double, z As Double) As Double
'****************************************************************************************************
'SCOPO
'  Arrotonda un numero al valore pi� vicino
'ARGOMENTI DI INPUT
'  y   numero da arrotondare
'  z   numero da usare come divisore prima di arrotondare (peso)
'ARGOMENTI DI OUTPUT O INPUT/OUTPUT
'  Nessuno
'VALORE RESTITUITO
'  Numero arrotondato al valore pi� vicino
'IMPLEMENTAZIONI
'  20140115LC prima implementazione
'****************************************************************************************************
  ARR5 = ARR(AA5, y, z)
End Function


Public Function ARR9(y As Double, z As Double) As Double
'****************************************************************************************************
'SCOPO
'  Arrotonda un numero per eccesso
'ARGOMENTI DI INPUT
'  y   numero da arrotondare
'  z   numero da usare come divisore prima di arrotondare (peso)
'ARGOMENTI DI OUTPUT O INPUT/OUTPUT
'  Nessuno
'VALORE RESTITUITO
'  Numero arrotondato per eccesso
'NOTE
'IMPLEMENTAZIONI
'  20140115LC prima implementazione
'****************************************************************************************************
  ARR9 = ARR(AA9, y, z)
End Function


Public Function ARRV(y As Double) As Double
'****************************************************************************************************
'SCOPO
'  Arrotonda un numero al centesimo pi� vicino
'ARGOMENTI DI INPUT
'  y   numero da arrotondare
'ARGOMENTI DI OUTPUT O INPUT/OUTPUT
'  Nessuno
'VALORE RESTITUITO
'  Numero arrotondato al centesimo pi� vicino
'NOTE
'  1) Pi� semplice da usare, quando si devono arrotondare valori in �
'IMPLEMENTAZIONI
'  20140115LC prima implementazione
'****************************************************************************************************
  ARRV = ARR(AA5, y, 100#)
End Function


Public Function Data360(d As Date) As Long
'****************************************************************************************************
'SCOPO
'  Calcola i giorni commerciali trascorsi dal 31/12/1899 fino data specificata
'ARGOMENTI DI INPUT
'  d   data di ingresso
'ARGOMENTI DI OUTPUT O INPUT/OUTPUT
'  Nessuno
'VALORE RESTITUITO
'  Giorno commerciali (anno da 360 gg) trascorsi dal 31/12/1999
'NOTE
'  1) Questa funzione pu� sostituire la GIORNO360(data1;data2;VERO) di Excel con
'     Data360(data2) - Data360(data1)
'  2) In Excel va usato VERO come terzo argomento, per lo standard europeo
'     Con FALSE (standard americano) si cambia mese passando dal'1 al 31 (sic!)
'IMPLEMENTAZIONI
'  20140115LC prima implementazione
'****************************************************************************************************
  Dim g As Integer  'Giorno del mese
  
  g = Day(d)
  If g > 30 Then
    g = 30
  End If
  '2000 da valori negativi che non vanno bene quando si usa MOD (c0<0): .sup(j).c0 = (Data360(.sup(j).DNasc) Mod 360) / 360#
  'Data360 = (Year(d) - 2000) * 360& + (Month(d) - 1) * 30 + g
  Data360 = (Year(d) - 0) * 360& + (Month(d) - 1) * 30 + g
End Function


Public Function ff0(rs As Variant) As Variant
  If VarType(rs) = vbNull Then
    ff0 = 0
  Else
    ff0 = rs
  End If
End Function


Public Function fs$(szi$, n&, iop%)
  Dim sz$
  
  sz = szi
  Select Case iop
  Case 0
    If Len(sz) < n Then sz = sz & Space$(n - Len(sz))
  Case 1
    If Len(sz) < n Then sz = Space$(n - Len(sz)) & sz
  Case 2
    If Len(sz) < n Then sz = Space$(Int(n - Len(sz)) / 2) & sz & Space$(Int(n + 1 - Len(sz)) / 2)
  End Select
  fs = sz
End Function


Public Function Giorni360(d1 As Date, d2 As Date) As Long
'****************************************************************************************************
'SCOPO
'  Calcola i giorni commerciali trascorsi tra due date
'ARGOMENTI DI INPUT
'  d1   data iniziale
'  d2   data finale
'ARGOMENTI DI OUTPUT O INPUT/OUTPUT
'  Nessuno
'VALORE RESTITUITO
'  Giorno commerciali (anno da 360 gg) trascorsi dal 31/12/1899
'FUNZIONI o SUB USATE
'  Data360
'NOTE
'  1) Questa funzione sostituisce direttamente la GIORNO360(data1;data2;VERO) di Excel con
'     GIORNI360(data1,data2)
'  2) In Excel va usato VERO come terzo argomento, per lo standard europeo
'     Con FALSE (standard americano) si cambia mese passando dal'1 al 31 (sic!)
'IMPLEMENTAZIONI
'  20140115LC prima implementazione
'****************************************************************************************************
  Giorni360 = Data360(d2) - Data360(d1)
End Function


Public Function Trimestre(d As Date) As Byte
  Trimestre = Int((Month(d) + 2) / 3)
End Function


Public Function anTr(d As Date) As Long
'20150321LC
  anTr = Year(d) * 10 + Trimestre(d)
End Function



