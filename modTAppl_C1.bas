'20181108LC  adattato per Enasarco
Option Explicit


  Public Enum EMef
    'Mef_PosIn = 0
    Mef_PosFin = 0
    Mef_PosRedd = 1
    Mef_PosPens = 2
    Mef_PosPens12 = 3
    Mef_PosAnz = 4
    Mef_PosContr = 5
    Mef_NumAgg = 6
    '---
    Mef_z_z = 0
    Mef_a_a = 1
    Mef_s_s = 2
    Mef_pa_pa = 3
    Mef_pn_pn = 4
    Mef_pb_pb = 5
    Mef_pi_pi = 6
    Mef_ps_ps = 7
    Mef_n_a = 8
    Mef_a_z = 9
    Mef_a_s = 10
    Mef_a_pa = 11
    Mef_a_pn = 12
    Mef_a_pb = 13
    Mef_a_pi = 14
    Mef_a_ps = 15
    Mef_s_z = 16
    Mef_s_a = 17
    Mef_s_pa = 18
    Mef_s_pn = 19
    Mef_s_pb = 20
    Mef_s_pi = 21
    Mef_s_ps = 22
    Mef_pa_pn = 23
    Mef_pa_ps = 24
    Mef_pa_z = 25
    Mef_pb_ps = 26
    Mef_pb_z = 27
    Mef_pi_ps = 28
    Mef_pi_z = 29
    Mef_pn_ps = 30
    Mef_pn_z = 31
    Mef_ps_z = 32
    Mef_Max = 32
  End Enum
  
  
  Private Sub jSP(ByRef iSP&, ByRef jSP0&, ByRef jSP1&)
    Select Case iSP
    Case Mef_z_z  '0
      jSP0 = Mef_z_z
      jSP1 = Mef_z_z
    Case Mef_a_a  '1
      jSP0 = Mef_a_a
      jSP1 = Mef_a_a
    Case Mef_n_a   '1
      jSP0 = Mef_n_a
      jSP1 = Mef_a_a
    '---
    Case Mef_a_z  '2
      jSP0 = Mef_a_a
      jSP1 = Mef_z_z
    Case Mef_a_s  '4
      jSP0 = Mef_a_a
      jSP1 = Mef_s_s
    Case Mef_a_pa  '5
      jSP0 = Mef_a_a
      jSP1 = Mef_pa_pa
    Case Mef_a_pn  '6
      jSP0 = Mef_a_a
      jSP1 = Mef_pn_pn
    Case Mef_a_pb  '7
      jSP0 = Mef_a_a
      jSP1 = Mef_pb_pb
    Case Mef_a_pi  '8
      jSP0 = Mef_a_a
      jSP1 = Mef_pi_pi
    Case Mef_a_ps  '9
      jSP0 = Mef_a_a
      jSP1 = Mef_ps_ps
    '---
    Case Mef_s_z  '10
      jSP0 = Mef_s_s
      jSP1 = Mef_z_z
    Case Mef_s_a  '11
      jSP0 = Mef_s_s
      jSP1 = Mef_a_a
    Case Mef_s_s  '12
      jSP0 = Mef_s_s
      jSP1 = Mef_s_s
    Case Mef_s_pa  '13
      jSP0 = Mef_s_s
      jSP1 = Mef_pa_pa
    Case Mef_s_pn  '14
      jSP0 = Mef_s_s
      jSP1 = Mef_pn_pn
    Case Mef_s_pb  '15
      jSP0 = Mef_s_s
      jSP1 = Mef_pb_pb
    Case Mef_s_pi  '16
      jSP0 = Mef_s_s
      jSP1 = Mef_pi_pi
    Case Mef_s_ps  '17
      jSP0 = Mef_s_s
      jSP1 = Mef_ps_ps
    '---
    Case Mef_pa_pa  '18
      jSP0 = Mef_pa_pa
      jSP1 = Mef_pa_pa
    Case Mef_pa_pn '19
      jSP0 = Mef_pa_pa
      jSP1 = Mef_pn_pn
    Case Mef_pa_ps  '20
      jSP0 = Mef_pa_pa
      jSP1 = Mef_ps_ps
    Case Mef_pa_z  '21
      jSP0 = Mef_pa_pa
      jSP1 = Mef_z_z
    '---
    Case Mef_pn_pn  '22
      jSP0 = Mef_pn_pn
      jSP1 = Mef_pn_pn
    Case Mef_pn_ps  '23
      jSP0 = Mef_pn_pn
      jSP1 = Mef_ps_ps
    Case Mef_pn_z  '24
      jSP0 = Mef_pn_pn
      jSP1 = Mef_z_z
    '---
    Case Mef_pb_pb  '25
      jSP0 = Mef_pb_pb
      jSP1 = Mef_pb_pb
    Case Mef_pb_ps  '26
      jSP0 = Mef_pb_pb
      jSP1 = Mef_ps_ps
    Case Mef_pb_z  '27
      jSP0 = Mef_pb_pb
      jSP1 = Mef_z_z
    '---
    Case Mef_pi_pi  '28
      jSP0 = Mef_pi_pi
      jSP1 = Mef_pi_pi
    Case Mef_pi_ps  '29
      jSP0 = Mef_pi_pi
      jSP1 = Mef_ps_ps
    Case Mef_pi_z  '30
      jSP0 = Mef_pi_pi
      jSP1 = Mef_z_z
    '---
    Case Mef_ps_ps  '31
      jSP0 = Mef_ps_ps
      jSP1 = Mef_ps_ps
    Case Mef_ps_z  '32
      jSP0 = Mef_ps_ps
      jSP1 = Mef_z_z
    '---
    Case Else
      Call MsgBox("TODO - jSP")
      Stop
    End Select
  End Sub
  
  
  Private Function kSP&(ByRef jSP&)
    Select Case jSP
    Case Mef_z_z, Mef_n_a '0,7
      kSP = 0
    Case Mef_a_a  '1
      kSP = 10
    Case Mef_pa_pa  '2
      kSP = 21
    Case Mef_pn_pn  '3
      kSP = 31
    Case Mef_pb_pb  '4
      kSP = 32
    Case Mef_pi_pi  '5
      kSP = 33
    Case Mef_ps_ps  '6
      kSP = 34
    '---
    Case Mef_s_s    '
      kSP = 40
    '---
    Case Else
      kSP = -1
    End Select
  End Function
  
  
  Public Sub MEF1(ByRef tbMEF#(), ByRef iSex1&, ByRef ix1&, ByRef iSP&, ByRef icol&, ByRef t0&, ByRef y#, _
      ByRef DBXMEF As Integer)
    'TIscr2_MovPop_4c:         CONSOLIDAMENTO MEF 2B,2C
    'TIscr2_MovPop_9:          CONSOLIDAMENTO MEF 3B
    'TIscr2_MovPop_Contributi: CONSOLIDAMENTO MEF 4B,4C,4D
    'TIscr2_MovPop_Sup1b_D:    CONSOLIDAMENTO MEF 5A
    'TIscr2_MovPop_Sup1b_L:    CONSOLIDAMENTO MEF 6A
    'TIscr2_MovPop_Sup4:       CONSOLIDAMENTO MEF 7A
    'TIscr2_MovPop_New:        CONSOLIDAMENTO MEF 9B
    Dim iSex0&, ix0&
    Dim t&
  
  If icol = EMef.Mef_PosPens Or icol = EMef.Mef_PosPens12 Then
    Call MsgBox("TODO")
    Stop
    Exit Sub
  End If
    If t0 >= 1 And t0 <= 99 And y <> 0 Then
      iSex0 = iSex1 - 1
      ix0 = ix1 - 20
      '---
      'If OPZ.DBXMEF < 0 And (icol = Mef_PosRedd Or icol = Mef_PosContr) Then
      If DBXMEF < 0 And (icol = Mef_PosRedd) Then
        t = t0 - 1
        ix0 = ix0 - 1
      Else
        t = t0
      End If
    '---
'If t >= 94 Then
't = t
'End If
      If ix0 < 0 Then
        ix0 = 0
      ElseIf ix0 > 130 Then
        ix0 = 130
      End If
      tbMEF(iSP, iSex0, ix0, Mef_NumAgg, t) = tbMEF(iSP, iSex0, ix0, Mef_NumAgg, t) + 1
      tbMEF(iSP, iSex0, ix0, icol, t) = tbMEF(iSP, iSex0, ix0, icol, t) + y
'If iSP = 18 And iSex0 = 1 And (ix0 = 100 Or ix0 = 101) And (t = 92 Or t = 93) And Int(tbMEF(iSP, iSex0, ix0, icol, t)) = 5587 Then
'y = y
'End If
    End If
  End Sub
  
  
  Public Sub MEF2(ByRef tbMEF#(), ByRef iSex1&, ByRef ix1&, ByRef iSP&, ByRef t&, ByRef y#, ByRef y12#)
    Dim iSex0&, ix0&
    Dim bNew As Boolean
    
    'If t >= 1 And t <= 99 And Abs(y) > AA0 Then
    If t >= 1 And t <= 99 And y <> 0 Then
      iSex0 = iSex1 - 1
      ix0 = ix1 - 20
      If ix0 < 0 Then
        ix0 = 0
      ElseIf ix0 > 130 Then
        ix0 = 130
      End If
  If iSP > Mef_ps_ps Then
    bNew = True
  End If
  'If iStato = 2 Then
  'y = y
  'End If
      tbMEF(iSP, iSex0, ix0, Mef_NumAgg, t) = tbMEF(iSP, iSex0, ix0, Mef_NumAgg, t) + 2
      tbMEF(iSP, iSex0, ix0, EMef.Mef_PosPens, t) = tbMEF(iSP, iSex0, ix0, EMef.Mef_PosPens, t) + y
      tbMEF(iSP, iSex0, ix0, EMef.Mef_PosPens12, t) = tbMEF(iSP, iSex0, ix0, EMef.Mef_PosPens12, t) + y12
    End If
  End Sub
  
  
  Public Sub TAppl_Stampa3e6(ByRef appl As TAppl, ByRef tbMEF() As Double, _
      ByRef db As ADODB.Connection)
    '******************************
    'Stampa delle tabelle di output
    '******************************
    Dim tau&
    Dim iSP&, jSP0&, jSP1&, kSP0&, kSP1&
    Dim iSex0&, iSex1&
    Dim ix0&, ix1&
    Dim tim1#
    Dim sErr$, sep$
    Dim fo1&, fo2&
    '---
    Dim flagNew&, flagTot&, nRec1&
    Dim pensNew#, popNew#, popFinNew#, popInNew#
    Dim pensTot#, popTot#, popFinTot#, popInTot#
    Dim SQL$
    Dim rs1 As ADODB.Recordset
    Dim rs2 As ADODB.Recordset
      
    '******************
    'Inizializzazione 1
    '******************
    tim1 = Timer
    sErr = ""
    sep = ";"
    '*********************
    'Cancella la tabella 2
    '*********************
    SQL = "DELETE"
    SQL = SQL & " FROM DBXMEF1"
    Call db.Execute(SQL, nRec1)
    '*********************
    'Cancella la tabella 2
    '*********************
    SQL = "DELETE"
    SQL = SQL & " FROM DBXMEF2"
    Call db.Execute(SQL, nRec1)
    '******************
    'Salva la tabella 1
    '******************
    SQL = "SELECT DBXMEF1.*"
    SQL = SQL & " FROM DBXMEF1"
    SQL = SQL & " WHERE 1=2"
    Set rs1 = New ADODB.Recordset
    Call rs1.Open(SQL, db, adOpenKeyset, adLockOptimistic)
    For tau = IIf(OPZ_DBXMEF < 0, 0, 1) To 99
      For iSP = 0 To EMef.Mef_Max
        For iSex0 = 0 To 1
          iSex1 = iSex0 + 1
          For ix0 = 0 To 130
            ix1 = ix0 + 20
            If tbMEF(iSP, iSex0, ix0, EMef.Mef_NumAgg, tau) > 0 Then
              Call jSP(iSP, jSP0, jSP1)
              kSP0 = kSP(jSP0)
              kSP1 = kSP(jSP1)
              Call rs1.AddNew
              rs1.Fields("Anno").Value = appl.t0 + tau
              rs1.Fields("SP").Value = iSP
              rs1.Fields("JSP0").Value = jSP0
              rs1.Fields("JSP1").Value = jSP1
              rs1.Fields("KSP0").Value = kSP0
              rs1.Fields("KSP1").Value = kSP1
              rs1.Fields("Sesso").Value = iSex1
              rs1.Fields("Eta").Value = ix1
              rs1.Fields("PosFin").Value = tbMEF(iSP, iSex0, ix0, Mef_PosFin, tau)
'If Int(rs1.Fields("PosFin").Value) = 5587 Then
'ix0 = ix0
'End If
              rs1.Fields("PosRedd").Value = tbMEF(iSP, iSex0, ix0, Mef_PosRedd, tau)
              rs1.Fields("PosPens").Value = tbMEF(iSP, iSex0, ix0, Mef_PosPens, tau)
              rs1.Fields("PosPens12").Value = tbMEF(iSP, iSex0, ix0, Mef_PosPens12, tau)
              rs1.Fields("PosAnz").Value = tbMEF(iSP, iSex0, ix0, Mef_PosAnz, tau)
              rs1.Fields("PosContr").Value = tbMEF(iSP, iSex0, ix0, Mef_PosContr, tau)
              rs1.Fields("NumAgg").Value = tbMEF(iSP, iSex0, ix0, Mef_NumAgg, tau)
              Call rs1.Update
            End If
          Next ix0
        Next iSex0
      Next iSP
    Next tau
    Call rs1.Close
    '*******************
    'Stampa la tabella 1
    '*******************
    fo1 = fopen("DBxMEF1.txt", "w")
    Print #fo1, "Anno";
    Print #fo1, sep & "SP";
    Print #fo1, sep & "Sesso";
    Print #fo1, sep & "Eta";
    Print #fo1, sep & "PosIn";
    Print #fo1, sep & "PosFin";
    Print #fo1, sep & "PosRedd";
    Print #fo1, sep & "PosPens";
    Print #fo1, sep & "PosAnz";
    Print #fo1, sep & "PosContr";
    Print #fo1, sep & "NumAgg";
    Print #fo1,
    '---
    SQL = "SELECT DBXMEF1.*"
    SQL = SQL & " FROM DBXMEF1"
    SQL = SQL & " ORDER BY Anno ASC,SP ASC, Sesso ASC,Eta ASC"
    Set rs1 = New ADODB.Recordset
    Call rs1.Open(SQL, db, adOpenForwardOnly, adLockReadOnly)
    '---
    Do Until rs1.EOF
      Print #fo1, rs1.Fields("Anno").Value;
      Print #fo1, sep & rs1.Fields("SP").Value;
      Print #fo1, sep & rs1.Fields("Sesso").Value;
      Print #fo1, sep & rs1.Fields("Eta").Value;
      Print #fo1, sep & rs1.Fields("PosFin").Value;
      Print #fo1, sep & rs1.Fields("PosRedd").Value;
      Print #fo1, sep & rs1.Fields("PosPens").Value;
      Print #fo1, sep & rs1.Fields("PosAnz").Value;
      Print #fo1, sep & rs1.Fields("PosContr").Value;
      Print #fo1, sep & rs1.Fields("NumAgg").Value;
      Print #fo1,
      Call rs1.MoveNext
    Loop
    Call rs1.Close
    Call fclose(fo1)
  'GoTo ExitPoint
    '*****************
    'Crea la tabella 2
    '*****************
    SQL = "SELECT Anno,Stato,TipoPens,Sesso,Eta" & vbCrLf
    SQL = SQL & ",PosFinNew1 AS PosFinNew" & vbCrLf
    SQL = SQL & ",IIf(PosFinNew1>0,PosReddNew1/PosFinNew1,0) AS PosReddNew" & vbCrLf
    SQL = SQL & ",IIf(PosFinNew1>0,PosPens12New1/PosFinNew1,0) AS PosPens12New" & vbCrLf
    SQL = SQL & ",IIf(PosFinNew1>0,PosAnzNew1/PosFinNew1,0) AS PosAnzNew" & vbCrLf
    SQL = SQL & ",IIf(PosFinNew1>0,PosContrNew1/PosFinNew1,0) AS PosContrNew" & vbCrLf
    SQL = SQL & ",PosFinTot1 AS PosFinTot" & vbCrLf
    SQL = SQL & ",PosReddTot1/PosFinTot1 AS posReddMed" & vbCrLf
    SQL = SQL & ",PosPens12Tot1/PosFinTot AS posPens12Med" & vbCrLf
    SQL = SQL & ",PosAnzTot1/PosFinTot AS posAnzMed" & vbCrLf
    SQL = SQL & ",PosContrTot1/PosFinTot AS posContrMed" & vbCrLf
    SQL = SQL & " FROM (SELECT Anno" & vbCrLf
    SQL = SQL & ",INT(KSP1/10) AS Stato" & vbCrLf
    SQL = SQL & ",IIF(KSP1 MOD 10>0,KSP1 MOD 10,NULL) AS TipoPens" & vbCrLf
    SQL = SQL & ",Sesso,Eta" & vbCrLf
    SQL = SQL & ",Sum(PosFin) AS PosFinTot1" & vbCrLf
    SQL = SQL & ",Sum(PosRedd) AS PosReddTot1" & vbCrLf
    SQL = SQL & ",Sum(PosPens12) AS PosPens12Tot1" & vbCrLf
    SQL = SQL & ",Sum(PosAnz) As PosAnzTot1" & vbCrLf
    SQL = SQL & ",Sum(PosContr) AS PosContrTot1" & vbCrLf
    SQL = SQL & ",Sum(IIf(JSP0=JSP1,0,PosFin)) AS PosFinNew1" & vbCrLf
    SQL = SQL & ",Sum(IIf(JSP0=JSP1,0,PosRedd)) AS PosReddNew1" & vbCrLf
    SQL = SQL & ",Sum(IIf(JSP0=JSP1,0,PosPens12)) AS PosPens12New1" & vbCrLf
    SQL = SQL & ",Sum(IIf(JSP0=JSP1,0,PosAnz)) AS PosAnzNew1" & vbCrLf
    SQL = SQL & ",Sum(IIf(JSP0=JSP1,0,PosContr)) AS PosContrNew1" & vbCrLf
    SQL = SQL & " FROM DBxMEF1" & vbCrLf
    SQL = SQL & " WHERE KSP1>0" & vbCrLf
    SQL = SQL & " AND PosFin>0" & vbCrLf
    SQL = SQL & " GROUP BY Anno,KSP1,Sesso,Eta" & vbCrLf
    SQL = SQL & " ORDER BY Anno,KSP1 ASC,Sesso ASC,Eta ASC)" & vbCrLf
    SQL = SQL & " ORDER BY Anno,Stato ASC,TipoPens ASC,Sesso ASC,Eta ASC" & vbCrLf
    Set rs1 = New ADODB.Recordset
    Call rs1.Open(SQL, db, adOpenForwardOnly, adLockReadOnly)
    '---
    SQL = "SELECT DBXMEF2.*"
    SQL = SQL & " FROM DBXMEF2"
    SQL = SQL & " WHERE 1=2"
    Set rs2 = New ADODB.Recordset
    Call rs2.Open(SQL, db, adOpenKeyset, adLockOptimistic)
    '---
    Do Until rs1.EOF
      Call rs2.AddNew
  'Anno  KSP1  PosFinTot PosReddMed  PosPens12Med  PosAnzMed PosContrMed NumAggTot PosFinNew PosReddNew  PosPens12New  PosAnzNew PosContrNew NumAggNew
  '2018  1 0,979359375921539 2878,84 0 2,98055555555556  2946  4 0 0 0 0 0 0
      rs2.Fields("Anno").Value = rs1.Fields("Anno").Value
      rs2.Fields("Stato").Value = rs1.Fields("Stato").Value
      rs2.Fields("TipoPens").Value = rs1.Fields("TipoPens").Value
      rs2.Fields("Sesso").Value = rs1.Fields("Sesso").Value
      rs2.Fields("Eta").Value = rs1.Fields("Eta").Value
      rs2.Fields("PosFinNew").Value = rs1.Fields("PosFinNew").Value
      rs2.Fields("PosReddNew").Value = rs1.Fields("PosReddNew").Value
      rs2.Fields("PosPens12New").Value = rs1.Fields("PosPens12New").Value
      rs2.Fields("PosAnzNew").Value = rs1.Fields("PosAnzNew").Value
      rs2.Fields("PosContrNew").Value = rs1.Fields("PosContrNew").Value
      rs2.Fields("PosFinTot").Value = rs1.Fields("PosFinTot").Value
      rs2.Fields("PosReddMed").Value = rs1.Fields("PosReddMed").Value
      rs2.Fields("PosPens12Med").Value = rs1.Fields("PosPens12Med").Value
      rs2.Fields("PosAnzMed").Value = rs1.Fields("PosAnzMed").Value
      rs2.Fields("PosContrMed").Value = rs1.Fields("PosContrMed").Value
      Call rs2.Update
      Call rs1.MoveNext
    Loop
    Call rs2.Close
    '*******************
    'Stampa la tabella 2
    '*******************
    fo2 = fopen("DBxMEF2.txt", "w")
    If 1 = 2 Then
      Print #fo2, "Anno";
      Print #fo2, sep & "Stato";
      Print #fo2, sep & "TipoPens";
      Print #fo2, sep & "Sesso";
      Print #fo2, sep & "Eta";
      Print #fo2, sep & "PosFinNew";
      Print #fo2, sep & "PosReddNew";
      Print #fo2, sep & "PosPens12New";
      Print #fo2, sep & "PosAnzNew";
      Print #fo2, sep & "PosContrNew";
      Print #fo2, sep & "PosFinTot";
      Print #fo2, sep & "PosReddMed";
      Print #fo2, sep & "PosPens12Med";
      Print #fo2, sep & "PosAnzMed";
      Print #fo2, sep & "PosContrMed";
      Print #fo2,
    End If
    '---
    SQL = "SELECT DBXMEF2.*"
    SQL = SQL & " FROM DBXMEF2"
    SQL = SQL & " WHERE Anno<=" & appl.t0 + 50
    SQL = SQL & " AND Anno>" & appl.t0
    SQL = SQL & " ORDER BY Anno ASC,Stato ASC,TipoPens ASC,Sesso ASC,Eta ASC"
    Set rs2 = New ADODB.Recordset
    Call rs2.Open(SQL, db, adOpenForwardOnly, adLockReadOnly)
    '---
    Do Until rs2.EOF
      Print #fo2, rs2.Fields("Anno").Value;
      Print #fo2, sep & rs2.Fields("Stato").Value;
      Print #fo2, sep & rs2.Fields("TipoPens").Value;
      Print #fo2, sep & rs2.Fields("Sesso").Value;
      Print #fo2, sep & rs2.Fields("Eta").Value;
      Print #fo2, sep & rs2.Fields("PosFinNew").Value;
      Print #fo2, sep & rs2.Fields("PosReddNew").Value;
      Print #fo2, sep & rs2.Fields("PosPens12New").Value;
      Print #fo2, sep & rs2.Fields("PosAnzNew").Value;
      Print #fo2, sep & rs2.Fields("PosContrNew").Value;
      Print #fo2, sep & rs2.Fields("PosFinTot").Value;
      Print #fo2, sep & rs2.Fields("PosReddMed").Value;
      Print #fo2, sep & rs2.Fields("PosPens12Med").Value;
      Print #fo2, sep & rs2.Fields("PosAnzMed").Value;
      Print #fo2, sep & rs2.Fields("PosContrMed").Value;
      Print #fo2,
      Call rs2.MoveNext
    Loop
    Call rs2.Close
    Call fclose(fo2)
    
ExitPoint:
    On Error GoTo 0
    Call fclose(fo1)
    Call fclose(fo2)
    tim1 = Timer - tim1
    If sErr <> "" Then
      Call MsgBox("Si è verificato il seguente errore:" & vbCrLf & vbCrLf & sErr, vbExclamation)
    End If
    Exit Sub
    
ErrorHandler:
    sErr = Err.Description
    Err.Clear
    GoTo ExitPoint
  End Sub
