Attribute VB_Name = "modAfpTGlo"
'A: 30330846,  E1: 30325517,  E0?: 30319051,  E1: 30328904,  E2: 30330136
Option Explicit

'--- 20181108LC (inizio)  'ex 20180514LC
Public Const P_VER As String = "3.2.A28C"
Public Const P_VERD As String = "08/11/2018"
Public Const AFP_SVER As String = "(v. 3.2.A28C del 08/11/2018)"
Public Const AFP_SVER1 As String = "32A28C"
'--- 20181108LC (fine)  'ex 20180514LC

Public OPZ_EA_HMIN_MONT As Double        'Anz. min per restituzione montante
Public OPZ_EA_ABBQ_REDDMIN As Double     'Abbattimento probabilit� di diventare EA (reddito minimo)
Public OPZ_EA_ABBQ_PERC As Double        'Abbattimento probabilit� di diventare EA (percentuale)
Public OPZ_RC_DEC_SENZA_SUP As Double    'Stima quota montante rimborsato agli eredi dei deceduti senza superstiti
Public OPZ_RC_65 As Double               'Rimborso montante, stima aliquota per usciti a 65 anni senza diritto
Public OPZ_RC_W As Double                'Stima quota montante rimborsato agli usciti senza diritto
Public OPZ_CV_NOME As String           '20160419LC-2  'ex 20080605LC
Public OPZ_Pveo30_19810128 As Integer  '20080207LC
Public OPZ_ELAB_CODA As Integer        '20080416LC
Public Const P_CENTO As Integer = 100  '20080416LC
Public P_DUECENTO As Long              '20080416LC
Public OPZ_TEMP1 As Integer            '20080416LC
'--- 20120302LC
Public OPZ_PENS_ANZ_MODEL As Integer   'PENSIONI DI ANZIANITA': CODICE MODELLO (-1: no abbatt.; 0,1: abbatt.normale; 2: abbatt. con COE_PA)
Public OPZ_PENS_ANZ_HABB As Integer      'PENSIONI DI ANZIANITA': ANZIANITA' MINIMA DI NON ABBATTIMENTO (ES: 40= ABBATTE LE PENSIONI CON H<40; 0=NO PENALIT�)
Public OPZ_PENS_ATT_MODEL As Integer   'PENSIONATI CONTRIBUENTI: CODICE MODELLO (0:NORMALE, 1:CONTRIBUTO MINIMO)
Public OPZ_PENS_ATT_ANNO As Integer      'PENSIONATI CONTRIBUENTI: ANNO INIZIALE
Public OPZ_PENS_ATT_FRAZ As Double       '(NEW) PENSIONATI CONTRIBUENTI: FRAZIONE DEL CONTRIBUTO MINIMO '20121008LC
'--- 20080611LC ---
Public OPZ_ABB_QM_MODEL As Long        'Abbattimento delle probabilit� di morte: codice modello
Public OPZ_ABB_QM_PRM_1 As Double      'Abbattimento delle probabilit� di morte: parametro 1
Public OPZ_ABB_QM_PRM_2 As Double      'Abbattimento delle probabilit� di morte: parametro 2 '20120109LC
Public OPZ_ABB_QM_BASE As String       'Abbattimento delle probabilit� di morte: nome base tecnica '20120312LC
Public OPZ_ABB_QM_SUP As Long          '20130611LC (spostato, ex OPZ_INT_SUB_AQ)
'--- 20140603LC (inizio)
Public OPZ_SUDDIVISIONI_MODEL As Integer    'Suddivisione degli iscritti: codice modello
Public OPZ_SUDDIVISIONI_MR As Integer       'Suddivisione degli iscritti: metodo di ripartizione
Public OPZ_SUDDIVISIONI_DT As Integer       'Suddivisione degli iscritti: anni di slittamento rispetto al BT
Public OPZ_INT_SUDDIVISIONI_N As Integer    'Suddivisione degli iscritti: numero di suddivisioni
Public OPZ_INT_SUDDIVISIONI_nomi As Variant 'Suddivisione degli iscritti: nomi di suddivisioni
'--- 20140603LC (inizio)
Public OPZ_OMEGA As Integer             '20080926LC
  Public OPZ_OMEGAP1 As Integer           '20130211LC
'---
Public OPZ_INTERP_H As Integer          '20111217LC
Public OPZ_INTERP_X As Integer          '20111217LC
'---
Public OPZ_PRORATA_MODEL As Byte        '20111208LC 'MODELLO DI PRORATA (1:in base all'anno)
  Public OPZ_PRORATA_ANNO As Integer    '20111208LC 'Anno di entrata in vigore del prorata
'--- 20131123C (inizio)
'Public OPZ_MISTO_MODEL As Integer   'MODELLO METODO MISTO (1:usa retributivo se meno oneroso, 2:agevolazioni per i giovani, 3:retributivo minimo)
Public OPZ_MISTO_AGEV_GIOV As Integer   'METODO MISTO agevolazioni per i giovani
Public OPZ_MISTO_RETRIB_MIN As Integer  'METODO MISTO retributivo minimo
'--- 20131123C (fine)
'20120514LC (inizio)
'Public OPZ_MISTO_SOGG_MODEL As Integer  '20140320LC (commentato)  'METODO MISTO - SOGGETTIVO - MODELLO (0:no contr.vol. e solidar., 1:vedi documento)
  Public OPZ_MISTO_SOGG_VOL_A As Double     'METODO MISTO - SOGGETTIVO - CONTRIBUTO FACOLTATIVO ATTIVI (% del reddito)
  Public OPZ_MISTO_SOGG_VOL_P As Double     'METODO MISTO - SOGGETTIVO - CONTRIBUTO FACOLTATIVO PENSIONATI ATTIVI (% del reddito)
  Public OPZ_MISTO_SOGG_SOL_P1 As Double    'METODO MISTO - SOGGETTIVO - CONTRIBUTO DI SOLIDARIETA PENSIONATI NON ATTIVI    (SULLA QUOTA RETRIBUTIVA)  '20121230LC
  Public OPZ_MISTO_SOGG_SOL_P2 As Double    'METODO MISTO - SOGGETTIVO - CONTRIBUTO DI SOLIDARIETA PENSIONATI ATTIVI        (SULLA QUOTA RETRIBUTIVA)  '20121230LC
  Public OPZ_MISTO_SOGG_SOL_P3 As Double    'METODO MISTO - SOGGETTIVO - CONTRIBUTO DI SOLIDARIETA PENSIONATI DI ANZIANITA' (SULLA QUOTA RETRIBUTIVA)  '20121230LC
  Public OPZ_MISTO_SOGG_SOL_ANNI As Integer 'METODO MISTO - SOGGETTIVO - ANNI DI CONTRIBUTO DI SOLIDARIETA'
Public OPZ_MISTO_INTEGR_MODEL As Integer  '(NEW) METODO MISTO - INTEGRATIVO: MODELLO (0:NO RETR., 4:DEFINITIVO, 1:RETR.COSTANTE, 2:RETR.CRESCENTE NEL TEMPO, 3:RETR.COSTANTE PER NUOVI ISCRITTI E CRESCENTE PER VECCHI)
  Public OPZ_MISTO_INTEGR_ALF As Double     'METODO MISTO - INTEGRATIVO: % FISSA DI RETROCESSIONE PER MONTANTE PENSIONISTICO (se negativa � in % del valore di C_COE_VAR4)
  Public OPZ_MISTO_INTEGR_ALR As Double     'METODO MISTO - INTEGRATIVO: % MAX DI RETROCESSIONE PER MONTANTE PENSIONISTICO   (se negativa � in % del valore di C_COE_VAR4)
  Public OPZ_MISTO_INTEGR_ANNI As Double    'METODO MISTO - INTEGRATIVO: ANNI PER RAGGIUNGERE LA % MAX DI CONTRIBUTI INTEGRATIVI UTILI PER MONTANTE PENSIONISTICO
  'Public OPZ_MISTO_INTEGR_MAX As Double     'METODO MISTO - INTEGRATIVO: MASSIMALE (% DEL REDDITO se positivo, es. 2,0,  oppure % DEL SOGG MAX se negativo; es. -1,5)
'--- 20140404LC (inizio)
Public OPZ_MISTO_PMIN_MODEL As Integer    '[0;1;2;3;4*]   'METODO MISTO - PENSIONE MINIMA: MODELLO (0 O 1:PRO RATA PER LA SOLA PARTE RETRIBUTIVA (obsoleto), 2:RGP 2012, 3:con pro-rata, 4:definitivo)
  Public OPZ_MISTO_PMIN_ABB As Double       '[0,0*-1,0]       'METODO MISTO - PENSIONE MINIMA: ABBATTIMENTO DELLA PENSIONE (ES 0,1: ABBATTE DEL 10%, OSSIA AL 90%)
  Public OPZ_MISTO_PMIN_NRM As Integer      '[0-19;20*;>20]   'METODO MISTO - PENSIONE MINIMA: NUMERO DI REDDITI (RIVALUTATI CON L'INFLAZIONE) PER IL CALCOLO DELLA MEDIA DI CONFRONTO (0 SE NON USATA)
  Public OPZ_MISTO_PMIN_ART25 As Integer    '[-1*;0-30]       'METODO MISTO - PENSIONE MINIMA: NUMERO ABBATTIMENTI SECONDO ART.25 (-1:RGP 2012, 0-30: specificati)
  Public OPZ_MISTO_PMIN_RESTMC As Integer   '[0;1;2*]         'METODO MISTO - PENSIONE MINIMA: RESTITUZIONE MONTANTE CONTRIBUTI (0:no; 1:si, fino a PMIN (abb.art.25); 2:si, totale)
  Public OPZ_MISTO_PMIN_X4 As Integer       '[0-54;55*;56-99] 'METODO MISTO - PENSIONE MINIMA: MODELLO 4, et� minima per il prorata
  Public OPZ_MISTO_PMIN_H4 As Integer       '[0-19;20*;21-99] 'METODO MISTO - PENSIONE MINIMA: MODELLO 4, anzianit� minima per il prorata
'--- 20140404LC (fine)
'--- 20130606LC (inizio) rinominati
Public OPZ_INT_PMIN_PANZ As Long
Public OPZ_INT_PMIN_DEBUG As Long
'--- 20130606LC (fine)
Public OPZ_INT_PENS_DEBUG As Byte       '20160420LC
'20120514LC (fine)
Public OPZ_MISTO_REDD_PENS As Integer   '20120109LC '1: media dei redditi fino a OPZ_PRORATA_ANNO-1, 0: media su tutti
Public OPZ_MISTO_DEB_MAT As Integer     '20120329LC '1: CALCOLO DEL DEBITO MATURATO, 0: CALCOLO NORMALE
Public OPZ_DEBUG As Integer             '20120601LC
'--- 20121204LC (inizio)
Public OPZ_MOD_ART22_MODEL As Integer   '20080709LC 'Modello di modifica art.22 (0:no, 1:si)
  Public OPZ_MOD_ART22_ANNO As Integer    'Anno di entrata in vigore della modifica
Public OPZ_MOD_ART25_MODEL As Integer   '(NEW*) MODELLO DI MODIFICA ART.25 (0:NO, 1:SI)
  Public OPZ_MOD_ART25_ANNO As Integer    '20111212LC 'Anno di modifica art.25  '20111212LC
  Public OPZ_MOD_ART25_DECR_ANZTOT        '20111212LC 'Decrementa AnzTot da DB se si applica Art.25 (0:NO, 1:SI)
Public OPZ_MOD_ART26_MODEL As Integer   '20121010LC '(NEW) MODELLO DI MODIFICA ART.26 (0:NO, 1:SI AL 31/12/2010, 2:SI AL 05/03/2010)
Public OPZ_MOD_ART_RIV_2001 As Integer  '20121008LC '(NEW) MODIFICA RIVALUTAZIONI CONTRIBUTI FINO AL 2001 (0:NO, 1:SI)
Public OPZ_MOD_ART_17_2 As Integer      '20121008LC '(NEW) MODIFICA NORMA PER ISCRITTI ANTE 29/1/1981 NEL MISTO (0:NO SEMPRE VALIDA, 1:SOLO PER TRE ANNI)
Public OPZ_MOD_ART_17_4 As Integer      '(NEW) MODIFICA NORMA N� REDDITI PER PENSIONE RETRIBUTIVA NEL MISTO (0:NO 22/27, 1:SI, -1 ANNO OGNI 5 DI ANZ.)
Public OPZ_MOD_ART_26_5 As Integer      '20121009LC 'MODIFICA per retrocessione integrativo (interno)
Public OPZ_MOD_ART_32_6 As Integer      '(NEW) MODIFICA NORMA PER PRO-RATA A 70 ANNI E ANZ(2012)>=20 (0:NO, 1:SI)
Public OPZ_MOD_X75 As Integer           '20121022LC '(NEW) DISATTIVA L'ATTESA A 75 ANNI PER LA PENS. DI VECCHIAA NEL MISTO (0:NO, 1:SI)
'---
Public OPZ_INT_BLOCKSIZE As Long        '20121031LC  '(INT) DIMENSIONE DEL BLOCCO DI RECORD DI LETTURA
Public OPZ_OUTPUT_MAX As Long       '20121114LC  '(INT)
Public OPZ_OUTPUT_MOD As String     '20121204LC  '(INT)
Public OPZ_OUTPUT_XLS As Byte       '20170125LC
Public OPZ_INT_SUP_ZZ As Long            '20121204LC  '(INT)
Public OPZ_INT_SUP_RP As Long            '20121207LC  '(INT)

Public OPZ_QUOTA_A As Long           'Mod MDG
Public OPZ_STERIL As Long            'Mod MDG
Public OPZ_PEREQ As Long             'Mod MDG
Public vecAliqMed(99) As Double      'Mod MDG

Public OPZ_INT_SUP_4 As Long            '20121212LC  '(INT)
Public OPZ_INT_SUP_DX As Double         '20130220LC  '(INT)
'--- 20160223LC (inizio) rinominati
Public OPZ_RND_MODEL As Long        '20121031LC '(INT) MODELLO DI GENERAZIONE RANDOM DELLA PROBABILITA' (0: NO, 1;SI PER TUTTE, SI:PER SINGOLE SPECIFICATE)
Public OPZ_RND_NIT As Long          '20121031LC '(INT) MODELLO DI GENERAZIONE RANDOM: NUMERO DI ITERAZIONI DA 1 A 99)
Public OPZ_RND_QAD As Long          '20121031LC '(INT) OPZIONE DI GENERAZIONE RANDOM DELLA PROBABILITA' DI PASSAGGIO DA ATTIVO A DECEDUTO (0:NO, 1:SI)
Public OPZ_RND_QED As Long          '20121031LC '(INT) OPZIONE DI GENERAZIONE RANDOM DELLA PROBABILITA' DI PASSAGGIO DA EX-ATTIVO A DECEDUTO (0:NO, 1:SI)
Public OPZ_RND_QBD As Long          '20121031LC '(INT) OPZIONE DI GENERAZIONE RANDOM DELLA PROBABILITA' DI PASSAGGIO DA PENSIONATO DI INABILITA' A DECEDUTO (0:NO, 1:SI)
Public OPZ_RND_QID As Long          '20121031LC '(INT) OPZIONE DI GENERAZIONE RANDOM DELLA PROBABILITA' DI PASSAGGIO DA PENSIONATO DI INVALIDITA' A DECEDUTO (0:NO, 1:SI)
Public OPZ_RND_QPD As Long          '20121031LC '(INT) OPZIONE DI GENERAZIONE RANDOM DELLA PROBABILITA' DI PASSAGGIO DA PENSIONATO DIRETTO A DECEDUTO (0:NO, 1:SI)
Public OPZ_RND_QAV As Long          '20150505LC '(INT) OPZIONE DI GENERAZIONE RANDOM DELLA PROBABILITA' DI PASSAGGIO DA ATTIVO A VOLONTARIO (0:NO, 1:SI)
Public OPZ_RND_QAE As Long          '20121031LC '(INT) OPZIONE DI GENERAZIONE RANDOM DELLA PROBABILITA' DI PASSAGGIO DA ATTIVO A EX-ATTIVO (0:NO, 1:SI)
Public OPZ_RND_QAB As Long          '20121031LC '(INT) OPZIONE DI GENERAZIONE RANDOM DELLA PROBABILITA' DI PASSAGGIO DA ATTIVO A PENSIONATI DI INABILITA' (0:NO, 1:SI)
Public OPZ_RND_QAI As Long          '20121031LC '(INT) OPZIONE DI GENERAZIONE RANDOM DELLA PROBABILITA' DI PASSAGGIO DA ATTIVO A PENSIONATI DI INVALIDTITA' (0:NO, 1:SI)
Public OPZ_RND_QAW As Long          '20121031LC '(INT) OPZIONE DI GENERAZIONE RANDOM DELLA PROBABILITA' DI PASSAGGIO DA ATTIVO A DECEDUTO PER ALTRE CAUSE (0:NO, 1:SI)
'--- 20160223LC (fine) rinominati
'--- 20160523LC (fine)
Public OPZ_RND_QVA As Long          '(INT) OPZIONE DI GENERAZIONE RANDOM DELLA PROBABILITA' DI PASSAGGIO DA VOLONTARIO A ATTIVO (0:NO, 1:SI)
Public OPZ_RND_QEA As Long          '(INT) OPZIONE DI GENERAZIONE RANDOM DELLA PROBABILITA' DI PASSAGGIO DA EX-ATTIVO A ATTIVO  (0:NO, 1:SI)
'--- 20160523LC (fine)
Public OPZ_INT_PF_XMIN As Long          '20121101lC
Public OPZ_INT_PF_XMAX As Long          '20121101lC
Public OPZ_INT_CV_AMIN As Long          '20121102LC
Public OPZ_INT_CV_AMAX As Long          '20121102LC
Public OPZ_INT_CV_DIM1 As Long          '20121102LC
Public OPZ_INT_CV_DIM2 As Long          '20121102LC
Public OPZ_INT_NO_AE_AB_AI As Long      '20121113LC  '(INT)
Public OPZ_INT_NO_AW As Long            '20121113LC  '(INT)
Public OPZ_INT_NO_PENSATT As Long       '20121113LC  '(INT)
Public OPZ_INT_NO_SUPERST As Long       '20121113LC  '(INT)
Public P_PFAM_MAX_TAU As Long           '20121204LC
Public P_PFAM_MAX_X As Long             '20121204LC
Public OPZ_INT_DNASC As Long            '20130211LC
Public OPZ_INT_ABBQ As Long             '20130216LC
'--- 20121204LC (fine)
'-- 20130919LC (inizio)
Public OPZ_INT_PROPEN_ANNI As Long
Public OPZ_INT_PROPEN_ALIQ As Double
'-- 20130919LC (inizio)
Public OPZ_INT_REGMIN_MODEL As Long      '20131015LC
'Public OPZ_INT_ASSIST_MODEL As Long      '20131122LC
'--- 20140317LC (inizio)
Public OPZ_SUP_MODEL As Long             '[0*;1]          'Modello di calcolo per i gruppi superstiti: 0:solo se generati, 1:sempre a gruppi
Public OPZ_SUP_DZ As Long                '[0*;1]          'Et� intere delle PFAM: 0:ultima et� in vita; 1:alla morte
'--- 20140317LC (fine)
'--- 20140319LC (inizio)
Public OPZ_MISTO_CCR_MODEL As Integer    '[0;1*;2;-1;-2]  '20140404LC  'MODELLO anno x coeff.conv.rend.: 0:pensionamento; 1:nascita ; 2:temporaneo poi nascita; valori negativi se si vuole usare per i supplementi l'anno di maturazione
Public OPZ_MISTO_SOGG_RID_F1 As Integer  '[0*;1]      'METODO MISTO - SOGGETTIVO - c.ridotti di fascia 1:  0:sempre;  1:solo se il reddito <= fascia 1
'--- 20140319LC (fine)
'--- 20140605LC
Public OPZ_INT_CV21 As Integer
Public OPZ_INT_CV25 As Integer
'--- 20150223LC
Public OPZ_TEMP_ALR_NI As Double         '[0*-1,0] '% di retrocessione nuovi ingressi: 0 come da regolanento, altrimenti � la frazione (es. 0,75 -> 3% di 4%)
'--- 20150504LC
Public OPZ_INT_MONT_CALC As Integer      '20150518LC
Public OPZ_MASSIMALE_2011 As Integer     '20150615LC   '[0;1*] STERILIZZAZIONE MASSIMALI AL 2011: 1-si(default), altro-no
Public OPZ_OUT_SIL_ATT As Byte           '20160222LC   '[0,1,2*] numero di generazioni di silenti da considerare come attivi in output
Public OPZ_FRAZ_COMP As Double           '20160306LC (mod)  '[0,00-0,75-1,00*] Frazione dei contributi che sono incassati nell'anno
Public OPZ_INT_AZZERA_REDDITO As Integer '20160306LC (mod)  '[0*,1,2] azzeramento reddito pens.att. ultimo anno: 1:totale (12 mesi), 2:parziale (ultimi 3 mesi), 0:no
Public OPZ_ART14_90 As Byte              '20160308LC  '[0-90*-255] pensione anticipata: quota minima
Public OPZ_ART14_5 As Double             '20160308LC  '[0-0,05*-1] pensione anticipata: penalizzazione annua
Public OPZ_ART16_2 As Double             '20160308LC  '[0-0,02*-1] rendita contributiva: penalizzazione annua
Public OPZ_ART19_3 As Byte               '20160309LC  '[0-3*-5] pensione di invalidit�: anz.contr. mimima nell'ultimo quinquennio
Public OPZ_ART20_1 As Byte               '20160309LC  '[0-1*-5] pensione di inabilit�:  anz.contr. minima nell'ultimo quinquennio
Public OPZ_ART23_1 As Byte               '20160420LC  '[0-1*-5] pensione indiretta:  anz.contr. minima nell'ultimo quinquennio
Public OPZ_TOLL_PENS As Double           '20160420LC
Public OPZ_INT_TOLL_PENS As Double       '20160420LC
Public OPZ_SIL_PVEP75 As Integer         '20160422LC
'--- 20161112LC (inizio)
Public OPZ_MOD_REG_1A As Integer
  Public OPZ_MOD_REG_1_CV  As String  '20170307LC
  Public OPZ_MOD_REG_1_ANNI As Byte
Public OPZ_MOD_REG_2 As Integer
  Public OPZ_MOD_REG_2_CV  As String  '20170307LC
  Public OPZ_MOD_REG_2_AMIN As Integer  '20161116LC-3
  Public OPZ_MOD_REG_2_ANNO As Integer
  Public OPZ_MOD_REG_2_ANNI As Byte  '20161116LC
  Public OPZ_MOD_REG_2_CIFRE_TOLL As Integer
  Public OPZ_MOD_REG_2_NITMAX As Integer
  Public OPZ_MOD_REG_2_PERC_PIL5 As Double
  Public OPZ_MOD_REG_2_RIV_MAX As Double  '20161114LC
Public OPZ_MOD_REG_3 As Integer
  Public OPZ_MOD_REG_3_CV  As String  '20170307LC
  Public OPZ_MOD_REG_3_ANNO As Integer
  Public OPZ_MOD_REG_3_PERC_ENAS As Double
'--- 20161112LC (fine)
'--- 20161126LC (inizio)
Public OPZ_PENS_MODEL As Integer
Public OPZ_PENS_SP0 As Double            '[0,0:1,0; 0,5*]  'Spesa pensionistica nell'anno precedente a quello di calcolo della prima pensione
Public OPZ_PENS_C0 As Double             '[0,0-0,5*-1,0]  'Coeff. di interp. dei pensionati NP0 per la spesa pensionistica SP = C0*NP0 +(1-C0)*NP1
'--- 20161126LC (fine)
Public OPZ_DBG_PENSIONE As Double        '20160312LC
Public OPZ_INT_PENSIONE As Double        '20160312LC
Public OPZ_DBG_INCRPENS As Double        '20160312LC
Public OPZ_INT_INCRPENS As Double        '20160312LC
'--- 20170724LC (inizio)
Public OPZ_RC_MODEL As Byte
Public OPZ_RC_ADEC As Integer
Public OPZ_RC_FRAZ_ATT As Double
Public OPZ_RC_FRAZ_SIL As Double
'--- 20170724LC (fine)
Public OPZ_RIATT_AMIN As Integer  'Anno minimo di riattivazione dei silenti (ANNO BT+1 se 0)

'--- 20181108LC (inizio) MEF
Public Const OPZ_DBXMEF As Long = 1
'--- 20181108LC (fine) MEF


'20131117LC (nuova)
Public Type TGlo_TSens
  Progr As Byte
  TVal As Double
  TInfl As Double
  TLS As Double
  TAnt As Double
  QD As Double
  QB As Double
  QI As Double
  QW As Double
End Type

Public Type TGlobale
  rep_chk(12) As Byte
  dbConn As String  '20121024LC
  db As New ADODB.Connection 'Database Access di lettura/scrittura
  DB_TYPE As Integer  '20121024LC
  rsDbg1 As New ADODB.Recordset
  rsDbg2 As New ADODB.Recordset
  Counter As Long
  nRead As Double
  nElab As Double
  bDebug As Byte
  OpzIni As Variant '20120319LC
  '******************
  'Interfaccia RISPEN
  '******************
  szFileRisPen As String
  dElabIn As Date
  dElabFin As Date
  tAcc As Double
  tStampa As Double
  tElab As Double
  tMin As Double
  tMax As Double
  tMed As Double
  szGruppo As String
  '--- Supporto Excel
  rep_opFile(2) As Boolean
  ''''xlsApp As Object
  xlsTemplate As String
  xlsOutput As String
  '--- Supporto ASP (20121010LC)
  MJ As Long
  MdiAfp As MDIForm
  sVer As String
  '--- 20140317LC (inizio)
  '--- OA ---
  iDB As Long
  '### OA (altro) ###
  '--- Versione sw
  ver1 As Long  'Versione
  ver2 As Long  'Sottoversione
  verLic As String 'Licenza
  '--- (old) ---
  rep_opb(1) As Long
  rep_opz As Long
  bOptions(4) As Boolean
  '--- 20100615LC
  bAlfaNew As Boolean
  ReddMin() As Double
  '### (fine) ###
  '--- 20131117LC
  TassoMP As Double 'Tasso medio ponderato
  bSens As Byte
  iSens As Byte
  nSens As Byte
  Sens() As TGlo_TSens
  '--- (spostati da TAppl)
  Sens_TVal As Double
  Sens_TInfl As Double
  Sens_TLS As Double
  Sens_TAnt As Double
  Sens_QD As Double
  Sens_QB As Double
  Sens_QI As Double
  Sens_QW As Double
  '--- 20131124LC
  idReport As Long
  '--- 20140317LC (fine)
End Type


Public Function TGlo_Init(glo As TGlobale, mi&, MdiAfp As Object) As String
'20150522LC  spostata la lettura di AFP_OPZ.INI
  Dim fi&
  Dim a$, sErr$
  Dim v As Variant
  
  With glo
    sErr = ""
    '--- 20121010LC (inizio)
    .sVer = AFP_SVER
    .MJ = mi
    '--- da MdiAfp.Form_Load
    ChDrive App.Path
    ChDir App.Path
    'bCv25 = False  '20140605LC  '20120213LC
    '--- 20121024LC (inizio)
    .bDebug = 0
    If ON_ERR_RN1 = True Then On Error Resume Next '20111217LC
    fi = FreeFile
    Open "AFP.INI" For Input As #fi
    '--- 20140317LC (inizio)
    Line Input #fi, a$
    If InStr(a, ",") > 0 Then
      v = Split(a, ",")
      .ver1 = Val(v(0))
      .ver2 = Val(v(1))
      .verLic = Val(v(2))
      Line Input #fi, a$
    Else
      Call MsgBox("Errore durante la lettura del DB", vbCritical)
      Stop
    End If
    '---
    If InStr(a, ",") > 0 Then
      v = Split(a, ",")
    Else
      v = Split(a & " ", " ")
    End If
    .iDB = Val(v(0))
    If UBound(v, 1) >= 1 Then
      .DB_TYPE = Val(v(1))
      If .DB_TYPE < DB_ACCESS Or .DB_TYPE >= DB_MYSQL Then
        .DB_TYPE = DB_ACCESS
      End If
    Else
      .DB_TYPE = DB_ACCESS
    End If
    '--- 20140317LC (fine)
    Line Input #fi, .dbConn
    Close #fi
    '--- 20121220LC (inizio)
    OPZ_OUTPUT_MAX = IIf(Val(a$) < 0, 1, 0)
    If Math.Abs(Val(a$)) = 4 Then
    '--- 20121220LC (fine)
      .DB_TYPE = DB_ORACLE
    Else
      .DB_TYPE = DB_ACCESS
    End If
    Call .db.Open(.dbConn)
    '---
    If .MJ = MI_VB Then
      Set .MdiAfp = MdiAfp
      MdiAfp.Tag = .sVer
      MdiAfp.Caption = "Analisi Fondi Pensione per ENASARCO " & .sVer & " (db:" & .DB_TYPE & ")"  '20150504LC
    Else
      Set .MdiAfp = Nothing
    End If
    '--- 20121024LC (fine)
    Call modAfpUtil0.OpzioniReport(glo, Nothing, -1) '20121010LC
  End With
  
ExitPoint:
  TGlo_Init = sErr
End Function


Public Function TGlo_Opz_Init(glo As TGlobale) As String
'20150522LC  nuova, separata la lettura di AFP_OPZ.INI
  Dim fi&, i&, ir&
  Dim y As Double  '20160222LC
  Dim a$, b$, sErr$, sErrOpzNA$, sErrOpzS$
  
  With glo
    sErr = ""
    sErrOpzNA = ""
    sErrOpzS = ""
    '**********************
    'Inizializza le opzioni
    '**********************
    '--- Lettura altre opzioni ---
    OPZ_EA_HMIN_MONT = 0.0001     'Anz. min per restituzione montante
    OPZ_RC_DEC_SENZA_SUP = 1      'Stima quota montante rimborsato agli eredi dei deceduti senza superstiti
    OPZ_RC_65 = 1                 'Rimborso montante, stima aliquota per usciti a 65 anni senza diritto
    OPZ_RC_W = 1                  'Stima quota montante rimborsato agli usciti senza diritto
    '--- 20150322LC (inizio)
    OPZ_PENS_ANZ_MODEL = 1        'PENSIONI DI ANZIANITA': CODICE MODELLO (-1: no abbatt.; 0,1: abbatt.normale; 2: abbatt. con COE_PA)
    '--- 20150322LC (fine)
    '20120302LC (inizio)
      OPZ_PENS_ANZ_HABB = 99        'PENSIONI DI ANZIANITA': ANZIANITA' MINIMA DI NON ABBATTIMENTO (ES: 40= ABBATTE LE PENSIONI CON H<40; 0=NO PENALIT�)
    OPZ_PENS_ATT_MODEL = 1        'PENSIONATI CONTRIBUENTI: CODICE MODELLO (0:NORMALE, 1:CONTRIBUTO MINIMO)
      OPZ_PENS_ATT_ANNO = 2013      'PENSIONATI CONTRIBUENTI: ANNO INIZIALE
      OPZ_PENS_ATT_FRAZ = 0.5       '(NEW) PENSIONATI CONTRIBUENTI: FRAZIONE DEL MINIMO '20121022LC
    '20120302LC (fine)
    OPZ_EA_ABBQ_REDDMIN = 0       'Abbattimento probabilit� di diventare EA (reddito minimo)
    '20091014LC
    OPZ_EA_ABBQ_PERC = 1#          'Abbattimento probabilit� di diventare EA (percentuale)
    OPZ_CV_NOME = "BASE"      '20160419LC-2  'ex 20140320LC
    OPZ_Pveo30_19810128 = 0   '20080207LC
    OPZ_ELAB_CODA = 0         '20080416LC '<>0 se si deve elaborare fino all'uscita di tutti gli iscritti
    '--- 20140603LC (inizio)
    OPZ_SUDDIVISIONI_MODEL = 0     'Suddivisione degli iscritti: codice modello
    OPZ_SUDDIVISIONI_MR = 0        'Suddivisione degli iscritti: metodo di ripartizione
    OPZ_SUDDIVISIONI_DT = 0        'Suddivisione degli iscritti: anni di slittamento rispetto al BT
    OPZ_INT_SUDDIVISIONI_N = 1     'Suddivisione degli iscritti: numero di suddivisioni
    '--- 20140603LC (fine)
    OPZ_MOD_ART22_MODEL = 1        '20121010LC 'Modello di modifica art.22 (0:no, 1:si)
      OPZ_MOD_ART22_ANNO = 2009      'Anno di entrata in vigore della modifica
    OPZ_MOD_ART25_MODEL = 1        '(NEW*) MODELLO DI MODIFICA ART.25 (0:NO, 1:SI)
      OPZ_MOD_ART25_ANNO = 2009      '20121010LC 'Anno di modifica art.25  '20111212LC
      OPZ_MOD_ART25_DECR_ANZTOT = 0  '20111212LC 'Decrementa AnzTot da DB se si applica Art.25 (0:NO, 1:SI)
    OPZ_MOD_ART26_MODEL = 2        '20121010LC '(NEW) MODELLO DI MODIFICA ART.26 (0:NO, 1:SI AL 31/12/2010, 2:SI AL 05/03/2010)
    '---
    OPZ_OMEGA = 120                'Massima et� ammissibile per le basi demografiche
    '---
    OPZ_INTERP_H = 0               '20111217LC
    OPZ_INTERP_X = 0               '20111217LC
    '---
    '20080611LC (inizio)
    OPZ_ABB_QM_MODEL = 0      'Abbattimento delle probabilit� di morte: codice modello
    OPZ_ABB_QM_PRM_1 = 0      'Abbattimento delle probabilit� di morte: parametro 1
    OPZ_ABB_QM_PRM_2 = 0      'Abbattimento delle probabilit� di morte: parametro 2
    OPZ_ABB_QM_BASE = ""      'Abbattimento delle probabilit� di morte: nome base tecnica '20120312LC
    OPZ_ABB_QM_SUP = 0        '20130611LC (rinominato e spostato)  'ex 20130220LC
    '20080611LC (fine)
    OPZ_PRORATA_MODEL = 1       '20160415LC '20111208LC 'MODELLO DI PRORATA (1:in base all'anno)
      OPZ_PRORATA_ANNO = 1999   '20160415LC 'Anno di entrata in vigore del prorata
    '--- 20131123C (inizio)
    'OPZ_MISTO_AGEV_GIOV >= 1        'MODELLO METODO MISTO (1:usa retributivo se meno oneroso, 2:agevolazioni per i giovani, 3:retributivo minimo)
    '20160606LC (commentato)
    'OPZ_MISTO_AGEV_GIOV = 1     'METODO MISTO agevolazioni per i giovani
    OPZ_MISTO_RETRIB_MIN = 0    'METODO MISTO retributivo minimo
    '--- 20131123C (fine)
    '20120514LC (inizio)
    'OPZ_MISTO_SOGG_MODEL = 0  '20140320LC (commentato) 'METODO MISTO - SOGGETTIVO - MODELLO (0:no contr.vol. e solidar., 1:vedi documento)
      OPZ_MISTO_SOGG_VOL_A = 0       'METODO MISTO - SOGGETTIVO - CONTRIBUTO FACOLTATIVO ATTIVI (% del reddito)
      OPZ_MISTO_SOGG_VOL_P = 0       'METODO MISTO - SOGGETTIVO - CONTRIBUTO FACOLTATIVO PENSIONATI ATTIVI (% del reddito)
      '--- 20140111LC-2 (inizio)
      OPZ_MISTO_SOGG_SOL_P1 = 0#    'METODO MISTO - SOGGETTIVO - CONTRIBUTO DI SOLIDARIETA PENSIONATI NON ATTIVI (SULLA QUOTA RETRIBUTIVA)  '20121230LC
      OPZ_MISTO_SOGG_SOL_P2 = 0#    'METODO MISTO - SOGGETTIVO - CONTRIBUTO DI SOLIDARIETA PENSIONATI ATTIVI                                '20121230LC
      OPZ_MISTO_SOGG_SOL_P3 = 0#    'METODO MISTO - SOGGETTIVO - CONTRIBUTO DI SOLIDARIETA PENSIONATI DI ANZIANITA'                         '20121230LC
      OPZ_MISTO_SOGG_SOL_ANNI = 0    'METODO MISTO - SOGGETTIVO - ANNI DI CONTRIBUTO DI SOLIDARIETA
      '--- 20140111LC-2 (inizio)
    OPZ_MISTO_INTEGR_MODEL = 4     'METODO MISTO - INTEGRATIVO: MODELLO (0:no retr., 4:defin.s.pro-rata, 5:defin.c.pro-rata, 1:retr.costante, 2:retr.crescente nel tempo, 3:retr.costante per nuovi iscritti e crescente per vecchi) '20121008LC
      OPZ_MISTO_INTEGR_ALF = 0.5     'METODO MISTO - INTEGRATIVO: % FISSA DI RETROCESSIONE PER MONTANTE PENSIONISTICO
      OPZ_MISTO_INTEGR_ALR = 0.5     'METODO MISTO - INTEGRATIVO: % MAX DI RETROCESSIONE PER MONTANTE PENSIONISTICO
      OPZ_MISTO_INTEGR_ANNI = 0      'METODO MISTO - INTEGRATIVO: ANNI PER RAGGIUNGERE LA % MAX DI CONTRIBUTI INTEGRATIVI UTILI PER MONTANTE PENSIONISTICO
      'OPZ_MISTO_INTEGR_MAX = 0       'METODO MISTO - INTEGRATIVO: MASSIMALE (% DEL REDDITO se positivo, es. 2,0,  oppure % DEL SOGG MAX se negativo; es. -1,5)
    '--- 20140404LC (inizio)
    OPZ_MISTO_PMIN_MODEL = 4        '[0;1;2;3;4*]   'METODO MISTO - PENSIONE MINIMA: MODELLO (0 O 1:PRO RATA PER LA SOLA PARTE RETRIBUTIVA (obsoleto), 2:RGP 2012, 3:con pro-rata, 4:definitivo)
      OPZ_MISTO_PMIN_ABB = 0        '[0,0*-1,0]       'METODO MISTO - PENSIONE MINIMA: ABBATTIMENTO DELLA PENSIONE (ES 0,1: ABBATTE DEL 10%, OSSIA AL 90%)
      OPZ_MISTO_PMIN_NRM = 20       '[0-19;20*;>20]   'METODO MISTO - PENSIONE MINIMA: NUMERO DI REDDITI (RIVALUTATI CON L'INFLAZIONE) PER IL CALCOLO DELLA MEDIA DI CONFRONTO (0 SE NON USATA)
      OPZ_MISTO_PMIN_ART25 = -1     '[-1*;0-30]       'METODO MISTO - PENSIONE MINIMA: NUMERO ABBATTIMENTI SECONDO ART.25 (-1:RGP 2012, 0-30: specificati)
      OPZ_MISTO_PMIN_RESTMC = 2     '[0;1;2*]         'METODO MISTO - PENSIONE MINIMA: RESTITUZIONE MONTANTE CONTRIBUTI (0:no; 1:si, fino a PMIN (abb.art.25); 2:si, totale)
      OPZ_MISTO_PMIN_X4 = 55        '[0-54;55*;56-99] 'METODO MISTO - PENSIONE MINIMA: MODELLO 4, et� minima per il prorata
      OPZ_MISTO_PMIN_H4 = 20        '[0-19;20*;21-99] 'METODO MISTO - PENSIONE MINIMA: MODELLO 4, anzianit� minima per il prorata
    '--- 20140404LC (fine)
    OPZ_MISTO_REDD_PENS = 0      '20120110LC '1: media dei redditi fino a OPZ_PRORATA_ANNO-1, 0: media su tutti
    OPZ_MISTO_DEB_MAT = 0        '20120329LC '1: CALCOLO DEL DEBITO MATURATO, 0: CALCOLO NORMALE
    OPZ_DEBUG = 0                '20120601LC
    '---
    OPZ_MOD_ART_RIV_2001 = 1  '20121008LC '(NEW) MODIFICA RIVALUTAZIONI CONTRIBUTI FINO AL 2001 (0:NO, 1:SI)
    OPZ_MOD_ART_17_2 = 1           '20121008LC '(NEW) MODIFICA NORMA PER ISCRITTI ANTE 29/1/1981 NEL MISTO (0:NO SEMPRE VALIDA, 1:SOLO PER TRE ANNI)
    OPZ_MOD_ART_17_4 = 1           '(NEW) MODIFICA NORMA N� REDDITI PER PENSIONE RETRIBUTIVA NEL MISTO (0:NO 22/27, 1:SI, -1 ANNO OGNI 5 DI ANZ.)
    'OPZ_MOD_ART_26_5 = IIf(OPZ_MISTO_INTEGR_MODEL = 4, 1, 0) '20121009LC 'MODIFICA per retrocessione integrativo (interno)
    OPZ_MOD_ART_32_6 = 1           '(NEW) MODIFICA NORMA PER PRO-RATA A 70 ANNI E ANZ(2012)>=20 (0:NO, 1:SI)
    OPZ_MOD_X75 = 2                '20140404LC: 2 per PVO  'ex 20121022LC '(NEW) DISATTIVA L'ATTESA A 75 ANNI PER LA PENS. DI VECCHIAIA NEL MISTO (0:NO, 1:SI)
    '---
    OPZ_INT_BLOCKSIZE = 1000       '20121031LC '(INT) DIMENSIONE DEL BLOCCO DI RECORD DI LETTURA
    OPZ_OUTPUT_MOD = "ModOutE.XLS"  '(INT) '20121204LC
    OPZ_OUTPUT_XLS = 0         '20170125LC
    'OPZ_OUTPUT_MAX = 0        '20121220LC  '(INT)
    OPZ_INT_SUP_ZZ = 0             '(INT) '20130220LC
    OPZ_INT_SUP_RP = 0             '(INT) '20130220LC
    
    OPZ_QUOTA_A = 0             'Mod MDG
    OPZ_STERIL = 0              'Mod MDG
    OPZ_PEREQ = 0               'Mod MDG

    OPZ_INT_SUP_4 = 1              '(INT) '20130220LC (non modificabile)
    OPZ_INT_SUP_DX = 0  '0.5       '(INT) '20130611LC  '20130220LC
    '---
    OPZ_RND_MODEL = 0          '20121031LC  '(INT) MODELLO DI GENERAZIONE RANDOM DELLA PROBABILITA' (0: NO, 1;SI PER TUTTE, SI:PER SINGOLE SPECIFICATE)
    OPZ_RND_NIT = 1            '20121031LC  '(INT) MODELLO DI GENERAZIONE RANDOM: NUMERO DI ITERAZIONI DA 1 A 99)
    OPZ_RND_QAD = 0            '20121031LC  '(INT) OPZIONE DI GENERAZIONE RANDOM DELLA PROBABILITA' DI PASSAGGIO DA ATTIVO A DECEDUTO (0:NO, 1:SI)
    OPZ_RND_QED = 0            '20121031LC  '(INT) OPZIONE DI GENERAZIONE RANDOM DELLA PROBABILITA' DI PASSAGGIO DA EX-ATTIVO A DECEDUTO (0:NO, 1:SI)
    OPZ_RND_QBD = 0            '20121031LC  '(INT) OPZIONE DI GENERAZIONE RANDOM DELLA PROBABILITA' DI PASSAGGIO DA PENSIONATO DI INABILITA' A DECEDUTO (0:NO, 1:SI)
    OPZ_RND_QID = 0            '20121031LC  '(INT) OPZIONE DI GENERAZIONE RANDOM DELLA PROBABILITA' DI PASSAGGIO DA PENSIONATO DI INVALIDITA' A DECEDUTO (0:NO, 1:SI)
    OPZ_RND_QPD = 0            '20121031LC  '(INT) OPZIONE DI GENERAZIONE RANDOM DELLA PROBABILITA' DI PASSAGGIO DA PENSIONATO DIRETTO A DECEDUTO (0:NO, 1:SI)
    OPZ_RND_QAV = 0            '20150505LC  '(INT) OPZIONE DI GENERAZIONE RANDOM DELLA PROBABILITA' DI PASSAGGIO DA ATTIVO A VOLONTARIO (0:NO, 1:SI)
    OPZ_RND_QAE = 0            '20121031LC  '(INT) OPZIONE DI GENERAZIONE RANDOM DELLA PROBABILITA' DI PASSAGGIO DA ATTIVO A EX-ATTIVO (0:NO, 1:SI)
    OPZ_RND_QAB = 0            '20121031LC  '(INT) OPZIONE DI GENERAZIONE RANDOM DELLA PROBABILITA' DI PASSAGGIO DA ATTIVO A PENSIONATI DI INABILITA' (0:NO, 1:SI)
    OPZ_RND_QAI = 0            '20121031LC  '(INT) OPZIONE DI GENERAZIONE RANDOM DELLA PROBABILITA' DI PASSAGGIO DA ATTIVO A PENSIONATI DI INVALIDTITA' (0:NO, 1:SI)
    OPZ_RND_QAW = 0            '20121031LC  '(INT) OPZIONE DI GENERAZIONE RANDOM DELLA PROBABILITA' DI PASSAGGIO DA ATTIVO A DECEDUTO PER ALTRE CAUSE (0:NO, 1:SI)
    OPZ_RND_QVA = 0            '20160523LC  '(INT) OPZIONE DI GENERAZIONE RANDOM DELLA PROBABILITA' DI PASSAGGIO DA VOLONTARIO A ATTIVO  (0:NO, 1:SI)
    OPZ_RND_QEA = 0            '20160523LC  '(INT) OPZIONE DI GENERAZIONE RANDOM DELLA PROBABILITA' DI PASSAGGIO DA EX-ATTIVO A ATTIVO   (0:NO, 1:SI)
    OPZ_INT_NO_AE_AB_AI = 0        '20121113LC  '(INT)
    OPZ_INT_NO_AW = 0              '20121113LC  '(INT)
    OPZ_INT_NO_PENSATT = 0         '20121113LC  '(INT)
    OPZ_INT_NO_SUPERST = 0         '20121113LC  '(INT)
    OPZ_INT_DNASC = 0              '20130211LC  '(INT)
    OPZ_INT_ABBQ = 1               '20130216LC  '(INT)
    '--- 20130604LC (inizio)
    OPZ_MISTO_PMIN_ART25 = -1  '20130606LC
    OPZ_INT_PMIN_PANZ = 0  '20131129LC (rimodutato) pmin per anz/pva/pvu - 0:no; 1:si; 2:si, solo per pvu
    OPZ_MISTO_PMIN_RESTMC = 0
    OPZ_INT_PMIN_DEBUG = 0
    OPZ_INT_PENS_DEBUG = 0 '20160420LC
    '-- 20130919LC (inizio)
    OPZ_INT_PROPEN_ANNI = 0
    OPZ_INT_PROPEN_ALIQ = 1#
    '-- 20130919LC (inizio)
    OPZ_INT_REGMIN_MODEL = 0       '20131015LC
    'OPZ_INT_ASSIST_MODEL = 1       '20131202LC
    '--- 20130604LC (fine)
    '--- 20140317LC (inizio)
    OPZ_SUP_MODEL = 0          '[0*;1]           'Modello di calcolo per i gruppi superstiti: 0:solo se generati, 1:sempre a gruppi
    OPZ_SUP_DZ = 0             '[0*;1]          'Et� intere delle PFAM: 0:ultima et� in vita; 1:alla morte
    '--- 20140317LC (fine)
    '--- 20140319LC (inizio)
    OPZ_MISTO_CCR_MODEL = 2    '[0;1*;2;-1;-2]  '20140404LC  'MODELLO anno x coeff.conv.rend.: 0:pensionamento; 1:nascita ; 2:temporaneo poi nascita; valori negativi se si vuole usare per i supplementi l'anno di maturazione
    OPZ_MISTO_SOGG_RID_F1 = 0  '[0*;1]          'METODO MISTO - SOGGETTIVO - c.ridotti di fascia 1:  0:sempre;  1:solo se il reddito <= fascia 1
    '--- 20140319LC (fine)
'--- 20140320LC (inizio)
OPZ_EA_HMIN_MONT = 0.0001       'EX ATTIVI, ANZ. MIN PER RESTITUZIONE MONTANTE
OPZ_RC_DEC_SENZA_SUP = 0#       'RIMBORSO MONTANTE, STIMA ALIQUOTA PER EREDI DI DECEDUTI SENZA SUPERSTITI
OPZ_RC_65 = 0#                  'RIMBORSO MONTANTE, STIMA ALIQUOTA PER USCITI A 65 ANNI SENZA DIRITTO
OPZ_RC_W = 0#                   'RIMBORSO MONTANTE, STIMA ALIQUOTA PER USCITI ALTRE CAUSE
OPZ_Pveo30_19810128 = 20        'LIMITE DI ANZIANIT� PER PENSIONE DI VECCHIAIA ANTE 29/01/1981
  OPZ_PENS_ANZ_HABB = 99          'PENSIONI DI ANZIANIT�: ANZIANIT� MINIMA DI NON ABBATTIMENTO (ES: 40= ABBATTE LE PENSIONI CON H<40; 0=NO PENALIT�)
OPZ_PENS_ATT_MODEL = 1          'PENSIONATI CONTRIBUENTI: CODICE MODELLO (0:NORMALE, 1:CONTRIBUTO MINIMO)
  OPZ_PENS_ATT_ANNO = 2013      'PENSIONATI CONTRIBUENTI: ANNO INIZIALE (PER CODICE_MODELLO <> 0)
    OPZ_PENS_ATT_FRAZ = 0.5       '(NEW) PENSIONATI CONTRIBUENTI: FRAZIONE DEL MINIMO '20121022LC
'OPZ_PRORATA_MODEL = 1           '20160415LC  'MODELLO DI PRORATA (1:IN BASE ALL'ANNO)
'  OPZ_PRORATA_ANNO = 1999       '20160415LC  'ANNO DI ENTRATA IN VIGORE DEL PRORATA
OPZ_MISTO_INTEGR_MODEL = 4      'METODO MISTO - INTEGRATIVO: MODELLO (retrocessione 0:no, 4:DEFINITIVO, 1:costante, 2:crescente nel tempo, 3:costante per nuovi iscritti e crescente per vecchi)
  OPZ_MISTO_INTEGR_ALF = 0.5      'METODO MISTO - INTEGRATIVO: % FISSA DI RETROCESSIONE PER MONTANTE PENSIONISTICO
  OPZ_MISTO_INTEGR_ALR = 0.5      'METODO MISTO - INTEGRATIVO: % MAX DI RETROCESSIONE PER MONTANTE PENSIONISTICO
  OPZ_MISTO_INTEGR_ANNI = 35      'METODO MISTO - INTEGRATIVO: ANNI PER RAGGIUNGERE LA % MAX DI CONTRIBUTI INTEGRATIVI UTILI PER MONTANTE PENSIONISTICO
OPZ_MOD_ART22_MODEL = 1         'MODELLO DI MODIFICA ART.22 (0:NO, 1:SI)
OPZ_MOD_ART25_MODEL = 1         'MODELLO DI MODIFICA ART.25 (0:NO, 1:SI DAL 2009)
    OPZ_MOD_ART25_ANNO = IIf(OPZ_MOD_ART25_MODEL = 1, 2009, 9999)
    OPZ_MOD_ART25_DECR_ANZTOT = 0
OPZ_MOD_ART26_MODEL = 2         'MODELLO DI MODIFICA ART.26 (0:NO, 1:SI AL 31/12/2010, 2:SI AL 05/03/2010)
OPZ_MOD_ART_RIV_2001 = 1  'MODIFICA RIVALUTAZIONI CONTRIBUTI FINO AL 2001 (0:NO, 1:SI)
OPZ_MOD_ART_17_2 = 1            'MODIFICA NORMA PER ISCRITTI ANTE 29/1/1981 NEL MISTO (0:NO SEMPRE VALIDA, 1:SOLO PER TRE ANNI)
OPZ_MOD_ART_17_4 = 1            'MODIFICA NORMA N� REDDITI PER PENSIONE RETRIBUTIVA NEL MISTO (0:NO 22/27, 1:SI, -1 ANNO OGNI 5 DI ANZ.)
OPZ_MOD_ART_32_6 = 1            'MODIFICA NORMA PER PRO-RATA A 70 ANNI E ANZ(2012)>=20 (0:NO, 1:SI)
OPZ_MOD_X75 = 2                 '20140404LC  'ex DISATTIVA L'ATTESA A 75 ANNI PER LA PENS. DI VECCHIAIA NEL MISTO (0:NO, 1:SI)
'OPZ_MISTO_MODEL = 2             'MODELLO METODO MISTO (1:USA RETRIBUTIVO SE MENO ONEROSO, 2:AGEVOLAZIONI PER I GIOVANI, 3:RETRIBUTIVO MINIMO)
'20160606LC
OPZ_MISTO_AGEV_GIOV = 0         'MODELLO MISTO - AGEVOLAZIONI PER I GIOVANI (0:no)
OPZ_MISTO_RETRIB_MIN = 0        'MODELLO MISTO - RETRIBUTIVO MISTO (0: no (default), 1:si
OPZ_MISTO_REDD_PENS = 1         '1: MEDIA DEI REDDITI PRIMA DEL METODO MISTO, 0: MEDIA SU TUTTI
OPZ_MISTO_DEB_MAT = 0           '1: CALCOLO DEL DEBITO MATURATO, 0: CALCOLO NORMALE
'OPZ_MISTO_SOGG_MODEL = 0        'METODO MISTO - SOGGETTIVO - MODELLO (0:NO CONTR.VOL. E SOLIDAR., 1:VEDI DOCUMENTO)
'--- 20140320LC (fine)
    '--- 20140605LC (inizio)
    OPZ_INT_CV21 = 0
    OPZ_INT_CV25 = 1
    '--- 20140605LC (fine)
    '--- 20150223LC
    OPZ_TEMP_ALR_NI = 0         '[0*-1,0] '% di retrocessione nuovi ingressi: 0 come da regolanento, altrimenti � la frazione (es. 0,75 -> 3% di 4%)
    '--- 20150522LC (inizio)
    OPZ_INT_MONT_CALC = 1
    '--- 20150522LC (fine)
    OPZ_MASSIMALE_2011 = 1        '20150615LC  '[0;1*] STERILIZZAZIONE MASSIMALI AL 2011: 1-si(default), altro-no
    OPZ_OUT_SIL_ATT = 2           '20160222LC  '[0,1,2*] numero di generazioni di silenti da considerare come attivi in output
    OPZ_FRAZ_COMP = 1  '0.75      '20160306LC  '[0,00-0,75-1,00*]  Frazione dei contributi che sono incassati nell'anno
    '--- 20160306LC  'ex 20150504LC
    OPZ_INT_AZZERA_REDDITO = -1   '[0*,1,2] azzeramento reddito pens.att. ultimo anno: 1:totale (12 mesi), 2:parziale (ultimi 3 mesi), 0:no
    OPZ_ART14_90 = 90             '20160308LC  '[0-90*-255] pensione anticipata: quota minima
    OPZ_ART14_5 = 0.05            '20160308LC  '[0-0,05*-1] pensione anticipata: penalizzazione annua
    OPZ_ART16_2 = 0.02            '20160308LC  '[0-0,02*-1] rendita contributiva: penalizzazione annua
    OPZ_ART19_3 = 3               '20160309LC  '[0-3*-5] pensione di invalidit�: anz.contr. mimima nell'ultimo quinquennio
    OPZ_ART20_1 = 1               '20160309LC  '[0-1*-5] pensione di inabilit�:  anz.contr. minima nell'ultimo quinquennio
    OPZ_ART23_1 = 1               '20160420LC  '[0-1*-5] pensione indiretta:  anz.contr. minima nell'ultimo quinquennio
    OPZ_DBG_PENSIONE = 0#         '20160312LC
    OPZ_INT_PENSIONE = 0#         '20160312LC
    OPZ_DBG_INCRPENS = 0#         '20160312LC
    OPZ_INT_INCRPENS = 0#         '20160312LC
    OPZ_TOLL_PENS = 0#            '20160420LC
    OPZ_SIL_PVEP75 = 0            '20160422LC
    '--- 20161112LC (inizio)
    OPZ_MOD_REG_1A = 0
      OPZ_MOD_REG_1_CV = ""  '20170307LC
      OPZ_MOD_REG_1_ANNI = 0
    OPZ_MOD_REG_2 = 0                '[0*;1;2] Modifica 2 (0:no; 1:s�,contr.sogg.; 2:s�,contr.sogg.+ass.)
      OPZ_MOD_REG_2_CV = ""  '20170307LC
      OPZ_MOD_REG_2_AMIN = 0  '20161116LC-3
      OPZ_MOD_REG_2_ANNO = 0
      OPZ_MOD_REG_2_ANNI = 0
      OPZ_MOD_REG_2_CIFRE_TOLL = 0
      OPZ_MOD_REG_2_NITMAX = 0
      OPZ_MOD_REG_2_PERC_PIL5 = 0#
      OPZ_MOD_REG_2_RIV_MAX = 0#  '20180306LC  'ex 20161114LC
    OPZ_MOD_REG_3 = 0
      OPZ_MOD_REG_3_CV = ""  '20170307LC
      OPZ_MOD_REG_3_ANNO = 0
      OPZ_MOD_REG_3_PERC_ENAS = 1#
    '--- 20161112LC (fine)
    '--- 20161126LC (inizio)
    OPZ_PENS_MODEL = 0
    OPZ_PENS_C0 = 0            '20161126LC  '[0,0-0,5*-1,0]  'Coeff. di interp. dei pensionati NP0 per la spesa pensionistica SP = C0*NP0 +(1-C0)*NP1
    OPZ_PENS_SP0 = 0           '20161116LC-3
    '--- 20161126LC (fine)
    '--- 20170724LC (inizio)
    OPZ_RC_MODEL = 0
    OPZ_RC_ADEC = 2012
    OPZ_RC_FRAZ_ATT = 1#
    OPZ_RC_FRAZ_SIL = 1#
    '--- 20170724LC (fine)
    OPZ_RIATT_AMIN = 0  'Anno minimo di riattivazione dei silenti
    '***************************
    'Inizializza le opzioni ENEA
    '***************************
    '--- 20160306LC (inizio)  'commentato
    'OPZ_ENEA_MAT = 8814688
    'OPZ_ENEA_QAD1 = 0#
    'OPZ_ENEA_QED1 = 0#
    'OPZ_ENEA_QPD1 = 0#
    'OPZ_ENEA_QAE2 = 0#
    'OPZ_ENEA_QAB2 = 0#
    'OPZ_ENEA_QAI2 = 0#
    '--- 20160306LC (fine)  'commentato
    '*******************************
    'Legge le opzioni da AFP_OPZ.INI
    '*******************************
    If ON_ERR_RN Then On Error Resume Next
    fi = FreeFile
    Open "AFP_OPZ.INI" For Input As #fi
    '20120319LC (inizio)
    a$ = Input(LOF(fi), fi)
    Close #fi
    .OpzIni = Split(a$, vbCrLf)
    'Do Until EOF(fi)
    For ir = 0 To UBound(.OpzIni, 1)
      'Line Input #fi, a$
      a$ = .OpzIni(ir)
      a$ = UCase(Trim(a$))
      .OpzIni(ir) = a$
    '20120319LC (fine)
      i = InStr(a$, "'")
      If i > 0 Then a$ = Left(a$, i - 1)
      a$ = Trim(a$)  '20131122LC
      i = InStr(a$, "=")
      If i > 0 Then
        b$ = Trim(Right(a$, Len(a$) - i))  '20131122LC
        '20121009LC (inizio)
        a$ = Trim(Left(a$, i - 1))
        Select Case a$
        '20121009LC (fine)
        '*********************
        'Legge le opzioni ENEA
        '*********************
        '--- 20160306LC (inizio) 'commentato  'ex 20150522LC (ENEA)
        'Case "OPZ_ENEA_MAT"
        '  OPZ_ENEA_MAT = Val(b)
        'Case "OPZ_ENEA_QPD1"
        '  If IsNumeric(b$) Then
        '    OPZ_ENEA_QPD1 = CDbl(b)
        '  End If
        'Case "OPZ_ENEA_QAD1"
        '  If IsNumeric(b$) Then
        '    OPZ_ENEA_QAD1 = CDbl(b)
        '  End If
        'Case Is = "OPZ_ENEA_QED1"
        '  If IsNumeric(b$) Then
        '    OPZ_ENEA_QED1 = CDbl(b)
        '  End If
        'Case "OPZ_ENEA_QAE2"
        '  If IsNumeric(b$) Then
        '    OPZ_ENEA_QAE2 = CDbl(b)
        '  End If
        'Case "OPZ_ENEA_QAB2"
        '  If IsNumeric(b$) Then
        '    OPZ_ENEA_QAB2 = CDbl(b)
        '  End If
        'Case Is = "OPZ_ENEA_QAI2"
        '  If IsNumeric(b$) Then
        '    OPZ_ENEA_QAI2 = CDbl(b)
        '  End If
        '--- 20160306LC (fine) 'commentato  'ex 20150522LC (ENEA)
        '********************
        'Legge le opzioni AFP
        '********************
        Case "$$OPZ_EA_HMIN_MONT"  '20140320LC
          OPZ_EA_HMIN_MONT = b$
        Case "$$OPZ_RC_DEC_SENZA_SUP"  '20140320LC
          OPZ_RC_DEC_SENZA_SUP = b$
        Case "$$OPZ_RC_65"  '20140320LC
          OPZ_RC_65 = b$
        Case "$$OPZ_RC_W"  '20140320LC
          OPZ_RC_W = b$
        '20120302LC (inizio)
        Case "$$OPZ_PENS_ANZ_MODEL"  '20140320LC
          If Val(b$) >= -1 And Val(b$) <= 2 Then
            OPZ_PENS_ANZ_MODEL = Val(b$)
            If OPZ_PENS_ANZ_MODEL = 0 Then
              OPZ_PENS_ANZ_MODEL = 1
            End If
          End If
        Case "$$OPZ_PENS_ANZ_HABB"  '20140320LC
          OPZ_PENS_ANZ_HABB = b$
        Case "OPZ_PENS_ATT_MODEL"
          If Val(b$) = 0 Or Val(b$) = 1 Then  '20140320LC  'ex 20121008LC
            OPZ_PENS_ATT_MODEL = Val(b$)
          End If
        Case "OPZ_PENS_ATT_ANNO"  '20140320LC
          OPZ_PENS_ATT_ANNO = Val(b$)
        '20120302LC (fine)
        '20121008LC (inizio)
        Case "OPZ_PENS_ATT_FRAZ"  '20140320LC
          If IsNumeric(b$) Then
            If CDbl(b$) >= 0# And CDbl(b$) <= 1# Then
              OPZ_PENS_ATT_FRAZ = CDbl(b$)
            End If
          End If
        '20121008LC (fine)
        Case "OPZ_EA_ABBQ_REDDMIN"
          OPZ_EA_ABBQ_REDDMIN = b$
        Case "OPZ_EA_ABBQ_PERC"
          OPZ_EA_ABBQ_PERC = b$
        Case "OPZ_CV_NOME"  '20160419LC-2  'ex 20140320LC
          OPZ_CV_NOME = b$
        Case "$$OPZ_Pveo30_19810128"  '20140320LC  'ex 20080207LC
          OPZ_Pveo30_19810128 = Val(b$)
        '20080416LC (inizio)
        Case "OPZ_ELAB_CODA"
          OPZ_ELAB_CODA = IIf(Val(b$) <> 0, 100, 0)
        '20080416LC (fine)
        '****************
        'OPZ_ABB_QM_MODEL
        '20080611LC
        '****************
        Case "OPZ_ABB_QM_MODEL"
          OPZ_ABB_QM_MODEL = Val(b$)
        '20120312LC (inizio)
        Case "OPZ_ABB_QM_PRM_1"
          If OPZ_ABB_QM_MODEL = 2 Then
            If CDbl(b$) > 0 And CDbl(b$) <= 2 Then
              OPZ_ABB_QM_PRM_1 = CDbl(b$)
            End If
          ElseIf OPZ_ABB_QM_MODEL <> 0 Then
            If CDbl(b$) > 0 And CDbl(b$) < 1 Then
              OPZ_ABB_QM_PRM_1 = CDbl(b$)
            End If
          End If
        Case "OPZ_ABB_QM_PRM_2"
          If OPZ_ABB_QM_MODEL = 2 Then
          ElseIf OPZ_ABB_QM_MODEL <> 0 Then
            If CDbl(b$) > 0 And CDbl(b$) < 1 Then
              OPZ_ABB_QM_PRM_2 = CDbl(b$)
            End If
          End If
        Case "OPZ_ABB_QM_BASE"
          OPZ_ABB_QM_BASE = b$
        '20120312LC (fine)
        Case "OPZ_INT_SUP_2"  '20160415LC (commentato)
        Case "OPZ_ABB_QM_SUP"  '20130611LC (rinominato e spostato) 'ex 20130220LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 199 And OPZ_ABB_QM_MODEL <> 0 Then  '20140215LC  'riportare 20140111LC-22
            OPZ_ABB_QM_SUP = Int(Val(b$))
          End If
        '**********************
        'OPZ_SUDDIVISIONI_MODEL
        '**********************
        '--- 20140603LC (inizio)
        Case "OPZ_SUDDIVISIONI_MODEL"
          OPZ_SUDDIVISIONI_MR = 0
          OPZ_SUDDIVISIONI_DT = 0  '20140603LC
          OPZ_SUDDIVISIONI_MODEL = Val(b$)
          If OPZ_SUDDIVISIONI_MODEL > 0 Then
            If OPZ_SUDDIVISIONI_MODEL > 99 Then '20120331LC
              OPZ_SUDDIVISIONI_MODEL = 99 '20120331LC
            End If
            ReDim OPZ_INT_SUDDIVISIONI_nomi(OPZ_SUDDIVISIONI_MODEL - 1) As Variant  '20140603LC  'ex +1
            '20080619LC (inizio)
            'OPZ_INT_SUDDIVISIONI_nomi(0) = "Risultati"
            For i = 0 To OPZ_SUDDIVISIONI_MODEL - 1
              OPZ_INT_SUDDIVISIONI_nomi(i) = "R." & i
            Next i
            '20080619LC (fine)
          ElseIf OPZ_SUDDIVISIONI_MODEL = -1 Then 'gruppo
            OPZ_INT_SUDDIVISIONI_nomi = Array("R.att", "R.pa", "R.pna", "R.rev", "R.ea")
          ElseIf OPZ_SUDDIVISIONI_MODEL = -2 Then 'gruppo e sesso
            OPZ_INT_SUDDIVISIONI_nomi = Array("R.attM", "R.paM", "R.pnaM", "R.supM", "R.eaM", "R.attF", "R.paF", "R.pnaF", "R.supF", "R.eaF")
          ElseIf OPZ_SUDDIVISIONI_MODEL = -3 Then 'gruppo e titolo
            OPZ_INT_SUDDIVISIONI_nomi = Array("R.attA", "R.paA", "R.pnaA", "R.supA", "R.eaA", "R.attI", "R.paI", "R.pnaI", "R.supI", "R.eaI")
          ElseIf OPZ_SUDDIVISIONI_MODEL = -4 Then 'sesso
            OPZ_INT_SUDDIVISIONI_nomi = Array("R.M", "R.F")
          ElseIf OPZ_SUDDIVISIONI_MODEL = -5 Then 'sesso e titolo
            OPZ_INT_SUDDIVISIONI_nomi = Array("R.AM", "R.AF", "R.IM", "R.IF")
          ElseIf OPZ_SUDDIVISIONI_MODEL = -6 Then 'titolo
            OPZ_INT_SUDDIVISIONI_nomi = Array("R.arch", "R.ing")
          ElseIf OPZ_SUDDIVISIONI_MODEL = -7 Then 'classi di et� quinquennali
            OPZ_INT_SUDDIVISIONI_nomi = Array("R.<30", "R.30-34", "R.35-39", "R.40-44", "R.45-49", "R.50-54", "R.55-59", "R.60-64", "R.65-69", "R.>=70")
          ElseIf OPZ_SUDDIVISIONI_MODEL = -8 Then 'classi di et� decennali
            OPZ_INT_SUDDIVISIONI_nomi = Array("R.<35", "R.35-44", "R.45-54", "R.55-64", "R.>=65")
          ElseIf OPZ_SUDDIVISIONI_MODEL = -9 Then 'classi di et� decennali e sesso
            OPZ_INT_SUDDIVISIONI_nomi = Array("R.<35M", "R.35-44M", "R.45-54M", "R.55-64M", "R.>=65M", "R.<35F", "R.35-44F", "R.45-54F", "R.55-64F", "R.>=65F")
          ElseIf OPZ_SUDDIVISIONI_MODEL = -10 Then 'classi di et� decennali e sesso
            OPZ_INT_SUDDIVISIONI_nomi = Array("R.<35A", "R.35-44A", "R.45-54A", "R.55-64A", "R.>=65A", "R.<35I", "R.35-44I", "R.45-54I", "R.55-64I", "R.>=65I")
          ElseIf OPZ_SUDDIVISIONI_MODEL = -11 Then 'Classi di et� annuali '20120331LC 'ex 20111224LC
            OPZ_INT_SUDDIVISIONI_nomi = Array("x25-", "x26", "x27", "x28", "x29", _
                                           "x30", "x31", "x32", "x33", "x34", "x35", "x36", "x37", "x38", "x39", _
                                           "x40", "x41", "x42", "x43", "x44", "x45", "x46", "x47", "x48", "x49", _
                                           "x50", "x51", "x52", "x53", "x54", "x55", "x56", "x57", "x58", "x59", _
                                           "x60", "x61", "x62", "x63", "x64", "x65", "x66", "x67", "x68", "x69", _
                                           "x70", "x71", "x72", "x73", "x74+")
          ElseIf OPZ_SUDDIVISIONI_MODEL = -12 Then 'Classi di anzianit� annuali '20120331LC
            OPZ_INT_SUDDIVISIONI_nomi = Array("h0", "h1", "h2", "h3", "h4", "h5", "h6", "h7", "h8", "h9", _
                                          "h10", "h11", "h12", "h13", "h14", "h15", "h16", "h17", "h18", "h19", _
                                          "h20", "h21", "h22", "h23", "h24", "h25", "h26", "h27", "h28", "h29", _
                                          "h30", "h31", "h32", "h33", "h34", "h35", "h36", "h37", "h38", "h39+")
          'ElseIf OPZ_SUDDIVISIONI_MODEL = -13 Then 'classi di reddito
          '  OPZ_INT_SUDDIVISIONI_nomi = Array("R.<min", "R.3�inf", "R.3�cen", "R.3�sup", "R.>=max")
          'ElseIf OPZ_SUDDIVISIONI_MODEL = -14 Then 'classi di reddito e sesso
          '  OPZ_INT_SUDDIVISIONI_nomi = Array("R.<minM", "R.3�infM", "R.3�cenM", "R.3�supM", "R.>=maxM", "R.<minF", "R.3�infF", "R.3�cenF", "R.3�supF", "R.>=maxF")
          'ElseIf OPZ_SUDDIVISIONI_MODEL = -15 Then 'classi di reddito e titolo
          '  OPZ_INT_SUDDIVISIONI_nomi = Array("R.<minA", "R.3�infA", "R.3�cenA", "R.3�supA", "R.>=maxA", "R.<minI", "R.3�infI", "R.3�cenI", "R.3�supI", "R.>=maxI")
          Else
            OPZ_SUDDIVISIONI_MODEL = 0
            OPZ_INT_SUDDIVISIONI_nomi = Array("")
          End If
          OPZ_INT_SUDDIVISIONI_N = UBound(OPZ_INT_SUDDIVISIONI_nomi, 1) + 1
        Case "OPZ_SUDDIVISIONI_MR"
          If OPZ_SUDDIVISIONI_MODEL <> 0 Then
            If Val(b$) >= 1 And Val(b$) <= 3 Then
              '1: per numerosit�
              OPZ_SUDDIVISIONI_MR = Val(b$)
            Else
              OPZ_SUDDIVISIONI_MR = 0
            End If
          End If
        Case "OPZ_SUDDIVISIONI_DT"
          'If OPZ_SUDDIVISIONI_MODEL <> 0 Then  '20140609LC
            If Val(b$) >= 1 And Val(b$) <= 99 Then
              OPZ_SUDDIVISIONI_DT = Val(b$)
            End If
          'End If
        '--- 20140603LC (fine)
        Case "OPZ_OMEGA" '20080926LC
          If Val(b$) >= 100 And Val(b$) <= 150 Then
            OPZ_OMEGA = Val(b$)
          End If
        '---
        Case "OPZ_INTERP_H"  '20111217LC
          If Val(b$) >= 0 And Val(b$) <= 3 Then
            OPZ_INTERP_H = Val(b$)
          End If
        Case "OPZ_INTERP_X"  '20111217LC
          If Val(b$) >= 0 And Val(b$) <= 3 Then
            OPZ_INTERP_X = Val(b$)
          End If
        '---
        Case "OPZ_PRORATA_MODEL"  '20150307LC (sbloccato)  'ex $$OPZ_PRORATA_MODEL 20140320LC  'ex 20111208LC
          '--- 20160415LC (inizio)
          If Val(b$) >= 0 And Val(b$) <= 1 Then
            OPZ_PRORATA_MODEL = Val(b$)
            If OPZ_PRORATA_MODEL = 0 Then
              OPZ_PRORATA_ANNO = 9999
            End If
          End If
          '--- 20160415LC (fine)
        Case "OPZ_PRORATA_ANNO"  '20150307LC (sbloccato)  'ex $$OPZ_PRORATA_ANNO 20140320LC  'ex 20111208LC
          If OPZ_PRORATA_MODEL <> 0 Then
            If Val(b$) >= 1 And Val(b$) <= 9999 Then
              OPZ_PRORATA_ANNO = Val(b$)
            End If
          End If
        Case "$$OPZ_MISTO_MODEL"  '20140320LC  'ex 20131123LC  'ex 20111213LC
          If Val(b$) = 2 Then
            OPZ_MISTO_AGEV_GIOV = 1
          ElseIf Val(b$) = 3 Then
            OPZ_MISTO_RETRIB_MIN = 1
          End If
        Case "$$OPZ_MISTO_AGEV_GIOV"  '20140320LC  'ex 20131123LC
          If Val(b$) >= 1 And Val(b$) <= 2 Then
            OPZ_MISTO_AGEV_GIOV = Val(b$)
          End If
        Case "$$OPZ_MISTO_RETRIB_MIN"  '20140320LC  '20131123LC
          If Val(b$) >= 1 And Val(b$) <= 1 Then
            OPZ_MISTO_RETRIB_MIN = Val(b$)
          End If
        '20120514LC (inizio)
        'Case "OPZ_MISTO_SOGG_MODEL"  '20140320LC
        '  If Val(b$) >= 0 And Val(b$) <= 1 Then
        '    OPZ_MISTO_SOGG_MODEL = Val(b$)
        '  End If
        Case "OPZ_MISTO_SOGG_VOL_A"
          'If OPZ_MISTO_SOGG_MODEL > 0 And CDbl(b$) > 0 Then  '20140320LC
            OPZ_MISTO_SOGG_VOL_A = CDbl(b$)
          'End If
        Case "OPZ_MISTO_SOGG_VOL_P"
          'If OPZ_MISTO_SOGG_MODEL > 0 And CDbl(b$) > 0 Then  '20140320LC
            OPZ_MISTO_SOGG_VOL_P = CDbl(b$)
          'End If
        Case "OPZ_MISTO_SOGG_SOL_P1"
          'If OPZ_MISTO_SOGG_MODEL > 0 And CDbl(b$) > 0 Then  '20140320LC
            OPZ_MISTO_SOGG_SOL_P1 = CDbl(b$)
          'End If
        Case "OPZ_MISTO_SOGG_SOL_P2"  '20121230LC
          'If OPZ_MISTO_SOGG_MODEL > 0 And CDbl(b$) > 0 Then  '20140320LC
            OPZ_MISTO_SOGG_SOL_P2 = CDbl(b$)
          'End If
        Case "OPZ_MISTO_SOGG_SOL_P3"  '20121230LC
          'If OPZ_MISTO_SOGG_MODEL > 0 And CDbl(b$) > 0 Then  '20140320LC
            OPZ_MISTO_SOGG_SOL_P3 = CDbl(b$)
          'End If
        Case "OPZ_MISTO_SOGG_SOL_ANNI"  '20140320LC
          'If OPZ_MISTO_SOGG_MODEL > 0 And CDbl(b$) > 0 Then
            OPZ_MISTO_SOGG_SOL_ANNI = CDbl(b$)
          'End If
        '---
        Case "$$OPZ_MISTO_INTEGR_MODEL"  '20140320LC
          If Val(b$) >= 0 And Val(b$) <= 4 Then '20121009LC
            OPZ_MISTO_INTEGR_MODEL = Val(b$)
          End If
        Case "$$OPZ_MISTO_INTEGR_ALF"  '20140320LC
          If OPZ_MISTO_INTEGR_MODEL > 0 And OPZ_MISTO_INTEGR_MODEL < 4 Then '20121009LC
            OPZ_MISTO_INTEGR_ALF = CDbl(b$)
          ElseIf OPZ_MISTO_INTEGR_MODEL = 4 Then '20130620LC
            OPZ_MISTO_INTEGR_ALF = 0
          End If
        Case "$$OPZ_MISTO_INTEGR_ALR"  '20140320LC
          If OPZ_MISTO_INTEGR_MODEL > 0 Then
            OPZ_MISTO_INTEGR_ALR = CDbl(b$)
          End If
        Case "$$OPZ_MISTO_INTEGR_ANNI"  '20140320LC
          If OPZ_MISTO_INTEGR_MODEL > 1 And OPZ_MISTO_INTEGR_MODEL < 4 Then '20121009LC
            If Val(b$) >= 1 And Val(b$) <= 99 Then
              OPZ_MISTO_INTEGR_ANNI = CDbl(b$)
            End If
          ElseIf OPZ_MISTO_INTEGR_MODEL = 4 Then '20130620LC
            OPZ_MISTO_INTEGR_ANNI = 0
          End If
        'Case "OPZ_MISTO_INTEGR_MAX"
        '  If OPZ_MISTO_INTEGR_MODEL > 0 Then
        '    OPZ_MISTO_INTEGR_MAX = CDbl(b$)
        '  End If
        '--- 20140404LC (inizio)
        Case "OPZ_MISTO_PMIN_MODEL"
          If Val(b$) >= 0 And Val(b$) <= 4 Then
            OPZ_MISTO_PMIN_MODEL = Val(b$)
          End If
        Case "OPZ_MISTO_PMIN_ABB"
          If OPZ_MISTO_PMIN_MODEL >= 2 Then
            If CDbl(b$) >= 0 And CDbl(b$) <= 1 Then
              OPZ_MISTO_PMIN_ABB = CDbl(b$)
            End If
          End If
        Case "OPZ_MISTO_PMIN_NRM"
          If OPZ_MISTO_PMIN_MODEL >= 2 Then
            If Val(b$) >= 0 And Val(b$) <= 99 Then
              OPZ_MISTO_PMIN_NRM = Val(b$)
            End If
          End If
        Case "OPZ_MISTO_PMIN_ART25"
          If Int(Val(b$)) >= -1 And Int(Val(b$)) <= 30 Then
            OPZ_MISTO_PMIN_ART25 = Int(Val(b$))
          End If
        Case "OPZ_MISTO_PMIN_RESTMC"
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 2 Then
            OPZ_MISTO_PMIN_RESTMC = Int(Val(b$))
          End If
        Case "OPZ_MISTO_PMIN_X4"
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 99 Then
            OPZ_MISTO_PMIN_X4 = Int(Val(b$))
          End If
        Case "OPZ_MISTO_PMIN_H4"
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 99 Then
            OPZ_MISTO_PMIN_H4 = Int(Val(b$))
          End If
        '---
        Case "OPZ_INT_PMIN_PANZ"  '20130606LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 2 Then  '20131129LC
            OPZ_INT_PMIN_PANZ = Int(Val(b$))
          End If
        Case "OPZ_INT_PMIN_DEBUG"  '20130606LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 2 Then
            OPZ_INT_PMIN_DEBUG = Int(Val(b$))
          End If
        Case "OPZ_INT_PENS_DEBUG"  '20160420LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 Then
            OPZ_INT_PENS_DEBUG = Int(Val(b$))
          End If
        '--- 20140404LC (fine)
        Case "$$OPZ_MISTO_REDD_PENS"  '20140320LC  'ex 20120110LC
          If Val(b$) >= 0 And Val(b$) <= 2 Then
            OPZ_MISTO_REDD_PENS = Val(b$)
          End If
        Case "$$OPZ_MISTO_DEB_MAT"  '20140320LC  'ex 20120329LC
          If Val(b$) >= 0 And Val(b$) <= 1 Then
            OPZ_MISTO_DEB_MAT = Val(b$)
          End If
        '---
        Case "$$OPZ_MOD_ART22_MODEL"  '20140320LC  'ex 20080709LC
          OPZ_MOD_ART22_MODEL = Val(b$)
          If OPZ_MOD_ART22_MODEL = 1 Then
            OPZ_MOD_ART22_ANNO = 2009
          Else
            OPZ_MOD_ART22_ANNO = 9999
          End If
        Case "$$OPZ_MOD_ART25_MODEL"  '20140320LC  'ex 20121010LC
          If Val(b$) >= 0 And Val(b$) <= 1 Then
            OPZ_MOD_ART25_MODEL = Val(b$)
            If OPZ_MOD_ART25_MODEL = 1 Then
              OPZ_MOD_ART25_ANNO = 2009
            Else
              OPZ_MOD_ART25_ANNO = 9999
            End If
            OPZ_MOD_ART25_DECR_ANZTOT = 0
          End If
        Case "$$OPZ_MOD_ART26_MODEL"  '20140320LC  'ex 20121010LC
          If Val(b$) >= 0 And Val(b$) <= 2 Then
            OPZ_MOD_ART26_MODEL = Val(b$)
          End If
        Case "$$OPZ_MOD_ART_RIV_2001"  '20140320LC  'ex 20121008LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 Then
            OPZ_MOD_ART_RIV_2001 = Int(Val(b$))
          End If
        Case "$$OPZ_MOD_ART_17_2"  '20140320LC  'ex 20121008LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 Then
            OPZ_MOD_ART_17_2 = Int(Val(b$))
          End If
        Case "$$OPZ_MOD_ART_17_4"  '20140320LC  'ex 20121009LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 Then
            OPZ_MOD_ART_17_4 = Int(Val(b$))
          End If
        Case "$$OPZ_MOD_ART_32_6"  '20140320LC  'ex 20121022LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 Then
            OPZ_MOD_ART_32_6 = Int(Val(b$))
          End If
        Case "OPZ_MOD_X75"  '20121022LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 Then
            OPZ_MOD_X75 = Int(Val(b$))
          End If
        '---
        Case "OPZ_INT_BLOCKSIZE"  '20121030LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 2 ^ 31 - 1 Then
            OPZ_INT_BLOCKSIZE = Int(Val(b$))
          End If
        'Case "OPZ_OUTPUT_MAX"  '20121220LC (commentato)
        '  If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 Then
        '    OPZ_OUTPUT_MAX = Val(b$)
        '  End If
        Case "OPZ_OUTPUT_MOD"  '20121204LC
          OPZ_OUTPUT_MOD = b$
        Case "OPZ_OUTPUT_XLS"  '20170125LC
          y = Fix(Val(b$))
          If y = 0 Or y = 1 Then
            OPZ_OUTPUT_XLS = y
          End If
        Case "OPZ_INT_SUP_1"  '20160415LC (inibito)
        Case "OPZ_INT_SUP_ZZ"  '20130220LC (ex OPZ_INT_SUP_1)
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 Then
            OPZ_INT_SUP_ZZ = Int(Val(b$))
          End If
        Case "OPZ_INT_SUP_3"  '20160415LC (inibito)
        Case "OPZ_INT_SUP_RP"  '20130220LC (ex OPZ_INT_SUP_3)
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 2 Then
            OPZ_INT_SUP_RP = Int(Val(b$))
          End If
        Case "OPZ_QUOTA_A"  '20130220LC (ex OPZ_INT_SUP_3)
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 Then
            OPZ_QUOTA_A = Int(Val(b$))
          End If
        Case "OPZ_STERIL"  '20130220LC (ex OPZ_INT_SUP_3)
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 Then
            OPZ_STERIL = Int(Val(b$))
          End If
        Case "OPZ_PEREQ"  '20130220LC (ex OPZ_INT_SUP_3)
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 Then
            OPZ_PEREQ = Int(Val(b$))
          End If
        'Case "OPZ_DBXMEF"  '20130220LC (ex OPZ_INT_SUP_3)
        '  If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 Then
        '    OPZ_DBXMEF = Int(Val(b$))
        '  End If
        Case "OPZ_INT_SUP_4"  '20160415LC (inibito)  '20130220LC
          'If Int(Val(b$)) >= 1 And Int(Val(b$)) <= 2 Then
          '  OPZ_INT_SUP_4 = Int(Val(b$))
          'End If
        Case "OPZ_INT_SUP_DX"  '20130220LC
          'If Val(b$) >= 0 And Val(b$) <= 1 And 1 = 2 Then '20140317LC
          '  OPZ_INT_SUP_DX = CDbl(b$)
          'End If
        '---
        Case "OPZ_RND_MODEL"  '20121030LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 2 Then
            OPZ_RND_MODEL = Val(b$)
          End If
        Case "OPZ_RND_NIT"  '20121030LC
          If Int(Val(b$)) >= 1 And Int(Val(b$)) <= 9999 Then
            OPZ_RND_NIT = Val(b$)
          End If
        Case "OPZ_RND_QAD"  '20121030LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 Then
            OPZ_RND_QAD = Val(b$)
          End If
        Case "OPZ_RND_QED"  '20121030LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 Then
            OPZ_RND_QED = Val(b$)
          End If
        Case "OPZ_RND_QBD"  '20121030LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 Then
            OPZ_RND_QBD = Val(b$)
          End If
        Case "OPZ_RND_QID"  '20121030LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 Then
            OPZ_RND_QID = Val(b$)
          End If
        Case "OPZ_RND_QPD"  '20121030LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 Then
            OPZ_RND_QPD = Val(b$)
          End If
        Case "OPZ_RND_QAV"  '20150505LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 Then
            OPZ_RND_QAV = Val(b$)
          End If
        Case "OPZ_RND_QAE"  '20121030LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 Then
            OPZ_RND_QAE = Val(b$)
          End If
        Case "OPZ_RND_QAB"  '20121030LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 Then
            OPZ_RND_QAB = Val(b$)
          End If
        Case "OPZ_RND_QAI"  '20121030LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 Then
            OPZ_RND_QAI = Val(b$)
          End If
        Case "OPZ_RND_QAW"  '20121030LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 Then
            OPZ_RND_QAW = Val(b$)
          End If
        Case "OPZ_RND_QVA"  '20160523LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 Then
            OPZ_RND_QVA = Val(b$)
          End If
        Case "OPZ_RND_QEA"  '20160523LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 Then
            OPZ_RND_QEA = Val(b$)
          End If
        Case "OPZ_INT_NO_AE_AB_AI"  '20121113LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 Then
            OPZ_INT_NO_AE_AB_AI = Val(b$)
          End If
        Case "OPZ_INT_NO_AW"  '20121113LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 Then
            OPZ_INT_NO_AW = Val(b$)
          End If
        Case "OPZ_INT_NO_PENSATT"  '20121113LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 Then
            OPZ_INT_NO_PENSATT = Val(b$)
          End If
        Case "OPZ_INT_NO_SUPERST"  '20121113LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 Then
            OPZ_INT_NO_SUPERST = Val(b$)
          End If
        Case "OPZ_INT_DNASC"  '20130211LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 31 Then
            OPZ_INT_DNASC = Int(Val(b$))
          End If
        Case "OPZ_INT_ABBQ"  '20130216LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 Then
            OPZ_INT_ABBQ = Int(Val(b$))
          End If
        '--- 20130604LC (inizio)
        Case "OPZ_MISTO_INTEGR_MAX"  '20130606LC
          'Per bypassare il warning
        '--- 20130604LC (fine)
        Case "OPZ_INT_PROPEN_ANNI"  '20130919LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 4 Then
            OPZ_INT_PROPEN_ANNI = Int(Val(b$))
          End If
        Case "OPZ_INT_PROPEN_ALIQ"  '20130919LC
          If CDbl(b$) >= 0 And CDbl(b$) <= 1 Then
            OPZ_INT_PROPEN_ALIQ = CDbl(b$)
          End If
        Case "OPZ_INT_REGMIN_MODEL"  '20131015LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 2 Then
            OPZ_INT_REGMIN_MODEL = Val(b$)
          End If
        Case "OPZ_INT_CV21"  '20140605LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 Then
            OPZ_INT_CV21 = Val(b$)
          End If
        Case "OPZ_INT_CV25"  '20140605LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 Then
            OPZ_INT_CV25 = Val(b$)
          End If
        Case "OPZ_TEMP_ALR_NI"  '20150223LC
          If IsNumeric(b$) = True Then
            If CDbl(b$) >= 0 And CDbl(b$) <= 1 Then
              OPZ_TEMP_ALR_NI = CDbl(b$)
            End If
          End If
        'Case "OPZ_INT_ASSIST_MODEL"  '20131122LC
        '  If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 Then
        '    OPZ_INT_ASSIST_MODEL = Val(b$)
        '  End If
        Case "OPZ_SUP_DZ"  '20140317LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 Then
            OPZ_SUP_DZ = Int(Val(b$))
          End If
        Case "OPZ_SUP_MODEL"  '20140424LC
          If Int(Val(b$)) >= -1 And Int(Val(b$)) <= 1 Then
            OPZ_SUP_MODEL = Int(Val(b$))
          End If
        Case "OPZ_MISTO_CCR_MODEL"  '20140404LC  'ex 20140319LC
          If Math.Abs(Int(Val(b$))) <= 2 Then
            OPZ_MISTO_CCR_MODEL = Int(Val(b$))
          End If
        Case "OPZ_MISTO_SOGG_RID_F1"  '20140319LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 Then
            OPZ_MISTO_SOGG_RID_F1 = Int(Val(b$))
          End If
        Case "OPZ_MASSIMALE_2011"  '20150615LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 Then
            OPZ_MASSIMALE_2011 = Int(Val(b$))
          End If
        Case "OPZ_INT_MONT_CALC"  '20160124LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 1 And 1 = 2 Then  '20160222LC (inibito)
            OPZ_INT_MONT_CALC = Int(Val(b$))
          End If
        Case "OPZ_OUT_SIL_ATT"  '20160222LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 2 Then
            OPZ_OUT_SIL_ATT = Int(Val(b$))
          End If
        Case "OPZ_FRAZ_COMP"  '20160222LC
          If IsNumeric(b$) Then
            y = CDbl(b$)
            If y >= 0# And y <= 1# Then
              OPZ_FRAZ_COMP = 1#
            End If
          End If
        Case "OPZ_INT_AZZERA_REDDITO"  '20160306LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 2 Then
            OPZ_INT_AZZERA_REDDITO = Int(Val(b$))
          End If
        Case "OPZ_ART14_90"  '20160308LC
          If Int(Val(b$)) >= 0 And Int(Val(b$)) <= 255 Then
            OPZ_ART14_90 = Int(Val(b$))
          End If
        Case "OPZ_ART14_5"  '20160308LC
          If IsNumeric(b$) Then
            y = CDbl(b$)
            If y >= 0 And y <= 1 Then
              OPZ_ART14_5 = y
            End If
          End If
        Case "OPZ_ART16_2"  '20160308LC
          If IsNumeric(b$) Then
            y = CDbl(b$)
            If y >= 0 And y <= 1 Then
              OPZ_ART16_2 = y
            End If
          End If
        Case "OPZ_ART19_3"  '20160309LC
          y = Int(Val(b$))
          If y >= 0 And y <= 5 Then
            OPZ_ART19_3 = y
          End If
        Case "OPZ_ART20_1"  '20160309LC
          y = Int(Val(b$))
          If y >= 0 And y <= 5 Then
            OPZ_ART20_1 = y
          End If
        Case "OPZ_ART23_1"  '20160420LC
          y = Int(Val(b$))
          If y >= 0 And y <= 5 Then
            OPZ_ART23_1 = y
          End If
        Case "OPZ_TOLL_PENS"  '20160420LC
          If IsNumeric(b$) Then
            y = CDbl(b$)
            If y >= 0# Then
              OPZ_TOLL_PENS = y
            End If
          End If
        Case "OPZ_SIL_PVEP75"  '20160422LC
          y = Fix(Val(b$))
          If y >= -199 And y <= 199 Then
            OPZ_SIL_PVEP75 = y
          End If
        '*************************
        'Mod.reg.contr.min. 2018/1
        '*************************
        Case "OPZ_MOD_REG_1"  '20161112LC
          If IsNumeric(b$) Then
            y = Fix(Val(b$))
            If y >= 0 And y <= 3 Then  '20170307LC  'ex 20170220LC
              OPZ_MOD_REG_1A = y
              If OPZ_MOD_REG_1A > 0 Then
                OPZ_MOD_REG_1_ANNI = 4  '20180514LC
              End If
            End If
          End If
        Case "OPZ_MOD_REG_1_CV"  '20170307LC
          If OPZ_MOD_REG_1A <> 0 Then
            OPZ_MOD_REG_1_CV = b$
          End If
        Case "OPZ_MOD_REG_1_ANNI"  '20180514LC
          If OPZ_MOD_REG_1A <> 0 Then
            If IsNumeric(b$) Then
              y = Fix(Val(b$))
              If y > 0 And y <= 99 Then
                OPZ_MOD_REG_1_ANNI = y
              End If
            End If
          End If
        '****************************
        'Mod.reg. 2018/2 (riv.contr.)
        '****************************
        Case "OPZ_MOD_REG_2"  '20161111LC
          If IsNumeric(b$) Then
            y = Fix(Val(b$))
            If Abs(y) >= 0 And Abs(y) <= 2 Then  '20180122LC (abs)  'ex 20170125LC (2)
              OPZ_MOD_REG_2 = y
              If OPZ_MOD_REG_2 <> 0 Then  '20180122LC (ex >)
                OPZ_MOD_REG_2_AMIN = 2016  '20161116LC
                OPZ_MOD_REG_2_ANNO = 2018
                OPZ_MOD_REG_2_ANNI = 2
                OPZ_MOD_REG_2_CIFRE_TOLL = 4
                OPZ_MOD_REG_2_NITMAX = 10
                OPZ_MOD_REG_2_PERC_PIL5 = 0#
                OPZ_MOD_REG_2_RIV_MAX = 0.1   '20161114LC
              End If
            End If
          End If
        Case "OPZ_MOD_REG_2_CV"  '20170307LC
          If OPZ_MOD_REG_2 <> 0 Then
            OPZ_MOD_REG_2_CV = b$
          End If
        Case "OPZ_MOD_REG_2_AMIN"  '20161116LC-3
          If OPZ_MOD_REG_2 <> 0 And IsNumeric(b$) Then
            y = Fix(Val(b$))
            If y > 0 Then
              OPZ_MOD_REG_2_AMIN = y
            End If
          End If
        Case "OPZ_MOD_REG_2_ANNO"  '20161111LC
          If OPZ_MOD_REG_2 <> 0 And IsNumeric(b$) Then
            y = Fix(Val(b$))
            If y > 0 Then
              OPZ_MOD_REG_2_ANNO = y
            End If
          End If
        Case "OPZ_MOD_REG_2_ANNI"  '20161116LC
          If OPZ_MOD_REG_2 <> 0 And IsNumeric(b$) Then
            y = Fix(Val(b$))
            If y >= 2 And y <= 5 Then
              OPZ_MOD_REG_2_ANNI = y
            End If
          End If
        Case "OPZ_MOD_REG_2_CIFRE_TOLL"  '20161111LC
          If OPZ_MOD_REG_2 <> 0 And IsNumeric(b$) Then
            y = Fix(Val(b$))
            If y >= 4 And y <= 8 Then
              OPZ_MOD_REG_2_CIFRE_TOLL = y
            End If
          End If
        Case "OPZ_MOD_REG_2_NITMAX"  '20161111LC
          If OPZ_MOD_REG_2 <> 0 And IsNumeric(b$) Then
            y = Fix(Val(b$))
            If y >= 1 And y <= 10 Then
              OPZ_MOD_REG_2_NITMAX = y
            End If
          End If
        Case "OPZ_MOD_REG_2_PERC_PIL5"  '20161112LC
          If OPZ_MOD_REG_2 <> 0 And IsNumeric(b$) Then
            y = CDbl(b$)
            If y >= 0# And y <= 1 Then
              OPZ_MOD_REG_2_PERC_PIL5 = y
            End If
          End If
        Case "OPZ_MOD_REG_2_RIV_MAX"  '20161114LC
          If OPZ_MOD_REG_2 <> 0 And IsNumeric(b$) Then
            y = CDbl(b$)
            If (y = -1#) Or (y >= 0# And y <= 1#) Then  '20180306LC  'ex 20161115LC (y=0)
              OPZ_MOD_REG_2_RIV_MAX = y
            End If
          End If
        '*****************************
        'Mod.reg. 2018/3 (pereq.pens.)
        '*****************************
        Case "OPZ_MOD_REG_3"  '20161112LC
          If IsNumeric(b$) Then
            y = Fix(Val(b$))
            If y >= 0 And y <= 1 Then
              OPZ_MOD_REG_3 = y
              If OPZ_MOD_REG_3 > 0 Then
                OPZ_MOD_REG_3_ANNO = 2018
                OPZ_MOD_REG_3_PERC_ENAS = 1# '0.2
              End If
            End If
          End If
        Case "OPZ_MOD_REG_3_CV"  '20170307LC
          If OPZ_MOD_REG_3 <> 0 Then
            OPZ_MOD_REG_3_CV = b$
          End If
        Case "OPZ_MOD_REG_3_ANNO"  '20161111LC
          If OPZ_MOD_REG_3 <> 0 And IsNumeric(b$) Then
            y = Fix(Val(b$))
            If y > 0 Then
              OPZ_MOD_REG_3_ANNO = y
            End If
          End If
        Case "OPZ_MOD_REG_3_PERC_ENAS"  '20161112LC
          If OPZ_MOD_REG_3 <> 0 And IsNumeric(b$) Then
            y = CDbl(b$)
            If y > 0# And y <= 1# Then
              OPZ_MOD_REG_3_PERC_ENAS = y
            End If
          End If
        '--- 20161126LC (inizio)
        Case "OPZ_PENS_MODEL"
          If IsNumeric(b$) Then
            y = Fix(Val(b$))
            If y = 1 Or y = 2 Or y = 3 Then
              OPZ_PENS_MODEL = y
              If OPZ_PENS_MODEL = 1 Or OPZ_PENS_MODEL = 2 Then
                OPZ_PENS_C0 = 1#
                OPZ_PENS_SP0 = 0#
              ElseIf OPZ_PENS_MODEL = 3 Then
                OPZ_PENS_C0 = 0.5
                OPZ_PENS_SP0 = 0#
              End If
            End If
          End If
        Case "OPZ_PENS_C0"  '20140312LC
          If (OPZ_PENS_MODEL = 3) Then
            If IsNumeric(b$) Then
              y = CDbl(b$)
              If y >= 0 And y <= 1 Then  '
                OPZ_PENS_C0 = y
              End If
            End If
          End If
        Case "OPZ_PENS_SP0"  '20161116LC
          If (OPZ_PENS_MODEL = 1 Or OPZ_PENS_MODEL = 2) Then
            If IsNumeric(b$) Then
              y = CDbl(b$)
              If y >= 0 And y <= 1 Then  '
                OPZ_PENS_SP0 = y
              End If
            End If
          End If
        '--- 20161126LC (fine)
        '--- 20170724LC (inizio)
        Case "OPZ_RC_MODEL"
          If IsNumeric(b$) Then
            y = Fix(Val(b$))
            If y = 0 Or y = 1 Or y = 2 Then '20170801LC  'ex 20170727LC  'ex <=3
              OPZ_RC_MODEL = y
            End If
          End If
        Case "OPZ_RC_ADEC"
          'If OPZ_RC_MODEL <> 0 Then
            If IsNumeric(b$) Then
              y = Fix(Val(b$))
              If y >= 0 And y <= 9999 Then
                OPZ_RC_ADEC = y
              End If
            End If
          'End If
        '--- 20170724LC (fine)
        '--- 20170727LC (inizio)
        Case "OPZ_RC_FRAZ_ATT"
          If OPZ_RC_MODEL <> 0 Then
            If IsNumeric(b$) Then
              y = CDbl(b$)
              If y >= 0# And y <= 1# Then
                OPZ_RC_FRAZ_ATT = y
              End If
            End If
          End If
        Case "OPZ_RC_FRAZ_SIL"
          If OPZ_RC_MODEL <> 0 Then
            If IsNumeric(b$) Then
              y = CDbl(b$)
              If y >= 0# And y <= 1# Then
                OPZ_RC_FRAZ_SIL = y
              End If
            End If
          End If
        '--- 20170724LC (fine)
        '--- 20170724LC (fine)
        Case "OPZ_RIATT_AMIN"  '20180305LC
          If IsNumeric(b$) Then
            y = Fix(Val(b$))
            OPZ_RIATT_AMIN = y
          End If
        '*****
        'DEBUG
        '*****
        Case "OPZ_DBG_PENSIONE"  '20160312LC
          If IsNumeric(b$) Then
            y = CDbl(b$)
            If y > 0 Then
              OPZ_DBG_PENSIONE = y
            End If
          End If
        Case "OPZ_DBG_INCRPENS"  '20160312LC
          If IsNumeric(b$) Then
            y = CDbl(b$)
            If y > 0 Then
              OPZ_DBG_INCRPENS = y
            End If
          End If
        'Case "OPZ_DEBUG"  '20160306LC (commentato)  'ex 20120601LC
        '  If Val(b$) >= 0 And Val(b$) <= 1 Then
        '    OPZ_DEBUG = Val(b$)
        '  End If
        '---
        '20121009LC (inizio)
        'Case "OPZ_ABB_PENS_ANZ", "OPZ_SOGGAL1", "OPZ_SOGGAL1_HXMIN", "OPZ_SOGGAL1_MIN", "OPZ_SOGGAL1_MAX", _
        '     "OPZ_SOGGAL1_INCR", "OPZ_SOGGAL1_ANNI", "OPZ_CREQ_ANNO", "OPZ_CREQ_HMIN", "OPZ_CREQ_XMIN", _
        '     "OPZ_CREQ_HANZ", "OPZ_CREQ_HVEC", "OPZ_RICRIS", "OPZ_RICRIS_1_ALC", "OPZ_RICRIS_1_DH", _
        '     "OPZ_RICRIS_1_DH2", "OPZ_RICRIS_REDDMIN", "OPZ_RICRIS_REDDMAX", "OPZ_RICRIS_NOME", "OPZ_H40", _
        '     "OPZ_TEMP1", "OPZ_RIV_TASSI_MASK", "OPZ_NI_INGR_M", "OPZ_VAR_XH_MODEL"
'OPZ_EA_AZZERA_INT0 = 1        'EX ATTIVI, 1:AZZERA L'INTEGRATIVO MINIMO
'OPZ_EA_AZZERA_INT = 1         'EX ATTIVI, 1:AZZERA TUTTI I CONTRIBUTI INTEGRATIVI (CALCOLATI ESTERNAMENTE)
'OPZ_EA_AZZERA_SOGG1_INIZ = 0  'EX ATTIVI, 1:AZZERA IL SALDO DELL'ULTIMO SOGGETTIVO
'OPZ_ABB_PENS_ANZ = 99         'ABBATTIMENTO PENSIONI DI ANZIANIT� (ES: 40= ABBATTE LE PENSIONI CON H<40; 0=NO PENALIT�)
'OPZ_SOGGAL1 = 0               'MODELLO ALTERNATIVO PER SOGGAL1 (1:IN BASE A X, 2: IN BASE A H)
'OPZ_SOGGAL1_HXMIN = 35        'ET� O ANZIANIT� MINIMA PER IL MODELLO ALTERNATIVO
'OPZ_SOGGAL1_MIN = 0,1         'ALIQUOTA MINIMA DEL MODELLO ALTERNATIVO PER SOGGAL1
'OPZ_SOGGAL1_MAX = 0,2         'ALIQUOTA MINIMA DEL MODELLO ALTERNATIVO PER SOGGAL1
'OPZ_SOGGAL1_INCR = 0,01       'INCREMENTO MINIMO DEL MODELLO ALTERNATIVO PER SOGGAL1
'OPZ_SOGGAL1_ANNI = 2          'ANNI PER UN INCREMENTO MINIMO DEL MODELLO ALTERNATIVO PER SOGGAL1
'OPZ_CREQ_ANNO = 0             'ANNO DI PARTENZA PER IL CAMBIO DEI REQUISITI
'OPZ_CREQ_HMIN = 20            'ANZIANIT� MINIMA PER NON CAMBIARE REQUISITI
'OPZ_CREQ_XMIN = 55            '(OPPURE) ET� MINIMA PER NON CAMBIARE REQUISITI
'OPZ_CREQ_HANZ = 40            'ANZIANIT� MINIMA PER PENS. DI ANZ. SE SI CAMBIANO I REQUISITI
'OPZ_CREQ_HVEC = 35            'ANZIANIT� MINIMA PER PENS. DI VEC. SE SI CAMBIANO I REQUISITI
'OPZ_RICRIS = 0                'MODELLO DI RICONGIUNZIONE/RISCATTO
'OPZ_RICRIS_1_ALC = 0.15       'ALIQUOTA DEL REDDITO PER PAGARE 1 ANNO DI RICONGIUNZIONE/RISCATTO
'OPZ_RICRIS_1_DH = 2           '1� INCR. DI ANZ. (OPZ_RICRIS: =1 IN % SEMPRE, =2 IN ANNI FINO A X=57 SE PRENDONO P.VECCH.)
'OPZ_RICRIS_1_DH2 = 5          '2� INCR. DI ANZ.  (OPZ_RICRIS: =2 IN ANNI FINO A X=57, SE PRENDONO P.CONTR.)
'OPZ_RICRIS_REDDMIN = 0        'REDDITO MINIMO PER RIC./RISC.
'OPZ_RICRIS_REDDMAX = 9999999  'REDDITO MASSIMO PER RIC./RISC.
'OPZ_RICRIS_NOME = BASE        'CHIAVE DI RICERCA IN TABELLA C_COE_RISC
'OPZ_H40 = 0                   'VERSIONE H40 (0:NO)
'OPZ_TEMP1 = 0                 '<>0 TENTA DI ABBREVIAVE I TEMPI DI ANALISI (TEMPORANEO)
'OPZ_RIV_TASSI_MASK = 0        '1:RIVALUTA CON I PARAMETRI DA MASCHERA, 0:ANZICH� DA C_COE_VAR2'
'OPZ_NI_INGR_M = 0
'OPZ_VAR_XH_MODEL = 0          'MODELLO DI VARIAZIONE ETA E ANZIANITA' (TEMPORANEO)
          '---
          'If sErrOpzNA <> "" Then
          '  sErrOpzNA = sErrOpzNA & ", " & a$
          'Else
          '  sErrOpzNA = a$
          'End If
        Case Else '20121009LC
          If sErrOpzS <> "" Then
            sErrOpzS = sErrOpzS & ", " & a$
          Else
            sErrOpzS = a$
          End If
        End Select
        '20121009LC (fine)
      End If
      '20120319LC (inizio)
      'Loop
      'Close #fi
    Next ir
    '20120319LC (fine)
    '--- 20121009LC (inizio)
    sErr = ""
    If sErrOpzNA <> "" Then
      If InStr(sErrOpzNA, ",") = 0 Then
        sErr = "Il file AFP_OPZ.INI contiene la seguente opzione non pi� attiva:" & vbCrLf & sErrOpzNA
      Else
        sErr = "Il file AFP_OPZ.INI contiene le seguenti opzioni non pi� attive:" & vbCrLf & sErrOpzNA
      End If
    End If
    If sErrOpzS <> "" Then
      If sErr <> "" Then
        sErr = sErr & vbCrLf & vbCrLf
      End If
      If InStr(sErrOpzNA, ",") = 0 Then
        sErr = sErr & "Il file AFP_OPZ.INI contiene la seguente opzione sconosciuta:" & vbCrLf & sErrOpzS
      Else
        sErr = sErr & "Il file AFP_OPZ.INI contiene le seguenti opzioni sconosciute:" & vbCrLf & sErrOpzS
      End If
    End If
    If sErr <> "" Then
      If glo.MJ = MI_VB Then  '20130307LC
        Call MsgBox(sErr, vbExclamation, "Lettura del file AFP_OPZ.INI")
      End If
    End If
    '---
    OPZ_MOD_ART_26_5 = IIf(OPZ_MISTO_INTEGR_MODEL = 4, 1, 0)  '20121009LC 'MODIFICA per retrocessione integrativo (interno)
    '--- 20121009LC (fine)
    '20120402LC (inizio)
    If OPZ_MISTO_REDD_PENS = 2 Then
      OPZ_MISTO_REDD_PENS = 1
      OPZ_MISTO_DEB_MAT = 1
    End If
    '20120402LC (fine)
    '--- 20121031LC (inizio)
    If OPZ_RND_MODEL < 2 Then
      If OPZ_RND_MODEL = 0 Then
        OPZ_RND_NIT = 1
      End If
      OPZ_RND_QAD = OPZ_RND_MODEL
      OPZ_RND_QED = OPZ_RND_MODEL
      OPZ_RND_QBD = OPZ_RND_MODEL
      OPZ_RND_QID = OPZ_RND_MODEL
      OPZ_RND_QPD = OPZ_RND_MODEL
      OPZ_RND_QAV = OPZ_RND_MODEL
      OPZ_RND_QAE = OPZ_RND_MODEL
      OPZ_RND_QAB = OPZ_RND_MODEL
      OPZ_RND_QAI = OPZ_RND_MODEL
      OPZ_RND_QAW = OPZ_RND_MODEL
      OPZ_RND_QVA = OPZ_RND_MODEL  '20160523LC
      OPZ_RND_QEA = OPZ_RND_MODEL  '20160523LC
    End If
    '--- 20121031LC (fine)
    'P_DUECENTO = 100 + OPZ_ELAB_CODA
    P_DUECENTO = OPZ_OMEGA + OPZ_ELAB_CODA
    '--- 20121101LC
    OPZ_INT_PF_XMIN = 16
    OPZ_INT_PF_XMAX = OPZ_OMEGA
    '--- 20130220LC (inizio)
    If OPZ_ABB_QM_SUP > 99 And OPZ_ELAB_CODA = 0 Then  '20130302LC
      OPZ_ABB_QM_SUP = 99  '20130302LC (commentato)
    End If
    P_PFAM_MAX_TAU = OPZ_ABB_QM_SUP
    P_PFAM_MAX_X = OPZ_OMEGA
    '--- 20130220LC (fine)
    OPZ_OMEGAP1 = OPZ_OMEGA + 1  '20130211LC
    '--- 20160306LC (inizio)
    If OPZ_INT_AZZERA_REDDITO = -1 Then
      If OPZ_FRAZ_COMP < 1 Then
        OPZ_INT_AZZERA_REDDITO = 2
      Else
        OPZ_INT_AZZERA_REDDITO = 0
      End If
    End If
    '--- 20161126LC (inizio)
    If OPZ_PENS_MODEL = 0 Then
      OPZ_PENS_MODEL = 3
      OPZ_PENS_C0 = 0.5
      OPZ_PENS_SP0 = 0#
    ElseIf OPZ_PENS_MODEL = 1 Or OPZ_PENS_MODEL = 2 Then
      OPZ_PENS_C0 = 1#
    ElseIf OPZ_PENS_MODEL = 3 Then
      OPZ_PENS_SP0 = 0#
    End If
    '--- 20161126LC (inizio)
  End With
  
ExitPoint:
  TGlo_Opz_Init = sErr
End Function

