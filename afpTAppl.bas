Attribute VB_Name = "modAfpTAppl"
Option Explicit


Public Const ERR_TEXT = 1
Public Const ERR_COMBO = 2
Public Const ERR_CHECK = 3
Public Const ERR_BUTTON = 4

'20080614LC (inizio) - rivisto
Public Const REP1_PRM = 0      'Parametri di input
Public Const REP1_OPZ_INI = 1  'File AFP_OPZ_INI
Public Const REP1_COE_VAR = 2  'Coefficienti variabili
Public Const REP1_POP = 3      'Sviluppo collettivit�
Public Const REP1_SUP = 4      'Sviluppo superstiti
Public Const REP1_ENT1 = 5     'Evoluzione redditi IRPEF e volume d'affari IVA
Public Const REP1_ENT2 = 6     'Evoluzione contributi
Public Const REP1_MONT = 7     'Evoluzione montanti contributivi
Public Const REP1_USC1 = 8     'Evoluzione pensioni
Public Const REP1_USC2 = 9     'Evoluzione pensionati non contribuenti
'--- 20160419LC-2 (inizio)
Public Const REP1_BIL_TECN_MIN = 10 'Bilancio tecnico (ministeriale)
Public Const REP1_BIL_FIRR = 11     'Bilancio tecnico
'--- 20160419LC-2 (fine)
Public Const REP1_VAL_CAP = 12 'Valori capitali
'20080614LC (fine)

'###########
'Da modTAppl
'###########
Public Type TAppl_CV4 '20120210LC
  Pveo30_orig As Byte '20121008LC
  Pveo65 As Byte
  Pveo98 As Byte
  '---
  Pvea35 As Byte
  Pvea58 As Byte
  Pvep70 As Byte
  '---
  Pinab2 As Byte
  Pinab10 As Byte
  Pinab35 As Byte
  '---
  Pinv5 As Byte
  Pinv70 As Double
  '---
  Pind2 As Byte
  Pind20 As Byte
  Pind30 As Byte
  '---
  TzHmin As Byte
  TzXmin As Byte
  TzRivMin As Double
  '---
  RcHmin As Byte
  RcXmin As Byte
  RcPrcCon As Double
  'RcPrcRiv As Double  '20150512LC (commentato)
End Type

Public Type TAppl
  idReport As Long
  nMatr As Long  'Numero di matricole esaminate
  xa As Double
  h As Double
  bEA As Boolean
  bProb_EA_IV_IB As Boolean  '20121113LC
  bProb_AW As Boolean        '20121113LC
  bProb_PensAtt As Boolean   '20121113LC
  matr1 As Long  'Prima matricola
  matr2 As Long  'Ultima matricola
  bStop As Boolean
  t0 As Integer    '// Anno del bilancio tecnico
  DataBilancio As Date  '// Data del bilancio
  DataInizio   As Date  '// Data del bilancio + 1 giorno
  '// *** Eta' ***
  XMIN As Long   '// Eta' minima per essere assunti
  XMAX As Long   '// Eta' alla quale si deve andare in pensione (variabile)
  xlim As Long   '// Eta' alla quale il Programma 1 segnala un errore (fisso)
  nx5 As Long    '// Numero di intervalli quinquennali da xmin a xlim - 1
  '// *** Anzianita' ***
  hmin5 As Long  '// Anzianita' minima per... (=5)
  hmin15 As Long '// Anzianita' minima per... (=15)
  hmax As Long   '// Anzianita' alla quale si deve andare in pensione (variabile)
  '--- 20121028LC (inizio)
  nSize1 As Long
  nSize2 As Long
  nSize3 As Long
  nSize4 As Long
  '--- 20121028LC (inizio)
  hlim As Long   '// Anzianita' alla quale il Programma 1 segnala un errore (fisso)
  nh5 As Long    '// Numero di intervalli quinquennali da 0 a hlim - 1
  '// *** altro ***
  Qualifica As Long  '// Qualifica degli appartenenti al sottogruppo
  sesso As Long      '// Sesso degli appartenenti al sottogruppo
  '// *** Nomi di file
  szDef1Input As String * 12
  szDef1Output As String * 12
  iDef1Gruppo As Long
  szDef2Input As String * 12
  szDef2Output As String * 12
  iDef2Gruppo As Long
  szDef3Input As String * 12
  szDef3Output As String * 12
  iDef3Gruppo As Long
  szDef3RenditeM As String * 12
  szDef3RenditeF As String * 12
  szDef3TipoFile As String * 1
  '*******
  'Matrici
  '*******
  LL1() As Double
  pfamL() As TWFL   '20130302LC
  pfam4() As TWFD   '20130216LC
  CoeffPA() As Double
  CoeffRiv() As Double
  CoeffVar() As Double
  cv() As TCV '20121102LC . tentativo di sostituire CoeffVar
  '--- 20170307LC (inizio)
  cvR1() As TCV_R1
  cvR2() As TCV_R2
  cvR3() As TCV_R3
  '--- 20170307LC (fine)
  CoeffNI() As Double
  tb1() As Double
  '20080614LC
  SuddivReport() As String
  SuddivRipart() As Double
  '--- 20080919LC
  nPensMat As Long
  nPensMax As Long
  '--- 20120210LC
  cv4() As TAppl_CV4
  '--- 20150521LC
  tb2() As Double
  nDipGen As Long   '20160411LC (ex Integer)
  nDipGenBk As Long '20160411LC (new)
  '####
  'ENEA
  '####
  bProb_Superst As Boolean       '20121109LC
  '20080416LC (inizio)
  nMax1 As Long  '// Durata massima
  nMax2 As Long  '// Durata massima
  '20080416LC (fine)
  '*********
  'Parametri
  '*********
  parm(299) As Variant
  '*******
  'Matrici
  '*******
  BDA() As Double   '20120312LC
  pf() As Double
  pfamD() As TWFD   '20130302LC
  Lsm() As Double
  '--- 20150527LC (inizio)
  Lni() As Double
  Lne() As Double
  Lnv() As Double
  '--- 20150527LC (fine)
  '###
  '###
  iDB As Integer
    jDB As Integer  '20140128LC
  annoBil As Integer
  'dRif1 As TDate  'Pveo30_19810128  '20160306LC (commentato) 'ex ENEA
  iDbType As Integer
  sDbConn As String
  sFileOpzIni As String
  '---
  LL() As Double
  '--- 20140203LC
  P_niTipo As Integer
  '--- 20150522LC
  req As Scripting.Dictionary
  '--- 20161111LC - Calcolo iterativo
  iterTipo As Integer
  iterNum As Integer
  iterCvNome As String
  iterAnniVar As Integer
  iterAnnoVarMin As Integer
  iterAnnoVarMax As Integer
  iterDiffMin As Double
  iterDiffMax As Double
  iterSaldo As Double
  '--- 20181108LC (inizio) MEF
  tbMEF() As Double
  '--- 20181108LC (fine) MEF
End Type


Public Type TInput
  iErr As Long    'Codice di errore
  szErr As String 'Messaggio di errore
  iFrame As Byte  'Frame del controllo che ha dato errore
  iFocus As Long  'Indice del controllo che ha dato errore
  iType As Byte   '0: testo, 1:combo, 2:
End Type


Public p_t As TInput
  

'20121020LC
Public Function TAppl_Req_Check(ByRef appl As TAppl, req) As String  'S044
  '******************
  'Controllo dei dati
  '******************
  Dim a$, SQL$, l$
  Dim rs As New ADODB.Recordset
  Dim i%, j As Byte, j1 As Byte, j2 As Byte, k%
  Dim Label1() As String
'Exit Function

  p_t.iErr = 0
  p_t.szErr = ""
  ReDim Label1(UBound(appl.parm, 1))
  If 1 = 2 Then
    Dim s As String
  
    If ON_ERR_RN1 Then On Error Resume Next
    s = ""
    For i = 0 To UBound(appl.parm, 1)
      ''''Label1(i) = obj.Label1(i)
      If Err.Number = 0 Then
      s = s & i & vbTab & Label1(i) & vbCrLf
      Else
        Err.Clear
      End If
    Next i
    Clipboard.Clear
    Clipboard.SetText s
    On Error GoTo 0
  End If
  '*******************
  'Frame PARAMETRI (1)
  '*******************
  Label1(P_ID) = "Gruppo parametri"                              '1
  '--- 1.1 Bilancio tecnico ----
  Label1(P_btAnno) = "Data di calcolo"                           '2
  Label1(P_btAnniEl1) = "Anni di valutazione"                    '3
  Label1(P_btAnniEl2) = ""                                       '4
  Label1(P_btPatrim) = "Patrimonio iniziale"                     '5
  Label1(P_btSpAmm) = "Contrib. per spese amm."                  '6
  Label1(P_btSpAss) = "Contrib. per prestaz. assist."            '7
  '---
  Label1(P_btPassSIV) = "Passivi societ� d'ingegneria"           '8
  Label1(P_btPassSIP) = ""                                       '9
  '--- 1.2 Tipo di calcolo ----
  Label1(P_TipoElab) = ""                                        '12  '??
  '--- 1.3 Modalit� di analisi ---
  Label1(P_SQL) = "Query di estrazione dati"                     '13
  Label1(P_FOUT) = "File di output"                              '14
  '******************************
  'Frame TASSI ED ALTRE STIME (2)
  '******************************
  '--- 2.1 Tassi di valutazione e rivalutazione ---
  '--- 20140605LC (inizio)
  a = IIf(OPZ_INT_CV21 = 0, "*", "")
  Label1(P_cvTv) = a & "Valutazione"                               '21  'Valutazione
  Label1(P_cvTi) = a & "Rendimento finanziario"                    '22  'Rendimento dell'attivo
  '--- 20140605LC (fine)
  Label1(P_cvTev) = "Inflazione"                                   '23  'Tasso di inflazione (elementi variabili)
  Label1(P_cvTrn) = "Reddito iniziale dei nuovi iscritti"          '24  'Tasso di rivalutazione dei redditi iniziali dei nuovi iscritti
  '--- 2.2 Tassi di rivalutazione dei montanti contributivi ---
  '--- 20161111LC (inizio)
  Label1(P_cvTrmc0) = "Rapporto contributi/pensioni"                  '25
  Label1(P_cvTrmc1) = "Riv. contributi quota B"                       '26
  Label1(P_cvTrmc2) = "Riv. contributi volontari"                     '27
  Label1(P_cvTrmc3) = "Riv. contributi per supplementi di pensione"   '28
  Label1(P_cvTrmc4) = "Riv. contributi quota C"                       '29
  'Label1(P_cvTrmc5) = "Contributi facoltativi"                       '30
  Label1(P_cvTrmc5) = "Riv. contributi PIL quinquennale"              '30
  Label1(P_cvTrmc6) = "Riv. contributi FIRR"                          '31
  '--- 20161111LC (inizio)
  '--- 2.3 Tassi di rivalutazione di redditi e volumi d'affari ---
  Label1(P_cvTs1a) = "Attivi"                                    '32
  Label1(P_cvTs1p) = "Pensionati attivi"                         '34
  Label1(P_cvTs1n) = "Nuovi iscritti"                            '36
  '--- 2.4 Rivalutazione delle pensioni ---
  Label1(P_cvTp1Max) = "1� scaglione"                            '38
  Label1(P_cvTp2Al) = "Massimale"                                '39*
  Label1(P_cvTp2Max) = "2� scaglione"                            '40
  Label1(P_cvTp1Al) = "Tasso"                                    '41*
  Label1(P_cvTp3Max) = "3� scaglione"                            '42
  Label1(P_cvTp3Al) = ""                                         '43
  '--- 20161112LC (inizio)
  Label1(P_cvTp4Max) = "4� scaglione"                            '138
  Label1(P_cvTp4Al) = "Massimale"                                '139*
  Label1(P_cvTp5Max) = "5� scaglione"                            '140
  Label1(P_cvTp4Al) = "Tasso"                                    '141*
  '--- 20161112LC (fine)
  '--- 2.5 Altre stime ----
  '--- 20140605LC (inizio)
  a = IIf(OPZ_INT_CV25 = 0, "*", "")
  Label1(P_cvPrcPVattM) = a & "Fraz. pensionati di Vecchiaia"       '44
  Label1(P_cvPrcPVattF) = "femmine"                                 '45
  Label1(P_cvPrcPAattM) = a & "Fraz. pensionati di Anzianit�"       '46
  Label1(P_cvPrcPAattF) = "maschi"                                  '47
  Label1(P_cvPrcPCattM) = a & "Fraz. pensionati Contributivi"       '48
  Label1(P_cvPrcPCattF) = ""                                        '49
  '--- 20140605LC (fine)
  '***********************
  'Frame BASI_TECNICHE (3)
  '***********************
  '--- 3.1 Probabilit� di morte ----
  Label1(P_LAD) = "Attivi"                                       '71
  Label1(P_LPD) = "Pensionati"                                   '73
  Label1(P_LSD) = "Mortalit� superstiti"                         '74
  Label1(P_LBD) = "Pensionati di inabilt�"                       '75
  Label1(P_LID) = "Pensionati di invalidit�"                     '76
  '--- 3.2 Altre probabilit� ----
  Label1(P_LED) = "Silenti"                                    '72
  Label1(P_LAV) = "Volontari da attivi"                        '277
  Label1(P_LAE) = "Silenti da attivi"                          '77
  Label1(P_LAB) = "Inabilit� degli attivi"                       '78
  Label1(P_LAI) = "Invalidit� degli attivi"                      '79
  Label1(P_LAW) = "Uscita per altre cause"                       '80
  '--- 3.3 Linee reddituali ----
  Label1(P_LRA) = "Attivi"                                       '81
  Label1(P_LRP) = "Pensionati attivi"                            '82
  Label1(P_LRN) = "Nuovi iscritti"                               '83
  '--- 3.4 Coefficienti di conversione del montante in rendita ----
  Label1(P_CCR) = "Tav. coeff. conversione"                      '84
  '--- 3.5 Superstiti ----
  Label1(P_PF) = "Probabilit� di famiglia"                       '85
  Label1(P_ZMAX) = "Et� limite orfani"                           '86
  '******************************
  'Frame REQUISITI DI ACCESSO (4)
  '******************************
  '--- 4.1 Pensione di vecchiaia ----
  Label1(P_cvPveo30M) = "Anz. minima (m/f)"                      '101
  Label1(P_cvPveo30F) = ""                                       '102
  Label1(P_cvPveo65M) = "Et� minima (m/f)"                       '103
  Label1(P_cvPveo65F) = ""                                       '104
  Label1(P_cvPveo98M) = "Quota minima (m/f)"                     '105
  Label1(P_cvPveo98F) = ""                                       '106
  '--- 4.2 Pensione di vecchiaia anticipata ----
  Label1(P_cvPvea35M) = "Anz. minima (m/f)"                      '107
  Label1(P_cvPvea35F) = ""                                       '108
  Label1(P_cvPvea58M) = "Et� minima (m/f)"                       '109
  Label1(P_cvPvea58F) = ""                                       '110
  '--- 4.3 Pensione di vecchiaia posticipata ----
  Label1(P_cvPvep70M) = "Et� (m/f)"                              '111
  Label1(P_cvPvep70F) = ""                                       '112
  '--- 4.4 Pensione di inabilit� ----
  Label1(P_cvPinab2M) = "Anz. minima (m/f)"                      '113
  Label1(P_cvPinab2F) = ""                                       '114
  Label1(P_cvPinab10M) = "Anz.agg.ricon. (m/f)"                  '115
  Label1(P_cvPinab10F) = ""                                      '116
  Label1(P_cvPinab35M) = "Anz.max.ricon. (m/f)"                  '117
  Label1(P_cvPinab35F) = ""                                      '118
  '--- 4.5 Pensione di invalidit� ----
  Label1(P_cvPinv5M) = "Anzianit� minima (m/f)"                  '119
  Label1(P_cvPinv5F) = ""                                        '120
  Label1(P_cvPinv70) = "Fraz. pens. inab."                       '121
  '--- 4.6 Pensione indiretta ----
  Label1(P_cvPind2M) = "Anz. minima (m/f)"                       '122
  Label1(P_cvPind2F) = ""                                        '123
  Label1(P_cvPind20M) = "Anz.min.calc. (m/f)"                    '124
  Label1(P_cvPind20F) = ""                                       '125
  Label1(P_cvPind30M) = "Anz.max.calc. (m/f)"                    '126
  Label1(P_cvPind30F) = ""                                       '127
  '--- 4.7 Restituzione dei contributi ----
  Label1(P_cvRcHminM) = "Anz.min.pens.sost.(m/f)"                '128
  Label1(P_cvRcHminF) = ""                                       '129
  Label1(P_cvRcXminM) = "Et� di uscita (m/f)"                    '130
  Label1(P_cvRcXminF) = ""                                       '131
  Label1(P_cvRcPrcCon) = "Contributi restituibili"               '132
  'Label1(P_cvRcPrcRiv) = "Tasso riv.contr."                     '133  '20150512LC (commentato)
  '--- 4.8 Totalizzazione ----
  '***********************
  'Frame CONTRIBUZIONE (5)
  '***********************
  '--- 5.1 Contribuzione ridotta ----
  '--- 5.2 Soggettivo ----
  Label1(P_cvSogg0Min) = "Minimo"                                '154
  Label1(P_cvSogg0MinR) = "Ridotta"                              '155*
  Label1(P_cvSogg1Al) = "1^ aliquota"                            '156
  Label1(P_cvSogg1AlR) = "Intera"                                '157*
  Label1(P_cvSogg1Max) = "Massimale 1^ aliquota"                 '158
  Label1(P_cvSogg1MaxR) = ""                                     '159
  '--- 5.3 Integrativo / FIRR ----
  Label1(P_cvInt0Min) = "Minimo"                                 '167
  Label1(P_cvInt0MinR) = "Ridotta"                               '168*
  Label1(P_cvInt1Al) = "1^ aliquota"                             '169
  Label1(P_cvInt1AlR) = "Intera"                                 '170*
  Label1(P_cvInt1Max) = "Massimale 1^ aliquota"                  '171
  Label1(P_cvInt1MaxR) = ""                                      '172
  Label1(P_cvInt2Al) = "2^ aliquota"                             '173
  Label1(P_cvInt2AlR) = ""                                       '174
  Label1(P_cvInt2Max) = "Massimale 2^ aliquota"                  '175
  Label1(P_cvInt2MaxR) = ""                                      '176
  Label1(P_cvInt3Al) = "3^ aliquota"                             '177
  Label1(P_cvInt3AlR) = ""                                       '178
  '--- 5.4 Assistenziale ----
  Label1(P_cvAss0Min) = "Minimo"                                 '180
  Label1(P_cvAss0MinR) = "Intera"                                '181*
  Label1(P_cvAss1Al) = "Aliquota"                                '182
  Label1(P_cvass1AlR) = "Ridotta"                                '183*
  '--- 5.5 Altro ----
  Label1(P_cvMat0Min) = "Maternit�"                              '190
  Label1(P_cvMat0MinR) = "Ridotta"                               '191*
  Label1(P_cvMat0MinR + 1) = "Intera"                            '192*
  '20121018LC (fine)
  '****************************
  'Frame MISURA PRESTAZIONI (6)
  '****************************
  '--- 6.1 Scaglioni IRPEF ----
  Label1(P_cvScA) = "Scaglione A"                                '201
  Label1(P_cvAlA) = "Aliquota"                                   '202*
  Label1(P_cvScB) = "Scaglione B"                                '203
  Label1(P_cvAlB) = "Tetto di reddito"                           '204*
  Label1(P_cvScC) = "Scaglione C"                                '205
  Label1(P_cvAlC) = ""                                           '206
  Label1(P_cvScD) = "Scaglione D"                                '207
  Label1(P_cvAlD) = ""                                           '208
  Label1(P_cvScE) = "(Scaglione E)"                              '209
  Label1(P_cvAlE) = ""                                           '210
  Label1(P_cvScF) = "(Scaglione F)"                              '211
  Label1(P_cvAlF) = ""                                           '212
  Label1(P_cvScG) = "(Scaglione G)"                              '213
  Label1(P_cvAlG) = ""                                           '214
  '--- 6.2 Pensione ----
  Label1(P_cvPMin) = "Pensione minima"                           '215
  Label1(P_cvNMR) = "n� migliori redditi"                        '216
  Label1(P_cvNTR) = "su n� totale di redditi"                    '217
  '--- 6.3 Supplementi di pensione ----
  Label1(P_cvSpAnni) = "Anni di calcolo supplemento"             '218
  Label1(P_cvSpEtaMax) = "Et� minima fine supplementi"           '219
  Label1(P_cvSpPrcCon) = "% di contributi da restituire"         '220
  'Label1(P_cvSpPrcRiv) = "Tasso di rivalut. contributi"         '221
  '--- 6.4 Reversibilit� superstiti ----
  Label1(P_cvAv) = "Vedovo/a solo"                               '222
  Label1(P_cvAvo) = " Vedovo/a e 1 orfano"                       '223
  Label1(P_cvAvoo) = "Vedovo/a e 2 orfani"                       '224
  Label1(P_cvAo) = "Orfano solo"                                 '225
  Label1(P_cvAvoo) = "2 orfani soli"                             '226
  Label1(P_cvAooo) = "3 orfani soli"                             '227
  'Label1(P_cvAgen) = "Genitori"                                 '228
  'Label1(P_cvAfra) = "Fratelli"                                 '229
  '************************
  'Frame NUOVI ISCRITTI (7)
  '************************
  Label1(P_niTipo) = ""                                          '251
  Label1(P_niP) = "Nuovi iscritti puntuali"                      '252
  '--- 20150527LC (inizio)
  Label1(P_niStat) = "Base statistica nuovi icritti"             '253
  Label1(P_neStat) = "Base statistica silenti riattivati"        '254
  Label1(P_nvStat) = "Base statistica volontari riattivati"      '255
  '*******************
  'Frame PARAMETRI (1)
  '*******************
  p_t.iFrame = F_PARAM
  i = P_cv1: appl.parm(i) = LCase(req("c" & i)) = "on"  '20120131LC
  If appl.parm(P_cv1) = False Then
    '--- Parametri ---
    l = "Parametri-"
    If Not IsDate(req("t" & P_btAnno)) Then
      p_t.iErr = ERR_TEXT: p_t.szErr = "Data Bilancio Tecnico non valida"
      p_t.iFocus = P_btAnno
      GoTo ExitPoint
    End If
    '20080416LC (inizio)
    If Val(req("t" & P_btAnniEl1)) < 1 Or Val(req("t" & P_btAnniEl1)) > 99 Then
      p_t.iErr = ERR_TEXT: p_t.szErr = "Arco temporale della valutazione non valido"
      p_t.iFocus = P_btAnniEl1
      GoTo ExitPoint
    End If
    '20080416LC (fine)
    i = P_btPatrim
      p_t.szErr = verNum(1, req("t" & i), 0, 1E+20, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_btSpAmm
      p_t.szErr = verNum(1, req("t" & i), 0, 1E+20, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_btSpAss
      p_t.szErr = verNum(1, req("t" & i), 0, 1E+20, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_btPassSIV
      p_t.szErr = verNum(1, req("t" & i), 0, 1E+20, l & Label1(P_btPassSIV))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_btPassSIP
      p_t.szErr = verNum(1, req("t" & i), 0, percMAX, l & Label1(P_btPassSIV))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    '--- 1.2 Tipo di calcolo ----
    If Not (req("r" & P_TipoElab) >= 0) Then
      p_t.iErr = ERR_TEXT: p_t.szErr = "Tipo di calcolo non selezionato"
      p_t.iFocus = P_TipoElab
      GoTo ExitPoint
    ElseIf req("r" & P_TipoElab) = 2 And Val(req("t" & P_TipoElab)) = 0 Then
      p_t.iErr = ERR_TEXT: p_t.szErr = "Anno di riferimento per calcolo misto non valido"
      p_t.iFocus = P_TipoElab
      GoTo ExitPoint
    End If
    '--- 1.3 Modalit� di analisi ---
    If req("t" & P_SQL) = "" Then
      p_t.iErr = ERR_TEXT: p_t.szErr = "Criterio di selezione non valido"
      p_t.iFocus = P_SQL
      GoTo ExitPoint
    End If
    If req("t" & P_FOUT) = "" Then
      p_t.iErr = ERR_TEXT: p_t.szErr = "Nome del file di output non presente"
      p_t.iFocus = P_FOUT
      GoTo ExitPoint
    End If
  End If
  '******************************
  'Frame TASSI ED ALTRE STIME (2)
  '******************************
  p_t.iFrame = F_TASSI_STIME
  i = P_cv2: appl.parm(i) = LCase(req("c" & i)) = "on" '20120131LC
  If appl.parm(P_cv2) = False Then
    '--- 2.1 Tassi di valutazione e rivalutazione ---
    l = "2.1 Tassi di valutazione e rivalutazione-"
    i = P_cvTi: p_t.szErr = verNum(1, req("t" & i), 0, TASSOMAX, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvTv: p_t.szErr = verNum(1, req("t" & i), 0, TASSOMAX, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvTev: p_t.szErr = verNum(1, req("t" & i), 0, TASSOMAX, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    '--- 20131122LC (inizio) (-TASSOMAX al posto di 0)  'ex 20131020LC
    i = P_cvTrn: p_t.szErr = verNum(1, req("t" & i), -TASSOMAX, TASSOMAX, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    '--- 2.2 Tassi di rivalutazione dei montanti contributivi ---
    l = "2.2 Tassi di rivalutazione dei montanti contributivi-"
    i = P_cvTrmc0
      p_t.szErr = verNum(1, req("t" & i), -TASSOMAX, TASSOMAX, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvTrmc1
      p_t.szErr = verNum(1, req("t" & i), -TASSOMAX, TASSOMAX, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvTrmc2
      p_t.szErr = verNum(1, req("t" & i), -TASSOMAX, TASSOMAX, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvTrmc3
      p_t.szErr = verNum(1, req("t" & i), -TASSOMAX, TASSOMAX, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvTrmc4
      p_t.szErr = verNum(1, req("t" & i), -TASSOMAX, TASSOMAX, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvTrmc5
      p_t.szErr = verNum(1, req("t" & i), -TASSOMAX, TASSOMAX, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvTrmc6
      p_t.szErr = verNum(1, req("t" & i), -TASSOMAX, TASSOMAX, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    '--- 2.3 Tassi di rivalutazione di redditi e volumi d'affari ---
    l = "2.3 Tassi di rivalutazione delle provvigioni-"
    i = P_cvTs1a
      p_t.szErr = verNum(1, req("t" & i), -TASSOMAX, TASSOMAX, l & Label1(P_cvTs1a))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvTs1p
      p_t.szErr = verNum(1, req("t" & i), -TASSOMAX, TASSOMAX, l & Label1(P_cvTs1p))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvTs1n
      p_t.szErr = verNum(1, req("t" & i), -TASSOMAX, TASSOMAX, l & Label1(P_cvTs1n))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    '--- 20131122LC (fine) (-TASSOMAX al posto di 0)
    '--- 2.4 Rivalutazione delle pensioni ---
    l = "2.4 Rivalutazione delle pensioni-"
    i = P_cvTp1Max: p_t.szErr = verNum(1, req("t" & i), 0, 1E+20, l & Label1(P_cvTp1Max))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvTp1Al: p_t.szErr = verNum(1, req("t" & i), 0, TASSOMAX, l & Label1(P_cvTp1Max))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvTp2Max: p_t.szErr = verNum(1, req("t" & i), 0, 1E+20, l & Label1(P_cvTp2Max))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvTp2Al: p_t.szErr = verNum(1, req("t" & i), 0, TASSOMAX, l & Label1(P_cvTp2Max))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvTp3Max: p_t.szErr = verNum(1, req("t" & i), 0, 1E+20, l & Label1(P_cvTp3Max))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvTp3Al: p_t.szErr = verNum(1, req("t" & i), 0, TASSOMAX, l & Label1(P_cvTp3Max))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    '--- 20161112LC (inizio)
    i = P_cvTp4Max: p_t.szErr = verNum(1, req("t" & i), 0, 1E+20, l & Label1(P_cvTp4Max))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvTp4Al: p_t.szErr = verNum(1, req("t" & i), 0, TASSOMAX, l & Label1(P_cvTp4Max))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvTp5Max: p_t.szErr = verNum(1, req("t" & i), 0, 1E+20, l & Label1(P_cvTp5Max))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvTp5Al: p_t.szErr = verNum(1, req("t" & i), 0, TASSOMAX, l & Label1(P_cvTp5Max))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    '--- 20161112LC (fine)
    '--- 2.5 Altre stime ----
    l = "2.5 Altre stime-"
    i = P_cvPrcPVattM
      p_t.szErr = verNum(1, req("t" & i), 0, TASSOMAX, l & Label1(P_cvPrcPVattM))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvPrcPVattF
      p_t.szErr = verNum(1, req("t" & i), 0, TASSOMAX, l & Label1(P_cvPrcPVattM))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvPrcPAattM
      p_t.szErr = verNum(1, req("t" & i), 0, TASSOMAX, l & Label1(P_cvPrcPAattM))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvPrcPAattF
      p_t.szErr = verNum(1, req("t" & i), 0, TASSOMAX, l & Label1(P_cvPrcPAattM))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvPrcPCattM
      p_t.szErr = verNum(1, req("t" & i), 0, TASSOMAX, l & Label1(P_cvPrcPCattM))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvPrcPCattF
      p_t.szErr = verNum(1, req("t" & i), 0, TASSOMAX, l & Label1(P_cvPrcPCattM))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
  End If
  '***********************
  'Frame BASI_TECNICHE (3)
  '***********************
  p_t.iFrame = F_BASI_TECNICHE
  i = P_cv3: appl.parm(i) = LCase(req("c" & i)) = "on" '20120131LC
  If appl.parm(P_cv3) = False Then
    '--- 3.1 Probabilit� di morte ----
    l = "Probabilit� di morte-"
'Public Const P_LAD  As Long = 71
'Public Const P_LED  As Long = 72
'Public Const P_LPD  As Long = 73
'Public Const P_LSD  As Long = 74
'Public Const P_LBD  As Long = 75
'Public Const P_LID  As Long = 76
    If req("s" & P_LAD) = "" Then
      p_t.iErr = ERR_COMBO: p_t.szErr = l & Label1(P_LAD) & " non valorizzato/a"
      p_t.iFocus = P_LAD
      GoTo ExitPoint
    End If
    If req("s" & P_LED) = "" Then
      p_t.iErr = ERR_COMBO: p_t.szErr = l & Label1(P_LED) & " non valorizzato/a"
      p_t.iFocus = P_LED
      GoTo ExitPoint
    End If
    If req("s" & P_LPD) = "" Then
      p_t.iErr = ERR_COMBO: p_t.szErr = l & Label1(P_LPD) & " non valorizzato/a"
      p_t.iFocus = P_LPD
      GoTo ExitPoint
    End If
    If req("s" & P_LSD) = "" Then
      p_t.iErr = ERR_COMBO: p_t.szErr = l & Label1(P_LSD) & " non valorizzato/a"
      p_t.iFocus = P_LSD
      GoTo ExitPoint
    End If
    If req("s" & P_LBD) = "" Then
      p_t.iErr = ERR_COMBO: p_t.szErr = l & Label1(P_LBD) & " non valorizzato/a"
      p_t.iFocus = P_LBD
      GoTo ExitPoint
    End If
    If req("s" & P_LID) = "" Then
      p_t.iErr = ERR_COMBO: p_t.szErr = l & Label1(P_LID) & " non valorizzato/a"
      p_t.iFocus = P_LID
      GoTo ExitPoint
    End If
    '--- 3.2 Altre probabilit� ----
    l = "Altre probabilit�-"
    If req("s" & P_LAV) = "" Then
      p_t.iErr = ERR_COMBO: p_t.szErr = l & Label1(P_LAV) & " non valorizzato/a"
      p_t.iFocus = P_LAV
      GoTo ExitPoint
    End If
    If req("s" & P_LAE) = "" Then
      p_t.iErr = ERR_COMBO: p_t.szErr = l & Label1(P_LAE) & " non valorizzato/a"
      p_t.iFocus = P_LAE
      GoTo ExitPoint
    End If
    If req("s" & P_LAB) = "" Then
      p_t.iErr = ERR_COMBO: p_t.szErr = l & Label1(P_LAB) & " non valorizzato/a"
      p_t.iFocus = P_LAB
      GoTo ExitPoint
    End If
    If req("s" & P_LAI) = "" Then
      p_t.iErr = ERR_COMBO: p_t.szErr = l & Label1(P_LAI) & " non valorizzato/a"
      p_t.iFocus = P_LAI
      GoTo ExitPoint
    End If
    If req("s" & P_LAW) = "" Then
      p_t.iErr = ERR_COMBO: p_t.szErr = l & Label1(P_LAW) & " non valorizzato/a"
      p_t.iFocus = P_LAW
      GoTo ExitPoint
    End If
    '--- 3.3 Linee reddituali ----
    l = "Linee reddituali-"
    If req("s" & P_LRA) = "" Then
      p_t.iErr = ERR_COMBO: p_t.szErr = l & Label1(P_LRA) & " non valorizzato/a"
      p_t.iFocus = P_LRA
      GoTo ExitPoint
    End If
    If req("s" & P_LRP) = "" Then
      p_t.iErr = ERR_COMBO: p_t.szErr = l & Label1(P_LRP) & " non valorizzato/a"
      p_t.iFocus = P_LRP
      GoTo ExitPoint
    End If
    If req("s" & P_LRN) = "" Then
      p_t.iErr = ERR_COMBO: p_t.szErr = l & Label1(P_LRN) & " non valorizzato/a"
      p_t.iFocus = P_LRN
      GoTo ExitPoint
    End If
    '--- 3.4 Coefficienti di conversione del montante in rendita ----
    l = "Coefficienti di conversione del montante in rendita-"
    If req("s" & P_CCR) = "" Then
      p_t.iErr = ERR_COMBO: p_t.szErr = l & Label1(P_CCR) & " non valorizzata"
      p_t.iFocus = P_CCR
      GoTo ExitPoint
    End If
    '--- 3.5 Superstiti ----
    If appl.bProb_Superst = True Then '20121113LC
      l = "Superstiti-"
      If req("s" & P_PF) = "" Then
        p_t.iErr = ERR_COMBO: p_t.szErr = l & Label1(P_PF) & " non valorizzato/a"
        p_t.iFocus = P_PF
        GoTo ExitPoint
      End If
      If Val(req("t" & P_ZMAX)) = 0 Then
        p_t.iErr = ERR_TEXT: p_t.szErr = l & "Et� limite degli orfani non specificata"
        p_t.iFocus = P_ZMAX
        GoTo ExitPoint
      End If
    End If
  End If
  '******************************
  'Frame REQUISITI DI ACCESSO (4)
  '******************************
  p_t.iFrame = F_REQUISITI_DI_ACCESSO
  i = P_cv4: appl.parm(i) = LCase(req("c" & i)) = "on" '20120131LC
  If appl.parm(P_cv4) = False Then
    '--- 4.1 Pensione di vecchiaia ordinaria ----
    l = "Pensione di vecchiaia ordinaria-"
    i = P_cvPveo30M: p_t.szErr = verNum(0, req("t" & i), 0, RMAX, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvPveo30F: p_t.szErr = verNum(0, req("t" & i), 0, RMAX, Label1(i - 1))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvPveo65M: p_t.szErr = verNum(0, req("t" & i), 0, RMAX, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvPveo65F: p_t.szErr = verNum(0, req("t" & i), 0, RMAX, Label1(i - 1))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvPveo98M: p_t.szErr = verNum(0, req("t" & i), 0, RMAX, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvPveo98F: p_t.szErr = verNum(0, req("t" & i), 0, RMAX, Label1(i - 1))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    '--- 4.2 Pensione di vecchiaia anticipata ----
    l = "Pensione di vecchiaia anticipata-"
    i = P_cvPvea35M: p_t.szErr = verNum(0, req("t" & i), 0, RMAX, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvPvea35F: p_t.szErr = verNum(0, req("t" & i), 0, RMAX, Label1(i - 1))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvPvea58M: p_t.szErr = verNum(0, req("t" & i), 0, RMAX, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvPvea58F: p_t.szErr = verNum(0, req("t" & i), 0, RMAX, Label1(i - 1))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    '--- 4.3 Pensione di vecchiaia posticipata ----
    i = P_cvPvep70M: p_t.szErr = verNum(0, req("t" & i), 0, RMAX, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvPvep70F: p_t.szErr = verNum(0, req("t" & i), 0, RMAX, Label1(i - 1))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    '--- 4.4 Pensione di inabilit� ----
    l = "Pensione di inabilit�-"
    i = P_cvPinab2M: p_t.szErr = verNum(0, req("t" & i), 0, RMAX, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvPinab2F: p_t.szErr = verNum(0, req("t" & i), 0, RMAX, Label1(i - 1))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvPinab10M: p_t.szErr = verNum(0, req("t" & i), 0, RMAX, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvPinab10F: p_t.szErr = verNum(0, req("t" & i), 0, RMAX, Label1(i - 1))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvPinab35M: p_t.szErr = verNum(0, req("t" & i), 0, RMAX, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvPinab35F: p_t.szErr = verNum(0, req("t" & i), 0, RMAX, Label1(i - 1))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    '--- 4.5 Pensione di invalidit� ----
    l = "Pensione di invalidit�-"
    i = P_cvPinv5M: p_t.szErr = verNum(0, req("t" & i), 0, RMAX, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvPinv5F: p_t.szErr = verNum(0, req("t" & i), 0, RMAX, Label1(i - 1))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvPinv70: p_t.szErr = verNum(1, req("t" & i), 0, RMAX, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    '--- 4.6 Pensione indiretta ----
    l = "Pensione indiretta-"
    i = P_cvPind2M: p_t.szErr = verNum(0, req("t" & i), 0, RMAX, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvPind2F: p_t.szErr = verNum(0, req("t" & i), 0, RMAX, Label1(i - 1))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvPind20M: p_t.szErr = verNum(0, req("t" & i), 0, RMAX, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvPind20F: p_t.szErr = verNum(0, req("t" & i), 0, RMAX, Label1(i - 1))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvPind30M: p_t.szErr = verNum(0, req("t" & i), 0, RMAX, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvPind30F: p_t.szErr = verNum(0, req("t" & i), 0, RMAX, Label1(i - 1))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    '--- 4.7 Restituzione dei contributi ----
    l = "Restituzione dei contributi-"
    i = P_cvRcHminM: p_t.szErr = verNum(0, req("t" & i), 0, RMAX, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvRcHminF: p_t.szErr = verNum(0, req("t" & i), 0, RMAX, Label1(i - 1))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvRcXminM: p_t.szErr = verNum(0, req("t" & i), 0, RMAX, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvRcXminF: p_t.szErr = verNum(0, req("t" & i), 0, RMAX, Label1(i - 1))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvRcPrcCon: p_t.szErr = verNum(1, req("t" & i), 0, RMAX, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    '--- 20150512LC (inizio) (commentato)
    'i = P_cvRcPrcRiv: p_t.szErr = verNum(1, req("t" & i), 1, 1, l & Label1(i))
    '  If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    '--- 20150512LC (fine)
    '--- 4.8 Totalizzazione ----
  End If
  '***********************
  'Frame CONTRIBUZIONE (5)
  '***********************
  p_t.iFrame = F_CONTRIBUZIONE
  i = P_cv5: appl.parm(i) = LCase(req("c" & i)) = "on" '20120131LC
  If appl.parm(P_cv5) = False Then
    '--- 5.1 Contribuzione ridotta ----
    '--- 5.2 Soggettivo ----
    l = "Soggettivo-"
    i = P_cvSogg0Min: p_t.szErr = verNum(1, req("t" & i), 0, RMAX, l & Label1(P_cvSogg0Min))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvSogg0MinR: p_t.szErr = verNum(1, req("t" & i), 0, RMAX, l & Label1(P_cvSogg0Min))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvSogg1Al: p_t.szErr = verNum(2, req("t" & i), 0, RMAX, l & Label1(P_cvSogg1Al))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvSogg1AlR: p_t.szErr = verNum(2, req("t" & i), 0, RMAX, l & Label1(P_cvSogg1Al))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvSogg1Max: p_t.szErr = verNum(1, req("t" & i), 0, RMAX, l & Label1(P_cvSogg1Max))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvSogg1MaxR: p_t.szErr = verNum(1, req("t" & i), 0, RMAX, l & Label1(P_cvSogg1Max))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    '--- 5.3 Integrativo / FIRR ----
    l = "FIRR-"
    i = P_cvInt0Min: p_t.szErr = verNum(1, req("t" & i), 0, RMAX, l & Label1(P_cvInt0Min))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvInt0MinR: p_t.szErr = verNum(1, req("t" & i), 0, RMAX, l & Label1(P_cvInt0Min))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvInt1Al: p_t.szErr = verNum(2, req("t" & i), 0, TASSOMAX, l & Label1(P_cvInt1Al))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvInt1AlR: p_t.szErr = verNum(2, req("t" & i), 0, TASSOMAX, l & Label1(P_cvInt1Al))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvInt1Max: p_t.szErr = verNum(1, req("t" & i), 0, RMAX, l & Label1(P_cvInt1Max))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvInt1MaxR: p_t.szErr = verNum(1, req("t" & i), 0, RMAX, l & Label1(P_cvInt1Max))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvInt2Al: p_t.szErr = verNum(2, req("t" & i), 0, TASSOMAX, l & Label1(P_cvInt2Al))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvInt2AlR: p_t.szErr = verNum(2, req("t" & i), 0, TASSOMAX, l & Label1(P_cvInt2Al))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvInt2Max: p_t.szErr = verNum(1, req("t" & i), 0, RMAX, l & Label1(P_cvInt2Max))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvInt2MaxR: p_t.szErr = verNum(1, req("t" & i), 0, RMAX, l & Label1(P_cvInt2Max))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvInt3Al: p_t.szErr = verNum(2, req("t" & i), 0, TASSOMAX, l & Label1(P_cvInt3Al))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvInt3AlR: p_t.szErr = verNum(2, req("t" & i), 0, TASSOMAX, l & Label1(P_cvInt3Al))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    '--- 5.4 Assistenziale ----
    l = "Assistenziale-"
    i = P_cvAss0Min: p_t.szErr = verNum(1, req("t" & i), 0, RMAX, l & Label1(P_cvAss0Min))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvAss0MinR: p_t.szErr = verNum(1, req("t" & i), 0, RMAX, l & Label1(P_cvAss0Min))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvAss1Al: p_t.szErr = verNum(2, req("t" & i), 0, TASSOMAX, l & Label1(P_cvAss1Al))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvass1AlR: p_t.szErr = verNum(2, req("t" & i), 0, TASSOMAX, l & Label1(P_cvAss1Al))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    '--- 5.5 Altro ----
    l = "Altro-"
    i = P_cvMat0Min: p_t.szErr = verNum(1, req("t" & i), 0, RMAX, l & Label1(P_cvMat0Min))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvMat0MinR: p_t.szErr = verNum(1, req("t" & i), 0, RMAX, l & Label1(P_cvMat0Min))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
  End If
  '****************************
  'Frame MISURA PRESTAZIONI (6)
  '****************************
  p_t.iFrame = F_MISURA_PRESTAZIONI
  i = P_cv6: appl.parm(i) = LCase(req("c" & i)) = "on" '20120131LC
  If appl.parm(P_cv6) = False Then
    '--- 6.1 Scaglioni IRPEF ----
    '--- 6.2 Pensione ----
    '--- 6.3 Supplementi di pensione ----
    l = "Supplementi di pensione-"
    i = P_cvSpAnni: p_t.szErr = verNum(0, req("t" & i), 0, 99, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvSpEtaMax: p_t.szErr = verNum(0, req("t" & i), 0, 99, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvSpPrcCon: p_t.szErr = verNum(1, req("t" & i), 0, TASSOMAX, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    'i = P_cvSpPrcRiv: p_t.szErr = verNum(1, req("t" & i), 0, TASSOMAX, l & Label1(i))
    '  If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    '--- 6.4 Reversibilit� superstiti ----
    l = "Reversibilit� superstiti-"
    i = P_cvAv: p_t.szErr = verNum(1, req("t" & i), 0, TASSOMAX, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvAvo: p_t.szErr = verNum(1, req("t" & i), 0, TASSOMAX, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvAvoo: p_t.szErr = verNum(1, req("t" & i), 0, TASSOMAX, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvAo: p_t.szErr = verNum(1, req("t" & i), 0, TASSOMAX, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvAoo: p_t.szErr = verNum(1, req("t" & i), 0, TASSOMAX, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
    i = P_cvAooo: p_t.szErr = verNum(1, req("t" & i), 0, TASSOMAX, l & Label1(i))
      If p_t.szErr <> "" Then p_t.iFocus = i: GoTo ExitPoint
  End If
  '************************
  'Frame NUOVI ISCRITTI (7)
  '************************
  p_t.iFrame = F_NUOVI_ISCRITTI
  i = P_cv7: appl.parm(i) = LCase(req("c" & i)) = "on" '20120131LC
  If appl.parm(P_cv7) = False Then
    '--- Nuovi iscritti ----
    k = req("r" & P_niTipo)
    If k >= 2 Then  '20140522LC  'ex 20121010LC
      '--- Nuovi iscritti attivi ----
      l = "nuovi iscritti attivi-"
      i = P_niStat
      If req("s" & i) = "" Then
        p_t.iErr = ERR_COMBO: p_t.szErr = l & Label1(i) & " non valorizzata"
        p_t.iFocus = i
        GoTo ExitPoint
      End If
    End If
    '--- 20150612LC (inizio)
    If 1 = 2 Then
      '--- Silenti riattivati ----
      l = "silenti riattivati-"
      i = P_neStat
      If req("s" & i) = "" Then
        p_t.iErr = ERR_COMBO: p_t.szErr = l & Label1(i) & " non valorizzata"
        p_t.iFocus = i
        GoTo ExitPoint
      End If
      '--- Volontari riattivati ----
      l = "volontari riattivati-"
      i = P_nvStat
      If req("s" & i) = "" Then
        p_t.iErr = ERR_COMBO: p_t.szErr = l & Label1(i) & " non valorizzata"
        p_t.iFocus = i
        GoTo ExitPoint
      End If
    End If
    '--- 20150612LC (fine)
    p_t.iErr = ERR_TEXT
  End If

ExitPoint:
  TAppl_Req_Check = p_t.szErr
End Function


Public Function TAppl_CreaReportt(ByRef appl As TAppl, ByRef glo As TGlobale, ByRef tb1() As Double, _
    ByRef frm As frmElab) As String
'*********************************************************************************************************************
'VERSIONI
'  20161112LC  gestione errori XLS
'  20080614LC - iTS
'  20130306LC
'*********************************************************************************************************************
  Dim bScriviXLS As Boolean
  Dim iFmt As Byte
  Dim iop% '0=txt+xls; 1=solo txt; 2=solo xls
  Dim fo&, i&, ijk&, iTS&, t& '20121028LC
  Dim Nmax#
  Dim sErr$, szFileName$, szFileNameTS$, szFileExt$
  Dim sErr1 As String  '20161112LC
  Dim xlsApp As Object
  
  sErr = ""
  sErr1 = ""
  Screen.MousePointer = vbHourglass: DoEvents
  If glo.rep_opFile(0) = True Then
    iop = 0
  ElseIf glo.rep_opFile(1) = True Then
    iop = 1
  ElseIf glo.rep_opFile(2) = True Then
    iop = 2
  End If
  ijk = appl.nSize2 * 0 + appl.nSize3 * 0 + appl.nSize4 * (0 + appl.t0 - 1960) '20121114LC   '11
  Nmax = tb1(ijk + TB_ZTOT)
  For t = 1 To appl.nMax2 + 1 '20121028LC  'UBound(appl.tb, 3)
    ijk = appl.nSize2 * 0 + appl.nSize3 * 0 + appl.nSize4 * (t + appl.t0 - 1960) '20121114LC   '12
    If Nmax < tb1(ijk + TB_ZTOT) Then
      Nmax = tb1(ijk + TB_ZTOT)
    End If
  Next t
  Select Case Nmax
  Case Is < 10: iFmt = 1
  Case Is < 100: iFmt = 2
  Case Is < 1000: iFmt = 3
  Case Is < 10000: iFmt = 4
  Case Is < 100000: iFmt = 5
  Case Is < 1000000: iFmt = 6
  Case Else: iFmt = 0
  End Select
  '****************************
  'Inizializza il file di testo
  '****************************
  For iTS = 0 To OPZ_INT_SUDDIVISIONI_N
    appl.SuddivReport(iTS) = ""
  Next iTS
  If glo.MJ = MI_VB Then  '20130306LC
    frm.rtfOutput.Text = appl.SuddivReport(0)
  End If
  szFileName = appl.parm(P_FOUT)
  i = InStr(szFileName, ".")
  If i > 0 Then
    szFileExt = Right(szFileName, Len(szFileName) - i)
    szFileName = Left(szFileName, i - 1)
  Else
    szFileExt = ""
  End If
  If ON_ERR_RN3 = True Then On Error Resume Next
  Kill szFileName & ".out"
  For i = 0 To 99 '20111224LC
    Kill szFileName & "_" & Format(i, "00") & ".out" '20111224LC
  Next i
  On Error GoTo 0
  '*****************
  'Inizializza Excel
  '*****************
  If iop <> 1 Then
    '--- 20121203LC (inizio)
    'If iFmt = 1 Then 'Formato con 6 decimali per la popolazione e 2 decimali per gli importi
    '  glo.xlsTemplate = "ModOutE.xls" '20120514LC
    'Else 'Formato
    '  glo.xlsTemplate = "ModOutE.xls" '20120514LC
    'End If
    glo.xlsTemplate = OPZ_OUTPUT_MOD
    '--- 20121203LC (fine)
    If ON_ERR_RN3 = True Then On Error Resume Next
    Set xlsApp = GetObject("Excel.Application")
    If Err.Number <> 0 Then
      Err.Clear
      Set xlsApp = CreateObject("Excel.Application")
      If Err.Number <> 0 Then
        Set xlsApp = Nothing
      End If
    End If
    On Error GoTo 0
    '---
    If ON_ERR_RN3 = True Then On Error Resume Next
    Kill "Output_" & Format(appl.idReport, "0000") & ".xls"
    For i = 0 To 99 '20111224LC
      Kill "Output_" & Format(appl.idReport, "0000") & "_" & Format(i, "00") & ".xls" '20111224LC
    Next i
    On Error GoTo 0
  End If
  bScriviXLS = Not (xlsApp Is Nothing)
  '*************************
  'Per ciascuna suddivisione
  '*************************
  For iTS = 0 To IIf(OPZ_INT_SUDDIVISIONI_N < 2, 0, OPZ_INT_SUDDIVISIONI_N)
    szFileNameTS = szFileName & IIf(iTS > 0, "_" & Format(iTS - 1, "00"), "") & "." & szFileExt '20111224LC
    If iop < 2 Then
      If ON_ERR_RN = True Then On Error Resume Next
      fo = fopen(szFileNameTS, "w")
      If Err.Number <> 0 Then
        sErr = "Impossibile aprire il file di output " & szFileNameTS
        GoTo ExitPoint
      End If
      On Error GoTo 0
    End If
    '---
    If Not xlsApp Is Nothing Then
      glo.xlsOutput = "Output_" & Format(appl.idReport, "0000")
      '--- 20161113LC (inizio)
      If appl.iterNum > 0 Then
        glo.xlsOutput = glo.xlsOutput & "_i" & appl.iterNum
      End If
      If iTS > 0 Then '20111224LC
        glo.xlsOutput = glo.xlsOutput & "_" & Format(iTS - 1, "00")
      End If
      glo.xlsOutput = glo.xlsOutput & ".xls"
      '--- 20161113LC (fine)
      bScriviXLS = ("" = xlsApri(xlsApp, glo.xlsTemplate, glo.xlsOutput))
    End If
    Screen.MousePointer = vbHourglass: DoEvents
    '20080926LC (inizio)
    Call TAppl_Stampa3a(appl, glo, fo, xlsApp, iop, iTS)
    '--- 20161112LC (inizio)
    Call TAppl_Stampa3c(appl, glo, appl.tb1, fo, xlsApp, iop, iFmt, iTS, sErr)   'ex 20080614LC
    '20080926LC (fine)
    If bScriviXLS = True Then
      sErr1 = xlsChiudi(xlsApp, glo.xlsOutput)
      If sErr = "" And sErr1 <> "" Then
        sErr = sErr1
      End If
    End If
    '--- 20161112LC (fine)
    Screen.MousePointer = vbHourglass: DoEvents
    '---
    If iop < 2 Then
      Call fclose(fo)
      fo = FreeFile
      Open szFileNameTS For Input As #fo
      appl.SuddivReport(iTS) = Input$(LOF(fo), fo)
      Call fclose(fo)
    End If
    '--- 20181108LC (inizio) MEF
    If OPZ_DBXMEF <> 0 And iTS = 0 Then
      Call TAppl_Stampa3e6(appl, appl.tbMEF, glo.db)
    End If
    '--- 20181108LC (fine) MEF
  Next iTS
  Set xlsApp = Nothing
  ''''frm.rtf1(0).Text = glo.szReport(0)
  
ExitPoint:
  Screen.MousePointer = vbDefault
  If iop < 2 Then
    Call fclose(fo)
  End If
  'If xlsApp Is Nothing And Not glo.xlsApp Is Nothing Then
  '  call MsgBox("Non � stato possibile creare il file Excel di output"
  'End If
  '--- 20161112LC (inizio)
  If sErr <> "" Then
    'Call MsgBox("Si � verificato il seguente errore:" & vbCrLf & sErr, vbExclamation)
  End If
  TAppl_CreaReportt = sErr
  '--- 20161112LC (fine)
End Function


Public Sub TAppl_Mask2Req(ByRef appl As TAppl, req As Object, frm As frmElab)
'ex 20121010LC
'20130307LC (tutta)
  Dim i%, j As Byte, j1 As Byte, j2 As Byte
  
  Set req = New Collection
  '*******************
  'Frame PARAMETRI (1)
  '*******************
  i = P_cv1: req.Add CStr(frm.Check1(i).Value), "c" & i
  '--- Bilancio Tecnico ----
  i = P_btAnno:     req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_btAnniEl1:  req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_btAnniEl2:  req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_btPatrim:   req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_btSpAmm:    req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_btSpAss:    req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_btPassSIV:  req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_btPassSIP:  req.Add CStr(frm.Text1(i).Text), "t" & i
  '--- 1.2 Tipo di calcolo ----
  For i = 2 To 1 Step -1
    If frm.optTipoElab(i).Value = True Then Exit For
  Next i
  req.Add CStr(i), "r" & P_TipoElab
  i = P_TipoElab:  req.Add IIf(frm.optTipoElab(2).Value, CStr(frm.Text1(i).Text), ""), "t" & i
  '--- 1.3 Modalit� di analisi ---
  i = P_SQL:  req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_FOUT:  req.Add CStr(frm.Text1(i).Text), "t" & i
  '******************************
  'Frame TASSI ED ALTRE STIME (2)
  '******************************
  i = P_cv2: req.Add CStr(frm.Check1(i).Value), "c" & i
  '--- 2.1 Tassi di valutazione e rivalutazione ---
  i = P_cvTi:       req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvTv:       req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvTev:      req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvTrn:      req.Add CStr(frm.Text1(i).Text), "t" & i
  '--- 2.2 Tassi di rivalutazione dei montanti contributivi ---
  i = P_cvTrmc0:    req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvTrmc1:    req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvTrmc2:    req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvTrmc3:    req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvTrmc4:    req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvTrmc5:    req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvTrmc6:    req.Add CStr(frm.Text1(i).Text), "t" & i
  '--- 2.3 Tassi di rivalutazione di redditi e volumi d'affari ---
  i = P_cvTs1a:     req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvTs1p:     req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvTs1n:     req.Add CStr(frm.Text1(i).Text), "t" & i
  '--- 2.4 Rivalutazione delle pensioni ---
  i = P_cvTp1Max:   req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvTp1Al:    req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvTp2Max:   req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvTp2Al:    req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvTp3Max:   req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvTp3Al:    req.Add CStr(frm.Text1(i).Text), "t" & i
  '--- 20161112LC (inizio)
  i = P_cvTp4Max:   req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvTp4Al:    req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvTp5Max:   req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvTp5Al:    req.Add CStr(frm.Text1(i).Text), "t" & i
  '--- 20161112LC (fine)
  '--- 2.5 Altre stime ----
  i = P_cvPrcPVattM: req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvPrcPVattF: req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvPrcPAattM: req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvPrcPAattF: req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvPrcPCattM: req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvPrcPCattF: req.Add CStr(frm.Text1(i).Text), "t" & i
  '***********************
  'Frame BASI_TECNICHE (3)
  '***********************
  i = P_cv3: req.Add CStr(frm.Check1(i).Value), "c" & i
  '--- 3.1 Probabilit� di morte ----
  i = P_LAD: req.Add CStr(frm.Combo1(i).Text), "s" & i
  i = P_LED: req.Add CStr(frm.Combo1(i).Text), "s" & i
  i = P_LPD: req.Add CStr(frm.Combo1(i).Text), "s" & i
  i = P_LSD: req.Add CStr(frm.Combo1(i).Text), "s" & i
  i = P_LBD: req.Add CStr(frm.Combo1(i).Text), "s" & i
  i = P_LID: req.Add CStr(frm.Combo1(i).Text), "s" & i
  '--- 3.2 Altre probabilit� ----
  i = P_LAV: req.Add CStr(frm.cbxQav.Text), "s" & i
  i = P_LAE: req.Add CStr(frm.cbxQae.Text), "s" & i
  i = P_LAB: req.Add CStr(frm.Combo1(i).Text), "s" & i
  i = P_LAI: req.Add CStr(frm.Combo1(i).Text), "s" & i
  i = P_LAW: req.Add CStr(frm.Combo1(i).Text), "s" & i
  '--- 3.3 Linee reddituali ----
  i = P_LRA: req.Add CStr(frm.Combo1(i).Text), "s" & i
  i = P_LRP: req.Add CStr(frm.Combo1(i).Text), "s" & i
  i = P_LRN: req.Add CStr(frm.Combo1(i).Text), "s" & i
  '--- 3.4 Coefficienti di conversione del montante in rendita ----
  i = P_CCR: req.Add CStr(frm.Combo1(i).Text), "s" & i
  '--- 3.5 Superstiti ----
  i = P_PF: req.Add CStr(frm.Combo1(i).Text), "s" & i
  i = P_ZMAX: req.Add CStr(frm.Text1(i).Text), "t" & i
  '******************************
  'Frame REQUISITI DI ACCESSO (4)
  '******************************
  i = P_cv4: req.Add CStr(frm.Check1(i).Value), "c" & i
  '--- 4.1 Pensione di vecchiaia ordinaria ----
  i = P_cvPveo30M:  req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvPveo30F:  req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvPveo65M:  req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvPveo65F:  req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvPveo98M:  req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvPveo98F:  req.Add CStr(frm.Text1(i).Text), "t" & i
  '--- 4.2 Pensione di vecchiaia anticipata ----
  i = P_cvPvea35M:  req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvPvea35F:  req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvPvea58M:  req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvPvea58F:  req.Add CStr(frm.Text1(i).Text), "t" & i
  '--- 4.3 Pensione di vecchiaia posticipata ----
  i = P_cvPvep70M:  req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvPvep70F:  req.Add CStr(frm.Text1(i).Text), "t" & i
  '--- 4.4 Pensione di inabilit� ----
  i = P_cvPinab2M:  req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvPinab2F:  req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvPinab10M: req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvPinab10F: req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvPinab35M: req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvPinab35F: req.Add CStr(frm.Text1(i).Text), "t" & i
  '--- 4.5 Pensione di invalidit� ----
  i = P_cvPinv5M:   req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvPinv5F:   req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvPinv70:   req.Add CStr(frm.Text1(i).Text), "t" & i
  '--- 4.6 Pensione indiretta ----
  i = P_cvPind2M:   req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvPind2F:   req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvPind20M:  req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvPind20F:  req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvPind30M:  req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvPind30F:  req.Add CStr(frm.Text1(i).Text), "t" & i
  '--- 4.7 Restituzione dei contributi ----
  i = P_cvRcHminM:  req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvRcHminF:  req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvRcXminM:  req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvRcXminF:  req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvRcPrcCon: req.Add CStr(frm.Text1(i).Text), "t" & i
  'i = P_cvRcPrcRiv: req.Add CStr(frm.Text1(i).Text), "t" & i  '20150512LC (commentato)
  '--- 4.8 Totalizzazione ----
  '***********************
  'Frame CONTRIBUZIONE (5)
  '***********************
  i = P_cv5: req.Add CStr(frm.Check1(i).Value), "c" & i
  '--- 5.1 Contribuzione ridotta ----
  '--- 5.2 Soggettivo ----
  i = P_cvSogg0Min:  req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvSogg0MinR: req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvSogg1Al:   req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvSogg1AlR:  req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvSogg1Max:  req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvSogg1MaxR: req.Add CStr(frm.Text1(i).Text), "t" & i
  '--- 5.3 Integrativo / FIRR ----
  i = P_cvInt0Min:   req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvInt0MinR:  req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvInt1Al:    req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvInt1AlR:   req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvInt1Max:   req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvInt1MaxR:  req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvInt2Al:    req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvInt2AlR:   req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvInt2Max:   req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvInt2MaxR:  req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvInt3Al:    req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvInt3AlR:   req.Add CStr(frm.Text1(i).Text), "t" & i
  '--- 5.4 Assistenziale ----
  i = P_cvAss0Min:   req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvAss0MinR:  req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvAss1Al:    req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvass1AlR:   req.Add CStr(frm.Text1(i).Text), "t" & i
  '--- 5.5 Altro ----
  i = P_cvMat0Min:   req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvMat0MinR:  req.Add CStr(frm.Text1(i).Text), "t" & i
  '****************************
  'Frame MISURA PRESTAZIONI (6)
  '****************************
  i = P_cv6: req.Add CStr(frm.Check1(i).Value), "c" & i
  '--- 6.1 Scaglioni IRPEF ---
  '--- 6.2 Pensione ----
  '--- 6.3 Supplementi di pensione ----
  i = P_cvSpAnni:    req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvSpEtaMax:  req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvSpPrcCon:  req.Add CStr(frm.Text1(i).Text), "t" & i
  'i = P_cvSpPrcRiv:  req.Add CStr(frm.Text1(i).Text), "t" & i
  '--- 6.4 Reversibilit� superstiti ----
  i = P_cvAv:        req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvAvo:       req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvAvoo:      req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvAo:        req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvAoo:       req.Add CStr(frm.Text1(i).Text), "t" & i
  i = P_cvAooo:      req.Add CStr(frm.Text1(i).Text), "t" & i
  '************************
  'Frame NUOVI ISCRITTI (7)
  '************************
  i = P_cv7: req.Add CStr(frm.ckxcv7.Value), "c" & i
  If frm.optNI_3.Value = True Then  '20140522LC
    i = 3
  ElseIf frm.optNI_2.Value = True Then
    i = 2
  ElseIf frm.optNI_1.Value = True Then
    i = 1
  Else
    i = 0
  End If
  req.Add CStr(i), "r" & P_niTipo
  req.Add CStr(frm.cbxNip.Text), "s" & P_niP
  req.Add IIf(frm.optNI_3.Value = True Or frm.optNI_2.Value = True, CStr(frm.cbxNiStat.Text), ""), "s" & P_niStat  '20140522LC
  '--- 20150527LC (inizio)
  req.Add IIf(1, CStr(frm.cbxNeStat.Text), ""), "s" & P_neStat
  req.Add IIf(1, CStr(frm.cbxNvStat.Text), ""), "s" & P_nvStat
  '--- 20150527LC (fine)
End Sub


'20121010LC (tutta)
Public Sub TAppl_Req2Dati(ByRef appl As TAppl, req As Object)
  Dim i%, j As Byte, j1 As Byte, j2 As Byte
 
  '*******************
  'Frame PARAMETRI (1)
  '*******************
  appl.parm(P_cv1) = req("c" & P_cv1) = vbChecked Or LCase(req("c" & P_cv1)) = "on"  '20130307LC
  '--- Bilancio Tecnico ----
  appl.parm(P_btAnno) = Year(req("t" & P_btAnno))
    appl.t0 = appl.parm(P_btAnno)
    appl.DataBilancio = "31/12/" & appl.t0
    appl.DataInizio = appl.DataBilancio + 1
  appl.parm(P_btAnniEl1) = Int(req("t" & P_btAnniEl1))
  appl.parm(P_btAnniEl2) = Int(req("t" & P_btAnniEl2))
  appl.parm(P_btPatrim) = CDbl(req("t" & P_btPatrim))
  appl.parm(P_btSpAmm) = CDbl(req("t" & P_btSpAmm))
  appl.parm(P_btSpAss) = CDbl(req("t" & P_btSpAss))
  appl.parm(P_btPassSIV) = CDbl(req("t" & P_btPassSIV))
  appl.parm(P_btPassSIP) = CDbl(req("t" & P_btPassSIP))
  '--- 1.2 Tipo di calcolo ----
  If req("r" & P_TipoElab) < 2 Then
    appl.parm(P_TipoElab) = req("r" & P_TipoElab)
  Else
    appl.parm(P_TipoElab) = req("t" & P_TipoElab)
  End If
  '--- 1.3 Modalit� di analisi ---
  appl.parm(P_SQL) = Sql_Normalize(LCase(req("t" & P_SQL))) '20120213LC
  appl.parm(P_FOUT) = LCase(req("t" & P_FOUT))
  '******************************
  'Frame TASSI ED ALTRE STIME (2)
  '******************************
  appl.parm(P_cv2) = req("c" & P_cv2) = vbChecked Or LCase(req("c" & P_cv2)) = "on"  '20130307LC
  '--- 2.1 Tassi di valutazione e rivalutazione ---
  appl.parm(P_cvTi) = CDbl(req("t" & P_cvTi))
  appl.parm(P_cvTv) = CDbl(req("t" & P_cvTv))
  appl.parm(P_cvTev) = CDbl(req("t" & P_cvTev))
  appl.parm(P_cvTrn) = CDbl(req("t" & P_cvTrn))
  '--- 2.2 Tassi di rivalutazione dei montanti contributivi ---
  '20121008LC (inizio)
  appl.parm(P_cvTrmc0) = CDbl(req("t" & P_cvTrmc0))
  '20121008LC (fine)
  appl.parm(P_cvTrmc1) = CDbl(req("t" & P_cvTrmc1))
  appl.parm(P_cvTrmc2) = CDbl(req("t" & P_cvTrmc2))
  appl.parm(P_cvTrmc3) = CDbl(req("t" & P_cvTrmc3))
  appl.parm(P_cvTrmc4) = CDbl(req("t" & P_cvTrmc4))
  appl.parm(P_cvTrmc5) = CDbl(req("t" & P_cvTrmc5))
  appl.parm(P_cvTrmc6) = CDbl(req("t" & P_cvTrmc6))
  '--- 2.3 Tassi di rivalutazione di redditi e volumi d'affari ---
  appl.parm(P_cvTs1a) = CDbl(req("t" & P_cvTs1a))
  appl.parm(P_cvTs1p) = CDbl(req("t" & P_cvTs1p))
  appl.parm(P_cvTs1n) = CDbl(req("t" & P_cvTs1n))
  '--- 2.4 Rivalutazione delle pensioni ---
  appl.parm(P_cvTp1Max) = CDbl(req("t" & P_cvTp1Max))
  appl.parm(P_cvTp1Al) = CDbl(req("t" & P_cvTp1Al))
  appl.parm(P_cvTp2Max) = CDbl(req("t" & P_cvTp2Max))
  appl.parm(P_cvTp2Al) = CDbl(req("t" & P_cvTp2Al))
  appl.parm(P_cvTp3Max) = CDbl(req("t" & P_cvTp3Max))
  appl.parm(P_cvTp3Al) = CDbl(req("t" & P_cvTp3Al))
  '--- 20161112LC (inizio)
  appl.parm(P_cvTp4Max) = CDbl(req("t" & P_cvTp4Max))
  appl.parm(P_cvTp4Al) = CDbl(req("t" & P_cvTp4Al))
  appl.parm(P_cvTp5Max) = CDbl(req("t" & P_cvTp5Max))
  appl.parm(P_cvTp5Al) = CDbl(req("t" & P_cvTp5Al))
  '--- 20161112LC (fine)
  '--- 2.5 Altre stime ----
  appl.parm(P_cvPrcPVattM) = CDbl(req("t" & P_cvPrcPVattM))
  appl.parm(P_cvPrcPVattF) = CDbl(req("t" & P_cvPrcPVattF))
  appl.parm(P_cvPrcPAattM) = CDbl(req("t" & P_cvPrcPAattM))
  appl.parm(P_cvPrcPAattF) = CDbl(req("t" & P_cvPrcPAattF))
  appl.parm(P_cvPrcPCattM) = CDbl(req("t" & P_cvPrcPCattM))
  appl.parm(P_cvPrcPCattF) = CDbl(req("t" & P_cvPrcPCattF))
  '***********************
  'Frame BASI_TECNICHE (3)
  '***********************
  appl.parm(P_cv3) = req("c" & P_cv3) = vbChecked Or LCase(req("c" & P_cv3)) = "on"  '20130307LC
  '--- 3.1 Probabilit� di morte ----
  appl.parm(P_LAD) = req("s" & P_LAD)
  appl.parm(P_LED) = req("s" & P_LED)
  appl.parm(P_LPD) = req("s" & P_LPD)
  appl.parm(P_LSD) = req("s" & P_LSD)
  appl.parm(P_LBD) = req("s" & P_LBD)
  appl.parm(P_LID) = req("s" & P_LID)
  '--- 3.2 Altre probabilit� ----
  appl.parm(P_LAV) = req("s" & P_LAV)
  appl.parm(P_LAE) = req("s" & P_LAE)
  appl.parm(P_LAB) = req("s" & P_LAB)
  appl.parm(P_LAI) = req("s" & P_LAI)
  appl.parm(P_LAW) = req("s" & P_LAW)
  '--- 3.3 Linee reddituali ----
  appl.parm(P_LRA) = req("s" & P_LRA)
  appl.parm(P_LRP) = req("s" & P_LRP)
  appl.parm(P_LRN) = req("s" & P_LRN)
  '--- 3.4 Coefficienti di conversione del montante in rendita ----
  appl.parm(P_CCR) = req("s" & P_CCR)
  '--- 3.5 Superstiti ----
  appl.parm(P_PF) = req("s" & P_PF)
  appl.parm(P_ZMAX) = CByte(req("t" & P_ZMAX))
  '******************************
  'Frame REQUISITI DI ACCESSO (4)
  '******************************
  appl.parm(P_cv4) = req("c" & P_cv4) = vbChecked Or LCase(req("c" & P_cv4)) = "on"  '20130307LC
  '--- 4.1 Pensione di vecchiaia ordinaria ----
  appl.parm(P_cvPveo30M) = CDbl(req("t" & P_cvPveo30M))
  appl.parm(P_cvPveo30F) = CDbl(req("t" & P_cvPveo30F))
  appl.parm(P_cvPveo65M) = CDbl(req("t" & P_cvPveo65M))
  appl.parm(P_cvPveo65F) = CDbl(req("t" & P_cvPveo65F))
  appl.parm(P_cvPveo98M) = CDbl(req("t" & P_cvPveo98M))
  appl.parm(P_cvPveo98F) = CDbl(req("t" & P_cvPveo98F))
  '--- 4.2 Pensione di vecchiaia anticipata ----
  appl.parm(P_cvPvea35M) = CDbl(req("t" & P_cvPvea35M))
  appl.parm(P_cvPvea35F) = CDbl(req("t" & P_cvPvea35F))
  appl.parm(P_cvPvea58M) = CDbl(req("t" & P_cvPvea58M))
  appl.parm(P_cvPvea58F) = CDbl(req("t" & P_cvPvea58F))
  '--- 4.3 Pensione di vecchiaia posticipata ----
  appl.parm(P_cvPvep70M) = CDbl(req("t" & P_cvPvep70M))
  appl.parm(P_cvPvep70F) = CDbl(req("t" & P_cvPvep70F))
  '--- 4.4 Pensione di inabilit� ----
  appl.parm(P_cvPinab2M) = Int(req("t" & P_cvPinab2M))
  appl.parm(P_cvPinab2F) = Int(req("t" & P_cvPinab2F))
  appl.parm(P_cvPinab10M) = Int(req("t" & P_cvPinab10M))
  appl.parm(P_cvPinab10F) = Int(req("t" & P_cvPinab10F))
  appl.parm(P_cvPinab35M) = Int(req("t" & P_cvPinab35M))
  appl.parm(P_cvPinab35F) = Int(req("t" & P_cvPinab35F))
  '--- 4.4 Pensione di invalidit� ----
  appl.parm(P_cvPinv5M) = Int(req("t" & P_cvPinv5M))
  appl.parm(P_cvPinv5F) = Int(req("t" & P_cvPinv5F))
  appl.parm(P_cvPinv70) = CDbl(req("t" & P_cvPinv70))
  '--- 4.5 Pensione indiretta ----
  appl.parm(P_cvPind2M) = Int(req("t" & P_cvPind2M))
  appl.parm(P_cvPind2F) = Int(req("t" & P_cvPind2F))
  appl.parm(P_cvPind20M) = CDbl(req("t" & P_cvPind20M))
  appl.parm(P_cvPind20F) = CDbl(req("t" & P_cvPind20F))
  appl.parm(P_cvPind30M) = CDbl(req("t" & P_cvPind30M))
  appl.parm(P_cvPind30F) = CDbl(req("t" & P_cvPind30F))
  '--- 4.6 Totalizzazione ----
  '--- 4.7 Restituzione dei contributi ----
  appl.parm(P_cvRcHminM) = CDbl(req("t" & P_cvRcHminM))
  appl.parm(P_cvRcHminF) = CDbl(req("t" & P_cvRcHminF))
  appl.parm(P_cvRcXminM) = CDbl(req("t" & P_cvRcXminM))
  appl.parm(P_cvRcXminF) = CDbl(req("t" & P_cvRcXminF))
  appl.parm(P_cvRcPrcCon) = CDbl(req("t" & P_cvRcPrcCon))
  'appl.parm(P_cvRcPrcRiv) = CDbl(req("t" & P_cvRcPrcRiv))  '20150512LC (commentato)
  '***********************
  'Frame CONTRIBUZIONE (5)
  '***********************
  appl.parm(P_cv5) = req("c" & P_cv5) = vbChecked Or LCase(req("c" & P_cv5)) = "on"  '20130307LC
  '--- 5.1 Contribuzione ridotta ----
  '--- 5.2 Soggettivo ----
  appl.parm(P_cvSogg0Min) = CDbl(req("t" & P_cvSogg0Min))
  appl.parm(P_cvSogg0MinR) = CDbl(req("t" & P_cvSogg0MinR))
  appl.parm(P_cvSogg1Al) = CDbl(req("t" & P_cvSogg1Al))
  appl.parm(P_cvSogg1AlR) = CDbl(req("t" & P_cvSogg1AlR))
  appl.parm(P_cvSogg1Max) = CDbl(req("t" & P_cvSogg1Max))
  appl.parm(P_cvSogg1MaxR) = CDbl(req("t" & P_cvSogg1MaxR))
  '--- 5.3 Integrativo / FIRR ----
  appl.parm(P_cvInt0Min) = CDbl(req("t" & P_cvInt0Min))
  appl.parm(P_cvInt0MinR) = CDbl(req("t" & P_cvInt0MinR))
  appl.parm(P_cvInt1Al) = CDbl(req("t" & P_cvInt1Al))
  appl.parm(P_cvInt1AlR) = CDbl(req("t" & P_cvInt1AlR))
  appl.parm(P_cvInt1Max) = CDbl(req("t" & P_cvInt1Max))
  appl.parm(P_cvInt1MaxR) = CDbl(req("t" & P_cvInt1MaxR))
  appl.parm(P_cvInt2Al) = CDbl(req("t" & P_cvInt2Al))
  appl.parm(P_cvInt2AlR) = CDbl(req("t" & P_cvInt2AlR))
  appl.parm(P_cvInt2Max) = CDbl(req("t" & P_cvInt2Max))
  appl.parm(P_cvInt2MaxR) = CDbl(req("t" & P_cvInt2MaxR))
  appl.parm(P_cvInt3Al) = CDbl(req("t" & P_cvInt3Al))
  appl.parm(P_cvInt3AlR) = CDbl(req("t" & P_cvInt3AlR))
  '--- 5.4 Assistenziale ----
  appl.parm(P_cvAss0Min) = CDbl(req("t" & P_cvAss0Min))
  appl.parm(P_cvAss0MinR) = CDbl(req("t" & P_cvAss0MinR))
  appl.parm(P_cvAss1Al) = CDbl(req("t" & P_cvAss1Al))
  appl.parm(P_cvass1AlR) = CDbl(req("t" & P_cvass1AlR))
  '--- 5.5 Altro ----
  appl.parm(P_cvMat0Min) = CDbl(req("t" & P_cvMat0Min))
  appl.parm(P_cvMat0MinR) = CDbl(req("t" & P_cvMat0MinR))
  '****************************
  'Frame MISURA PRESTAZIONI (6)
  '****************************
  appl.parm(P_cv6) = req("c" & P_cv6) = vbChecked Or LCase(req("c" & P_cv6)) = "on"  '20130307LC
  '--- 6.1 Scaglioni IRPEF ---
  '--- 6.2 Pensione ----
  '--- 6.3 Supplementi di pensione ----
  appl.parm(P_cvSpAnni) = Int(req("t" & P_cvSpAnni))
  appl.parm(P_cvSpEtaMax) = Int(req("t" & P_cvSpEtaMax))
  appl.parm(P_cvSpPrcCon) = CDbl(req("t" & P_cvSpPrcCon))
  'appl.parm(P_cvSpPrcRiv) = CDbl(req("t" & P_cvSpPrcRiv))
  '--- 6.4 Reversibilit� superstiti ----
  appl.parm(P_cvAv) = CDbl(req("t" & P_cvAv))
  appl.parm(P_cvAvo) = CDbl(req("t" & P_cvAvo))
  appl.parm(P_cvAvoo) = CDbl(req("t" & P_cvAvoo))
  appl.parm(P_cvAo) = CDbl(req("t" & P_cvAo))
  appl.parm(P_cvAoo) = CDbl(req("t" & P_cvAoo))
  appl.parm(P_cvAooo) = CDbl(req("t" & P_cvAooo))
  '************************
  'Frame NUOVI ISCRITTI (7)
  '************************
  appl.parm(P_cv7) = req("c" & P_cv7) = vbChecked Or LCase(req("c" & P_cv7)) = "on"  '20130307LC
  '20120319LC (inizio)
  i = req("r" & P_niTipo)
  appl.parm(P_niTipo) = i
  appl.parm(P_niP) = IIf(i >= 2, req("s" & P_niP), "")  '20140522LC
  appl.parm(P_niStat) = IIf(i >= 2, req("s" & P_niStat), "")
  '20120319LC (fine)
  '20150527LC (inizio)
  appl.parm(P_neStat) = IIf(1 = 1, req("s" & P_neStat), "")
  appl.parm(P_nvStat) = IIf(1 = 1, req("s" & P_nvStat), "")
  '20150527LC (fine)
End Sub


Public Sub TAppl_DatiPut(ByRef appl As TAppl, ByRef glo As TGlobale, req As Object, frm As Object)
'ex 20121010LC
'20130307LC
  Call TAppl_Dati2Req(appl, glo, req)
  If glo.MJ = MI_VB Then
    Call TAppl_Req2Mask(appl, req, frm)
  End If
End Sub


Public Sub TAppl_Dati2Req(ByRef appl As TAppl, ByRef glo As TGlobale, req As Object)
'20121010LC (tutta)
  Dim bEnabledNI As Boolean
  Dim j As Byte, j1 As Byte, j2 As Byte
  Dim i%, k%
  
  '*******************
  'Frame PARAMETRI (1)
  '*******************
  Set req = New Collection
  req.Add glo.szGruppo, "s" & P_ID
  req.Add IIf(appl.parm(P_cv1) = True, "on", ""), "c" & P_cv1  '20130307LC
  '--- Bilancio Tecnico ----
  req.Add CStr(appl.DataBilancio), "t" & P_btAnno
  i = P_btAnniEl1: req.Add CStr(appl.parm(i)), "t" & i
  i = P_btAnniEl2: req.Add CStr(appl.parm(i)), "t" & i
  i = P_btPatrim:  req.Add CStr(appl.parm(i)), "t" & i
  i = P_btSpAmm:   req.Add CStr(appl.parm(i)), "t" & i
  i = P_btSpAss:   req.Add CStr(appl.parm(i)), "t" & i
  i = P_btPassSIV: req.Add CStr(appl.parm(i)), "t" & i
  i = P_btPassSIP: req.Add CStr(appl.parm(i)), "t" & i
  '--- 1.2 Tipo di calcolo ----
  i = P_TipoElab
  If appl.parm(i) < 2 Then
    req.Add CStr(appl.parm(i)), "r" & i
  Else
    req.Add CStr(2), "r" & i
    req.Add CStr(appl.parm(i)), "t" & i
  End If
  '--- 1.3 Modalit� di analisi ---
  i = P_SQL: req.Add Sql_Normalize(LCase(appl.parm(P_SQL))), "t" & i
  i = P_FOUT: req.Add LCase$(appl.parm(i)), "t" & i
  '******************************
  'Frame TASSI ED ALTRE STIME (2)
  '******************************
  req.Add IIf(appl.parm(P_cv2) = True, "on", ""), "c" & P_cv2  '20130307LC
  '--- 2.1 Tassi di valutazione e rivalutazione ---
  i = P_cvTv: req.Add CStr(appl.parm(P_cvTv)), "t" & i
  i = P_cvTi: req.Add CStr(appl.parm(P_cvTi)), "t" & i
  i = P_cvTev: req.Add CStr(appl.parm(P_cvTev)), "t" & i
  i = P_cvTrn: req.Add CStr(appl.parm(P_cvTrn)), "t" & i
  '--- 2.2 Tassi di rivalutazione dei montanti contributivi ---
  '20121008LC (inizio)
  i = P_cvTrmc0: req.Add CStr(appl.parm(P_cvTrmc0)), "t" & i
  '20121008LC (fine)
  i = P_cvTrmc1: req.Add CStr(appl.parm(P_cvTrmc1)), "t" & i
  i = P_cvTrmc2: req.Add CStr(appl.parm(P_cvTrmc2)), "t" & i
  i = P_cvTrmc3: req.Add CStr(appl.parm(P_cvTrmc3)), "t" & i
  i = P_cvTrmc4: req.Add CStr(appl.parm(P_cvTrmc4)), "t" & i
  i = P_cvTrmc5: req.Add CStr(appl.parm(P_cvTrmc5)), "t" & i
  i = P_cvTrmc6: req.Add CStr(appl.parm(P_cvTrmc6)), "t" & i
  '--- 2.3 Tassi di rivalutazione di redditi e volumi d'affari ---
  i = P_cvTs1a: req.Add CStr(appl.parm(P_cvTs1a)), "t" & i
  i = P_cvTs1p: req.Add CStr(appl.parm(P_cvTs1p)), "t" & i
  i = P_cvTs1n: req.Add CStr(appl.parm(P_cvTs1n)), "t" & i
  '--- 2.4 Rivalutazione delle pensioni ---
  i = P_cvTp1Max: req.Add CStr(appl.parm(P_cvTp1Max)), "t" & i
  i = P_cvTp1Al: req.Add CStr(appl.parm(P_cvTp1Al)), "t" & i
  i = P_cvTp2Max: req.Add CStr(appl.parm(P_cvTp2Max)), "t" & i
  '--- 20161112LC (inizio)
  i = P_cvTp2Al: req.Add CStr(appl.parm(P_cvTp2Al)), "t" & i
  i = P_cvTp3Max: req.Add CStr(appl.parm(P_cvTp3Max)), "t" & i
  i = P_cvTp3Al: req.Add CStr(appl.parm(P_cvTp3Al)), "t" & i
  i = P_cvTp4Max: req.Add CStr(appl.parm(P_cvTp4Max)), "t" & i
  i = P_cvTp4Al: req.Add CStr(appl.parm(P_cvTp4Al)), "t" & i
  i = P_cvTp5Max: req.Add CStr(appl.parm(P_cvTp5Max)), "t" & i
  i = P_cvTp5Al: req.Add CStr(appl.parm(P_cvTp5Al)), "t" & i
  '--- 20161112LC (fine)
  '--- 2.5 Altre stime ----
  i = P_cvPrcPVattM: req.Add CStr(appl.parm(P_cvPrcPVattM)), "t" & i
  i = P_cvPrcPVattF: req.Add CStr(appl.parm(P_cvPrcPVattF)), "t" & i
  i = P_cvPrcPAattM: req.Add CStr(appl.parm(P_cvPrcPAattM)), "t" & i
  i = P_cvPrcPAattF: req.Add CStr(appl.parm(P_cvPrcPAattF)), "t" & i
  i = P_cvPrcPCattM: req.Add CStr(appl.parm(P_cvPrcPCattM)), "t" & i
  i = P_cvPrcPCattF: req.Add CStr(appl.parm(P_cvPrcPCattF)), "t" & i
  '***********************
  'Frame BASI_TECNICHE (3)
  '***********************
  req.Add IIf(appl.parm(P_cv3) = True, "on", ""), "c" & P_cv3  '20130307LC
  '--- 3.1 Probabilit� di morte ----
  i = P_LAD: req.Add CStr(appl.parm(P_LAD)), "s" & i
  i = P_LED: req.Add CStr(appl.parm(P_LED)), "s" & i
  i = P_LPD: req.Add CStr(appl.parm(P_LPD)), "s" & i
  i = P_LSD: req.Add CStr(appl.parm(P_LSD)), "s" & i
  i = P_LBD: req.Add CStr(appl.parm(P_LBD)), "s" & i
  i = P_LID: req.Add CStr(appl.parm(P_LID)), "s" & i
  '--- 3.2 Altre probabilit� ----
  i = P_LAV: req.Add CStr(appl.parm(P_LAV)), "s" & i
  i = P_LAE: req.Add CStr(appl.parm(P_LAE)), "s" & i
  i = P_LAB: req.Add CStr(appl.parm(P_LAB)), "s" & i
  i = P_LAI: req.Add CStr(appl.parm(P_LAI)), "s" & i
  i = P_LAW: req.Add CStr(appl.parm(P_LAW)), "s" & i
  '--- 3.3 Linee reddituali ----
  i = P_LRA: req.Add CStr(appl.parm(P_LRA)), "s" & i
  i = P_LRP: req.Add CStr(appl.parm(P_LRP)), "s" & i
  i = P_LRN: req.Add CStr(appl.parm(P_LRN)), "s" & i
  '--- 3.4 Coefficienti di conversione del montante in rendita ----
  i = P_CCR: req.Add CStr(appl.parm(P_CCR)), "s" & i
  '--- 3.5 Superstiti ----
  i = P_PF: req.Add CStr(appl.parm(P_PF)), "s" & i
  i = P_ZMAX: req.Add CStr(appl.parm(P_ZMAX)), "t" & i
  '******************************
  'Frame REQUISITI DI ACCESSO (4)
  '******************************
  req.Add IIf(appl.parm(P_cv4) = True, "on", ""), "c" & P_cv4  '20130307LC
  '--- 4.1 Pensione di vecchiaia ordinaria ----
  i = P_cvPveo30M: req.Add CStr(appl.parm(P_cvPveo30M)), "t" & i
  i = P_cvPveo30F: req.Add CStr(appl.parm(P_cvPveo30F)), "t" & i
  i = P_cvPveo65M: req.Add CStr(appl.parm(P_cvPveo65M)), "t" & i
  i = P_cvPveo65F: req.Add CStr(appl.parm(P_cvPveo65F)), "t" & i
  i = P_cvPveo98M: req.Add CStr(appl.parm(P_cvPveo98M)), "t" & i
  i = P_cvPveo98F: req.Add CStr(appl.parm(P_cvPveo98F)), "t" & i
  '--- 4.2 Pensione di vecchiaia anticipata ----
  i = P_cvPvea58M: req.Add CStr(appl.parm(P_cvPvea58M)), "t" & i
  i = P_cvPvea58F: req.Add CStr(appl.parm(P_cvPvea58F)), "t" & i
  i = P_cvPvea35M: req.Add CStr(appl.parm(P_cvPvea35M)), "t" & i
  i = P_cvPvea35F: req.Add CStr(appl.parm(P_cvPvea35F)), "t" & i
  '--- 4.3 Pensione di vecchiaia posticipata ----
  i = P_cvPvep70M: req.Add CStr(appl.parm(P_cvPvep70M)), "t" & i
  i = P_cvPvep70F: req.Add CStr(appl.parm(P_cvPvep70F)), "t" & i
  '--- 4.4 Pensione di inabilit� ----
  i = P_cvPinab2M: req.Add CStr(appl.parm(P_cvPinab2M)), "t" & i
  i = P_cvPinab2F: req.Add CStr(appl.parm(P_cvPinab2F)), "t" & i
  i = P_cvPinab10M: req.Add CStr(appl.parm(P_cvPinab10M)), "t" & i
  i = P_cvPinab10F: req.Add CStr(appl.parm(P_cvPinab10F)), "t" & i
  i = P_cvPinab35M: req.Add CStr(appl.parm(P_cvPinab35M)), "t" & i
  i = P_cvPinab35F: req.Add CStr(appl.parm(P_cvPinab35F)), "t" & i
  '--- 4.5 Pensione di invalidit� ----
  i = P_cvPinv5M: req.Add CStr(appl.parm(P_cvPinv5M)), "t" & i
  i = P_cvPinv5F: req.Add CStr(appl.parm(P_cvPinv5F)), "t" & i
  i = P_cvPinv70: req.Add CStr(appl.parm(P_cvPinv70)), "t" & i
  '--- 4.6 Pensione indiretta ----
  i = P_cvPind2M: req.Add CStr(appl.parm(P_cvPind2M)), "t" & i
  i = P_cvPind2F: req.Add CStr(appl.parm(P_cvPind2F)), "t" & i
  i = P_cvPind20M: req.Add CStr(appl.parm(P_cvPind20M)), "t" & i
  i = P_cvPind20F: req.Add CStr(appl.parm(P_cvPind20F)), "t" & i
  i = P_cvPind30M: req.Add CStr(appl.parm(P_cvPind30M)), "t" & i
  i = P_cvPind30F: req.Add CStr(appl.parm(P_cvPind30F)), "t" & i
  '--- 4.7 Restituzione dei contributi ----
  i = P_cvRcHminM: req.Add CStr(appl.parm(P_cvRcHminM)), "t" & i
  i = P_cvRcHminF: req.Add CStr(appl.parm(P_cvRcHminF)), "t" & i
  i = P_cvRcXminM: req.Add CStr(appl.parm(P_cvRcXminM)), "t" & i
  i = P_cvRcXminF: req.Add CStr(appl.parm(P_cvRcXminF)), "t" & i
  i = P_cvRcPrcCon: req.Add CStr(appl.parm(P_cvRcPrcCon)), "t" & i
  'i = P_cvRcPrcRiv: req.Add CStr(appl.parm(P_cvRcPrcRiv)), "t" & i  '20150512LC (commentato)
  '--- 4.8 Totalizzazione ----
  '***********************
  'Frame CONTRIBUZIONE (5)
  '***********************
  req.Add IIf(appl.parm(P_cv5) = True, "on", ""), "c" & P_cv5  '20130307LC
  '--- 5.1 Contribuzione ridotta ----
  '--- 5.2 Soggettivo ----
  i = P_cvSogg0Min: req.Add CStr(appl.parm(P_cvSogg0Min)), "t" & i
  i = P_cvSogg0MinR: req.Add CStr(appl.parm(P_cvSogg0MinR)), "t" & i
  i = P_cvSogg1Al: req.Add CStr(appl.parm(P_cvSogg1Al)), "t" & i
  i = P_cvSogg1AlR: req.Add CStr(appl.parm(P_cvSogg1AlR)), "t" & i
  i = P_cvSogg1Max: req.Add CStr(appl.parm(P_cvSogg1Max)), "t" & i
  i = P_cvSogg1MaxR: req.Add CStr(appl.parm(P_cvSogg1MaxR)), "t" & i
  '--- 5.3 Integrativo / FIRR ----
  i = P_cvInt0Min: req.Add CStr(appl.parm(P_cvInt0Min)), "t" & i
  i = P_cvInt0MinR: req.Add CStr(appl.parm(P_cvInt0MinR)), "t" & i
  i = P_cvInt1Al: req.Add CStr(appl.parm(P_cvInt1Al)), "t" & i
  i = P_cvInt1AlR: req.Add CStr(appl.parm(P_cvInt1AlR)), "t" & i
  i = P_cvInt1Max: req.Add CStr(appl.parm(i)), "t" & i
  i = P_cvInt1MaxR: req.Add CStr(appl.parm(i)), "t" & i
  i = P_cvInt2Al: req.Add CStr(appl.parm(i)), "t" & i
  i = P_cvInt2AlR: req.Add CStr(appl.parm(i)), "t" & i
  i = P_cvInt2Max: req.Add CStr(appl.parm(i)), "t" & i
  i = P_cvInt2MaxR: req.Add CStr(appl.parm(i)), "t" & i
  i = P_cvInt3Al: req.Add CStr(appl.parm(i)), "t" & i
  i = P_cvInt3AlR: req.Add CStr(appl.parm(i)), "t" & i
  '--- 5.4 Assistenziale ----
  i = P_cvAss0Min: req.Add CStr(appl.parm(P_cvAss0Min)), "t" & i
  i = P_cvAss0MinR: req.Add CStr(appl.parm(P_cvAss0MinR)), "t" & i
  i = P_cvAss1Al: req.Add CStr(appl.parm(P_cvAss1Al)), "t" & i
  i = P_cvass1AlR: req.Add CStr(appl.parm(P_cvass1AlR)), "t" & i
  '--- 5.5 Altro ----
  i = P_cvMat0Min: req.Add CStr(appl.parm(P_cvMat0Min)), "t" & i
  i = P_cvMat0MinR: req.Add CStr(appl.parm(P_cvMat0MinR)), "t" & i
  '****************************
  'Frame MISURA PRESTAZIONI (6)
  '****************************
  req.Add IIf(appl.parm(P_cv6) = True, "on", ""), "c" & P_cv6  '20130307LC
  '--- 6.1 Scaglioni IRPEF ----
  '--- 6.2 Pensione ----
  '--- 6.3 Supplementi di pensione ----
  i = P_cvSpAnni: req.Add CStr(appl.parm(P_cvSpAnni)), "t" & i
  i = P_cvSpEtaMax: req.Add CStr(appl.parm(P_cvSpEtaMax)), "t" & i
  i = P_cvSpPrcCon: req.Add CStr(appl.parm(P_cvSpPrcCon)), "t" & i
  'i = P_cvSpPrcRiv: req.Add CStr(appl.parm(P_cvSpPrcRiv)), "t" & i  '20150512LC (commentato)
  '--- 6.4 Reversibilit� superstiti ----
  i = P_cvAv: req.Add CStr(appl.parm(P_cvAv)), "t" & i
  i = P_cvAvo: req.Add CStr(appl.parm(P_cvAvo)), "t" & i
  i = P_cvAvoo: req.Add CStr(appl.parm(P_cvAvoo)), "t" & i
  i = P_cvAo: req.Add CStr(appl.parm(P_cvAo)), "t" & i
  i = P_cvAoo: req.Add CStr(appl.parm(P_cvAoo)), "t" & i
  i = P_cvAooo: req.Add CStr(appl.parm(P_cvAooo)), "t" & i
  '************************
  'Frame NUOVI ISCRITTI (7)
  '************************
  req.Add IIf(appl.parm(P_cv7) = True, "on", ""), "c" & P_cv7  '20130307LC
  i = P_niTipo: req.Add CStr(appl.parm(i)), "r" & i
  i = P_niP: req.Add CStr(appl.parm(i)), "s" & i
  i = P_niP: req.Add IIf(UCase(Left(appl.parm(i), 1)) = "A", "on", ""), "c" & P_niP
  '--- 20150527LC (inizio)
  i = P_niStat: req.Add CStr(appl.parm(i)), "s" & i
  i = P_neStat: req.Add CStr(appl.parm(i)), "s" & i
  i = P_nvStat: req.Add CStr(appl.parm(i)), "s" & i
  '--- 20150527LC (inizio)
End Sub


Public Sub TAppl_Req2Mask(ByRef appl As TAppl, req As Object, frm As frmElab)
'20121010LC (tutta)
'20130307LC
  Dim bEnabledNI As Boolean
  Dim i As Byte, j As Byte, j1 As Byte, j2 As Byte
  Dim k%
  
  '*******************
  'Frame PARAMETRI (1)
  '*******************
  frm.Check1(P_cv1).Value = IIf(appl.parm(P_cv1) = True, vbChecked, vbUnchecked)
  '--- Bilancio Tecnico ----
  frm.Text1(P_btAnno) = appl.DataBilancio
  '20080416LC (inizio)
  frm.Text1(P_btAnniEl1) = appl.parm(P_btAnniEl1)
  frm.Text1(P_btAnniEl2) = appl.parm(P_btAnniEl2)
  '20080416LC (fine)
  frm.Text1(P_btPatrim) = ff0(appl.parm(P_btPatrim))
  frm.Text1(P_btSpAmm) = ff0(appl.parm(P_btSpAmm))
  frm.Text1(P_btSpAss) = ff0(appl.parm(P_btSpAss))
  frm.Text1(P_btPassSIV) = ff0(appl.parm(P_btPassSIV))
  frm.Text1(P_btPassSIP) = ff0(appl.parm(P_btPassSIP))
  '--- 1.2 Tipo di calcolo ----
  Select Case appl.parm(P_TipoElab)
  Case 0, 1
    frm.optTipoElab(appl.parm(P_TipoElab)).Value = True
    frm.Text1(P_TipoElab) = ""
    frm.Text1(P_TipoElab).Enabled = False
    frm.Text1(P_TipoElab).BackColor = &H8000000F
  Case Else
    frm.optTipoElab(2).Value = True
    frm.Text1(P_TipoElab) = appl.parm(P_TipoElab)
    frm.Text1(P_TipoElab).Enabled = True
    frm.Text1(P_TipoElab).BackColor = &H80000005
  End Select
  '--- 1.3 Modalit� di analisi ---
  frm.Text1(P_SQL) = Sql_Normalize(LCase(appl.parm(P_SQL))) '20120213LC
  frm.Text1(P_FOUT) = LCase(appl.parm(P_FOUT))
  '******************************
  'Frame TASSI ED ALTRE STIME (2)
  '******************************
  frm.Check1(P_cv2).Value = IIf(appl.parm(P_cv2) = True, vbChecked, vbUnchecked)
  '--- 2.1 Tassi di valutazione e rivalutazione ---
  frm.Text1(P_cvTv) = appl.parm(P_cvTv)
  frm.Text1(P_cvTi) = appl.parm(P_cvTi)
  frm.Text1(P_cvTev) = appl.parm(P_cvTev)
  frm.Text1(P_cvTrn) = appl.parm(P_cvTrn)
  '--- 2.2 Tassi di rivalutazione dei montanti contributivi ---
  '20121008LC (inizio)
  frm.Text1(P_cvTrmc0) = appl.parm(P_cvTrmc0)
  '20121008LC (fine)
  frm.Text1(P_cvTrmc1) = appl.parm(P_cvTrmc1)
  frm.Text1(P_cvTrmc2) = appl.parm(P_cvTrmc2)
  frm.Text1(P_cvTrmc3) = appl.parm(P_cvTrmc3)
  frm.Text1(P_cvTrmc4) = appl.parm(P_cvTrmc4)
  frm.Text1(P_cvTrmc5) = appl.parm(P_cvTrmc5)
  frm.Text1(P_cvTrmc6) = appl.parm(P_cvTrmc6)
  '--- 2.3 Tassi di rivalutazione di redditi e volumi d'affari ---
  frm.Text1(P_cvTs1a) = appl.parm(P_cvTs1a)
  frm.Text1(P_cvTs1p) = appl.parm(P_cvTs1p)
  frm.Text1(P_cvTs1n) = appl.parm(P_cvTs1n)
  '--- 2.4 Rivalutazione delle pensioni ---
  frm.Text1(P_cvTp1Max) = appl.parm(P_cvTp1Max)
  frm.Text1(P_cvTp1Al) = appl.parm(P_cvTp1Al)
  frm.Text1(P_cvTp2Max) = appl.parm(P_cvTp2Max)
  frm.Text1(P_cvTp2Al) = appl.parm(P_cvTp2Al)
  frm.Text1(P_cvTp3Max) = appl.parm(P_cvTp3Max)
  frm.Text1(P_cvTp3Al) = appl.parm(P_cvTp3Al)
  '--- 20161112LC (inizio)
  frm.Text1(P_cvTp4Max) = appl.parm(P_cvTp4Max)
  frm.Text1(P_cvTp4Al) = appl.parm(P_cvTp4Al)
  frm.Text1(P_cvTp5Max) = appl.parm(P_cvTp5Max)
  frm.Text1(P_cvTp5Al) = appl.parm(P_cvTp5Al)
  '--- 20161112LC (fine)
  '--- 2.5 Altre stime ----
  frm.Text1(P_cvPrcPVattM) = appl.parm(P_cvPrcPVattM)
  frm.Text1(P_cvPrcPVattF) = appl.parm(P_cvPrcPVattF)
  frm.Text1(P_cvPrcPAattM) = appl.parm(P_cvPrcPAattM)
  frm.Text1(P_cvPrcPAattF) = appl.parm(P_cvPrcPAattF)
  frm.Text1(P_cvPrcPCattM) = appl.parm(P_cvPrcPCattM)
  frm.Text1(P_cvPrcPCattF) = appl.parm(P_cvPrcPCattF)
  '***********************
  'Frame BASI_TECNICHE (3)
  '***********************
  frm.Check1(P_cv3).Value = IIf(appl.parm(P_cv3) = True, vbChecked, vbUnchecked)
  '--- 3.1 Probabilit� di morte ----
  ComboSelect frm.Combo1(P_LAD), appl.parm(P_LAD), 1
  ComboSelect frm.Combo1(P_LED), appl.parm(P_LED), 1
  ComboSelect frm.Combo1(P_LPD), appl.parm(P_LPD), 1
  ComboSelect frm.Combo1(P_LSD), appl.parm(P_LSD), 1
  ComboSelect frm.Combo1(P_LBD), appl.parm(P_LBD), 1
  ComboSelect frm.Combo1(P_LID), appl.parm(P_LID), 1
  '--- 3.2 Altre probabilit� ----
  ComboSelect frm.cbxQav, appl.parm(P_LAV), 1
  ComboSelect frm.cbxQae, appl.parm(P_LAE), 1
  ComboSelect frm.Combo1(P_LAB), appl.parm(P_LAB), 1
  ComboSelect frm.Combo1(P_LAI), appl.parm(P_LAI), 1
  ComboSelect frm.Combo1(P_LAW), appl.parm(P_LAW), 1
  '--- 3.3 Linee reddituali ----
  ComboSelect frm.Combo1(P_LRA), appl.parm(P_LRA), 1
  ComboSelect frm.Combo1(P_LRP), appl.parm(P_LRP), 1
  ComboSelect frm.Combo1(P_LRN), appl.parm(P_LRN), 1
  '--- 3.4 Coefficienti di conversione del montante in rendita ----
  ComboSelect frm.Combo1(P_CCR), appl.parm(P_CCR), 1
  '--- 3.5 Superstiti ----
  ComboSelect frm.Combo1(P_PF), appl.parm(P_PF), 1
  frm.Text1(P_ZMAX) = appl.parm(P_ZMAX)
  '******************************
  'Frame REQUISITI DI ACCESSO (4)
  '******************************
  frm.Check1(P_cv4).Value = IIf(appl.parm(P_cv4) = True, vbChecked, vbUnchecked)
  '--- 4.1 Pensione di vecchiaia ordinaria ----
  frm.Text1(P_cvPveo30M) = appl.parm(P_cvPveo30M)
  frm.Text1(P_cvPveo30F) = appl.parm(P_cvPveo30F)
  frm.Text1(P_cvPveo65M) = appl.parm(P_cvPveo65M)
  frm.Text1(P_cvPveo65F) = appl.parm(P_cvPveo65F)
  frm.Text1(P_cvPveo98M) = appl.parm(P_cvPveo98M)
  frm.Text1(P_cvPveo98F) = appl.parm(P_cvPveo98F)
  '--- 4.2 Pensione di vecchiaia anticipata ----
  frm.Text1(P_cvPvea58M) = appl.parm(P_cvPvea58M)
  frm.Text1(P_cvPvea58F) = appl.parm(P_cvPvea58F)
  frm.Text1(P_cvPvea35M) = appl.parm(P_cvPvea35M)
  frm.Text1(P_cvPvea35F) = appl.parm(P_cvPvea35F)
  '--- 4.3 Pensione di vecchiaia posticipata ----
  frm.Text1(P_cvPvep70M) = appl.parm(P_cvPvep70M)
  frm.Text1(P_cvPvep70F) = appl.parm(P_cvPvep70F)
  '--- 4.4 Pensione di inabilit� ----
  frm.Text1(P_cvPinab2M) = appl.parm(P_cvPinab2M)
  frm.Text1(P_cvPinab2F) = appl.parm(P_cvPinab2F)
  frm.Text1(P_cvPinab10M) = appl.parm(P_cvPinab10M)
  frm.Text1(P_cvPinab10F) = appl.parm(P_cvPinab10F)
  frm.Text1(P_cvPinab35M) = appl.parm(P_cvPinab35M)
  frm.Text1(P_cvPinab35F) = appl.parm(P_cvPinab35F)
  '--- 4.5 Pensione di invalidit� ----
  frm.Text1(P_cvPinv5M) = appl.parm(P_cvPinv5M)
  frm.Text1(P_cvPinv5F) = appl.parm(P_cvPinv5F)
  frm.Text1(P_cvPinv70) = appl.parm(P_cvPinv70)
  '--- 4.6 Pensione indiretta ----
  frm.Text1(P_cvPind2M) = appl.parm(P_cvPind2M)
  frm.Text1(P_cvPind2F) = appl.parm(P_cvPind2F)
  frm.Text1(P_cvPind20M) = appl.parm(P_cvPind20M)
  frm.Text1(P_cvPind20F) = appl.parm(P_cvPind20F)
  frm.Text1(P_cvPind30M) = appl.parm(P_cvPind30M)
  frm.Text1(P_cvPind30F) = appl.parm(P_cvPind30F)
  '--- 4.7 Restituzione dei contributi ----
  frm.Text1(P_cvRcHminM) = appl.parm(P_cvRcHminM)
  frm.Text1(P_cvRcHminF) = appl.parm(P_cvRcHminF)
  frm.Text1(P_cvRcXminM) = appl.parm(P_cvRcXminM)
  frm.Text1(P_cvRcXminF) = appl.parm(P_cvRcXminF)
  frm.Text1(P_cvRcPrcCon) = appl.parm(P_cvRcPrcCon)
  'frm.Text1(P_cvRcPrcRiv) = appl.parm(P_cvRcPrcRiv)  '20150512LC (commentato)
  '--- 4.8 Totalizzazione ----
  '***********************
  'Frame CONTRIBUZIONE (5)
  '***********************
  frm.Check1(P_cv5).Value = IIf(appl.parm(P_cv5) = True, vbChecked, vbUnchecked)
  '--- 5.1 Contribuzione ridotta ----
  '--- 5.2 Soggettivo ----
  frm.Text1(P_cvSogg0Min) = appl.parm(P_cvSogg0Min)
  frm.Text1(P_cvSogg0MinR) = appl.parm(P_cvSogg0MinR)
  frm.Text1(P_cvSogg1Al) = appl.parm(P_cvSogg1Al)
  frm.Text1(P_cvSogg1AlR) = appl.parm(P_cvSogg1AlR)
  frm.Text1(P_cvSogg1Max) = appl.parm(P_cvSogg1Max)
  frm.Text1(P_cvSogg1MaxR) = appl.parm(P_cvSogg1MaxR)
  '--- 5.3 Integrativo / FIRR ----
  frm.Text1(P_cvInt0Min) = appl.parm(P_cvInt0Min)
  frm.Text1(P_cvInt0MinR) = appl.parm(P_cvInt0MinR)
  frm.Text1(P_cvInt1Al) = appl.parm(P_cvInt1Al)
  frm.Text1(P_cvInt1AlR) = appl.parm(P_cvInt1AlR)
  frm.Text1(P_cvInt1Max) = appl.parm(P_cvInt1Max)
  frm.Text1(P_cvInt1MaxR) = appl.parm(P_cvInt1MaxR)
  frm.Text1(P_cvInt2Al) = appl.parm(P_cvInt2Al)
  frm.Text1(P_cvInt2AlR) = appl.parm(P_cvInt2AlR)
  frm.Text1(P_cvInt2Max) = appl.parm(P_cvInt2Max)
  frm.Text1(P_cvInt2MaxR) = appl.parm(P_cvInt2MaxR)
  frm.Text1(P_cvInt3Al) = appl.parm(P_cvInt3Al)
  frm.Text1(P_cvInt3AlR) = appl.parm(P_cvInt3AlR)
  '--- 5.4 Assistenziale ----
  frm.Text1(P_cvAss0Min) = appl.parm(P_cvAss0Min)
  frm.Text1(P_cvAss0MinR) = appl.parm(P_cvAss0MinR)
  frm.Text1(P_cvAss1Al) = appl.parm(P_cvAss1Al)
  frm.Text1(P_cvass1AlR) = appl.parm(P_cvass1AlR)
  '--- 5.5 Altro ----
  frm.Text1(P_cvMat0Min) = appl.parm(P_cvMat0Min)
  frm.Text1(P_cvMat0MinR) = appl.parm(P_cvMat0MinR)
  '****************************
  'Frame MISURA PRESTAZIONI (6)
  '****************************
  frm.Check1(P_cv6).Value = IIf(appl.parm(P_cv6), vbChecked, vbUnchecked)
  '--- 6.1 Scaglioni IRPEF ----
  '--- 6.2 Pensione ----
  '--- 6.3 Supplementi di pensione ----
  frm.Text1(P_cvSpAnni) = appl.parm(P_cvSpAnni)
  frm.Text1(P_cvSpEtaMax) = appl.parm(P_cvSpEtaMax)
  frm.Text1(P_cvSpPrcCon) = appl.parm(P_cvSpPrcCon)
  'frm.Text1(P_cvSpPrcRiv) = appl.parm(P_cvSpPrcRiv)  '20150512LC (commentato)
  '--- 6.4 Reversibilit� superstiti ----
  frm.Text1(P_cvAv) = appl.parm(P_cvAv)
  frm.Text1(P_cvAvo) = appl.parm(P_cvAvo)
  frm.Text1(P_cvAvoo) = appl.parm(P_cvAvoo)
  frm.Text1(P_cvAo) = appl.parm(P_cvAo)
  frm.Text1(P_cvAoo) = appl.parm(P_cvAoo)
  frm.Text1(P_cvAooo) = appl.parm(P_cvAooo)
  '************************
  'Frame NUOVI ISCRITTI (7)
  '************************
  frm.ckxcv7.Value = IIf(appl.parm(P_cv7), vbChecked, vbUnchecked)
  '20120329LC (inizio)
  i = appl.parm(P_niTipo)
  If i = 3 Then
    frm.optNI_3.Value = True
  ElseIf i = 2 Then
    frm.optNI_2.Value = True
  ElseIf i = 1 Then
    frm.optNI_1.Value = True
  Else
    frm.optNI_0.Value = True
  End If
  If ON_ERR_RN1 Then On Error Resume Next
  If appl.parm(P_niP) = "" Then
    frm.cbxNip.ListIndex = 0
  Else
    frm.cbxNip.Text = appl.parm(P_niP)
    If frm.cbxNip.ListIndex < 0 Then
      frm.cbxNip.ListIndex = 0
    End If
  End If
  If appl.parm(P_niStat) = "" Then
    frm.cbxNiStat.ListIndex = 0
  Else
    frm.cbxNiStat = appl.parm(P_niStat)
    If frm.cbxNiStat.ListIndex < 0 Then
      frm.cbxNiStat.ListIndex = 0
    End If
  End If
  '--- 20150527LC (inizio)
  If appl.parm(P_neStat) = "" Then
    frm.cbxNeStat.ListIndex = 0
  Else
    frm.cbxNeStat = appl.parm(P_neStat)
    If frm.cbxNeStat.ListIndex < 0 Then
      frm.cbxNeStat.ListIndex = 0
    End If
  End If
  '---
  If appl.parm(P_nvStat) = "" Then
    frm.cbxNvStat.ListIndex = 0
  Else
    frm.cbxNvStat = appl.parm(P_nvStat)
    If frm.cbxNvStat.ListIndex < 0 Then
      frm.cbxNvStat.ListIndex = 0
    End If
  End If
  '--- 20150527LC (fine)
  On Error GoTo 0
  '20120329LC (fine)
End Sub


Public Sub TAppl_Init(ByRef appl As TAppl, ByRef glo As TGlobale)
'***************************
'Inizializza l'oggetto TAppl
'20150522LC  glo
'***************************
  Dim fi&, fo&, g&, m&
  Dim sz As String
Dim f&
Dim sa As String
Dim sb As String
Dim sFileIni As String
Dim sFileOpzIni As String
Dim iErr&
Dim v As Variant

  iErr = 0&
  With appl
    .XMIN = 15  '20160323LC (ex 18)
    .XMAX = OPZ_OMEGA
    .xlim = .XMAX
    .nx5 = (.xlim - .XMIN + 4) \ 5
    .hmin5 = 5
    .hmin15 = 15
    .hmax = 60
    '20080416LC (inizio)
    .nMax1 = OPZ_OMEGA - 20
    .nMax2 = .nMax1 + OPZ_ELAB_CODA
    '20080416LC (fine)
    .hlim = .hmax
    .nh5 = (.hlim - 0 + 4) \ 5
    .DataBilancio = "31/12/2000"
    .t0 = Year(.DataBilancio)
    .DataInizio = .DataBilancio + 1
    'DatiPut appl
    '--- 20150522LC (inizio) ENEA
    If 1 = 2 Then
      sFileIni = "ENEA.INI"
      f = FreeFile
      Open sFileIni For Input As #f
      Input #f, .iDB
      '--- 20140128LC (inizio)
      If .iDB < 0 Then
        .jDB = 2
        .iDB = Abs(.iDB)
      Else
        .jDB = 0
      End If
      '--- 20140128LC (fine)
      Input #f, .iDbType
      Input #f, .sDbConn
      Input #f, .sFileOpzIni
      Close #f
    End If
.iDB = 8
.iDbType = 1
.sDbConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=G:\X\EN\MDB\ENEA3_c7.mdb;Persist Security Info=False"
.sFileOpzIni = "ENEA_OPZ8.INI"
    '---
    If Dir(.sFileOpzIni) <> "" Then
      f = FreeFile
      Open .sFileOpzIni For Input As #f
      Do Until EOF(f)
        Line Input #f, sa
        sa = Trim(sa)
        If sa = "" Then
        ElseIf Left(sa, 1) = "'" Then
        Else
          v = Split(sa, "=")
          If UBound(v) <> 1 Then
            Call MsgBox("TODO")
            Stop
          End If
          sa = UCase(Trim(v(0)))
          sb = Trim(v(1))
          If Not IsNumeric(sb) Then
            Call MsgBox("TODO")
            Stop
          End If
          '--- 20160306LC (inizio)  'commentato  (ex ENEA)
          'If sa = "OPZ_ENEA_MAT" Then
          '  OPZ_ENEA_MAT = Val(sb)
          'ElseIf sa = "OPZ_ENEA_QPD1" Then
          '  OPZ_ENEA_QPD1 = CDbl(sb)
          'ElseIf sa = "OPZ_ENEA_QAD1" Then
          '  OPZ_ENEA_QAD1 = CDbl(sb)
          'ElseIf sa = "OPZ_ENEA_QED1" Then
          '  OPZ_ENEA_QED1 = CDbl(sb)
          'ElseIf sa = "OPZ_ENEA_QAE2" Then
          '  OPZ_ENEA_QAE2 = CDbl(sb)
          'ElseIf sa = "OPZ_ENEA_QAB2" Then
          '  OPZ_ENEA_QAB2 = CDbl(sb)
          'ElseIf sa = "OPZ_ENEA_QAI2" Then
          '  OPZ_ENEA_QAI2 = CDbl(sb)
          'End If
          '--- 20160306LC (fine)  'commentato  (ex ENEA)
        End If
      Loop
      Close #f
      .bProb_Superst = 1  '20140201LC
    End If
  End With
  
  '--- 20150522LC (fine)
End Sub


'20120210LC
Public Sub TAppl_CV4_Get(CoeffVar() As Double, cv4() As TAppl_CV4, anno%)
  With cv4(anno, 0)
    '--- 4.1 Pensione di vecchiaia ordinaria ----
    .Pveo30_orig = CoeffVar(anno, ecv.cv_Pveo30M)  '20121008LC
    .Pveo65 = CoeffVar(anno, ecv.cv_Pveo65M)
    .Pveo98 = CoeffVar(anno, ecv.cv_Pveo98M)
    '--- 4.2 Pensione di vecchiaia anticipata ----
    .Pvea35 = CoeffVar(anno, ecv.cv_Pvea35M)
    .Pvea58 = CoeffVar(anno, ecv.cv_Pvea58M)
    '--- 4.3 Pensione di vecchiaia posticipata ----
    .Pvep70 = CoeffVar(anno, ecv.cv_Pvep70M)
    '--- 4.4 Pensione di inabilit� ----
    .Pinab2 = CoeffVar(anno, ecv.cv_Pinab2M)
    .Pinab10 = CoeffVar(anno, ecv.cv_Pinab10M)
    .Pinab35 = CoeffVar(anno, ecv.cv_Pinab35M)
    '--- 4.5 Pensione di invalidit� ----
    .Pinv5 = CoeffVar(anno, ecv.cv_Pinv5M)
    .Pinv70 = CoeffVar(anno, ecv.cv_Pinv70)
    '--- 4.6 Pensione indiretta ----
    .Pind2 = CoeffVar(anno, ecv.cv_Pind2M)
    .Pind20 = CoeffVar(anno, ecv.cv_Pind20M)
    .Pind30 = CoeffVar(anno, ecv.cv_Pind30M)
    '--- 4.7 Restituzione dei contributi ----
    .RcHmin = CoeffVar(anno, ecv.cv_RcHminM)
    .RcXmin = CoeffVar(anno, ecv.cv_RcXminM)
    .RcPrcCon = CoeffVar(anno, ecv.cv_RcPrcCon)
    '.RcPrcRiv = CoeffVar(anno, ecv.cv_RcPrcRiv)    '20150512LC (commentato)
    '--- 4.8 Totalizzazione ----
    .TzHmin = 99
    .TzXmin = 99
    .TzRivMin = 0#
  End With
  '---
  With cv4(anno, 1)
    '--- 4.1 Pensione di vecchiaia ordinaria ----
    .Pveo30_orig = CoeffVar(anno, ecv.cv_Pveo30F)  '20121008LC
    .Pveo65 = CoeffVar(anno, ecv.cv_Pveo65F)
    .Pveo98 = CoeffVar(anno, ecv.cv_Pveo98F)
    '--- 4.2 Pensione di vecchiaia anticipata ----
    .Pvea35 = CoeffVar(anno, ecv.cv_Pvea35F)
    .Pvea58 = CoeffVar(anno, ecv.cv_Pvea58F)
    '--- 4.3 Pensione di vecchiaia posticipata ----
    .Pvep70 = CoeffVar(anno, ecv.cv_Pvep70F)
    '--- 4.4 Pensione di inabilit� ----
    .Pinab2 = CoeffVar(anno, ecv.cv_Pinab2F)
    .Pinab10 = CoeffVar(anno, ecv.cv_Pinab10F)
    .Pinab35 = CoeffVar(anno, ecv.cv_Pinab35F)
    '--- 4.5 Pensione di invalidit� ----
    .Pinv5 = CoeffVar(anno, ecv.cv_Pinv5F)
    .Pinv70 = CoeffVar(anno, ecv.cv_Pinv70)
    '--- 4.6 Pensione indiretta ----
    .Pind2 = CoeffVar(anno, ecv.cv_Pind2F)
    .Pind20 = CoeffVar(anno, ecv.cv_Pind20F)
    .Pind30 = CoeffVar(anno, ecv.cv_Pind30F)
    '--- 4.7 Restituzione dei contributi ----
    .RcHmin = CoeffVar(anno, ecv.cv_RcHminF)
    .RcXmin = CoeffVar(anno, ecv.cv_RcXminF)
    .RcPrcCon = CoeffVar(anno, ecv.cv_RcPrcCon)
    '.RcPrcRiv = CoeffVar(anno, ecv.cv_RcPrcRiv)    '20150512LC (commentato)
    '--- 4.8 Totalizzazione ----
    .TzHmin = 99
    .TzXmin = 99
    .TzRivMin = 0#
  End With
End Sub


Public Function TAppl_ParamDbGet$(ByRef appl As TAppl, ByRef glo As TGlobale, db As ADODB.Connection, szGruppo$, IO$)
  Dim t0%
  Dim i&, iTC&, j&
  Dim SQL$, szErr$
  Dim rs As New ADODB.Recordset
  
  If IO = "I" Then
    SQL = "SELECT * FROM I_PARAM WHERE P_ID='" & UCase(szGruppo) & "'" '20121024LC
  Else
    SQL = "SELECT * FROM O_PARAM WHERE opProgr=" & appl.idReport
  End If
  rs.Open SQL, db, adOpenForwardOnly, adLockReadOnly
  If rs.EOF Then
    szErr = "Gruppo di parametri non trovato"
  Else
    '20120521LC (inizio) spostato in alto, per aprire analisi esistente
    appl.t0 = rs.Fields("P_btAnno").Value
    szErr = LeggiCoeffVar1(glo.db, appl.t0, appl.CoeffVar(), appl.cv(), appl.cv4())
    If szErr <> "" Then GoTo ExitPoint
    '20120521LC (fine)
    If IO = "I" Then
    Else
      appl.matr1 = rs.Fields("opMatr1").Value
      appl.matr2 = rs.Fields("opMatr2").Value
    End If
    '*******************
    'Frame PARAMETRI (1)
    '*******************
    appl.parm(P_cv1) = Math.Abs(rs.Fields("P_cv1").Value) = 1 '20121024LC
    If appl.parm(P_cv1) = False Then
      '--- Bilancio Tecnico ----
      appl.parm(P_ID) = LCase(rs.Fields("P_ID").Value)
      appl.parm(P_btAnno) = rs.Fields("P_btAnno").Value
        appl.t0 = rs.Fields("P_btAnno").Value
        appl.DataBilancio = "31/12/" & appl.t0
        appl.DataInizio = appl.DataBilancio + 1
      appl.parm(P_btAnniEl1) = rs.Fields("P_btAnniEl1").Value
      appl.parm(P_btAnniEl2) = appl.parm(P_btAnniEl1) + OPZ_ELAB_CODA
      appl.parm(P_btPatrim) = rs.Fields("P_btPatrim").Value
      appl.parm(P_btSpAmm) = rs.Fields("P_btSpAmm").Value
      appl.parm(P_btSpAss) = rs.Fields("P_btSpAss").Value
      appl.parm(P_btPassSIV) = rs.Fields("P_btPassSIV").Value
      appl.parm(P_btPassSIP) = rs.Fields("P_btPassSIP").Value
      '--- 1.2 Tipo di calcolo ----
      appl.parm(P_TipoElab) = ff0(rs.Fields("P_TIPOELAB").Value)
      '--- 1.3 Modalit� di analisi ---
      appl.parm(P_SQL) = Sql_Normalize(LCase("" & rs.Fields("P_SQL").Value)) '20120213LC
      appl.parm(P_FOUT) = LCase("" & rs.Fields("P_FOUT").Value)
      If appl.parm(P_FOUT) = "" Then
        appl.parm(P_FOUT) = "enasarco.out"
      ElseIf InStr(appl.parm(P_FOUT), ".") = 0 Then
        appl.parm(P_FOUT) = appl.parm(P_FOUT) & ".out"
      End If
    Else
      Call MsgBox("TODO TAppl_ParamDbGet 1")
      Stop
    End If
    '******************************
    'Frame TASSI ED ALTRE STIME (2)
    '******************************
    appl.parm(P_cv2) = Math.Abs(rs.Fields("P_cv2").Value) = 1 '20121024LC
    If appl.parm(P_cv2) = False Then
      '--- 2.1 Tassi di valutazione e rivalutazione ---
      appl.parm(P_cvTv) = rs.Fields("P_cvTv").Value
      appl.parm(P_cvTi) = rs.Fields("P_cvTi").Value
      appl.parm(P_cvTev) = rs.Fields("P_cvTev").Value
      appl.parm(P_cvTrn) = rs.Fields("P_cvTrn").Value
      '--- 2.2 Tassi di rivalutazione dei montanti contributivi ---
      appl.parm(P_cvTrmc0) = rs.Fields("P_cvTrmc0").Value
      appl.parm(P_cvTrmc1) = rs.Fields("P_cvTrmc1").Value
      appl.parm(P_cvTrmc2) = rs.Fields("P_cvTrmc2").Value
      appl.parm(P_cvTrmc3) = rs.Fields("P_cvTrmc3").Value
      appl.parm(P_cvTrmc4) = rs.Fields("P_cvTrmc4").Value
      appl.parm(P_cvTrmc5) = rs.Fields("P_cvTrmc5").Value
      appl.parm(P_cvTrmc6) = rs.Fields("P_cvTrmc6").Value
      '--- 2.3 Tassi di rivalutazione di redditi e volumi d'affari ---
      appl.parm(P_cvTs1a) = rs.Fields("P_cvTs1A").Value
      appl.parm(P_cvTs1p) = rs.Fields("P_cvTs1P").Value
      appl.parm(P_cvTs1n) = rs.Fields("P_cvTs1N").Value
      '--- 2.4 Rivalutazione delle pensioni ---
      appl.parm(P_cvTp1Max) = rs.Fields("P_cvTp1Max").Value
      appl.parm(P_cvTp1Al) = rs.Fields("P_cvTp1Al").Value
      appl.parm(P_cvTp2Max) = rs.Fields("P_cvTp2Max").Value
      appl.parm(P_cvTp2Al) = rs.Fields("P_cvTp2Al").Value
      appl.parm(P_cvTp3Max) = rs.Fields("P_cvTp3Max").Value
      appl.parm(P_cvTp3Al) = rs.Fields("P_cvTp3Al").Value
      '--- 20161112LC (inizio)
      appl.parm(P_cvTp4Max) = rs.Fields("P_cvTp4Max").Value
      appl.parm(P_cvTp4Al) = rs.Fields("P_cvTp4Al").Value
      appl.parm(P_cvTp5Max) = rs.Fields("P_cvTp5Max").Value
      appl.parm(P_cvTp5Al) = rs.Fields("P_cvTp5Al").Value
      '--- 20161112LC (fine)
      '--- 2.5 Altre stime ----
      '20121113LC (inizio)
      If OPZ_INT_NO_PENSATT = 0 Then
        appl.parm(P_cvPrcPVattM) = rs.Fields("P_cvPrcPVattM").Value
        appl.parm(P_cvPrcPVattF) = rs.Fields("P_cvPrcPVattF").Value
        appl.parm(P_cvPrcPAattM) = rs.Fields("P_cvPrcPAattM").Value
        appl.parm(P_cvPrcPAattF) = rs.Fields("P_cvPrcPAattF").Value
        appl.parm(P_cvPrcPCattM) = rs.Fields("P_cvPrcPCattM").Value
        appl.parm(P_cvPrcPCattF) = rs.Fields("P_cvPrcPCattF").Value
      Else
        appl.parm(P_cvPrcPVattM) = 0
        appl.parm(P_cvPrcPVattF) = 0
        appl.parm(P_cvPrcPAattM) = 0
        appl.parm(P_cvPrcPAattF) = 0
        appl.parm(P_cvPrcPCattM) = 0
        appl.parm(P_cvPrcPCattF) = 0
      End If
      '20121113LC (fine)
    Else
      '--- 2.1 Tassi di valutazione e rivalutazione ---
      If OPZ_INT_CV21 = 1 Then  '20140605LC  'ex bCv25 = True
        appl.parm(P_cvTv) = appl.CoeffVar(appl.t0, ecv.cv_Tv)
        appl.parm(P_cvTi) = appl.CoeffVar(appl.t0, ecv.cv_Ti)
      Else
        appl.parm(P_cvTv) = rs.Fields("P_cvTv").Value
        appl.parm(P_cvTi) = rs.Fields("P_cvTi").Value
      End If
      appl.parm(P_cvTev) = appl.CoeffVar(appl.t0, ecv.cv_Tev)
      appl.parm(P_cvTrn) = appl.CoeffVar(appl.t0, ecv.cv_Trn)
      '--- 2.2 Tassi di rivalutazione dei montanti contributivi ---
      '--- 20150609LC (inizio)
      appl.parm(P_cvTrmc0) = appl.CoeffVar(appl.t0, ecv.cv_Trmc0)
      appl.parm(P_cvTrmc1) = appl.CoeffVar(appl.t0, ecv.cv_Trmc1)
      appl.parm(P_cvTrmc2) = appl.CoeffVar(appl.t0, ecv.cv_Trmc2)
      appl.parm(P_cvTrmc3) = appl.CoeffVar(appl.t0, ecv.cv_Trmc3)
      appl.parm(P_cvTrmc4) = appl.CoeffVar(appl.t0, ecv.cv_Trmc4)
      appl.parm(P_cvTrmc5) = appl.CoeffVar(appl.t0, ecv.cv_Trmc5)
      appl.parm(P_cvTrmc6) = appl.CoeffVar(appl.t0, ecv.cv_Trmc6)
      '--- 20150609LC (fine)
      '--- 2.3 Tassi di rivalutazione di redditi e volumi d'affari ---
      appl.parm(P_cvTs1a) = appl.CoeffVar(appl.t0, ecv.cv_Ts1a)
      appl.parm(P_cvTs1p) = appl.CoeffVar(appl.t0, ecv.cv_Ts1p)
      appl.parm(P_cvTs1n) = appl.CoeffVar(appl.t0, ecv.cv_Ts1n)
      '20160606LC (commentato)
      'If Math.Abs(OPZ_INT_COMP) = COMP_ENAS Then
      'Else
        appl.parm(P_cvTs2a) = appl.CoeffVar(appl.t0, ecv.cv_Ts2a)
        appl.parm(P_cvTs2p) = appl.CoeffVar(appl.t0, ecv.cv_Ts2p)
        appl.parm(P_cvTs2n) = appl.CoeffVar(appl.t0, ecv.cv_Ts2n)
      'End If
      '--- 2.4 Rivalutazione delle pensioni ---
      appl.parm(P_cvTp1Max) = appl.CoeffVar(appl.t0, ecv.cv_Tp1Max)
      appl.parm(P_cvTp1Al) = appl.CoeffVar(appl.t0, ecv.cv_Tp1Al)
      appl.parm(P_cvTp2Max) = appl.CoeffVar(appl.t0, ecv.cv_Tp2Max)
      appl.parm(P_cvTp2Al) = appl.CoeffVar(appl.t0, ecv.cv_Tp2Al)
      appl.parm(P_cvTp3Max) = appl.CoeffVar(appl.t0, ecv.cv_Tp3Max)
      appl.parm(P_cvTp3Al) = appl.CoeffVar(appl.t0, ecv.cv_Tp3Al)
      '--- 20161112LC (inizio)
      appl.parm(P_cvTp4Max) = appl.CoeffVar(appl.t0, ecv.cv_Tp4Max)
      appl.parm(P_cvTp4Al) = appl.CoeffVar(appl.t0, ecv.cv_Tp4Al)
      appl.parm(P_cvTp5Max) = appl.CoeffVar(appl.t0, ecv.cv_Tp5Max)
      appl.parm(P_cvTp5Al) = appl.CoeffVar(appl.t0, ecv.cv_Tp5Al)
      '--- 20161112LC (fine)
      '--- 2.5 Altre stime ----
      If OPZ_INT_CV25 = 1 Then  '20140605LC  'ex bCv25 = True
        appl.parm(P_cvPrcPVattM) = appl.CoeffVar(appl.t0, ecv.cv_PrcPVattM)
        appl.parm(P_cvPrcPVattF) = appl.CoeffVar(appl.t0, ecv.cv_PrcPVattF)
        appl.parm(P_cvPrcPAattM) = appl.CoeffVar(appl.t0, ecv.cv_PrcPAattM)
        appl.parm(P_cvPrcPAattF) = appl.CoeffVar(appl.t0, ecv.cv_PrcPAattF)
        appl.parm(P_cvPrcPCattM) = appl.CoeffVar(appl.t0, ecv.cv_PrcPCattM)
        appl.parm(P_cvPrcPCattF) = appl.CoeffVar(appl.t0, ecv.cv_PrcPCattF)
      Else
        appl.parm(P_cvPrcPVattM) = rs.Fields("P_cvPrcPVattM").Value
        appl.parm(P_cvPrcPVattF) = rs.Fields("P_cvPrcPVattF").Value
        appl.parm(P_cvPrcPAattM) = rs.Fields("P_cvPrcPAattM").Value
        appl.parm(P_cvPrcPAattF) = rs.Fields("P_cvPrcPAattF").Value
        appl.parm(P_cvPrcPCattM) = rs.Fields("P_cvPrcPCattM").Value
        appl.parm(P_cvPrcPCattF) = rs.Fields("P_cvPrcPCattF").Value
      End If
    End If
    '***********************
    'Frame BASI_TECNICHE (3)
    '***********************
    appl.parm(P_cv3) = Math.Abs(rs.Fields("P_cv3").Value) = 1 '20121024LC
    If appl.parm(P_cv3) = False Then
      '--- 3.1 Probabilit� di morte ----
      appl.parm(P_LAD) = LCase(Trim("" & rs.Fields("P_LAD").Value))
      appl.parm(P_LED) = LCase(Trim("" & rs.Fields("P_LED").Value))
      appl.parm(P_LPD) = LCase(Trim("" & rs.Fields("P_LPD").Value))
      appl.parm(P_LSD) = LCase(Trim("" & rs.Fields("P_LSD").Value))
      If appl.parm(P_LSD) = "" Then
        appl.parm(P_LSD) = appl.parm(P_LPD)
      End If
      '--- 20121113LC (inizio)
      If OPZ_INT_NO_AE_AB_AI = 0 Then
        appl.parm(P_LBD) = LCase(Trim("" & rs.Fields("P_LBD").Value))
        appl.parm(P_LID) = LCase(Trim("" & rs.Fields("P_LID").Value))
      Else
        appl.parm(P_LED) = "zzz"
        appl.parm(P_LBD) = "zzz"
        appl.parm(P_LID) = "zzz"
      End If
      '--- 20121113LC (fine)
      '--- 3.2 Altre probabilit� ----
      '--- 20121113LC (inizio)
      If OPZ_INT_NO_AE_AB_AI = 0 Then
        appl.parm(P_LAV) = LCase(Trim("" & rs.Fields("P_LAV").Value))
        If appl.parm(P_LAV) = "" Then
          appl.parm(P_LAV) = "zzz"
        End If
        appl.parm(P_LAE) = LCase(Trim("" & rs.Fields("P_LAE").Value))
        appl.parm(P_LAB) = LCase(Trim("" & rs.Fields("P_LAB").Value))
        appl.parm(P_LAI) = LCase(Trim("" & rs.Fields("P_LAI").Value))
      Else
        appl.parm(P_LAV) = "zzz"
        appl.parm(P_LAE) = "zzz"
        appl.parm(P_LAB) = "zzz"
        appl.parm(P_LAI) = "zzz"
      End If
      If OPZ_INT_NO_AW = 0 Then
        appl.parm(P_LAW) = LCase(Trim("" & rs.Fields("P_LAW").Value))
      Else
        appl.parm(P_LAW) = "zzz"
      End If
      '--- 20121113LC (fine)
      '--- 3.3 Linee reddituali ----
      appl.parm(P_LRA) = LCase(Trim("" & rs.Fields("P_LRA").Value))
      appl.parm(P_LRP) = LCase(Trim("" & rs.Fields("P_LRP").Value))
      appl.parm(P_LRN) = LCase(Trim("" & rs.Fields("P_LRN").Value))
      '--- 3.4 Coefficienti di conversione del montante in rendita ----
      appl.parm(P_CCR) = LCase(Trim("" & rs.Fields("P_CCR").Value))
      '--- 3.5 Superstiti ----
      '--- 20121113LC (inizio)
      If OPZ_INT_NO_SUPERST = 0 Then
        appl.parm(P_PF) = LCase(Trim("" & rs.Fields("P_PF").Value))
        appl.parm(P_ZMAX) = rs.Fields("P_ZMAX").Value
      Else
        appl.parm(P_PF) = "z0000"
        appl.parm(P_ZMAX) = 0
      End If
      '--- 20121113LC (fine)
      '********
      'Segue...
      '********
      If 1 = 2 Then '20121108LC
        '--- 3.1 Probabilit� di morte ----
        ReDim appl.LL1(0 To OPZ_OMEGAP1, 0 To BT_MAX, 0 To 3)  '20130211LC
        Call LeggiBD(glo.db, CStr(appl.parm(P_LAD)), BT_QAD, appl.LL1, True)
        Call LeggiBD(glo.db, CStr(appl.parm(P_LED)), BT_QED, appl.LL1, True)
        Call LeggiBD(glo.db, CStr(appl.parm(P_LPD)), BT_QPD, appl.LL1, True)
        Call LeggiBD(glo.db, CStr(appl.parm(P_LSD)), BT_QSD, appl.LL1, True)
        Call LeggiBD(glo.db, CStr(appl.parm(P_LBD)), BT_QBD, appl.LL1, True)
        Call LeggiBD(glo.db, CStr(appl.parm(P_LID)), BT_QID, appl.LL1, True)
        '--- 3.2 Altre probabilit� ----
        Call LeggiBD(glo.db, CStr(appl.parm(P_LAV)), BT_QAV, appl.LL1, False)
        Call LeggiBD(glo.db, CStr(appl.parm(P_LAE)), BT_QAE, appl.LL1, False)
        Call LeggiBD(glo.db, CStr(appl.parm(P_LAB)), BT_QAB, appl.LL1, False)
        Call LeggiBD(glo.db, CStr(appl.parm(P_LAI)), BT_QAI, appl.LL1, False)
        Call LeggiBD(glo.db, CStr(appl.parm(P_LAW)), BT_QAW, appl.LL1, False)
        '--- 3.3 Linee reddituali ----
        ReDim appl.Lsm(K_HMIN To K_HMAX, K_XMIN To K_XMAX, 0 To 2, 0 To 1, 0 To 3) As Double '20080617LC
        Call LeggiLS(glo.db, CStr(appl.parm(P_LRA)), 0, 0, appl.Lsm)
        Call LeggiLS(glo.db, CStr(appl.parm(P_LRA)), 0, 1, appl.Lsm)
        Call LeggiLS(glo.db, CStr(appl.parm(P_LRP)), 1, 0, appl.Lsm)
        Call LeggiLS(glo.db, CStr(appl.parm(P_LRP)), 1, 1, appl.Lsm)
        Call LeggiLS(glo.db, CStr(appl.parm(P_LRN)), 2, 0, appl.Lsm)
        Call LeggiLS(glo.db, CStr(appl.parm(P_LRN)), 2, 1, appl.Lsm)
        '--- 3.4 Coefficienti di conversione del montante in rendita ----
        Call LeggiCoeffPA(appl.t0, glo.db, CStr(appl.parm(P_CCR)), appl.CoeffPA())
        '--- 3.5 Superstiti ----
        Call LeggiPFam(glo.db, appl.LL1, CStr(appl.parm(P_PF)), appl.pf())
      End If
    Else
      Call MsgBox("TODO TAppl_ParamDbGet 2")
      Stop
    End If
    '******************************
    'Frame REQUISITI DI ACCESSO (4)
    '******************************
    appl.parm(P_cv4) = Math.Abs(rs.Fields("P_cv4").Value) = 1 '20121024LC
    If appl.parm(P_cv4) = False Then
      '--- 4.1 Pensione di vecchiaia ordinaria ----
      appl.parm(P_cvPveo30M) = rs.Fields("P_cvPveo30M").Value
      appl.parm(P_cvPveo30F) = rs.Fields("P_cvPveo30F").Value
      appl.parm(P_cvPveo65M) = rs.Fields("P_cvPveo65M").Value
      appl.parm(P_cvPveo65F) = rs.Fields("P_cvPveo65F").Value
      appl.parm(P_cvPveo98M) = rs.Fields("P_cvPveo98M").Value
      appl.parm(P_cvPveo98F) = rs.Fields("P_cvPveo98F").Value
      '--- 4.2 Pensione di vecchiaia anticipata ----
      appl.parm(P_cvPvea35M) = rs.Fields("P_cvPvea35M").Value
      appl.parm(P_cvPvea35F) = rs.Fields("P_cvPvea35F").Value
      appl.parm(P_cvPvea58M) = rs.Fields("P_cvPvea58M").Value
      appl.parm(P_cvPvea58F) = rs.Fields("P_cvPvea58F").Value
      '--- 4.3 Pensione di vecchiaia posticipata ----
      appl.parm(P_cvPvep70M) = rs.Fields("P_cvPvep70M").Value
      appl.parm(P_cvPvep70F) = rs.Fields("P_cvPvep70F").Value
      '--- 4.4 Pensione di inabilit� ----
      appl.parm(P_cvPinab2M) = rs.Fields("P_cvPinab2M").Value
      appl.parm(P_cvPinab2F) = rs.Fields("P_cvPinab2F").Value
      appl.parm(P_cvPinab10M) = rs.Fields("P_cvPinab10M").Value
      appl.parm(P_cvPinab10F) = rs.Fields("P_cvPinab10F").Value
      appl.parm(P_cvPinab35M) = rs.Fields("P_cvPinab35M").Value
      appl.parm(P_cvPinab35F) = rs.Fields("P_cvPinab35F").Value
      '--- 4.5 Pensione di invalidit� ----
      appl.parm(P_cvPinv5M) = rs.Fields("P_cvPinv5M").Value
      appl.parm(P_cvPinv5F) = rs.Fields("P_cvPinv5F").Value
      appl.parm(P_cvPinv70) = rs.Fields("P_cvPinv70").Value
      '--- 4.6 Pensione indiretta ----
      appl.parm(P_cvPind2M) = rs.Fields("P_cvPind2M").Value
      appl.parm(P_cvPind2F) = rs.Fields("P_cvPind2F").Value
      appl.parm(P_cvPind20M) = rs.Fields("P_cvPind20M").Value
      appl.parm(P_cvPind20F) = rs.Fields("P_cvPind20F").Value
      appl.parm(P_cvPind30M) = rs.Fields("P_cvPind30M").Value
      appl.parm(P_cvPind30F) = rs.Fields("P_cvPind30F").Value
      '--- 4.7 Restituzione dei contributi ----
      appl.parm(P_cvRcHminM) = rs.Fields("P_cvRcHminM").Value
      appl.parm(P_cvRcHminF) = rs.Fields("P_cvRcHminF").Value
      appl.parm(P_cvRcXminM) = rs.Fields("P_cvRcXminM").Value
      appl.parm(P_cvRcXminF) = rs.Fields("P_cvRcXminF").Value
      appl.parm(P_cvRcPrcCon) = rs.Fields("P_cvRcPrcCon").Value
      'appl.parm(P_cvRcPrcRiv) = rs.Fields("P_cvRcPrcRiv").Value  '20150512LC (commentato)
      '--- 4.8 Totalizzazione ----
    Else
      '--- 4.1 Pensione di vecchiaia ordinaria ----
      appl.parm(P_cvPveo30M) = appl.CoeffVar(appl.t0, ecv.cv_Pveo30M)
      appl.parm(P_cvPveo30F) = appl.CoeffVar(appl.t0, ecv.cv_Pveo30F)
      appl.parm(P_cvPveo65M) = appl.CoeffVar(appl.t0, ecv.cv_Pveo65M)
      appl.parm(P_cvPveo65F) = appl.CoeffVar(appl.t0, ecv.cv_Pveo65F)
      appl.parm(P_cvPveo98M) = appl.CoeffVar(appl.t0, ecv.cv_Pveo98M)
      appl.parm(P_cvPveo98F) = appl.CoeffVar(appl.t0, ecv.cv_Pveo98F)
      '--- 4.2 Pensione di vecchiaia anticipata ----
      appl.parm(P_cvPvea35M) = appl.CoeffVar(appl.t0, ecv.cv_Pvea35M)
      appl.parm(P_cvPvea35F) = appl.CoeffVar(appl.t0, ecv.cv_Pvea35F)
      appl.parm(P_cvPvea58M) = appl.CoeffVar(appl.t0, ecv.cv_Pvea58M)
      appl.parm(P_cvPvea58F) = appl.CoeffVar(appl.t0, ecv.cv_Pvea58F)
      '--- 4.3 Pensione di vecchiaia posticipata ----
      appl.parm(P_cvPvep70M) = appl.CoeffVar(appl.t0, ecv.cv_Pvep70M)
      appl.parm(P_cvPvep70F) = appl.CoeffVar(appl.t0, ecv.cv_Pvep70F)
      '--- 4.3 Pensione di inabilit� ----
      appl.parm(P_cvPinab2M) = appl.CoeffVar(appl.t0, ecv.cv_Pinab2M)
      appl.parm(P_cvPinab2F) = appl.CoeffVar(appl.t0, ecv.cv_Pinab2F)
      appl.parm(P_cvPinab10M) = appl.CoeffVar(appl.t0, ecv.cv_Pinab10M)
      appl.parm(P_cvPinab10F) = appl.CoeffVar(appl.t0, ecv.cv_Pinab10F)
      appl.parm(P_cvPinab35M) = appl.CoeffVar(appl.t0, ecv.cv_Pinab35M)
      appl.parm(P_cvPinab35F) = appl.CoeffVar(appl.t0, ecv.cv_Pinab35F)
      '--- 4.4 Pensione di invalidit� ----
      appl.parm(P_cvPinv5M) = appl.CoeffVar(appl.t0, ecv.cv_Pinv5M)
      appl.parm(P_cvPinv5F) = appl.CoeffVar(appl.t0, ecv.cv_Pinv5F)
      appl.parm(P_cvPinv70) = appl.CoeffVar(appl.t0, ecv.cv_Pinv70)
      '--- 4.5 Pensione indiretta ----
      appl.parm(P_cvPind2M) = appl.CoeffVar(appl.t0, ecv.cv_Pind2M)
      appl.parm(P_cvPind2F) = appl.CoeffVar(appl.t0, ecv.cv_Pind2F)
      appl.parm(P_cvPind20M) = appl.CoeffVar(appl.t0, ecv.cv_Pind20M)
      appl.parm(P_cvPind20F) = appl.CoeffVar(appl.t0, ecv.cv_Pind20F)
      appl.parm(P_cvPind30M) = appl.CoeffVar(appl.t0, ecv.cv_Pind30M)
      appl.parm(P_cvPind30F) = appl.CoeffVar(appl.t0, ecv.cv_Pind30F)
      '--- 4.7 Restituzione dei contributi ----
      appl.parm(P_cvRcHminM) = appl.CoeffVar(appl.t0, ecv.cv_RcHminM)
      appl.parm(P_cvRcHminF) = appl.CoeffVar(appl.t0, ecv.cv_RcHminF)
      appl.parm(P_cvRcXminM) = appl.CoeffVar(appl.t0, ecv.cv_RcXminM)
      appl.parm(P_cvRcXminF) = appl.CoeffVar(appl.t0, ecv.cv_RcXminF)
      appl.parm(P_cvRcPrcCon) = appl.CoeffVar(appl.t0, ecv.cv_RcPrcCon)
      'appl.parm(P_cvRcPrcRiv) = appl.CoeffVar(appl.t0, ecv.cv_RcPrcRiv)  '20150512LC (commentato)
      '--- 4.8 Totalizzazione ----
    End If
    '***********************
    'Frame CONTRIBUZIONE (5)
    '***********************
    appl.parm(P_cv5) = Math.Abs(rs.Fields("P_cv5").Value) = 1 '20121024LC
    If appl.parm(P_cv5) = False Then
      '--- 5.1 Contribuzione ridotta ----
      '--- 5.2 Soggettivo ----
      appl.parm(P_cvSogg0Min) = ff0(rs.Fields("P_cvSogg0Min").Value)
      appl.parm(P_cvSogg0MinR) = ff0(rs.Fields("P_cvSogg0MinR").Value)
      appl.parm(P_cvSogg1Al) = ff0(rs.Fields("P_cvSogg1Al").Value)
      appl.parm(P_cvSogg1AlR) = ff0(rs.Fields("P_cvSogg1AlR").Value)
      appl.parm(P_cvSogg1Max) = ff0(rs.Fields("P_cvSogg1Max").Value)
      appl.parm(P_cvSogg1MaxR) = ff0(rs.Fields("P_cvSogg1MaxR").Value)
      '--- 5.3 Integrativo / FIRR ----
      appl.parm(P_cvInt0Min) = ff0(rs.Fields("P_cvInt0Min").Value)
      appl.parm(P_cvInt0MinR) = ff0(rs.Fields("P_cvInt0MinR").Value)
      appl.parm(P_cvInt1Al) = ff0(rs.Fields("P_cvInt1Al").Value)
      appl.parm(P_cvInt1AlR) = ff0(rs.Fields("P_cvInt1AlR").Value)
      appl.parm(P_cvInt1Max) = ff0(rs.Fields("P_cvInt1Max").Value)
      appl.parm(P_cvInt1MaxR) = ff0(rs.Fields("P_cvInt1MaxR").Value)
      appl.parm(P_cvInt2Al) = ff0(rs.Fields("P_cvInt2Al").Value)
      appl.parm(P_cvInt2AlR) = ff0(rs.Fields("P_cvInt2AlR").Value)
      appl.parm(P_cvInt2Max) = ff0(rs.Fields("P_cvInt2Max").Value)
      appl.parm(P_cvInt2MaxR) = ff0(rs.Fields("P_cvInt2MaxR").Value)
      appl.parm(P_cvInt3Al) = ff0(rs.Fields("P_cvInt3Al").Value)
      appl.parm(P_cvInt3AlR) = ff0(rs.Fields("P_cvInt3AlR").Value)
      '--- 5.4 Integrativo ----
      appl.parm(P_cvAss0Min) = ff0(rs.Fields("P_cvAss0Min").Value)
      appl.parm(P_cvAss0MinR) = ff0(rs.Fields("P_cvAss0MinR").Value)
      appl.parm(P_cvAss1Al) = ff0(rs.Fields("P_cvAss1Al").Value)
      appl.parm(P_cvass1AlR) = ff0(rs.Fields("P_cvAss1AlR").Value)
      '--- 5.5 Altro ----
      appl.parm(P_cvMat0Min) = ff0(rs.Fields("P_cvMat0Min").Value)
      appl.parm(P_cvMat0MinR) = ff0(rs.Fields("P_cvMat0MinR").Value)
    Else
      '--- 5.1 Contribuzione ridotta ----
      '--- 5.2 Soggettivo ----
      appl.parm(P_cvSogg0Min) = appl.CoeffVar(appl.t0, ecv.cv_Sogg0Min)
      appl.parm(P_cvSogg0MinR) = appl.CoeffVar(appl.t0, ecv.cv_Sogg0MinR)
      appl.parm(P_cvSogg1Al) = appl.CoeffVar(appl.t0, ecv.cv_Sogg1Al)
      appl.parm(P_cvSogg1AlR) = appl.CoeffVar(appl.t0, ecv.cv_Sogg1AlR)
      appl.parm(P_cvSogg1Max) = appl.CoeffVar(appl.t0, ecv.cv_Sogg1Max)
      appl.parm(P_cvSogg1MaxR) = appl.CoeffVar(appl.t0, ecv.cv_Sogg1MaxR)
      '--- 5.3 Integrativo / FIRR ----
      appl.parm(P_cvInt0Min) = appl.CoeffVar(appl.t0, ecv.cv_Int0Min)
      appl.parm(P_cvInt0MinR) = appl.CoeffVar(appl.t0, ecv.cv_Int0MinR)
      appl.parm(P_cvInt1Al) = appl.CoeffVar(appl.t0, ecv.cv_Int1Al)
      appl.parm(P_cvInt1AlR) = appl.CoeffVar(appl.t0, ecv.cv_Int1AlR)
      appl.parm(P_cvInt1Max) = appl.CoeffVar(appl.t0, ecv.cv_Int1Max)
      appl.parm(P_cvInt1MaxR) = appl.CoeffVar(appl.t0, ecv.cv_Int1MaxR)
      appl.parm(P_cvInt2Al) = appl.CoeffVar(appl.t0, ecv.cv_Int2Al)
      appl.parm(P_cvInt2AlR) = appl.CoeffVar(appl.t0, ecv.cv_Int2AlR)
      appl.parm(P_cvInt2Max) = appl.CoeffVar(appl.t0, ecv.cv_Int2Max)
      appl.parm(P_cvInt2MaxR) = appl.CoeffVar(appl.t0, ecv.cv_Int2MaxR)
      appl.parm(P_cvInt3Al) = appl.CoeffVar(appl.t0, ecv.cv_Int3Al)
      appl.parm(P_cvInt3AlR) = appl.CoeffVar(appl.t0, ecv.cv_Int3AlR)
      '--- 5.4 Assistenziale ----
      appl.parm(P_cvAss0Min) = appl.CoeffVar(appl.t0, ecv.cv_Ass0Min)
      appl.parm(P_cvAss0MinR) = appl.CoeffVar(appl.t0, ecv.cv_Ass0MinR)
      appl.parm(P_cvAss1Al) = appl.CoeffVar(appl.t0, ecv.cv_Ass1Al)
      appl.parm(P_cvass1AlR) = appl.CoeffVar(appl.t0, ecv.cv_Ass1AlR)
      '--- 5.5 Altro ----
      appl.parm(P_cvMat0Min) = appl.CoeffVar(appl.t0, ecv.cv_Mat0Min)
      appl.parm(P_cvMat0MinR) = appl.CoeffVar(appl.t0, ecv.cv_Mat0MinR)
    End If
    '****************************
    'Frame MISURA PRESTAZIONI (6)
    '****************************
    appl.parm(P_cv6) = Math.Abs(rs.Fields("P_cv6").Value) = 1 '20121024LC
    If appl.parm(P_cv6) = False Then
      '--- 6.1 Scaglioni IRPEF ----
      '--- 6.2 Pensione ----
      '--- 6.3 Supplementi di pensione ----
      appl.parm(P_cvSpAnni) = rs.Fields("P_cvSpANNI").Value
      appl.parm(P_cvSpEtaMax) = rs.Fields("P_cvSpEtaMax").Value
      appl.parm(P_cvSpPrcCon) = rs.Fields("P_cvSpPRCCON").Value
      'appl.parm(P_cvSpPrcRiv) = rs.Fields("P_cvSpPRCRIV").Value
      '--- 6.4 Reversibilit� superstiti ----
      appl.parm(P_cvAv) = rs.Fields("P_cvAv").Value
      appl.parm(P_cvAvo) = rs.Fields("P_cvAvO").Value
      appl.parm(P_cvAvoo) = rs.Fields("P_cvAvOO").Value
      appl.parm(P_cvAo) = rs.Fields("P_cvAo").Value
      appl.parm(P_cvAoo) = rs.Fields("P_cvAoO").Value
      appl.parm(P_cvAooo) = rs.Fields("P_cvAoOO").Value
    Else
      '--- 6.1 Scaglioni IRPEF ----
      '--- 6.2 Pensione ----
      '--- 6.3 Supplementi di pensione ----
      appl.parm(P_cvSpAnni) = appl.CoeffVar(appl.t0, ecv.cv_SpAnni)
      appl.parm(P_cvSpEtaMax) = appl.CoeffVar(appl.t0, ecv.cv_SpEtaMax)
      appl.parm(P_cvSpPrcCon) = appl.CoeffVar(appl.t0, ecv.cv_SpPrcCon)
      'appl.parm(P_cvSpPrcRiv) = appl.CoeffVar(appl.t0, ecv.cv_SpPrcRiv)  '20150512LC (commentato)
      '--- 6.4 Reversibilit� superstiti ----
      appl.parm(P_cvAv) = appl.CoeffVar(appl.t0, ecv.cv_Av)
      appl.parm(P_cvAvo) = appl.CoeffVar(appl.t0, ecv.cv_Avo)
      appl.parm(P_cvAvoo) = appl.CoeffVar(appl.t0, ecv.cv_Avoo)
      appl.parm(P_cvAo) = appl.CoeffVar(appl.t0, ecv.cv_Ao)
      appl.parm(P_cvAoo) = appl.CoeffVar(appl.t0, ecv.cv_Aoo)
      appl.parm(P_cvAooo) = appl.CoeffVar(appl.t0, ecv.cv_Aooo)
    End If
    '************************
    'Frame NUOVI ISCRITTI (7)
    '************************
    appl.parm(P_cv7) = Math.Abs(rs.Fields("P_cv7").Value) = 1 '20121024LC
    If appl.parm(P_cv7) = False Then
      '20120319LC (inizio)
      appl.parm(P_niTipo) = ff0(rs.Fields("P_niTIPO").Value)
      i = appl.parm(P_niTipo)
      appl.parm(P_niP) = IIf(i >= 2, LCase(Trim("" & rs.Fields("P_niP").Value)), "")  '20140522LC
      appl.parm(P_niStat) = IIf(i >= 2, LCase(Trim("" & rs.Fields("P_niStat").Value)), "")
      '20120319LC (fine)
      '--- 20150527LC (inizio)
      appl.parm(P_neStat) = IIf(1 = 1, LCase(Trim("" & rs.Fields("P_neStat").Value)), "")
      appl.parm(P_nvStat) = IIf(1 = 1, LCase(Trim("" & rs.Fields("P_nvStat").Value)), "")
      '--- 20150527LC (fine)
    Else
      'call MsgBox("TODO")
      'Stop
    End If
  End If
  rs.Close
  '--- 20121224LC (inizio)
  szErr = LeggiCoeffVar2(appl.parm(), appl.CoeffVar(), appl.cv(), appl.cv4())
  If IO = "I" And szErr = "" Then
    Call LeggiCoeffRiv(glo.db, appl.t0 + 1, appl.parm(), appl.CoeffVar(), appl.CoeffRiv())
  End If
  '--- 20121224LC (fine)
  '--- 20170307LC (inizio)
  If szErr = "" Then
    szErr = LeggiCoeffVar4(appl, glo)
  End If
  '--- 20170307LC (fine)
ExitPoint: '20120607LC
  TAppl_ParamDbGet = szErr$
End Function


Public Function TAppl_ParamDbPut$(ByRef appl As TAppl, ByRef glo As TGlobale, db As ADODB.Connection, szGruppo$, IO$) '20121024LC
  Dim i As Byte, j As Byte, j1 As Byte, j2 As Byte
  Dim SQL$, szErr$
  Dim rs As New ADODB.Recordset
  
  If IO = "I" Then
    SQL = "SELECT *"
    SQL = SQL & " FROM I_PARAM"
    SQL = SQL & " WHERE P_ID='" & UCase(szGruppo) & "'" '20121024LC
  Else
    SQL = "SELECT *"
    SQL = SQL & " FROM O_PARAM"
    SQL = SQL & " WHERE opProgr=" & appl.idReport
  End If
  rs.Open SQL, db, adOpenKeyset, adLockOptimistic
  '20120319LC (inizio)
  If rs.EOF = False Then
    rs.Delete
  End If
  If 1 = 1 Or rs.EOF = True Then
  '20120319LC (fine)
    rs.AddNew
    If IO = "O" Then
      rs.Fields("opProgr").Value = appl.idReport
    End If
    rs.Fields("p_DIns").Value = Now
    rs.Fields("p_OpIns").Value = 1
  Else
    rs.Fields("p_DVar").Value = Now
    rs.Fields("p_OpVar").Value = 1
  End If
  If IO = "O" Then
    rs.Fields("opMatr1").Value = appl.matr1
    rs.Fields("opMatr2").Value = appl.matr2
    '--- 20161112LC (inizio)
    rs.Fields("opIter").Value = appl.iterNum
    rs.Fields("opCvNome").Value = Left(appl.iterCvNome, rs.Fields("opCvNome").DefinedSize)
    '--- 20161112LC (fine)
  End If
  '*******************
  'Frame PARAMETRI (1)
  '*******************
  rs.Fields("P_ID").Value = szGruppo
  rs.Fields("P_cv1").Value = IIf(glo.DB_TYPE = DB_ORACLE, Math.Abs(appl.parm(P_cv1)), appl.parm(P_cv1)) '20121024LC
  If appl.parm(P_cv1) = False Then
    '--- Bilancio Tecnico ----
    rs.Fields("P_btAnno").Value = appl.t0
    rs.Fields("P_btAnniEl1").Value = appl.parm(P_btAnniEl1)
    rs.Fields("P_btAnniEl2").Value = appl.parm(P_btAnniEl2)
    rs.Fields("P_btPatrim").Value = appl.parm(P_btPatrim)
    rs.Fields("P_btSpAmm").Value = appl.parm(P_btSpAmm)
    rs.Fields("P_btSpAss").Value = appl.parm(P_btSpAss)
    rs.Fields("P_btPassSIV").Value = appl.parm(P_btPassSIV)
    rs.Fields("P_btPassSIP").Value = appl.parm(P_btPassSIP)
    '--- 1.2 Tipo di calcolo ----
    rs.Fields("P_TIPOELAB").Value = appl.parm(P_TipoElab)
    '--- 1.3 Modalit� di analisi ---
    rs.Fields("P_SQL").Value = Sql_Normalize(CStr(appl.parm(P_SQL))) '20120213LC
    If Trim("" & appl.parm(P_FOUT)) = "" Then
      rs.Fields("P_FOUT").Value = szGruppo & ".fout"
    ElseIf InStr(appl.parm(P_FOUT), ".") Then
      rs.Fields("P_FOUT").Value = appl.parm(P_FOUT)
    Else
      rs.Fields("P_FOUT").Value = appl.parm(P_FOUT) & ".fout"
    End If
  Else
    Call MsgBox("TODO TAppl_ParamDbPut 1")
    Stop
  End If
  '******************************
  'Frame TASSI ED ALTRE STIME (2)
  '******************************
  rs.Fields("P_cv2").Value = IIf(glo.DB_TYPE = DB_ORACLE, Math.Abs(appl.parm(P_cv2)), appl.parm(P_cv2)) '20121024LC
  If appl.parm(P_cv2) = False Then
    '--- 2.1 Tassi di valutazione e rivalutazione ---
    rs.Fields("P_cvTv").Value = appl.parm(P_cvTv)
    rs.Fields("P_cvTi").Value = appl.parm(P_cvTi)
    rs.Fields("P_cvTev").Value = appl.parm(P_cvTev)
    rs.Fields("P_cvTrn").Value = appl.parm(P_cvTrn)
    '--- 2.2 Tassi di rivalutazione dei montanti contributivi ---
    rs.Fields("P_cvTrmc0").Value = appl.parm(P_cvTrmc0)
    rs.Fields("P_cvTrmc1").Value = appl.parm(P_cvTrmc1)
    rs.Fields("P_cvTrmc2").Value = appl.parm(P_cvTrmc2)
    rs.Fields("P_cvTrmc3").Value = appl.parm(P_cvTrmc3)
    rs.Fields("P_cvTrmc4").Value = appl.parm(P_cvTrmc4)
    rs.Fields("P_cvTrmc5").Value = appl.parm(P_cvTrmc5)
    rs.Fields("P_cvTrmc6").Value = appl.parm(P_cvTrmc6)
    '--- 2.3 Tassi di rivalutazione di redditi e volumi d'affari ---
    rs.Fields("P_cvTs1A").Value = appl.parm(P_cvTs1a)
    rs.Fields("P_cvTs1P").Value = appl.parm(P_cvTs1p)
    rs.Fields("P_cvTs1N").Value = appl.parm(P_cvTs1n)
    '--- 2.4 Rivalutazione delle pensioni ---
    rs.Fields("P_cvTp1Max").Value = appl.parm(P_cvTp1Max)
    rs.Fields("P_cvTp1Al").Value = appl.parm(P_cvTp1Al)
    rs.Fields("P_cvTp2Max").Value = appl.parm(P_cvTp2Max)
    rs.Fields("P_cvTp2Al").Value = appl.parm(P_cvTp2Al)
    rs.Fields("P_cvTp3Max").Value = appl.parm(P_cvTp3Max)
    rs.Fields("P_cvTp3Al").Value = appl.parm(P_cvTp3Al)
    '--- 20161112LC (inizio)
    rs.Fields("P_cvTp4Max").Value = appl.parm(P_cvTp4Max)
    rs.Fields("P_cvTp4Al").Value = appl.parm(P_cvTp4Al)
    rs.Fields("P_cvTp5Max").Value = appl.parm(P_cvTp5Max)
    rs.Fields("P_cvTp5Al").Value = appl.parm(P_cvTp5Al)
    '--- 20161112LC (fine)
    '--- 2.5 Altre stime ----
    rs.Fields("P_cvPrcPVattM").Value = appl.parm(P_cvPrcPVattM)
    rs.Fields("P_cvPrcPVattF").Value = appl.parm(P_cvPrcPVattF)
    rs.Fields("P_cvPrcPAattM").Value = appl.parm(P_cvPrcPAattM)
    rs.Fields("P_cvPrcPAattF").Value = appl.parm(P_cvPrcPAattF)
    rs.Fields("P_cvPrcPCattM").Value = appl.parm(P_cvPrcPCattM)
    rs.Fields("P_cvPrcPCattF").Value = appl.parm(P_cvPrcPCattF)
  Else
    '20120319LC (inizio)
    '--- 2.1 Tassi di valutazione e rivalutazione ---
    rs.Fields("P_cvTv").Value = appl.parm(P_cvTv)
    rs.Fields("P_cvTi").Value = appl.parm(P_cvTi)
    '--- 2.5 Altre stime ----
    rs.Fields("P_cvPrcPVattM").Value = appl.parm(P_cvPrcPVattM)
    rs.Fields("P_cvPrcPVattF").Value = appl.parm(P_cvPrcPVattF)
    rs.Fields("P_cvPrcPAattM").Value = appl.parm(P_cvPrcPAattM)
    rs.Fields("P_cvPrcPAattF").Value = appl.parm(P_cvPrcPAattF)
    rs.Fields("P_cvPrcPCattM").Value = appl.parm(P_cvPrcPCattM)
    rs.Fields("P_cvPrcPCattF").Value = appl.parm(P_cvPrcPCattF)
    '20120319LC (fine)
  End If
  '***********************
  'Frame BASI_TECNICHE (3)
  '***********************
  rs.Fields("P_cv3").Value = IIf(glo.DB_TYPE = DB_ORACLE, Math.Abs(appl.parm(P_cv3)), appl.parm(P_cv3)) '20121024LC
  If appl.parm(P_cv3) = False Then
    '--- 3.1 Probabilit� di morte ----
    rs.Fields("P_LAD").Value = appl.parm(P_LAD)
    rs.Fields("P_LED").Value = appl.parm(P_LED)
    rs.Fields("P_LPD").Value = appl.parm(P_LPD)
    rs.Fields("P_LSD").Value = appl.parm(P_LSD)
    rs.Fields("P_LBD").Value = appl.parm(P_LBD)
    rs.Fields("P_LID").Value = appl.parm(P_LID)
    '--- 3.2 Altre probabilit� ----
    rs.Fields("P_LAV").Value = appl.parm(P_LAV)
    rs.Fields("P_LAE").Value = appl.parm(P_LAE)
    rs.Fields("P_LAB").Value = appl.parm(P_LAB)
    rs.Fields("P_LAI").Value = appl.parm(P_LAI)
    rs.Fields("P_LAW").Value = appl.parm(P_LAW)
    '--- 3.3 Linee reddituali ----
    rs.Fields("P_LRA").Value = appl.parm(P_LRA)
    rs.Fields("P_LRP").Value = appl.parm(P_LRP)
    rs.Fields("P_LRN").Value = appl.parm(P_LRN)
    '--- 3.4 Coefficienti di conversione del montante in rendita ----
    rs.Fields("P_CCR").Value = appl.parm(P_CCR)
    '--- 3.5 Superstiti ----
    rs.Fields("P_PF").Value = appl.parm(P_PF)
    rs.Fields("P_ZMAX").Value = appl.parm(P_ZMAX)
  Else
    Call MsgBox("TODO TAppl_ParamDbPut 2")
    Stop
  End If
  '******************************
  'Frame REQUISITI DI ACCESSO (4)
  '******************************
  rs.Fields("P_cv4").Value = IIf(glo.DB_TYPE = DB_ORACLE, Math.Abs(appl.parm(P_cv4)), appl.parm(P_cv4)) '20121024LC
  If appl.parm(P_cv4) = False Then
    '--- 4.1 Pensione di vecchiaia ordinaria ----
    rs.Fields("P_cvPveo30M").Value = appl.parm(P_cvPveo30M)
    rs.Fields("P_cvPveo30F").Value = appl.parm(P_cvPveo30F)
    rs.Fields("P_cvPveo65M").Value = appl.parm(P_cvPveo65M)
    rs.Fields("P_cvPveo65F").Value = appl.parm(P_cvPveo65F)
    rs.Fields("P_cvPveo98M").Value = appl.parm(P_cvPveo98M)
    rs.Fields("P_cvPveo98F").Value = appl.parm(P_cvPveo98F)
    '--- 4.2 Pensione di vecchiaia anticipata ----
    rs.Fields("P_cvPvea35M").Value = appl.parm(P_cvPvea35M)
    rs.Fields("P_cvPvea35F").Value = appl.parm(P_cvPvea35F)
    rs.Fields("P_cvPvea58M").Value = appl.parm(P_cvPvea58M)
    rs.Fields("P_cvPvea58F").Value = appl.parm(P_cvPvea58F)
    '--- 4.3 Pensione di vecchiaia posticipata ----
    rs.Fields("P_cvPvep70M").Value = appl.parm(P_cvPvep70M)
    rs.Fields("P_cvPvep70F").Value = appl.parm(P_cvPvep70F)
    '--- 4.4 Pensione di inabilit� ----
    rs.Fields("P_cvPinab2M").Value = appl.parm(P_cvPinab2M)
    rs.Fields("P_cvPinab2F").Value = appl.parm(P_cvPinab2F)
    rs.Fields("P_cvPinab10M").Value = appl.parm(P_cvPinab10M)
    rs.Fields("P_cvPinab10F").Value = appl.parm(P_cvPinab10F)
    rs.Fields("P_cvPinab35M").Value = appl.parm(P_cvPinab35M)
    rs.Fields("P_cvPinab35F").Value = appl.parm(P_cvPinab35F)
    '--- 4.5 Pensione di invalidit� ----
    rs.Fields("P_cvPinv5M").Value = appl.parm(P_cvPinv5M)
    rs.Fields("P_cvPinv5F").Value = appl.parm(P_cvPinv5F)
    rs.Fields("P_cvPinv70").Value = appl.parm(P_cvPinv70)
    '--- 4.6 Pensione indiretta ----
    rs.Fields("P_cvPind2M").Value = appl.parm(P_cvPind2M)
    rs.Fields("P_cvPind2F").Value = appl.parm(P_cvPind2F)
    rs.Fields("P_cvPind20M").Value = appl.parm(P_cvPind20M)
    rs.Fields("P_cvPind20F").Value = appl.parm(P_cvPind20F)
    rs.Fields("P_cvPind30M").Value = appl.parm(P_cvPind30M)
    rs.Fields("P_cvPind30F").Value = appl.parm(P_cvPind30F)
    '--- 4.7 Restituzione dei contributi ----
    rs.Fields("P_cvRcHminM").Value = appl.parm(P_cvRcHminM)
    rs.Fields("P_cvRcHminF").Value = appl.parm(P_cvRcHminF)
    rs.Fields("P_cvRcXminM").Value = appl.parm(P_cvRcXminM)
    rs.Fields("P_cvRcXminF").Value = appl.parm(P_cvRcXminF)
    rs.Fields("P_cvRcPrcCon").Value = appl.parm(P_cvRcPrcCon)
    'rs.Fields("P_cvRcPrcRiv").Value = appl.parm(P_cvRcPrcRiv)  '20150512LC (commentato)
    '--- 4.8 Totalizzazione ----
  Else
    'call MsgBox("TODO")
    'Stop
  End If
  '***********************
  'Frame CONTRIBUZIONE (5)
  '***********************
  rs.Fields("P_cv5").Value = IIf(glo.DB_TYPE = DB_ORACLE, Math.Abs(appl.parm(P_cv5)), appl.parm(P_cv5)) '20121024LC
  If appl.parm(P_cv5) = False Then
    '--- 5.1 Contribuzione ridotta ----
    '--- 5.2 Soggettivo ----
    rs.Fields("P_cvSogg0Min").Value = appl.parm(P_cvSogg0Min)
    rs.Fields("P_cvSogg0MinR").Value = appl.parm(P_cvSogg0MinR)
    rs.Fields("P_cvSogg1Al").Value = appl.parm(P_cvSogg1Al)
    rs.Fields("P_cvSogg1AlR").Value = appl.parm(P_cvSogg1AlR)
    rs.Fields("P_cvSogg1Max").Value = appl.parm(P_cvSogg1Max)
    rs.Fields("P_cvSogg1MaxR").Value = appl.parm(P_cvSogg1MaxR)
    '--- 5.3 Integrativo / FIRR ----
    rs.Fields("P_cvInt0Min").Value = appl.parm(P_cvInt0Min)
    rs.Fields("P_cvInt0MinR").Value = appl.parm(P_cvInt0MinR)
    rs.Fields("P_cvInt1Al").Value = appl.parm(P_cvInt1Al)
    rs.Fields("P_cvInt1AlR").Value = appl.parm(P_cvInt1AlR)
    rs.Fields("P_cvInt1Max").Value = appl.parm(P_cvInt1Max)
    rs.Fields("P_cvInt1MaxR").Value = appl.parm(P_cvInt1MaxR)
    rs.Fields("P_cvInt2Al").Value = appl.parm(P_cvInt2Al)
    rs.Fields("P_cvInt2AlR").Value = appl.parm(P_cvInt2AlR)
    rs.Fields("P_cvInt2Max").Value = appl.parm(P_cvInt2Max)
    rs.Fields("P_cvInt2MaxR").Value = appl.parm(P_cvInt2MaxR)
    rs.Fields("P_cvInt3Al").Value = appl.parm(P_cvInt3Al)
    rs.Fields("P_cvInt3AlR").Value = appl.parm(P_cvInt3AlR)
    '--- 5.4 Assistenziale ----
    rs.Fields("P_cvAss0Min").Value = appl.parm(P_cvAss0Min)
    rs.Fields("P_cvAss0MinR").Value = appl.parm(P_cvAss0MinR)
    rs.Fields("P_cvAss1Al").Value = appl.parm(P_cvAss1Al)
    rs.Fields("P_cvAss1AlR").Value = appl.parm(P_cvass1AlR)
    '--- 5.5 Altro ----
    rs.Fields("P_cvMat0Min").Value = appl.parm(P_cvMat0Min)
    rs.Fields("P_cvMat0MinR").Value = appl.parm(P_cvMat0MinR)
  Else
    'call MsgBox("TODO")
    'Stop
  End If
  '****************************
  'Frame MISURA PRESTAZIONI (6)
  '****************************
  rs.Fields("P_cv6").Value = IIf(glo.DB_TYPE = DB_ORACLE, Math.Abs(appl.parm(P_cv6)), appl.parm(P_cv6)) '20121024LC
  If appl.parm(P_cv6) = False Then
    '--- 6.1 Scaglioni IRPEF ----
    '--- 6.2 Pensione ----
    rs.Fields("P_cvPMin").Value = appl.parm(P_cvPMin)
    rs.Fields("P_cvNMR").Value = appl.parm(P_cvNMR)
    rs.Fields("P_cvNTR").Value = appl.parm(P_cvNTR)
    '--- 6.3 Supplementi di pensione ----
    rs.Fields("P_cvSpAnni").Value = appl.parm(P_cvSpAnni)
    rs.Fields("P_cvSpEtaMax").Value = appl.parm(P_cvSpEtaMax)
    rs.Fields("P_cvSpPrcCon").Value = appl.parm(P_cvSpPrcCon)
    'rs.Fields("P_cvSpPrcRiv").Value = appl.parm(P_cvSpPrcRiv)
    '--- 6.4 Reversibilit� superstiti ----
    rs.Fields("P_cvAv").Value = appl.parm(P_cvAv)
    rs.Fields("P_cvAvo").Value = appl.parm(P_cvAvo)
    rs.Fields("P_cvAvoo").Value = appl.parm(P_cvAvoo)
    rs.Fields("P_cvAo").Value = appl.parm(P_cvAo)
    rs.Fields("P_cvAoo").Value = appl.parm(P_cvAoo)
    rs.Fields("P_cvAooo").Value = appl.parm(P_cvAooo)
  Else
    'call MsgBox("TODO")
    'Stop
  End If
  '************************
  'Frame NUOVI ISCRITTI (7)
  '************************
  rs.Fields("P_cv7").Value = IIf(glo.DB_TYPE = DB_ORACLE, Math.Abs(appl.parm(P_cv7)), appl.parm(P_cv7)) '20121024LC
  If appl.parm(P_cv7) = False Then
    '20120319LC (inizio)
    i = appl.parm(P_niTipo)
    rs.Fields("P_niTIPO").Value = i
    rs.Fields("P_niP").Value = IIf(i >= 2, appl.parm(P_niP), Null)  '20140522LC
    rs.Fields("P_niStat").Value = IIf(i >= 2, appl.parm(P_niStat), Null)
    '20120319LC (fine)
    '--- 20150612LC (inizio)
    rs.Fields("P_neStat").Value = IIf(appl.parm(P_neStat) = "", Null, appl.parm(P_neStat))
    rs.Fields("P_nvStat").Value = IIf(appl.parm(P_nvStat) = "", Null, appl.parm(P_nvStat))
    '--- 20150612LC (fine)
  Else
    'call MsgBox("TODO")
    'Stop
  End If
  '*************
  'COMPLETAMENTO
  '*************
  rs.Update
'rs.CancelUpdate
  rs.Close

ExitPoint:
  TAppl_ParamDbPut = szErr$
End Function


Public Sub TAppl_ParamSalva(ByRef appl As TAppl, ByRef glo As TGlobale, frm As Object) '20130307LC
  '*************************
  'Salva i dati dell'analisi
  '*************************
  Dim szErr$, szGruppo$
  Dim req As Object
Call MsgBox("TODO - req")
Stop

  szGruppo = LCase(Trim(appl.parm(P_ID)))
  If szGruppo = "base" Then szGruppo = ""
  '20160323LC (10 caratteri)
  szGruppo = LCase(Trim(InputBox("Nome da attribuire all'attuale gruppo di parametri (da 1 a 10 caratteri)", "Salva parametri", szGruppo)))
  If szGruppo = "" Then
    Call MsgBox("Operazione annullata", vbInformation)
    Exit Sub
  ElseIf szGruppo = "base" Then
    Call MsgBox("Il nome BASE � riservato", vbExclamation)
    Exit Sub
  ElseIf Len(szGruppo) > 10 Then  '20160323LC
    Call MsgBox("La lunghezza massima del nome del gruppo � 10 caratteri", vbExclamation)  '20160323LC
    Exit Sub
  End If
  'Call TAppl_DatiGet(appl, frm)
  If glo.MJ = MI_VB Then  '20130307LC
    Call TAppl_Mask2Req(appl, req, frm)
  End If
  Call TAppl_Req2Dati(appl, req)
  szErr = TAppl_ParamDbPut(appl, glo, glo.db, szGruppo, "I")
  If szErr = "" Then
    szErr = modAfpServer.Param_MaskLoad_Prm(glo.MJ, glo.db, frm)  '20130307LC
    frm.Combo1(P_ID).Text = szGruppo
  End If
  If szErr <> "" Then
    '20120319LC (inizio)
    'call MsgBox(szErr, vbCritical, "ERRORE CRITICO"
    'End
    Call MsgBox(szErr, vbExclamation)
    '20120319LC (fine)
  Else
    Call MsgBox("Il gruppo di parametri � stato salvato con il nome: " & szGruppo)
  End If
End Sub


'20080825LC
Public Function TAppl_ReportDbGet$(ByRef appl As TAppl, ByRef glo As TGlobale, tb1#(), db As ADODB.Connection)
  Dim i&, ijk&, j&, k&, l&
  Dim SQL$, szErr$
  Dim rs As New ADODB.Recordset
  
  szErr = ""
  Screen.MousePointer = vbHourglass
  '***************************
  'Legge dalla tabella O_PARAM
  '***************************
  szErr = TAppl_ParamDbGet(appl, glo, db, CStr(appl.idReport), "O")
  If szErr <> "" Then GoTo ExitPoint '20121021LC
  appl.nMax1 = appl.parm(P_btAnniEl1)
  appl.nMax2 = appl.nMax1 + OPZ_ELAB_CODA
  '20121028LC (inizio)
  appl.nSize4 = (1 + TB_LAST)
  'appl.nSize3 = (1 + appl.Nmax2 + 1) * appl.nSize4
  appl.nSize3 = (1 + appl.nMax2 + 1 + appl.t0 - 1960) * appl.nSize4  '20121114LC
  appl.nSize2 = (1 + 4) * appl.nSize3
  appl.nSize1 = (1 + OPZ_INT_SUDDIVISIONI_N) * appl.nSize2
  ReDim tb1(appl.nSize1 - 1)
  '20121028LC (fine)
  '*************************
  'Legge dalla tabella O_DAT
  '*************************
  SQL = "SELECT * FROM O_DAT WHERE odID=" & appl.idReport
  rs.Open SQL, db, adOpenForwardOnly, adLockReadOnly
  Do Until rs.EOF
    i = 0
    j = rs(1)
    k = rs(2)
    For l = 0 To TB_LAST
      ijk = appl.nSize2 * i + appl.nSize3 * j + appl.nSize4 * (k + appl.t0 - 1960) '20121114LC
      tb1(ijk + l) = ff0(rs(l + 3).Value)  '20160613LC
    Next l
    rs.MoveNext
  Loop
  rs.Close
  
ExitPoint:
  On Error GoTo 0
  Screen.MousePointer = vbDefault
  TAppl_ReportDbGet = szErr
  Exit Function
  
ErrorHandler:
  szErr = "Errore " & Err.Number & " (" & Hex(Err.Number) & ") nel modulo " & Err.Source & vbCrLf & Err.Description  '20161112LC
  GoTo ExitPoint
End Function


Public Function TAppl_ReportDbPut$(ByRef appl As TAppl, ByRef glo As TGlobale, tb1#(), db As ADODB.Connection)
  Dim ijk&, iTS&, t&, l& '20121028LC
  Dim SQL$, szErr$
  Dim rs As New ADODB.Recordset
  
  Screen.MousePointer = vbHourglass
  '***********************
  'Cattura l'ID del report
  '***********************
  SQL = "SELECT MAX(opProgr)"
  SQL = SQL & " FROM O_PARAM"
  rs.Open SQL, db, adOpenForwardOnly, adLockReadOnly
  appl.idReport = ff0(rs(0)) + 1
  rs.Close
  '****************************
  'Scrive sulla tabella O_PARAM
  '****************************
  Call TAppl_ParamDbPut(appl, glo, db, glo.szGruppo, "O")
  '**************************
  'Scrive sulla tabella O_DAT
  '**************************
  SQL = "SELECT *"
  SQL = SQL & " FROM O_DAT"
  rs.Open SQL, db, adOpenKeyset, adLockOptimistic
  For iTS = 0 To OPZ_INT_SUDDIVISIONI_N '20121028LC  'UBound(tb, 1)
    For t = 0 To appl.nMax2 + 1 '20121028LC          'UBound(tb, 3) '20080617LC
      rs.AddNew
      rs(0) = appl.idReport
      rs(1) = iTS
      rs(2) = t
      For l = 0 To TB_LAST
        ijk = appl.nSize2 * iTS + appl.nSize3 * 0 + appl.nSize4 * (t + appl.t0 - 1960) '20121114LC   '10
        rs(l + 3) = tb1(ijk + l)
      Next l
      rs.Update
'rs.CancelUpdate
    Next t
  Next iTS
  rs.Close
  
ExitPoint:
  On Error GoTo 0
  Screen.MousePointer = vbDefault
  TAppl_ReportDbPut = szErr
  Exit Function
  
ErrorHandler:
  szErr = "Errore " & Err.Number & " (" & Hex(Err.Number) & ") nel modulo " & Err.Source & vbCrLf & Err.Description  '20161112LC
  GoTo ExitPoint
End Function



'20120206LC
Private Sub TAppl_Stampa3a(ByRef appl As TAppl, ByRef glo As TGlobale, fo&, xlsApp As Object, iop%, iTS&)
  Dim i As Byte
  Dim anno0%, iRiga%, iCol%
  Dim tim1#
  Dim a$
  Dim xlsWsh As Object
  
  anno0 = Year(appl.DataBilancio)
  If iop <> 1 And Not xlsApp Is Nothing Then
    Set xlsWsh = xlsApp.ActiveWorkbook.Worksheets(1)
  End If
  tim1 = Timer
  If iop < 2 Then
    Print #fo, "  ***********************************************************"
    Print #fo, "  *                          A F P                          *"
    Print #fo, "  * ------------------------------------------------------- *"
    Print #fo, "  * Calcolo delle riserve matematiche per il fondo pensioni *"
    Print #fo, "  * degli iscritti all'ENASARCO                             *"
    Print #fo, "  * ------------------------------------------------------- *"
    Print #fo, "  * Versione " & AFP_SVER & Space(47 - Len(AFP_SVER)) & "*"
    Print #fo, "  ***********************************************************"
    Print #fo,
    If glo.rep_chk(REP1_PRM) = vbChecked Then
      Print #fo, "  1. DATI GENERALI"
      Print #fo, "  ================"
      Print #fo, "  - Numero progressivo dell'analisi.............................. (progr) : "; appl.idReport & IIf(iTS > 0, " (suddivisione:" & iTS & ")", "")
      Print #fo, "  - Codice del gruppo di parametri utilizzato per l'analisi......... (id) : "; glo.szGruppo 'Combo1(0).Text
      Print #fo, "  - Data di elaborazione......................................... (delab) : "; Format(Now, "dd/mm/yyyy hh:nn:ss")
      If appl.matr2 <> appl.matr1 Then
        a$ = "  - Matricole esaminate (da " & appl.matr1 & " a " & appl.matr2 & ")"
        Print #fo, a$; String(66 - Len(a), "."); " (nmat) : "; appl.nMatr & " (riattivazioni = " & appl.nDipGenBk & ")"  '20160411LC
      Else
        a$ = "  - Matricola esaminata (et�=" & appl.xa & ", anz.t�=" & Format(appl.h, "#0.00") & ")" & " (riattivazioni =" & appl.nDipGenBk & ")"  '20160411LC
        Print #fo, a$; String(67 - Len(a), "."); " (mat) : "; appl.matr1
      End If
      '*******************
      'Frame PARAMETRI (1)
      '*******************
      '--- 1.1 Bilancio tecnico ----
      Print #fo, "  1.1 BILANCIO TECNICO"
      Print #fo, "  - Data del bilancio tecnico................................... (btData) : "; Format(appl.DataBilancio, "dd/mm/yyyy")
      Print #fo, "  - Anni di valutazione....................................... (btAnniEl) : "; fs(CStr(appl.parm(P_btAnniEl1)), 10, 1) '20080416LC
      Print #fo, "  - Patrimonio iniziale....................................... (btPatrim) : "; ValFmt(CDbl(appl.parm(P_btPatrim)), 10)
      If appl.parm(P_btSpAmm) < 1 Then
        Print #fo, "  - % spese di amministrazione (su contributi)................. (btSpAmm) : "; ValFmt(CDbl(appl.parm(P_btSpAmm) * 100), 9); "%"
      Else
        Print #fo, "  - spese di amministrazione................................... (btSpAmm) : "; ValFmt(CDbl(appl.parm(P_btSpAmm)), 10)
      End If
      If appl.parm(P_btSpAss) < 1 Then
        Print #fo, "  - % prestazioni assistenziali (su contributi)................ (btSpAss) : "; ValFmt(CDbl(appl.parm(P_btSpAss) * 100), 9); "%"
      Else
        Print #fo, "  - prestazioni assistenziali ................................. (btSpAss) : "; ValFmt(CDbl(appl.parm(P_btSpAss)), 10)
      End If
      Print #fo, "  - Contributi passivi societ� di capitali.................. (btPassSIV) : "; ValFmt(CDbl(appl.parm(P_btPassSIV)), 10)
      Print #fo, "  - Incremento annuo contributi passivi societ� di capitali. (btPassSIP) : "; ValFmt(CDbl(appl.parm(P_btPassSIP) * 100), 9); "%"
      '--- 1.2 Tipo di calcolo ----
      Print #fo, "  1.2 TIPO DI CALCOLO"
      Print #fo, "  - tipo prescelto............................................ (tipoelab) : ";
      If appl.parm(P_TipoElab) = 0 Then
        a$ = "metodo retributivo"
      ElseIf appl.parm(P_TipoElab) = 1 Then
        a$ = "metodo contributivo"
      Else
        a$ = "metodo misto, contributivo dal " & appl.parm(P_TipoElab)
      End If
      Print #fo, a$
      '--- 1.3 Modalit� di analisi ---
      Print #fo, "  1.3 MODALITA' DI ANALISI"
      '--- 20161111LC (inizio)
      Print #fo, "  - iterazione.................................................... (iter) : " & appl.iterNum
      Print #fo, "  - coefficienti variabili...................................... (cvNome) : "; appl.iterCvNome
      '--- 20161111LC (fine)
      Print #fo, "  - query di estrazione............................................ (sql) : " & appl.parm(P_SQL)
      Print #fo, "  - file di output................................................ (fout) : "; appl.parm(P_FOUT)
      '******************************
      'Frame TASSI ED ALTRE STIME (2)
      '******************************
      Print #fo,
      Print #fo, "  2. TASSI ED ALTRE STIME"
      Print #fo, "  ======================="
      '--- 2.1 Tassi di valutazione e rivalutazione ---
      Print #fo, "  2.1 ALTRI TASSI DI VALUTAZIONE E RIVALUTAZIONE"
      Print #fo, "  - tassi di valutazione e di rendimento finanziario............. (tv,ti) : "; ValFmt(appl.parm(P_cvTv) * 100, 9) & "% "; " "; ValFmt(appl.parm(P_cvTi) * 100, 9) & "% "
      Print #fo, "  - tasso di inflazione e incr. redditi iniziali nuovi iscritti (tev,trn) : "; ValFmt(appl.parm(P_cvTev) * 100, 9) & "% "; " "; ValFmt(appl.parm(P_cvTrn) * 100, 9) & "% "
      '--- 2.2 Tassi di rivalutazione dei montanti contributivi ---
      Print #fo, "  2.2 TASSI DI RIVALUTAZIONE DEI MONTANTI CONTRIBUTIVI"
      '--- 20161111LC (inizio)  'ex 20150612LC-2
      Print #fo, "  - rapporto cpontributi/pensioni................................. (trmc0) : "; ValFmt(appl.parm(P_cvTrmc0) * 100, 9) & "% "  '20161115LC (scommentato)
      'Print #fo, "  - contributi quota B........................................... (trmc1) : "; ValFmt(appl.parm(P_cvTrmc1) * 100, 9) & "% "
      'Print #fo, "  - contributi volontari......................................... (trmc2) : "; ValFmt(appl.parm(P_cvTrmc2) * 100, 9) & "% "
      'Print #fo, "  - contributi per supplementi di pensione....................... (trmc3) : "; ValFmt(appl.parm(P_cvTrmc3) * 100, 9) & "% "
      'Print #fo, "  - contributi quota C........................................... (trmc4) : "; ValFmt(appl.parm(P_cvTrmc4) * 100, 9) & "% "
      Print #fo, "  - riv. contributi obbligatori.................................. (trmc4) : "; ValFmt(appl.parm(P_cvTrmc4) * 100, 9) & "% "
      Print #fo, "  - riv. contributi con PIL quinquennale......................... (trmc5) : "; ValFmt(appl.parm(P_cvTrmc5) * 100, 9) & "% "
      Print #fo, "  - riv. contributi FIRR......................................... (trmc6) : "; ValFmt(appl.parm(P_cvTrmc6) * 100, 9) & "% "
      '--- 20161111LC (fine)  'ex 20150612LC-2
      '--- 2.3 Tassi di rivalutazione di redditi e volumi d'affari ---
      Print #fo, "  2.3 TASSI DI RIVALUTAZIONE DELLE PROVVIGIONI"
      Print #fo, "  - incr.provvigioni attivi....................................... (ts1a) : "; ValFmt(appl.parm(P_cvTs1a) * 100, 9) & "% "
      Print #fo, "  - incr.provvigioni pensionati attivi............................ (ts1p) : "; ValFmt(appl.parm(P_cvTs1p) * 100, 9) & "% "
      Print #fo, "  - incr.provvigioni nuovi iscritti............................... (ts1n) : "; ValFmt(appl.parm(P_cvTs1n) * 100, 9) & "% "
      '--- 2.4 Rivalutazione delle pensioni ---
      Print #fo, "  2.4 MASSIMALI E TASSI DI RIVALUTAZIONE DELLE PENSIONI"
      Print #fo, "  - 1� scaglione.......................................... (tp1max,tp1al) : "; ValFmt(CDbl(appl.parm(P_cvTp1Max)), 10) & " " & ValFmt(appl.parm(P_cvTp1Al) * 100, 9) & "% "
      Print #fo, "  - 2� scaglione.......................................... (tp2max,tp2al) : "; ValFmt(CDbl(appl.parm(P_cvTp2Max)), 10) & " " & ValFmt(appl.parm(P_cvTp2Al) * 100, 9) & "% "
      Print #fo, "  - 3� scaglione.......................................... (tp3max,tp3al) : "; ValFmt(CDbl(appl.parm(P_cvTp3Max)), 10) & " " & ValFmt(appl.parm(P_cvTp3Al) * 100, 9) & "% "
      '--- 20161112LC (inizio)
      Print #fo, "  - 1� scaglione.......................................... (tp4max,tp4al) : "; ValFmt(CDbl(appl.parm(P_cvTp4Max)), 10) & " " & ValFmt(appl.parm(P_cvTp4Al) * 100, 9) & "% "
      Print #fo, "  - 2� scaglione.......................................... (tp5max,tp5al) : "; ValFmt(CDbl(appl.parm(P_cvTp5Max)), 10) & " " & ValFmt(appl.parm(P_cvTp4Al) * 100, 9) & "% "
      '--- 20161112LC (fine)
      '--- 2.5 Altre stime ----
      Print #fo, "  2.5 ALTRE STIME"
      Print #fo, "  - % pens. di vecchiaia ord. che proseguono nell'attivit� (prcPVatt m/f) : "; ValFmt(appl.parm(P_cvPrcPVattM) * 100, 9) & "% " & ValFmt(appl.parm(P_cvPrcPVattF) * 100, 9) & "%"
      Print #fo, "  - % pens. di vecchiaia ant. che proseguono nell'attivit� (prcPAatt m/f) : "; ValFmt(appl.parm(P_cvPrcPAattM) * 100, 9) & "% " & ValFmt(appl.parm(P_cvPrcPAattF) * 100, 9) & "%"
      Print #fo, "  - % pens. contributivi che proseguono nell'attivit�..... (prcPCatt m/f) : "; ValFmt(appl.parm(P_cvPrcPCattM) * 100, 9) & "% " & ValFmt(appl.parm(P_cvPrcPCattF) * 100, 9) & "%"
      '***********************
      'Frame BASI_TECNICHE (3)
      '***********************
      Print #fo,
      Print #fo, "  3. BASI TECNICHE"
      Print #fo, "  ================"
      '--- 3.1 Probabilit� di morte ----
      Print #fo, "  3.1 PROBABILITA' DI MORTE"
      Print #fo, "  - attivi e volontari............................................. (lad) : "; fs(Trim(appl.parm(P_LAD)), 10, 0)
      Print #fo, "  - silenti........................................................ (led) : "; fs(Trim(appl.parm(P_LED)), 10, 0)
      Print #fo, "  - pensionati diretti............................................. (lpd) : "; fs(Trim(appl.parm(P_LPD)), 10, 0)
      Print #fo, "  - pensionati superstiti ed indiretti............................. (lsd) : "; fs(Trim(appl.parm(P_LSD)), 10, 0)
      Print #fo, "  - pensionati di inabilit�........................................ (lbd) : "; fs(Trim(appl.parm(P_LBD)), 10, 0)
      Print #fo, "  - pensionati di invalidit�....................................... (lid) : "; fs(Trim(appl.parm(P_LID)), 10, 0)
      '--- 3.2 Altre probabilit� ----
      Print #fo, "  3.2 ALTRE PROBABILITA'"
      Print #fo, "  - passaggio da attivi a volontari.............................. (lav) : "; fs(Trim(appl.parm(P_LAV)), 10, 0)
      Print #fo, "  - passaggio da attivi a silenti................................ (lae) : "; fs(Trim(appl.parm(P_LAE)), 10, 0)
      Print #fo, "  - passaggio da attivi a inabili.................................. (lab) : "; fs(Trim(appl.parm(P_LAB)), 10, 0)
      Print #fo, "  - passaggio da attivi a invalidi................................. (lai) : "; fs(Trim(appl.parm(P_LAI)), 10, 0)
      Print #fo, "  - passaggio da attivi a elimin. per altre cause, senza pensione.. (law) : "; fs(Trim(appl.parm(P_LAW)), 10, 0)
      '--- 3.3 Linee reddituali ----
      Print #fo, "  3.3 LINEE PROVVIGIONALI"
      Print #fo, "  - attivi ........................................................ (lra) : "; fs(Trim(appl.parm(P_LRA)), 10, 0)
      Print #fo, "  - pensionati attivi ............................................. (lrp) : "; fs(Trim(appl.parm(P_LRP)), 10, 0)
      Print #fo, "  - nuovi iscritti ................................................ (lrn) : "; fs(Trim(appl.parm(P_LRN)), 10, 0)
      '--- 3.4 Coefficienti di conversione del montante in rendita ----
      Print #fo, "  3.4 CONVERSIONE DEI MONTANTI IN RENDITA"
      Print #fo, "  - tabella dei coefficienti di conversione................... (spTavCCR) : "; fs(CStr(appl.parm(P_CCR)), 10, 1)
      '--- 3.5 Superstiti ----
      Print #fo, "  3.5 SUPERSTITI"
      Print #fo, "  - probabilit� e composizione familiari .......................... (fam) : "; fs(Trim(appl.parm(P_PF)), 10, 0)
      Print #fo, "  - et� massima dei figli superstiti.............................. (zmax) : "; appl.parm(P_ZMAX)
      '******************************
      'Frame REQUISITI DI ACCESSO (4)
      '******************************
      Print #fo,
      Print #fo, "  4. REQUISITI DI ACCESSO ALLE PRESTAZIONI"
      Print #fo, "  ========================================"
      '--- 4.1 Pensione di vecchiaia ordinaria ----
      Print #fo, "  4.1 PENSIONE DI VECCHIAIA ORDINARIA"
      Print #fo, "  - anzianit� minima per pens. di vecchiaia ordinaria....... (pVeo30 m/f) : "; fs(CStr(appl.parm(P_cvPveo30M)), 10, 1); " "; fs(CStr(appl.parm(P_cvPveo30F)), 10, 1)
      Print #fo, "  - et� minima per pensione di vecchiaia ordinaria.......... (pVeo65 m/f) : "; fs(CStr(appl.parm(P_cvPveo65M)), 10, 1); " "; fs(CStr(appl.parm(P_cvPveo65F)), 10, 1)
      Print #fo, "  - et� per pensione di vecchiaia posticipata............... (pVeo98 m/f) : "; fs(CStr(appl.parm(P_cvPveo98M)), 10, 1); " "; fs(CStr(appl.parm(P_cvPveo98F)), 10, 1)
      '--- 4.2 Pensione di vecchiaia anticipata ----
      Print #fo, "  4.2 PENSIONE DI VECCHIAIA ANTICIPATA"
      Print #fo, "  - anzianit� minima per pensione di vecchiaia anticipata... (pVea35 m/f) : "; fs(CStr(appl.parm(P_cvPvea35M)), 10, 1); " "; fs(CStr(appl.parm(P_cvPvea35F)), 10, 1)
      Print #fo, "  - et� minima per pensione di vecchiaia anticipata......... (pVea58 m/f) : "; fs(CStr(appl.parm(P_cvPvea58M)), 10, 1); " "; fs(CStr(appl.parm(P_cvPvea58F)), 10, 1)
      '--- 4.3 Pensione di vecchiaia posticipata ----
      Print #fo, "  4.3 PENSIONE DI VECCHIAIA POSTICIPATA"
      Print #fo, "  - et� per pensione posticipata............................ (pVep70 m/f) : "; fs(CStr(appl.parm(P_cvPvep70M)), 10, 1); " "; fs(CStr(appl.parm(P_cvPvep70F)), 10, 1)
      '--- 4.4 Pensione di inabilit� ----
      Print #fo, "  4.4 PENSIONE DI INABILITA'"
      Print #fo, "  - anzianit� minima per pensione di inabilit�.............. (pInab2 m/f) : "; fs(CStr(appl.parm(P_cvPinab2M)), 10, 1); " "; fs(CStr(appl.parm(P_cvPinab2F)), 10, 1)
      Print #fo, "  - anzianit� riconosciuta per pensione di inabilit�....... (pInab10 m/f) : "; fs(CStr(appl.parm(P_cvPinab10M)), 10, 1); " "; fs(CStr(appl.parm(P_cvPinab10F)), 10, 1)
      Print #fo, "  - anzianit� massima riconosciuta per pens. di inabilit�.. (pInab35 m/f) : "; fs(CStr(appl.parm(P_cvPinab35M)), 10, 1); " "; fs(CStr(appl.parm(P_cvPinab35F)), 10, 1)
      '--- 4.5 Pensione di invalidit� ----
      Print #fo, "  4.5 PENSIONE DI INVALIDITA'"
      Print #fo, "  - anzianit� minima per pensione di invalidit�.............. (pInv2 m/f) : "; fs(CStr(appl.parm(P_cvPinv5M)), 10, 1); " "; fs(CStr(appl.parm(P_cvPinv5F)), 10, 1)
      Print #fo, "  - quota della pens. di inabilit� per pensione di invalidit�... (pInv70) : "; ValFmt(appl.parm(P_cvPinv70) * 100, 9); "% "
      '--- 4.6 Pensione indiretta ----
      Print #fo, "  4.6 PENSIONE AI SUPERSTITI"
      Print #fo, "  - anzianit� minima per pensione ai superstiti.............. (pInd2 m/f) : "; fs(CStr(appl.parm(P_cvPind2M)), 10, 1); " "; fs(CStr(appl.parm(P_cvPind2F)), 10, 1)
      Print #fo, "  - anz. minima per il calcolo dalla pensione ai superstiti. (pInd20 m/f) : "; fs(CStr(appl.parm(P_cvPind20M)), 10, 1); " "; fs(CStr(appl.parm(P_cvPind20M)), 10, 1)
      Print #fo, "  - anz. massima per il calcolo dalla pensione ai superstiti (pInd30 m/f) : "; fs(CStr(appl.parm(P_cvPind30M)), 10, 1); " "; fs(CStr(appl.parm(P_cvPind30M)), 10, 1)
      '--- 4.7 Pensione contributiva ----
      Print #fo, "  4.7 RENDITA CONTRIBUTIVA"
      Print #fo, "  - anzianit� minima per la rendita contributiva............ (rcHmin m/f) : "; fs(CStr(appl.parm(P_cvRcHminM)), 10, 1); " "; fs(CStr(appl.parm(P_cvRcHminF)), 10, 1)
      Print #fo, "  - et� minima per la restituzione dei contributi........... (rcXmin m/f) : "; fs(CStr(appl.parm(P_cvRcXminM)), 10, 1); " "; fs(CStr(appl.parm(P_cvRcXminF)), 10, 1)
      Print #fo, "  - % di restituzione dei contributi.......................... (rcPrcCon) : "; ValFmt(appl.parm(P_cvRcPrcCon) * 100, 9); "% "
      'Print #fo, "  - % del tasso di rivalutazione per la restit. dei contributi (rcPrcRiv) : "; ValFmt(appl.parm(P_cvRcPrcRiv) * 100, 9); "% "
      '--- 4.8 Totalizzazione ----
      '***********************
      'Frame CONTRIBUZIONE (5)
      '***********************
      Print #fo,
      Print #fo, "  5. CONTRIBUZIONE"
      Print #fo, "  ================"
      '--- 5.1 Contribuzione ridotta ----
      '--- 5.2 Soggettivo ----
      Print #fo, "  5.2 SOGGETTIVO"
      Print #fo, "  - contributo soggettivo minimo.......................... (Sogg0Min m/p) : "; ValFmt(appl.CoeffVar(anno0, ecv.cv_Sogg0Min) + 0, 10); " "; ValFmt(appl.CoeffVar(anno0, ecv.cv_Sogg0MinR) + 0, 10)
      Print #fo, "  - aliquota 1� scaglione contributivo..................... (Sogg1Al m/p) : "; ValFmt(appl.CoeffVar(anno0, ecv.cv_Sogg1Al) * 100, 9); "% "; ValFmt(appl.CoeffVar(anno0, ecv.cv_Sogg1AlR) * 100, 9); "%"
      Print #fo, "  - massimale 1� scaglione contributivo................... (Sogg1Max m/p) : "; ValFmt(appl.CoeffVar(anno0, ecv.cv_Sogg1Max) + 0, 10); " "; ValFmt(appl.CoeffVar(anno0, ecv.cv_Sogg1MaxR) + 0, 10)
      '--- 5.3 Integrativo / FIRR ----
      Print #fo, "  5.3 FIRR"
      Print #fo, "  - contributo FIRR minimo................................. (Int0Min m/p) : "; ValFmt(appl.CoeffVar(anno0, ecv.cv_Int0Min) + 0, 10); " "; ValFmt(appl.CoeffVar(anno0, ecv.cv_Int0MinR) + 0, 10)
      Print #fo, "  - aliquota 1� scaglione FIRR............................. (Int1Al m/p) : "; ValFmt(appl.CoeffVar(anno0, ecv.cv_Int1Al) * 100, 9); "% "; ValFmt(appl.CoeffVar(anno0, ecv.cv_Int1AlR) * 100, 9); "%"
      Print #fo, "  - massimale 1� scaglione FIRR............................ (Int1Max m/p) : "; ValFmt(appl.CoeffVar(anno0, ecv.cv_Int1Max) + 0, 10); " "; ValFmt(appl.CoeffVar(anno0, ecv.cv_Int1MaxR) + 0, 10)
      Print #fo, "  - aliquota 2� scaglione FIRR............................. (Int2Al m/p) : "; ValFmt(appl.CoeffVar(anno0, ecv.cv_Int1Al) * 100, 9); "% "; ValFmt(appl.CoeffVar(anno0, ecv.cv_Int2AlR) * 100, 9); "%"
      Print #fo, "  - massimale 2� scaglione FIRR............................ (Int2Max m/p) : "; ValFmt(appl.CoeffVar(anno0, ecv.cv_Int2Max) + 0, 10); " "; ValFmt(appl.CoeffVar(anno0, ecv.cv_Int2MaxR) + 0, 10)
      Print #fo, "  - aliquota 3� scaglione FIRR............................. (Int3Al m/p) : "; ValFmt(appl.CoeffVar(anno0, ecv.cv_Int3Al) * 100, 9); "% "; ValFmt(appl.CoeffVar(anno0, ecv.cv_Int3AlR) * 100, 9); "%"
      '--- 5.4 Assistenziale / Solidariet� ----
      Print #fo, "  5.4 SOLIDARIETA'"
      Print #fo, "  - contributo assistenziale minimo........................ (Ass0Min i/r) : "; ValFmt(appl.CoeffVar(anno0, ecv.cv_Ass0Min) + 0, 10); " "; ValFmt(appl.CoeffVar(anno0, ecv.cv_Ass0MinR) + 0, 10)
      Print #fo, "  - aliquota contributo assistenziale....................... (Ass1Al i/r) : "; ValFmt(appl.CoeffVar(anno0, ecv.cv_Ass1Al) * 100, 9); "% "; ValFmt(appl.CoeffVar(anno0, ecv.cv_Ass1AlR) * 100, 9); "%"
      '--- 5.5 Altro ----
      Print #fo, "  5.5 ALTRO"
      Print #fo, "  - contributo di maternit�................................... (Mat0 i/r) : "; ValFmt(appl.CoeffVar(anno0, ecv.cv_Mat0MinR) + 0, 10); " "; ValFmt(appl.CoeffVar(anno0, ecv.cv_Mat0MinR) + 0, 10)
      '****************************
      'Frame MISURA PRESTAZIONI (6)
      '****************************
      Print #fo,
      Print #fo, "  6. MISURA DELLE PRESTAZIONI"
      Print #fo, "  ==========================="
      '--- 6.1 Scaglioni IRPEF ----
      '--- 6.2 Pensione ----
      '--- 6.3 Supplementi di pensione ----
      Print #fo, "  6.3 SUPPLEMENTI DI PENSIONE"
      Print #fo, "  - anni necessari per ciascun supplemento...................... (spAnni) : "; fs(appl.parm(P_cvSpAnni) + 0, 10, 1)
      Print #fo, "  - et� massima per l'ultimo supplemento...................... (spEtaMax) : "; fs(appl.parm(P_cvSpEtaMax) + 0, 10, 1)
      Print #fo, "  - % dei contributi utile per il calcolo del supplemento......(spPrcCon) : "; fs(appl.parm(P_cvSpPrcCon) * 100, 9, 1); "%"
      'Print #fo, "  - % del tasso di rival. dei contr. per il calcolo del suppl. (spPrcRiv) : "; fs(appl.parm(P_cvSpPrcRiv) * 100, 9, 1); "%"
      '--- 6.4 Reversibilit� superstiti ----
      Print #fo, "  6.4 ALIQUOTE DI REVERSIBILITA' PER I SUPERSTITI"
      Print #fo, "  - vedovi soli..................................................... (Av) : "; fs(appl.parm(P_cvAv) * 100, 9, 1); "%"
      Print #fo, "  - vedovi soli e 1 orfano......................................... (Avo) : "; fs(appl.parm(P_cvAvo) * 100, 9, 1); "%"
      Print #fo, "  - vedovi soli e 2 orfani........................................ (Avoo) : "; fs(appl.parm(P_cvAvoo) * 100, 9, 1); "%"
      Print #fo, "  - orfano solo..................................................... (Ao) : "; fs(appl.parm(P_cvAo) * 100, 9, 1); "%"
      Print #fo, "  - 2 orfani....................................................... (Aoo) : "; fs(appl.parm(P_cvAoo) * 100, 9, 1); "%"
      Print #fo, "  - 3 orfani...................................................... (Aooo) : "; fs(appl.parm(P_cvAooo) * 100, 9, 1); "%"
      '************************
      'Frame NUOVI ISCRITTI (7)
      '************************
      Print #fo,
      Print #fo, "  7. NUOVI ISCRITTI"
      Print #fo, "  ================="
      i = appl.parm(P_niTipo)
      Select Case i
      Case 0: a$ = "Non considerati"
      Case 1: a$ = "Letti dalla tabella degli iscritti"
      Case 2: a$ = "Generati per rimpiazzare gli attivi eliminati annualmente"
      Case 3: a$ = "Generati per rimpiazzare attivi e pensionati attivi eliminati annualmente"  '20140522LC
      '20120319LC (inizio)
      End Select
      Print #fo, "  - Tipo di elaborazione......... : "; a$
      If i >= 2 Then
        Print #fo, "  - Nuovi iscritti puntuali...... : "; fs(CStr(appl.parm(P_niP)), 10, 1)
        '--- 20150527LC (inizio)
        Print #fo, "  - Base statistica nuovi iscritti attivi : "; fs(CStr(appl.parm(P_niStat)), 10, 1)
        Print #fo, "  - Base statistica silenti riattivati... : "; fs(CStr(appl.parm(P_neStat)), 10, 1)
        Print #fo, "  - Base statistica volontari riattivati. : "; fs(CStr(appl.parm(P_nvStat)), 10, 1)
        '--- 20150527LC (inizio)
      End If
      '20120319LC (fine)
    End If
  End If
  '********
  'File XLS
  '********
  If iop Mod 2 = 0 Then
    If glo.rep_chk(REP1_PRM) = vbChecked And Not xlsWsh Is Nothing Then
      With xlsWsh
        '*************
        'DATI GENERALI
        '*************
        iRiga = 7: iCol = 4
        .cells(iRiga, iCol) = appl.idReport & " " & glo.sVer: iRiga = iRiga + 1  '20130307LC  (glo.MdiAfp.Tag)
        .cells(iRiga, iCol) = glo.szGruppo: iRiga = iRiga + 1
        .cells(iRiga, iCol) = Format(Now, "dd/mm/yyyy hh:nn:ss"): iRiga = iRiga + 1: iCol = 2
        '--- 20121208LC (inizio)
        If appl.matr2 <> appl.matr1 Then
          a$ = "Matricole esaminate (da " & appl.matr1 & " a " & appl.matr2 & ")" & " (riattivazioni = " & appl.nDipGenBk & ")" '20160411LC
          .cells(iRiga, iCol) = a: iCol = iCol + 1
          .cells(iRiga, iCol) = "(nmat)": iCol = iCol + 1
          .cells(iRiga, iCol) = appl.nMatr: iCol = iCol + 1
        Else
          a$ = "Matricola esaminata (et�=" & appl.xa & ", anz.t�=" & Format(appl.h, "#0.00") & ")" & " (riattivazioni = " & appl.nDipGenBk & ")" '20160411LC
          .cells(iRiga, iCol) = a: iCol = iCol + 1
          .cells(iRiga, iCol) = "(mat)": iCol = iCol + 1
          .cells(iRiga, iCol) = appl.matr1: iCol = iCol + 1
        End If
        '--- 20121208LC (fine)
        '*******************
        'Frame PARAMETRI (1)
        '*******************
        '--- 1.1 Bilancio tecnico ----
        iRiga = iRiga + 2: iCol = 4
        .cells(iRiga, iCol) = CDate(Format(appl.DataBilancio, "dd/mm/yyyy")): iRiga = iRiga + 1
        .cells(iRiga, iCol) = appl.parm(P_btAnniEl1): iRiga = iRiga + 1
        .cells(iRiga, iCol) = appl.parm(P_btPatrim): iRiga = iRiga + 1
        .cells(iRiga, iCol) = appl.parm(P_btSpAmm): iRiga = iRiga + 1
        .cells(iRiga, iCol) = appl.parm(P_btSpAss): iRiga = iRiga + 1
        .cells(iRiga, iCol) = appl.parm(P_btPassSIV): iRiga = iRiga + 1
        .cells(iRiga, iCol) = appl.parm(P_btPassSIP): iRiga = iRiga + 1
        'iRiga = iRiga + 2
        '--- 1.2 Tipo di calcolo ----
        If appl.parm(P_TipoElab) = 0 Then
          '20120329LC
          '.Cells(iRiga, iCol) = "metodo retributivo": iRiga = iRiga + 1
          a$ = "metodo retributivo"
        ElseIf appl.parm(P_TipoElab) = 1 Then
          a$ = "metodo contributivo"
        Else
          a$ = "metodo misto, contributivo dal " & appl.parm(P_TipoElab)
        End If
        iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = a$: iRiga = iRiga + 1
        '--- 1.3 Modalit� di analisi ---
        iRiga = iRiga + 1: iCol = 4
        '--- 20161111LC (inizio)
        .cells(iRiga, iCol) = appl.iterNum: iRiga = iRiga + 1
        .cells(iRiga, iCol) = appl.iterCvNome: iRiga = iRiga + 1
        '--- 20161111LC (fine)
        .cells(iRiga, iCol) = appl.parm(P_SQL): iRiga = iRiga + 1
        .cells(iRiga, iCol) = appl.parm(P_FOUT): iRiga = iRiga + 1
        '******************************
        'Frame TASSI ED ALTRE STIME (2)
        '******************************
        '--- 2.1 Tassi di valutazione e rivalutazione ---
        iRiga = iRiga + 3: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_cvTv): iCol = iCol + 1
        .cells(iRiga, iCol) = appl.parm(P_cvTi): iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_cvTev): iCol = iCol + 1
        .cells(iRiga, iCol) = appl.parm(P_cvTrn): iRiga = iRiga + 1: iCol = 4
        '--- 2.2 Tassi di rivalutazione dei montanti contributivi ---
        iRiga = iRiga + 1: iCol = 4
        '--- 20150612LC-2 (inizio)
        .cells(iRiga, iCol) = appl.parm(P_cvTrmc0): iRiga = iRiga + 1: iCol = 4  '20161115LC (scommentato)
        '.Cells(iRiga, iCol) = appl.parm(P_cvTrmc1): iRiga = iRiga + 1: iCol = 4
        '.Cells(iRiga, iCol) = appl.parm(P_cvTrmc2): iRiga = iRiga + 1: iCol = 4
        '.Cells(iRiga, iCol) = appl.parm(P_cvTrmc3): iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_cvTrmc4): iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_cvTrmc5): iRiga = iRiga + 1: iCol = 4  '20161111LC (scommentato)
        .cells(iRiga, iCol) = appl.parm(P_cvTrmc6): iRiga = iRiga + 1: iCol = 4
        '--- 20150612LC-2 (fine)
        '--- 2.3 Tassi di rivalutazione di redditi e volumi d'affari ---
        iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_cvTs1a): iCol = iCol + 1
        .cells(iRiga, iCol) = "": iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_cvTs1p): iCol = iCol + 1
        .cells(iRiga, iCol) = "": iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_cvTs1n): iCol = iCol + 1
        .cells(iRiga, iCol) = "": iRiga = iRiga + 1: iCol = 4
        '--- 2.4 Rivalutazione delle pensioni ---
        iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_cvTp1Max): iCol = iCol + 1
        .cells(iRiga, iCol) = appl.parm(P_cvTp1Al): iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_cvTp2Max): iCol = iCol + 1
        .cells(iRiga, iCol) = appl.parm(P_cvTp2Al): iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_cvTp3Max): iCol = iCol + 1
        .cells(iRiga, iCol) = appl.parm(P_cvTp3Al): iRiga = iRiga + 1: iCol = 4
        '--- 20161112LC (inizio)
        .cells(iRiga, iCol) = appl.parm(P_cvTp4Max): iCol = iCol + 1
        .cells(iRiga, iCol) = appl.parm(P_cvTp4Al): iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_cvTp5Max): iCol = iCol + 1
        .cells(iRiga, iCol) = appl.parm(P_cvTp5Al): iRiga = iRiga + 1: iCol = 4
        '--- 20161112LC (fine)
        '--- 2.5 Altre stime ----
        iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_cvPrcPVattM): iCol = iCol + 1
        .cells(iRiga, iCol) = appl.parm(P_cvPrcPVattF): iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_cvPrcPAattM): iCol = iCol + 1
        .cells(iRiga, iCol) = appl.parm(P_cvPrcPAattF): iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_cvPrcPCattM): iCol = iCol + 1
        .cells(iRiga, iCol) = appl.parm(P_cvPrcPCattF): iRiga = iRiga + 1: iCol = 4
        'iRiga = iRiga + 1
        '***********************
        'Frame BASI_TECNICHE (3)
        '***********************
        iRiga = iRiga + 3
        '--- 3.1 Probabilit� di morte ----
        iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_LAD): iRiga = iRiga + 1
        .cells(iRiga, iCol) = appl.parm(P_LED): iRiga = iRiga + 1
        .cells(iRiga, iCol) = appl.parm(P_LPD): iRiga = iRiga + 1
        .cells(iRiga, iCol) = appl.parm(P_LSD): iRiga = iRiga + 1
        .cells(iRiga, iCol) = appl.parm(P_LBD): iRiga = iRiga + 1
        .cells(iRiga, iCol) = appl.parm(P_LID): iRiga = iRiga + 1
        '--- 3.2 Altre probabilit� ----
        iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_LAV): iRiga = iRiga + 1
        .cells(iRiga, iCol) = appl.parm(P_LAE): iRiga = iRiga + 1
        .cells(iRiga, iCol) = appl.parm(P_LAB): iRiga = iRiga + 1
        .cells(iRiga, iCol) = appl.parm(P_LAI): iRiga = iRiga + 1
        .cells(iRiga, iCol) = appl.parm(P_LAW): iRiga = iRiga + 1
        '--- 20160306LC (inizio)  'ex "(preprocessing)"
        .cells(iRiga, iCol) = appl.parm(P_neStat): iRiga = iRiga + 1  'P_LVA
        .cells(iRiga, iCol) = appl.parm(P_nvStat): iRiga = iRiga + 1  'P_LEA
        '--- 20160306LC (fine)
        '--- 3.3 Linee reddituali ----
        iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_LRA): iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_LRP): iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_LRN): iRiga = iRiga + 1: iCol = 4
        '--- 3.4 Coefficienti di conversione del montante in rendita ----
        iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_CCR): iRiga = iRiga + 1
        '--- 3.5 Superstiti ----
        iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_PF): iRiga = iRiga + 1
        .cells(iRiga, iCol) = appl.parm(P_ZMAX): iRiga = iRiga + 1
        '******************************
        'Frame REQUISITI DI ACCESSO (4)
        '******************************
        iRiga = iRiga + 3
        '--- 4.1 Pensione di vecchiaia ----
        iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_cvPveo30M): iCol = iCol + 1
        .cells(iRiga, iCol) = appl.parm(P_cvPveo30F): iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_cvPveo65M): iCol = iCol + 1
        .cells(iRiga, iCol) = appl.parm(P_cvPveo65F): iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_cvPveo98M): iCol = iCol + 1
        .cells(iRiga, iCol) = appl.parm(P_cvPveo98F): iRiga = iRiga + 1: iCol = 4
        '--- 4.2 Pensione di vecchiaia anticipata ----
        iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_cvPvea35M): iCol = iCol + 1
        .cells(iRiga, iCol) = appl.parm(P_cvPvea35F): iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_cvPvea58M): iCol = iCol + 1
        .cells(iRiga, iCol) = appl.parm(P_cvPvea58F): iRiga = iRiga + 1: iCol = 4
        '--- 4.3 Pensione di vecchiaia posticipata ----
        iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_cvPvep70M): iCol = iCol + 1
        .cells(iRiga, iCol) = appl.parm(P_cvPvep70F): iRiga = iRiga + 1: iCol = 4
        '--- 4.4 Pensione di inabilit� ----
        iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_cvPinab2M): iCol = iCol + 1
        .cells(iRiga, iCol) = appl.parm(P_cvPinab2F): iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_cvPinab10M): iCol = iCol + 1
        .cells(iRiga, iCol) = appl.parm(P_cvPinab10F): iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_cvPinab35M): iCol = iCol + 1
        .cells(iRiga, iCol) = appl.parm(P_cvPinab35F): iRiga = iRiga + 1: iCol = 4
        '--- 4.5 Pensione di invalidit� ----
        iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_cvPinv5M): iCol = iCol + 1
        .cells(iRiga, iCol) = appl.parm(P_cvPinv5F): iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_cvPinv70): iRiga = iRiga + 1: iCol = 4
        '--- 4.6 Pensione indiretta ----
        iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_cvPind2M): iCol = iCol + 1
        .cells(iRiga, iCol) = appl.parm(P_cvPind2F): iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_cvPind20M): iCol = iCol + 1
        .cells(iRiga, iCol) = appl.parm(P_cvPind20M): iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_cvPind30M): iCol = iCol + 1
        .cells(iRiga, iCol) = appl.parm(P_cvPind30M): iRiga = iRiga + 1: iCol = 4
        '--- 4.7 Pensione contributiva ----
        iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_cvRcHminM): iCol = iCol + 1
        .cells(iRiga, iCol) = appl.parm(P_cvRcHminF): iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_cvRcXminM): iCol = iCol + 1
        .cells(iRiga, iCol) = appl.parm(P_cvRcXminF): iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_cvRcPrcCon): iRiga = iRiga + 1
        '.Cells(iRiga, iCol) = appl.parm(P_cvRcPrcRiv): iRiga = iRiga + 1  '20150512LC (commentato)
        '--- 4.8 Totalizzazione ----
        '***********************
        'Frame CONTRIBUZIONE (5)
        '***********************
        iRiga = iRiga + 3
        '--- 5.1 Contribuzione ridotta ----
        '--- 5.2 Soggettivo ----
        iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.CoeffVar(anno0, ecv.cv_Sogg0Min): iCol = iCol + 1
        .cells(iRiga, iCol) = appl.CoeffVar(anno0, ecv.cv_Sogg0MinR): iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.CoeffVar(anno0, ecv.cv_Sogg1Al): iCol = iCol + 1
        .cells(iRiga, iCol) = appl.CoeffVar(anno0, ecv.cv_Sogg1AlR): iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.CoeffVar(anno0, ecv.cv_Sogg1Max): iCol = iCol + 1
        .cells(iRiga, iCol) = appl.CoeffVar(anno0, ecv.cv_Sogg1MaxR): iRiga = iRiga + 1: iCol = 4
        '--- 5.3 Integrativo / FIRR ----
        iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.CoeffVar(anno0, ecv.cv_Int0Min): iCol = iCol + 1
        .cells(iRiga, iCol) = appl.CoeffVar(anno0, ecv.cv_Int0MinR): iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.CoeffVar(anno0, ecv.cv_Int1Al) * 100 & "%": iCol = iCol + 1
        .cells(iRiga, iCol) = appl.CoeffVar(anno0, ecv.cv_Int1AlR) * 100 & "%": iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.CoeffVar(anno0, ecv.cv_Int1Max): iCol = iCol + 1
        .cells(iRiga, iCol) = appl.CoeffVar(anno0, ecv.cv_Int1MaxR): iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.CoeffVar(anno0, ecv.cv_Int2Al) * 100 & "%": iCol = iCol + 1
        .cells(iRiga, iCol) = appl.CoeffVar(anno0, ecv.cv_Int2AlR) * 100 & "%": iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.CoeffVar(anno0, ecv.cv_Int2Max): iCol = iCol + 1
        .cells(iRiga, iCol) = appl.CoeffVar(anno0, ecv.cv_Int2MaxR): iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.CoeffVar(anno0, ecv.cv_Int3Al) * 100 & "%": iCol = iCol + 1
        .cells(iRiga, iCol) = appl.CoeffVar(anno0, ecv.cv_Int3AlR) * 100 & "%": iRiga = iRiga + 1: iCol = 4
        '--- 5.4 Assistenziale / Solidariet� ----
        iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.CoeffVar(anno0, ecv.cv_Ass0Min): iCol = iCol + 1
        .cells(iRiga, iCol) = appl.CoeffVar(anno0, ecv.cv_Ass0MinR): iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.CoeffVar(anno0, ecv.cv_Ass1Al) * 100 & "%": iCol = iCol + 1
        .cells(iRiga, iCol) = appl.CoeffVar(anno0, ecv.cv_Ass1AlR) * 100 & "%": iRiga = iRiga + 1: iCol = 4
        '--- 5.4 Altro ----
        iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.CoeffVar(anno0, ecv.cv_Mat0Min): iCol = iCol + 1
        .cells(iRiga, iCol) = appl.CoeffVar(anno0, ecv.cv_Mat0MinR): iRiga = iRiga + 1: iCol = 4
        '****************************
        'Frame MISURA PRESTAZIONI (6)
        '****************************
        iRiga = iRiga + 3: iCol = 4
        '--- 6.1 Scaglioni IRPEF ----
        '--- 6.2 Pensione ----
        '--- 6.3 Supplementi di pensione ----
        iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_cvSpAnni): iRiga = iRiga + 1
        .cells(iRiga, iCol) = appl.parm(P_cvSpEtaMax): iRiga = iRiga + 1
        .cells(iRiga, iCol) = appl.parm(P_cvSpPrcCon) * 100 & "%": iRiga = iRiga + 1
        '.Cells(iRiga, iCol) = appl.parm(P_cvSpPrcRiv) * 100 & "%": iRiga = iRiga + 1
        '--- 6.4 Reversibilit� superstiti ----
        iRiga = iRiga + 1: iCol = 4
        .cells(iRiga, iCol) = appl.parm(P_cvAv) * 100 & "%": iRiga = iRiga + 1
        .cells(iRiga, iCol) = appl.parm(P_cvAvo) * 100 & "%": iRiga = iRiga + 1
        .cells(iRiga, iCol) = appl.parm(P_cvAvoo) * 100 & "%": iRiga = iRiga + 1
        .cells(iRiga, iCol) = appl.parm(P_cvAo) * 100 & "%": iRiga = iRiga + 1
        .cells(iRiga, iCol) = appl.parm(P_cvAoo) * 100 & "%": iRiga = iRiga + 1
        .cells(iRiga, iCol) = appl.parm(P_cvAooo) * 100 & "%": iRiga = iRiga + 1
        '************************
        'Frame NUOVI ISCRITTI (7)
        '************************
        iRiga = iRiga + 3: iCol = 4
        i = appl.parm(P_niTipo)
        Select Case i
        Case 0: a$ = "Non considerati"
        Case 1: a$ = "Letti dalla tabella degli iscritti"
        Case 2: a$ = "Generati per rimpiazzare gli attivi eliminati annualmente"
        Case 3: a$ = "Generati per rimpiazzare attivi e pensionati attivi eliminati annualmente"  '20140522LC
        '20120319LC (inizio)
        End Select
        .cells(iRiga, iCol) = a: iRiga = iRiga + 1
        If i >= 2 Or 1 = 1 Then  '20150604LC
          .cells(iRiga, iCol) = appl.parm(P_niP): iRiga = iRiga + 1: iCol = 4
          '--- 20150527LC (inizio)
          .cells(iRiga, iCol) = appl.parm(P_niStat): iRiga = iRiga + 1: iCol = 4
          .cells(iRiga, iCol) = appl.parm(P_neStat): iRiga = iRiga + 1: iCol = 4
          .cells(iRiga, iCol) = appl.parm(P_nvStat): iRiga = iRiga + 1: iCol = 4
          '--- 20150527LC (fine)
        End If
        '20120319LC (fine)
      End With
    End If
  End If
  glo.tStampa = glo.tStampa + Timer - tim1
End Sub


Private Sub TAppl_Stampa3c(ByRef appl As TAppl, ByRef glo As TGlobale, ByRef tb1() As Double, ByRef fo As Long, _
    ByRef xlsApp As Object, ByRef iop As Integer, ByRef iFmt As Byte, ByRef iTS As Long, ByRef sErr As String)
'*********************************************************************************************************************
'SCOPO
'  Stampa delle tabelle di output
'VERSIONI
'  20080614LC - iTS
'  20161112LC  gestioni errori XLS
'*********************************************************************************************************************
  Dim fi&, ijk0&, r&, tau&, tauMax& '20121028LC  'ex 20080612LC
  Dim tim1#, ZARR#
  Dim a$ '20080612LC
  '20080614LC (inizio)
  Dim buf_opz_ini#()
  Dim buf_coe_var#()
  Dim buf_pop#()
  Dim buf_sup#()
  Dim buf_ent1#()
  Dim buf_ent2#()
  Dim buf_mont#()
  Dim buf_usc1#()
  Dim buf_usc2#()
  Dim buf_bil1#()
  Dim buf_bil2#()
  Dim buf_val_cap#()
  '20080614LC (fine)
  Dim xlsWsh As Object '20080612LC
    
  '****************
  'Inizializzazione
  '****************
  sErr = ""
  tim1 = Timer
  tauMax = appl.nMax2  '20161112LC (ex GoSub TempoMax1)
  '***************
  'Crea le tabelle
  '***************
  '01) OPZ_INI
  'Call TAppl_Stampa3c_ElaboraDati(appl, tb(), REP1_OPZ_INI, nt, tauMax, buf_opz_ini(), iFmt, iTS, ZARR)
  '02) C_COE_VAR4
  Call TAppl_Stampa3c_ElaboraDati(appl, appl.tb1, REP1_COE_VAR, tauMax, buf_coe_var(), iFmt, iTS, ZARR, 0)
  '03) SVILUPPO COLLETTIVITA'
  Call TAppl_Stampa3c_ElaboraDati(appl, appl.tb1, REP1_POP, tauMax, buf_pop(), iFmt, iTS, ZARR, 0)
  '04) DECEDUTI CON SUPERSTITI E LORO RIPARTIZIONE
  Call TAppl_Stampa3c_ElaboraDati(appl, appl.tb1, REP1_SUP, tauMax, buf_sup(), iFmt, iTS, ZARR, 0)
  '05) EVOLUZIONE REDDITI IRPEF E VOLUME D'AFFARI IVA
  Call TAppl_Stampa3c_ElaboraDati(appl, appl.tb1, REP1_ENT1, tauMax, buf_ent1(), iFmt, iTS, ZARR, 0)
  '06) EVOLUZIONE CONTRIBUTI
  Call TAppl_Stampa3c_ElaboraDati(appl, appl.tb1, REP1_ENT2, tauMax, buf_ent2(), iFmt, iTS, ZARR, 0)
  '07) EVOLUZIONE MONTANTI
  Call TAppl_Stampa3c_ElaboraDati(appl, appl.tb1, REP1_MONT, tauMax, buf_mont(), iFmt, iTS, ZARR, 0)
  '08) EVOLUZIONE PENSIONI
  Call TAppl_Stampa3c_ElaboraDati(appl, appl.tb1, REP1_USC1, tauMax, buf_usc1(), iFmt, iTS, ZARR, 0)
  '09) EVOLUZIONE PENSIONATI NON CONTRIBUENTI
  Call TAppl_Stampa3c_ElaboraDati(appl, appl.tb1, REP1_USC2, tauMax, buf_usc2(), iFmt, iTS, ZARR, 0)
  '--- 20160419LC-2 (inizio)
  '10) BILANCIO TECNICO (ministeriale)
  Call TAppl_Stampa3c_ElaboraDati(appl, appl.tb1, REP1_BIL_TECN_MIN, tauMax, buf_bil2(), iFmt, iTS, ZARR, 0)
  '11) BILANCIO FIRR
  Call TAppl_Stampa3c_ElaboraDati(appl, appl.tb1, REP1_BIL_FIRR, tauMax, buf_bil1(), iFmt, iTS, ZARR, 0)
  '--- 20160419LC-2 (fine)
  '12) VALORI CAPITALI
  Call TAppl_Stampa3c_ElaboraDati(appl, appl.tb1, REP1_VAL_CAP, tauMax, buf_val_cap(), iFmt, iTS, ZARR, 0)
  '*****************
  'Stampa le tabelle
  '*****************
  '--- 20161112LC (inizio)
  If sErr <> "" Then GoTo ExitPoint
  '01) OPZ_INI
  sErr = TAppl_Stampa3c_StampaDati_A(appl, glo, REP1_OPZ_INI, tauMax, buf_opz_ini(), iop, fo, xlsApp, iFmt, iTS, ZARR)
  '02) C_COE_VAR4
  If sErr = "" Then sErr = TAppl_Stampa3c_StampaDati_A(appl, glo, REP1_COE_VAR, tauMax, buf_coe_var(), iop, fo, xlsApp, iFmt, iTS, ZARR)
  '03) SVILUPPO COLLETTIVITA'
  If sErr = "" Then sErr = TAppl_Stampa3c_StampaDati_B(appl, glo, REP1_POP, tauMax, buf_pop(), iop, fo, xlsApp, iFmt, iTS, ZARR)
  '04) DECEDUTI CON SUPERSTITI E LORO RIPARTIZIONE
  If sErr = "" Then sErr = TAppl_Stampa3c_StampaDati_B(appl, glo, REP1_SUP, tauMax, buf_sup(), iop, fo, xlsApp, iFmt, iTS, ZARR)
  '05) EVOLUZIONE REDDITI IRPEF E VOLUME D'AFFARI IVA
  If sErr = "" Then sErr = TAppl_Stampa3c_StampaDati_B(appl, glo, REP1_ENT1, tauMax, buf_ent1(), iop, fo, xlsApp, iFmt, iTS, ZARR)
  '06) EVOLUZIONE CONTRIBUTI
  If sErr = "" Then sErr = TAppl_Stampa3c_StampaDati_B(appl, glo, REP1_ENT2, tauMax, buf_ent2(), iop, fo, xlsApp, iFmt, iTS, ZARR)
  '07) EVOLUZIONE MONTANTI
  If sErr = "" Then sErr = TAppl_Stampa3c_StampaDati_B(appl, glo, REP1_MONT, tauMax, buf_mont(), iop, fo, xlsApp, iFmt, iTS, ZARR)
  '08) EVOLUZIONE PENSIONI
  If sErr = "" Then sErr = TAppl_Stampa3c_StampaDati_B(appl, glo, REP1_USC1, tauMax, buf_usc1(), iop, fo, xlsApp, iFmt, iTS, ZARR)
  '09) EVOLUZIONE PENSIONATI NON CONTRIBUENTI
  If sErr = "" Then sErr = TAppl_Stampa3c_StampaDati_B(appl, glo, REP1_USC2, tauMax, buf_usc2(), iop, fo, xlsApp, iFmt, iTS, ZARR)
  '--- 20160419LC-2 (inizio)
  '10) BILANCIO TECNICO (ministeriale)
  If sErr = "" Then sErr = TAppl_Stampa3c_StampaDati_B(appl, glo, REP1_BIL_TECN_MIN, tauMax, buf_bil2(), iop, fo, xlsApp, iFmt, iTS, ZARR)
  '11) BILANCIO FIRR
  If sErr = "" Then sErr = TAppl_Stampa3c_StampaDati_B(appl, glo, REP1_BIL_FIRR, tauMax, buf_bil1(), iop, fo, xlsApp, iFmt, iTS, ZARR)
  '--- 20160419LC-2 (fine)
  '12) VALORI CAPITALI
  If sErr = "" Then sErr = TAppl_Stampa3c_StampaDati_B(appl, glo, REP1_VAL_CAP, tauMax, buf_val_cap(), iop, fo, xlsApp, iFmt, iTS, ZARR)
  tim1 = Timer - tim1
  glo.tStampa = glo.tStampa + tim1
  '--- 20161112LC (fine)

ExitPoint:
  On Error GoTo 0
  '--- 20161112LC (inizio)
  'If sErr <> "" Then
  '  Call MsgBox("Si � verificato il seguente errore:" & vbCrLf & vbCrLf & sErr, vbExclamation)
  'End If
  Exit Sub
  '--- 20161112LC (fine)
  
ErrorHandler:
  sErr = "Errore " & Err.Number & " (" & Hex(Err.Number) & ") nel modulo " & Err.Source & vbCrLf & Err.Description  '20161112LC
  Err.Clear
  GoTo ExitPoint
End Sub


'20140312LC   'ex 201210228LC '20080614LC - iTS
Public Sub TAppl_Stampa3c_ElaboraDati(ByRef appl As TAppl, tb1#(), iTab&, tauMax&, buf#(), iFmt As Byte, iTS&, ByVal ZARR#, iQ&)
  Dim ib%  '20140414LC
  Dim ijk&, ijk0&, j&, tau&, tauMin&  '20121114LC
  Dim fraz#, ptot1#, ptot2#, y#, z#   '20131126LC
  Dim PTOT#, UTOT#, STOT#             '20140312LC
  Dim v2#(), v3#(), vv#()

  '--- 20140312LC (inizio)
  If iQ = 0 Then
    ZARR = IIf(iFmt <> 1, 1000, 1)
'ZARR = 1
  ElseIf iQ <> 0 Then
    ZARR = 1
  End If
  '--- 20140312LC (fine)
  Select Case iTab
  Case REP1_POP
    '**************************************
    'Crea la tabella SVILUPPO COLLETTIVITA'
    '**************************************
    '--- 20121114LC (inizio)
    tauMin = 1
    ReDim buf(tauMin To tauMax, 1 To 21)  '20150505LC  'ex 20131129LC
    For tau = tauMin To tauMax
    '--- 20121114LC (fine)
'  ANNO    ATTIVI   EX-ATT.  PENS.ATI    PENSIONATI NON CONTRIBUENTI                                                                       TOTALE  ELIMINATI SENZA DIRITTO                   TOTALE  "
'                            CONTRIB.    VECCH.  VECCH.A.  VECCH.P.   ANZIAN.  CONTRIB. TOTALIZZ.   INABIL.  INVALID.  SUPERST.    TOTALE  COLLETT.  USC.A.C.   CESSATI    S.SUP.  SUP.DEC.  GENERALE"
'   (T)      (Na)     (Nea)     (Npc)     (Npv)    (Npva)    (Npvp)     (Npa)     (Nps)     (Npt)     (Npb)     (Npi)     (Nps)    (Npnc)    (Ntot)     (Nw1)     (Nw2)     (Nw3)     (Nw4)   (NtotG)"
'    1        2        4         5         6        7         8          9         10        11        12*       13        14       15        16
      ijk0 = appl.nSize2 * iTS + appl.nSize3 * 0 + appl.nSize4 * (tau - 1 + appl.t0 - 1960) '20121114LC '14
      ijk = -1
      buf(tau, 1) = appl.t0 + tau
      buf(tau, 2) = tb1(ijk0 + TB_ZAA)
      '--- 2050505LC (inizio)  'ex 20131129LC
      buf(tau, 3) = tb1(ijk0 + TB_ZVA)
      buf(tau, 4) = tb1(ijk0 + TB_ZEA)
      buf(tau, 5) = tb1(ijk0 + TB_ZPC_VO) + _
                    tb1(ijk0 + TB_ZPC_VA) + _
                    tb1(ijk0 + TB_ZPC_VP) + _
                    tb1(ijk0 + TB_ZPC_A) + _
                    tb1(ijk0 + TB_ZPC_S) + _
                    tb1(ijk0 + TB_ZPC_T) '20120115LC
      buf(tau, 6) = tb1(ijk0 + TB_ZPN_VO)
      buf(tau, 7) = tb1(ijk0 + TB_ZPN_VA)
      buf(tau, 8) = tb1(ijk0 + TB_ZPN_VP)
      buf(tau, 9) = tb1(ijk0 + TB_ZPN_A)
      buf(tau, 10) = tb1(ijk0 + TB_ZPN_S)
      buf(tau, 11) = tb1(ijk0 + TB_ZPN_T)
      buf(tau, 12) = tb1(ijk0 + TB_ZPN_B)
      buf(tau, 13) = tb1(ijk0 + TB_ZPN_I)
      buf(tau, 14) = tb1(ijk0 + TB_ZPS_2)
      buf(tau, 15) = buf(tau, 6) + _
                     buf(tau, 7) + _
                     buf(tau, 8) + _
                     buf(tau, 9) + _
                     buf(tau, 10) + _
                     buf(tau, 11) + _
                     buf(tau, 12) + _
                     buf(tau, 13) + _
                     buf(tau, 14) '20150526LC
      buf(tau, 16) = buf(tau, 2) + _
                     buf(tau, 3) + _
                     buf(tau, 4) + _
                     buf(tau, 5) + _
                     buf(tau, 15)  '20150526LC  'ex 20140325LC  'ex 20131202LC  'ex 20120115LC
      buf(tau, 17) = tb1(ijk0 + TB_ZW_1)
      buf(tau, 18) = tb1(ijk0 + TB_ZW_2)
      buf(tau, 19) = tb1(ijk0 + TB_ZW_3)
      buf(tau, 20) = tb1(ijk0 + TB_ZD_S)
      buf(tau, 21) = tb1(ijk0 + TB_ZTOT)
      '--- 2050505LC (fine)
    Next tau
    If OPZ_SUDDIVISIONI_MR = 0 Then 'Ripartisce in parti uguali
      appl.SuddivRipart(iTS) = 1
    ElseIf OPZ_SUDDIVISIONI_MR = 1 Then 'Ripartisce secondo la numerosit�
      ijk = appl.nSize2 * iTS + appl.nSize3 * 0 + appl.nSize4 * (0 + appl.t0 - 1960) '20121114LC
      appl.SuddivRipart(iTS) = tb1(ijk + TB_ZTOT) '20111224LC 'ex 20080614LC
    End If
  Case REP1_SUP
    '***********************************************************
    'Crea la tabella DECEDUTI CON SUPERSTITI E LORO RIPARTIZIONE
    '***********************************************************
    '--- 20121114LC (inizio)
    tauMin = 1
    ReDim buf(tauMin To tauMax, 1 To 9) 'mod MDG
    For tau = tauMin To tauMax
    '--- 20121114LC (fine)
      ijk0 = appl.nSize2 * iTS + appl.nSize3 * 0 + appl.nSize4 * (tau - 1 + appl.t0 - 1960) '20121114LC '15
      ijk = -1
      buf(tau, 1) = appl.t0 + tau
      buf(tau, 2) = tb1(ijk0 + TB_ZPS_1)
      buf(tau, 3) = tb1(ijk0 + TB_ZPS_V)
      buf(tau, 4) = tb1(ijk0 + TB_ZPS_VO)
      buf(tau, 5) = tb1(ijk0 + TB_ZPS_VOO)
      buf(tau, 6) = tb1(ijk0 + TB_ZPS_O)
      buf(tau, 7) = tb1(ijk0 + TB_ZPS_OO)
      buf(tau, 8) = tb1(ijk0 + TB_ZPS_OOO)
      buf(tau, 9) = vecAliqMed(tau)
    Next tau
  Case REP1_ENT1
    '**************************************************************
    'Crea la tabella EVOLUZIONE REDDITI IRPEF E VOLUME D'AFFARI IVA
    '**************************************************************
    '--- 20121114LC (inizio)
    tauMin = IIf(OPZ_OUTPUT_MAX = 0, 1, 1961 - appl.t0)
    ReDim buf(tauMin To tauMax, 1 To 17)  '20140414LC
    For tau = tauMin To tauMax
    '--- 20121114LC (fine)
      ijk0 = -1
      ijk = appl.nSize2 * iTS + appl.nSize3 * 0 + appl.nSize4 * (tau + appl.t0 - 1960) '20121114LC  '16
      '--- 20140414LC (inizio)
      y = tb1(ijk + TB_AA_ZETOT) + tb1(ijk + TB_PA_ZETOT)  '+ tb1(ijk + TB_EA_ZETOT)
      buf(tau, 1) = appl.t0 + tau
      buf(tau, 2) = y
      buf(tau, 3) = (tb1(ijk + TB_AA_IRPEF) + tb1(ijk + TB_PA_IRPEF)) / ZARR
      buf(tau, 4) = ymed(tb1(ijk + TB_AA_IRPEF) + tb1(ijk + TB_PA_IRPEF), y) / ZARR
      buf(tau, 5) = (tb1(ijk + TB_AA_IVA) + tb1(ijk + TB_PA_IVA)) / ZARR         '+ tb1(ijk + TB_EA_IVA)
      buf(tau, 6) = ymed(tb1(ijk + TB_AA_IVA) + tb1(ijk + TB_PA_IVA), y) / ZARR  '+ tb1(ijk + TB_EA_IVA)
      buf(tau, 7) = (tb1(ijk + TB_AA_ETOT) + tb1(ijk + TB_PA_ETOT)) / ZARR       '+ tb1(ijk + TB_EA_ETOT)
      buf(tau, 8) = ymed(tb1(ijk + TB_AA_ETOT) + tb1(ijk + TB_PA_ETOT), y) / ZARR '+ tb1(ijk + TB_EA_ETOT)
      buf(tau, 9) = (tb1(ijk + TB_AA_IRPEF2) + tb1(ijk + TB_PA_IRPEF2)) / ZARR '20080611LC
      buf(tau, 10) = ymed(tb1(ijk + TB_AA_IRPEF2) + tb1(ijk + TB_PA_IRPEF2), y) / ZARR '20080611LC
      '---
      buf(tau, 11) = tb1(ijk + TB_AA_ZETOT)
      buf(tau, 12) = tb1(ijk + TB_AA_IRPEF) / ZARR
      buf(tau, 13) = ymed(tb1(ijk + TB_AA_IRPEF), y) / ZARR
      buf(tau, 14) = tb1(ijk + TB_AA_IVA) / ZARR
      buf(tau, 15) = ymed(tb1(ijk + TB_AA_IVA), y) / ZARR
      buf(tau, 16) = tb1(ijk + TB_AA_IRPEF2) / ZARR '20080611LC
      buf(tau, 17) = ymed(tb1(ijk + TB_AA_IRPEF2), y) / ZARR '20080611LC
      '--- 20140414LC (fine)
    Next tau
  Case REP1_ENT2
    '*************************************
    'Crea la tabella EVOLUZIONE CONTRIBUTI
    '*************************************
    '20121018LC (inizio)
    '--- 20121114LC (inizio)
    tauMin = IIf(OPZ_OUTPUT_MAX = 0, 1, 1961 - appl.t0)
    ReDim buf(tauMin To tauMax, 1 To 53) '20150512LC  'ex 20140414LC
    For tau = tauMin To tauMax
    '--- 20121114LC (fine)
      ijk0 = -1
      ijk = appl.nSize2 * iTS + appl.nSize3 * 0 + appl.nSize4 * (tau + appl.t0 - 1960) '20121114LC  '17
      buf(tau, 1) = appl.t0 + tau
      '---
      buf(tau, 2) = (tb1(ijk + TB_AA_ECSR) + tb1(ijk + TB_PA_ECSR)) / ZARR
      buf(tau, 3) = (tb1(ijk + TB_AA_ECS01) + tb1(ijk + TB_PA_ECS01)) / ZARR
      buf(tau, 4) = (tb1(ijk + TB_AA_ECS02) + tb1(ijk + TB_PA_ECS02)) / ZARR
      buf(tau, 5) = (tb1(ijk + TB_AA_ECS1) + tb1(ijk + TB_PA_ECS1)) / ZARR
      buf(tau, 6) = (tb1(ijk + TB_AA_ECS2) + tb1(ijk + TB_PA_ECS2)) / ZARR
      buf(tau, 7) = (tb1(ijk + TB_AA_ECS3) + tb1(ijk + TB_PA_ECS3)) / ZARR
      buf(tau, 8) = (tb1(ijk + TB_AA_ECSR) + tb1(ijk + TB_AA_ECS01) + tb1(ijk + TB_AA_ECS02) + _
                     tb1(ijk + TB_AA_ECS1) + tb1(ijk + TB_AA_ECS2) + tb1(ijk + TB_AA_ECS3) + _
                     tb1(ijk + TB_PA_ECSR) + tb1(ijk + TB_PA_ECS01) + tb1(ijk + TB_PA_ECS02) + _
                     tb1(ijk + TB_PA_ECS1) + tb1(ijk + TB_PA_ECS2) + tb1(ijk + TB_PA_ECS3)) / ZARR
      buf(tau, 9) = (tb1(ijk + TB_AA_ECIR) + tb1(ijk + TB_PA_ECIR)) / ZARR '+ tb1(ijk + TB_EA_ECIR)
      buf(tau, 10) = (tb1(ijk + TB_AA_ECI0) + tb1(ijk + TB_PA_ECI0)) / ZARR '+ tb1(ijk + TB_EA_ECI0)
      buf(tau, 11) = (tb1(ijk + TB_AA_ECI1) + tb1(ijk + TB_PA_ECI1)) / ZARR '+ tb1(ijk + TB_EA_ECI1)
      buf(tau, 12) = (tb1(ijk + TB_AA_ECI2) + tb1(ijk + TB_PA_ECI2)) / ZARR '+ tb1(ijk + TB_EA_ECI2)
      buf(tau, 13) = (tb1(ijk + TB_AA_ECI3) + tb1(ijk + TB_PA_ECI3)) / ZARR
      buf(tau, 14) = (tb1(ijk + TB_AA_ECIR) + tb1(ijk + TB_AA_ECI0) + tb1(ijk + TB_AA_ECI1) + tb1(ijk + TB_AA_ECI2) + tb1(ijk + TB_AA_ECI3) + _
                      tb1(ijk + TB_PA_ECIR) + tb1(ijk + TB_PA_ECI0) + tb1(ijk + TB_PA_ECI1) + tb1(ijk + TB_PA_ECI2) + tb1(ijk + TB_PA_ECI3)) / ZARR
      buf(tau, 15) = (tb1(ijk + TB_AA_ETOT) + tb1(ijk + TB_PA_ETOT)) / ZARR '+ tb1(ijk + TB_EA_ETOT)
      buf(tau, 16) = (tb1(ijk + TB_AA_ECAR) + tb1(ijk + TB_PA_ECAR)) / ZARR
      buf(tau, 17) = (tb1(ijk + TB_AA_ECA0) + tb1(ijk + TB_PA_ECA0)) / ZARR
      buf(tau, 18) = (tb1(ijk + TB_AA_ECA1) + tb1(ijk + TB_PA_ECA1)) / ZARR
      buf(tau, 19) = (tb1(ijk + TB_AA_ECAR) + tb1(ijk + TB_AA_ECA0) + tb1(ijk + TB_AA_ECA1) + _
                      tb1(ijk + TB_PA_ECAR) + tb1(ijk + TB_PA_ECA0) + tb1(ijk + TB_PA_ECA1)) / ZARR
      buf(tau, 20) = (tb1(ijk + TB_AA_EMAT) + tb1(ijk + TB_PA_EMAT)) / ZARR
      buf(tau, 21) = (tb1(ijk + TB_AA_ECSP) + tb1(ijk + TB_PA_ECSP)) / ZARR
      buf(tau, 22) = (tb1(ijk + TB_AA_EAI0) + tb1(ijk + TB_PA_EAI0)) / ZARR
      buf(tau, 23) = (tb1(ijk + TB_AA_EAI1) + tb1(ijk + TB_PA_EAI1)) / ZARR
      buf(tau, 24) = (tb1(ijk + TB_AA_EFS0) + tb1(ijk + TB_PA_EFS0)) / ZARR
      buf(tau, 25) = (tb1(ijk + TB_AA_EFS1) + tb1(ijk + TB_PA_EFS1)) / ZARR
      buf(tau, 26) = (tb1(ijk + TB_AA_EFI0) + tb1(ijk + TB_PA_EFI0)) / ZARR
      buf(tau, 27) = (tb1(ijk + TB_AA_EFI1) + tb1(ijk + TB_PA_EFI1)) / ZARR
      '--
      ib = 26
      buf(tau, ib + 2) = (tb1(ijk + TB_AA_ECSR)) / ZARR
      buf(tau, ib + 3) = (tb1(ijk + TB_AA_ECS01)) / ZARR
      buf(tau, ib + 4) = (tb1(ijk + TB_AA_ECS02)) / ZARR
      buf(tau, ib + 5) = (tb1(ijk + TB_AA_ECS1)) / ZARR
      buf(tau, ib + 6) = (tb1(ijk + TB_AA_ECS2)) / ZARR
      buf(tau, ib + 7) = (tb1(ijk + TB_AA_ECS3)) / ZARR
      buf(tau, ib + 8) = (tb1(ijk + TB_AA_ECSR) + tb1(ijk + TB_AA_ECS01) + tb1(ijk + TB_AA_ECS02) + _
                          tb1(ijk + TB_AA_ECS1) + tb1(ijk + TB_AA_ECS2) + tb1(ijk + TB_AA_ECS3)) / ZARR
      buf(tau, ib + 9) = (tb1(ijk + TB_AA_ECIR)) / ZARR
      buf(tau, ib + 10) = (tb1(ijk + TB_AA_ECI0)) / ZARR
      buf(tau, ib + 11) = (tb1(ijk + TB_AA_ECI1)) / ZARR
      buf(tau, ib + 12) = (tb1(ijk + TB_AA_ECI2)) / ZARR
      buf(tau, ib + 13) = (tb1(ijk + TB_AA_ECI3)) / ZARR
      buf(tau, ib + 14) = (tb1(ijk + TB_AA_ECIR) + tb1(ijk + TB_AA_ECI0) + tb1(ijk + TB_AA_ECI1) + tb1(ijk + TB_AA_ECI2) + tb1(ijk + TB_AA_ECI3)) / ZARR
      buf(tau, ib + 15) = (tb1(ijk + TB_AA_ETOT)) / ZARR
      buf(tau, ib + 16) = (tb1(ijk + TB_AA_ECAR)) / ZARR
      buf(tau, ib + 17) = (tb1(ijk + TB_AA_ECA0)) / ZARR
      buf(tau, ib + 18) = (tb1(ijk + TB_AA_ECA1)) / ZARR
      buf(tau, ib + 19) = (tb1(ijk + TB_AA_ECAR) + tb1(ijk + TB_AA_ECA0) + tb1(ijk + TB_AA_ECA1)) / ZARR
      buf(tau, ib + 20) = (tb1(ijk + TB_AA_EMAT)) / ZARR
      buf(tau, ib + 21) = (tb1(ijk + TB_AA_ECSP)) / ZARR
      buf(tau, ib + 22) = (tb1(ijk + TB_AA_EAI0)) / ZARR
      buf(tau, ib + 23) = (tb1(ijk + TB_AA_EAI1)) / ZARR
      buf(tau, ib + 24) = (tb1(ijk + TB_AA_EFS0)) / ZARR
      buf(tau, ib + 25) = (tb1(ijk + TB_AA_EFS1)) / ZARR
      buf(tau, ib + 26) = (tb1(ijk + TB_AA_EFI0)) / ZARR
      buf(tau, ib + 27) = (tb1(ijk + TB_AA_EFI1)) / ZARR
    Next tau
    '20121018LC (fine)
  Case REP1_MONT
    '**************************************************************************
    'Crea la tabella EVOLUZIONE MONTANTI CONTRIBUTIVI E RESTTTUZIONI CONTRIBUTI
    '**************************************************************************
    '--- 20121114LC (inizio)
    tauMin = IIf(OPZ_OUTPUT_MAX = 0, 0, 1961 - appl.t0)  '20160611LC (anche annoBT)
    ReDim buf(tauMin To tauMax, 1 To 3)  '20150612LC-2
    For tau = tauMin To tauMax
      'ijk0 = -1
      ijk = appl.nSize2 * iTS + appl.nSize3 * 0 + appl.nSize4 * (tau + appl.t0 - 1960) '20121114LC  '18
      buf(tau, 1) = appl.t0 + tau
      '--- 20150612LC-2 (inizio)
      buf(tau, 2) = tb1(ijk + TB_OMRC) / ZARR
      buf(tau, 3) = tb1(ijk + TB_OMRC_6) / ZARR
      '--- 20150612LC-2 (fine)
    Next tau
  Case REP1_USC1
    '***********************************
    'Crea la tabella EVOLUZIONE PENSIONI
    '***********************************
    '--- 20121114LC (inizio)
    ReDim vv(0 To tauMax, 0 To 11)
    For tau = 0 To tauMax
    '--- 20121114LC (fine)
      ijk0 = -1
      ijk = appl.nSize2 * iTS + appl.nSize3 * 0 + appl.nSize4 * (tau + appl.t0 - 1960) '20121114LC  '19
      '---
      vv(tau, 0) = tb1(ijk + TB_ZPN_VO) _
                 + tb1(ijk + TB_ZPN_VA) _
                 + tb1(ijk + TB_ZPN_VP) _
                 + tb1(ijk + TB_ZPN_A) _
                 + tb1(ijk + TB_ZPN_T) _
                 + tb1(ijk + TB_ZPN_S) _
                 + tb1(ijk + TB_ZPN_I) _
                 + tb1(ijk + TB_ZPN_B) _
                 + tb1(ijk + TB_ZPS_1)
      vv(tau, 4) = tb1(ijk + TB_ZPC_VO) + _
                   tb1(ijk + TB_ZPC_VA) + _
                   tb1(ijk + TB_ZPC_VP) + _
                   tb1(ijk + TB_ZPC_A) + _
                   tb1(ijk + TB_ZPC_S) + _
                   tb1(ijk + TB_ZPC_T)  '20140321LC  'ex 20131202LC  'ex 20120115LC
      vv(tau, 8) = vv(tau, 0) + vv(tau, 4)
      '---
      If tau > 0 Then
        vv(tau, 1) = (vv(tau - 1, 0) + vv(tau, 0)) / 2
        '--- 20131129LC (inizio)
        vv(tau, 2) = tb1(ijk + TB_MPN_VO) _
                   + tb1(ijk + TB_MPN_VA) _
                   + tb1(ijk + TB_MPN_VP) _
                   + tb1(ijk + TB_MPN_A) _
                   + tb1(ijk + TB_MPN_S) _
                   + tb1(ijk + TB_MPN_T) _
                   + tb1(ijk + TB_MPN_B) _
                   + tb1(ijk + TB_MPN_I) _
                   + tb1(ijk + TB_MPS)
                 y = tb1(ijk + TB_SPN_VO) _
                   + tb1(ijk + TB_SPN_VA) _
                   + tb1(ijk + TB_SPN_VP) _
                   + tb1(ijk + TB_SPN_A) _
                   + tb1(ijk + TB_SPN_S) _
                   + tb1(ijk + TB_SPN_T) _
                   + tb1(ijk + TB_SPN_B) _
                   + tb1(ijk + TB_SPN_I) _
                   + tb1(ijk + TB_SPS)
        '--- 20131129LC (fine)
        If y > 0 Then vv(tau, 3) = vv(tau, 2) / y
        '--- 20140325LC (inizio)
        vv(tau, 5) = (vv(tau - 1, 4) + vv(tau, 4)) / 2
        vv(tau, 6) = tb1(ijk + TB_MPC_VO) _
                   + tb1(ijk + TB_MPC_VA) _
                   + tb1(ijk + TB_MPC_VP) _
                   + tb1(ijk + TB_MPC_A) _
                   + tb1(ijk + TB_MPC_S) _
                   + tb1(ijk + TB_MPC_T)
                 z = tb1(ijk + TB_SPC_VO) _
                   + tb1(ijk + TB_SPC_VA) _
                   + tb1(ijk + TB_SPC_VP) _
                   + tb1(ijk + TB_SPC_A) _
                   + tb1(ijk + TB_SPC_S) _
                   + tb1(ijk + TB_SPC_T)
        If z > 0 Then vv(tau, 7) = vv(tau, 6) / z
        '--- 20140325LC (fine)
        vv(tau, 9) = (vv(tau - 1, 8) + vv(tau, 8)) / 2
        vv(tau, 10) = vv(tau, 2) + vv(tau, 6)
        If vv(tau, 9) > 0 Then
          vv(tau, 11) = vv(tau, 10) / vv(tau, 9)
        End If
      End If
    Next tau
    '--- 20121114LC (inizio)
    tauMin = 1
    ReDim buf(tauMin To tauMax, 1 To 10)
    For tau = tauMin To tauMax
    '--- 20121114LC (fine)
      buf(tau, 1) = appl.t0 + tau
      'TODO: TUTTI I PENSIONATI A ZERO !?
      buf(tau, 2) = vv(tau - 1, 0)
      buf(tau, 3) = vv(tau, 2) / ZARR
      buf(tau, 4) = vv(tau, 3) / ZARR
      buf(tau, 5) = vv(tau - 1, 4)
      buf(tau, 6) = vv(tau, 6) / ZARR
      buf(tau, 7) = vv(tau, 7) / ZARR
      buf(tau, 8) = vv(tau - 1, 8)
      buf(tau, 9) = vv(tau, 10) / ZARR
      buf(tau, 10) = vv(tau, 11) / ZARR
      'buf(tau, 8) = (vv(tau - 1, 11 - 8 - 3) + vv(tau - 1, 11 - 4 - 3)) '/ ZARR
      'buf(tau, 9) = (vv(tau, 11 - 8 - 1) + vv(tau, 11 - 4 - 1)) / ZARR
      'buf(tau, 10) = (vv(tau, 11 - 8) + vv(tau, 11 - 4)) / ZARR
    Next tau
  Case REP1_USC2
    '******************************************************
    'Crea la tabella EVOLUZIONE PENSIONATI NON CONTRIBUENTI
    '******************************************************
    '--- 20121114LC (inizio)
    tauMin = 1
    ReDim buf(tauMin To tauMax, 1 To 28)  '20131129LC
    For tau = tauMin To tauMax
    '--- 20121114LC (fine)
      ijk0 = appl.nSize2 * iTS + appl.nSize3 * 0 + appl.nSize4 * (tau - 1 + appl.t0 - 1960) '20121210LC-2
      ijk = appl.nSize2 * iTS + appl.nSize3 * 0 + appl.nSize4 * (tau + appl.t0 - 1960) '20121114LC  '20
      buf(tau, 1) = appl.t0 + tau
      '--- Vecchiaia ---
      z = tb1(ijk + TB_SPN_VO)
      buf(tau, 2) = tb1(ijk0 + TB_ZPN_VO)
      buf(tau, 3) = tb1(ijk + TB_MPN_VO) / ZARR
      buf(tau, 4) = tb1(ijk + TB_MPN_VO) / IIf(z > 0, z, 1) / ZARR
      '--- Vecchiaia anticipata ---
      z = tb1(ijk + TB_SPN_VA)
      buf(tau, 5) = tb1(ijk0 + TB_ZPN_VA)
      buf(tau, 6) = tb1(ijk + TB_MPN_VA) / ZARR
      buf(tau, 7) = tb1(ijk + TB_MPN_VA) / IIf(z > 0, z, 1) / ZARR
      '--- Vecchiaia posticipata ---
      z = tb1(ijk + TB_SPN_VP)
      buf(tau, 8) = tb1(ijk0 + TB_ZPN_VP)
      buf(tau, 9) = tb1(ijk + TB_MPN_VP) / ZARR
      buf(tau, 10) = tb1(ijk + TB_MPN_VP) / IIf(z > 0, z, 1) / ZARR
      '--- Anzianit� ---
      z = tb1(ijk + TB_SPN_A)
      buf(tau, 11) = tb1(ijk0 + TB_ZPN_A)
      buf(tau, 12) = tb1(ijk + TB_MPN_A) / ZARR
      buf(tau, 13) = tb1(ijk + TB_MPN_A) / IIf(z > 0, z, 1) / ZARR
      '--- Sostitutiva ---
      z = tb1(ijk + TB_SPN_S)
      buf(tau, 14) = tb1(ijk0 + TB_ZPN_S)
      buf(tau, 15) = tb1(ijk + TB_MPN_S) / ZARR
      buf(tau, 16) = tb1(ijk + TB_MPN_S) / IIf(z > 0, z, 1) / ZARR
      '--- Totalizzazione ---
      z = tb1(ijk + TB_SPN_T)
      buf(tau, 17) = tb1(ijk0 + TB_ZPN_T)
      buf(tau, 18) = tb1(ijk + TB_MPN_T) / ZARR
      buf(tau, 19) = tb1(ijk + TB_MPN_T) / IIf(z > 0, z, 1) / ZARR
      '--- Inabilit� ---
      z = tb1(ijk + TB_SPN_B)
      buf(tau, 20) = tb1(ijk0 + TB_ZPN_B)
      buf(tau, 21) = tb1(ijk + TB_MPN_B) / ZARR
      buf(tau, 22) = tb1(ijk + TB_MPN_B) / IIf(z > 0, z, 1) / ZARR
      '--- Invalidit� ---
      z = tb1(ijk + TB_SPN_I)
      buf(tau, 23) = tb1(ijk0 + TB_ZPN_I)
      buf(tau, 24) = tb1(ijk + TB_MPN_I) / ZARR
      buf(tau, 25) = tb1(ijk + TB_MPN_I) / IIf(z > 0, z, 1) / ZARR
      '--- Superstiti ---
      z = tb1(ijk + TB_SPS)
      buf(tau, 26) = tb1(ijk0 + TB_ZPS_1)
      buf(tau, 27) = tb1(ijk + TB_MPS) / ZARR
      buf(tau, 28) = tb1(ijk + TB_MPS) / IIf(z > 0, z, 1) / ZARR
    Next tau
  Case REP1_BIL_TECN_MIN
    '***********************************************
    'Crea la tabella BILANCIO TECNICO (ministeriale)
    '***********************************************
    If appl.SuddivRipart(0) > 0 Then
      fraz = appl.SuddivRipart(iTS) / appl.SuddivRipart(0)
    Else
      fraz = 1
    End If
    '--- 20121114LC (inizio)
    tauMin = 1
    ReDim v2#(tauMin To tauMax)
    ReDim v3#(tauMin To tauMax)
    ReDim buf(0 To tauMax, 1 To 22)  '20140404LC  'ex 20131126LC
    buf(0, 1) = appl.t0
    buf(0, 15) = fraz * appl.parm(P_btPatrim) / ZARR
    For tau = tauMin To tauMax
    '--- 20121114LC (fine)
      ijk0 = -1
      ijk = appl.nSize2 * iTS + appl.nSize3 * 0 + appl.nSize4 * (tau + appl.t0 - 1960) '20121114LC  '23
      z = TassoZCB(appl.CoeffVar, appl.t0 + tau, cv_Ti)  '20121209LC
      If tau = 1 Then
        '20080614LC (inizio)
        v2(tau) = fraz * appl.parm(P_btPassSIV) * (1 + appl.parm(P_btPassSIP)) ^ 0
        v3(tau) = 0#
        '20080614LC (fine)
      Else
        v2(tau) = v2(tau - 1) * (1 + appl.parm(P_btPassSIP)) * (1 + appl.parm(P_cvTs2a)) ^ 0
        v3(tau) = 0#
      End If
      '---
      ' 1 ANNO
      buf(tau, 1) = appl.t0 + tau
      ' 2 CONTRIBUTI SOGGETTIVI
      ' 3 CONTRIBUTI INTEGRATIVI
      'buf(tau, 2) = (tb1(ijk+TB_A_ETOT) + tb1(ijk+TB_EA_ETOT)) / ZARR
      'buf(tau, 3) = 0
      '--- 20140414LC (inizio)
      buf(tau, 2) = (tb1(ijk + TB_AA_ECS01) + tb1(ijk + TB_AA_ECS02) + tb1(ijk + TB_AA_ECS1) + _
                     tb1(ijk + TB_AA_ECS2) + tb1(ijk + TB_AA_ECS3) + tb1(ijk + TB_AA_ECSR) + _
                     tb1(ijk + TB_PA_ECS01) + tb1(ijk + TB_PA_ECS02) + tb1(ijk + TB_PA_ECS1) + _
                     tb1(ijk + TB_PA_ECS2) + tb1(ijk + TB_PA_ECS3) + tb1(ijk + TB_PA_ECSR) + _
                     tb1(ijk + TB_ECRR)) / ZARR
      buf(tau, 2) = buf(tau, 2) + (tb1(ijk + TB_AA_ECSP) + tb1(ijk + TB_PA_ECSP)) / ZARR '20120515LC
      '20121018LC (fine)
      '--- 20140414LC (fine)
      '20140409LC (commentato)
      ' 4 ALTRI CONTRIBUTI
      buf(tau, 4) = (v2(tau) + v3(tau)) / ZARR
      ' 6 ALTRE ENTRATE
      buf(tau, 6) = 0
      '20120301LC (inizio)
      If 1 = 1 Then
         ptot2 = 0
      Else  '20131126LC
        ptot2 = tb1(ijk + TB_MPN_B) _
              + tb1(ijk + TB_MPN_I) _
              + tb1(ijk + TB_MPS) _
              + tb1(ijk + TB_PASS1) _
              + tb1(ijk + TB_PASS2)
      End If
      ptot1 = tb1(ijk + TB_MP_TOT) - ptot2
      ' 8 PRESTAZIONI PENSIONISTICHE
      buf(tau, 8) = ptot1 / ZARR
      ' 9 ALTRE PRESTAZIONI
      buf(tau, 9) = (ptot2 + tb1(ijk + TB_MPW_1) + tb1(ijk + TB_MPW_2) + tb1(ijk + TB_MPW_3)) / ZARR
      '20120301LC (fine)
      '10 ALTRE USCITE
      If appl.parm(P_btSpAss) >= 1 Then
        '20080614LC
        buf(tau, 10) = fraz * appl.parm(P_btSpAss) * (1 + appl.parm(P_btPassPFP)) ^ (tau - 1) / ZARR
      Else
        buf(tau, 10) = appl.parm(P_btSpAss) * (buf(tau, 2) + buf(tau, 3))
      End If
      '11 SPESE DI GESTIONE
      If appl.parm(P_btSpAmm) >= 1 Then
        '20080614LC
        buf(tau, 11) = fraz * appl.parm(P_btSpAmm) * (1 + appl.parm(P_btPassPFP)) ^ (tau - 1) / ZARR
      Else
        buf(tau, 11) = appl.parm(P_btSpAmm) * (buf(tau, 2) + buf(tau, 3))
      End If
      '12 TOTALE USCITE
      buf(tau, 12) = buf(tau, 8) + buf(tau, 9) + buf(tau, 10) + buf(tau, 11)
      ' 5 RENDIMENTI
      buf(tau, 5) = (buf(tau - 1, 15) + 0.5 * (buf(tau, 2) + buf(tau, 3) + buf(tau, 4) - buf(tau, 12))) * z
      ' 7 TOTALE ENTRATE
      buf(tau, 7) = buf(tau, 2) + buf(tau, 3) + buf(tau, 4) + buf(tau, 5) + buf(tau, 6)
      '13 SALDO PREVIDENZIALE
      buf(tau, 13) = buf(tau, 2) + buf(tau, 3) + buf(tau, 4) - buf(tau, 8) - buf(tau, 9)
      '14 SALDO CORRENTE
      buf(tau, 14) = buf(tau, 7) - buf(tau, 12)
      '15 PATRIMONIO FINALE
      buf(tau, 15) = buf(tau - 1, 15) + buf(tau, 14)
      '16 RENDIMENTO
      buf(tau, 16) = z '013 RENDIMENTO
      '--- 20131126LC (inizio)
      'If OPZ_INT_ASSIST_MODEL >= 1 Then
        buf(tau, 17) = tb1(ijk + TB_MPN_B) / ZARR
        buf(tau, 18) = tb1(ijk + TB_MPN_I) / ZARR
        buf(tau, 19) = tb1(ijk + TB_MPS) / ZARR
        buf(tau, 20) = tb1(ijk + TB_PASS1) / ZARR
        buf(tau, 21) = tb1(ijk + TB_PASS2) / ZARR
        buf(tau, 22) = (tb1(ijk + TB_AA_ECSP) + tb1(ijk + TB_PA_ECSP)) / ZARR 'CONTRIBUTI DI SOLIDARIETA'  '20140414LC
      'End If
      '--- 20131126LC (fine)
    Next tau
  Case REP1_BIL_FIRR  '20160419LC-2
    '*****************************
    'Crea la tabella BILANCIO FIRR  '20160419LC-2
    '*****************************
    If appl.SuddivRipart(0) > 0 Then
      fraz = appl.SuddivRipart(iTS) / appl.SuddivRipart(0)
    Else
      fraz = 1
    End If
    '---
    tauMin = 1
    ReDim buf(tauMin To tauMax, 1 To 8)  '20140404LC  'ex 20131126LC
    ijk = appl.nSize2 * iTS + appl.nSize3 * 0 + appl.nSize4 * (0 + appl.t0 - 1960)
    For tau = tauMin To tauMax
      ijk0 = ijk
      ijk = appl.nSize2 * iTS + appl.nSize3 * 0 + appl.nSize4 * (tau + appl.t0 - 1960)
      buf(tau, 1) = appl.t0 + tau
      '--- montante iniziale
      buf(tau, 2) = tb1(ijk0 + TB_OMRC_6) / ZARR
      '--- entrate
      buf(tau, 3) = (tb1(ijk + TB_AA_ECI0) + tb1(ijk + TB_AA_ECI1) + tb1(ijk + TB_AA_ECI2) + tb1(ijk + TB_AA_ECI3) + tb1(ijk + TB_AA_ECIR) + _
                     tb1(ijk + TB_PA_ECI0) + tb1(ijk + TB_PA_ECI1) + tb1(ijk + TB_PA_ECI2) + tb1(ijk + TB_PA_ECI3) + tb1(ijk + TB_PA_ECIR)) / ZARR
      '--- uscite
      buf(tau, 4) = (tb1(ijk + TB_FIRR_USC)) / ZARR  '20160612LC
      '--- uscite senza superstiti
      buf(tau, 5) = (tb1(ijk + TB_FIRR_SSUP)) / ZARR  '20160613LC
      '--- montante finale
      buf(tau, 7) = tb1(ijk + TB_OMRC_6) / ZARR
      '--- interessi
      buf(tau, 6) = buf(tau, 7) - (buf(tau, 2) + buf(tau, 3) - buf(tau, 4) - buf(tau, 5))
      '--- rendimento
      z = TassoZCB(appl.CoeffVar, appl.t0 + tau, ecv.cv_Trmc6)
      buf(tau, 8) = z
    Next tau
  Case REP1_VAL_CAP
    '*******************************
    'Crea la tabella VALORI CAPITALI
    '*******************************
'                      Contributi     Pensioni     Pensioni      dirette      dirette     Pensioni     Pensioni    Restituz.                          "
'  Anno     Iscritti    e versam.    inabilit�   invalidit�     contrib.   non contr.    reversib.       totali   contributi       Uscite        Saldi"
'   (T)      ( NTOT)      (AETOT)     (APBTOT)     (APITOT)    (APDCTOT)    (APDNTOT)     (APRTOT)      (APTOT)       (ISOS)      (AUTOT)      (ASTOT)"
'  ---------------------------------------------------------------------------------------------------------------------------------------------------"
'         Redd. fino    Dettaglio pensioni ai non contribuenti                                         Dettaglio restituz. contributi    "
'  Anno     al tetto   vecch.ord.   vecch.ant.  vecch.post.    anzianit�   contrib.va    totalizz.     usc.a.c.      cessati       s.sup."
'   (T)     (IRPEF2)   (APDVOTOT)   (APDVATOT)   (APDVPTOT)    (APDATOT)    (APDSTOT)    (APDTTOT)      (ISOS1)      (ISOS2)      (ISOS3)"
'  --------------------------------------------------------------------------------------------------------------------------------------"
    '--- 20140312LC (inizio)  '20131202LC
    ReDim buf(1 To 22)
    z = TassoZCB(appl.CoeffVar, appl.t0 + 1, cv_Tv)  '20121209LC
    y = (1# + z) ^ -0.5 / ZARR
    buf(1) = appl.t0 + 1
    ijk = appl.nSize2 * iTS + appl.nSize3 * iQ + appl.nSize4 * (0 + appl.t0 - 1960)      '20140312LC  'ex 20121114LC  '24
    buf(2) = tb1(ijk + TB_ZTOT)                                ' 2 NTOT
    For tau = 1 To appl.nMax2 + 1  'UBound(appl.tb4, 3) '20080617LC
      ijk0 = -1
      ijk = appl.nSize2 * iTS + appl.nSize3 * iQ + appl.nSize4 * (tau + appl.t0 - 1960)  '20140312LC  'ex 20121114LC  '25
      If iQ = 0 Then
        PTOT = tb1(ijk + TB_MP_TOT)
        UTOT = tb1(ijk + TB_MU_TOT)
        STOT = tb1(ijk + TB_STOT)
      Else
        '--- 20140325LC (inizio)
        PTOT = tb1(ijk + TB_MPC_VO) _
             + tb1(ijk + TB_MPC_VA) _
             + tb1(ijk + TB_MPC_VP) _
             + tb1(ijk + TB_MPC_A) _
             + tb1(ijk + TB_MPC_T) _
             + tb1(ijk + TB_MPC_S) _
             + tb1(ijk + TB_MPN_VO) _
             + tb1(ijk + TB_MPN_VA) _
             + tb1(ijk + TB_MPN_VP) _
             + tb1(ijk + TB_MPN_A) _
             + tb1(ijk + TB_MPN_T) _
             + tb1(ijk + TB_MPN_S) _
             + tb1(ijk + TB_MPN_B) _
             + tb1(ijk + TB_MPN_I) _
             + tb1(ijk + TB_MPS)
        '--- 20140325LC (fine)
        UTOT = PTOT _
             + tb1(ijk + TB_MPW_1) _
             + tb1(ijk + TB_MPW_2) _
             + tb1(ijk + TB_MPW_3)
        STOT = tb1(ijk + TB_AA_ETOT) + tb1(ijk + TB_PA_ETOT) - UTOT  '20140414LC  '+ tb1(ijk + TB_EA_ETOT)
      End If
      buf(3) = buf(3) + (tb1(ijk + TB_AA_ETOT) + tb1(ijk + TB_PA_ETOT)) * y    ' 3 AETOT  '20140414LC  '+tb1(ijk + TB_EA_ETOT)
      buf(4) = buf(4) + tb1(ijk + TB_MPN_B) * y         ' 4 APBTOT
      buf(5) = buf(5) + tb1(ijk + TB_MPN_I) * y         ' 5 APITOT
      '--- 20140325LC (inizio)
      buf(6) = buf(6) + (tb1(ijk + TB_MPC_VO) _
                      + tb1(ijk + TB_MPC_VA) _
                      + tb1(ijk + TB_MPC_VP) _
                      + tb1(ijk + TB_MPC_A) _
                      + tb1(ijk + TB_MPC_T) _
                      + tb1(ijk + TB_MPC_S)) * y        ' 6 APDCTOT
      '--- 20140325LC (fine)
      buf(7) = buf(7) + (tb1(ijk + TB_MPN_VO) _
                      + tb1(ijk + TB_MPN_VA) _
                      + tb1(ijk + TB_MPN_VP) _
                      + tb1(ijk + TB_MPN_A) _
                      + tb1(ijk + TB_MPN_T) _
                      + tb1(ijk + TB_MPN_S)) * y        ' 7 APDNTOT
      buf(8) = buf(8) + tb1(ijk + TB_MPS) * y           ' 8 APSTOT
      buf(9) = buf(9) + PTOT * y                        ' 9 APTOT
      buf(10) = buf(10) + (tb1(ijk + TB_MPW_1) _
                        + tb1(ijk + TB_MPW_2) _
                        + tb1(ijk + TB_MPW_3)) * y      '10 ISOS
      buf(11) = buf(11) + UTOT * y                      '11 AUTOT
      buf(12) = buf(12) + STOT * y                      '12 ASTOT
      z = TassoZCB(appl.CoeffVar, appl.t0 + tau + 1, cv_Tv) '20121209LC
      y = y / (1# + z)
    Next tau
    appl.iterSaldo = buf(12)  '20161111LC
    '---
    z = TassoZCB(appl.CoeffVar, appl.t0 + 1, cv_Tv)  '20121209LC
    y = (1# + z) ^ -0.5 / ZARR
    For tau = 1 To tauMax
      ijk0 = -1
      ijk = appl.nSize2 * iTS + appl.nSize3 * iQ + appl.nSize4 * (tau + appl.t0 - 1960) '20121114LC  '26
      buf(13) = buf(13) + (tb1(ijk + TB_AA_IRPEF2) + tb1(ijk + TB_PA_IRPEF2)) * y   '20140409LC   '13 IRPEF2   redditi fino al tetto
      buf(14) = buf(14) + tb1(ijk + TB_MPN_VO) * y     '14 PDVOTOT  vecchiaia ordinaria
      buf(15) = buf(15) + tb1(ijk + TB_MPN_VA) * y     '15 PDVATOT  vecchiaia anticipata
      buf(16) = buf(16) + tb1(ijk + TB_MPN_VP) * y     '16 PDVPTOT  vecchiaia posticipata
      buf(17) = buf(17) + tb1(ijk + TB_MPN_A) * y      '17 PDATOT   anzianit�
      buf(18) = buf(18) + tb1(ijk + TB_MPN_S) * y      '18 PDSTOT   sostitutiva
      buf(19) = buf(19) + tb1(ijk + TB_MPN_T) * y      '19 PDTTOT   totalizz.ne
      buf(20) = buf(20) + tb1(ijk + TB_MPW_1) * y      '20 ISOS1    usciti a.c.
      buf(21) = buf(21) + tb1(ijk + TB_MPW_2) * y      '21 ISOS2    cessati
      buf(22) = buf(22) + tb1(ijk + TB_MPW_3) * y      '22 ISOS3    s.sup.
      z = TassoZCB(appl.CoeffVar, appl.t0 + tau + 1, cv_Tv)    '20121209LC
      y = y / (1# + z)
    Next tau
    '--- 20140312LC (fine)  'ex 20131202LC
    '--- 20140312LC (inizio)
    If iQ = 0 Then
      If OPZ_SUDDIVISIONI_MR = 2 Then 'Ripartisce secondo il VA delle uscite
        appl.SuddivRipart(iTS) = buf(11)
      ElseIf OPZ_SUDDIVISIONI_MR = 3 Then 'Ripartisce secondo il VA delle entrate
        appl.SuddivRipart(iTS) = buf(3)
      End If
    End If
    '--- 20140312LC (fine)
  End Select
End Sub


Private Function TAppl_Stampa3c_StampaDati_A(ByRef appl As TAppl, ByRef glo As TGlobale, ByRef iTab As Long, _
    ByRef tauMax As Long, ByRef buf() As Double, ByRef iop As Integer, ByRef fo As Long, ByRef xlsApp As Object, _
    ByRef iFmt As Byte, ByRef iTS As Long, ByRef ZARR As Double) As String
'*********************************************************************************************************************
'20080614LC - iTS
'20140111LC-17 (tutta)
'20161112LC  gestione errori XLS
'*********************************************************************************************************************
'Anno Tv Ti Tev Trn
'Trmc0 Trmc1 Trmc2 Trmc3 Trmc4 Trmc5 Trmc6
'Ts1A Ts1P Ts1N
'Tp1Max Tp1Al Tp2Max Tp2Al Tp3Max Tp3Al
'PrcPVattM PrcPVattF PrcPAattM PrcPAattF PrcPCattM PrcPCattF
'Pveo30M Pveo30F Pveo65M Pveo65F Pveo98M Pveo98F
'Pvea35M Pvea35F Pvea58M Pvea58F Pvep70M Pvep70F
'Pinab2M Pinab2F Pinab10M Pinab10F Pinab35M Pinab35F
'Pinv5M Pinv5F Pinv70
'Pind2M Pind2F Pind20M Pind20F Pind30M Pind30F
'RcHminM RcHminF RcXminM RcXminF RcPrcCon
'Sogg0Min Sogg0MinR Sogg1Al Sogg1AlR Sogg1Max Sogg1MaxR Sogg2Al Sogg2AlR Sogg2Max Sogg2MaxR Sogg3Al Sogg3AlR
'Int0Min Int0MinR Int1Al Int1AlR Int1Max Int1MaxR Int2Al Int2AlR Int3Al Int3AlR
'Ass0Min Ass0MinR Ass1Al Ass1AlR Mat0Min Mat0MinR
'SpAnni SpPrcCon SpEtaMax
'Av Avo Avoo Ao Aoo Aooo
'Prd_Tv Prd_Ti Prd_Tev Prd_Trn Prd_Trmc0 Prd_Trmc1 Prd_Trmc2 Prd_Trmc3 Prd_Trmc4 Prd_Trmc5 Prd_Trmc6 Prd_Ts1A Prd_Ts1P Prd_Ts1N
  Dim ib%, iCol%, iRiga%, r%  '20140414LC
  Dim anno&, fi&, ir&, j&, tau&, tauMin&  '20121114LC
  Dim y#, z#
  Dim a$, sfL$, sfM$, sfp$, sfT0$, sfT1$, sfT2$, sfT3$, sfT4$, s$, sz$  '20140111LC-17
  Dim sErr As String  '20161112LC
  Dim xlsWsh As Object
  
  sErr = ""
  '********
  'File TXT
  '********
  Select Case iFmt
  Case 1
    sfL = "  #.######"
    sfT0 = " #######.##"
    sfM = " #######.##"
  Case 2
    sfL = "  ##.#####"
    sfT0 = " #######.##"
    sfM = " #######.##"
  Case 3
    sfL = "  ###.####"
    sfT0 = " #######.##"
    sfM = " #######.##"
  Case 4
    sfL = "  ####.###"
    sfT0 = " #######.##"
    sfM = " #######.##"
  Case 5
    sfL = "  #####.##"
    sfT0 = " #######.##"
    sfM = " #######.##"
  Case 6
    sfL = "  ######.#"
    sfT0 = " ########.#"
    sfM = " ########.#"
  Case Else
    sfL = "  ########"
    sfT0 = " ##########"
    sfM = " ##########"
  End Select
  '---
  If iop < 2 Then
    If iTab = REP1_OPZ_INI And glo.rep_chk(iTab) = vbChecked Then
      '**************************
      'Stampa il file AFP_OPZ.INI
      '**************************
      '20120319LC (inizio)
      'If ON_ERR_RN Then On Error Resume Next
      'fi = FreeFile
      'Open "AFP_OPZ.INI" For Input As #fi
      'If Err.Number = 0 Then
        Print #fo,
        Print #fo,
        Print #fo, "  ***********"
        Print #fo, "  AFP_OPZ.INI"
        Print #fo, "  ***********"
        Print #fo,
        For ir = 0 To UBound(glo.OpzIni, 1)
        'Do Until EOF(fi)
          'Line Input #fi, a$
          a$ = glo.OpzIni(ir)
          a$ = UCase(Trim(a$))
          If a$ <> "" Then
            Print #fo, "  "; a$
          End If
        'Loop
        Next ir
      'End If
      'Close #fi
      'On Error GoTo 0
      '20120319LC (fine)
    ElseIf iTab = REP1_COE_VAR And glo.rep_chk(iTab) = vbChecked Then
      '********************************************
      'Stampa la tabella dei COEFFICIENTI VARIABILI
      '********************************************
      Print #fo,
      Print #fo,
      Print #fo, "  **********************************"
      Print #fo, "  TABELLA DEI COEFFICIENTI VARIABILI"
      Print #fo, "  **********************************"
      Print #fo,
      '---
      Print #fo, "  ANNO         Tv         Ti        Tev        Trn      Trmc0      Trmc1      Trmc2      Trmc3      Trmc4      Trmc5      Trmc6"
      Print #fo, "  -----------------------------------------------------------------------------------------------------------------------------"
      '--- 20121114LC (inizio)
      tauMin = 0
      For tau = tauMin To tauMax
      '--- 20121114LC (fine)
        Print #fo, fd(appl.t0 + tau, "  ####");
        '--- 2.1 Tassi di valutazione e rivalutazione ---
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Tv), " ###.######");        '  0
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Ti), " ###.######");        '  1
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Tev), " ###.######");       '  2
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Trn), " ###.######");       '  3
        '--- 2.2 Tassi di rivalutazione dei montanti contributivi ---
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Trmc0), " ###.######");     '  4
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Trmc1), " ###.######");     '  5
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Trmc2), " ###.######");     '  6
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Trmc3), " ###.######");     '  7
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Trmc4), " ###.######");     '  8
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Trmc5), " ###.######");     '  9
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Trmc6), " ###.######");     ' 10
        Print #fo,
      Next tau
      Print #fo, "  -----------------------------------------------------------------------------------------------------------------------------"
      Print #fo,
      Print #fo, "  ANNO       Ts1A       Ts1P       Ts1N     Tp1Max      Tp1Al     Tp2Max      Tp2Al     Tp3Max      Tp3Al     Tp4Max      Tp4Al     Tp5Max      Tp5Al  PrcPVattM  PrcPVattF"
      Print #fo, "  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
      For tau = tauMin To tauMax  '20121114LC
        Print #fo, fd(appl.t0 + tau, "  ####");
        '--- 2.3 Tassi di rivalutazione di redditi e volumi d'affari ---
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Ts1a), " ###.######");      ' 11
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Ts1p), " ###.######");      ' 13
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Ts1n), " ###.######");      ' 15
        '--- 2.4 Rivalutazione delle pensioni ---
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Tp1Max), " #######.##");    ' 17
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Tp1Al), " ###.######");     ' 18
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Tp2Max), " #######.##");    ' 19
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Tp2Al), " ###.######");     ' 20
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Tp3Max), " #######.##");    ' 21
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Tp3Al), " ###.######");     ' 22
        '--- 20161112LC (inizio)
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Tp4Max), " #######.##");    ' ??
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Tp4Al), " ###.######");     ' ??
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Tp5Max), " #######.##");    ' ??
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Tp5Al), " ###.######");     ' ??
        '--- 20161112LC (fine)
        '--- 2.5 Altre stime ----
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_PrcPVattM), " ###.######"); ' 23
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_PrcPVattF), " ###.######"); ' 24
        Print #fo,
      Next tau
      Print #fo, "  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
      Print #fo,
      Print #fo, "  ANNO  PrcPAattM  PrcPAattF  PrcPCattM  PrcPCattF    Pveo20M    Pveo20F    Pveo67M    Pveo67F    Pveo92M    Pveo92F"  '20160419LC
      Print #fo, "  ------------------------------------------------------------------------------------------------------------------"
      For tau = tauMin To tauMax  '20121114LC
        Print #fo, fd(appl.t0 + tau, "  ####");
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_PrcPAattM), " ###.######"); ' 25
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_PrcPAattF), " ###.######"); ' 26
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_PrcPCattM), " ###.######"); ' 27
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_PrcPCattF), " ###.######"); ' 28
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_PrcPTattM), " ###.######"); ' 29
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_PrcPTattF), " ###.######"); ' 30
        '--- 4.1 Pensione di vecchiaia ordinaria ----
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Pveo30M), " ##########");   ' 31
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Pveo30F), " ##########");   ' 32
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Pveo65M), " ##########");   ' 33
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Pveo65F), " ##########");   ' 34
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Pveo98M), " ##########");   ' 35
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Pveo98M), " ##########");   ' 36
        Print #fo,
      Next tau
      Print #fo, "  ------------------------------------------------------------------------------------------------------------------"
      Print #fo,
      Print #fo, "  ANNO    Pvea20M    Pvea20F    Pvea65M    Pvea65F    Pvep75M    Pvep75F    Pinab5M    Pinab5F    Pinab3_    Pinab4_    Pinab5_    Pinab6_"  '20160419LC
      Print #fo, "  ----------------------------------------------------------------------------------------------------------------------------------------"
      For tau = tauMin To tauMax  '20121114LC
        Print #fo, fd(appl.t0 + tau, "  ####");
        '--- 4.2 Pensione di vecchiaia anticipata ----
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Pvea35M), " ##########");   ' 37
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Pvea35F), " ##########");   ' 38
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Pvea58M), " ##########");   ' 39
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Pvea58F), " ##########");   ' 40
        '--- 4.3 vecchiaia posticipata ----
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Pvep70M), " ##########");   ' 41
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Pvep70F), " ##########");   ' 42
        '--- 4.4 Pensione di inabilit� ----
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Pinab2M), " ##########");   ' 43
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Pinab2F), " ##########");   ' 44
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Pinab10M), " ##########");  ' 45
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Pinab10F), " ##########");  ' 46
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Pinab35M), " ##########");  ' 47
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Pinab35F), " ##########");  ' 48
        Print #fo,
      Next tau
      Print #fo, "  ----------------------------------------------------------------------------------------------------------------------------------------"
      Print #fo,
      Print #fo, "  ANNO     Pinv5M     Pinv5F     Pinv70     Pind5M     Pind5F    Pind20M    Pind20F     Pind5_    _Pind6_"  '20160419LC
      Print #fo, "  -------------------------------------------------------------------------------------------------------"
      For tau = tauMin To tauMax  '20121114LC
        Print #fo, fd(appl.t0 + tau, "  ####");
        '--- 4.5 Pensione di invalidit� ----
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Pinv5M), " ##########");    ' 49
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Pinv5F), " ##########");    ' 50
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Pinv70), " ###.######");    ' 51
        '--- 4.6 Pensione indiretta ----
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Pind2M), " ##########");    ' 52
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Pind2F), " ##########");    ' 53
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Pind20M), " ##########");   ' 54
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Pind20F), " ##########");   ' 55
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Pind30M), " ##########");   ' 56
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Pind30F), " ##########");   ' 57
        Print #fo,
      Next tau
      Print #fo, "  -------------------------------------------------------------------------------------------------------"
      Print #fo,
      Print #fo, "  ANNO    RcHminM    RcHminF    RcXminM    RcXminF       Rc5_"  '20160419LC
      Print #fo, "  -----------------------------------------------------------"
      For tau = tauMin To tauMax  '20121114LC
        Print #fo, fd(appl.t0 + tau, "  ####");
        '--- 4.7 Restituzione dei contributi ----
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_RcHminM), " ##########");   ' 58
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_RcHminF), " ##########");   ' 59
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_RcXminM), " ##########");   ' 60
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_RcXminF), " ##########");   ' 61
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_RcPrcCon), " ###.######");  ' 62
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_RcPrcRiv), " ###.######");  ' 63
        '--- 4.8 Totalizzazione ----
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_TzHminM), " ##########");   ' 64
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_TzHminF), " ##########");   ' 65
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_TzXminM), " ##########");   ' 66
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_TzXminF), " ##########");   ' 67
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_TzRivMin), " ###.######");  ' 68
        '--- 5.1 Contribuzione ridotta ----
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_EtaR), " ##########");      ' 69
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_XMaxR), " ##########");     ' 70
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_AnniR), " ##########");     ' 71
        Print #fo,
      Next tau
      Print #fo, "  -----------------------------------------------------------"
      Print #fo,
      Print #fo, "  ANNO   Sogg0Min  Sogg0MinR    Sogg1Al   Sogg1AlR   Sogg1Max  Sogg1MaxR"
      Print #fo, "  ----------------------------------------------------------------------"
      For tau = tauMin To tauMax  '20121114LC
        Print #fo, fd(appl.t0 + tau, "  ####");
        '--- 5.2 Soggettivo ----
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Sogg0Min), " #######.##");  ' 72
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Sogg0MinR), " #######.##"); ' 73
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Sogg1Al), " ###.######");   ' 74
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Sogg1AlR), " ###.######");  ' 75
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Sogg1Max), " #######.##");  ' 76
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Sogg1MaxR), " #######.##"); ' 77
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Sogg2Al), " ###.######");   ' 78
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Sogg2AlR), " ###.######");  ' 79
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Sogg2Max), " #######.##");  ' 80
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Sogg2MaxR), " #######.##"); ' 81
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Sogg3Al), " ###.######");   ' 82
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Sogg3AlR), " ###.######");  ' 83
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_SoggArt25), " #######.##"); ' 84
        Print #fo,
      Next tau
      Print #fo, "  ----------------------------------------------------------------------"
      Print #fo,
      '20121018LC (inizio)
      Print #fo, "  ANNO   Firr0Min  Firr0MinR    Firr1Al   Firr1AlR   Firr1Max  Firr1MaxR    Firr2Al   Firr2AlR   Firr2Max  Firr2MaxR    Firr3Al   Firr3AlR"  '20160419LC
      Print #fo, "  ----------------------------------------------------------------------------------------------------------------------------------------"
      For tau = tauMin To tauMax  '20121114LC
        Print #fo, fd(appl.t0 + tau, "  ####");
        '--- 5.3 Integrativo / FIRR ----
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Int0Min), " #######.##");   ' 85
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Int0MinR), " #######.##");  ' 86
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Int1Al), " ###.######");    ' 87
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Int1AlR), " ###.######");   ' 88
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Int1Max), " #######.##");   ' 89
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Int1MaxR), " #######.##");  ' 90
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Int2Al), " ###.######");    ' 91
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Int2AlR), " ###.######");   ' 92
        '--- 20150512LC (inizio) (riciclo indici)
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Int2Max), " #######.##");   ' 63
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Int2MaxR), " #######.##");  ' 69
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Int3Al), " ###.######");    ' 70
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Int3AlR), " ###.######");   ' 71
        '--- 20150512LC (fine)
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_IntArt25), " #######.##");  ' 93
        Print #fo,
      Next tau
      Print #fo, "  ----------------------------------------------------------------------------------------------------------------------------------------"
      Print #fo,
      Print #fo, "  ANNO    Ass0Min   Ass0MinR     Ass1Al    Ass1AlR    Mat0Min   Mat0MinR     SpAnni   SpEtaMax   SpPrcCon"
      Print #fo, "  -------------------------------------------------------------------------------------------------------"
      For tau = tauMin To tauMax  '20121114LC
        Print #fo, fd(appl.t0 + tau, "  ####");
        '--- 5.4 Assistenziale ----
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Ass0Min), " #######.##");   ' 94
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Ass0MinR), " #######.##");  ' 95
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Ass1Al), " ###.######");    ' 96
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Ass1AlR), " ###.######");   ' 97
        '--- 5.5 Altro ----
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Mat0Min), " #######.##");   ' 98
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Mat0MinR), " #######.##");  ' 99
        '--- 6.1 Scaglioni IRPEF ----
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_ScA), " #######.##");       '100
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_AlA), " ###.######");       '101
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_ScB), " #######.##");       '102
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_AlB), " ###.######");       '103
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_ScC), " #######.##");       '104
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_AlC), " ###.######");       '105
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_ScD), " #######.##");       '106
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_AlD), " ###.######");       '107
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_ScE), " #######.##");       '108
        'Print #fo,
        '######
        'Print #fo, fd(appl.t0 + tau, "  ####");
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_AlE), " ###.######");       '109
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_ScF), " #######.##");       '110
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_AlF), " ###.######");       '111
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_ScG), " #######.##");       '112
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_AlG), " ###.######");       '113
        '--- 6.2 Pensione ----
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_PMin), " #######.##");      '114
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_NMR), " ##########");       '115
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_NTR), " ##########");       '116
        '--- 6.3 Supplementi di pensione ----
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_SpAnni), " ##########");    '117
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_SpEtaMax), " ##########");  '120
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_SpPrcCon), " #######.##");  '118
        'Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_SpPrcRiv), " #######.##");  '119
        Print #fo,
      Next tau
      Print #fo, "  -------------------------------------------------------------------------------------------------------"
      Print #fo,
      Print #fo, "  ANNO         Av        Avo       Avoo         Ao        Aoo       Aooo     Prd_Tv     Prd_Ti    Prd_Tev    Prd_Trn"
      Print #fo, "  ------------------------------------------------------------------------------------------------------------------"
      For tau = tauMin To tauMax  '20121114LC
        Print #fo, fd(appl.t0 + tau, "  ####");
        '--- 6.4 Reversibilit� superstiti ----
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Av), " ###.######");        '121
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Avo), " ###.######");       '122
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Avoo), " ###.######");      '123
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Ao), " ###.######");        '124
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Aoo), " ###.######");       '125
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Aooo), " ###.######");      '126
        '--- 2.1 Tassi di valutazione e rivalutazione ---
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Prd_Tv), " #######.##");    '127
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Prd_Ti), " #######.##");    '128
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_prd_Tev), " #######.##");   '129
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_prd_Trn), " #######.##");   '130
        Print #fo,
      Next tau
      Print #fo, "  ------------------------------------------------------------------------------------------------------------------"
      Print #fo,
      Print #fo, "  ANNO  Prd_Trmc0  Prd_Trmc1  Prd_Trmc2  Prd_Trmc3  Prd_Trmc4  Prd_Trmc5  Prd_Trmc6   Prd_Ts1a   Prd_Ts1p   Prd_Ts1n"
      Print #fo, "  ------------------------------------------------------------------------------------------------------------------"
      For tau = tauMin To tauMax  '20121114LC
        Print #fo, fd(appl.t0 + tau, "  ####");
        '--- 2.2 Tassi di rivalutazione dei montanti contributivi ---
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Prd_Trmc0), " #######.##"); '131
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Prd_Trmc1), " #######.##"); '132
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Prd_Trmc2), " #######.##"); '133
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Prd_Trmc3), " #######.##"); '134
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Prd_Trmc4), " #######.##"); '135
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Prd_Trmc5), " #######.##"); '136
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Prd_Trmc6), " #######.##"); '137
        '--- 2.3 Tassi di rivalutazione di redditi e volumi d'affari ---
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Prd_Ts1a), " #######.##");  '138
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Prd_Ts1p), " #######.##");  '140
        Print #fo, fd(appl.CoeffVar(appl.t0 + tau, cv_Prd_Ts1n), " #######.##");  '142
      '20121018LC (fine)
        Print #fo,
      Next tau
      Print #fo, "  -----------------------------------------------------------------------------------------------------------------------------"
    Else
    End If
  End If 'iop < 2 Then
  '---
  '********
  'File XLS
  '********
  If iop <> 1 And Not xlsApp Is Nothing Then
    If iTab = REP1_OPZ_INI Then
      '***********
      'AFP_OPZ_INI
      '***********
      '20120319LC (inizio)
      'fi = FreeFile
      'Open "AFP_OPZ.INI" For Input As #fi
      sz = ""
      'Do Until EOF(fi)
      For ir = 0 To UBound(glo.OpzIni, 1)
        'Line Input #fi, s
        s = glo.OpzIni(ir)
        s = UCase(Trim(s))
        If s <> "" Then
          sz = sz & s & vbCrLf
          r = r + 1
        End If
      'Loop
      Next ir
      Close #fi
      '20120319LC (fine)
'Set xlsWsh = xlsApp.ActiveWorkbook.Worksheets("afp_opz.ini")
      xlsApp.Sheets("afp_opz.ini").Select
      '--- 20161112LC (inizio)
      'xlsApp.Range("B7").Select  '20121226C (ripristinato) '20121115LC (per far spazio alle formule per il debug)
      sErr = xlsPaste(xlsApp, sz, "B7")
      If sErr <> "" Then GoTo ExitPoint
      '--- 20161112LC (inizio)
    ElseIf iTab = REP1_COE_VAR Then
      '**********************
      'Coefficienti variabili
      '**********************
      sz = ""
      '--- 20121114LC (inizio)
      tauMin = IIf(OPZ_OUTPUT_MAX = 0, 1, 1961 - appl.t0)  '20121115LC
      For iRiga = tauMin To tauMax
      '--- 20121114LC (fine)
        s = appl.t0 + iRiga
        '--- 2.1 Tassi di valutazione e rivalutazione ---
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Tv)        '  0
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Ti)        '  1
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Tev)       '  2
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Trn)       '  3
        '--- 2.2 Tassi di rivalutazione dei montanti contributivi ---
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Trmc0)     '  4
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Trmc1)     '  5
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Trmc2)     '  6
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Trmc3)     '  7
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Trmc4)     '  8
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Trmc5)     '  9
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Trmc6)     ' 10
        '--- 2.3 Tassi di rivalutazione di redditi e volumi d'affari ---
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Ts1a)      ' 11
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Ts1p)      ' 13
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Ts1n)      ' 15
        '--- 2.4 Rivalutazione delle pensioni ---
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Tp1Max)    ' 17
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Tp1Al)     ' 18
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Tp2Max)    ' 19
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Tp2Al)     ' 20
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Tp3Max)    ' 21
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Tp3Al)     ' 22
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Tp4Max)    ' ??
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Tp4Al)     ' ??
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Tp5Max)    ' ??
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Tp5Al)     ' ??
        '--- 2.5 Altre stime ----
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_PrcPVattM) ' 23
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_PrcPVattF) ' 24
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_PrcPAattM) ' 25
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_PrcPAattF) ' 26
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_PrcPCattM) ' 27
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_PrcPCattF) ' 28
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_PrcPTattM) ' 29
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_PrcPTattF) ' 30
        '******************************
        'Frame REQUISITI DI ACCESSO (4)
        '******************************
        '--- 4.1 Pensione di vecchiaia ordinaria ----
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Pveo30M)    ' 31
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Pveo30F)    ' 32
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Pveo65M)    ' 33
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Pveo65F)    ' 34
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Pveo98M)   ' 35
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Pveo98F)   ' 36
        '--- 4.2 Pensione di vecchiaia anticipata ----
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Pvea35M)    ' 37
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Pvea35F)    ' 38
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Pvea58M)    ' 39
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Pvea58F)    ' 40
        '--- 4.3 Pensione di vecchiaia posticipata ----
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Pvep70M)    ' 41
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Pvep70F)    ' 42
        '--- 4.4 Pensione di inabilit� ----
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Pinab2M)    ' 43
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Pinab2F)    ' 44
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Pinab10M)   ' 45
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Pinab10F)   ' 46
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Pinab35M)   ' 47
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Pinab35F)   ' 48
        '--- 4.5 Pensione di invalidit� ----
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Pinv5M)     ' 49
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Pinv5F)     ' 50
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Pinv70)     ' 51
        '--- 4.6 Pensione indiretta ----
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Pind2M)     ' 52
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Pind2F)     ' 53
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Pind20M)    ' 54
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Pind20F)    ' 55
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Pind30M)    ' 56
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Pind30F)    ' 57
        '--- 4.7 Restituzione dei contributi ----
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_RcHminM)    ' 58
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_RcHminF)    ' 59
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_RcXminM)    ' 60
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_RcXminF)    ' 61
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_RcPrcCon)   ' 62
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_RcPrcRiv)   ' 63
        '--- 4.6 Totalizzazione ----
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_TzHminM)    ' 64
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_TzHminF)    ' 65
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_TzXminM)    ' 66
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_TzXminF)    ' 67
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_TzRivMin)   ' 68
        '***********************
        'Frame CONTRIBUZIONE (5)
        '***********************
        '--- 5.1 Contribuzione ridotta ----
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_EtaR)       ' 69
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_XMaxR)      ' 70
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_AnniR)      ' 71
        '--- 5.2 Soggettivo ----
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Sogg0Min)   ' 72
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Sogg0MinR)  ' 73
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Sogg1Al)    ' 74
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Sogg1AlR)   ' 75
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Sogg1Max)   ' 76
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Sogg1MaxR)  ' 77
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Sogg2Al)    ' 78
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Sogg2AlR)   ' 79
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Sogg2Max)   ' 80
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Sogg2MaxR)  ' 81
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Sogg3Al)    ' 82
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Sogg3AlR)   ' 83
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_SoggArt25)  ' 84
        '--- 5.3 Integrativo / FIRR ----
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Int0Min)    ' 85
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Int0MinR)   ' 86
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Int1Al)     ' 87
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Int1AlR)    ' 88
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Int1Max)    ' 89
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Int1MaxR)   ' 90
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Int2Al)     ' 91
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Int2AlR)    ' 92
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Int2Max)    ' 63
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Int2MaxR)   ' 69
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Int3Al)     ' 70
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Int3AlR)    ' 71
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_IntArt25)   ' 93
        '--- 5.4 Assistenziale ----
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Ass0Min)    ' 94
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Ass0MinR)   ' 95
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Ass1Al)     ' 96
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Ass1AlR)    ' 97
        '--- 5.5 Altro ----
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Mat0Min)    ' 98
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Mat0MinR)   ' 99
        '****************************
        'Frame MISURA PRESTAZIONI (6)
        '****************************
        '--- 6.1 Scaglioni IRPEF ----
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_ScA)        '100
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_AlA)        '101
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_ScB)        '102
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_AlB)        '103
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_ScC)        '104
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_AlC)        '105
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_ScD)        '106
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_AlD)        '107
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_ScE)        '108
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_AlE)        '109
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_ScF)        '110
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_AlF)        '111
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_ScG)        '112
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_AlG)        '113
        '--- 6.2 Pensione ----
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_PMin)       '114
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_NMR)        '115
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_NTR)        '116
        '--- 6.3 Supplementi di pensione ----
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_SpAnni)      '117
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_SpEtaMax)    '118
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_SpPrcCon)    '119
        's = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_SpPrcRiv)   '120
        '--- 6.4 Reversibilit� superstiti ----
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Av)         '121
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Avo)        '122
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Avoo)       '123
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Ao)         '124
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Aoo)        '125
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Aooo)       '126
        '******************************
        'Frame TASSI ED ALTRE STIME (2)
        '******************************
        '--- 2.1 Tassi di valutazione e rivalutazione ---
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Prd_Tv)     '127
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Prd_Ti)     '128
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_prd_Tev)    '129
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_prd_Trn)    '130
        '--- 2.2 Tassi di rivalutazione dei montanti contributivi ---
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Prd_Trmc0)  '131
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Prd_Trmc1)  '132
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Prd_Trmc2)  '133
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Prd_Trmc3)  '134
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Prd_Trmc4)  '135
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Prd_Trmc5)  '136
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Prd_Trmc6)  '137
        '--- 2.3 Tassi di rivalutazione di redditi e volumi d'affari ---
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Prd_Ts1a)   '138
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Prd_Ts1p)   '140
        s = s & vbTab & appl.CoeffVar(appl.t0 + iRiga, cv_Prd_Ts1n)   '142
        '---
        sz = sz & s & vbCrLf
      Next iRiga
'Set xlsWsh = xlsApp.ActiveWorkbook.Worksheets("Coeff.var.")
      xlsApp.Sheets("Coeff.var.").Select
      '--- 20161112LC (inizio)
      'xlsApp.Range("B8").Select
      sErr = xlsPaste(xlsApp, sz, "B8")  '20161113LC
      If sErr <> "" Then GoTo ExitPoint
      '--- 20161112LC (fine)
    End If
  End If
  
ExitPoint:
  TAppl_Stampa3c_StampaDati_A = sErr
End Function


Private Function TAppl_Stampa3c_StampaDati_B(ByRef appl As TAppl, ByRef glo As TGlobale, ByRef iTab As Long, _
    ByRef tauMax As Long, ByRef buf() As Double, ByRef iop As Integer, ByRef fo As Long, ByRef xlsApp As Object, _
    ByRef iFmt As Byte, ByRef iTS As Long, ByRef ZARR As Double) As String
'*********************************************************************************************************************
'20080614LC - iTS
'20140111LC-17 (tutta)
'20150608LC  it
'20161112LC  gestione errori XLS
'*********************************************************************************************************************
  Dim ib%, iCol%, iRiga%, it%, r%  '20150608LC  '20140414LC
  Dim anno&, fi&, ir&, j&, tau&, tauMin&  '20121114LC
  Dim y#, z#
  Dim a$, sfL$, sfM$, sfp$, sfT0$, sfT1$, sfT2$, sfT3$, sfT4$, s$, sz$  '20140111LC-17
  Dim sErr As String  '20161112LC
  Dim xlsWsh As Object
  
  sErr = ""
  '********
  'File TXT
  '********
  Select Case iFmt
  Case 1
    sfL = "  #.######"
    sfT0 = " #######.##"
    sfM = " #######.##"
  Case 2
    sfL = "  ##.#####"
    sfT0 = " #######.##"
    sfM = " #######.##"
  Case 3
    sfL = "  ###.####"
    sfT0 = " #######.##"
    sfM = " #######.##"
  Case 4
    sfL = "  ####.###"
    sfT0 = " #######.##"
    sfM = " #######.##"
  Case 5
    sfL = "  #####.##"
    sfT0 = " #######.##"
    sfM = " #######.##"
  Case 6
    sfL = "  ######.#"
    sfT0 = " ########.#"
    sfM = " ########.#"
  Case Else
    sfL = "  ########"
    sfT0 = " ##########"
    sfM = " ##########"
  End Select
  If iop < 2 Then
    If iTab = REP1_POP And glo.rep_chk(iTab) = vbChecked Then
      '****************************************
      'Stampa la tabella SVILUPPO COLLETTIVITA'
      '****************************************
      Print #fo,
      Print #fo,
      Print #fo, "  *********************************************"
      Print #fo, "  SVILUPPO COLLETTIVITA' (valori a inizio anno)"
      Print #fo, "  *********************************************"
      Print #fo,
      '--- 20150505LC (inizio)  'ex 20131129LC  'ex 20121207LC
      Print #fo, "                                      PENS.ATI    PENSIONATI NON CONTRIBUENTI                                                   TOTALE    ELIMINATI SENZA DIRITTO                 TOTALE  "
      Print #fo, "  ANNO    ATTIVI   VOLONT.   SILENTI  CONTRIB.    VECCH.  VECCH.P.   ANZIAN.  CONTRIB.   INABIL.  INVALID.  SUPERST.    TOTALE  COLLETT.  USC.A.C.   CESSATI    S.SUP.  SUP.DEC.  GENERALE"
      Print #fo, "   (T)      (Na)      (Nv)      (Ne)     (Npc)     (Npv)    (Npvp)     (Npa)     (Nps)     (Npb)     (Npi)     (Nps)    (Npnc)    (Ntot)     (Nw1)     (Nw2)     (Nw3)     (Nw4)   (NtotG)"
      Print #fo, "  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
      tauMin = 1
      For tau = tauMin To tauMax
        Print #fo, fd(buf(tau, 1), "  ####"); 'T
        Print #fo, fd(buf(tau, 2), sfL);   'Na:   ZAA
        Print #fo, fd(buf(tau, 3), sfL); 'Nva:   ZVA
        Print #fo, fd(buf(tau, 4), sfL);   'Nea:  ZEA
        Print #fo, fd(buf(tau, 5), sfL);   'Npc:  ZPDC
        Print #fo, fd(buf(tau, 6), sfL);   'Npv:  ZPDV
        Print #fo, fd(buf(tau, 7), sfL);   'Npva: ZPDVA
        Print #fo, fd(buf(tau, 8), sfL);   'Npvp: ZPDVP
        Print #fo, fd(buf(tau, 10), sfL);  'Nps:  ZPDS
        Print #fo, fd(buf(tau, 12), sfL);  'Npi:  ZPI
        Print #fo, fd(buf(tau, 13), sfL);  'Npb:  ZPB
        Print #fo, fd(buf(tau, 14), sfL);  'Nps:  ZPS
        Print #fo, fd(buf(tau, 15), sfL);  'Npnc: ZPDV +ZPDVA +ZPDVP +ZPDA +ZPDS +ZPDT +ZPI +ZPB +ZPS
        Print #fo, fd(buf(tau, 16), sfL);  'Nw1:  ZW1
        Print #fo, fd(buf(tau, 17), sfL);  'Nw2:  ZW2
        Print #fo, fd(buf(tau, 18), sfL);  'Nw3:  ZW3
        Print #fo, fd(buf(tau, 19), sfL);  'Ntot: ZAA + ZEA + ZPDC
        Print #fo, fd(buf(tau, 20), sfL);  'Nsd:   ZSD
        Print #fo, fd(buf(tau, 21), sfL);  'NtotG: Ntot +ZW1 + ZW2 +ZW3 +ZSD
        Call TAppl_StampaDebug2(appl, appl.tb1, iTS, tau, fo)
      Next tau
      Print #fo, "  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
      '--- 20150505LC (fine)  'ex 20131129LC
    ElseIf iTab = REP1_SUP And glo.rep_chk(iTab) = vbChecked Then
      '*************************************************************
      'Stampa la tabella DECEDUTI CON SUPERSTITI E LORO RIPARTIZIONE
      '*************************************************************
      sfL = " " & sfL
      Print #fo,
      Print #fo,
      Print #fo, "  *******************************************"
      Print #fo, "  DECEDUTI CON SUPERSTITI E LORO RIPARTIZIONE"
      Print #fo, "  *******************************************"
      Print #fo,
      Print #fo, "         Deceduti  Numero di  Numero di  Numero di  Numero di  Numero di  Numero di"
      Print #fo, "       c.superst.    coniugi  coniugi e  coniugi e   1 orfano   2 orfani  3+ orfani"
      Print #fo, "  Anno  in godim.       soli   1 orfano  2+ orfani       solo       soli       soli"
      'Print #fo, "  Anno       (Ns)       (Nv)      (Nvo)     (Nvoo)       (No)      (Noo)     (Nooo)"
      Print #fo, "  ---------------------------------------------------------------------------------"
      '--- 20121114LC (inizio)
      tauMin = 1
      For tau = tauMin To tauMax
      '--- 20121114LC (fine)
        Print #fo, fd(buf(tau, 1), "  ####"); 'T
        Print #fo, fd(buf(tau, 2), sfL);   'Ns:   PFAM
        Print #fo, fd(buf(tau, 3), sfL);   'Nv:   ZSV
        Print #fo, fd(buf(tau, 4), sfL);   'Nvo:  ZSVO
        Print #fo, fd(buf(tau, 5), sfL);   'Nvoo: ZSVOO
        Print #fo, fd(buf(tau, 6), sfL);   'No:   ZSO
        Print #fo, fd(buf(tau, 7), sfL);   'Noo:  ZSOO
        Print #fo, fd(buf(tau, 8), sfL);   'Nooo: ZSOOO
        Call TAppl_StampaDebug2(appl, appl.tb1, iTS, tau, fo)
      Next tau
      Print #fo, "  ---------------------------------------------------------------------------------"
    ElseIf iTab = REP1_ENT1 And glo.rep_chk(iTab) = vbChecked Then
      '****************************************************************
      'Stampa la tabella EVOLUZIONE REDDITI IRPEF E VOLUME D'AFFARI IVA
      '****************************************************************
      Print #fo,
      Print #fo,
      Print #fo, "  **********************"
      Print #fo, "  EVOLUZIONE PROVVIGIONI"
      Print #fo, "  **********************"
      Print #fo,
      Print #fo, "       ATTIVI E PENSIONATI ATTIVI                                                     SOLO ATTIVI"
      Print #fo, "                        PROVVIGIONE           CONTRIBUTI          PROVV.FINO AL TETTO                  PROVVIGIONE         PROVV.FINO AL TETTO"
      Print #fo, "  ANNO CONTRIBUENTI     TOTALE      MEDIA     TOTALE      MEDIO     TOTALE      MEDIO CONTRIBUENTI     TOTALE      MEDIA     TOTALE      MEDIO"
      Print #fo, "  --------------------------------------------------------------------------------------------------------------------------------------------"
      '--- 20121114LC (inizio)
      tauMin = IIf(OPZ_OUTPUT_MAX = 0, 1, 1961 - appl.t0)
      For tau = tauMin To tauMax
      '--- 20121114LC (fine)
        Print #fo, fd(buf(tau, 1), "  ####"); 'T
        Print #fo, fd(buf(tau, 2), "   " & sfL);   'ZETOT
        Print #fo, fd(buf(tau, 3), sfT0);  'IRPTOT
        Print #fo, fd(buf(tau, 4), sfM);   'IRPMED
        Print #fo, fd(buf(tau, 7), sfT0);  'ETOT
        Print #fo, fd(buf(tau, 8), sfM);   'EMED
        Print #fo, fd(buf(tau, 9), sfT0);  'IRP2TOT - 20080611LC
        Print #fo, fd(buf(tau, 10), sfM);  'IRP2MED - 20080611LC
        '---
        Print #fo, fd(buf(tau, 11), "   " & sfL);   'AA_ZETOT
        Print #fo, fd(buf(tau, 12), sfT0);  'AA_IRPTOT
        Print #fo, fd(buf(tau, 13), sfM);   'AA_IRPMED
        Print #fo, fd(buf(tau, 16), sfT0);  'AA_IRP2TOT
        Print #fo, fd(buf(tau, 17), sfM);  'AA_IRP2MED
        Call TAppl_StampaDebug2(appl, appl.tb1, iTS, tau, fo)
      Next tau
      Print #fo, "  --------------------------------------------------------------------------------------------------------------------------------------------"
      '--- 20121114LC (fine)
      '--- 20140414LC (fine)
    '--- 20150608LC (inizio)
    ElseIf iTab = REP1_ENT2 And glo.rep_chk(iTab) = vbChecked Then
      '***************************************
      'Stampa la tabella EVOLUZIONE CONTRIBUTI
      '***************************************
      For it = 1 To 2
        Print #fo,
        Print #fo,
        If it = 1 Then
          Print #fo, "  *************************************************************"
          Print #fo, "  EVOLUZIONE CONTRIBUTI (ATTIVI, VOLONTARI E PENSIONATI ATTIVI)"
          Print #fo, "  *************************************************************"
        Else
          Print #fo, "  ***********************************************"
          Print #fo, "  EVOLUZIONE CONTRIBUTI (SOLO ATTIVI E VOLONTARI)"
          Print #fo, "  ***********************************************"
        End If
        Print #fo,
         Print #fo, "        CONTRIBUTI SOGGETTIVI                      CONTR.FIRR  CONTR.SOLIDARIETA'                CONTRIB.   CONTRIB."
         Print #fo, "  ANNO  NELL'ANNO ANNO SUCC.    VOLONT.     TOTALE     TOTALE  NELL'ANNO ANNO SUCC.     TOTALE MATERNITA'  SOL.PENS."
         Print #fo, "  ------------------------------------------------------------------------------------------------------------------"
        'Print #fo, "   (T)    (Ecs01)      (Ecs*)   (Ecs02)      (Ecs)      (Eci)     (Eca0)     (Eca1)      (Eca)     (Emat)     (Esol)"
        tauMin = IIf(OPZ_OUTPUT_MAX = 0, 1, 1961 - appl.t0)
        ib = IIf(it = 1, 0, 26)
        For tau = tauMin To tauMax
          Print #fo, fd(buf(tau, 1), "  ####"); 'T
          Print #fo, fd(buf(tau, ib + 3), sfT0);     'ECS01
          Print #fo, fd(buf(tau, ib + 2) + buf(tau, ib + 5) + buf(tau, ib + 6) + buf(tau, ib + 7), sfT0); 'ECSR+ECS1+ECS2+ECS3
          Print #fo, fd(buf(tau, ib + 4), sfT0);     'ECS02
          Print #fo, fd(buf(tau, ib + 8), sfT0);     'ECS
          Print #fo, fd(buf(tau, ib + 14), sfT0);    'ECI=ECIR+ECI1+ECI2+ECI3
          Print #fo, fd(buf(tau, ib + 17), sfT0);    'ECA0
          Print #fo, fd(buf(tau, ib + 16) + buf(tau, ib + 18), sfT0);  'ECAR+ECA1
          Print #fo, fd(buf(tau, ib + 19), sfT0);    'ECA
          Print #fo, fd(buf(tau, ib + 20), sfT0);    'EMAT
          Print #fo, fd(buf(tau, ib + 21), sfT0);    'ECSP
          Call TAppl_StampaDebug2(appl, appl.tb1, iTS, tau, fo)
        Next tau
        Print #fo, "  ------------------------------------------------------------------------------------------------------------------"
      Next it
    '--- 20150608LC (fine)
    ElseIf iTab = REP1_MONT And glo.rep_chk(iTab) = vbChecked Then
      '****************************************************************************
      'Stampa la tabella EVOLUZIONE MONTANTI CONTRIBUTIVI E RESTTTUZIONI CONTRIBUTI
      '****************************************************************************
      Print #fo,
      Print #fo,
      Print #fo, "  **********************************************************"
      Print #fo, "  EVOLUZIONE MONTANTI CONTRIBUTIVI E RESTTTUZIONI CONTRIBUTI"
      Print #fo, "  **********************************************************"
      Print #fo,
      Print #fo, "         MONTANTI DEI CONTRIBUTI"
      '--- 20150612LC-2 (inizio)
      'Print #fo, "  ANNO     TOTALE    QUOTA A    QUOTA B    VOLONT. SUPPL.PENS    QUOTA C    VOLONT.       FIRR"
      'Print #fo, "  --------------------------------------------------------------------------------------------"
      Print #fo, "  ANNO CONTRIBUTI       FIRR"
      Print #fo, "  --------------------------"
      '--- 20150612LC-2 (fine)
      tauMin = IIf(OPZ_OUTPUT_MAX = 0, 0, 1961 - appl.t0)  '20160611LC (anche annoBT)
      For tau = tauMin To tauMax
        Print #fo, fd(buf(tau, 1), "  ####"); 'T
        Print #fo, fd(buf(tau, 2), sfT0);   'OMRC
        '--- 20150612LC-2 (inizio)
        Print #fo, fd(buf(tau, 3), sfT0);   'OMRC6
        '--- 20150612LC-2 (fine)
        Call TAppl_StampaDebug2(appl, appl.tb1, iTS, tau, fo)
      Next tau
      Print #fo, "  --------------------------"
    ElseIf iTab = REP1_USC1 And glo.rep_chk(iTab) = vbChecked Then
      '*************************************
      'Stampa la tabella EVOLUZIONE PENSIONI
      '*************************************
      Print #fo,
      Print #fo,
      Print #fo, "  *******************"
      Print #fo, "  EVOLUZIONE PENSIONI"
      Print #fo, "  *******************"
      Print #fo,
      Print #fo, "         PENSIONATI NON CONTRIBUENTI     PENSIONATI CONTRIBUENTI         TUTTI I PENSIONATI"
      Print #fo, "  ANNO   N�PENS.  PENS.TOT.  PENS.MED.   N�PENS.  PENS.TOT.  PENS.MED.   N�PENS.  PENS.TOT.  PENS.MED."
     'Print #fo, "   (T)        ()        ()        ()        ()        ()        ()        ()        ()        ()"
      Print #fo, "  ----------------------------------------------------------------------------------------------------"
      '--- 20121114LC (inizio)
      tauMin = 1
      For tau = tauMin To tauMax
      '--- 20121114LC (fine)
        Print #fo, fd(buf(tau, 1), "  ####"); 'T
        'PENSIONATI NON CONTRIBUENTI
        Print #fo, fd(buf(tau, 2), sfL);   'N� PENS.
        Print #fo, fd(buf(tau, 3), sfT0);  'PENS.TOT.
        Print #fo, fd(buf(tau, 4), sfM);   'PENS.MED.
        'PENSIONATI CONTRIBUENTI
        Print #fo, fd(buf(tau, 5), sfL);   'N� PENS.
        Print #fo, fd(buf(tau, 6), sfT0);  'PENS.TOT.
        Print #fo, fd(buf(tau, 7), sfM);   'PENS.MED.
        'TUTTI I PENSIONATI
        Print #fo, fd(buf(tau, 8), sfL);   'N� PENS.
        Print #fo, fd(buf(tau, 9), sfT0);  'PENS.TOT.
        Print #fo, fd(buf(tau, 10), sfM);  'PENS.MED.
        Call TAppl_StampaDebug2(appl, appl.tb1, iTS, tau, fo)
      Next tau
      Print #fo, "  ----------------------------------------------------------------------------------------------------"
    ElseIf iTab = REP1_USC2 And glo.rep_chk(iTab) = vbChecked Then
      '********************************************************
      'Stampa la tabella EVOLUZIONE PENSIONATI NON CONTRIBUENTI
      '********************************************************
      Print #fo,
      Print #fo,
      Print #fo, "  **************************************"
      Print #fo, "  EVOLUZIONE PENSIONATI NON CONTRIBUENTI"
      Print #fo, "  **************************************"
      Print #fo,
      Print #fo, "         VECCHIAIA                       VECCHIAIA ANTICIPATA            VECCHIAIA POSTICIPATA           CONTRIBUTIVA                 "
      Print #fo, "  ANNO   N�PENS.     TOTALE      MEDIA   N�PENS.     TOTALE      MEDIA   N�PENS.     TOTALE      MEDIA   N�PENS.     TOTALE      MEDIA"
     'Print #fo, "   (T)    (Npdv)   (PdvTot)   (PdvMed)    (Npdv)   (PdvTot)   (PdvMed)    (Npdv)   (PdvTot)   (PdvMed)    (Npds)   (PdsTot)   (PdsMed)"
      Print #fo, "  ------------------------------------------------------------------------------------------------------------------------------------"
      '--- 20121114LC (inizio)
      tauMin = 1
      For tau = tauMin To tauMax
      '--- 20121114LC (fine)
        Print #fo, fd(buf(tau, 1), "  ####"); 'T
        '--- Vecchiaia ---
        Print #fo, fd(buf(tau, 2), sfL);   'N� PENS.
        Print #fo, fd(buf(tau, 3), sfT0);  'PENS.TOT.
        Print #fo, fd(buf(tau, 4), sfM);   'PENS.MED.
        '--- Vecchiaia anticipata ---
        Print #fo, fd(buf(tau, 5), sfL);   'N� PENS.
        Print #fo, fd(buf(tau, 6), sfT0);  'PENS.TOT.
        Print #fo, fd(buf(tau, 7), sfM);   'PENS.MED.
        '--- Vecchiaia posticipata ---
        Print #fo, fd(buf(tau, 8), sfL);   'N� PENS.
        Print #fo, fd(buf(tau, 9), sfT0);  'PENS.TOT.
        Print #fo, fd(buf(tau, 10), sfM);  'PENS.MED.
        '--- Anzianit� ---
        '--- Sostitutiva ---
        Print #fo, fd(buf(tau, 14), sfL);  'N� PENS.
        Print #fo, fd(buf(tau, 15), sfT0); 'PENS.TOT.
        Print #fo, fd(buf(tau, 16), sfM);  'PENS.MED.
        '---
        Call TAppl_StampaDebug2(appl, appl.tb1, iTS, tau, fo)
      Next tau
      Print #fo, "  ------------------------------------------------------------------------------------------------------------------------------------"
      Print #fo,
      Print #fo, "         INABILITA'                      INVALIDITA'                     SUPERSTITI"
      Print #fo, "  ANNO   N�PENS.     TOTALE      MEDIA   N�PENS.     TOTALE      MEDIA   N�PENS.     TOTALE      MEDIA"
     'Print #fo, "   (T)     (Npi)    (PiTot)    (PiMed)     (Npb)    (PbTot)    (PbMed)     (Nps)    (PsTot)    (PsMed)"
      Print #fo, "  ----------------------------------------------------------------------------------------------------"
      For tau = tauMin To tauMax  '20121114LC
        Print #fo, fd(buf(tau, 1), "  ####"); 'T
        '--- Totalizzazione ---
        '--- Invalidit� ---
        Print #fo, fd(buf(tau, 20), sfL);  'N� PENS.
        Print #fo, fd(buf(tau, 21), sfT0);  'PENS.TOT.
        Print #fo, fd(buf(tau, 22), sfM);  'PENS.MED.
        '--- Inabilit� ---
        Print #fo, fd(buf(tau, 23), sfL);  'N� PENS.
        Print #fo, fd(buf(tau, 24), sfT0);  'PENS.TOT.
        Print #fo, fd(buf(tau, 25), sfM);  'PENS.MED.
        '--- Superstiti ---
        Print #fo, fd(buf(tau, 26), sfL);  'N� PENS.
        Print #fo, fd(buf(tau, 27), sfT0);  'PENS.TOT.
        Print #fo, fd(buf(tau, 28), sfM);  'PENS.MED.
        '---
        Call TAppl_StampaDebug2(appl, appl.tb1, iTS, tau, fo)
      Next tau
      Print #fo, "  ----------------------------------------------------------------------------------------------------"
    ElseIf iTab = REP1_BIL_TECN_MIN And glo.rep_chk(iTab) = vbChecked Then  '20160419LC-2
      '*************************************************
      'Stampa la tabella BILANCIO TECNICO (ministeriale)
      '*************************************************
      '--- 20140111LC-17 (inizio)
      sfT0 = " ##########"      '10
      sfT1 = " ###########"     '11
      sfT2 = " ############"    '12
      sfT3 = " #############"   '13
      sfT4 = " ##############"  '14
      sfp = " #" & Right(sfT0, Len(sfT0) - 1)
      Print #fo,
      Print #fo,
      Print #fo, "  *******************************"
      Print #fo, "  BILANCIO TECNICO (ministeriale)"
      Print #fo, "  *******************************"
      Print #fo,
      '--- 20140603LC (inizio)  'ex 20140409LC  'ex 20131122LC
      Print #fo, "        CONTRIBUTI CONTRIBUTI      ALTRI                   ALTRE       TOTALE    PRESTAZ.      ALTRE      ALTRE   SPESE DI      TOTALE        SALDO        SALDO     PATRIMONIO TASSO        RISERVA    PENSIONI    PENSIONI    PENSIONI    PENSIONI    PENSIONI  CONTRIBUTI"
      Print #fo, "  ANNO  SOGGETTIVI  INTEGRAT. CONTRIBUTI   RENDIMENTI    ENTRATE      ENTRATE    PENSION.   PRESTAZ.     USCITE   GESTIONE      USCITE      PREVID.       TOTALE         FINALE REND.         LEGALE  INABILITA'   INVAL.TA'  SUPERSTITI    INT.MIN.  CONTR.FIG.  DI SOL.TA'"
      '                            2          3          4            5          6            7           8          9         10         11          12           13           14             15    16           15-8          17          18          19          20          21          22
      Print #fo, "  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
      '--- 20140603LC (fine)
      '--- 20121114LC (inizio)
      tauMin = 1
      For tau = tauMin To tauMax
      '--- 20121114LC (fine)
        Print #fo, fd(buf(tau, 1), "  ####"); '1 ANNO
        If tau = 0 Then
          Print #fo, Space(Len(sfT0) * 12 + Len(sfp) + 4); '20080710LC
          Print #fo, fd(buf(tau, 15), sfT0);  '15 PATRIMONIO FINALE
          '20080614LC
          Print #fo,
        Else
          Print #fo, fd(buf(tau, 2), sfT1);   ' 2 CONTRIBUTI SOGGETTIVI
          Print #fo, fd(buf(tau, 3), sfT0);   ' 3 CONTRIBUTI INTEGRATIVI
          Print #fo, fd(buf(tau, 4), sfT0);   ' 4 ALTRI CONTRIBUTI
          Print #fo, fd(buf(tau, 5), sfT2);   ' 5 RENDIMENTI
          Print #fo, fd(buf(tau, 6), sfT0);   ' 6 ALTRE ENTRATE
          Print #fo, fd(buf(tau, 7), sfT2);   ' 7 TOTALE ENTRATE
          Print #fo, fd(buf(tau, 8), sfT1);   ' 8 PRESTAZIONI PENSIONISTICHE
          Print #fo, fd(buf(tau, 9), sfT0);   ' 9 ALTRE PRESTAZIONI
          Print #fo, fd(buf(tau, 10), sfT0);  '10 ALTRE USCITE
          Print #fo, fd(buf(tau, 11), sfT0);  '11 SPESE DI GESTIONE
          Print #fo, fd(buf(tau, 12), sfT1);  '12 TOTALE USCITE
          Print #fo, fd(buf(tau, 13), sfT2);  '13 SALDO PREVIDENZIALE
          Print #fo, fd(buf(tau, 14), sfT2);  '14 SALDO CORRENTE
          Print #fo, fd(buf(tau, 15), sfT4);  '15 PATRIMONIO FINALE
          '--- 20140404LC (inizio)  'ex 20131126LC
          Print #fo, fd(buf(tau, 16) * 100, " 0.00%"); '16 RENDIMENTO
          Print #fo, fd(buf(tau, 15) - 5 * buf(tau, 8), sfT4);   '  RISERVA LEGALE
          Print #fo, fd(buf(tau, 17), sfT1);  'PENSIONI INABILITA'
          Print #fo, fd(buf(tau, 18), sfT1);  'PENSIONI INVALIDITA'
          Print #fo, fd(buf(tau, 19), sfT1);  'PENSIONI SUPERSTITI
          Print #fo, fd(buf(tau, 20), sfT1);  'PENSIONI INTEGR.MIN.
          Print #fo, fd(buf(tau, 21), sfT1);  'PENSIONI CONTRIB.FIG.
          Print #fo, fd(buf(tau, 22), sfT1);  'CONTRIBUTI DI SOLIDARIETA'
          '--- 20140404LC (fine)
          '20080614LC (spostato)
          Call TAppl_StampaDebug2(appl, appl.tb1, iTS, tau, fo)
        End If
      Next tau
      Print #fo, "  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
      '--- 20140111LC-17 (fine)
    ElseIf iTab = REP1_BIL_FIRR And glo.rep_chk(iTab) = vbChecked Then
      '*******************************
      'Stampa la tabella BILANCIO FIRR
      '*******************************
      '--- 20140111LC-17 (inizio)
      sfT0 = " ##########"      '10
      sfT1 = " ###########"     '11
      sfT2 = " ############"    '12
      sfT3 = " #############"   '13
      sfT4 = " ##############"  '14
      sfp = " #" & Right(sfT0, Len(sfT0) - 1)
      '--- 20140111LC-17 (fine)
      Print #fo,
      Print #fo,
      Print #fo, "  *************"
      Print #fo, "  BILANCIO FIRR"
      Print #fo, "  *************"
      Print #fo,
      Print #fo, "          MONT.FIRR                                                    MONT.FIRR      "
      Print #fo, "  ANNO     INIZIALE     ENTRATE      USCITE   USC.S.SUP       SALDO       FINALE  REND"
      '                             2           3           4           5           6            7     8'
      Print #fo, "  ------------------------------------------------------------------------------------"
      tauMin = 1
      For tau = tauMin To tauMax
        Print #fo, fd(buf(tau, 1), "  ####");          'T
        Print #fo, fd(buf(tau, 2), sfT2);  'MONT.FIRR INIZIALE
        Print #fo, fd(buf(tau, 3), sfT1);  'ENTRATE
        Print #fo, fd(buf(tau, 4), sfT1);  'USCITE
        Print #fo, fd(buf(tau, 5), sfT1);  'USC.S.SUP
        Print #fo, fd(buf(tau, 6), sfT1);  'INTERESSI
        Print #fo, fd(buf(tau, 7), sfT2);  'MONT.FIRR FINALE
        Print #fo, fd(buf(tau, 8) * 100, " #.##%"); 'RENDIMENTO
        Call TAppl_StampaDebug2(appl, appl.tb1, iTS, tau, fo)
      Next tau
      Print #fo, "  ------------------------------------------------------------------------------------"
    '20080611LC (inizio)
    ElseIf iTab = REP1_VAL_CAP And glo.rep_chk(iTab) = vbChecked Then
    '20080611LC (fine)
      '*********************************
      'Stampa la tabella VALORI CAPITALI
      '*********************************
      '--- 20140111LC-17 (inizio)
      sfT0 = " ##########"      '10
      sfT1 = " ###########"     '11
      sfT2 = " ############"    '12
      sfT3 = " #############"   '13
      sfT4 = " ##############"  '14
      y = buf(2)
      If y <= 0 Then y = 1
      Print #fo,
      Print #fo,
      Print #fo, "  *****************************************"
      Print #fo, "  Valori capitali totali e medi al 1/1/" & Format(Year(appl.DataBilancio) + 1, "00")
      Print #fo, "  *****************************************"
      Print #fo,
      Print #fo, "                      Contributi     Pensioni     Pensioni      dirette      dirette     Pensioni     Pensioni                          "
      Print #fo, "  Anno     Iscritti    e versam.    inabilit�   invalidit�     contrib.   non contr.    reversib.       totali       Uscite        Saldi"
     'Print #fo, "   (T)      ( NTOT)      (AETOT)     (APBTOT)     (APITOT)    (APDCTOT)    (APDNTOT)     (APSTOT)      (APTOT)      (AUTOT)      (ASTOT)"
      Print #fo, "  --------------------------------------------------------------------------------------------------------------------------------------"
      Print #fo, fd(buf(1), "  ####");  'T
      Print #fo, fd(buf(2), sfT2);      'NTOT
      Print #fo, fd(buf(3), sfT2);      'AETOT
      Print #fo, fd(buf(4), sfT2);      'APBTOT
      Print #fo, fd(buf(5), sfT2);      'APITOT
      Print #fo, fd(buf(6), sfT2);      'APDCTOT
      Print #fo, fd(buf(7), sfT2);      'APDNTOT
      Print #fo, fd(buf(8), sfT2);      'APSTOT
      Print #fo, fd(buf(9), sfT2);      'APTOT
      Print #fo, fd(buf(11), sfT2);     'AUTOT
      Print #fo, fd(buf(12), sfT2);     'ASTOT
      Print #fo,
      Print #fo, Space(Len("  ####"));  'T
      Print #fo, Space(Len(sfT2));      'NTOT
      Print #fo, fd(buf(3) / y, sfT2);  'AETOT
      Print #fo, fd(buf(4) / y, sfT2);  'APBTOT
      Print #fo, fd(buf(5) / y, sfT2);  'APITOT
      Print #fo, fd(buf(6) / y, sfT2);  'APDCTOT
      Print #fo, fd(buf(7) / y, sfT2);  'APDNTOT
      Print #fo, fd(buf(8) / y, sfT2);  'APSTOT
      Print #fo, fd(buf(9) / y, sfT2);  'APTOT
      Print #fo, fd(buf(11) / y, sfT2); 'AUTOT
      Print #fo, fd(buf(12) / y, sfT2); 'ASTOT
      Print #fo,
      Print #fo, "  --------------------------------------------------------------------------------------------------------------------------------------"
      '---
      Print #fo,
      Print #fo, "         Redd. fino    Dettaglio pensioni ai non contribuenti"
      Print #fo, "  Anno     al tetto    vecchiaia   vecch.ant.  vecch.post.   contrib.va"
     'Print #fo, "   (T)    (AIRPEF2)   (APDVOTOT)   (APDVATOT)   (APDVPTOT)    (APDSTOT)
      Print #fo, "  ----------------------------------------------------------------------"
      Print #fo, fd(buf(1), "  ####");    'T
      Print #fo, fd(buf(13), sfT2);       '13 IRPEF2  redditi fino al tetto reddituale
      Print #fo, fd(buf(14), sfT2);       '14 PDVOTOT vecchiaia ordinaria
      Print #fo, fd(buf(15), sfT2);       '15 PDVATOT vecchiaia anticipata
      Print #fo, fd(buf(16), sfT2);       '16 PDVPTOT vecchiaia posticipata
      Print #fo, fd(buf(18), sfT2);     '18 PDSTOT  sostitutiva
      Print #fo,
      Print #fo, Space(Len("  ####"));    't
      Print #fo, fd(buf(13) / y, sfT2);   '13 IRPEF2  redditi fino al tetto reddituale
      Print #fo, fd(buf(14) / y, sfT2);   '14 PDVOTOT vecchiaia ordinaria
      Print #fo, fd(buf(15) / y, sfT2);   '15 PDVATOT vecchiaia anticipata
      Print #fo, fd(buf(16) / y, sfT2);   '16 PDVPTOT vecchiaia posticipata
      Print #fo, fd(buf(18) / y, sfT2); '18 PDSTOT  sostitutiva
      Print #fo,
      Print #fo, "  ----------------------------------------------------------------------"
      '20080616LC (fine)
      '--- 20140111LC-17 (fine)
    End If
  End If
  '********
  'File XLS
  '********
  If iop <> 1 And Not xlsApp Is Nothing Then
    '--- 20150512LC (inizio)
    Select Case iTab
    Case REP1_POP
      '***********
      'Popolazione
      '***********
      sz = ""
      tauMin = 1
      For iRiga = tauMin To UBound(buf, 1)
        s = buf(iRiga, 1)
        For iCol = 2 To UBound(buf, 2)
          If (iCol = 9 Or iCol = 11) Then
          Else
            s = s & vbTab & buf(iRiga, iCol)
          End If
        Next iCol
        sz = sz & s & vbCrLf
      Next iRiga
'Set xlsWsh = xlsApp.ActiveWorkbook.Worksheets("Sviluppo Coll.")
      xlsApp.Sheets("Sviluppo Coll.").Select
      '--- 20161112LC (inizio)
      'xlsApp.Range("B9").Select
      sErr = xlsPaste(xlsApp, sz, "B9")
      If sErr <> "" Then GoTo ExitPoint
      '--- 20161112LC (fine)
    Case REP1_SUP
      '**********
      'Superstiti
      '**********
      sz = ""
      tauMin = 1
      For iRiga = tauMin To UBound(buf, 1)
        s = buf(iRiga, 1)
        For iCol = 2 To UBound(buf, 2)
          s = s & vbTab & buf(iRiga, iCol)
        Next iCol
        sz = sz & s & vbCrLf
      Next iRiga
'Set xlsWsh = xlsApp.ActiveWorkbook.Worksheets("Deceduti")
      xlsApp.Sheets("Deceduti").Select
      '--- 20161112LC (inizio)
      'xlsApp.Range("B9").Select
      sErr = xlsPaste(xlsApp, sz, "B9")
      If sErr <> "" Then GoTo ExitPoint
      '--- 20161112LC (fine)
    Case REP1_ENT1
      '*****************************************************
      'Evoluzione redditi IRPEF e volume d'affari IVA / FIRR
      '*****************************************************
      sz = ""
      tauMin = IIf(OPZ_OUTPUT_MAX = 0, 1, 1961 - appl.t0)
      For iRiga = tauMin To UBound(buf, 1)
        s = buf(iRiga, 1)
        For iCol = 2 To UBound(buf, 2)
          If (iCol = 5 Or iCol = 6 Or iCol = 14 Or iCol = 15) Then '20150512LC
          Else
            s = s & vbTab & buf(iRiga, iCol)
          End If
        Next iCol
        sz = sz & s & vbCrLf
      Next iRiga
      xlsApp.Sheets("Provvigioni").Select
      '--- 20161112LC (inizio)
      'xlsApp.Range("B9").Select
      sErr = xlsPaste(xlsApp, sz, "B9")
      If sErr <> "" Then GoTo ExitPoint
      '--- 20161112LC (fine)
    '--- 20150608LC (inizio)  'ex 20140414LC
    Case REP1_ENT2
      '**************************************************
      'EVOLUZIONE CONTRIBUTI (ATTIVI E PENSIONATI ATTIVI)
      'EVOLUZIONE CONTRIBUTI (SOLO ATTIVI)
      '**************************************************
      For it = 1 To 2
        sz = ""
        tauMin = IIf(OPZ_OUTPUT_MAX = 0, 1, 1961 - appl.t0)
        ib = IIf(it = 1, 0, 26)
        For iRiga = tauMin To UBound(buf, 1)
          s = buf(iRiga, 1)  '20150616LC
          s = s & vbTab & buf(iRiga, ib + 3)   'ECS01
          '20150604LC
          s = s & vbTab & buf(iRiga, ib + 2) + buf(iRiga, ib + 5) + buf(iRiga, ib + 6) + buf(iRiga, ib + 7)  'ECSR+ECS1+ECS2+ECS3
          s = s & vbTab & buf(iRiga, ib + 4)   'ECS02
          s = s & vbTab & buf(iRiga, ib + 8)   'ECS=ECS01+ECSR+ECS1+ECS2+ECS3+ECS02
          s = s & vbTab & buf(iRiga, ib + 14)  'ECI=ECI0+ECIR+ECI1+ECI2+ECI3
          's = s & vbTab & buf(iRiga, ib + 15)  'ETOT=ECS+ECI
          s = s & vbTab & buf(iRiga, ib + 17)  'ECA0
          s = s & vbTab & (buf(iRiga, ib + 16) + buf(iRiga, ib + 18)) 'ECAR+ECA1
          s = s & vbTab & buf(iRiga, ib + 19)  'ECA=ECAR+ECA0+ECA1
          s = s & vbTab & buf(iRiga, ib + 20)  'EMAT
          s = s & vbTab & buf(iRiga, ib + 21)  'ECSP
          sz = sz & s & vbCrLf
        Next iRiga
        xlsApp.Sheets(IIf(it = 1, "Contributi", "ContributiA")).Select
        '--- 20161112LC (inizio)
        'xlsApp.Range("B9").Select
        sErr = xlsPaste(xlsApp, sz, "B9")
        If sErr <> "" Then GoTo ExitPoint
        '--- 20161112LC (fine)
      Next it
    '--- 20150608LC (fine)  'ex 20140414LC
    '--- 20150512LC (fine)
    Case REP1_MONT
      '********
      'Montanti
      '********
      sz = ""
      tauMin = IIf(OPZ_OUTPUT_MAX = 0, 0, 1961 - appl.t0)  '20160611LC (anche annoBT)
      For iRiga = tauMin To UBound(buf, 1)
        s = buf(iRiga, 1)
        For iCol = 2 To UBound(buf, 2)
          s = s & vbTab & buf(iRiga, iCol)
        Next iCol
        sz = sz & s & vbCrLf
      Next iRiga
      xlsApp.Sheets("Montanti").Select
      '--- 20161112LC (inizio)
      'xlsApp.Range("B9").Select
      sErr = xlsPaste(xlsApp, sz, "B9")
      If sErr <> "" Then GoTo ExitPoint
      '--- 20161112LC (fine)
    '--- 20150512LC (fine)
    Case REP1_USC1
      '***********************
      'Pensionati contribuenti
      '***********************
      sz = ""
      tauMin = 1
      For iRiga = tauMin To UBound(buf, 1)
        s = buf(iRiga, 1)
        s = s & vbTab & buf(iRiga, 2)
        s = s & vbTab & buf(iRiga, 3)
        s = s & vbTab & buf(iRiga, 4)
        s = s & vbTab & buf(iRiga, 5)
        s = s & vbTab & buf(iRiga, 6)
        s = s & vbTab & buf(iRiga, 7)
        s = s & vbTab & buf(iRiga, 8)
        s = s & vbTab & buf(iRiga, 9)
        s = s & vbTab & buf(iRiga, 10)
        sz = sz & s & vbCrLf
      Next iRiga
      xlsApp.Sheets("Ev. Pens.").Select
      '--- 20161112LC (inizio)
      'xlsApp.Range("B9").Select
      sErr = xlsPaste(xlsApp, sz, "B9")
      If sErr <> "" Then GoTo ExitPoint
      '--- 20161112LC (fine)
    Case REP1_USC2
      '***************************
      'Pensionati non contribuenti
      '***************************
      sz = ""
      tauMin = 1
      For iRiga = tauMin To UBound(buf, 1)
        s = buf(iRiga, 1)
        '--- Vecchiaia ---
        s = s & vbTab & buf(iRiga, 2)
        s = s & vbTab & buf(iRiga, 3)
        s = s & vbTab & buf(iRiga, 4)
        '--- Vecchiaia anticipata ---
        s = s & vbTab & buf(iRiga, 5)
        s = s & vbTab & buf(iRiga, 6)
        s = s & vbTab & buf(iRiga, 7)
        '--- Vecchiaia posticipata ---
        s = s & vbTab & buf(iRiga, 8)
        s = s & vbTab & buf(iRiga, 9)
        s = s & vbTab & buf(iRiga, 10)
        '--- Anzianit� ---
        '--- Sostitutiva ---
        s = s & vbTab & buf(iRiga, 14)
        s = s & vbTab & buf(iRiga, 15)
        s = s & vbTab & buf(iRiga, 16)
        sz = sz & s & vbCrLf
      Next iRiga
      xlsApp.Sheets("Ev. Pens. N.C.1").Select
      '--- 20161112LC (inizio)
      'xlsApp.Range("B9").Select
      sErr = xlsPaste(xlsApp, sz, "B9")
      If sErr <> "" Then GoTo ExitPoint
      '--- 20161112LC (fine)
      '--- 20140414LC
      sz = ""
      tauMin = 1
      For iRiga = tauMin To UBound(buf, 1)
        s = buf(iRiga, 1)
        '--- Totalizzazione ---
        '--- Inabilit� ---
        s = s & vbTab & buf(iRiga, 20)
        s = s & vbTab & buf(iRiga, 21)
        s = s & vbTab & buf(iRiga, 22)
        '--- Invalidit� ---
        s = s & vbTab & buf(iRiga, 23)
        s = s & vbTab & buf(iRiga, 24)
        s = s & vbTab & buf(iRiga, 25)
        '--- Superstiti ---
        s = s & vbTab & buf(iRiga, 26)
        s = s & vbTab & buf(iRiga, 27)
        s = s & vbTab & buf(iRiga, 28)
        sz = sz & s & vbCrLf
      Next iRiga
      xlsApp.Sheets("Ev. Pens. N.C.2").Select
      '--- 20161112LC (inizio)
      'xlsApp.Range("B9").Select
      sErr = xlsPaste(xlsApp, sz, "B9")
      If sErr <> "" Then GoTo ExitPoint
      '--- 20161112LC (fine)
    Case REP1_BIL_TECN_MIN  '20160419LC-2
      '***********************
      'Bilancio (ministeriale)
      '***********************
      sz = ""
      tauMin = 0
      For iRiga = tauMin To UBound(buf, 1)
        r = iRiga + 8
        s = buf(iRiga, 1) ' 1 ANNO
        If iRiga = 0 Then
          s = s & vbTab   ' 2 CONTRIBUTI SOGGETTIVI
          s = s & vbTab   ' 3
          s = s & vbTab   ' 4
          s = s & vbTab   ' 5
          s = s & vbTab   ' 6
          s = s & vbTab   ' 7
          s = s & vbTab   ' 8
          s = s & vbTab   ' 9
          s = s & vbTab   '10
          s = s & vbTab   '11
          s = s & vbTab   '12
          s = s & vbTab   '13
          s = s & vbTab   '14
          s = s & vbTab & buf(iRiga, 15) '15 PATRIMONIO FINALE
          s = s & vbTab   '16
          s = s & vbTab   'RISERVA LEGALE
          s = s & vbTab   'PENSIONI INABILITA'
          s = s & vbTab   'PENSIONI INVALIDITA'
          s = s & vbTab   'PENSIONI SUPERSTITI
          s = s & vbTab                                                         'CONTRIBUTI DI SOL.TA'  '20140409LC
        Else
          s = s & vbTab & buf(iRiga, 2)                                         ' 2 CONTRIBUTI SOGGETTIVI
          s = s & vbTab & buf(iRiga, 3)                                         ' 3 CONTRIBUTI INTEGRATIVI
          s = s & vbTab & buf(iRiga, 4)                                         ' 4 ALTRI CONTRIBUTI
          s = s & vbTab & "=(P" & r - 1 & "+(C" & r & "+D" & r & "+E" & r & "-M" & r & ")/2)*Q" & r ' 5 RENDIMENTI
          s = s & vbTab & buf(iRiga, 6)  ' 6 ALTRE ENTRATE
          s = s & vbTab & "=C" & r & "+D" & r & "+E" & r & "+F" & r & "+G" & r  ' 7 TOTALE ENTRATE
          s = s & vbTab & buf(iRiga, 8)                                         ' 8 PRESTAZIONI PENSIONISTICHE
          s = s & vbTab & buf(iRiga, 9)                                         ' 9 ALTRE PRESTAZIONI
          s = s & vbTab & buf(iRiga, 10)                                        '10 ALTRE USCITE
          s = s & vbTab & buf(iRiga, 11)                                        '11 SPESE DI GESTIONE
          s = s & vbTab & "=I" & r & "+J" & r & "+K" & r & "+L" & r             '12 TOTALE USCITE
          s = s & vbTab & "=C" & r & "+D" & r & "+E" & r & "-I" & r & "-J" & r  '13 SALDO PREVIDENZIALE  '20080710LC
          s = s & vbTab & "=H" & r & "-M" & r                                   '14 SALDO CORRENTE
          s = s & vbTab & "=P" & r - 1 & "+O" & r                               '15 PATRIMONIO FINALE
          s = s & vbTab & TassoZCB(appl.CoeffVar, appl.t0 + iRiga, cv_Ti)       '16 RENDIMENTO  '20121209LC
          s = s & vbTab & "=P" & r & "-5*I" & r                                 'RISERVA LEGALE
          s = s & vbTab & buf(iRiga, 17)                                        'PENSIONI INABILITA'
          s = s & vbTab & buf(iRiga, 18)                                        'PENSIONI INVALIDITA'
          s = s & vbTab & buf(iRiga, 19)                                        'PENSIONI SUPERSTITI
          s = s & vbTab & buf(iRiga, 22)                                        'CONTRIBUTI DI SOL.TA'  '20140409LC
        End If
        sz = sz & s & vbCrLf
      Next iRiga
      xlsApp.Sheets("Bil_prev").Select  '20160502LC
      '--- 20161112LC (inizio)
      'xlsApp.Range("B8").Select
      sErr = xlsPaste(xlsApp, sz, "B8")
      If sErr <> "" Then GoTo ExitPoint
      '--- 20161112LC (fine)
    Case REP1_BIL_FIRR  '20160419LC-2
      '*************
      'Bilancio FIRR  '20160419LC-2
      '*************
      sz = ""
      tauMin = 1
      For iRiga = tauMin To UBound(buf, 1)
        r = iRiga + 8
        s = buf(iRiga, 1)                                             'B Anno
        '--- 20160611LC (inizio)
        For iCol = 2 To UBound(buf, 2)
          s = s & vbTab & buf(iRiga, iCol)
        Next iCol
        '--- 20160611LC (fine)
        sz = sz & s & vbCrLf
      Next iRiga
      xlsApp.Sheets("Bil_FIRR").Select  '20160419LC-2
      '--- 20161112LC (inizio)
      'xlsApp.Range("B9").Select
      sErr = xlsPaste(xlsApp, sz, "B9")
      If sErr <> "" Then GoTo ExitPoint
      '--- 20161112LC (fine)
    Case REP1_VAL_CAP
      '***************
      'Valori capitali
      '***************
      y = buf(2)
      If y <= AA0 Then
        y = 1#
      End If
      sz = ""
      s = buf(1)                  ' 1         Anno
      s = s & vbTab & buf(2)      ' 2 NTOT    Iscritti
      s = s & vbTab & buf(3)      ' 3 AETOT   Contributi e versamenti
      s = s & vbTab & buf(4)      ' 4 APBTOT  Pensioni inabilit�
      s = s & vbTab & buf(5)      ' 5 APITOT  Pensioni invalidit�
      s = s & vbTab & buf(6)      ' 6 APDCTOT Pensioni dir. contrib.
      s = s & vbTab & buf(7)      ' 7 APDNTOT Pensioni dir. non contr.
      s = s & vbTab & buf(8)      ' 8 APSTOT  Pensioni reversibilit�
      s = s & vbTab & buf(9)      ' 9 APTOT   Pensioni totali
      s = s & vbTab & buf(11)     '11 AUTOT   Uscite
      s = s & vbTab & buf(12)     '12 ASTOT   Saldi
      sz = sz & s & vbCrLf
      '---
      s = ""
      s = s & vbTab & buf(2) / y  ' 2 NTOT    Iscritti
      s = s & vbTab & buf(3) / y  ' 3 AETOT   Contributi e versamenti
      s = s & vbTab & buf(4) / y  ' 4 APBTOT  Pensioni inabilit�
      s = s & vbTab & buf(5) / y  ' 5 APITOT  Pensioni invalidit�
      s = s & vbTab & buf(6) / y  ' 6 APDCTOT Pensioni dir. contrib.
      s = s & vbTab & buf(7) / y  ' 7 APDNTOT Pensioni dir. non contr.
      s = s & vbTab & buf(8) / y  ' 8 APSTOT  Pensioni reversibilit�
      s = s & vbTab & buf(9) / y  ' 9 APTOT   Pensioni totali
      s = s & vbTab & buf(11) / y '11 AUTOT   Uscite
      s = s & vbTab & buf(12) / y '12 ASTOT   Saldi
      sz = sz & s & vbCrLf
      '---
      xlsApp.Sheets("Val. Capitali").Select
      'xlsApp.Range("B9").Select  '20161112LC (commentayo)
      sErr = xlsPaste(xlsApp, sz, "B9")
      If sErr <> "" Then GoTo ExitPoint
      '---
      sz = ""
      s = buf(1)                  '1          Anno
      s = s & vbTab & buf(13)     '13 IRPEF2  Redditi fino al tetto reddituale
      s = s & vbTab & buf(14)     '14 PDVOTOT vecchiaia ordinaria
      s = s & vbTab & buf(15)     '15 PDVATOT vecchiaia anticipata
      s = s & vbTab & buf(16)     '16 PDVPTOT vecchiaia posticipata
      s = s & vbTab & buf(18)     '18 PDSTOT  sostitutiva
      sz = sz & s & vbCrLf
      '---
      s = ""
      s = s & vbTab & buf(13) / y '13 IRPEF2  Redditi fino al tetto reddituale
      s = s & vbTab & buf(14) / y '14 PDVOTOT vecchiaia ordinaria
      s = s & vbTab & buf(15) / y '15 PDVATOT vecchiaia anticipata
      s = s & vbTab & buf(16) / y '16 PDVPTOT vecchiaia posticipata
      s = s & vbTab & buf(18) / y '18 PDSTOT  sostitutiva
      sz = sz & s & vbCrLf
      'xlsApp.Sheets("Val. Capitali").Select
      '--- 20161112LC (inizio)
      'xlsApp.Range("B16").Select
      sErr = xlsPaste(xlsApp, sz, "B16")
      If sErr <> "" Then GoTo ExitPoint
      '--- 20161112LC (fine)
    End Select
  End If
ExitPoint:
  TAppl_Stampa3c_StampaDati_B = sErr
End Function


'20121028LC  'ex 20080614LC
Private Sub TAppl_StampaDebug2(ByRef appl As TAppl, tb1#(), iTS&, tau&, fo&)
  Dim ijk0&
  Dim h#, za#, zea#
  
  If appl.nMatr = 1 Then
    ijk0 = appl.nSize2 * iTS + appl.nSize3 * 0 + appl.nSize4 * (tau - 1 + appl.t0 - 1960) '20121114LC '27
    za = Math.Abs(tb1(ijk0 + TB_ZAA))
    zea = Math.Abs(tb1(ijk0 + TB_ZEA))
    Print #fo, fd(appl.xa + tau, " ###");
    If appl.bEA = False Then
      '20120110LC (inizio)
      If za > AA0 Then
        h = appl.h + tau - IIf(appl.h < 0, 1, 0)
        Print #fo, fd(h, " ###");
      '20120329LC
      'ElseIf OPZ_MISTO_REDD_PENS = 2 And math.Abs(tb(iTS, 0, tau - 1, TB_ZEA)) > AA0 Then
      ElseIf OPZ_MISTO_DEB_MAT = 1 And zea > AA0 Then
        h = appl.h + tau - IIf(appl.h < 0, 1, 0)
        Print #fo, " ("; fd(h, "###"); ")";
      End If
      '20120110LC (fine)
    End If
  End If
  Print #fo,
End Sub


Public Sub TAppl_Superstiti(ByRef appl As TAppl)
  Dim iro&, irv&, j&, jQ&, nSup&, t&, z0&, z1&, z2&
  Dim c0#, c1#, fact#, prob#, prod#, q#, ro#, rv#, y0#, y1#
  '---
  Dim ipf&, ipft&, iQ&, iSex&, iTit&, nMax3&, tau&, tauMax&, tauMin&, x&
  Dim pfam1() As TWFD
  '--- 20140215LC (inizio)
  Dim f1&, f2&
  Dim AdirA#, AdirP#, AfamA#, AfamP#, ti#, vi#
  Dim s As String
  Dim AA() As Double
  Dim qsd() As Double  'ex 20130220LC (Qx)
  Dim qpd() As Double
  '--- 20140215LC (fine)
  
  '******************
  'Muove i superstiti
  '******************
  With appl
    '--- 20130220LC (inizio)
    c1 = OPZ_INT_SUP_DX
    c0 = 1# - c1
    '--- 20130220LC (fine)
    '--- 20140215LC (inizio)
    ti = 0#
    vi = 1# / (1 + ti)
    '--- 20140215LC (fine)
    '--- 20121204LC (inizio)
    OPZ_INT_PF_XMIN = 20
    OPZ_INT_PF_XMAX = OPZ_OMEGA '96
    tauMin = 0
    tauMax = P_PFAM_MAX_TAU  '20121212LC
    '--- 20130302LC (inizio)
    If UBound(appl.CoeffVar, 1) < appl.t0 + 1 + tauMax Then
      tauMax = UBound(appl.CoeffVar, 1) - appl.t0 - 1
    End If
    If OPZ_ABB_QM_SUP <= 99 Then
      ReDim .pfamD(tauMin To tauMax, (OPZ_INT_SUP_4 * 2) * (1 + OPZ_INT_PF_XMAX - OPZ_INT_PF_XMIN) * (1 + OPZ_OMEGA))  '20121212LC
    Else
      ReDim .pfamL(tauMin To tauMax, (OPZ_INT_SUP_4 * 2) * (1 + OPZ_INT_PF_XMAX - OPZ_INT_PF_XMIN) * (1 + OPZ_OMEGA))  '20121212LC
    End If
    '--- 20130302LC (fine)
    nMax3 = appl.nMax2 + OPZ_ELAB_CODA  '20130216LC
    For tau = tauMin To tauMax
      For iTit = 1 To OPZ_INT_SUP_4  '20121212LC
        For iSex = 1 To 2
          iQ = 2 * (iTit - 1) + iSex
          jQ = 2 * (iTit - 1) + 3 - iSex
    '--- 20121204LC (fine)
          '--- 20140215LC (inizio)  'ex 201230220LC
          ReDim qsd(0 To OPZ_OMEGA)
          For z0 = 0 To OPZ_OMEGA
            qsd(z0) = InterpL(appl.LL1, BT_QSD, 3 - iSex, iTit, z0 - 0)
          Next z0
          '--- 20140215LC (fine)
          For x = OPZ_INT_PF_XMIN To OPZ_INT_PF_XMAX
'If x = 73 Then
'x = x
'End If
            '--- 20121204LC (inizio)
            'ipf = (iSex - 1) * (1 + OPZ_INT_PF_XMAX - OPZ_INT_PF_XMIN) * (1 + OPZ_OMEGA) + (x - OPZ_INT_PF_XMIN) * (1 + OPZ_OMEGA)
            ipf = (iQ - 1) * (1 + OPZ_INT_PF_XMAX - OPZ_INT_PF_XMIN) * (1 + OPZ_OMEGA) + (x - OPZ_INT_PF_XMIN) * (1 + OPZ_OMEGA)
            '--- 20121204LC (fine)
            '--- 20140425LC (inizio)
            If OPZ_SUP_MODEL = 0 Then
              Call TAppl_Superstiti_1(appl, c0, c1, x, iTit, iSex, tau, pfam1(), nMax3, jQ, qsd)
            Else 'If OPZ_SUP_MODEL <> 0 Then
              Call TAppl_Superstiti_4(appl, c0, c1, x, iTit, iSex, tau, pfam1(), nMax3)
              If OPZ_SUP_MODEL < 0 And Math.Abs(OPZ_PENS_C0 - 0.5) <= AA0 Then
                Call TAppl_Superstiti_1(appl, c0, c1, x, iTit, iSex, tau, appl.pfam4(), nMax3, jQ, qsd)
                For t = 0 To nMax3
                  For j = 0 To 8
                    If Math.Abs(appl.pfam4(t).sg(j) - pfam1(t).sg(j)) > AA0 Then
                      Call MsgBox("TODO", , "TAppl_Superstiti_1_4")
                      Stop
                    End If
                  Next j
                Next t
              End If
            End If
            '--- 20140425LC (fine)
            '--- 20130302LC (inizio)
            If OPZ_ABB_QM_SUP <= 99 Then
              For t = 0 To nMax3
                If pfam1(t).sg(0) > AA0 Then
                  For iro = 0 To 8
                    .pfamD(tau, ipf + t).sg(iro) = pfam1(t).sg(iro)
                  Next iro
                End If
              Next t
            Else
              For t = 0 To nMax3
                If pfam1(t).sg(0) > AA0 Then
                  For iro = 0 To 8
                    .pfamL(tau, ipf + t).sg(iro) = pfam1(t).sg(iro) * 1000000#
                  Next iro
                End If
              Next t
            End If
            '--- 20130302LC (fine)
          Next x
        Next iSex
      Next iTit '20121203LC
    Next tau '20121203LC
  End With
End Sub


'20140424LC (tutta)
Private Sub TAppl_Superstiti_1(ByRef appl As TAppl, c0#, c1#, x&, iTit&, iSex&, tau&, pfam1() As TWFD, nMax3&, jQ&, qsd#())
  Dim iro&, irv&, nSup&, t&, z0&, z1&, z2&, ZMAX&
  Dim fact#, prob#, prod#, q#, ro#, rv#, y0#, y1#
  
  ZMAX = appl.parm(P_ZMAX)  '20140424LC  'ex 20140215LC (+dz)
  '--- 20130216LC (inizio)
  ReDim pfam1(0 To nMax3)  '20130216LC
  '--- 20130216LC (fine)
  '--- Gruppo 1 ---
  If 1 = 1 Then
    z0 = appl.pf(x, iSex, epf.pf_yv)
    If z0 <= OPZ_OMEGA Then
      prob = appl.pf(x, iSex, pf_pv)
      If prob > AA0 Then
        For t = 0 To nMax3  '20130216LC
          'ipft = ipf + t
          '--- 20130220LC (fine)
          If z0 > OPZ_OMEGA Then
            Exit For
          ElseIf qsd(z0 - 1) >= AA9 Then
            Exit For
          ElseIf t = 0 Then
            prod = 1#
          Else
            y0 = qsd(z0 - 1)
            y1 = qsd(z0)
            If OPZ_ABB_QM_SUP > 0 Then
              If OPZ_INT_ABBQ = 0 Then
                fact = TIscr2_AbbQ(appl.BDA, tau + t - 1, z0 - c0, jQ - 1)
                y0 = y0 * fact
                y1 = y1 * fact
              Else
                y0 = y0 * TIscr2_AbbQ(appl.BDA, tau + t - 1, z0 - 1, jQ - 1)
                If y1 < AA9 Then
                  y1 = y1 * TIscr2_AbbQ(appl.BDA, tau + t - 1, z0 - 0, jQ - 1)
                End If
              End If
            End If
            q = (y0 * c0 + y1 * c1)
            prod = prod * (1 - q)
          End If
          '--- 20130220LC (fine)
          '--- 20121204LC (inizio)
          irv = 1: rv = appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Av) 'appl.parm(P_cvAv)
          iro = 0: ro = 0
          pfam1(t).sg(irv) = pfam1(t).sg(irv) + prob * prod
          pfam1(t).sg(0) = pfam1(t).sg(0) + prob * prod
          pfam1(t).sg(7) = pfam1(t).sg(7) + prob * prod '20080915LC
          pfam1(t).sg(8) = pfam1(t).sg(8) + prob * prod * rv
          '--- 20121204LC (fine)
          z0 = z0 + 1
        Next t
      End If
    End If
  End If
  '--- Gruppo 2 ---
  If 1 = 1 Then
    z0 = appl.pf(x, iSex, epf.pf_yvo)
    z1 = appl.pf(x, iSex, epf.pf_z1vo)
    If z0 <= OPZ_OMEGA Then
      prob = appl.pf(x, iSex, pf_pvo)
      If prob > AA0 Then
        For t = 0 To nMax3  '20130216LC
          'ipft = ipf + t
          '--- 20130220LC (fine)
          If z0 > OPZ_OMEGA Then
            Exit For
          ElseIf qsd(z0 - 1) >= AA9 Then
            Exit For
          ElseIf t = 0 Then
            prod = 1#
          Else
            y0 = qsd(z0 - 1)
            y1 = qsd(z0)
            If OPZ_ABB_QM_SUP > 0 Then
              If OPZ_INT_ABBQ = 0 Then
                fact = TIscr2_AbbQ(appl.BDA, tau + t - 1, z0 - c0, jQ - 1)
                y0 = y0 * fact
                y1 = y1 * fact
              Else
                y0 = y0 * TIscr2_AbbQ(appl.BDA, tau + t - 1, z0 - 1, jQ - 1)
                If y1 < AA9 Then
                  y1 = y1 * TIscr2_AbbQ(appl.BDA, tau + t - 1, z0 - 0, jQ - 1)
                End If
              End If
            End If
            q = (y0 * c0 + y1 * c1)
            prod = prod * (1 - q)
          End If
          '--- 20130220LC (fine)
          '--- 20121204LC (inizio)
          If z1 < ZMAX Then  '20140215LC
            irv = 2: rv = appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Avo)  'appl.parm(P_cvAvo)
            iro = 4: ro = appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Ao)   'appl.parm(P_cvAo)
          Else
            irv = 1: rv = appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Av)   'appl.parm(P_cvAv)
            iro = 0: ro = 0
          End If
          '--- 20121204LC (fine)
          '20080913LC (fine)
          pfam1(t).sg(irv) = pfam1(t).sg(irv) + prob * prod
          If iro > 0 Then
            pfam1(t).sg(iro) = pfam1(t).sg(iro) + prob * (1 - prod)
            pfam1(t).sg(0) = pfam1(t).sg(0) + prob
            pfam1(t).sg(7) = pfam1(t).sg(7) + prob * (1 + prod) '20080915LC
            pfam1(t).sg(8) = pfam1(t).sg(8) + prob * (prod * rv + (1 - prod) * ro)
          Else
            pfam1(t).sg(0) = pfam1(t).sg(0) + prob * prod
            pfam1(t).sg(7) = pfam1(t).sg(7) + prob * prod '20080915LC
            pfam1(t).sg(8) = pfam1(t).sg(8) + prob * prod * rv
          End If
          z0 = z0 + 1
          z1 = z1 + 1
        Next t
      End If
    End If
  End If
  '--- Gruppo 3 ---
  If 1 = 1 Then
    z0 = appl.pf(x, iSex, epf.pf_yvoo)
    z1 = appl.pf(x, iSex, epf.pf_z1voo)
    z2 = appl.pf(x, iSex, epf.pf_z2voo)
    If z0 <= OPZ_OMEGA Then
      prob = appl.pf(x, iSex, pf_pvoo)
      If prob > AA0 Then
        If z1 = z2 And OPZ_INT_SUP_ZZ = 0 Then  '20121204LC
          z2 = z1 + dmin(z1 + 0, 1)
          z1 = z1 - dmin(z1 + 0, 1)
        End If
        For t = 0 To nMax3  '20130216LC
          'ipft = ipf + t
          '--- 20130220LC (fine)
          If z0 > OPZ_OMEGA Then
            Exit For
          ElseIf qsd(z0 - 1) >= AA9 Then
            Exit For
          ElseIf t = 0 Then
            prod = 1#
          Else
            y0 = qsd(z0 - 1)
            y1 = qsd(z0)
            If OPZ_ABB_QM_SUP > 0 Then
              If OPZ_INT_ABBQ = 0 Then
                fact = TIscr2_AbbQ(appl.BDA, tau + t - 1, z0 - c0, jQ - 1)
                y0 = y0 * fact
                y1 = y1 * fact
              Else
                y0 = y0 * TIscr2_AbbQ(appl.BDA, tau + t - 1, z0 - 1, jQ - 1)
                If y1 < AA9 Then
                  y1 = y1 * TIscr2_AbbQ(appl.BDA, tau + t - 1, z0 - 0, jQ - 1)
                End If
              End If
            End If
            q = (y0 * c0 + y1 * c1)
            prod = prod * (1 - q)
          End If
          '--- 20130220LC (fine)
          '--- 20121204LC (inizio)
          If z2 < ZMAX Then  '20140215LC
            irv = 3: rv = appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Avoo)  'appl.parm(P_cvAvoo)
            iro = 5: ro = appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Aoo)   'appl.parm(P_cvAoo)
            nSup = 3
          ElseIf z1 < ZMAX Then  '20140215LC
            irv = 2: rv = appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Avo)  'appl.parm(P_cvAvo)
            iro = 4: ro = appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Ao)   'appl.parm(P_cvAo)
            nSup = 2
          Else
            irv = 1: rv = appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Av)  'appl.parm(P_cvAv)
            iro = 0: ro = 0
            nSup = 1
          End If
          '--- 20121204LC (fine)
          '20080913LC (fine)
          pfam1(t).sg(irv) = pfam1(t).sg(irv) + prob * prod
          If iro > 0 Then
            pfam1(t).sg(iro) = pfam1(t).sg(iro) + prob * (1 - prod)
            pfam1(t).sg(0) = pfam1(t).sg(0) + prob
            pfam1(t).sg(7) = pfam1(t).sg(7) + prob * (nSup - 1 + prod) '20080915LC
            pfam1(t).sg(8) = pfam1(t).sg(8) + prob * (prod * rv + (1 - prod) * ro)
          Else
            pfam1(t).sg(0) = pfam1(t).sg(0) + prob * prod
            pfam1(t).sg(7) = pfam1(t).sg(7) + prob * prod '20080915LC
            pfam1(t).sg(8) = pfam1(t).sg(8) + prob * prod * rv
          End If
          z0 = z0 + 1
          z1 = z1 + 1
          z2 = z2 + 1
        Next t
      End If
    End If
  End If
  '--- Gruppo 4 ---
  If 1 = 1 Then
    z0 = appl.pf(x, iSex, epf.pf_z1o)
    prob = appl.pf(x, iSex, pf_po)
    If prob > AA0 Then
      For t = 0 To nMax3  '20130216LC
        'ipft = ipf + t
        '--- 20121204LC (inizio)
        If z0 < ZMAX Then  '20140215LC
          iro = 4: ro = appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Ao)  'appl.parm(P_cvAo)
        Else
          Exit For
        End If
        pfam1(t).sg(iro) = pfam1(t).sg(iro) + prob
        pfam1(t).sg(0) = pfam1(t).sg(0) + prob
        pfam1(t).sg(7) = pfam1(t).sg(7) + prob '20080915LC
        pfam1(t).sg(8) = pfam1(t).sg(8) + prob * ro
        '--- 20121204LC (fine)
        z0 = z0 + 1
      Next t
    End If
  End If
  '--- Gruppo 5 ---
  If 1 = 1 Then
    z0 = appl.pf(x, iSex, epf.pf_z1oo)
    z1 = appl.pf(x, iSex, epf.pf_z2oo)
    prob = appl.pf(x, iSex, pf_poo)
    If prob > AA0 Then
      If z0 = z1 And OPZ_INT_SUP_ZZ = 0 Then  '20121204LC
        z1 = z0 + dmin(z0 + 0, 1)
        z0 = z0 - dmin(z0 + 0, 1)
      End If
      For t = 0 To nMax3  '20130216LC
        'ipft = ipf + t
        '--- 20121204LC (inizio)
        If z1 < ZMAX Then  '20140215LC
          iro = 5: ro = appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Aoo)  'appl.parm(P_cvAoo)
          nSup = 2
        ElseIf z0 < ZMAX Then  '20140215LC
          iro = 4: ro = appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Ao)   'appl.parm(P_cvAo)
          nSup = 1
        Else
          Exit For
        End If
        pfam1(t).sg(iro) = pfam1(t).sg(iro) + prob
        pfam1(t).sg(0) = pfam1(t).sg(0) + prob
        pfam1(t).sg(7) = pfam1(t).sg(7) + prob * nSup '20080915LC
        pfam1(t).sg(8) = pfam1(t).sg(8) + prob * ro
        '--- 20121204LC (fine)
        z0 = z0 + 1
        z1 = z1 + 1
      Next t
    End If
  End If
  '--- Gruppo 6 ---
  If 1 = 1 Then
    z0 = appl.pf(x, iSex, epf.pf_z1ooo)
    z1 = appl.pf(x, iSex, epf.pf_z2ooo)
    z2 = appl.pf(x, iSex, epf.pf_z3ooo)
    prob = appl.pf(x, iSex, pf_pooo)
    If prob > AA0 Then
      If z0 = z1 And z1 = z2 And OPZ_INT_SUP_ZZ = 0 Then  '20121204LC
        z2 = z0 + dmin(z0 + 0, 2)
        z1 = z0
        z0 = z0 - dmin(z0 + 0, 2)
      End If
      For t = 0 To nMax3  '20130216LC
        'ipft = ipf + t
        '--- 20121204LC (inizio)
        If z2 < ZMAX Then  '20140215LC
          iro = 6: ro = appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Aooo)  'appl.parm(P_cvAooo)
          nSup = 3
        ElseIf z1 < ZMAX Then  '20140215LC
          iro = 5: ro = appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Aoo)   'appl.parm(P_cvAoo)
          nSup = 2
        ElseIf z0 < ZMAX Then  '20140215LC
          iro = 4: ro = appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Ao)    'appl.parm(P_cvAo)
          nSup = 1
        Else
          Exit For
        End If
        pfam1(t).sg(iro) = pfam1(t).sg(iro) + prob
        pfam1(t).sg(0) = pfam1(t).sg(0) + prob
        pfam1(t).sg(7) = pfam1(t).sg(7) + prob * nSup '20080915LC
        pfam1(t).sg(8) = pfam1(t).sg(8) + prob * ro
        '--- 20121204LC (fine)
        z0 = z0 + 1
        z1 = z1 + 1
        z2 = z2 + 1
      Next t
    End If
  End If
End Sub


'20140424LC (tutta)
Private Sub TAppl_Superstiti_4(ByRef appl As TAppl, c0#, c1#, x&, iTit&, iSex&, tau&, pfam4() As TWFD, nMax3&)
  Dim z0&, z1&, z2&, ZMAX&
  Dim prob#
  Dim dip As TIscr2
  
  ZMAX = appl.parm(P_ZMAX)
  dip.Qualifica = iTit
  dip.zs = 1#
  ReDim pfam4(0 To nMax3)
  '--- Gruppo 1
  If 1 = 1 Then
    prob = appl.pf(x, iSex, pf_pv)
    z0 = appl.pf(x, iSex, epf.pf_yv)
    dip.nSup = 1
    ReDim dip.sup(dip.nSup - 1)
    With dip.sup(0)
      .c0 = c0
      .c1 = c1
      .DNasc = 0
      .mat = -1
      .rev = appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Av)
      .s = 3 - iSex
      .tipGen = 0
      .tipSup = 1
      .y = z0
      .ymax = OPZ_OMEGA
    End With
    Call TIscr2_MovPop_Superstiti4_2(dip, appl, 0, tau, prob, pfam4)
  End If
  '--- Gruppo 2
  If 1 = 1 Then
    prob = appl.pf(x, iSex, pf_pvo)
    z0 = appl.pf(x, iSex, epf.pf_yvo)
    z1 = appl.pf(x, iSex, epf.pf_z1vo)
    dip.nSup = 2
    ReDim dip.sup(dip.nSup - 1)
    With dip.sup(0)
      .c0 = c0
      .c1 = c1
      .DNasc = 0
      .mat = -1
      .rev = appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Av)
      .s = 3 - iSex
      .tipGen = 0
      .tipSup = 1
      .y = z0
      .ymax = OPZ_OMEGA
    End With
    With dip.sup(1)
      .c0 = c0
      .c1 = c1
      .DNasc = 0
      .mat = -2
      .rev = appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Avo) - appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Av)
      .s = 1
      .tipGen = 0
      .tipSup = 4
      .y = z1
      .ymax = ZMAX
    End With
    Call TIscr2_MovPop_Superstiti4_2(dip, appl, 0, tau, prob, pfam4)
  End If
  '--- Gruppo 3
  If 1 = 1 Then
    prob = appl.pf(x, iSex, pf_pvoo)
    z0 = appl.pf(x, iSex, epf.pf_yvoo)
    z1 = appl.pf(x, iSex, epf.pf_z1voo)
    z2 = appl.pf(x, iSex, epf.pf_z2voo)
    If z1 = z2 And OPZ_INT_SUP_ZZ = 0 Then  '20121204LC
      z2 = z1 + dmin(z1 + 0, 1)
      z1 = z1 - dmin(z1 + 0, 1)
    End If
    dip.nSup = 3
    ReDim dip.sup(dip.nSup - 1)
    With dip.sup(0)
      .c0 = c0
      .c1 = c1
      .DNasc = 0
      .mat = -1
      .rev = appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Av)
      .s = 3 - iSex
      .tipGen = 0
      .tipSup = 1
      .y = z0
      .ymax = OPZ_OMEGA
    End With
    With dip.sup(1)
      .c0 = c0
      .c1 = c1
      .DNasc = 0
      .mat = -2
      .rev = appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Avo) - appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Av)
      .s = 1
      .tipGen = 0
      .tipSup = 4
      .y = z1
      .ymax = ZMAX
    End With
    With dip.sup(2)
      .c0 = c0
      .c1 = c1
      .DNasc = 0
      .mat = -3
      .rev = appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Avoo) - appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Avo)
      .s = 1
      .tipGen = 0
      .tipSup = 4
      .y = z2
      .ymax = ZMAX
    End With
    Call TIscr2_MovPop_Superstiti4_2(dip, appl, 0, tau, prob, pfam4)
  End If
  '--- Gruppo 4
  If 1 = 1 Then
    prob = appl.pf(x, iSex, pf_po)
    z0 = appl.pf(x, iSex, epf.pf_z1o)
    dip.nSup = 1
    ReDim dip.sup(dip.nSup - 1)
    With dip.sup(0)
      .c0 = c0
      .c1 = c1
      .DNasc = 0
      .mat = -1
      .rev = appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Ao)
      .s = 1
      .tipGen = 0
      .tipSup = 4
      .y = appl.pf(x, iSex, epf.pf_z1o)
      .ymax = ZMAX
    End With
    Call TIscr2_MovPop_Superstiti4_2(dip, appl, 0, tau, prob, pfam4)
  End If
  '--- Gruppo 5
  If 1 = 1 Then
    prob = appl.pf(x, iSex, pf_poo)
    z0 = appl.pf(x, iSex, epf.pf_z1oo)
    z1 = appl.pf(x, iSex, epf.pf_z2oo)
    If z0 = z1 And OPZ_INT_SUP_ZZ = 0 Then  '20121204LC
      z1 = z0 + dmin(z0 + 0, 1)
      z0 = z0 - dmin(z0 + 0, 1)
    End If
    dip.nSup = 2
    ReDim dip.sup(dip.nSup - 1)
    With dip.sup(0)
      .c0 = c0
      .c1 = c1
      .DNasc = 0
      .mat = -1
      .rev = appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Ao)
      .s = 1
      .tipGen = 0
      .tipSup = 4
      .y = z0
      .ymax = ZMAX
    End With
    With dip.sup(1)
      .c0 = c0
      .c1 = c1
      .DNasc = 0
      .mat = -2
      .rev = appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Aoo) - appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Ao)
      .s = 1
      .tipGen = 0
      .tipSup = 4
      .y = z1
      .ymax = ZMAX
    End With
    Call TIscr2_MovPop_Superstiti4_2(dip, appl, 0, tau, prob, pfam4)
  End If
  '--- Gruppo 6
  If 1 = 1 Then
    prob = appl.pf(x, iSex, pf_pooo)
    z0 = appl.pf(x, iSex, epf.pf_z1ooo)
    z1 = appl.pf(x, iSex, epf.pf_z2ooo)
    z2 = appl.pf(x, iSex, epf.pf_z3ooo)
    If z0 = z1 And z1 = z2 And OPZ_INT_SUP_ZZ = 0 Then  '20121204LC
      z2 = z0 + dmin(z0 + 0, 2)
      z1 = z0
      z0 = z0 - dmin(z0 + 0, 2)
    End If
    dip.nSup = 3
    ReDim dip.sup(dip.nSup - 1)
    With dip.sup(0)
      .c0 = c0
      .c1 = c1
      .DNasc = 0
      .mat = -1
      .rev = appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Ao)
      .s = 1
      .tipGen = 0
      .tipSup = 4
      .y = z0
      .ymax = ZMAX
    End With
    With dip.sup(1)
      .c0 = c0
      .c1 = c1
      .DNasc = 0
      .mat = -2
      .rev = appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Aoo) - appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Ao)
      .s = 1
      .tipGen = 0
      .tipSup = 4
      .y = z1
      .ymax = ZMAX
    End With
    With dip.sup(2)
      .c0 = c0
      .c1 = c1
      .DNasc = 0
      .mat = -3
      .rev = appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Aooo) - appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Aoo)
      .s = 1
      .tipGen = 0
      .tipSup = 4
      .y = z2
      .ymax = ZMAX
    End With
    Call TIscr2_MovPop_Superstiti4_2(dip, appl, 0, tau, prob, pfam4)
  End If
End Sub


Public Sub LeggiPFam(db As ADODB.Connection, LL#(), pfNome$, pf#())  'da AFP
'****************************************************************************************************
'SCOPO
'  Legge le probabilit� di famiglia
'CHIAMATO DA
'  TAppl1_BilTecn
'VERSIONI
'  20120130LC
'  20140426LC  OPZ_SUP_DZ
'  20160306LC  spostato da modEneaTIscrS (eliminato)
'****************************************************************************************************
  Dim iSex%, t%, x%, XMAX%, XMIN%
  Dim tim#
  Dim SQL$
  Dim rs As New ADODB.Recordset
  
  ReDim pf(0 To OPZ_OMEGA, 1 To 2, 25)  '20080814LC - ex TO 110
  For iSex = 1 To 2
    '*************************************
    'Lettura delle probabilit� di famiglia
    '*************************************
    SQL = "SELECT * FROM C_PF"
    SQL = SQL & " WHERE pfNome='" & UCase(pfNome) & "'" '20121024LC
    SQL = SQL & " AND pfSesso='" & UCase(IIf(iSex = 1, "M", "F")) & "'" '20121024LC
    SQL = SQL & " ORDER BY pfX"
    rs.Open SQL, db, adOpenForwardOnly, adLockReadOnly
    If rs.EOF = False Then
      XMIN = rs.Fields("pfX").Value
      Do Until rs.EOF
        x = rs.Fields("pfX").Value + OPZ_SUP_DZ '20140426LC
        'pf(x, iSex, epf.pf_ps) = rs.Fields("pfPs
        pf(x, iSex, epf.pf_pv) = ff0(rs.Fields("pfPv").Value)
        pf(x, iSex, epf.pf_yv) = ff0(rs.Fields("pfYv").Value)
        pf(x, iSex, epf.pf_pvo) = ff0(rs.Fields("pfPvo").Value)
        pf(x, iSex, epf.pf_yvo) = ff0(rs.Fields("pfYvo").Value)
        pf(x, iSex, epf.pf_z1vo) = ff0(rs.Fields("pfZvo").Value)
        pf(x, iSex, epf.pf_pvoo) = ff0(rs.Fields("pfPvoo").Value)
        pf(x, iSex, epf.pf_yvoo) = ff0(rs.Fields("pfYvoo").Value)
        pf(x, iSex, epf.pf_z1voo) = ff0(rs.Fields("pfZvoo").Value)
        pf(x, iSex, epf.pf_z2voo) = ff0(rs.Fields("pfZvoo").Value)
        If pf(x, iSex, epf.pf_z2voo) > 0 Then
          'pf(x, iSex, epf.pf_z1voo) = pf(x, iSex, epf.pf_z1voo) + 1
          'pf(x, iSex, epf.pf_z2voo) = pf(x, iSex, epf.pf_z2voo) - 1
        End If
        pf(x, iSex, epf.pf_po) = ff0(rs.Fields("pfPo").Value)
        pf(x, iSex, epf.pf_z1o) = ff0(rs.Fields("pfZo").Value)
        pf(x, iSex, epf.pf_poo) = ff0(rs.Fields("pfPoo").Value)
        pf(x, iSex, epf.pf_z1oo) = ff0(rs.Fields("pfZoo").Value)
        pf(x, iSex, epf.pf_z2oo) = ff0(rs.Fields("pfZoo").Value)
        If pf(x, iSex, epf.pf_z2oo) > 0 Then
          'pf(x, iSex, epf.pf_z1oo) = pf(x, iSex, epf.pf_z1oo) + 1
          'pf(x, iSex, epf.pf_z2oo) = pf(x, iSex, epf.pf_z2oo) - 1
        End If
        pf(x, iSex, epf.pf_pooo) = rs.Fields("pfPooo").Value
        pf(x, iSex, epf.pf_z1ooo) = rs.Fields("pfZooo").Value
        pf(x, iSex, epf.pf_z2ooo) = rs.Fields("pfZooo").Value
        pf(x, iSex, epf.pf_z3ooo) = rs.Fields("pfZooo").Value
        If pf(x, iSex, epf.pf_z3ooo) > 2 Then
          'pf(x, iSex, epf.pf_z1ooo) = pf(x, iSex, epf.pf_z1ooo) + 2
          'pf(x, iSex, epf.pf_z3ooo) = pf(x, iSex, epf.pf_z3ooo) - 2
        ElseIf pf(x, iSex, epf.pf_z3ooo) > 1 Then
          'pf(x, iSex, epf.pf_z1ooo) = pf(x, iSex, epf.pf_z1ooo) + 1
          'pf(x, iSex, epf.pf_z3ooo) = pf(x, iSex, epf.pf_z3ooo) - 1
        End If
        pf(x, iSex, epf.pf_pfam) = pf(x, iSex, epf.pf_pv) + pf(x, iSex, epf.pf_pvo) + pf(x, iSex, epf.pf_pvoo) + pf(x, iSex, epf.pf_po) + pf(x, iSex, epf.pf_poo) + pf(x, iSex, epf.pf_pooo)
        pf(x, iSex, epf.pf_afam) = pf(x, iSex, epf.pf_pv) * 0.6 + pf(x, iSex, epf.pf_pvo) * 0.8 + pf(x, iSex, epf.pf_pvoo) * 1 + pf(x, iSex, epf.pf_po) * 0.6 + pf(x, iSex, epf.pf_poo) * 0.8 + pf(x, iSex, epf.pf_pooo) * 1
        If pf(x, iSex, epf.pf_pfam) > 1 Then
          pf(x, iSex, epf.pf_pv) = pf(x, iSex, epf.pf_pv) / pf(x, iSex, epf.pf_pfam)
          pf(x, iSex, epf.pf_pvo) = pf(x, iSex, epf.pf_pvo) / pf(x, iSex, epf.pf_pfam)
          pf(x, iSex, epf.pf_pvoo) = pf(x, iSex, epf.pf_pvoo) / pf(x, iSex, epf.pf_pfam)
          pf(x, iSex, epf.pf_po) = pf(x, iSex, epf.pf_po) / pf(x, iSex, epf.pf_pfam)
          pf(x, iSex, epf.pf_poo) = pf(x, iSex, epf.pf_poo) / pf(x, iSex, epf.pf_pfam)
          pf(x, iSex, epf.pf_pooo) = pf(x, iSex, epf.pf_pooo) / pf(x, iSex, epf.pf_pfam)
          pf(x, iSex, epf.pf_pfam) = 1
        End If
        pf(x, iSex, epf.pf_ps) = 1 - pf(x, iSex, epf.pf_pfam)
        pf(x, iSex, epf.pf_pfra) = LL(x, BT_PFRA, iSex)
        pf(x, iSex, epf.pf_pgen) = LL(x, BT_PGEN, iSex)
        pf(x, iSex, epf.pf_per) = pf(x, iSex, epf.pf_ps) - pf(x, iSex, epf.pf_pfra) - pf(x, iSex, epf.pf_pgen)
        If pf(x, iSex, epf.pf_per) < 0 Then
          pf(x, iSex, epf.pf_pfra) = pf(x, iSex, epf.pf_pfra) / pf(x, iSex, epf.pf_ps)
          pf(x, iSex, epf.pf_pgen) = pf(x, iSex, epf.pf_pgen) / pf(x, iSex, epf.pf_ps)
          pf(x, iSex, epf.pf_per) = 0
        End If
        rs.MoveNext
      Loop
      XMAX = x
    End If
    rs.Close
  Next iSex
End Sub

