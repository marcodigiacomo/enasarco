Attribute VB_Name = "modAfpServer"
Option Explicit

'############################
'Interfaccia privata (inizio)
'############################
'--- 20130307LC (fine)
Private p_bInit As Boolean
Private p_nCCR&
Private p_vCCR$()
Private p_nIRR&
Private p_vIRR$()
Private p_nLS&
Private p_vLS$()
Private p_nNI&
Private p_vNI$()
'--- 20160323LC (inizio)
Private p_nNE&
Private p_vNE$()
Private p_nNV&
Private p_vNV$()
'--- 20160323LC (fine)
Private p_nNIP&
Private p_vNIP$()
Private p_nPF&
Private p_vPF$()
Private p_nPrm&
Private p_vPrm$()
Private p_nQ&
Private p_vQ$()
'// Supporto ASP (fine)
'##########################
'Interfaccia privata (fine)
'##########################



'#############################
'Interfaccia pubblica (inizio)
'#############################
'20130307LC (tutta)  'ex ParametriMaskLoad6
Public Function Param_MaskLoad_CCR(mi&, db As ADODB.Connection, frm As Variant) As String
  '****************************************************************
  'Lettura nomi coefficienti di conversione del capitale in rendita
  '****************************************************************
  Dim sErr$
  
  '--- Lettura da DB
  sErr = Param_MaskLoad(db, "C_CCR", "cpaNome", "", p_nCCR, p_vCCR)  '20160419LC-2
  If sErr <> "" Then GoTo ExitPoint
  '--- Caricamento maschera
  If mi = MI_VB Then
    Call Param_MaskLoad_VB(p_nCCR, p_vCCR, frm.Combo1(P_CCR))
  Else
    Call Param_MaskLoad_ASP(p_nCCR, p_vCCR, frm)
  End If
  
ExitPoint:
  Param_MaskLoad_CCR = sErr
End Function


'20130307LC (tutta)  'ex ParametriMaskLoad7
Public Function Param_MaskLoad_IRR(mi&, db As ADODB.Connection, frm As Variant) As String
  '**********************
  'Lettura nomi tassi IRR
  '**********************
  Dim sErr$
  
  '--- Lettura da DB
  sErr = Param_MaskLoad(db, "ALM_IRR", "irrNome", "", p_nIRR, p_vIRR)
  If sErr <> "" Then GoTo ExitPoint
  '--- Caricamento maschera
  If mi = MI_VB Then
    Call Param_MaskLoad_VB(p_nIRR, p_vIRR, frm.Combo1(P_cvTv))
    Call Param_MaskLoad_VB(p_nIRR, p_vIRR, frm.Combo1(P_cvTi))
  Else
    Call Param_MaskLoad_ASP(p_nIRR, p_vIRR, frm)
  End If
  
ExitPoint:
  Param_MaskLoad_IRR = sErr
End Function


'20130307LC (tutta)  'ex ParametriMaskLoad4
Public Function Param_MaskLoad_LS(mi&, db As ADODB.Connection, frm As Variant) As String  '20130307LC
  '*****************************
  'Lettura nomi linee reddituali
  '*****************************
  Dim sErr$
  
  '--- Lettura da DB
  sErr = Param_MaskLoad(db, "C_LS", "lsNome", "", p_nLS, p_vLS)  '20160419LC
  If sErr <> "" Then GoTo ExitPoint
  '--- Caricamento maschera
  If mi = MI_VB Then
    Call Param_MaskLoad_VB(p_nLS, p_vLS, frm.Combo1(P_LRA))
    Call Param_MaskLoad_VB(p_nLS, p_vLS, frm.Combo1(P_LRP))
    Call Param_MaskLoad_VB(p_nLS, p_vLS, frm.Combo1(P_LRN))
  Else
    Call Param_MaskLoad_ASP(p_nLS, p_vLS, frm)
  End If

ExitPoint:
  Param_MaskLoad_LS = sErr
End Function


Public Function Param_MaskLoad_NI(mi&, db As ADODB.Connection, obj As Variant) As String
'***************************************
'Lettura nomi statistiche nuovi iscritti
'20130307LC (tutta)  'ex ParametriMaskLoad5
'20130307LC
'20150520LC
'***************************************
  Dim sErr$
  Dim sTableNI As String  '20160419LC
  
  '--- Lettura da DB
  sTableNI = "C_NI"  '20160419LC
  sErr = Param_MaskLoad(db, sTableNI, "niNome", "", p_nNI, p_vNI)  '20160419LC
  If sErr <> "" Then GoTo ExitPoint
  '--- 20160323LC (inizio)
  sErr = Param_MaskLoad(db, sTableNI, "neNome", "", p_nNE, p_vNE)  '20160419LC
  If sErr <> "" Then GoTo ExitPoint
  sErr = Param_MaskLoad(db, sTableNI, "nvNome", "", p_nNV, p_vNV)  '20160419LC
  If sErr <> "" Then GoTo ExitPoint
  '--- 20160323LC (fine)
  '--- Caricamento maschera
  If mi = MI_VB Then
    Dim frm As frmElab
    
    Set frm = obj
    Call Param_MaskLoad_VB(p_nNI, p_vNI, frm.cbxNiStat)
    '--- 20160323LC (inizio)  'ex 20150612LC
    Call Param_MaskLoad_VB(p_nNE, p_vNE, frm.cbxNeStat, True)
    Call Param_MaskLoad_VB(p_nNV, p_vNV, frm.cbxNvStat, True)
    '--- 20160323LC (fine)  'ex 20150612LC
  Else
    Call Param_MaskLoad_ASP(p_nNI, p_vNI, obj)
  End If
  
ExitPoint:
  Param_MaskLoad_NI = sErr
End Function


Public Function Param_MaskLoad_NIP(mi&, db As ADODB.Connection, obj As Variant) As String
'************************************
'Lettura nomi nuovi iscritti puntuali
'20130307LC (tutta)  'ex ParametriMaskLoad8
'************************************
  Dim sErr$
  
  '--- Lettura da DB
  sErr = Param_MaskLoad(db, "C_NIP", "nipNome", "", p_nNIP, p_vNIP)
  If sErr <> "" Then GoTo ExitPoint
  '--- Caricamento maschera
  If mi = MI_VB Then
    Dim frm As frmElab
    
    Set frm = obj
    Call Param_MaskLoad_VB(p_nNIP, p_vNIP, frm.cbxNip)
  Else
    Call Param_MaskLoad_ASP(p_nNIP, p_vNIP, obj)
  End If
  
ExitPoint:
  Param_MaskLoad_NIP = sErr
End Function


'20130307LC (tutta)  'ex ParametriMaskLoad3
Public Function Param_MaskLoad_PF(mi&, db As ADODB.Connection, frm As Variant) As String  '20130307LC
  '************************************
  'Lettura nomi probabilit� di famiglia
  '************************************
  Dim sErr$
  
  '--- Lettura da DB
  sErr = Param_MaskLoad(db, "C_PF", "pfNome", "", p_nPF, p_vPF)
  If sErr <> "" Then GoTo ExitPoint
  '--- Caricamento maschera
  If mi = MI_VB Then
    Call Param_MaskLoad_VB(p_nPF, p_vPF, frm.Combo1(P_PF))
  Else
    Call Param_MaskLoad_ASP(p_nPF, p_vPF, frm)
  End If

ExitPoint:
  Param_MaskLoad_PF = sErr
End Function


'20130307LC (tutta)  'ex ParametriMaskLoad1
Public Function Param_MaskLoad_Prm(mi&, db As ADODB.Connection, frm As Variant) As String
  '*****************************
  'Lettura nomi gruppi parametri
  '*****************************
  Dim sErr$
  
  '--- Lettura da DB
  sErr = Param_MaskLoad(db, "I_PARAM", "P_ID", "base", p_nPrm, p_vPrm)
  If sErr <> "" Then GoTo ExitPoint
  '--- Caricamento maschera
  If mi = MI_VB Then
    Call Param_MaskLoad_VB(p_nPrm, p_vPrm, frm.Combo1(P_ID))
  Else
    Call Param_MaskLoad_ASP(p_nPrm, p_vPrm, frm)
  End If
  
ExitPoint:
  Param_MaskLoad_Prm = sErr
End Function


Public Function Param_MaskLoad_Q(mi&, db As ADODB.Connection, frm As frmElab, obj As Variant) As String
'20130307LC (tutta)  'ex ParametriMaskLoad2
'20150505LC
  '******************************
  'Lettura nomi basi demografiche
  '******************************
  Dim sErr$
  
  '--- Lettura da DB
  sErr = Param_MaskLoad(db, "C_BD", "bdNome", "", p_nQ, p_vQ)  '20160419LC
  If sErr <> "" Then GoTo ExitPoint
  '--- Caricamento maschera
  If mi = MI_VB Then
    Call Param_MaskLoad_VB(p_nQ, p_vQ, frm.Combo1(P_LAD))
    Call Param_MaskLoad_VB(p_nQ, p_vQ, frm.Combo1(P_LED))
    Call Param_MaskLoad_VB(p_nQ, p_vQ, frm.Combo1(P_LPD))
    Call Param_MaskLoad_VB(p_nQ, p_vQ, frm.Combo1(P_LSD))
    Call Param_MaskLoad_VB(p_nQ, p_vQ, frm.Combo1(P_LBD))
    Call Param_MaskLoad_VB(p_nQ, p_vQ, frm.Combo1(P_LID))
    '---
    Call Param_MaskLoad_VB(p_nQ, p_vQ, frm.cbxQav)
    Call Param_MaskLoad_VB(p_nQ, p_vQ, frm.cbxQae)
    Call Param_MaskLoad_VB(p_nQ, p_vQ, frm.Combo1(P_LAB))
    Call Param_MaskLoad_VB(p_nQ, p_vQ, frm.Combo1(P_LAI))
    Call Param_MaskLoad_VB(p_nQ, p_vQ, frm.Combo1(P_LAW))
  Else
    Call Param_MaskLoad_ASP(p_nQ, p_vQ, obj)
  End If

ExitPoint:
  Param_MaskLoad_Q = sErr
End Function


'#############################
'Interfaccia pubblica (inizio)
'#############################


'############################
'Interfaccia privata (inizio)
'############################
Private Function Param_MaskLoad(ByRef db As ADODB.Connection, ByRef sTable As String, ByRef sKeyA As String, _
                                ByRef sFind As String, ByRef nS As Long, ByRef vS() As String) As String
'20130307LC (tutta)
'20160323LC  tutta
  Dim bFound As Boolean
  Dim tipo As Byte
  Dim i As Long
  Dim sErr As String
  Dim sKey As String  '20160323LC
  Dim SQL As String
  Dim rs As ADODB.Recordset
  
  sErr = ""
  nS = 0
  Erase vS
  '--- 20160323LC (inizio)
  sKey = UCase(sKeyA)
  Select Case sKey
  Case "NINOME"
    tipo = 1
  Case "NENOME"
    tipo = 2
    sKey = "NINOME"
  Case "NVNOME"
    tipo = 3
    sKey = "NINOME"
  Case Else
    tipo = 0
  End Select
  '--- 20160323LC (fine)
  '--- Lettura da DB
  SQL = "SELECT DISTINCT " & sKey
  SQL = SQL & " FROM " & sTable
  If tipo = 1 Then
    SQL = SQL & " WHERE (" & sKey & " NOT LIKE 'S%')"
    SQL = SQL & " AND (" & sKey & " NOT LIKE 'V%')"
  ElseIf tipo = 2 Then
    SQL = SQL & " WHERE (" & sKey & " LIKE 'SX%')"
    SQL = SQL & " OR (" & sKey & " LIKE 'SH%')"
  ElseIf tipo = 3 Then
    SQL = SQL & " WHERE (" & sKey & " LIKE 'VX%')"
    SQL = SQL & " OR (" & sKey & " LIKE 'VH%')"
  End If
  SQL = SQL & " ORDER BY " & sKey & " ASC"
  Set rs = New ADODB.Recordset
  rs.CursorLocation = adUseClient
  Call rs.Open(SQL, db, adOpenForwardOnly, adLockReadOnly)
  If rs.EOF = True Then
    If tipo = 0 Then
      sErr = "La tabella '" & sTable & "' � vuota"
    End If
  Else
    nS = rs.RecordCount
    ReDim vS(nS - 1)
    bFound = False
    For i = 0 To nS - 1
      vS(i) = LCase(Trim(rs(0)))
      If vS(i) = sFind Then
        bFound = True
      End If
      Call rs.MoveNext
    Next i
    If sFind <> "" And bFound = False Then
      sErr = "La tabella '" & sTable & "' non contiene la chiave '" & sFind & "'"
    End If
  End If
  Call rs.Close
  Param_MaskLoad = sErr
End Function


Private Sub Param_MaskLoad_ASP(nS&, vS$(), obj As Variant)
  Dim i&
  Dim s$
  
  s = ""
  For i = 0 To nS - 1
    s = s & "<option value='" & Replace(vS(i), "'", "''") & "'>" & vS(i) & "</option>"
  Next i
  obj = s
End Sub


Private Sub Param_MaskLoad_VB(ByRef nS&, ByRef vS$(), ByRef cb As ComboBox, Optional ByRef bStrNull As Boolean = False)
'20150612LC
  Dim i&
  
  cb.Clear
  '--- 20150612LC (inizio)
  If bStrNull = True Then
    cb.AddItem ""
  End If
  '--- 20150612LC (fine)
  For i = 0 To nS - 1
    cb.AddItem vS(i)
  Next i
End Sub
'##########################
'Interfaccia privata (fine)
'##########################
