Attribute VB_Name = "modAfpTIscr2"
Option Explicit

  '1. MATRICOLE NON ELABORATE: abbiamo casualmente individuato una matricola attiva di 51 anni che non viene elaborata; la matricola � 1120910;
  '2. PENSIONE DI VECCHIAIA POSTICIPATA: Non ci sembra che il software gestisca l�uscita per vecchiaia posticipata: abbiamo testato una matricola con et� al 31.12.2014 = 70 anni, anzianit� al 31.12.2014 = 4 anni; abbiamo imposto et� massima di permanenza nello stato di attivo = 75 anni e ci aspettavamo uscisse (eventualmente senza diritto o con una rendita contributiva) a 75 anni; invece prosegue l�attivit� fino al raggiungimento dei 20 anni di anzianit� uscendo a 85 anni di et�; anche nella valutazione complessiva non ci sono pensionati di vecchiaia posticipata che mi sembra un po� strano; (nel DB matricola 300 con parametri �testpost�)
  '3. PENSIONI DI INVALIDIT�: abbiamo testato un attivo cui abbiamo applicato al probabilit� di divenire invalido e di divenire pensionato contribuente al pensionamento; ci sembra che nel momento in cui avviene il passaggio da attivo a pensionato contribuente, agli invalidi �residui� non applichi pi� la mortalit� da invalido ma quella da pensionato non invalido, invece ci aspettava che per gli invalidi la mortalit� fosse sempre quella specifica per gli invalidi (nel DB matricola 30137737 con parametri �testinv�);
  '4. PENSIONAMENTO DI VECCHIAIA ANTICIPATA PER VOLONTARI E SILENTI: abbiamo testato la matricola attiva 8792723 (ha gi� 25 anni di anzianit� in input) applicando tutte le probabilit� di passaggio di stato, si creano quindi anche nuovi volontari e nuovi silenti; quando raggiunge i requisiti di vecchiaia anticipata, l�attivo residuo diventa interamente pensionato anticipato, mentre solo una parte dei volontari e dei silenti accede al pensionamento anticipato; la restante parte accede nei due anni successivi; ci aspettavamo che una volta raggiunto il requisito accedessero tutti; (nel DB matricola 8792723 con parametri �provig�)
'5. PROVVIGIONI AL MASSIMALE: abbiamo testato una matricola (es. 30137737) che ha una provvigione al di sotto del massimale; nel primo anno abbiamo previsto una riduzione del -7% delle provvigioni e per questa matricola la provvigione fino al tetto risulta pi� alta di quella senza plafont; la cosa strana � che questo accade solo se applichiamo come unica probabilit� di uscita dallo stato di attivo l�invalidit� (nel DB matricola 30137737 con parametri �testinv�).

'SOFTWARE DI ESTRAZIONE DI DB FINALI
'1. Messa a punto estrazione dei superstiti
'2. Redazione della procedura di utilizzo del software

'SOFTWARE DI CREAZIONE DELLE TABELLE PER LE BASI TECNICHE
'1. Fornitura file excel per la creazione della tabella SWA_R;
'2. Per le query A, B, D, G considerare X1 in luogo di X0;
'3. nella Query H (riattivazione da silente) riportare gli eventi e gli esposti (come in H_Aux, che a questo punto non servirebbe pi�); questo riallinea la Query H con la Query I (che ha la stessa logica);
'4. nella Query J definire intervallo di et� possibile per i nuovi ingressi (18-55); � possibile sceglierlo dall�esterno questo intervallo? inoltre le frequenze dovrebbero essere calcolate in modo che il totale delle 4 categorie nell�intervallo considerato sia 1 per tutte e 4 le colonne (attualmente la somma 1 la si ottiene per ciascuna et�);
'5. nella Query N, sarebbe necessario lasciare il livello di dettaglio attualmente contenuto in N_Aux, considerando solo l'ultimo anno (2014 in questo caso);
'6. sempre nella Query N abbiamo notato che per il calcolo di �m_ni2M� c�� una incongruenza nel calcolo della provvigione media: fa il rapporto tra il totale delle provvigioni dei mono e il numero dei pluri.

'SOFTWARE DI REDAZIONE DEL BILANCIO TECNICO
  '1. Controllo matricole non elaborate dal software;
  '2. Modifica gestione della base tecnica delle riattivazioni sia da silente che da volontario prevedendo la possibilit�
  '   di gestirle sia per anzianit� che per et�, e ovviamente per antidurata, sfruttando i campi attualmente a disposizione;
  '3. Per i nuovi ingressi scrivere, nella tabella, il numero dei mandati e cambiare la codifica della categoria (era rimasto
  '   ingegneri e architetti);
  '4. attualmente le probabilit� di passaggio di stato da attivo a volontario vengono applicate per et�; dovremmo poterle
  '   applicare sia per et� che per anzianit� (sfruttando la denominazione "qh") come per tutti gli altri passaggi di stato;
  '5. la prosecuzione volontaria � ammessa solo per coloro i quali vantino un'anzianit� contributiva pari ad almeno 5 anni di
  '   cui almeno 3 nel quinquennio precedente (art. 9); qualora la base tecnica in questione (ATT-VOL) fosse applicata per
  '   anzianit� non ci sarebbero problemi ma sarebbe opportuno fare questo controllo sulle anzianit� da programma in modo da
  '   poterla applicare anche per et�.
  '6. "scollegare" il pi� possibile l'attribuzione dell'et�/mandati e cos� via dall'ordine in cui sono generate le matricole dei nuovi iscritti


'--- CONSOLIDAMENTO
'1 TIscr2_MovPop_4:  Montanti
'2 TIscr2_MovPop_4c: Pensioni
'3 TIscr2_MovPop_9:  Popolazione
'4 TIscr2_MovPop_Contributi: Redditi e contributi
'5 TIscr2_MovPop_Sup1b_D: Superstiti
'6 TIscr2_MovPop_Sup1b_L: Superstiti
'7 TIscr2_MovPop_Sup4: Superstiti
'8 TIscr2_MovPop_New: Popolazione fino a scadenza
'---
'TODO: 20121129 Contributi facoltativi differenziati per pensionati

'--- 20121101LC (inizio) - statistica di chiamate
'TIscr2_Pens         = 8819023
'TIscr2_MovPop_Sup1b = 4719846
'TIscr2_MovPop_3a    = 1302142
'TIscr2_MovPop1_hea  = 1292215
'TIscr2_PensDirInd   =  762655
'TIscr2_PensMista    =  762655
'TIscr2_MovPop_4a    =  728110
'TIscr2_MovPop_4b1   =  683409
'TIscr2_MovPop_3     =  668561
'TIscr2_MovPop_1     =  242700
'TIscr2_MovPop_9     =  242700
'TIscr2_MovPop1_dh   =  242700
'TIscr2_MovPop_4     =  240273
'TIscr2_MovPop_4c    =  240273
'TIscr2_MovPop_Contr =  240273
'TIscr2_MovPop_4b2   =   59851
'TIscr2_PensioniUnit =   58478
'TIscr2_MovPop_0     =    2427
'TIscr2_MovPop_2     =    2427
'TIscr2_MovPop_Supe1 =    2375
'TIscr2_MovPop_Sup4  =      62
'TIscr2_MovPop_Supe4 =      62
'--- 20121101LC (fine)

'20120619LC
Public Enum etp
  PDIR = 0
  PIND = 1
  PINAB = 2
  PINV = 3
End Enum

'20160609LC
Public Enum EFirr
  Firr_None = 0
  Firr_A = 1
  Firr_E = 2
  Firr_V = 3
  Firr_PA = 4
  Firr_S = 5
  Firr_NS = 6
End Enum


Private Const ErrQualifica = 1
Private Const ErrSesso = 2
Private Const ErrDataNascita = 4
Private Const ErrDataAssunzione = 8
Private Const ErrDataCess = 16
Private Const ErrRetrAnnuaFondo = 32
Private Const ErrRetrAnnuaPensione = 64
Private Const ErrPensione = 128
Private Const ErrDataEtaMin = 256
Private Const ErrDataEtaMax = 512
Private Const ErrAnzianitaMax = 1024
Private Const ErrDataNascAss = 2048

Private Const A1_RC = 255 '20120502LC
Private Const A1_PA1 = 1
Private Const A1_PV1 = 2
Private Const A1_PS1 = 3
Private Const A1_PT1 = 4
Private Const A1_PST = 5
Private Const A1_PB1 = 6
Private Const A1_PI1 = 7
'Private Const A2_TOT = 0
'Private Const A2_IND = 1
'Private Const A2_REV = 2

Private Const BB1_INIT = 1
Private Const BB1_PENS = 2
Private Const BB1_RIVALUTA = 3

Private Const CAT_PENS_VEC = 1 '1 Pensione di vecchiaia
  '--- 20140404LC (inizio)
  Private Const CAT_PENS_SOS = 19  '-1 '1 Pensione sostitutiva
  Private Const CAT_PENS_TOT = 20  '-2 '1 Pensione di totalizzazione
  '--- 20140404LC (fine)
Private Const CAT_PENS_ANZ = 5 '2 Pensione di anzianit�
Private Const CAT_PENS_REV = 2 '3 Pensione di reversibilit�
Private Const CAT_PENS_SUP = 3 '4 Pensione ai superstiti
Private Const CAT_PENS_INA = 6 '5 Pensione di inabilit�
Private Const CAT_PENS_INV = 4 '6 Pensione di invalidit�
Private Const CAT_PENS_ALT = 7 '7 Pensione di altro tipo
'--- 20150504LC (inizio)
Private Const CONTR_AA = 0    ' 1  'Attivi con contributi normali
Private Const CONTR_EA = 1    ' 0  'Ex attivi senza contributi
Private Const CONTR_VA = 2    '    'Prosecutori volontari (ENAS)
Private Const CONTR_PB_0 = 3  ' 3  'Pens.di inabilit� senza contributi
Private Const CONTR_PI_0 = 4  ' 4  'Pens.di invalidit� senza contributi
Private Const CONTR_PA_0 = 5  '-1  'Pens.di anz. senza contributi
Private Const CONTR_PS_0 = 6  ' 6  'Pens.sostitutiva senza contributi
Private Const CONTR_PT_0 = 7  ' 7  'Pens.di totalizz. senza contributi
Private Const CONTR_S_0 = 8   ' 0  'Superstiti senza contributi
Private Const CONTR_PW_0 = 9  ' 0  'Pens.di vecch. senza contributi
Private Const CONTR_PW_2 = 10 ' 2  'Pens.di vecch. con contributi x scatti biennali
Private Const CONTR_PW_5 = 11 ' 5  'Pens.di vecch. con contributi x scatti quinquennali
'--- 20150504LC (fine)
Private Const RS_mat = 0
Private Const RS_anStat = 1
Private Const RS_anGruppo = 2
Private Const RS_anTit = 3
Private Const RS_anTC = 4
Private Const RS_anTS = 5
Private Const RS_anSex = 6
Private Const RS_anDNasc = 7
Private Const RS_anNIngr = 8
Private Const RS_anIscN = 9
Private Const RS_anIscInC = 10
Private Const RS_anIscInD = 11
Private Const RS_anCanFinC = 12
Private Const RS_anCanFinD = 13
Private Const RS_anAnzTot = 14
'--- 20150410LC (inizio)  'ex 20140706LC
Private Const RS_anAnzA = 15
Private Const RS_anAnzB = 16
Private Const RS_anMandati = 17   '20150410LC
'-- 20150518LC (inizio)
'Private Const RS_anPriatt = 18    '20150413LC
Private Const RS_anPens = 18
Private Const RS_anPensAUS = 19
Private Const RS_IrpefNI = 20
Private Const RS_SoggNI = 21  '20150612LC
Private Const RS_IvaNI = 22   '20150612LC
Private Const RS_IntNI = 23   '20150612LC
Private Const RS_anSupN = 24
Private Const RS_anSMat = 25
'-- 20150518LC (fine)
'--- 20150410LC (fine)  'ex 20140706LC
Private Const RS_anSDNasc = RS_anSMat + 1
Private Const RS_anSSex = RS_anSMat + 2
Private Const RS_anSCcod = RS_anSMat + 3
Private Const RS_anSRev = RS_anSMat + 4
Private Const RS_andmorte = RS_anSMat + 15
Private Const RS_anCatPens = RS_anSMat + 16
Private Const RS_anPenADcr = RS_anSMat + 17  '20140307LC
Private Const RS_IRPEF = RS_anSMat + 18      '20140307LC
Private Const RS_SOGG = RS_IRPEF + 1
Private Const RS_IVA = RS_IRPEF + 2
Private Const RS_INT = RS_IRPEF + 3

Private Const ST_A = 1   'Attivo
Private Const ST_PAT = 2 'Pensionato attivo
Private Const ST_PNA = 3 'Pensionato non attivo
Private Const ST_S = 4   'Superstiti
Private Const ST_W = 5   'Uscito senza diritto a pensione

'--- 20131129LC (inizio)
Private Const TPENS_B = 10
Private Const TPENS_I = 20
Private Const TPENS_VO = 30
Private Const TPENS_VA = 31
Private Const TPENS_VP = 32
Private Const TPENS_AO = 40
Private Const TPENS_S = 50
Private Const TPENS_T = 60
'--- 20131129LC (fine)

'############
'Da modTIscr2
'############
Public Type TWA
  c0a_z As Double    '0
  c0v_z As Double    '1  '20150504LC
  c0e_z As Double    '2  '20150504LC  ex c0ea_z
  c1i_z As Double    '2
  c1b_z As Double    '3
  c1cvo_z As Double   '5  '20131202LC
  c1cva_z As Double   '5  '20131202LC
  c1cvp_z As Double   '5  '20131202LC
  c1ca_z As Double   '4
  c1cs_z As Double   '6
  c1ct_z As Double   '7
  c1nvo_z As Double   '9  '20131202LC
  c1nva_z As Double   '9  '20131202LC
  c1nvp_z As Double   '9  '20131202LC
  c1na_z As Double   '8
  c1ns_z As Double   '10
  c1nt_z As Double   '11
  s1_z As Double     '12
  sj0_z As Double    '13
  sj1_z As Double    '14
  sj2_z As Double    '15
  sj3_z As Double    '16
  sj4_z As Double    '17
  sj5_z As Double    '18
  sj6_z As Double    '19
  sj7_z As Double    '20
  sj8_z As Double    '21
  sd_z As Double     '22
  w1b_z As Double    '23
  w2b_z As Double    '24  'Non usata?
  w3b_z As Double    '25
  totAtt_z As Double '26
  tot_z As Double    '27
  '---
  c1nvo_p As Double   '29
  c1nva_p As Double   '29
  c1nvp_p As Double   '29
  c1na_p As Double   '28
  c1ns_p As Double   '30
  c1nt_p As Double   '31
  s1_p As Double     '32  'Non usata
  w1b_p As Double    '33
  '--- 20121116LC (inizio)
  w2b_p As Double    '34  'Non usata?
  w3b_p As Double    '35
  '---
  c1nvo_zp As Double  '37
  c1nva_zp As Double  '37
  c1nvp_zp As Double  '37
  c1na_zp As Double  '36
  c1ns_zp As Double  '38
  c1nt_zp As Double  '39
  s1_zp As Double    '40
  w1b_zp As Double   '41
  w2b_zp As Double   '42  'Non usata?
  w3b_zp As Double   '43
  '---
  c1nvo_zm As Double  '45
  c1nva_zm As Double  '45
  c1nvp_zm As Double  '45
  c1na_zm As Double  '44
  c1ns_zm As Double  '46
  c1nt_zm As Double  '47
  s1_zm As Double    '48
  w1b_zm As Double   '49
  w2b_zm As Double   '50  'Non usata?
  w3b_zm As Double   '51
  pensTot As Double     'Mod MDG
  numMedPens As Double  'Mod MDG
  eta As Integer        'Mod MDG
  anno As Integer       'Mod MDG
  '--- 20121116LC (fine)
  '--- 20181108LC (inizio) MEF
  w00_mef As Double
  '--- 20181108LC (fine) MEF
  c0e_dh As Double    '20181121LC-2  (anzianit� media da togliere per i silenti generati)
End Type

'---
'Public Const TWB_eg2_z = 0
'Public Const TWB_bg2_z = 1
'Public Const TWB_ig2_z = 2
'Public Const TWB_max = 2 * 0
Public Type TWB
  vg2 As Double  '20150504LC
    '--- 20150506LC (inizio)
    vg2sm As Double     'base provvigionale
    vg2sm2ns As Double  'base provvigionale fino al massimale (non sterilizzata)  '20160414LC
    vg2sm2s As Double   'base provvigionale fino al massimale (sterilizzata)      '20160414LC
    vg2cSogg As Double  'contributo soggettivo
    vg2cAss As Double   'contributo assistenziale
    vg2cMat As Double   'contributo di maternit�
    vg2rcV As Double    'montante attualizzato
    vg2p1 As Double     'pensione unitaria quota A
    vg2p2 As Double     'pensione unitaria quota B
    '--- 20150506LC (fine)
  eg2 As Double
  bg2 As Double
  ig2 As Double
  '--- 20150504LC (inizio)
  atryPensVA As Long
  annoPensVA As Long
  tipoPensVA As Long
  stipoPensVA As Long '20120531LC
  abbPensVA As Double '20160308LC
  '--- 20150504LC (fine)
  '--- 20140111LC-12 (inizio) spostato qui
  atryPensEA As Long  '20130919LC
  annoPensEA As Long
  tipoPensEA As Long
  stipoPensEA As Long '20120531LC
  abbPensEA As Double  '20160308LC
  '--- 20140111LC-12 (fine(
End Type
'---
Public Const TWC_SIZE = 7  '20121204LC


Public Type TIscrSup2
  mat As Long
  DNasc As Date
  'sex As String * 1  '20140424LC (non era pi� usato)
  s As Byte      'Sesso
  tipGen As Byte
  rev As Double
  bDNascSupNull As Byte
  '---
  y As Double    'Et�
  c0 As Double   'Coeff. di interp.iniz.  per le prob. di morte (1� superstite)
  c1 As Double   'Coeff. di interp.finale per le prob. di morte (1� superstite)
  ymax As Double 'Massima et� raggiungibile dal superstite
  tipSup As Byte 'Tipo di superstite (1:V, 4:O)  '20140425LC (rinominato "gruppo")
End Type

'20080912LC
Public Type TIscr2pens1
  fa1 As Double     '20131122LC (frazione integrazione al minimo)
  fa2 As Double     '20131126LC (frazione contributi figurativi)
  fc As Double      '20121115LC (frazione contributiva)
  p As Double       'Pensione
  z As Double       'Popolazione finale
  zm As Double      'Popolazione media
  zp As Double      'Popolazione media*Pensione
  zcs As Double     'Popolazione media*Contibuto di solidariet� '20120514LC
End Type

'20080912LC
Public Type TIscr2pens
  tp As Integer       'Tipo di pensione
    kzm As Long '20121031LC
    kzp As Long '20121031LC
  stp As Integer      'Sottotipo di pensione '20120531LC
  gr0 As Byte         'Gruppo di provenienza
  t0 As Integer       'Anno di provenienza
  t1 As Integer       'Anno ultimo supplemento
  x0 As Double        'Et� iniziale raggiunta nell'anno
  '--- 20181121LC-1 (inizio)
  h0a As Double       'Anzianit� iniziale raggiunta nell'anno
  h0b As Double       'Anzianit� a fine anno per i pensionati attivi
  '--- 20181121LC-1 (fine)
  tb() As TIscr2pens1 'Dati
End Type

Public Type TIscr2_Anz '20111226LC
  h As Double
    h_retr As Double
    h_contr As Double
    h_ante As Double
      h_retr_ante  As Double
        h0_retr_ante As Double
        h1_retr_ante As Double
      h_contr_ante As Double
        h2_contr_ante As Double
        h3_contr_ante As Double
    h_post As Double
      h_retr_post As Double
        h0_retr_post As Double
        h1_retr_post As Double
      h_contr_post As Double
        h2_contr_post As Double
        h3_contr_post As Double
End Type

Public Type TIscr2_Stato
  sm As Double        'Vettore dei redditi IRPEF
  sm2ns As Double     '20080611LC Vettore dei redditi IRPEF fino al tetto
  sm2s As Double      '20150615LC  Vettore dei redditi IRPEF fino al tetto, sterilizzati al 2011
  sm3ns As Double     '20150506LC  reddito per contribuzione volontaria
  sm3s As Double      '20150615LC  reddito per contribuzione volontaria, sterilizzato al 2011
  iva As Double       'Vettore dei volumi di affari
  c_sogg0a1 As Double 'Vettore dei contributi soggetivi (minimo)      '20120514LC
  c_sogg0a2 As Double 'Vettore dei contributi soggetivi (volontari)   '20120514LC
  c_sogg1a As Double  'Vettore dei contributi soggetivi (1� scaglione)
  c_sogg2a As Double  'Vettore dei contributi soggetivi (2� scaglione) '20120113LC
  c_sogg3a As Double  'Vettore dei contributi soggetivi (3� scaglione)
  c_int0a As Double   'Vettore dei contributi integrativi (minimo)
  c_int1a As Double   'Vettore dei contributi integrativi (saldo retrocedibile)     | FIRR (1� scaglione)
  c_int2a As Double   'Vettore dei contributi integrativi (saldo non retrocedibile) | FIRR (2� scaglione)  '20121018LC 'ex 20120521LC
  c_int3a As Double   '20150512LC                                                   | FIRR (3� scaglione)
  c_ass0a As Double   'Vettore dei contributi assistenziali (minimo)
  c_ass1a As Double   'Vettore dei contributi assistenziali (saldo)
  c_mat As Double     'Vettore dei contributi di maternit�
  c_tot As Double     'Vettore dei contributi totali
  '--- 20131126LC
  c_sogg_fig0 As Double
  c_sogg_fig1 As Double
  c_int_retr0 As Double
  c_int_retr1 As Double
  c_int_ass0 As Double
  c_int_ass1 As Double
  c_int_fig0 As Double
  c_int_fig1 As Double
  '--- 20120114LC
  om_rcU(eom.om_max) As Double  '20131122LC  'Matrice ausiliaria contributi complessivi
  om_rcV(eom.om_max) As Double  '20131122LC  'Matrice ausiliaria Montante contributi complessivi
  '--- 20121105LC
  bIsArt25   As Boolean '20121105LC
  bIsMisto   As Boolean 'flag misto contr.   '20120515LC
  bIsProRata As Boolean '20121105LC
  ic         As Integer 'flag contr.rid.     '20120502LC
  ico        As eom     'indice contributi   '20121105LC
  ipr        As Integer 'indice pro-rata     '20121105LC
  mi         As Integer 'mesi di iscrizione  '20121105LC
  ALR        As Double  'al.retr.integr.     '20120508LC
  '---
  it         As Byte '--- 20120112LC
  stato      As Integer
  caus       As Integer
  gruppo1    As Byte    'Gruppo di appartenenza
  bPensAtt   As Boolean '20121009LC
  napa       As Integer 'Numero di anni per la pensione aggiuntiva
  napa0      As Integer 'Numero precedente di anni per la pensione aggiuntiva
  tapa       As Integer 'Anno per la pensione aggiuntiva
  '--- 20121103LC
  pens1a1 As Double    'Pensione di riferimento per riscatto
  pens1a2 As Double    'Pensione di riferimento per riscatto
  '--- Anzianit�
  anz As TIscr2_Anz    '20111226LC
  AnzTot As Double
  hs As Double              'Anzianit� di servizio (AGO)
  '20111213LC (inizio)
  h_retr_ante1 As Double    'Anzianit� di servizio retributiva 1 (ante prorata o senza prorata)
  h_retr_post1 As Double    'Anzianit� di servizio retributiva 2 (con prorata)
  h_contr_ante1 As Double   'Anzianit� di servizio contributiva 1 (ante prorata o senza prorata)
  h_contr_post1 As Double   'Anzianit� di servizio contributiva 2 (con prorata)
  '20111213LC (fine)
  hq As Double              'Anzianit� nella qualifica attuale
  hf As Double              'Anzianit� nel fondo
  nMandati2 As Double       '20150410LC
  bAzzeraReddito As Byte    '20150504LC
End Type

Public Type TIscr2
    mat As Long
    gruppo0 As Byte '20121105LC
    gruppo00 As Byte '20150504LC
    dElab As Date
    annoIniz As Integer
    DCan As Date
    bModArt26 As Byte '20081016LC
  iQ As Integer
  iSQ As Long  '20121018LC
  iSQT As Long '20121018LC
  iTC As Long  '20080305LC
  iTS As Long  '20080614LC: Suddivisione
  '--- EX TIscr2 (inizio) ---
  Qualifica As Long    'Qualifica degli appartenenti al sottogruppo
  Carriera As Long     '20080305LC Carriera (0:standard, 1:alta)
  sesso As Long        'Sesso degli appartenenti al sottogruppo
  DNasc As Date        'Data di nascita
  dAssunz As Date      'Data di assunzione
    annoAss As Integer 'Anno di assunzione
  dIscrFondo As Date   'Data di iscrizione nel fondo
  xan As Integer       'Et� anagrafica
  xr   As Double       'Et� "demografica" con parte reale
  dx As Integer        '20080611LC 'Age shifting
  '---
  annoInizCR As Integer 'Anno di inizio contribuzione ridotta  '20161112LC
  annoFineCR As Integer 'Anno di cessazione contribuzione ridotta
  PensAUS As Integer    'Anno di riattivazione
  'annoInizCV As Integer '20150504LC 'Anno di inizio contribuzione volontaria (ENAS)
  'contrVol As Double   '20150504LC 'Contributo volontario  '20150518LC (commentato)
  n As Double           'Numero di dipendenti iniziali
  nRes As Double        '20150525LC  'Numero di dipendenti residui (tolti i riattivati)
  '---
  qab2 As Double
  qae2 As Double
  qav2 As Double  '20150504LC
  qai2 As Double
  qaw2 As Double
  qad1 As Double
  qvd1 As Double  '20150504LC
  qed1 As Double
  qbd As Double
  qid As Double
  qpad As Double
  qpnd As Double
  '--- 20121115LC (inizio)
  pwDB As Double       'Pensione letta su DB
  pwDB_fc As Double    '20121115LC  'Frazione pensione contributiva letta su DB
  pwDB_fa1 As Double   '20121115LC  'Frazione pensione assistenziale letta su DB
  '---
  a_tpind As Byte      '20080915LC
  a_hind As Double     '20080912LC
  a_pind As Double
  a_pind_fc As Double  '20121115LC
  a_pind_fa1 As Double  '20131202LC
  '---
  a_tprev As Byte      '20080915LC
  a_hrev As Double     '20080912LC
  a_prev As Double
  a_prev_fc As Double  '20121115LC
  a_prev_fa1 As Double  '20131202LC
  '--- 20150504LC (inizio)
  va_tpind As Byte
  va_hind As Double
  va_pind As Double
  va_pind_fc As Double
  va_pind_fa1 As Double
  '---
  va_tprev As Byte
  va_hrev As Double
  va_prev As Double
  va_prev_fc As Double
  va_prev_fa1 As Double
  '--- 20150504LC (fine)
  ea_tpind As Byte      '20080915LC
  ea_hind As Double     '20080912LC
  ea_pind As Double
  ea_pind_fc As Double  '20121115LC
  ea_pind_fa1 As Double  '20131202LC
  '---
  ea_tprev As Byte      '20080915LC
  ea_hrev As Double     '20080912LC
  ea_prev As Double     '20080924LC
  ea_prev_fc As Double  '20121115LC
  ea_prev_fa1 As Double  '20131202LC
  '--- 20121115LC (fine)
  irpefNI As Double    'Reddito IRPEF iniziale dei nuovi entrati
  ivaNI As Double      'Volume di affari iniziale dei nuovi entrati
  FRAZ_PC0 As Double
  FRAZ_PNA0 As Double
  FRAZ_PNV0 As Double
  FRAZ_TZ0 As Double
  qm As Double         'Montante accantonamenti fondo
  qaS As Double        'Retribuzione media
  NMR As Byte
  NTR As Byte
  st As String
  catPens As Byte
  PensAnno As Integer  '20150612LC
  t As Integer
  t0 As Integer '20121105LC
    rr_tmin As Integer
    rr_tmax As Integer
  annoPNAA As Integer
  annoPNAV As Integer
  c0 As Double
  c1 As Double
  coe0 As Double
  coe1 As Double
  d0 As Double
  d1 As Double
  doe0 As Double
  doe1 As Double
  '---
  anno0 As Integer
  anno1 As Integer
    bNuovoEntrato As Boolean '20121105LC
  n0 As TWA
  zd As Double
  zs As Double
  zs1 As Double  '20121209LC
  ZW As Double
  '---
  ZMAX As Byte
  '---
  AnniElab1 As Integer '20080416LC - rinominato
  AnniElab2 As Integer '20080416LC
  TipoElab As Integer
  '20120214LC (inizio)
  'PRC_PV_ATT As Double '20080611LC
  'PRC_PA_ATT As Double '20080611LC
  'PRC_PC_ATT As Double '20120214LC
  'PRCTTLZ As Double
  '20120214LC (fine)
  '--- 20120210LC
  cv4 As TAppl_CV4
  '--- 20120112LC (afpnew)
  s() As TIscr2_Stato
  '---
  nSup As Long
  sup() As TIscrSup2
  'RestContrT As Double  '20150512LC (commentato)  'ex RC2mat  'ex 20120117LC
  '20120114LC (inizio)
  om_rcU() As Double    'Vettore Dati ausiliarii contributi complessivi
  om_rcV() As Double    'Vettore Dati ausiliarii Montante contributi complessivi
  '20120114LC (fine)
  rr_dhMax As Double    'Incremento di anz. max per ric./risc.
  rr_dhEff As Double    'Incremento di anz. eff per ric./risc.
  rr_dhEff0 As Double  'Incremento di anz. eff per ric./risc. (iniziale) '20111209LC
  rr_crr As Double      'Riscatto
  '--- 20080812LC ---
  anno As Long
  qsd As Double
  tau1 As Long
  tau0 As Long
  wa() As TWA '20121028LC
  wb() As TWB '20121028LC
  wc() As Double '20121031LC
  '--- 20080813LC ---
  atryPensA As Long  '20130919LC
  annoPensA As Long
  tipoPensA As Long
  stipoPensA As Long '20120531LC
  abbPensA As Double  '20160308LC
  '--- 20140111LC-12 (inizio) spostato in TWB
  'atryPensEA As Long  '20130919LC
  'annoPensEA As Long
  'tipoPensEA As Long
  'stipoPensEA As Long '20120531LC
  '--- 20140111LC-12 (fine)
  '--- 20080912LC ---
  nPens As Long      'Numero pensionati
  lp() As TIscr2pens 'Lista pensionati
  '--- 20120110LC ---
  bFalsoEA As Boolean
  '--- 20130609LC (riorganizzato per debug)
  Pens1r As Double
  Pens1C As Double
  ax As Double
  ax0 As Double
  '--- Informazioni per pensione minima
  bPmin As Integer   '20130604LC
  nAbbArt25 As Integer
  nRMPmin As Integer
  RMPmin As Double
  PminBase As Double   'ex Pmin
  PMinAbb As Double    '20130604LC
  PMinCalc As Double
  PSost As Double
  Pfig As Double      '20131126LC
  Pcalc As Double     '20131202LC
  bModPmin As Byte    '201404904LC
  nMandati1 As Double '20150410LC
  annoUC As Integer   '20150609LC
  cmNI As Double      '20150616LC
  FirrU As Double     '20160609LC
  FirrV As Double     '20160609LC
  RcIt As Byte    '20170724LC
  RcKK As Byte    '20170805LC
  RcNa As Double  '20170801LC
  RcNe As Double  '20170801LC
End Type
Public p_diaGen() As TIscr2Aux  '20150521LC
Public p_diaColl As Collection  '20150612LC


Public Type TIscrSupAux
  mat As Long
  DNasc As Date
  s As Byte
  tipGen As Byte
  rev As Double
  bDNascSupNull As Byte
End Type

Public Type TIscr2Aux
  iErr As Long    'iErr
  mat As Long     'IdDip
  gr As Byte      'IdAz
  sex As Byte     'Sesso
  tit As Byte     'Categ
  DNasc As Date   'DNasc
  DIscr As Date   'DIscrFondo
  dCess As Date   'DCessaz
  c1 As Byte      'c1
  c2 As Byte      'c2
  h As Double     'hmf
  ha As Double    'hmfa
  hb As Double    'hmfb
  n As Double     'n
  nMan As Double  'Mandati
  smNI As Double  'Mandati
  sm() As Double
  om0 As Double
  qm0 As Double
  cmNI As Double
  cm() As Double
  ivaNI As Double  'Mandati
  iva() As Double
  inteNI As Double  'Mandati
  inte() As Double
  PensTipo As Byte
  PensAnno  As Integer
  PensAUS As Integer
  Pensione As Double
  nSup  As Byte
  sup() As TIscrSupAux
  annoUC As Integer  '20150612LC
  z_sKey As String  '20160411LC
  z_nRiatt As Long  '20160411LC
  RcIt As Byte  '20170724LC
End Type
Private p_dia As TIscr2Aux

Public Type valTriennio
  anno1 As Long
  anno2 As Long
  anno3 As Long
End Type

'------ Strutture per Export MEF ------
'--------------------------------------
Public Type exportMefData
  anno As Integer
  stato As Integer
  tipoPensione As Integer
  sesso As Integer
  eta As Integer
  newPos31Dic As Double
  reddMedNewPos As Double
  pensMedNewPos As Double
  anzMedNewPos As Double
  pos31Dic As Double
  redMedPosDic As Double
  pensMedPosDic As Double
  anzMedPosDic As Double
  contrMedPosDis As Double
End Type

Public Type dipExportMef
  data(130) As exportMefData
End Type
'-------------------------------------


Public Function if6_AnnoFineCR(ByRef DNasc As Date, ByRef DAss As Date, ByRef cvEtaR As Double, _
    ByRef cvXMaxR As Double, ByRef cvAnniR As Double) As Integer
'20120112LC - afpnew
  Dim annoFineCR%
  Dim d As Date
  
  annoFineCR = 0
  If 1 = 1 Then
    'If Year(DAss) < OPZ_INT_CV_AMIN Then
    If Year(DAss) < 1961 Then
      If DateAdd("yyyy", cvEtaR + 1, DNasc) > DAss Then
        annoFineCR = Year(DAss) + cvAnniR
      Else
        annoFineCR = 0
      End If
    ElseIf OPZ_MOD_ART22_MODEL = 1 And Year(DAss) >= OPZ_MOD_ART22_ANNO And OPZ_MOD_ART22_ANNO > 0 Then
      annoFineCR = Year(DAss) + cvAnniR
      If DateAdd("yyyy", cvXMaxR, DNasc) > DAss Then
        If annoFineCR > Year(DNasc) + cvXMaxR + 1 Then
          annoFineCR = Year(DNasc) + cvXMaxR + 1
        End If
      Else
        If annoFineCR > Year(DNasc) + cvXMaxR Then
          annoFineCR = Year(DNasc) + cvXMaxR
        End If
      End If
    Else
      If DateAdd("yyyy", cvEtaR + 1, DNasc) > DAss Then
        annoFineCR = Year(DAss) + cvAnniR
      Else
        annoFineCR = 0
      End If
    End If
    If annoFineCR < Year(DAss) Then
      annoFineCR = 0
    End If
  Else 'AfpNew
    If cvEtaR > 0 And cvEtaR < 99 Then
      If cvXMaxR > cvEtaR And cvAnniR > 0 Then
        d = DateAdd("yyyy", cvEtaR + 1, DNasc)
        If DAss < d Then
          annoFineCR = Year(DNasc) + cvXMaxR + 1
          If annoFineCR > Year(DAss) + cvAnniR Then
            annoFineCR = Year(DAss) + cvAnniR
          End If
        End If
      End If
    End If
  End If
  if6_AnnoFineCR = annoFineCR
End Function


Public Function TAppl_Elab32(ByRef appl As TAppl, ByRef glo As TGlobale, tb1#(), nItRand&, iop&, req As Object, frm As frmElab) As Boolean
  Dim bDataChanged As Boolean, bElab As Boolean, bStimaMI As Boolean '20130306LC
  'Dim bRiatt As Byte  '20150518LC (eliminato)  'ex 20150413LC
  Dim RC_MODEL As Byte  '20170801LC
  Dim ia As Integer  '20160411LC
  Dim bStop&, fo&, i&, iBlocco&, iErr&, ijk&, ijk0&, iLS&, iQ&, iSex&, iTable&, iTC&, iTit&, iTS& '20121028LC
  Dim j&, jBlocco&, k&, nBlocchi&, nRec1&, tau&, x&, XMAX&, XMIN&  '20140425LC  'ex 20140422LC  'ex 20140213LC
  Dim fact1#, fact2#, hNI#, irpefNI#, ivaNI#, tim0#, tim1#, tim2#, y#, zaa0#, zaa1#, zpa0#, zpa1#  '20140522LC
  Dim dNow As Date
  Dim s$, SQL$, sz$, sErr$, szGruppo$, szTableDI$, szTableNI$  '20140317LC
  Dim rsMat As New ADODB.Recordset
  Dim rsNip As New ADODB.Recordset
  Dim grMat As Variant
  Dim dip As TIscr2
  Dim dip1 As TIscr2  '20170801LC
  Dim dip0 As TIscr2
  Dim ixdb() As Long
  '--- 20140312LC
  Dim buf1() As Double
  Dim buf2() As Double
  Dim fr&  '20140605LC
  '--- 20161111LC
  Dim anno As Integer
  Dim tauMin As Integer
  Dim tauMax As Integer
  Dim fAut As Long
  Dim fErr As Long
  Dim nIter As Long
  Dim contributi As Double
  Dim pensioni As Double
  Dim tRivMont As Double
  Dim ZARR As Double
  Dim sFileAut As String
  Dim sFileErr As String
  '---
  
  Dim vecDip() As dipExportMef
  
  If ON_ERR_EH = True Then On Error GoTo ErrorHandler
  
  sErr = ""
  '--- 20161112LC (inizio)
  fErr = FreeFile
  sFileErr = "AFP_ERR.TXT"
  Open sFileErr For Output As #fErr
  Close #fErr
  '--- 20161112LC (fine)
  '--- 20161111LC (inizio)
  If Abs(OPZ_MOD_REG_2) >= 1 Then  '20180122LC
    nIter = OPZ_MOD_REG_2_NITMAX
    ZARR = 10 ^ OPZ_MOD_REG_2_CIFRE_TOLL
    SQL = "DELETE FROM C_CV WHERE cvNome LIKE '_auto1_%'"
    Call glo.db.Execute(SQL, nRec1)
    appl.iterTipo = 1
    fAut = FreeFile
    sFileAut = "AFP_AUTO" & appl.iterTipo & ".TXT"
    Open sFileAut For Output As #fAut
    Print #fAut, "iter; cvNome; anniVar; annoVarMin; annoVarMax; diffMin; diffMax; saldo"
    Close #fAut
  Else
    nIter = 0
    appl.iterTipo = 0
  End If
  appl.iterCvNome = LCase(OPZ_CV_NOME)
  For appl.iterNum = 0 To nIter
    frm.txtIter.Text = appl.iterNum
    frm.txtCvNome.Text = LCase(appl.iterCvNome)
    If appl.iterTipo <> 0 Then
      If 1 = 2 Then  '20161114LC
        appl.iterAnniVar = 0#
        appl.iterAnnoVarMin = 0
        appl.iterAnnoVarMax = 0
        appl.iterDiffMin = 0#
        appl.iterDiffMax = 0#
        appl.iterSaldo = 0#
      End If
      '---
      If appl.iterNum > 0 Then
        '--- Ricalcola le produttorie
        Call LeggiCoeffVar3(appl.CoeffVar(), appl.t0)
        '--- Crea il nome del nuovo gruppo di coefficienti variabili
        appl.iterCvNome = LCase("_auto" & appl.iterTipo & "_" & appl.iterNum)
        '--- Scrive i dati del nuovo gruppo di coefficienti variabili
        SQL = "SELECT C_CV.*"
        SQL = SQL & " FROM C_CV"
        SQL = SQL & " WHERE cvNome='" & OPZ_CV_NOME & "'"
        SQL = SQL & " ORDER BY cvAnno ASC"
        Call rsMat.Open(SQL, glo.db, adOpenForwardOnly, adLockReadOnly)
        '---
        SQL = "SELECT *"
        SQL = SQL & " FROM C_CV"
        SQL = SQL & " WHERE 1=2"
        Call rsNip.Open(SQL, glo.db, adOpenKeyset, adLockOptimistic)
        '---
        Do Until rsMat.EOF
          anno = rsMat.Fields("cvAnno").Value
          Call rsNip.AddNew
          For i = 0 To rsMat.Fields.Count - 1
            rsNip.Fields(i).Value = rsMat.Fields(i).Value
          Next i
          rsNip.Fields("cvNome").Value = appl.iterCvNome
          If appl.iterAnnoVarMin <= anno And anno <= appl.iterAnnoVarMax Then
            rsNip.Fields("cvTrmc0").Value = appl.CoeffVar(anno, ecv.cv_Trmc0)
            rsNip.Fields("cvTrmc1").Value = appl.CoeffVar(anno, ecv.cv_Trmc1)
            rsNip.Fields("cvTrmc2").Value = appl.CoeffVar(anno, ecv.cv_Trmc2)
            rsNip.Fields("cvTrmc3").Value = appl.CoeffVar(anno, ecv.cv_Trmc3)
            rsNip.Fields("cvTrmc4").Value = appl.CoeffVar(anno, ecv.cv_Trmc4)
            rsNip.Fields("cvTrmc5").Value = appl.CoeffVar(anno, ecv.cv_Trmc5)
            rsNip.Fields("cvTrmc6").Value = appl.CoeffVar(anno, ecv.cv_Trmc6)
          End If
          Call rsNip.Update
          Call rsMat.MoveNext
        Loop
        Call rsNip.Close
        Call rsMat.Close
      End If
    End If
  '--- 20161111LC (fine)
    '--- 20161114LC (inizio)
    appl.iterAnniVar = 0#
    appl.iterAnnoVarMin = 0
    appl.iterAnnoVarMax = 0
    appl.iterDiffMin = 0#
    appl.iterDiffMax = 0#
    appl.iterSaldo = 0#
    '--- 20161114LC (fine)
    '--- 20140609LC (inizio)
    If OPZ_SUDDIVISIONI_MODEL = 0 And OPZ_SUDDIVISIONI_DT > 0 Then
      fr = FreeFile
      Open "AFP_CLASSI.TXT" For Output As #fr
      Print #fr, "matricola;reddito;classe"
    End If
    '--- 20140609LC (fine)
    '--- 20140317LC (inizio)
    szTableDI = "Iscritti"
    szTableNI = "NuoviIscritti"
    '--- 20140317LC (fine)
    '--- 20140422LC (inizio)  'ex 20140312LC
    'SPOSTATO SOTTO
    '...
    'End If
    '--- 20140422LC (fine)
    '20080919LC (inizio)
    appl.nPensMat = 0
    appl.nPensMax = 0
    '20080919LC (fine)
    '20120601LC (inizio)
    If OPZ_DEBUG = 1 Then
      fo = FreeFile
      Open "afp_debug.txt" For Output As #fo
      Close #fo
    End If
    '20120601LC (fine)
    '*******************
    'Test su AFP_OPZ.INI
    '*******************
    '20120312LC (inizio)
    If OPZ_ABB_QM_MODEL = 2 Then
      ReDim appl.BDA(0 To P_DUECENTO, 0 To OPZ_OMEGA, 0 To 3)        '20130301LC
      Call LeggiBDA(glo.db, OPZ_ABB_QM_BASE, appl.t0 + 1, appl.BDA)  '20130301LC
    End If
    '20120312LC (fine)
    '--- 20130306LC (inzio)  'ex 20120220LC
    'For i = 2 To 1 Step -1 'ex 4
    '  If frm.optNI(i).Value Then Exit For
    'Next i
    i = req("r" & P_niTipo)
    appl.parm(P_niTipo) = i
    '--- 20130306LC (fine)
    '--- 20161111LC (inizio)
    '************************************
    'Messaggio di controllo sulle opzioni
    '************************************
    If appl.iterNum = 0 Then
    '--- 20161111LC (fine)
      '--- 20120110LC (inizio)
      '20120329LC
      'If appl.parm(P_niTipo) <> 0 And OPZ_MISTO_REDD_PENS = 2 Then
      If appl.parm(P_niTipo) <> 0 And OPZ_MISTO_DEB_MAT = 1 Then
        sErr = "I nuovi iscritti non sono ammessi se OPZ_MISTO_DEB_MAT = 1"
        GoTo ExitPoint
      ElseIf appl.parm(P_niTipo) >= 2 Then  '20140522LC
      '--- 20120110LC (fine)
        If 1 = 2 And OPZ_SUDDIVISIONI_MODEL <> 0 Then
          sErr = "Per i nuovi iscritti OPZ_SUDDIVISIONI_MODEL deve valere 0 o -5 in AFP_OPZ.INI"
          GoTo ExitPoint
        ElseIf glo.MJ = MI_ASP Then  '20130307LC
        ElseIf frm.cbxNip.ListIndex = 0 Then
          sz = sz & "Verranno elaborati i nuovi iscritti per rimpiazzo degli attivi esistenti" & vbCrLf
        '--- 20150522LC (inizio)
        ElseIf 1 = 1 Then 'frm.Check1(P_niP).Value = vbChecked Then
        '  sz = sz & "Verranno elaborati i nuovi iscritti puntuali senza rimpiazzo degli attivi esistenti" & vbCrLf
        '--- 20150522LC (fine)
        '20080617LC (inizio)
        ElseIf UCase(Left(frm.cbxNip.Text, 1)) = "A" Then
          sErr = "Per le basi di nuovi iscritti puntuali che iniziano con la lettera A bisogna selezionare l'opzione SOLO NUOVI ISCRITTI PUNTUALI"
          GoTo ExitPoint
        '20080617LC (fine)
        Else
          sz = sz & "Verranno elaborati i nuovi iscritti puntuali con rimpiazzo degli attivi esistenti" & vbCrLf
        End If
      End If
        '--- 20140404LC (inizio)
      If OPZ_ABB_QM_MODEL <> 0 Then
        sz = sz & "OPZ_ABB_QM_MODEL=" & OPZ_ABB_QM_MODEL & "(" & OPZ_ABB_QM_PRM_1 & ";" & OPZ_ABB_QM_PRM_2 & ";'" & OPZ_ABB_QM_BASE & "';" & OPZ_ABB_QM_SUP & ")" & vbCrLf   '20130611LC  'ex 20120312LC
      End If
      '---  20160419LC-2 (inizio)
      If OPZ_CV_NOME <> "BASE" Then
        sz = sz & "OPZ_CV_NOME=" & OPZ_CV_NOME & vbCrLf
      End If
      '---  20160419LC-2 (fine)
      '--- 20160415LC (commentato)
      'If OPZ_EA_ABBQ_REDDMIN < 9999999# Or OPZ_EA_ABBQ_PERC <> 1# Then  'Abbattimento probabilit� di diventare EA (reddito minimo)
      '  sz = sz & "OPZ_EA_ABBQ_REDDMIN=" & OPZ_EA_ABBQ_REDDMIN & " (OPZ_EA_ABBQ_PERC=" & OPZ_EA_ABBQ_PERC & ")" & vbCrLf
      'End If
      '---
      If OPZ_ELAB_CODA <> 0 Then
        sz = sz & "OPZ_ELAB_CODA =" & OPZ_ELAB_CODA & vbCrLf
      End If
      '---
      If OPZ_INTERP_H <> 0 Or OPZ_INTERP_X <> 0 Then
        sz = sz & "OPZ_INTERP_H=" & OPZ_INTERP_H & " (OPZ_INTERP_X=" & OPZ_INTERP_X & ")" & vbCrLf
      End If
      '---
      If OPZ_MISTO_CCR_MODEL <> 0 Then
        sz = sz & "OPZ_MISTO_CCR_MODEL=" & OPZ_MISTO_CCR_MODEL & vbCrLf
      End If
      '---
      If 1 = 2 And OPZ_MISTO_PMIN_MODEL <> 1 Then '20160415LC  '201404040LC  'ex 20130611LC
        sz = sz & "OPZ_MISTO_PMIN_MODEL=" & OPZ_MISTO_PMIN_MODEL _
                & "(" & OPZ_MISTO_PMIN_ABB _
                & ";" & OPZ_MISTO_PMIN_NRM _
                & ";" & OPZ_MISTO_PMIN_ART25 _
                & ";" & OPZ_MISTO_PMIN_RESTMC _
                & ";" & OPZ_MISTO_PMIN_X4 _
                & ";" & OPZ_MISTO_PMIN_H4 _
                & ")"
        If OPZ_MISTO_RETRIB_MIN = 1 Then  '20131123LC (ex OPZ_MISTO_MODEL=3)
          sz = sz & "(attenzione! Sconsigliato se OPZ_MISTO_RETRIB_MIN=1)" & vbCrLf
        Else
          sz = sz & vbCrLf
        End If
      End If
      '---
      If 1 = 2 Then  '20160415LC
        sz = sz & "OPZ_MISTO_SOGG(" & OPZ_MISTO_SOGG_VOL_A _
                & ";" & OPZ_MISTO_SOGG_VOL_P _
                & ";" & OPZ_MISTO_SOGG_SOL_P1 _
                & ";" & OPZ_MISTO_SOGG_SOL_P2 _
                & ";" & OPZ_MISTO_SOGG_SOL_P3 _
                & ";" & OPZ_MISTO_SOGG_SOL_ANNI _
                & ";" & OPZ_MISTO_SOGG_RID_F1 & ")" & vbCrLf
      End If
      '---
      If OPZ_OMEGA <> 120 Then '20081022LC
        sz = sz & "OPZ_OMEGA=" & OPZ_OMEGA & vbCrLf
      End If
      '--- 20161126LC (inizio)
      If (OPZ_PENS_MODEL = 1 Or OPZ_PENS_MODEL = 2) Then
        sz = sz & "OPZ_PENS_MODEL=" & OPZ_PENS_MODEL & " (SP0=" & OPZ_PENS_SP0 & ")" & vbCrLf
        If OPZ_PENS_C0 <> 1 Then
          Call MsgBox("OPZ_PENS_C0<>1")
          Stop
        End If
      ElseIf (OPZ_PENS_MODEL = 3) Then
        sz = sz & "OPZ_PENS_MODEL=" & OPZ_PENS_MODEL & " (C0=" & OPZ_PENS_C0 & ")" & vbCrLf
        If OPZ_PENS_SP0 <> 0 Then
          Call MsgBox("OPZ_PENS_SP0<>0")
          Stop
        End If
      End If
      '--- 20161126LC (fine)
      If OPZ_SUDDIVISIONI_MODEL <> 0 Then
        '20140603LC
        sz = sz & "OPZ_SUDDIVISIONI_MODEL=" & OPZ_SUDDIVISIONI_MODEL & "(" & OPZ_SUDDIVISIONI_MR & "," & OPZ_SUDDIVISIONI_DT & ")" & vbCrLf
      End If
      '---  20140424LC
      If OPZ_SUP_MODEL <> 0 Or OPZ_SUP_DZ <> 0 Then
        sz = sz & "OPZ_SUP_MODEL=" & OPZ_SUP_MODEL & " (OPZ_SUP_DZ=" & OPZ_SUP_DZ & ")" & vbCrLf
      End If
      '--- 20140404LC (fine)
      '--- 20140320LC (inizio)
      If 1 = 2 And OPZ_PENS_ANZ_MODEL <> -1 Then '20120302LC
        sz = sz & "OPZ_PENS_ANZ_MODEL=" & OPZ_PENS_ANZ_MODEL & "(" & OPZ_PENS_ANZ_HABB & ")" & vbCrLf
      End If
      If 1 = 2 And OPZ_PENS_ATT_MODEL <> 0 Then '20120302LC
        sz = sz & "OPZ_PENS_ATT_MODEL=" & OPZ_PENS_ATT_MODEL & "(" & OPZ_PENS_ATT_ANNO & "," & OPZ_PENS_ATT_FRAZ & ")" & vbCrLf '20121008LC
      End If
      If 1 = 2 And OPZ_Pveo30_19810128 <> 30 And OPZ_Pveo30_19810128 <> 0 Then
        sz = sz & "OPZ_Pveo30_19810128=" & OPZ_Pveo30_19810128 & vbCrLf
      End If
      If 1 = 2 And OPZ_MOD_ART22_MODEL <> 0 Then '20080709LC
        sz = sz & "OPZ_MOD_ART22_MODEL=" & OPZ_MOD_ART22_MODEL & vbCrLf
      End If
      If 1 = 2 And OPZ_MOD_ART25_MODEL <> 0 Then '20111212LC
        'sz = sz & "OPZ_MOD_ART25_MODEL=" & OPZ_MOD_ART25_MODEL & "(" & OPZ_MOD_ART25_ANNO & " , " & OPZ_MOD_ART25_DECR_ANZTOT & ")" & vbCrLf
        sz = sz & "OPZ_MOD_ART25_MODEL=" & OPZ_MOD_ART25_MODEL & vbCrLf '20121010LC
      End If
      If 1 = 2 And OPZ_MOD_ART26_MODEL <> 0 Then '20080709LC
        sz = sz & "OPZ_MOD_ART26_MODEL=" & OPZ_MOD_ART26_MODEL & vbCrLf
      End If
      If 1 = 2 And OPZ_MOD_ART_RIV_2001 <> 0 Then '20121008LC
        sz = sz & "OPZ_MOD_ART_RIV_2001=" & OPZ_MOD_ART_RIV_2001 & vbCrLf
      End If
      '---
      'If OPZ_PRORATA_MODEL <> 0 Then  '20160415LC (commentato)  'ex 20111208LC
      '  sz = sz & "OPZ_PRORATA_MODEL=" & OPZ_PRORATA_MODEL _
      '          & "(" & OPZ_PRORATA_ANNO & ")" & vbCrLf
      'End If
      '--- 20131123LC (inizio)
      'If OPZ_MISTO_MODEL <> 0 Then '20111213LC
      '  sz = sz & "OPZ_MISTO_MODEL=" & OPZ_MISTO_MODEL & vbCrLf
      'End If
      If 1 = 2 And OPZ_MISTO_AGEV_GIOV <> 0 Then '20111213LC
        sz = sz & "OPZ_MISTO_AGEV_GIOV=" & OPZ_MISTO_AGEV_GIOV & vbCrLf
      End If
      '--- 20131123LC (fine)
      '--- 20130620LC (inizio)
      If 1 = 2 And OPZ_MISTO_INTEGR_MODEL <> 0 Then  '20140320LC  'ex 20120514LC
        sz = sz & "OPZ_MISTO_INTEGR_MODEL=" & OPZ_MISTO_INTEGR_MODEL _
                & "(" & OPZ_MISTO_INTEGR_ALF _
                & ";" & OPZ_MISTO_INTEGR_ALR _
                & ";" & OPZ_MISTO_INTEGR_ANNI & ")" & vbCrLf
      End If
      '--- 20130620LC (fine)
      If 1 = 2 And OPZ_MISTO_REDD_PENS <> 0 Then '20120109LC
        sz = sz & "OPZ_MISTO_REDD_PENS=" & OPZ_MISTO_REDD_PENS & vbCrLf
      End If
      If OPZ_MISTO_DEB_MAT <> 0 Then '20120329LC
        sz = sz & "OPZ_MISTO_DEB_MAT=" & OPZ_MISTO_DEB_MAT & vbCrLf
      End If
      '--- 20140320LC (fine)
      If OPZ_MASSIMALE_2011 = 0 Then '20150615LC
        sz = sz & "OPZ_MASSIMALE_2011=" & OPZ_MASSIMALE_2011 & vbCrLf
      End If
      '--- 20160223LC (inizio)
      If OPZ_RND_MODEL > 0 Then
        sz = sz & "OPZ_RND_MODEL=" & OPZ_RND_MODEL & " NIT=" & OPZ_RND_NIT & vbCrLf
      End If
      '--- 20160223LC (fine)
    '--- 20161111LC (inizio)
      If OPZ_MOD_REG_1A <> 0 Then
        sz = sz & "OPZ_MOD_REG_1=" & OPZ_MOD_REG_1A _
                & " (CV=" & OPZ_MOD_REG_1_CV _
                & ", ANNI=" & OPZ_MOD_REG_1_ANNI _
                & ")" & vbCrLf  '20180514LC
      End If
      If OPZ_MOD_REG_2 <> 0 Then
        sz = sz & "OPZ_MOD_REG_2=" & OPZ_MOD_REG_2 _
                & " (AMIN=" & OPZ_MOD_REG_2_AMIN _
                & ", ANNO=" & OPZ_MOD_REG_2_ANNO _
                & ", ANNI=" & OPZ_MOD_REG_2_ANNI _
                & ", CIFRE_TOLL=" & OPZ_MOD_REG_2_CIFRE_TOLL _
                & ", NITMAX=" & OPZ_MOD_REG_2_NITMAX _
                & ", PERC_PIL5=" & OPZ_MOD_REG_2_PERC_PIL5 _
                & ", RIV_MAX=" & OPZ_MOD_REG_2_RIV_MAX _
                & ")" & vbCrLf  '20161116LC-3
      End If
      If OPZ_MOD_REG_3 <> 0 Then
        sz = sz & "OPZ_MOD_REG_3=" & OPZ_MOD_REG_3 _
                & " (ANNO=" & OPZ_MOD_REG_3_ANNO _
                & ", PERC_ENAS=" & OPZ_MOD_REG_3_PERC_ENAS _
                & ")" & vbCrLf
      End If
      '---
      If OPZ_RC_MODEL <> 0 Then  '20170727LC
        sz = sz & "OPZ_RC_MODEL=" & OPZ_RC_MODEL _
                & " (ADEC=" & OPZ_RC_ADEC _
                & ", FRAZ_ATT=" & OPZ_RC_FRAZ_ATT _
                & ", FRAZ_SIL=" & OPZ_RC_FRAZ_SIL _
                & ")" & vbCrLf
      End If
      '---
      If OPZ_RIATT_AMIN <> 0 Then  '20180305LC
        sz = sz & "OPZ_RIATT_AMIN=" & OPZ_RIATT_AMIN
      End If
      '---
      If sz <> "" Then
        sz = sz & vbCrLf & "Si vuole effettuare l'analisi?"
        If glo.MJ = MI_ASP Then  '20130306LC
        ElseIf vbYes <> MsgBox(sz, vbYesNo + vbDefaultButton2 + vbExclamation, "ATTENZIONE") Then
          Call MsgBox("Operazione annullata", vbInformation)
          GoTo ExitPoint  '20150504LC
        End If
      End If
    End If
    '*************************
    'Salva i dati dell'analisi
    '*************************
    If appl.iterNum = 0 Then
    '--- 20161111LC (fine)
      appl.xa = 0
      appl.h = 0
      szGruppo = LCase(appl.parm(P_ID))
      '--- 20130306LC (inizio)
      If glo.MJ = MI_VB Then
        bDataChanged = frm.p_bDataChanged
      Else
        bDataChanged = False
      End If
      If bDataChanged = True And szGruppo <> "base" Then
      '--- 20130306LC (fine)
        If vbYes = MsgBox("Si vuole salvare il gruppo di parametri ?", vbYesNo + vbDefaultButton2) Then
          '20160323LC (10 caratteri)
          szGruppo = LCase(Trim(InputBox("Nome da attribuire all'attuale gruppo di parametri (da 1 a 10 caratteri)", "Salva parametri", szGruppo)))
          If szGruppo = "" Then
            Call MsgBox("Operazione annullata", vbInformation)
            Exit Function
          ElseIf Len(szGruppo) > 10 Then  '20160323LC
            Call MsgBox("La lunghezza massima del nome del gruppo � 10 caratteri", vbExclamation)  '20160323LC
            Exit Function
          End If
          '--- 20121016LC (inizio)
          'sErr = TAppl1_CheckDati(appl, frm)
          If glo.MJ = MI_VB Then  '20130307LC
            Call TAppl_Mask2Req(appl, req, frm)
          End If
          sErr = TAppl_Req_Check(appl, req)
          If sErr = "" Then
            'Call TAppl1_DatiGet(appl, frm)
            Call TAppl_Req2Dati(appl, req)
          End If
          '--- 20121016LC (fine)
          If sErr = "" Then
            sErr = TAppl_ParamDbPut(appl, glo, glo.db, szGruppo, "I")
          End If
          If sErr = "" Then
            sErr = modAfpServer.Param_MaskLoad_Prm(glo.MJ, glo.db, frm)  '20130307LC
            frm.Combo1(P_ID).Text = szGruppo
          End If
          If sErr <> "" Then
            Call MsgBox(sErr, vbCritical, "ERRORE CRITICO")
            Stop
          Else
            Call MsgBox("Il gruppo di parametri � stato salvato con il nome: " & szGruppo)
          End If
        Else
          '--- 20121016LC (inizio)
          'sErr = TAppl1_CheckDati(appl, frm)
          Call TAppl_Mask2Req(appl, req, frm)  '20130307LC
          sErr = TAppl_Req_Check(appl, req)
          If sErr = "" Then
            'Call TAppl1_DatiGet(appl, frm)
            Call TAppl_Req2Dati(appl, req)
          End If
          '--- 20121016LC (fine)
        End If
      Else
        '--- 20121016LC (inizio)
        'sErr = TAppl1_CheckDati(appl, frm)
        If glo.MJ = MI_VB Then  '20130306LC
          Call TAppl_Mask2Req(appl, req, frm)  '20130307LC
        End If
        sErr = TAppl_Req_Check(appl, req)
        If sErr = "" Then
          'Call TAppl1_DatiGet(appl, frm)
          Call TAppl_Req2Dati(appl, req)
        End If
        '--- 20121016LC (fine)
      End If
    End If
    '***********************
    'Legge CoeffVar/CoeffRiv
    '***********************
    If appl.iterNum = 0 Then
      '20120214LC (inizio)
      If 1 = 1 Then
        '--- 20160222LC (inizio)
        sErr = LeggiCoeffVar1(glo.db, appl.t0, appl.CoeffVar(), appl.cv(), appl.cv4())
        If sErr <> "" Then GoTo ExitPoint
        '--- 20160222LC (fine)
        Call LeggiCoeffVar2(appl.parm(), appl.CoeffVar(), appl.cv(), appl.cv4())
        Call LeggiCoeffRiv(glo.db, appl.t0 + 1, appl.parm(), appl.CoeffVar(), appl.CoeffRiv())
      End If
      '20120214LC (fine)
      '--- 20130306LC (inizio)
      bDataChanged = False
      If glo.MJ = MI_VB Then
        frm.p_bDataChanged = bDataChanged
      End If
      '--- 20130306LC (fine)
      '--- 20140609LC (inizio) spostato in basso  'ex 20140603LC
      If OPZ_SUDDIVISIONI_MODEL > 0 And appl.parm(P_niTipo) >= 2 Then
        sErr = "L'opzione OPZ_SUDDIVISIONI_MODEL>0 (suddivisione manuale) non � compatibile con la generazione automatica di nuovi ingressi."
        GoTo ExitPoint
      End If
      '--- 20140609LC (fine)
    End If
    '*****************************
    'Legge i parametri da maschera
    '*****************************
    If sErr = "" And appl.iterNum = 0 Then  '20161111LC
      '************************
      'Legge le linee salariali
      '************************
      '20121113LC (inizio)
      '--- 20140111LC-7
      appl.bProb_EA_IV_IB = True  '(OPZ_INT_NO_AE_AB_AI = 0) And (UCase(appl.parm(P_LAV)) <> "ZZZ" or (UCase(appl.parm(P_LAE)) <> "ZZZ" Or UCase(appl.parm(P_LAB)) <> "ZZZ" Or UCase(appl.parm(P_LAI)) <> "ZZZ")
      '20121113LC (fine)
      '--- 3.1 Probabilit� di morte ----
      ReDim appl.LL1(0 To OPZ_OMEGAP1, 0 To BT_MAX, 0 To 3)  '20130211LC
      Call LeggiBD(glo.db, CStr(appl.parm(P_LAD)), BT_QAD, appl.LL1, True)
      Call LeggiBD(glo.db, CStr(appl.parm(P_LED)), BT_QED, appl.LL1, True)
      Call LeggiBD(glo.db, CStr(appl.parm(P_LPD)), BT_QPD, appl.LL1, True)
      Call LeggiBD(glo.db, CStr(appl.parm(P_LSD)), BT_QSD, appl.LL1, True)
      '20121113LC (inizio)
      If appl.bProb_EA_IV_IB = True Then
        Call LeggiBD(glo.db, CStr(appl.parm(P_LBD)), BT_QBD, appl.LL1, True)
        Call LeggiBD(glo.db, CStr(appl.parm(P_LID)), BT_QID, appl.LL1, True)
      End If
      '20121113LC (fine)
      '--- 3.2 Altre probabilit� ----
      '20121113LC (inizio)
      If appl.bProb_EA_IV_IB = True Then
        Call LeggiBD(glo.db, CStr(appl.parm(P_LAV)), BT_QAV, appl.LL1, False)
        Call LeggiBD(glo.db, CStr(appl.parm(P_LAE)), BT_QAE, appl.LL1, False)
        Call LeggiBD(glo.db, CStr(appl.parm(P_LAB)), BT_QAB, appl.LL1, False)
        Call LeggiBD(glo.db, CStr(appl.parm(P_LAI)), BT_QAI, appl.LL1, False)
      End If
      '20121113LC (fine)
      Call LeggiBD(glo.db, CStr(appl.parm(P_LAW)), BT_QAW, appl.LL1, False)
      '--- 3.3 Linee reddituali ----
      ReDim appl.Lsm(K_HMIN To K_HMAX, K_XMIN To K_XMAX, 0 To 2, 0 To 1, 0 To 3) As Double '20080617LC
      Call LeggiLS(glo.db, CStr(appl.parm(P_LRA)), 0, 0, appl.Lsm)
      Call LeggiLS(glo.db, CStr(appl.parm(P_LRA)), 0, 1, appl.Lsm)
      Call LeggiLS(glo.db, CStr(appl.parm(P_LRP)), 1, 0, appl.Lsm)
      Call LeggiLS(glo.db, CStr(appl.parm(P_LRP)), 1, 1, appl.Lsm)
      Call LeggiLS(glo.db, CStr(appl.parm(P_LRN)), 2, 0, appl.Lsm)
      Call LeggiLS(glo.db, CStr(appl.parm(P_LRN)), 2, 1, appl.Lsm)
      '--- 3.4 Coefficienti di conversione del montante in rendita ----
      Call LeggiCoeffPA(appl.t0, glo.db, CStr(appl.parm(P_CCR)), appl.CoeffPA())
      '--- 3.5 Superstiti ----
       '20121109LC (inizio)
      appl.bProb_Superst = (OPZ_INT_NO_SUPERST = 0) And (UCase(appl.parm(P_PF)) <> "Z0000")   '20121113LC
      If appl.bProb_Superst = True Then
        Call LeggiPFam(glo.db, appl.LL1, CStr(appl.parm(P_PF)), appl.pf())
        Call TAppl_Superstiti(appl)
      End If
      '20121109LC (fine)
    End If
    '---
    If sErr <> "" Then
      If glo.MJ = MI_VB Then  '20130306LC
        frm.TabStrip1.SelectedItem = frm.TabStrip1.Tabs(p_t.iFrame)
        If ON_ERR_RN = True Then On Error Resume Next
        If p_t.iErr = ERR_TEXT Then
          frm.Text1(p_t.iFocus).SetFocus
        ElseIf p_t.iErr = ERR_COMBO Then
          frm.Combo1(p_t.iFocus).SetFocus
        ElseIf p_t.iErr = ERR_CHECK Then
          frm.Check1(p_t.iFocus).SetFocus
        ElseIf p_t.iErr = ERR_COMBO Then
          'option(p_t.iFocus).SetFocus
        End If
      End If
      GoTo ExitPoint
    '20120301LC (inizio)
    Else
      For i = OPZ_INT_CV_AMIN To OPZ_INT_CV_AMAX
        If appl.CoeffVar(i, ecv.cv_ScA) > appl.CoeffVar(i, ecv.cv_ScB) + AA0 Then
          sErr = "Scaglione Irpef A > Scaglione Irpef B per l'anno " & i
        ElseIf appl.CoeffVar(i, ecv.cv_AlA) < appl.CoeffVar(i, ecv.cv_AlB) - AA0 Then
          sErr = "Aliquota Irpef A < Aliquota Irpef B per l'anno " & i
        '---
        ElseIf appl.CoeffVar(i, ecv.cv_ScB) > appl.CoeffVar(i, ecv.cv_ScC) + AA0 Then
          sErr = "Scaglione Irpef B > Scaglione Irpef C per l'anno " & i
        ElseIf appl.CoeffVar(i, ecv.cv_AlB) < appl.CoeffVar(i, ecv.cv_AlC) - AA0 Then
          sErr = "Aliquota Irpef B < Aliquota Irpef C per l'anno " & i
        '---
        ElseIf appl.CoeffVar(i, ecv.cv_ScC) > appl.CoeffVar(i, ecv.cv_ScD) + AA0 Then
          sErr = "Scaglione Irpef C > Scaglione Irpef D per l'anno " & i
        ElseIf appl.CoeffVar(i, ecv.cv_AlC) < appl.CoeffVar(i, ecv.cv_AlD) - AA0 Then
          sErr = "Aliquota Irpef C < Aliquota Irpef D per l'anno " & i
        '---
        ElseIf appl.CoeffVar(i, ecv.cv_ScD) > appl.CoeffVar(i, ecv.cv_ScE) + AA0 Then
          sErr = "Scaglione Irpef D > Scaglione Irpef E per l'anno " & i
        ElseIf appl.CoeffVar(i, ecv.cv_AlD) < appl.CoeffVar(i, ecv.cv_AlE) - AA0 Then
          sErr = "Aliquota Irpef D < Aliquota Irpef E per l'anno " & i
        '---
        ElseIf appl.CoeffVar(i, ecv.cv_ScE) > appl.CoeffVar(i, ecv.cv_ScF) + AA0 Then
          sErr = "Scaglione Irpef E > Scaglione Irpef F per l'anno " & i
        ElseIf appl.CoeffVar(i, ecv.cv_AlE) < appl.CoeffVar(i, ecv.cv_AlF) - AA0 Then
          sErr = "Aliquota Irpef E < Aliquota Irpef F per l'anno " & i
        '---
        ElseIf appl.CoeffVar(i, ecv.cv_ScF) > appl.CoeffVar(i, ecv.cv_ScG) + AA0 Then
          sErr = "Scaglione Irpef F > Scaglione Irpef G per l'anno " & i
        ElseIf appl.CoeffVar(i, ecv.cv_AlF) < appl.CoeffVar(i, ecv.cv_AlG) - AA0 Then
          sErr = "Aliquota Irpef F < Aliquota Irpef G per l'anno " & i
        End If
        If sErr <> "" Then GoTo ExitPoint
      Next i
    '20120301LC (fine)
    End If
    '---
    '20120214LC (inizio)
    'Call TAppl_DatiGet(appl, frm)
    'If 1 = 1 Then
    '  Call LeggiCoeffVar2(appl.parm(), appl.CoeffVar(), appl.cv4())
    '  Call LeggiCoeffRiv(glo.db, appl.t0 + 1, appl.parm(), appl.CoeffVar(), appl.CoeffRiv())
    'End If
    '20120214LC (fine)
    glo.nRead = 0
    glo.nElab = 0
    '***********************************
    'Legge le tabelle dei nuovi iscritti
    '***********************************
    If appl.iterNum = 0 Then  '20161111LC
      '--- 20150527LC (inizio)
      If appl.parm(P_niTipo) >= 2 Then
        ReDim appl.Lni(K_XMIN To K_XMAX, 0 To MAX_TC, 0 To 3, 0 To 3) As Double '20080307LC
        sErr = LeggiNI(glo.db, CStr(appl.parm(P_niStat)), appl.Lni)  '20160523LC
        If sErr <> "" Then GoTo ExitPoint  '20160523LC
      End If
      '--- 20150527LC (fine)
      '---  20160323LC (inizio)
      ReDim appl.Lne(0 To 30, 0 To 60, 0 To 3, 0 To 3, 0 To MAX_TC) As Double  '20170312LC
      sErr = LeggiNE(glo.db, CStr(appl.parm(P_neStat)), appl.Lne)
      If sErr <> "" Then GoTo ExitPoint
      '---
      ReDim appl.Lnv(0 To 30, 0 To 60, 0 To 3, 0 To 3, 0 To MAX_TC) As Double  '20170312LC
      sErr = LeggiNE(glo.db, CStr(appl.parm(P_nvStat)), appl.Lnv)
      If sErr <> "" Then GoTo ExitPoint
    End If
    '---  20160323LC (inizio)
    '********************************
    'Inizializza la tabella di output
    '********************************
    '20080416LC (inizio)
    appl.nMax1 = appl.parm(P_btAnniEl1)
    appl.nMax2 = appl.nMax1 + OPZ_ELAB_CODA
    '20080416LC (fine)
    '20121028LC (inizio)
    appl.nSize4 = (1 + TB_LAST)
    'appl.nSize3 = (1 + appl.Nmax2 + 1) * appl.nSize4
    appl.nSize3 = (1 + appl.nMax2 + 1 + appl.t0 - 1960) * appl.nSize4  '20121114LC
    appl.nSize2 = (1 + 4) * appl.nSize3
    appl.nSize1 = (1 + OPZ_INT_SUDDIVISIONI_N) * appl.nSize2
    ReDim tb1(appl.nSize1 - 1)
    '20121028LC (fine)
    '--- 20181108LC (inizio) MEF
    If OPZ_DBXMEF <> 0 Then
      ReDim appl.tbMEF(EMef.Mef_Max, 1, 130, EMef.Mef_NumAgg, 99)
    End If
    '--- 20181108LC (inizio) MEF

    '--- 20111214-5C (inizio)
    'If OPZ_MISTO_MODEL = -1 And appl.parm(P_TipoElab) > 1 Then  '20131123LC (inibito)  'ex 20120502LC
    '  fo = FreeFile
    '  Open App.Path & "\afp_misto_log.csv" For Output As fo
    '  Write #fo, "mat", "anno", "iop", "_t", "t", "t1", "bEA", "pens1rr", "APens", "pens1", "pens1r", "pens1c", "pmin", "pens1a1", "h_retr_ante", "h_contr_ante", "pens1a2", "h_retr_post", "h_contr_post", "fraz"
    '  Close #fo
    'End If
    '--- 20111214-5C (fine)
    '*******
    'Calcolo
    '*******
    dNow = Now
    tim1 = Timer
    SQL = Trim(appl.parm(P_SQL))
    If SQL = "" Then
      SQL = "SELECT * FROM " & szTableDI  '20140317LC
    ElseIf InStr(SQL, " ") > 0 Then
      SQL = SQL
    ElseIf SQL = CStr(Val(SQL)) Then
      SQL = "SELECT * FROM " & szTableDI & " WHERE IdDip=" & SQL   '20140317LC
    Else
      SQL = "SELECT * FROM " & SQL
    End If
    '20111217LC (inizio)
    If ON_ERR_RN3 = True Then On Error Resume Next
    Call rsMat.Open(SQL, glo.db, adOpenKeyset, adLockOptimistic)
    If Err.Number <> 0 Then
      GoTo ErrorHandler
    End If
    On Error GoTo 0
    If ON_ERR_EH = True Then On Error GoTo ErrorHandler
    '20111217LC (fine)
    '--- Individua la posizione dei campi ----
    '20081008LC
    Call TIscr2_ListaCampi(dip, appl.t0, rsMat, ixdb)
    If rsMat.EOF = True Then
      sErr = "Nessun record trovato"
      Call rsMat.Close
      GoTo ExitPoint
    End If
    '---
    Call rsMat.MoveLast
    Call rsMat.MoveFirst
    appl.nMatr = rsMat.RecordCount
    glo.tAcc = glo.tAcc + Timer - tim1
    frm.ProgressBar1.Min = 0
    frm.ProgressBar1.Max = appl.nMatr
    frm.ProgressBar1.Visible = True
    appl.bStop = False
    tim0 = Timer
    If Not rsMat.EOF Then
      appl.matr1 = rsMat.Fields("IdDip").Value
      appl.matr2 = appl.matr1
    End If
    bStop = 0
    iTable = 1
    '20080813LC (inizio)
    '--- Ciclo sui blocchi ----
    nBlocchi = Int((rsMat.RecordCount + OPZ_INT_BLOCKSIZE - 1) / OPZ_INT_BLOCKSIZE)
    For iBlocco = 0 To nBlocchi - 1
      rsMat.AbsolutePosition = iBlocco * OPZ_INT_BLOCKSIZE + 1
      If iBlocco < nBlocchi - 1 Or rsMat.RecordCount Mod OPZ_INT_BLOCKSIZE = 0 Then
        grMat = rsMat.GetRows(OPZ_INT_BLOCKSIZE, adBookmarkCurrent)
      Else
        '20080930LC
        'grMat = rsMat.GetRows(rsMat.RecordCount Mod OPZ_INT_BLOCKSIZE, 1)
        grMat = rsMat.GetRows(rsMat.RecordCount Mod OPZ_INT_BLOCKSIZE, adBookmarkCurrent)
      End If
      
      'Mod MDG
      'ReDim vecDip(UBound(grMat, 2)) 'Ridimensiono l'array in base a quanti dipendenti ci sono
      
      For jBlocco = 0 To UBound(grMat, 2)
      
        'dip.wa(0).eta = dip.sup(0).y
      
      
        If (jBlocco + 1) Mod 100 = 0 Then
          ''''frm.Caption = iBlocco * OPZ_INT_BLOCKSIZE + 1 + jBlocco & "/" & rsMat.RecordCount: DoEvents
        End If
        Call TAppl_Elab32_Elab(appl, glo, dip, ixdb(), buf1(), buf2(), grMat, _
                               bElab, fo, fr, i, iBlocco, iErr, iop, _
                               iTable, jBlocco, tim1, tim2, dNow, s, sErr, rsMat)
                               
        'Call saveDataDip(dip, vecDip(jBlocco))
                               
        Call TAppl_Elab32_ProgressBar(appl, glo, dip, bElab, jBlocco, tim0, tim2, grMat, frm)
        bStop = TAppl_Elab32_TestStop(appl, glo, frm)
        If bStop > 0 Then Exit For
        '--- 20150527LC (inizio)
        '--- 20150527LC (inizio)
        '********************
        'Elabora i riattivati
        '********************
        If 1 = 2 And appl.nDipGen > 0 Then
          'frm.ProgressBar1.Max = frm.ProgressBar1.Max + appl.nDipGen
          'glo.nElab = glo.nElab + 1
          For i = 0 To appl.nDipGen - 1
            Call TIscr2_ReadMdb(dip, p_diaGen(i), appl)  '20140605LC
            Call TIscr2_Elabora(dip, appl, glo, appl.tb1, fr, 1)  '20140605LC
            If bStop > 0 Then Exit For
          Next i
          appl.nDipGenBk = appl.nDipGen  '20160411LC
          appl.nDipGen = 0
          Erase p_diaGen()
          Set p_diaColl = Nothing  '20150612LC
        End If
        '--- 20150527LC (fine)
      Next jBlocco
      If bStop > 0 Then Exit For
      '--- 20150527LC (fine)
    Next iBlocco
    '20080813LC (fine)
    Call rsMat.Close
    If glo.MJ = MI_VB Then  '20130306LC
      frm.ProgressBar1.Visible = False
    End If
    If bStop = 2 Then GoTo ExitPoint
    '--- 20150612LC (inizio)
    '********************
    'Elabora i riattivati
    '********************
    If 1 = 1 And appl.nDipGen > 0 Then
      'frm.ProgressBar1.Max = frm.ProgressBar1.Max + appl.nDipGen
      'glo.nElab = glo.nElab + 1
      For i = 0 To appl.nDipGen - 1
        '--- 20160411LC (inizio)
        With p_diaGen(i)
'.n = .n
          If .z_nRiatt > 1 Then
            .DIscr = Int(.DIscr)
            .DNasc = Int(.DNasc)
            .h = Int(.h * 4) / 4
            .ha = Int(.ha * 4 / 4)
            .hb = Int(.hb * 4) / 4
            .om0 = Int(.om0 * 100#) / 100#
            .qm0 = Int(.qm0 * 100#) / 100#
            .cmNI = Int(.cmNI * 100#) / 100#
            .smNI = Int(.smNI * 100#) / 100#
            For ia = 0 To UBound(.cm)
              .cm(ia) = Int(.cm(ia) * 100#) / 100#
            Next ia
            For ia = 0 To UBound(.sm)
              .sm(ia) = Int(.sm(ia) * 100#) / 100#
            Next ia
          End If
        End With
        '--- 20160411LC (fine)
        Call TIscr2_ReadMdb(dip, p_diaGen(i), appl)  '20140605LC
        dip.RcIt = p_diaGen(i).RcIt  '0 o 1 per OPZ_RC_MODEL=1, solo 0 per OPZ_RC_MODEL=2
        'dip.RcKK = 1  'NON pu� generare riattivati e volontari (servirebbe solo per GRUPPO=5 o 6)
        Call TIscr2_Elabora(dip, appl, glo, appl.tb1, fr, 1)  '20140605LC
        If bStop > 0 Then Exit For
      Next i
      appl.nDipGenBk = appl.nDipGen  '20160411LC
      appl.nDipGen = 0
      Erase p_diaGen()
      Set p_diaColl = Nothing  '20150612LC
    End If
    '--- 20150612LC (fine)
    '************************
    'Elabora i nuovi iscritti
    '************************
    If appl.parm(P_niTipo) >= 2 Then
      bStimaMI = False
      iTable = 2
      '--- Cancella i record precedenti ---
      '20080305LC (inizio)
      If ON_ERR_RN Then On Error Resume Next
      SQL = SQL_Del(glo.DB_TYPE)
      SQL = SQL & " FROM " & szTableNI '20120607LC
      SQL = SQL & " WHERE 1=1"
      glo.db.Execute SQL
      If Err.Number = &H80040E37 Then
        Err.Clear 'Tabella non trovata
      '20080305LC
      ElseIf Err.Number <> 0 Then
        GoTo ErrorHandler
      End If
      On Error GoTo 0
      'SQL = "SELECT TOP 1 * INTO D_NUOVI_ISCRITTI FROM D_ISCRITTI"
      'glo.db.Execute SQL
      'SQL = SQL_Del(glo.DB_TYPE) & " FROM D_NUOVI_ISCRITTI"
      'glo.db.Execute SQL
      '20080305LC (fine)
      '--- Calcola le probabilit� assolute --- appl.Lni(K_XMIN To K_XMAX, 0 To 3, 0 To 3)
      XMIN = LBound(appl.Lni, 1)
      XMAX = UBound(appl.Lni, 1)
      '20080307LC (inizio)
      For j = 0 To 3
        y = 0
        For iTC = 0 To MAX_TC
          For x = XMIN To XMAX
            y = y + appl.Lni(x, iTC, j, 1)
          Next x
        Next iTC
        If y > 0 Then
          For iTC = 0 To MAX_TC
            For x = XMIN To XMAX
              If appl.Lni(x, iTC, j, 1) <> 0 Then
                appl.Lni(x, iTC, j, 1) = appl.Lni(x, iTC, j, 1) / y
              End If
            Next x
          Next iTC
        End If
      Next j
      '20080307LC (fine)
      '--- Inizializza i moltiplicatori IRPEF e IVA ---
      fact1 = 1
      fact2 = 1
      '--- Elaborazione annuale ---
      ReDim vnip(3)
      For tau = 1 To appl.nMax1 '20080416LC
        '--- 20140111LC-19 (inizio) spostato in alto  'ex 20120607LC (Ts1n e Ts2n)  'ex 20120220LC (Ts1a e Ts2a)
        '--- Aggiorna i moltiplicatori IRPEF e IVA ---
        fact1 = fact1 * (1 + appl.CoeffVar(appl.t0 + tau, ecv.cv_Trn))
        fact2 = fact2 * (1 + appl.CoeffVar(appl.t0 + tau, ecv.cv_Trn))
        '--- 20140111LC-19 (fine)
        '20080617LC (inizio)
        '--- Legge i nuovi iscritti puntuali da aggiungere ---
        If appl.parm(P_niP) <> "" And appl.parm(P_niP) <> "(no)" Then
          'SQL = "SELECT nipAM,nipAF,nipIM,nipIF"
          SQL = "SELECT nip1M,nip1F,nip2M,nip2F"  '20150528LC
          SQL = SQL & " FROM C_NIP"
          SQL = SQL & " WHERE nipNome='" & UCase(appl.parm(P_niP)) & "'" '20121024LC
          SQL = SQL & " AND nipAnno=" & appl.parm(P_btAnno) + tau
          rsNip.Open SQL, glo.db, adOpenKeyset, adLockOptimistic
          If rsNip.EOF = False Then
            For j = 0 To 3
              'If UCase(Left(appl.parm(P_niP), 1)) = "A" Then
              '  vNip(j) = ff0(rsNip(j).Value) - vNip(j)
              'Else
                vnip(j) = ff0(rsNip(j).Value)
              'End If
            Next j
          Else
            For j = 0 To 3
              vnip(j) = 0
            Next j
          End If
          rsNip.Close
          Set rsNip = Nothing
        Else
          For j = 0 To 3
            vnip(j) = 0
          Next j
        End If
        '20080617LC (fine)
        '--- Costruisce i nuovi iscritti ---
        SQL = "SELECT *"
        SQL = SQL & " FROM " & szTableNI '20120607LC
        rsMat.Open SQL, glo.db, adOpenKeyset, adLockOptimistic
        '--- Individua la posizione dei campi ----
        '20081008LC
        Call TIscr2_ListaCampi(dip, appl.t0, rsMat, ixdb)
        '--- 20140603LC (inizio)  'ex 20080617LC
        For iTit = 1 To 2
          For iSex = 1 To 2
            j = (iTit - 1) * 2 + iSex - 1
            k = (iTit - 1) + 100 * (iSex - 1)
            '--- 20140522LC (inizio)
            zaa0 = 0#
            zaa1 = 0#
            zpa0 = 0#
            zpa1 = 0#
            ijk0 = appl.nSize2 * (0 + 1) + appl.nSize3 * (j + 1) + appl.nSize4 * (tau - 1 + appl.t0 - 1960)  '20121114LC
            ijk = appl.nSize2 * (0 + 1) + appl.nSize3 * (j + 1) + appl.nSize4 * (tau + appl.t0 - 1960)       '20121114LC
            For iTS = 0 To OPZ_INT_SUDDIVISIONI_N - 1
'If ijk0 <> appl.nSize2 * (iTS + 1) + appl.nSize3 * (j + 1) + appl.nSize4 * (tau - 1 + appl.t0 - 1960) Then
'  call MsgBox("TODO")
'  Stop
'ElseIf ijk <> appl.nSize2 * (iTS + 1) + appl.nSize3 * (j + 1) + appl.nSize4 * (tau + appl.t0 - 1960) Then
'  call MsgBox("TODO")
'  Stop
'End If
              zaa0 = zaa0 + tb1(ijk0 + TB_ZAA)
              zaa1 = zaa1 + tb1(ijk + TB_ZAA)
              If appl.parm(P_niTipo) = 3 Then
                zpa0 = zpa0 + tb1(ijk0 + TB_ZPC_VO) + tb1(ijk0 + TB_ZPC_VA) + tb1(ijk0 + TB_ZPC_VP) + _
                              tb1(ijk0 + TB_ZPC_A) + tb1(ijk0 + TB_ZPC_S) + tb1(ijk0 + TB_ZPC_T)
                zpa1 = zpa1 + tb1(ijk + TB_ZPC_VO) + tb1(ijk + TB_ZPC_VA) + tb1(ijk + TB_ZPC_VP) + _
                              tb1(ijk + TB_ZPC_A) + tb1(ijk + TB_ZPC_S) + tb1(ijk + TB_ZPC_T)
              End If
              ijk0 = ijk0 + appl.nSize2
              ijk = ijk + appl.nSize2
            Next iTS
            zaa0 = zaa0 + zpa0
            zaa1 = zaa1 + zpa1
            '--- 20140522LC (fine)
            '20080605LC (inizio)
            'If UCase(Left(appl.parm(P_niP), 1)) = "A" and frm.Check1(P_niP).Enabled = True  Then
            'If frm.Check1(P_niP).Value = vbChecked Then  '20120319LC
            If UCase(Left(req("s" & P_niP), 1)) = "A" Then  '20130307LC
              y = vnip(j) - zaa1  '20140522LC
              If y < 0# Then
                y = 0#
              End If
            Else
              y = 0
              'If frm.Check1(P_niP).Enabled = False Or frm.Check1(P_niP).Value = vbUnchecked Then
              'If frm.Check1(P_niP).Value = vbUnchecked Then  '20120319LC
              If UCase(Left(req("s" & P_niP), 1)) <> "A" Then  '20130307LC
                y = zaa0 - zaa1  '20140522LC
              End If
              y = y + vnip(j)
            End If
            '20080605LC (fine)
            '--- Test su iTS ---  '20140603LC (non pi� necessario)
            '--- Test su Y ---
            If y > AA0 Then
              For x = XMIN To XMAX
                '20080305LC (inizio)
                For iTC = 0 To MAX_TC '20080307LC
                  If appl.Lni(x, iTC, j, 1) > 0 Then
                    If appl.parm(P_niTipo) >= 2 Then 'Anzianit� da base statistica
                      hNI = appl.Lni(x, iTC, j, 0)
                      irpefNI = ARRV(appl.Lni(x, iTC, j, 2) * fact1)
                      ivaNI = ARRV(appl.Lni(x, iTC, j, 3) * fact2)
                    Else
                      hNI = 0
                      irpefNI = 0
                      ivaNI = 0
                    End If
                    rsMat.AddNew
                    '--- 20140317LC (inizio)
                    rsMat("IdDip").Value = -(tau * 1000 + j * 100 + x) * 10 - iTC
                    rsMat("IdAz").Value = 1
                    rsMat("categ").Value = IIf(iTit = 1, "1", "2")  '20160323LC
                    rsMat("mandati").Value = IIf(iTit = 1, 1, 2)  '20160323LC
                    rsMat("c1").Value = iTC '20080305LC
                    rsMat("Sesso").Value = IIf(iSex = 2, "2", "1")  '20160323LC
                    rsMat("DNasc").Value = "30/06/" & (appl.t0 + tau - x)
                    rsMat("n").Value = y * appl.Lni(x, iTC, j, 1) '20080305LC
                    rsMat("DIscrFondo").Value = "01/07/" & (appl.t0 + tau)
                    rsMat("hmf").Value = Int(hNI) * 1000 + Format((hNI - Int(hNI)) * 360, "000")
                    rsMat("smNI").Value = irpefNI
                    '--- 20140603LC (inizio)
                    If appl.CoeffVar(appl.t0 + tau, ecv.cv_Sogg1Al) > AA0 Then
                      rsMat("c2").Value = TIscr2_TS_set(iTS, 1, iSex, iTit, iTC, rsMat("DNasc").Value, Int(hNI), appl.t0 + tau, irpefNI, appl.CoeffVar(appl.t0 + tau, ecv.cv_Sogg0Min) / appl.CoeffVar(appl.t0 + tau, ecv.cv_Sogg1Al), appl.CoeffVar(appl.t0 + tau, ecv.cv_Sogg2Max)) '20120331LC
                    Else
                      rsMat("c2").Value = TIscr2_TS_set(iTS, 1, iSex, iTit, iTC, rsMat("DNasc").Value, Int(hNI), appl.t0 + tau, irpefNI, 1E+20, appl.CoeffVar(appl.t0 + tau, ecv.cv_Sogg2Max))
                    End If
                    '--- 20140603LC (fine)
                    rsMat.Update
                  End If
                Next iTC
                '20080305LC (fine)
              Next x
            End If
          Next iSex
        Next iTit
'Next iTS
        '--- 20140603LC (fine)  'ex 20080617LC
        rsMat.Close
        '--- Elabora i nuovi iscritti ---
        SQL = "SELECT * FROM " & szTableNI '20120607LC
        '--- 20140317LC (inizio)
        SQL = SQL & " WHERE IdDip>=" & -(tau * 10000 + 9999)
        SQL = SQL & " AND IdDip<=" & -(tau * 10000)
        SQL = SQL & " ORDER BY IdDip DESC"
        '--- 20140317LC (fine)
        rsMat.Open SQL, glo.db, adOpenKeyset, adLockOptimistic
        If rsMat.EOF = False Then
          '20080813LC (inizio)
          rsMat.MoveLast
          rsMat.MoveFirst
          If bStimaMI = False Then
            If glo.MJ = MI_VB Then
              frm.ProgressBar1.Min = 0 'appl.nMatr - 1  '20080906LC
              frm.ProgressBar1.Max = frm.ProgressBar1.Max + rsMat.RecordCount '* (appl.Nmax1 - tau + 1)
              frm.ProgressBar1.Visible = True
            End If
            bStimaMI = True
          End If
          '--- Ciclo sui blocchi ----
          nBlocchi = Int((rsMat.RecordCount + OPZ_INT_BLOCKSIZE - 1) / OPZ_INT_BLOCKSIZE)
          For iBlocco = 0 To nBlocchi - 1
            rsMat.AbsolutePosition = iBlocco * OPZ_INT_BLOCKSIZE + 1
            If iBlocco < nBlocchi - 1 Or rsMat.RecordCount Mod OPZ_INT_BLOCKSIZE = 0 Then
              grMat = rsMat.GetRows(OPZ_INT_BLOCKSIZE, adBookmarkCurrent)
            Else
              grMat = rsMat.GetRows(rsMat.RecordCount Mod OPZ_INT_BLOCKSIZE, 1)
            End If
            For jBlocco = 0 To UBound(grMat, 2)
              If (jBlocco + 1) Mod 100 = 0 Then
                ''''frm.Caption = iBlocco * OPZ_INT_BLOCKSIZE + 1 + jBlocco & "/" & rsMat.RecordCount: DoEvents
              End If
              '--- 20150518LC (inizio)  'ex 20150413LC
              Call TAppl_Elab32_Elab(appl, glo, dip, ixdb(), buf1(), buf2(), grMat, _
                                     bElab, fo, fr, i, iBlocco, iErr, iop, _
                                     iTable, jBlocco, tim1, tim2, dNow, s, sErr, rsMat)
              Call TAppl_Elab32_ProgressBar(appl, glo, dip, bElab, jBlocco, tim0, tim2, grMat, frm)
              bStop = TAppl_Elab32_TestStop(appl, glo, frm)
              If bStop > 0 Then Exit For
              'If bRiatt = 1 Then
              '  GoSub Elabora
              '  GoSub ProgressBar
              '  GoSub TestStop
              '  If bStop > 0 Then Exit For
              'End If
              '--- 20150518LC (fine)
            Next jBlocco
            If bStop > 0 Then Exit For
          Next iBlocco
        '20080813LC (fine)
        End If
        rsMat.Close
        If bStop > 0 Then Exit For
      Next tau
      If bStop = 2 Then GoTo ExitPoint
    End If
    '*************************
    'Consolida la tabella TB()
    '*************************
    '20080617LC (inizio)
appl.nPensMat = appl.nPensMat
appl.nPensMax = appl.nPensMax
    For iTS = 1 To OPZ_INT_SUDDIVISIONI_N
      For iQ = 1 To 4
        For tau = (0 - appl.t0 + 1960) To appl.nMax2 '20121114LC
          ijk = appl.nSize2 * iTS + appl.nSize3 * iQ + appl.nSize4 * (tau + appl.t0 - 1960) '20121114LC   '5
If tb1(ijk + TB_SP_TOT) <> 0 Then
Call MsgBox("TODO")
End If
          '--- 20140325LC (inizio)
          tb1(ijk + TB_SP_TOT) = tb1(ijk + TB_SPC_VO) _
                               + tb1(ijk + TB_SPC_VA) _
                               + tb1(ijk + TB_SPC_VP) _
                               + tb1(ijk + TB_SPC_A) _
                               + tb1(ijk + TB_SPC_T) _
                               + tb1(ijk + TB_SPC_S) _
                               + tb1(ijk + TB_SPN_VO) _
                               + tb1(ijk + TB_SPN_VA) _
                               + tb1(ijk + TB_SPN_VP) _
                               + tb1(ijk + TB_SPN_A) _
                               + tb1(ijk + TB_SPN_T) _
                               + tb1(ijk + TB_SPN_T) _
                               + tb1(ijk + TB_SPN_B) _
                               + tb1(ijk + TB_SPN_I) _
                               + tb1(ijk + TB_SPS)
          '--- 20140325LC (fine)
          tb1(ijk + TB_SU_TOT) = tb1(ijk + TB_SP_TOT) _
                               + tb1(ijk + TB_SPW_1) _
                               + tb1(ijk + TB_SPW_2) _
                               + tb1(ijk + TB_SPW_3)
If tb1(ijk + TB_MP_TOT) <> 0 Then
Call MsgBox("TODO")
End If
          '--- 20140325LC (inizio)
          tb1(ijk + TB_MP_TOT) = tb1(ijk + TB_MPC_VO) _
                               + tb1(ijk + TB_MPC_VA) _
                               + tb1(ijk + TB_MPC_VP) _
                               + tb1(ijk + TB_MPC_A) _
                               + tb1(ijk + TB_MPC_T) _
                               + tb1(ijk + TB_MPC_S) _
                               + tb1(ijk + TB_MPN_VO) _
                               + tb1(ijk + TB_MPN_VA) _
                               + tb1(ijk + TB_MPN_VP) _
                               + tb1(ijk + TB_MPN_A) _
                               + tb1(ijk + TB_MPN_T) _
                               + tb1(ijk + TB_MPN_B) _
                               + tb1(ijk + TB_MPN_I) _
                               + tb1(ijk + TB_MPN_S) _
                               + tb1(ijk + TB_MPS)
          '--- 20140325LC (fine)
          tb1(ijk + TB_MU_TOT) = tb1(ijk + TB_MP_TOT) _
                               + tb1(ijk + TB_MPW_1) _
                               + tb1(ijk + TB_MPW_2) _
                               + tb1(ijk + TB_MPW_3)
          tb1(ijk + TB_STOT) = tb1(ijk + TB_AA_ETOT) + tb1(ijk + TB_PA_ETOT) - tb1(ijk + TB_MU_TOT)  '20140409LC  '+ tb1(ijk + TB_EA_ETOT)
        Next tau
      Next iQ
    Next iTS
    '---
    For iTS = 1 To OPZ_INT_SUDDIVISIONI_N
      For tau = (0 - appl.t0 + 1960) To appl.nMax2 '20121114LC
        For j = 0 To TB_LAST
          y = 0
          For iQ = 1 To 4
            ijk = appl.nSize2 * iTS + appl.nSize3 * iQ + appl.nSize4 * (tau + appl.t0 - 1960) '20121114LC   '6
            y = y + tb1(ijk + j)
          Next iQ
          ijk = appl.nSize2 * iTS + appl.nSize3 * 0 + appl.nSize4 * (tau + appl.t0 - 1960) '20121114LC   '7
          tb1(ijk + j) = y
        Next j
      Next tau
    Next iTS
    '---
    For tau = (0 - appl.t0 + 1960) To appl.nMax2 '20121114LC
      For j = 0 To TB_LAST
        y = 0
        For iTS = 1 To OPZ_INT_SUDDIVISIONI_N
          ijk = appl.nSize2 * iTS + appl.nSize3 * 0 + appl.nSize4 * (tau + appl.t0 - 1960) '20121114LC   '8
          y = y + tb1(ijk + j)
        Next iTS
        ijk = appl.nSize2 * 0 + appl.nSize3 * 0 + appl.nSize4 * (tau + appl.t0 - 1960) '20121114LC   '9
        tb1(ijk + j) = y
      Next j
    Next tau
    '20080617LC (fine)
    '*************************
    'Salva i dati dell'analisi
    '*************************
    sErr = TAppl_ReportDbPut(appl, glo, tb1, glo.db)
    If sErr <> "" Then GoTo ExitPoint
    '*****************************
    'Visualizzazione dei risultati
    '*****************************
    '--- 20161112LC (inizio)
    sErr = TAppl_CreaReportt(appl, glo, tb1, frm)
    If sErr <> "" Then
      Open sFileErr For Append As #fErr
      Print #fErr, "Iter:" & appl.iterNum & " - Errore durante la creazione del report dell'analisi:" & glo.idReport
      Close #fErr
      sErr = ""
    End If
    '--- 20161112LC (fine)
  '--- 20161111LC (inizio)
    '*****************
    'Calcoli iterativi
    '*****************
    If Abs(OPZ_MOD_REG_2) >= 1 Then  '20180122LC
      If appl.iterNum = 0 Then
        tauMin = IIf(OPZ_OUTPUT_MAX = 0, 1, 1961 - appl.t0)
        tauMax = appl.nMax2
      End If
      For tau = tauMin To tauMax
        ijk = appl.nSize2 * 0 + appl.nSize3 * 0 + appl.nSize4 * (tau + appl.t0 - 1960) '20121114LC  '17
        anno = appl.t0 + tau
        contributi = tb1(ijk + TB_AA_ETOT) + tb1(ijk + TB_PA_ETOT)
        '--- 20170125LC (inizio)
        If Abs(OPZ_MOD_REG_2) >= 2 Then  '20180122LC
          '--- 6) Contributi assistenziali/solidariet� attivi
          contributi = contributi + tb1(ijk + TB_AA_ECA0) + tb1(ijk + TB_PA_ECA0)
        End If
        '--- 20170125LC (fine)
        pensioni = tb1(ijk + TB_MPN_VO) _
                     + tb1(ijk + TB_MPN_VA) _
                     + tb1(ijk + TB_MPN_VP) _
                     + tb1(ijk + TB_MPN_A) _
                     + tb1(ijk + TB_MPN_S) _
                     + tb1(ijk + TB_MPN_T) _
                     + tb1(ijk + TB_MPN_B) _
                     + tb1(ijk + TB_MPN_I) _
                     + tb1(ijk + TB_MPS)
        pensioni = pensioni + tb1(ijk + TB_MPC_VO) _
                     + tb1(ijk + TB_MPC_VA) _
                     + tb1(ijk + TB_MPC_VP) _
                     + tb1(ijk + TB_MPC_A) _
                     + tb1(ijk + TB_MPC_S) _
                     + tb1(ijk + TB_MPC_T)
        '--- 20161115LC (inizio)
        '--- 20161116LC-3 (inizio)
        If anno < OPZ_MOD_REG_2_AMIN Then
          'Do nothing
        ElseIf pensioni > 0# Then
        '--- 20161116LC-3 (fine)
          appl.CoeffVar(anno, ecv.cv_Trmc0) = contributi / pensioni - 1#
        Else
          appl.CoeffVar(anno, ecv.cv_Trmc0) = 0#
        End If
        If anno >= OPZ_MOD_REG_2_ANNO Then
          '--- 20161116LC (inizio)
          If 1 + appl.CoeffVar(anno - OPZ_MOD_REG_2_ANNI, ecv.cv_Trmc0) > 0 Then
            tRivMont = (1 + appl.CoeffVar(anno - 1, ecv.cv_Trmc0)) / (1 + appl.CoeffVar(anno - OPZ_MOD_REG_2_ANNI, ecv.cv_Trmc0)) - 1#
          Else
            tRivMont = 0#
          End If
          '--- 20180122LC (inizio)
          If OPZ_MOD_REG_2 > 0 And OPZ_MOD_REG_2_ANNI > 1 Then
            tRivMont = (1# + tRivMont) ^ (1# / (OPZ_MOD_REG_2_ANNI - 1)) - 1#
          End If
          '--- 20180122LC (fine)
          tRivMont = tRivMont + (appl.CoeffVar(anno, ecv.cv_Trmc5) - tRivMont) * OPZ_MOD_REG_2_PERC_PIL5  '20161112LC
          '--- 20161116LC (fine)
          If tRivMont < 0# Then
            tRivMont = 0#
          Else
          End If
        '--- 20161115LC (fine)
          '--- 20180306LC (inizio)
          If OPZ_MOD_REG_2_RIV_MAX = -1# Then
            If tRivMont > appl.CoeffVar(anno, ecv.cv_Trmc1) Then
              tRivMont = appl.CoeffVar(anno, ecv.cv_Trmc1)
            End If
          ElseIf tRivMont > OPZ_MOD_REG_2_RIV_MAX Then  '20161114LC
            tRivMont = OPZ_MOD_REG_2_RIV_MAX
          End If
          '--- 20180306LC (fine)
          tRivMont = ARR5(tRivMont * ZARR, 1#) / ZARR
          y = tRivMont - appl.CoeffVar(anno, ecv.cv_Trmc4)  '20161115LC
          If y <> 0# Then
            appl.iterAnniVar = appl.iterAnniVar + 1
            If appl.iterAnnoVarMin = 0 Then
              appl.iterAnnoVarMin = anno
            End If
            appl.iterAnnoVarMax = anno
            'appl.CoeffVar(anno, ecv.cv_Trmc0) = tRivMont  '20161115LC (commentato)
            'appl.CoeffVar(anno, ecv.cv_Trmc1) = tRivMont  '20180306LC (commentata)
            appl.CoeffVar(anno, ecv.cv_Trmc2) = tRivMont
            appl.CoeffVar(anno, ecv.cv_Trmc3) = tRivMont
            appl.CoeffVar(anno, ecv.cv_Trmc4) = tRivMont
            If appl.iterDiffMin > y Then
              appl.iterDiffMin = y
            ElseIf appl.iterDiffMax < y Then
              appl.iterDiffMax = y
            End If
          End If
        End If
      Next tau
      '--- Scrive sul file di log
      Open sFileAut For Append As #fAut
      Print #fAut, CStr(appl.iterNum);
      Print #fAut, "; "; appl.iterCvNome;
      Print #fAut, "; "; CStr(appl.iterAnniVar);
      Print #fAut, "; "; CStr(appl.iterAnnoVarMin);
      Print #fAut, "; "; CStr(appl.iterAnnoVarMax);
      Print #fAut, "; "; Trim(Format(appl.iterDiffMin, "#0." & String(OPZ_MOD_REG_2_CIFRE_TOLL - 2, "0") & "%"));
      Print #fAut, "; "; Trim(Format(appl.iterDiffMax, "#0." & String(OPZ_MOD_REG_2_CIFRE_TOLL - 2, "0") & "%"));
      Print #fAut, "; "; Trim(Format(appl.iterSaldo, "#,##0.00"))
      Close #fAut
    End If
    '---
    Close #fr
    If appl.iterAnniVar = 0 Then
      Exit For
    End If
  Next appl.iterNum
  '--- 20161111LC (fine)

ExitPoint:
  On Error GoTo 0
  Close #fo
  Close #fr  '20140609LC
  Close #fAut  '20161111LC
  '--- 20161112LC (inizio)
  Close #fErr
  If FileLen(sFileErr) = 0 Then
    Kill sFileErr
  End If
  '--- 20161112LC (inizio)
  '--- 20111214-5C (inizio)
  'If OPZ_MISTO_MODEL = -1 And appl.parm(P_TipoElab) > 1 Then  '20131123LC (inibito)  'ex 20120502LC
  '  fo = FreeFile
  '  Open App.Path & "\afp_misto_log.csv" For Input As fo
  '  sz = Input(LOF(fo), fo)
  '  Close #fo
  '  sz = Replace(sz, ",", ";")
  '  sz = Replace(sz, ".", ",")
  '  Open App.Path & "\afp_misto_log.csv" For Output As fo
  '  Print #fo, sz;
  '  Close #fo
  'End If
  '---- 20111214-5C (fine)
  Screen.MousePointer = vbDefault
  If sErr <> "" Then
    Call MsgBox(sErr, vbExclamation)
  End If
  TAppl_Elab32 = (sErr = "")
  Exit Function
  
ErrorHandler:
  If Err.Number Then sErr = "Errore " & Err.Number & " (" & Hex(Err.Number) & ") nel modulo " & Err.Source & vbCrLf & Err.Description
  GoTo ExitPoint
End Function

Public Sub saveDataDip(ByRef dip As TIscr2, ByRef dipStruct As dipExportMef)

  Dim i As Integer
  
  With dip
    Select Case .catPens
    
      Case CAT_PENS_REV
        
        For i = 0 To UBound(dip.wa)
        
          dipStruct.data(dip.wa(i).eta).anno = dip.wa(i).anno
          dipStruct.data(dip.wa(i).eta).stato = dip.gruppo0
          dipStruct.data(dip.wa(i).eta).tipoPensione = dip.catPens
          dipStruct.data(dip.wa(i).eta).sesso = dip.sup(0).s
          dipStruct.data(dip.wa(i).eta).eta = dip.wa(i).eta
          dipStruct.data(dip.wa(i).eta).pos31Dic = dip.wa(i).sj7_z
          dipStruct.data(dip.wa(i).eta).pensMedPosDic = dip.wa(i).pensTot / ((dip.wa(i).sj7_z + dip.wa(i + 1).sj7_z) / 2)
          
        
        
        Next i
        
    End Select
  End With
End Sub


Public Sub TAppl_Elab32_Elab(ByRef appl As TAppl, ByRef glo As TGlobale, ByRef dip As TIscr2, ByRef ixdb() As Long, _
                             ByRef buf1() As Double, ByRef buf2() As Double, ByRef grMat As Variant, _
                             ByRef bElab As Boolean, ByRef fo As Long, ByRef fr As Long, ByRef i As Long, _
                             ByRef iBlocco As Long, ByRef iErr As Long, ByRef iop As Long, _
                             ByRef iTable As Long, ByRef jBlocco As Long, ByRef tim1 As Double, ByRef tim2 As Double, _
                             ByRef dNow As Date, ByRef s As String, ByRef szErr As String, ByRef rsMat As Object)
  Dim ijk&  '20150521LC
  Dim y#
  Dim dip0 As TIscr2
  '--- 20170724LC
  Dim RC_MODEL As Byte
  Dim RC_FRAZ As Double  '20170727LC
  Dim dip1 As TIscr2

  appl.bEA = False
  On Error GoTo 0
  tim2 = Timer
  iErr = 0
  szErr = ""
  glo.nRead = glo.nRead + 1
  bElab = False
  tim1 = Timer
  dip = dip0
  If iop = 1 Then
    rsMat.AbsolutePosition = iBlocco * OPZ_INT_BLOCKSIZE + 1 + jBlocco
  End If
  '---
  szErr = TIscr2Aux_ReadMdb(p_dia, appl, ixdb, grMat, jBlocco)  '20150525LC
  If szErr = "" Then
    szErr = TIscr2_ReadMdb(dip, p_dia, appl)
  End If
  If szErr = "" Then
    iErr = TIscr2_ErrTest(dip, dip.s(dip.anno1), appl.XMIN, appl.XMAX, appl.hmax, appl.hlim, appl.DataBilancio, szErr)
  Else
    iErr = -1
  End If
  If iErr = 0 Then 'Nessun Errore
    If appl.nMatr = 1 And appl.xa = 0 And appl.h = 0 Then
      appl.xa = TIscr2_xa(dip)
      appl.h = TIscr2_h(dip)
      appl.bEA = TIscr2_bEA(dip)
    End If
    '20140307LC  'riportare  20140111LC-24
    'If (iTable = 1 And (dip.bNuovoEntrato = False Or (appl.parm(P_niTipo) = 1) And dip.gruppo0 = 1)) Or _
    '   (iTable = 2 And dip.bNuovoEntrato = True And appl.parm(P_niTipo) >= 2) Then
    If dip.n <= AA0 Then  '20150413LC
    ElseIf (iTable = 1 And (dip.bNuovoEntrato = False Or (appl.parm(P_niTipo) = 1))) Or _
       (iTable = 2 And dip.bNuovoEntrato = True And appl.parm(P_niTipo) >= 2) Then
      glo.nElab = glo.nElab + 1
      bElab = True
      '--- 20170724LC (inizio)  'ex 20170724LC  'ex 20150525LC
      dip.RcIt = IIf(dip.annoAss >= OPZ_RC_ADEC, 0, 1)  'Consente l'uscita per rendita contributiva
      dip.RcKK = 0  'pu� generare riattivati e volontari  '20170805LC
      dip.RcNa = 0#  '20170801LC
      dip.RcNe = 0#  '20170801LC
      RC_MODEL = 0
      RC_FRAZ = IIf(dip.gruppo0 = 5, OPZ_RC_FRAZ_SIL, OPZ_RC_FRAZ_ATT)
      If OPZ_RC_MODEL > 0 And RC_FRAZ < 1# Then
        If dip.gruppo0 = 1 Or dip.gruppo0 = 5 Or dip.gruppo0 = 6 Then
          If dip.annoAss >= OPZ_RC_ADEC Then
            dip1 = dip
            dip1.RcIt = 1  'Inibisce l'uscita per rendita contributiva
            RC_MODEL = OPZ_RC_MODEL
            If RC_MODEL = 1 Then
              dip.n = dip.n * RC_FRAZ
              dip.nRes = dip.nRes * RC_FRAZ
              '---
              dip1.n = dip1.n * (1# - RC_FRAZ)
              dip1.nRes = dip1.nRes * (1# - RC_FRAZ)
            End If
          End If
        End If
      End If
      '---
      If (dip.gruppo0 = 5 Or dip.gruppo0 = 6) Then
        If RC_MODEL = 0 Then
          If dip.n > 0 Then
            Call TIscr2_Elabora(dip, appl, glo, appl.tb2, fr, 0)  '20150521LC  'ex 20140605LC
            y = dip.nRes / dip.n
            If y > 0 Then
              For ijk = 0 To appl.nSize1 - 1
                appl.tb1(ijk) = appl.tb1(ijk) + appl.tb2(ijk) * y
              Next ijk
            End If
          End If
        ElseIf RC_MODEL = 1 Then
          '--- Elabora la frazione che pu� avere la rendita contributiva
          If dip.n > 0 Then
            Call TIscr2_Elabora(dip, appl, glo, appl.tb2, fr, 0)  '20150521LC  'ex 20140605LC
            y = dip.nRes / dip.n
            If y > 0 Then
              For ijk = 0 To appl.nSize1 - 1
                appl.tb1(ijk) = appl.tb1(ijk) + appl.tb2(ijk) * y
              Next ijk
            End If
          End If
          '--- Elabora la frazione che NON pu� avere la rendita contributiva
          If dip1.n > 0 Then   '20170802LC
            ReDim appl.tb2(appl.nSize1 - 1)
            Call TIscr2_Elabora(dip1, appl, glo, appl.tb2, fr, 0)  '20150521LC  'ex 20140605LC
            y = dip1.nRes / dip1.n
            If y > 0 Then
              For ijk = 0 To appl.nSize1 - 1
                appl.tb1(ijk) = appl.tb1(ijk) + appl.tb2(ijk) * y
              Next ijk
            End If
          End If
        ElseIf RC_MODEL = 2 Then
          '--- 20170805LC (inizio)
          '--- Elabora la frazione che pu� avere la rendita contributiva
          Call TIscr2_Elabora(dip, appl, glo, appl.tb2, fr, 0)  '20150521LC  'ex 20140605LC
          If dip.RcIt = 2 Then
            y = dip.nRes / dip.n * RC_FRAZ  'frazione che prende la rendita contributiva
          Else
            y = dip.nRes / dip.n  'l'ntera matricola NON prende la rendita contributiva
          End If
          If y > 0 Then
            For ijk = 0 To appl.nSize1 - 1
              appl.tb1(ijk) = appl.tb1(ijk) + appl.tb2(ijk) * y
            Next ijk
          End If
          '--- Elabora la frazione che NON prende la rendita contributiva
          If dip.RcIt = 2 Then
            dip1.nRes = dip.nRes * (1# - RC_FRAZ)
            If dip1.nRes > 0 Then
              dip1.n = dip.n * (1# - RC_FRAZ)
              dip1.RcKK = 1  'NON pu� generare riattivati e volontari
              ReDim appl.tb2(appl.nSize1 - 1)
              Call TIscr2_Elabora(dip1, appl, glo, appl.tb2, fr, 0)  '20150521LC  'ex 20140605LC
              y = dip1.nRes / dip1.n
              For ijk = 0 To appl.nSize1 - 1
                appl.tb1(ijk) = appl.tb1(ijk) + appl.tb2(ijk) * y
              Next ijk
            End If
          End If
          '--- 20170805LC (fine)
        End If
      Else
        If RC_MODEL = 0 Then
          Call TIscr2_Elabora(dip, appl, glo, appl.tb1, fr, 0) '20150521LC  'ex 20140605LC
        ElseIf RC_MODEL = 1 Then
          '--- Elabora la frazione che pu� avere la rendita contributiva
          If RC_FRAZ > 0# Then
            Call TIscr2_Elabora(dip, appl, glo, appl.tb1, fr, 0) '20150521LC  'ex 20140605LC
          End If
          '--- Elabora la frazione che NON pu� avere la rendita contributiva
          If RC_FRAZ < 1# Then
            Call TIscr2_Elabora(dip1, appl, glo, appl.tb1, fr, 0) '20150521LC  'ex 20140605LC
          End If
        ElseIf RC_MODEL = 2 Then
          '--- Elabora la matricola nell'ipotesi che pu� avere la rendita contributiva
          ReDim appl.tb2(appl.nSize1 - 1)
          Call TIscr2_Elabora(dip, appl, glo, appl.tb2, fr, 0)
          If dip.RcIt = 2 Then
            '--- 20170801LC (inizio)
            If dip.RcNe + dip.RcNa > 0 Then
              RC_FRAZ = (OPZ_RC_FRAZ_SIL * dip.RcNe + OPZ_RC_FRAZ_ATT * dip.RcNa) / (dip.RcNe + dip.RcNa)
            End If
            '--- 20170801LC (fine)
            '--- Consolida la frazione che pu� avere la rendita contributiva
            y = dip.nRes / dip.n * RC_FRAZ
            For ijk = 0 To appl.nSize1 - 1
              appl.tb1(ijk) = appl.tb1(ijk) + appl.tb2(ijk) * y
            Next ijk
            '--- Elabora e consolida la frazione che NON pu� avere la rendita contributiva
            dip1.nRes = dip.nRes
            ReDim appl.tb2(appl.nSize1 - 1)
            Call TIscr2_Elabora(dip1, appl, glo, appl.tb2, fr, 0)
            y = dip1.nRes / dip1.n * (1# - RC_FRAZ)
            For ijk = 0 To appl.nSize1 - 1
              appl.tb1(ijk) = appl.tb1(ijk) + appl.tb2(ijk) * y
            Next ijk
          Else
            '--- La matricola non ha la rendita contributiva
            y = dip.nRes / dip.n
            For ijk = 0 To appl.nSize1 - 1
              appl.tb1(ijk) = appl.tb1(ijk) + appl.tb2(ijk) * y
            Next ijk
          End If
        End If
      End If
      '--- 20170727LC (fine)  'ex 20170724LC  'ex 20150525LC
      '20080919LC (inizio)
      If appl.nPensMax < dip.nPens Then
        appl.nPensMat = dip.mat
        appl.nPensMax = dip.nPens
      End If
      '20080919LC (fine)
    Else
      iErr = -1
      szErr = "Record non elaborabile"
      If iop = 1 Then
        rsMat!iErr = iErr
        rsMat!sErr = Left(szErr, 50)
      End If
    End If
  Else
    If iop = 1 Then
      rsMat!iErr = iErr
      If iErr = -1 Then
        rsMat!sErr = Left(szErr, 50)
      Else
        rsMat!sErr = Left(Left(szErr, Len(szErr) - 2), 50)
      End If
    End If
  End If
  szErr = ""
  If iop = 1 Then
    rsMat!dElab = dNow
    rsMat!iElab = glo.nRead
    rsMat.Update
'rsMat.CancelUpdate
    rsMat.MoveNext
  End If
End Sub


Public Sub TAppl_Elab32_ProgressBar(ByRef appl As TAppl, ByRef glo As TGlobale, ByRef dip As TIscr2, ByRef bElab As Boolean, _
                                    ByRef jBlocco As Long, ByRef tim0 As Double, ByRef tim2 As Double, ByRef grMat As Variant, ByRef frm As frmElab)
  If appl.matr1 > dip.mat Then
    appl.matr1 = dip.mat
  ElseIf appl.matr2 < dip.mat Then
    appl.matr2 = dip.mat
  End If
  tim2 = Timer - tim2
  If tim2 > 0 And bElab Then
    If glo.tMax < tim2 Then glo.tMax = tim2
    If glo.tMin = 0 Or glo.tMin > tim2 Then glo.tMin = tim2
  End If
  If glo.nRead > 0 And (glo.nRead Mod 100 = 0 Or jBlocco = UBound(grMat, 2)) Then
    '20121022LC (inizio)
    If glo.MJ = MI_ASP Then  '20130306LC
    ElseIf glo.nElab > appl.nMatr Then
      Call glo.MdiAfp.StatusBar("Letti: " & appl.nMatr & " + " & glo.nRead - appl.nMatr & "   Elaborati:" & glo.nElab & " (" & Format(glo.nElab / glo.nRead, "#0.00%") & ")   Record/secondo:" & Format(glo.nRead / dmax(Timer - tim0, 1), "#,##0.00"))
    Else
      Call glo.MdiAfp.StatusBar("Letti: " & glo.nRead & "   Elaborati:" & glo.nElab & " (" & Format(glo.nElab / glo.nRead, "#0.00%") & ")   Record/secondo:" & Format(glo.nRead / dmax(Timer - tim0, 1), "#,##0.00"))
    End If
    '20121022LC (fine)
    If glo.nElab > 0 Then
      If glo.MJ = MI_ASP Then  '20130306LC
      ElseIf frm.ProgressBar1.Max < glo.nElab Then
        frm.ProgressBar1.Value = frm.ProgressBar1.Max
      Else
        frm.ProgressBar1.Value = glo.nElab
      End If
    End If
  End If
End Sub


Public Function TAppl_Elab32_TestStop(ByRef appl As TAppl, ByRef glo As TGlobale, ByRef frm As frmElab) As Byte
'************
'Test di STOP
'************
  Dim bStop As Byte
  
  If appl.bStop Then
    Select Case MsgBox("L'analisi sta per essere interrotta." & vbCrLf & "Si vuole comunque creare il report per i dati elaborati parzialmente ?", vbYesNo + vbDefaultButton1 + vbQuestion)
    Case vbYes
      bStop = 1
    Case vbNo
      frm.rtfOutput.Text = "Analisi interrotta:" & vbCrLf & vbCrLf _
                         & "Record letti: " & glo.nRead & vbCrLf _
                         & "Record elaborati:" & glo.nElab
      bStop = 2
    End Select
  End If
  TAppl_Elab32_TestStop = bStop
End Function


Public Function TIscr2_AbbQ#(BDA#(), tt&, xr#, iQ%)
'ex 20121203LC
'20130216LC (tutta)
  Dim xi&
  Dim a#, b#, dx#, fact#  '20130216LC
  
  If OPZ_ABB_QM_MODEL = 0 Then
    fact = 1#
  Else
    If tt <= P_DUECENTO Then
      If OPZ_ABB_QM_MODEL = 1 Then
        fact = (1# - OPZ_ABB_QM_PRM_2) * (1# - OPZ_ABB_QM_PRM_1) ^ (tt)
      ElseIf OPZ_ABB_QM_MODEL = 2 Then
        xi = Int(xr)
        dx = xr - xi
        If xi < UBound(BDA, 2) - 0 Then  '20130216LC (ex -1)
          fact = BDA(tt, xi, iQ)
          fact = fact + (BDA(tt, xi + 1, iQ) - fact) * dx
        Else
          fact = 1#
        End If
      End If
    Else
      '--- 20130216LC (inizio)
      'fact = TIscr2_AbbQ(BDA(), P_DUECENTO + 0, xr, iQ)
      If OPZ_ABB_QM_PRM_1 < 0 Or OPZ_ABB_QM_PRM_1 > 2 Then  'Nessuna etrapolazione
        fact = 1#
      Else
        a = TIscr2_AbbQ(BDA(), P_DUECENTO - 1, xr, iQ)
        b = TIscr2_AbbQ(BDA(), P_DUECENTO + 0, xr, iQ)
        If OPZ_ABB_QM_PRM_1 = 1 Then 'Estrapolazione lineare
          fact = b + (b - a) * (tt - P_DUECENTO)
        ElseIf OPZ_ABB_QM_PRM_1 = 2 Then  'Estrapolazione esponenziale
          fact = b * (b / a) ^ (tt - P_DUECENTO)
        End If
      End If
      '--- 20130216LC (fine)
    End If
    If fact > 1# Then
      fact = 1#
    ElseIf fact < 0# Then
      fact = 0#
    End If
  End If
  TIscr2_AbbQ = fact
End Function


Private Function TIscr2_Mont_Prd(i%) As Integer
'20120220LC
  Dim j%
  
  If 1 = 2 Then
    j = IIf(i = eom.om_2001, ecv.cv_Prd_Trmc0, ecv.cv_Prd_Trmc1)
  Else
    Select Case i
    Case eom.om_2001        '0
      j = ecv.cv_Prd_Trmc0
    Case eom.om_2002        '1
      j = ecv.cv_Prd_Trmc1
    Case eom.om_art25       '2
      j = ecv.cv_Prd_Trmc2
    '--- 20131122LC (inizio)
    Case eom.om_pens_att    '3  'ex 4
      j = ecv.cv_Prd_Trmc3
    Case eom.om_misto_obbl  '4  'ex 3
      j = ecv.cv_Prd_Trmc3
    Case eom.om_misto_fac   '5
      j = ecv.cv_Prd_Trmc5
    Case eom.om_misto_int   '6
      j = ecv.cv_Prd_Trmc6
    Case eom.om_misto_fig   '6
      j = ecv.cv_Prd_Trmc6  'mancando cv_Prd_Trmc7
    Case Else
      j = ecv.cv_Prd_Trmc1
    End Select
  End If
  TIscr2_Mont_Prd = j
End Function


Public Sub TIscr2_Aggiorna_Mont(ByRef dip As TIscr2, s0 As TIscr2_Stato, s1 As TIscr2_Stato, CoeffVar#(), anno%, bAcconto As Boolean, bSaldo As Boolean)
'20111212LC (tutta)
  Dim i%, j%, t%
  Dim y#
  Dim ya#(), ys#()
  
  With dip
    ReDim ya(eom.om_max) '20131122LC
    ReDim ys(eom.om_max) '20131122LC
    '--- Saldo ---
    If bSaldo = True Then
      t = anno - 1
      If anno = .annoAss Then
        i = -1
      ElseIf (s1.stato = ST_PAT) Then
        If s1.napa > 0 Then
          i = eom.om_pens_att
        Else
          i = -1
        End If
      Else
        i = s0.ico
      End If
      '---
      If i < 0 Then
      ElseIf i = eom.om_2001 Then
        ys(i) = s0.c_sogg1a + s0.c_sogg2a + s0.c_sogg3a
        ys(eom.om_Int0) = s0.c_int1a + s0.c_int2a + s0.c_int3a  '20150512LC  'ex 20121018LC
      ElseIf i = eom.om_misto_obbl Then
        '--- 20131123LC (inizio)
        ys(i) = s0.c_sogg1a + s0.c_sogg2a
        ys(eom.om_Sogg0) = s0.c_sogg3a
        '--- 20160606LC (inizio)
        ys(eom.om_misto_int) = s0.c_int1a + s0.c_int2a + s0.c_int3a
        '--- 20160606LC (fine)
        '--- 20131123LC (fine)
      Else
        ys(i) = s0.c_sogg1a + s0.c_sogg2a
        ys(eom.om_Sogg0) = s0.c_sogg3a
        ys(eom.om_misto_int) = s0.c_int1a + s0.c_int2a + s0.c_int3a
      End If
    End If
    '--- Acconto ---
    If bAcconto = True Then
      t = anno
      If (s1.stato = ST_PAT) Then
        'If 1 = 2 And s1.napa > 0 Then '20111231LC
        If s1.napa > 0 Then  '20150322LC
          i = eom.om_pens_att
        Else
          i = -1
        End If
      ElseIf (s1.stato = ST_PNA) Then '20120115LC
        i = -1
      ElseIf s1.stato <> ST_A Then '20111231LC
        If .gruppo0 = 3 Or .gruppo0 = 4 Then '20120115LC
          i = -1
        Else
Call MsgBox("TODO TIscr_Aggiorna_Mont 1", , .mat)
Stop
        End If
      Else
        i = s1.ico
      End If
      '---
      If i < 0 Then '20120112LC
      ElseIf i = eom.om_2001 Then
        ya(i) = s1.c_sogg0a1 '20120514LC
      ElseIf i = eom.om_misto_obbl Then
        '--- 20131123LC (inizio)
        ya(i) = s1.c_sogg0a1
        '--- 20160606LC (inizio)
        ya(eom.om_misto_int) = s1.c_int0a
        '--- 20160606LC (fine)
        '--- 20131123LC (fine)
      Else
        ya(i) = s1.c_sogg0a1 '20120514LC
        ya(eom.om_Int0) = s1.c_int0a
      End If
    End If
    '--- Facoltativo ---
    '20120514LC (inizio)
    If bAcconto = True And i = eom.om_misto_obbl And s1.c_sogg0a2 > AA0 Then
      i = eom.om_misto_fac
      ya(i) = s1.c_sogg0a2
    End If
    '20120514LC (fine)
    '--- Completamento ---
    For i = eom.om_min To eom.om_max  '20131122LC
      j = TIscr2_Mont_Prd(i) 'IIf(i = eom.om_2001, ecv.cv_Prd_Trmc0, ecv.cv_Prd_Trmc1)
      y = ya(i) + ys(i)
      If y <> 0 Then
        .om_rcU(i) = .om_rcU(i) + y
        '.om_rcU(eom.om_tot) = .om_rcU(eom.om_tot) + y
        If i < eom.om_Sogg0 Then
          .om_rcV(i) = .om_rcV(i) + y / CoeffVar(anno, j)  'MontRiv(2, anno) / MontRiv(2, 2010)
          '.om_rcV(eom.om_tot) = .om_rcV(eom.om_tot) + y / CoeffVar(anno, j)
'20130610LC
'If t >= 2030 Or (i = -3) Then
If i >= 3 Then
i = i
End If
'End If
        End If
      End If
    Next i
    If (bAcconto = True) Or (s1.stato = ST_PAT) Then
      For i = eom.om_min To eom.om_max  '20131122LC
        s1.om_rcU(i) = .om_rcU(i)
        s1.om_rcV(i) = .om_rcV(i)
      Next i
      '20120117LC (fine)
's1.om_rcU(eom.om_tot) = s1.om_rcU(eom.om_tot)
    End If
    '--- 20160609LC (inizio)
    dip.FirrU = dip.om_rcU(om_misto_int)
    dip.FirrV = dip.om_rcV(om_misto_int)
If dip.FirrV > AA0 Then
dip.FirrV = dip.FirrV
End If
    '--- 20160609LC (fine)
  End With
End Sub


Public Sub TIscr2_Aggiorna_Redd0(ByRef dip As TIscr2, ByRef s0 As TIscr2_Stato, ByRef s1 As TIscr2_Stato, ByRef appl As TAppl, ByRef CoeffVar#(), ByRef tia As TIscr2Aux)
'20150421LC  CoeffVar
'20150512LC  tia
  Dim ia&
  Dim cvSogg1Max#, cvSogg1Al#, cvAss1Al '20150511LC  'ex 20150410LC

  With dip
    ia = appl.t0 - .t
    '***********************************
    'Reddito Irpef e volume d'affari IVA
    '***********************************
    '--- Legge la tabella IRPEF ---
    '--- 20150322LC (inizio)
    If .t < .annoAss Then
      s1.sm = 0#
      s1.nMandati2 = 0#
    Else
      '--- 20150410LC (inizio)
      cvSogg1Max = CoeffVar(.t, ecv.cv_Sogg1Max + dip.Qualifica - 1)
      cvSogg1Al = CoeffVar(.t + 1, ecv.cv_Sogg1Al + 0)   '20150511LC
      cvAss1Al = CoeffVar(.t + 1, ecv.cv_Ass1Al + 0)     '20150511LC
'--- 20150421LC (inizio) TEMPTEMP
'If cvSogg1Max <> modEneaSwa.MaxMP(dip.Qualifica + 0, .t * 10 + 4, 1) Then
'  call MsgBox("TODO")
'  Stop
'End If
'--- 20150421LC (inizio)
      s1.sm = tia.sm(ia)
      If s1.sm <= AA0 Then
        s1.nMandati2 = 0
      ElseIf dip.Qualifica < 2 Then
        s1.nMandati2 = 1
      '--- 20150612LC (inizio)
      ElseIf 1 = 1 Then
        s1.nMandati2 = dip.nMandati1
      Else
      '--- 20150612LC (fine)
        s1.nMandati2 = 2
        If cvSogg1Al + cvAss1Al <= AA0 Or cvSogg1Max <= AA0 Then  '20150511LC
        ElseIf s1.nMandati2 * cvSogg1Max < s1.sm Then
          s1.nMandati2 = Int(s1.sm / cvSogg1Max + AA9)
        End If
      End If
      '--- 20150410LC (inizio)
    End If
    '--- 20150322LC (fine)
    '--- Legge la tabella IVA --
    '--- 20150307LC (inizio)  'ex 20130326LC
    's1.sm = IIf(.t < 2000, 100, IIf(.t = 2000, 100000, 10000))  '20130326LC (commentata)  'ex 20130321LC
    s1.iva = 0
    '--- 20150307LC (fine)
  End With
ExitPoint:
End Sub


Public Sub TIscr2_Aggiorna_Redd1(ByRef dip As TIscr2, ByRef appl As TAppl, ByRef s0 As TIscr2_Stato, ByRef s1 As TIscr2_Stato, ByRef CoeffVar#(), ByRef Lsm#())
'***********************************
'Reddito Irpef e volume d'affari IVA
'***********************************
  Dim napa%  '20150612LC
  Dim iLsm&
  Dim cden#, cnum#, fact#, fact1#, fact2#, h#, x#
  'Dim s1max#  '20150410LC
  
  With dip
    s1.bAzzeraReddito = 0  '20150504LC
    If (s1.stato = ST_A) Then 'Contribuzione da attivo
      s1.it = 1
    ElseIf (s1.stato = ST_PAT) And (s1.napa > 0) Then 'Contribuzione da pensionato
      s1.it = 2
      '--- 20150410LC (inizio)
      If s1.tapa = s1.napa - 1 Then
        '--- 20150612LC (inizio)
        napa = appl.CoeffVar(dip.t, ecv.cv_SpAnni)
        If napa < 72 - dip.t + Year(dip.DNasc) Then
          napa = 72 - dip.t + Year(dip.DNasc)
        End If
        'If Int(.xr + 1) + s1.napa > CoeffVar(.t, ecv.cv_SpEtaMax) Or s1.sm <= AA0 Then '20140307LC  'riportare 20140111LC-24
        If Int(.xr + 1) + napa > CoeffVar(.t, ecv.cv_SpEtaMax) Or s1.sm <= AA0 Then '20140307LC  'riportare 20140111LC-24
          '--- 20150504LC (inizio)
          If OPZ_INT_AZZERA_REDDITO = 1 Then
            s1.bAzzeraReddito = 12
          ElseIf OPZ_INT_AZZERA_REDDITO = 2 Then
            s1.bAzzeraReddito = 3
          End If
          '--- 20150504LC (fine)
        End If
        '--- 20150612LC (fine)
      End If
      '--- 20150410LC (fine)
    ElseIf (s1.stato = ST_PAT) And (s1.napa <= 0) Then
      s1.stato = ST_PNA ':=
      If s1.it = 3 Then
        s1.it = 0
      Else
        s1.it = 3
      End If
    Else
      s1.it = 0
    End If
    '---
    If s1.it > 0 Then
      '--- 20150504LC (inizio)  ex 20150410LC
      If s1.bAzzeraReddito >= 12 Or s0.bAzzeraReddito > 0 Then
        s1.sm = 0#
        s1.iva = 0#
      '--- 20150504LC (fine)
      '--- 20150526LC (inizio)
      ElseIf dip.gruppo0 = 5 Then
        s1.sm = 0#
        s1.iva = 0#
      '--- 20150526LC (fine)
      '--- 20150609LC (inizio)  'ex 20150527LC
      ElseIf dip.gruppo0 = 6 Then
        If dip.t = Year(dip.DCan) + 1 Then
           s1.sm = ARRV((dip.s(dip.t - 1).sm + dip.s(dip.t - 2).sm + dip.s(dip.t - 3).sm) / 3#)
        Else
          s1.sm = s0.sm
        End If
        s1.iva = 0#
      '--- 20150609LC (inizio)  'ex 20150527LC
      Else
        h = s1.hq + 1 '+ .d1 '20120112LC (+1)
        x = .xan + 1 + .c1 '20120112LC (+1)
        iLsm = IIf(s1.stato = ST_PAT, 1, IIf(.anno1 > .anno0, 2, 0)) '20120220LC
        '--- 20150528LC-2 (inizio)
        If .t = .annoAss Or (.PensAUS = .t) And (.gruppo00 = 5 Or .gruppo00 = 6) Then
          s1.sm = .irpefNI
          s1.iva = .ivaNI
        ElseIf .t > .annoAss Then
        '--- 20150528LC-2 (fine)
          '--- 20150612LC (inizio)
          If dip.t = appl.t0 + 1 Then
            If .irpefNI > 0 Then
              s0.sm = .irpefNI
              s1.sm = .irpefNI
              '--- 20160606LC (inizio)
              s0.iva = .ivaNI
              s1.iva = .ivaNI
              '--- 20160606LC (fine)
            End If
          End If
          '--- 20150612LC (fine)
          cden = InterpLSM(h - 1, x - 1, Lsm(), iLsm, .sesso, .Qualifica, .iTC)
          cnum = InterpLSM(h, x, Lsm(), iLsm&, .sesso, .Qualifica, .iTC)
          '--- 20140111LC-20 (inizio) (mat=863080?)
          'If cden > 0 Then
          If cden > AA0 And cnum > AA0 Then
            fact = cnum / cden
          '20150522LC (ripristinato)  'ex 20150407LC (saltato, pensavo che con ZZZ non andasse bene)  'ex 20140703LC  'riportare 20140111LC-24
          ElseIf iLsm = 1 Then
            fact = 0#
          Else
            fact = 1#
          End If
          '--- 20140111LC-20 (fine)
          If s1.it <> 1 Then
            fact1 = fact * (1 + CoeffVar(.t, ecv.cv_Ts1p))
            fact2 = fact * (1 + CoeffVar(.t, ecv.cv_Ts2p))
          ElseIf .anno1 > .anno0 Then
            fact1 = fact * (1 + CoeffVar(.t, ecv.cv_Ts1n))
            fact2 = fact * (1 + CoeffVar(.t, ecv.cv_Ts2n))
          Else
            fact1 = fact * (1 + CoeffVar(.t, ecv.cv_Ts1a))
            fact2 = fact * (1 + CoeffVar(.t, ecv.cv_Ts2a))
          End If
          '--- 20150410LC (inizio)
          'If s1.IT > 0 Then
            'If s0.sm <> -1# Then
            If s0.sm > AA0 Then  '20150410LC
              s1.sm = ARRV(s0.sm * fact1)
            Else
              s1.sm = s0.sm
            End If
          'Else
          '  s1.sm = -1#
          'End If
          '---
          'If s0.iva <> -1# Then
          If s0.iva > AA0 Then  '20150410LC
            s1.iva = ARRV(s0.iva * fact2)
          Else
            s1.iva = s0.iva
          End If
          's1.nMandati2 = s0.nMandati2
          's1.nMandati2 = dip.nMandati1  '--- 20150118LC (commentato perch� spostato in alto)
          '--- 20150410LC (fine)
          '--- 20150504LC (inizio)
          If s1.bAzzeraReddito > 0 Then
            s1.sm = ARRV(s1.sm / 12#) * (12# - s1.bAzzeraReddito)
            s1.iva = ARRV(s1.iva / 12#) * (12# - s1.bAzzeraReddito)
          End If
          '--- 20150504LC (fine)
        Else
          Call MsgBox("Errore inatteso per la matricola " & .mat & vbCrLf & "Salario iniziale non calcolabile.", vbCritical)
          Stop
        End If
      End If
    Else
      s1.sm = 0#
      s1.iva = 0#
    End If
  End With
ExitPoint:
End Sub


'20121105LC
Public Sub TIscr2_Aggiorna_Stato(ByRef dip As TIscr2, s0 As TIscr2_Stato, s1 As TIscr2_Stato, CoeffVar#())
  Dim hRetr#, hretrI#
  Dim na&
  
  With dip
    '****************
    'Inizializzazione
    '****************
    '--- 20140310LC (inizio) dovrebbe essere ininfluente per la vecchia versione  'riportare 20140111LC-26
    If .TipoElab > 1 And .t >= .TipoElab Then
      s1.bIsMisto = True
    Else
      s1.bIsMisto = False
    End If
    '---
    s1.bIsArt25 = False
    If .gruppo0 = 1 And s1.bIsMisto = False Then
    '--- 20140310LC (fine)  'riportare 20140111LC-26
      If OPZ_MOD_ART25_MODEL <> 0 Then
        If .t >= OPZ_MOD_ART25_ANNO And OPZ_MOD_ART25_ANNO > 0 Then '20121010LC
          If s1.sm < CoeffVar(.t, ecv.cv_SoggArt25) And CoeffVar(.t, ecv.cv_SoggArt25) > AA0 Then
            If s1.iva < CoeffVar(.t, ecv.cv_IntArt25) And CoeffVar(.t, ecv.cv_IntArt25) > AA0 Then
              '.mat = 67000 (2009)
              s1.bIsArt25 = True
            End If
          End If
        End If
      End If
    End If
    '---
    If .t - OPZ_PRORATA_ANNO < 0 Then
      s1.bIsProRata = False
    'ElseIf dip.anno1 >= OPZ_PRORATA_ANNO Then
    ElseIf dip.anno1 >= OPZ_PRORATA_ANNO And 1 = 2 Then                      '20150307LC 'rip 20130326LC TODO capire perch� false
      s1.bIsProRata = False
    Else
      s1.bIsProRata = True
    End If
    '---
    If .t >= .annoFineCR Then
      s1.ic = 0 'Intera
    Else
      s1.ic = 1 'Ridotta
    End If
    '--- 20150322LC (inizio)
    If s1.bIsMisto = True Then
      s1.ico = eom.om_misto_obbl
    ElseIf s1.bIsProRata Then
      s1.ico = eom.om_2002
    Else
      s1.ico = eom.om_2001
    End If
    '--- 20150322LC (fine)
    '--- 20150322LC (fine)
    '---
    If s1.bIsProRata = True Then
      s1.ipr = 1
    Else
      s1.ipr = 0
    End If
    '---
    If .t > .annoAss Then  '20150307LC
      s1.mi = 12
    '--- 20150307LC (inizio)
    Else
      If .DCan > 0 And Year(.DCan) = .annoAss Then
        s1.mi = Month(.DCan) - Month(.dAssunz) + 1
      Else
        s1.mi = 13 - Month(.dAssunz)
      End If
      s1.mi = Int((s1.mi - 1) / 3 + 1) * 3
    '--- 20150307LC (fine)
    End If
    '---
    '20120514LC (inizio)
    s1.ALR = 0
    'If OPZ_MISTO_INTEGR_MODEL > 0 Then
    'If (OPZ_MISTO_INTEGR_MODEL > 0) And (.gruppo0 = 1) And (s1.napa = 0) Then  '20120601LC
    If (OPZ_MISTO_INTEGR_MODEL > 0) And (.gruppo0 = 1) And (s1.napa = 0) And (OPZ_MOD_ART_26_5 = 0 Or s1.bPensAtt = False) Then '20121022LC
      If s1.bIsMisto = True Then '20120515LC
        '20121009LC (inizio)
        'OPZ_MISTO_INTEGR_MODEL 'METODO MISTO - INTEGRATIVO: MODELLO
        '0:no retr.
        '1:retr.costante
        '2:retr.crescente nel tempo
        '3:retr.costante per nuovi iscritti e crescente per vecchi
        '4:definitivo (OPZ_MOD_ART_26_5 = 1)
        If OPZ_MOD_ART_26_5 <> 0 Then
          If .t = .TipoElab Or (.t > .TipoElab And .t = .anno1) Then
            'xp = .xr + fraz
'mat     T s1    anDNasc   X      hRetr     hRetrI
'274302  2 1 15/01/1948  64  33,925000  16,708333
'222639  2 1 23/01/1948  64  45,166667  25,166667
'385549  1 1 15/12/1948  64  37,469444  18,675
'712811  1 1 15/06/1977  35  14,750000   9,75
'810796  2 1 24/06/1983  29   9,930556   3,95
            '20140111LC-4 (mat=28923,181704)
            'hretr = s1.AnzTot + AA0 '20121023LC
            hRetr = Int(s1.AnzTot * 360# + AA0) / 360#
            If hRetr > 0 Then '(per n.i. vale -0,5)  'TODO: l'anz. senza ric./risc. va letta da DB, comunque � � antieconomico toglierla
              hretrI = (Data360("31/12/" & .t - 1) - Data360(.dIscrFondo) + 1) / 360 'Anzianit� di iscrizione
              If hRetr > hretrI Then
                hRetr = hretrI
              End If
            End If
            '--- 20150223LC (inizio)
            '**************
            'Calcolo di ALR
            '**************
            s1.ALR = 0
          ElseIf .t > .TipoElab Then
            s1.ALR = s0.ALR
          Else
Call MsgBox("TODO - TIscr2_Aggiorna_Contr 1b", , .mat)
Stop
          End If
        Else
        '20121009LC (fine)
          If OPZ_MISTO_INTEGR_MODEL = 3 Then
            na = OPZ_MISTO_INTEGR_ANNI + .t - (2 * .TipoElab - .annoAss)
          ElseIf OPZ_MISTO_INTEGR_MODEL = 2 Then
            na = .t - .TipoElab + 1
          Else
            na = OPZ_MISTO_INTEGR_ANNI
          End If
          If na >= OPZ_MISTO_INTEGR_ANNI Or OPZ_MISTO_INTEGR_ANNI < 1 Then
            s1.ALR = OPZ_MISTO_INTEGR_ALR
          ElseIf na > 0 Then
            s1.ALR = OPZ_MISTO_INTEGR_ALF + (OPZ_MISTO_INTEGR_ALR - OPZ_MISTO_INTEGR_ALF) * na / OPZ_MISTO_INTEGR_ANNI
          End If
        End If
      End If
    End If
    '20120514LC (fine)
  End With
End Sub


Public Sub TIscr2_Aggiorna_X(ByRef dip As TIscr2, s1 As TIscr2_Stato, Parm1() As Variant, Lsm1#(), CoeffRiv1#(), CoeffVar#())
  Dim dAnz As TIscr2_Anz '20111226LC
  
  With dip
    .xan = .xan + 1
    .xr = .xr + 1
    '--- 20150518LC (inizio)
    If .t <= .anno1 Then
      'Skip aumento anzianit�
    Else
    '--- 20150518LC (fine)
      '20111226LC (inizio)
      If .rr_dhEff > AA0 Or .gruppo0 > 1 Then '20111216LC
        If .t > .anno1 Then
          Call TIscr2_Anz_Dh(dAnz, dip, .t, CoeffVar#(), .rr_dhEff)
        ElseIf .anno1 <= .anno0 Then
  Call MsgBox("TODO TIscr_Aggiorna_X 1", , .mat)
  Stop
        Else
          Call TIscr2_Anz_Dh(dAnz, dip, .t, CoeffVar#(), .rr_dhEff * dip.d1)  '20150518LC  'ex 0.5
        End If
        If s1.bIsMisto = False Then
          If s1.bIsProRata = False Then
            s1.h_retr_ante1 = s1.h_retr_ante1 + .rr_dhEff
          Else
            s1.h_retr_post1 = s1.h_retr_post1 + .rr_dhEff
          End If
          's1.h_contr_ante = s1.h_contr_ante
          's1.h_contr_post = s1.h_contr_post
        Else
          's1.h_retr_ante = s1.h_retr_ante
          's1.h_retr_post = s1.h_retr_post
          If s1.bIsProRata = False Then
            s1.h_contr_ante1 = s1.h_contr_ante1 + .rr_dhEff
          Else
            s1.h_contr_post1 = s1.h_contr_post1 + .rr_dhEff
          End If
        End If
      Else
        If s1.bIsProRata = False Then
          s1.h_contr_ante1 = s1.h_contr_ante1 + 1
        Else
          s1.h_contr_post1 = s1.h_contr_post1 + 1
        End If
      End If
      '---
      Call TIscr2_Anz_Add(s1.anz, dAnz)
      s1.AnzTot = s1.h_retr_ante1 + s1.h_retr_post1
      s1.hs = s1.AnzTot + s1.h_contr_ante1 + s1.h_contr_post1 '20111213LC
      s1.hq = s1.hq + IIf(.rr_dhEff0 > 0, 1, 0)
      s1.hf = s1.hf + IIf(.rr_dhEff0 > 0, 1, 0)
      '20111226LC (fine)
    End If
  End With
End Sub


Private Function TIscr2_bEA(ByRef dip As TIscr2) As Boolean
  TIscr2_bEA = dip.gruppo0 = 5
End Function


'20131123LC
Public Function TIscr2_bFig(ByRef dip As TIscr2, h#) As Boolean
  If OPZ_MISTO_AGEV_GIOV = 0 Then
    TIscr2_bFig = False
  ElseIf OPZ_MISTO_AGEV_GIOV = 1 Then
If h = 0# Then
  Call MsgBox("TODO TIscr2_bFig", , dip.mat)
  Stop
End If
    TIscr2_bFig = True
  ElseIf OPZ_MISTO_AGEV_GIOV = 2 Then
    TIscr2_bFig = (h >= 25#)
  End If
End Function


Public Function TIscr2_Elabora(ByRef dip As TIscr2, ByRef appl As TAppl, ByRef glo As TGlobale, ByRef tb1() As Double, _
                               ByRef fr&, ByRef iop&) As Double
'ex 20121031LC
'20140609LC  fr
'20150521LC  tb1, iop
'20160323LC  nRes
  Dim f&, it&, i&, t&
  Dim tim1#, zn#
  Dim nRes As Double  '20160323LC
  
  tim1 = Timer
  OPZ_INT_TOLL_PENS = OPZ_TOLL_PENS + AA0 '20160420LC
  If OPZ_RND_NIT > 1 Then
    Dim dip0 As TIscr2
    
    '--- 20160523LC (inizio)
    nRes = 0#
    dip.nRes = dip.nRes / OPZ_RND_NIT
    '--- 20160523LC (fine)
    dip0 = dip
  End If
  For it = 1 To OPZ_RND_NIT
    With dip
      Call Randomize(.mat + it * 1000000#)   '20160223LC (overflow long)
      .t = appl.t0 + .tau0  '20121104LC  'ex 20080915LC
      zn = .n / OPZ_RND_NIT
      If (.s(.t).stato = ST_S) Then
        .zs = zn
        'Il resto in TIscr2_MovPop_2
      Else
        'Call TIscr2_MovPop_Superstiti(dip, appl)
      End If
'.s(.t).stato = ST_PNA: .caus = CONTR_PB_0 * 1: .pwDB = 10000
      .dx = dxRend(OPZ_ABB_QM_MODEL, .sesso, Year(.DNasc))
      If (.s(.t).stato = ST_A) Then
        '--- 20150504LC (inizio)
        .n0.c0a_z = 0#
        .n0.c0v_z = 0#
        .n0.c0e_z = 0#
        If .s(.t).caus = CONTR_VA Then
          .n0.c0v_z = zn
          '--- 20150526LC (inizio)
          .wb(0).vg2sm = .s(.t).sm
          .wb(0).vg2rcV = dip.om_rcV(eom.om_misto_obbl)
          '--- 20150526LC (fine)
        ElseIf .s(.t).caus = CONTR_EA Then
          .n0.c0e_z = zn
        Else
          .s(.t).caus = CONTR_AA
          .n0.c0a_z = zn
        '--- 20150504LC (fine)
        End If
      '--- 20131202LC (inizio)
      ElseIf (.s(.t).stato = ST_PAT) Then
        .n0.c1cvo_z = zn
        '--- 20181121LC-1 (inizio)  'ex 20131202LC  'ex 20121115LC
        'Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPC_VO, 1, 2, .xr, .s(.t).AnzTot, .pwDB, .pwDB_fc, .pwDB_fa1, zn, EFirr.Firr_PA)
        Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPC_VO, 1, 2, .xr, .s(.t).anz.h, .pwDB, .pwDB_fc, .pwDB_fa1, zn, EFirr.Firr_PA)
        '--- 20181121LC-1 (fine)
      '--- 20131202LC (fine)
      ElseIf (.s(.t).stato = ST_PNA) Then
'CP   Num  matMin  matMax        Caus       catPens
'-1                        CONTR_PT_0  CAT_PENS_SOS 'Pensione sostitutiva
'-2                        CONTR_PS_0  CAT_PENS_TOT 'Pensione di totalizzazione
' 1  4005      42  640691  CONTR_PW_0  CAT_PENS_VEC 'Pensione di vecchiaia
' 2                                    CAT_PENS_REV 'Pensione di reversibilit�
' 3                                    CAT_PENS_SUP 'Pensione ai superstiti
' 4    49    8966  617001  CONTR_PI_0  CAT_PENS_INV 'Pensione di invalidit�
' 5   371    1067  390379  CONTR_PA_0  CAT_PENS_ANZ 'Pensione di anzianit�
' 6   118    6976  693446  CONTR_PB_0  CAT_PENS_INA 'Pensione di inabilit�
' 7    (1     239     239) CONTR_PA_0  CAT_PENS_ALT 'Pensione di altro tipo
'91     2   21296  212570
'92  (203   14293  651024)
'93 (5643      23  684973)
        '20080915LC (inizio)
        If .s(.t).caus = CONTR_PB_0 Then
          .n0.c1b_z = zn
          Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPN_B, 1, 3, .xr, .s(.t).AnzTot, .pwDB, .pwDB_fc, .pwDB_fa1, _
                                 zn, EFirr.Firr_A)  '20131202LC  'ex 20121115LC
        ElseIf .s(.t).caus = CONTR_PI_0 Then
          .n0.c1i_z = zn
          Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPN_I, 1, 3, .xr, .s(.t).AnzTot, .pwDB, .pwDB_fc, .pwDB_fa1, _
                                 zn, EFirr.Firr_A)  '20131202LC  'ex 20121115LC
        ElseIf .s(.t).caus = CONTR_PA_0 Then
          .n0.c1na_z = zn
          Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPN_A, 1, 3, .xr, .s(.t).AnzTot, .pwDB, .pwDB_fc, .pwDB_fa1, _
                                 zn, EFirr.Firr_A)  '20131202LC  'ex 20121115LC
        ElseIf .s(.t).caus = CONTR_PT_0 Then
          .n0.c1nt_z = zn
          Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPN_T, 1, 3, .xr, .s(.t).AnzTot, .pwDB, .pwDB_fc, .pwDB_fa1, _
                                 zn, EFirr.Firr_A) '20131202LC  'ex 20121115LC
        ElseIf .s(.t).caus = CONTR_PS_0 Then
          .n0.c1ns_z = zn
          Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPN_S, 1, 3, .xr, .s(.t).AnzTot, .pwDB, .pwDB_fc, .pwDB_fa1, _
                                 zn, EFirr.Firr_A)  '20131202LC  'ex 20121115LC
        '--- 20131202LC (inizio)  TODOTODO: TB_MPN_VA e TB_MPN_VP
        ElseIf .s(.t).caus = CONTR_PW_0 Then
          .n0.c1nvo_z = zn
          Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPN_VO, 1, 3, .xr, .s(.t).AnzTot, .pwDB, .pwDB_fc, .pwDB_fa1, _
                                 zn, EFirr.Firr_A)  '20131202LC  'ex 20121115LC
        ElseIf .s(.t).caus = CONTR_PW_0 Then
          .n0.c1nva_z = zn
          Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPN_VA, 1, 3, .xr, .s(.t).AnzTot, .pwDB, .pwDB_fc, .pwDB_fa1, _
                                 zn, EFirr.Firr_A)  '20131202LC  'ex 20121115LC
        ElseIf .s(.t).caus = CONTR_PW_0 Then
          .n0.c1nvp_z = zn
          Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPN_VP, 1, 3, .xr, .s(.t).AnzTot, .pwDB, .pwDB_fc, .pwDB_fa1, _
                                 zn, EFirr.Firr_A)  '20131202LC  'ex 20121115LC
        ElseIf 1 = 1 Then
          .n0.c1nvo_z = zn
          Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPN_VO, 1, 3, .xr, .s(.t).AnzTot, .pwDB, .pwDB_fc, .pwDB_fa1, _
                                 zn, EFirr.Firr_A)  '20131202LC  'ex 20121115LC
        ElseIf 1 = 1 Then
          .n0.c1nva_z = zn
          Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPN_VA, 1, 3, .xr, .s(.t).AnzTot, .pwDB, .pwDB_fc, .pwDB_fa1, _
                                 zn, EFirr.Firr_A)  '20131202LC  'ex 20121115LC
        ElseIf 1 = 1 Then
          .n0.c1nvp_z = zn
          Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPN_VP, 1, 3, .xr, .s(.t).AnzTot, .pwDB, .pwDB_fc, .pwDB_fa1, _
                                 zn, EFirr.Firr_A)  '20131202LC  'ex 20121115LC
        '--- 20131202LC (fine)
        End If
        '20080915LC (inizio)
      ElseIf (.s(.t).stato = ST_W) Then
        .ZW = zn
      End If
      Call TIscr2_MovPop_New(dip, appl, glo, tb1, fr, iop) '20150521LC  'ex 20140609LC
    End With
    If OPZ_RND_NIT > 1 Then
      '--- 20160523LC (inizio)
      nRes = nRes + dip.nRes
      '--- 20160523LC (fine)
      dip = dip0
    End If
  Next it
  '--- 20160523LC (inizio)
  If OPZ_RND_NIT > 1 Then
    dip.nRes = nRes
  End If
  '--- 20160523LC (fine)
  tim1 = OPZ_RND_NIT / (Timer - tim1 + 1E-20)
'call MsgBox("Record al secondo:" & tim1
End Function


Private Function TIscr2_ErrTest&(ByRef dip As TIscr2, s1 As TIscr2_Stato, XMIN&, XMAX&, hmax&, hlim&, dBil As Date, szErr$)
  '***********************************************************************
  'Test di correttezza dei dati della "scheda" con elaborazione statistica
  'dei dati letti e stampa degli eventuali messaggi di errore
  '***********************************************************************
  Dim iErr&, i&, j&
  Dim tau
  
  With dip
    '****************
    'Inizializzazione
    '****************
    iErr = 0
    tau = .anno1 - .anno0
    '***************************
    'Ricerca di eventuali errori
    '***************************
    '--- Test iniziali ---
    If .gruppo0 < 0 Or .gruppo0 > 6 Then  '20150514LC-2
      iErr = iErr + ErrQualifica
      szErr = szErr & "Guppo non valido" & vbCrLf
    ElseIf Val(.Qualifica) < 1 Or Val(.Qualifica) > 2 Then
      iErr = iErr + ErrQualifica
      szErr = szErr & "Qualifica non valida" & vbCrLf
    End If
    If .sesso < 1 Or .sesso > 2 Then
      iErr = iErr + ErrQualifica
      szErr = szErr & "Sesso non valido" & vbCrLf
    End If
    If .DNasc = 0 Then
      If .gruppo0 <> 4 Then
        iErr = iErr + ErrDataNascita
        szErr = szErr & "Data di nascita non valida" & vbCrLf
      End If
    End If
    If .dAssunz = 0 Then
      If .gruppo0 = 1 Then
        iErr = iErr + ErrDataAssunzione
        szErr = szErr & "Data di iscrizione non valida" & vbCrLf
      End If
    End If
    '--- Test centrali ---
    If .s(.annoAss).sm <= 0 Then
      'iErr = iErr + ErrRetrAnnuaPensione
    End If
    If (.gruppo0 <> 1) And (.gruppo0 <> 5) And (.gruppo0 <> 6) And (.pwDB <= 0) Then  '20150527LC
      iErr = iErr + ErrPensione  '20150514LC-2
      szErr = szErr & "Importo della pensione non valido" & vbCrLf
    End If
    '--- Test finali ---
    If .xan + tau < XMIN Then
      iErr = iErr + ErrDataEtaMin
      szErr = szErr & "Et� inferiore al minimo" & vbCrLf
    End If
    If .xan > XMAX Then
      If .gruppo0 <> 4 Then
        iErr = iErr + ErrDataEtaMax
        szErr = szErr & "Et� superiore al massimo" & vbCrLf
      End If
    End If
    If Int(.s(.t).hs) > hlim Then '20111213LC - TODO?
      If (.gruppo0 = 1) Or (.gruppo0 = 5) Then   '20130103LC
        iErr = iErr + ErrAnzianitaMax
        szErr = szErr & "Anzianit� superiore al massimo" & vbCrLf
      End If
    End If
    If .DNasc >= .dAssunz Then
      If .gruppo0 = 1 Then
        iErr = iErr + ErrDataNascAss
        szErr = szErr & "Data di nascita posteriore alla data di assunzione" & vbCrLf
      End If
    End If
  End With
  TIscr2_ErrTest = iErr
End Function


Private Function TIscr2_h#(ByRef dip As TIscr2)
  With dip
    TIscr2_h = .s(.t).hs + .d1
  End With
End Function


Private Function TIscr2_LexPensRivaluta(ByRef s() As TIscr2_Stato, CoeffRiv#(), ByRef CoeffVar#(), ByRef anno%, ByRef annoRid%, ByRef temp#(), ByRef n%) As Integer
'20120112LC - afpnew
  Dim anno0%, i%
  Dim y#, ymin#, ymax#, z#

If annoRid >= 2012 And OPZ_MISTO_REDD_PENS > 0 Then '20120110LC - temp
'call MsgBox("TODO-LexPensRivaluta"
'Stop
End If
'Open "c:\pippo.txt" For Output As #99: Close #99
'20130610LC  'TEMPTEMP
'If anno >= 2022 Then
'y = y
'End If
  ReDim temp(n - 1)
  For i = 0 To n - 1
    temp(i) = -1#
  Next i
  i = 0
  For anno0 = anno - 1 To LBound(s) Step -1
    If anno0 > annoRid Then
      y = -1#
    Else
      y = s(anno0).sm
    End If
    '--- 20140111LC-21 (inizio)
    'If y = -1# Then
    If y < 0# Then
    '--- 20140111LC-21 (fine
      y = y
    Else
      If anno0 >= 1982 Then
        ymax = CoeffVar(anno0, ecv.cv_ScG) '20120301LC 'ex cv_ScE
        If y > ymax Then
          y = ymax
        End If
      Else
        ymax = y
      End If
      '--- 20150307LC (inizio)  'ex 201303221LC
      y = y * CoeffVar(anno - 1, ecv.cv_prd_Tev) / CoeffVar(anno0, ecv.cv_prd_Tev)
      '--- 20150307LC (fine)
'20130610LC  'TEMPTEMP
'If y <> 0 Then
'y = y
'End If
      temp(i) = y
      i = i + 1
    End If
    If i >= n Then
      Exit For
    End If
  Next anno0
  TIscr2_LexPensRivaluta = i
End Function


Private Sub TIscr2_ListaCampi(ByRef dip As TIscr2, t0%, rsMat As ADODB.Recordset, ixdb&())
  Dim i&, j&
  Dim y#
  
  '--- Individua la posizione dei campi ----
  ReDim ixdb(RS_INT + (t0 - 1961) * 4)
  For i = 0 To UBound(ixdb, 1)
    ixdb(i) = -1
  Next i
  For j = 0 To rsMat.Fields.Count - 1
    i = -1
    '--- 20140317LC (inizio)
    Select Case LCase(rsMat(j).Name)
    Case "iddip": i = RS_mat
    Case "idaz": i = RS_anGruppo
    Case "categ": i = RS_anTit
    Case "c1": i = RS_anTC
    Case "c2": i = RS_anTS  '20140603LC
    Case "sesso": i = RS_anSex
    Case "dnasc": i = RS_anDNasc
    Case "n": i = RS_anNIngr
    Case "discrfondo": i = RS_anIscInD
    Case "dcessaz": i = RS_anCanFinD
    Case "hmf": i = RS_anAnzTot
    '--- 20150410LC (inizio)  'ex 20140706LC
    Case "hmfa": i = RS_anAnzA
    Case "hmfb": i = RS_anAnzB
    '--- 20140706LC (fine)
    Case "mandati": i = RS_anMandati
    '--- 20150410LC (fine)
    Case "pensione": i = RS_anPens
    Case "pensaus": i = RS_anPensAUS
    Case "smni"
      i = RS_IrpefNI
    Case "cmni"  '20150518LC
      i = RS_SoggNI
    Case "ivani", "om"  '20150518LC
      i = RS_IvaNI
    Case "inteni", "qm"  '20150612LC
      i = RS_IntNI
    Case "nsup": i = RS_anSupN
    Case "dnascfam1": i = RS_anSDNasc
    Case "sessofam1": i = RS_anSSex
    Case "codfam1": i = RS_anSCcod
    Case "revfam1": i = RS_anSRev
    Case "dnascfam2": i = RS_anSDNasc + 5
    Case "sessofam2": i = RS_anSSex + 5
    Case "codfam2": i = RS_anSCcod + 5
    Case "revfam2": i = RS_anSRev + 5
    Case "dnascfam3": i = RS_anSDNasc + 10
    Case "sessofam3": i = RS_anSSex + 10
    Case "codfam3": i = RS_anSCcod + 10
    Case "revfam3": i = RS_anSRev + 10
    Case "penstipo": i = RS_anCatPens
    Case "pensanno": i = RS_anPenADcr  '20140307LC
    Case Else
      y = Int(Val(Right(rsMat(j).Name, 4)))
      If y >= 1961 And y <= t0 Then
        y = y - 1961
        Select Case LCase(Left(rsMat(j).Name, Len(rsMat(j).Name) - 4))
        Case "sm"
          i = RS_IRPEF + y * 4
        Case "cm"
          i = RS_SOGG + y * 4
        Case "iva"
          i = RS_IVA + y * 4
        Case "int"
          i = RS_INT + y * 4
        End Select
      End If
    End Select
    '--- 20140317LC (fine)
    If i >= 0 Then
      ixdb(i) = j
    End If
  Next j
End Sub


Private Function TIscr2_Montante(ByRef dip As TIscr2, ByRef appl As TAppl, ByRef anno0 As Integer, _
                                 ByVal te As Integer, ByRef iop As Long, ByVal b23 As Boolean, _
                                 ByRef bFig As Boolean, ByRef fraz As Double, ByRef omV() As Double) As Double
'20120103LC
'20121008LC
'20121127LC (tolto omV
'20131123LC (bFig)
'20150322LC  anno+1 per ENAS se il calcolo avviene al 31/12
'20150512LC  omV
'20150518LC  ByVal b23
'20160124LC  appl, b23
  Dim anno%, i%, j%, t%, t1%           '20150322LC  'ex 20140111LC-1
  Dim ALR#, ALR70#, om#, xp#, y#, y1#  '20140111LC-26  'ex 20140111LC-1  'ex 20121009LC
  Dim SETTANTA As Integer  '20140404LC

  With dip
    ReDim omV(eom.om_tot)  '20150522LC
    '--- 20150322LC (inizio)
    anno = anno0 + 1
    '--- 20150322LC (fine)
    '--- 20140404LC (inizio)
    If OPZ_MOD_X75 > 1 Then
      SETTANTA = .cv4.Pveo98
      If SETTANTA < 70 Then
        SETTANTA = 70
      End If
    Else
      SETTANTA = 70
    End If
    '--- 20140404LC (fine)
    'ReDim omV(eom.om_tot)  '20121127LC (non serve)
    If iop >= 2 Then 'Al 30/6
      t = anno - 1
    Else 'AL 31/12
      t = anno
    End If
    om = 0#
    '--- 20140111LC-1 (inizio)
    xp = .xr + fraz  'spostato in alto
    t1 = IIf(te < 0, .t, te)
    '--- 20140310LC (inizio)  'riportare 20140111LC-26
    '20160124LC
    'b23 = True  '20150526LC (era False)
    b23 = IIf(Val(appl.parm(P_TipoElab)) = 1, False, True)
    ALR = 1#
    ALR70 = 1#
    '--- 20140310LC (fine)  'riportare 20140111LC-26
    '--- 20140111LC-1 (fine)
    '--- 20131122LC (inizio)
    'For i = 0 To eom.om_tot - 1
    For i = eom.om_min To eom.om_max
      If i <> eom.om_tot Then
    '--- 20131122LC (fine)
        j = TIscr2_Mont_Prd(i) 'IIf(i = 0, ecv.cv_Prd_Trmc0, ecv.cv_Prd_Trmc1)
        '20120103LC (inizio)
        If te < 0 Then
          y = .s(t).om_rcV(i) * appl.CoeffVar(t, j)
        Else
          '20120117LC
          'y = .s(te).om_rcV(i) * CoeffVar(t, j)
          y = .s(te).om_rcV(i) * appl.CoeffVar(te, j)
        End If
        '20120103LC (fine)
        '--- 20131123 (inizio)
        If i = eom.om_misto_fig Then
          If bFig = False Then
            y = 0#
          End If
        End If
        '--- 20131123 (inizio)
        If y <> 0 Then '20120117LC
          '20120117LC (inizio)
          If te < 0 Then
          Else
            y = y * (appl.CoeffVar(t, j) / appl.CoeffVar(te, j))
          End If
          '20120117LC (fine)
          If i > eom.om_2002 Or b23 = False Then '20111226LC
            omV(i) = y  '20150512LC  'ex 20121127LC (commentato)
            If i <> eom.om_misto_int Then  '20150512LC
              om = om + y
            Else
            End If
          End If
        End If
      End If
    Next i
    '---
    If iop = 3 Then  'Acconto al 30/6
      '20120117LC (inizio)
      '20121130LC TODO: eliminare voci che danno la pens. retr.
      If te < 0 Then
      ElseIf 1 = 1 Then  '20150504LC (ripristinato per ENAS)
        '--- 20140111LC-1 (inizio) rivalutazioni differenziate 4 e 6
        ''20121130LC TODO: rivalutazioni differenziate
        ''--- anno te ---
        'i = .s(te).ico
        'j = TIscr2_Mont_Prd(i) 'IIf(i = 0, ecv.cv_Prd_Trmc0, ecv.cv_Prd_Trmc1)
        ''--- saldo
        'y = .s(te).c_sogg1a + .s(te).c_sogg2a
        'If i = eom.om_2001 Then
        '  y = y + .s(te).c_sogg3a
        'End If
        'If i = eom.om_misto_obbl Then
        '  y = y + .s(te).c_int1a * .s(te).alr '20120521LC
        'End If
        ''--- incremento montante
        'If t > te + 1 Then
        '  y = y * (CoeffVar(t, j) / CoeffVar(te + 1, j))
        'End If
        'If i > eom.om_2002 Or b23 = False Then '20111226LC
        '  'omV(i) = omV(i) + y  '20121127LC (non serve, e comunque i � variabile)
        '  om = om + y
        'End If
        '--- saldo anno precedente ---
        i = .s(te).ico
        y = .s(te).c_sogg1a + .s(te).c_sogg2a
        If i = eom.om_2001 Then
          y = y + .s(te).c_sogg3a
        End If
        '--- 20150514LC (inizio)
        y1 = 0
        '--- 20150514LC (fine)
        '--- incremento montante
        If t > te + 1 Then
          j = TIscr2_Mont_Prd(i)
          y = y * (appl.CoeffVar(t, j) / appl.CoeffVar(te + 1, j))
          If y1 > 0# Then
            j = TIscr2_Mont_Prd(eom.om_misto_int)
            y1 = y1 * (appl.CoeffVar(t, j) / appl.CoeffVar(te + 1, j))
          End If
        End If
        '---
        If i > eom.om_2002 Or b23 = False Then '20111226LC
          om = om + y + y1
        End If
        '--- 20140111LC-1 (fine)
      End If
      '20120117LC (fine)
    End If
    TIscr2_Montante = om
  End With
End Function


Private Function TIscr2_Montante_AV(ByRef dip As TIscr2, ByRef CoeffVar#(), ByRef t%, ByVal te%, ByVal b23 As Boolean, wb0 As TWB, omV#()) As Double
'20150506LC
'20150512LC  omV()
'20150526LC  ByVal b23
  Dim i%, j%
  Dim y#, om
  
  With dip
    om = 0#
    ReDim omV(eom.om_tot)  '20150522LC
    b23 = True
    '--- Quota A
    If b23 = False Then
      i = eom.om_2001
      j = TIscr2_Mont_Prd(i)
      y = .s(t).om_rcV(i) * CoeffVar(t, j)
      om = om + y
      '--- Quota B
      i = eom.om_2002
      j = TIscr2_Mont_Prd(i)
      y = .s(t).om_rcV(i) * CoeffVar(t, j)
      om = om + y
    Else
    End If
    '--- Contributi volontari
    i = eom.om_art25
    j = TIscr2_Mont_Prd(i)
    y = wb0.vg2rcV * CoeffVar(t, j)
    om = om + y
    '--- Quota C
    If 1 = 2 Then  '20150526LC
      i = eom.om_misto_obbl
      j = TIscr2_Mont_Prd(i)
      y = .s(te).om_rcV(i) * CoeffVar(t, j) + .s(te).c_sogg1a * CoeffVar(t - 1, j) / CoeffVar(te, j)
      om = om + y
    End If
    '---
    TIscr2_Montante_AV = om
  End With
End Function


'20080811LC
Private Sub TIscr2_MovPop_0(ByRef dip As TIscr2, ByRef appl As TAppl, s1 As TIscr2_Stato)
  Dim dAnz As TIscr2_Anz '20111226LC
  
  With dip
    If .anno1 > .anno0 Then  'nuovi ingressi
      '.tau0 = .anno1 - .anno0
      .xan = .xan + .tau0 - 1
      .xr = .xr + .tau0 - 1
    Else
      '.tau0 = 0
    End If
  End With
End Sub


Public Sub TIscr2_MovPop_1(ByRef dip As TIscr2, ByRef appl As TAppl, ByRef glo As TGlobale, ByRef tb1() As Double, _
                           ByRef t&, ByRef s0 As TIscr2_Stato, ByRef s1 As TIscr2_Stato, ByRef iop&)
'20150521LC  tb1, iop
'20150525LC  p_dia
'20170312LC  iTC
  Dim bPrimoAnno As Boolean
  Dim iTC As Byte  '20170312LC
  Dim fact#, q#, redd#, y# '20150527LC
  Dim hi As Long   '20160323LC (rinominata)  '20150609LC
  Dim hx As Long   '20160323LC (rinominata)  '20150609LC
  Dim ti As Long   '20160323LC (long)  '20150609LC
  Dim xi As Long   '20160411LC (ripristinata, ex x5)
  '--- 20150612LC
  Dim ia&, iClone&
  Dim fraz0#, fraz1#, pop#
  Dim RC_FRAZ As Double  '20170727LC
  Dim sKey$
  Dim dia0 As TIscr2Aux
  Dim dia1 As TIscr2Aux
  Dim dia2 As TIscr2Aux
    
  'With dip
    '**************************
    'Inizializzazione del passo
    '**************************
    '***********************
    'Incremento di anzianit�
    '***********************
    Call TIscr2_MovPop1_dh(dip)
    '*************************************************
    'Completamento inizializzazione del passo iniziale
    '*************************************************
    If t = dip.tau0 Then
      Call TIscr2_MovPop_2(dip, appl, tb1, t, dip.wa(t))
    Else
      s1 = s0
      's1.Anz = s0.Anz
    End If
    '**************************************************
    'Completamento inizializzazione dei passi sucessivi
    'Movimento della popolazione
    '**************************************************
    bPrimoAnno = (t = dip.tau0) And (dip.anno1 > dip.anno0)
    If (t > dip.tau0) Or (bPrimoAnno = True) Then
      '--- 20150609LC (inizio)  ex 20150527LC
      If (dip.gruppo0 = 5 Or dip.gruppo0 = 6) And _
         (t - dip.tau0 > 0) And _
         (bPrimoAnno = False) And _
         (iop = 0) Then
        '-- 20160323LC (inizio)
        ti = dip.t - dip.annoUC - 1
        hi = Int(dip.s(dip.t).hs)
        xi = dip.t - Year(dip.DNasc) - 1  '20160411LC
        '---
        hx = hi
        If dip.gruppo0 = 5 Then
          If UCase(Left(appl.parm(P_neStat), 2)) = "SX" Then
            hx = xi - 15
          End If
        ElseIf dip.gruppo0 = 6 Then
          If UCase(Left(appl.parm(P_nvStat), 2)) = "VX" Then
            hx = xi - 15
          End If
        End If
        '---
        For iTC = 0 To MAX_TC  '20170312LC
          y = 0#
          redd = 0#
          q = 0#
          If ti < 1 Or ti > 30 Then
          ElseIf hx < 1 Or hx > 60 Then
          '--- 20180305LC (inizio)
          'ElseIf dip.t <= appl.t0 + 1 Then  'rip 20171117LC
          ElseIf dip.t < OPZ_RIATT_AMIN Then
          '--- 20180305LC (inizio)
          ElseIf dip.gruppo0 = 5 Then
            q = appl.Lne(ti, hx, dip.iQ, 1, iTC)  '20170312LC
            '--- 20160523LC (inizio)
            If OPZ_RND_QEA <> 0 Then
              y = Rnd(1)
              If q > y Then
                q = 1#
              Else
                q = 0#
              End If
            End If
            '--- 20160523LC (inizio)
            redd = appl.Lne(ti, hx, dip.iQ, 2, iTC)  '20170312LC
            y = dip.wa(t - 1).c0e_z
          Else
            q = appl.Lnv(ti, hx, dip.iQ, 1, iTC)  '20170312LC
            '--- 20160523LC (inizio)
            If OPZ_RND_QVA <> 0 Then
              y = Rnd(1)
              If q > y Then
                q = 1#
              Else
                q = 0#
              End If
            End If
            '--- 20160523LC (inizio)
            redd = appl.Lnv(ti, hx, dip.iQ, 2, iTC)  '20170312LC
            y = dip.wa(t - 1).c0v_z
          End If
          '-- 20160323LC (fine)
'If ti >= 2 And q > 0 Then
'If dip.t <= 2016 And q > 0 Then
'q = 0
'End If
          'If t - dip.tau0 <= 2 And y > AA0 Then
          '--- 20170805LC (inizio)
          If q > 0 And dip.RcKK > 0 Then
            q = 0#
          End If
          '--- 20170805LC (fine)
          If y > AA0 And q > AA0 Then
            '--- 20150612LC (inizio)
            dia1 = p_dia
            With dia1
              '--- 20160411LC (inizio)
              '--- 20170512LC (inizio) ripristinato Trn
              fact = appl.CoeffVar(appl.t0 + t, cv_prd_Trn) / appl.CoeffVar(appl.t0, cv_prd_Trn)
              'fact = appl.CoeffVar(appl.t0 + t, cv_prd_Tev) / appl.CoeffVar(appl.t0, cv_prd_Tev)  '20160609LC
              '--- 20170512LC (fine) ripristinato
              .n = dip.nRes * q
              dip.nRes = dip.nRes - .n
              .RcIt = dip.RcIt  '20170724LC
              .PensAUS = appl.t0 + t
              .smNI = ARRV(redd * fact)
              .ivaNI = .smNI
              '---
              '.annoUC = .annoUC
              .c1 = 0
              .c2 = 0
              .dCess = DATA_ZERO
              '.DIscr = .DIscr
              '.DNasc = .DNasc
              '.gr = .gr
              '.h = .h
              '.ha = .ha
              '.hb = .hb
              .iErr = 0
              '.mat = .mat
              .nMan = 1
              .nSup = 0
              '.PensAnno = .PensAnno
              '.Pensione = .Pensione
              '.PensTipo = .PensTipo
              '.sex = .sex
              '.sup = .sup
              .tit = 1
              '--- 20160411LC (fine)
            End With
            '---
            If p_diaColl Is Nothing Then
              Set p_diaColl = New Collection
            End If
            '---  20160411LC (inizio)
            'sKey = dip.gruppo0 & ";" & hi & ";" & ti & ";" & x5
            'sKey = sKey & ";" & dip.sesso
            'sKey = sKey & ";" & dip.Qualifica  '(commentato perch� la qualifica del riattivato � 1)
            'sKey = dip.t & ";" & dip.gruppo0 & ";" & dip.sesso & ";" & xi & ";" & hi
            'sKey = dip.t & ";" & dip.gruppo0 & ";" & dip.sesso & ";" & xi & ";" & hi & ";" & iTC  '20170312LC
            sKey = dip.t & ";" & dip.gruppo0 & ";" & dip.sesso & ";" & xi & ";" & hi & ";" & iTC & ";" & dip.RcIt  '20170724LC
            '---  20160411LC (fine)
            iClone = -1
            On Error Resume Next
            Call p_diaColl.Add(appl.nDipGen, sKey)
            If Err.Number <> 0 Then
              Err.Clear
              iClone = p_diaColl(sKey)
            End If
            On Error GoTo 0
            If iClone = -1 Then
              ReDim Preserve p_diaGen(appl.nDipGen)
              '--- 20160411LC (inizio)
              dia1.z_nRiatt = 1
              dia1.z_sKey = sKey
              '--- 20160411LC (fine)
              dia1.c1 = iTC  '20170312LC
              p_diaGen(appl.nDipGen) = dia1
              appl.nDipGen = appl.nDipGen + 1
            Else
              dia0 = p_diaGen(iClone)
              dia2 = dia0
              pop = dia0.n + dia1.n
              If pop > 0 Then
                fraz0 = dia0.n / pop
                fraz1 = dia1.n / pop
                With dia2
                  '--- 20160411LC (inizio)
                  .z_nRiatt = .z_nRiatt + 1
'If .z_nRiatt >= 3 Then
'.mat = .mat  '5563000 30239000 | 30272000
'End If
                  If .annoUC < dia1.annoUC Then
                    .annoUC = dia1.annoUC
                  End If
                  If .DIscr <> DATA_ZERO Then
                    .DIscr = Math.Abs(dia0.DIscr) * fraz0 + Math.Abs(dia1.DIscr) * fraz1
                  End If
                  If .DNasc <> DATA_ZERO Then
                    .DNasc = Math.Abs(dia0.DNasc) * fraz0 + Math.Abs(dia1.DNasc) * fraz1
                  End If
                  .h = Math.Abs(dia0.h) * fraz0 + Math.Abs(dia1.h) * fraz1
                  .ha = Math.Abs(dia0.ha) * fraz0 + Math.Abs(dia1.ha) * fraz1
                  .hb = Math.Abs(dia0.hb) * fraz0 + Math.Abs(dia1.hb) * fraz1
                  .n = pop
                  '---
                  .om0 = Math.Abs(dia0.om0) * fraz0 + Math.Abs(dia1.om0) * fraz1
                  .qm0 = Math.Abs(dia0.qm0) * fraz0 + Math.Abs(dia1.qm0) * fraz1
                  .cmNI = Math.Abs(dia0.cmNI) * fraz0 + Math.Abs(dia1.cmNI) * fraz1
                  For ia = 0 To UBound(.cm, 1)
                    .cm(ia) = Math.Abs(dia0.cm(ia)) * fraz0 + Math.Abs(dia1.cm(ia)) * fraz1
                  Next ia
                  .smNI = Math.Abs(dia0.smNI) * fraz0 + Math.Abs(dia1.smNI) * fraz1
                  For ia = 0 To UBound(.sm, 1)
                    .sm(ia) = Math.Abs(dia0.sm(ia)) * fraz0 + Math.Abs(dia1.sm(ia)) * fraz1
                  Next ia
                  '--- 20160411LC (fine)
                  p_diaGen(iClone) = dia2
                End With
              End If
            End If
            '--- 20150612LC (fine)
          End If
        Next iTC
        

      
      End If
          
      'Mod MDG - mi posiziono qui per assegnare l'eta della prima riga, subito dopo aver valorizzato i campi della struttura Dip
'      If (dip.wa(0).eta = 0) Then
'        dip.wa(0).eta = dip.sup(0).y
'      End If
'      If (dip.wa(0).anno = 0) Then
'        dip.wa(0).anno = appl.t0 - 1
'      End If
      '-------------------------------------------------------------
      
      '--- 20150609LC (fine)
      Call TIscr2_Aggiorna_Redd1(dip, appl, s0, s1, appl.CoeffVar(), appl.Lsm())
      Call TIscr2_Aggiorna_Stato(dip, s0, s1, appl.CoeffVar())
      Call TIscr2_Aggiorna_Contr1(dip, appl, s0, s1, appl.CoeffVar(), False, 0#, 0#, 0#, 0#, 0#)
      Call TIscr2_MovPop_4(dip, s0, s1, appl, tb1, t, dip.wa(t - 1), dip.wa(t))  '20140414LC
      Call TIscr2_Aggiorna_X(dip, s1, appl.parm(), appl.Lsm(), appl.CoeffRiv(), appl.CoeffVar())
    End If
    '************
    'Aggregazione
    '************
    Call TIscr2_MovPop_9(dip, appl, glo, s1, tb1, t, dip.wa(t), dip.wb(), dip.wc())
    '*************
    'Contribuzione
    '*************
    If (t > dip.tau0) Or bPrimoAnno Then
      Call TIscr2_MovPop_Contributi(dip, appl, tb1, t, dip.wa(t - 1), dip.wa(t), s0, s1, 1)  '20121114LC
    End If
  'End With
End Sub


Public Sub TIscr2_MovPop_2(ByRef dip As TIscr2, ByRef appl As TAppl, ByRef tb1() As Double, ByRef t&, ByRef wa1 As TWA)
'*************************************************
'Completamento inizializzazione del passo iniziale
'20080915LC
'20121113LC  tutta
'20150521LC  tb1
'*************************************************
  Dim i&, iwb&
  Dim peg#, pem#
  Dim nSup&, ii&
  Dim sup0 As TIscrSup2
  
  With dip
    If .anno1 > .anno0 Then 'Nuovo iscritto
      If .gruppo0 = 1 Then
        wa1.c0a_z = .n0.c0a_z  '20080918LC
      '--- 20140307LC (inizio)  'riportare 20140111LC-24
      ElseIf .gruppo0 = 2 Then
        wa1.c1ca_z = .n0.c1ca_z
        wa1.c1cs_z = .n0.c1cs_z
        wa1.c1ct_z = .n0.c1ct_z
        wa1.c1ca_z = .n0.c1cva_z
        wa1.c1cvo_z = .n0.c1cvo_z
        wa1.c1cva_z = .n0.c1cva_z
        wa1.c1cvp_z = .n0.c1cvp_z
      ElseIf .gruppo0 = 3 Then
        wa1.c1na_z = .n0.c1na_z
        wa1.c1ns_z = .n0.c1ns_z
        wa1.c1nt_z = .n0.c1nt_z
        wa1.c1na_z = .n0.c1nva_z
        wa1.c1nvo_z = .n0.c1nvo_z
        wa1.c1nva_z = .n0.c1nva_z
        wa1.c1nvp_z = .n0.c1nvp_z
      ElseIf .gruppo0 = 4 Then
        'If OPZ_INT_NO_SUPERST = 0 And (.s(.t).stato = ST_S) Then
        If OPZ_SUP_MODEL = 0 Then  '20140424LC
          nSup = .nSup
          sup0 = .sup(0)
          .nSup = 1
          ii = 0
          For i = 0 To nSup - 1
            If i > 0 Then
              .sup(0) = .sup(i)
            End If
            Call TIscr2_MovPop_Superstiti4_0(dip, appl, i)  '20140424LC
            Call TIscr2_MovPop_4b1(dip, appl, tb1, .s(.t), -TB_MPN_VO, 4, .sup(0).y + .sup(0).c1, 0, .pwDB, _
                                   .pwDB_fc, .pwDB_fa1, .zs, EFirr.Firr_None)  '20131202LC
            ii = ii + 1
          Next i
          .nSup = nSup
          .sup(0) = sup0
        Else
          '--- 20140424LC (inizio)
          'call MsgBox("TODO TIscr2_MovPop_2-9a", , .mat)
          'Stop
          ReDim appl.pfam4(OPZ_OMEGA)
          Call TIscr2_MovPop_Superstiti4_2(dip, appl, 1, .anno1 - .anno0, 1#, appl.pfam4)  '20140424LC
          Call TIscr2_MovPop_4b1(dip, appl, tb1, .s(.t), -TB_MPN_VO, 4, .sup(0).y + .sup(0).c1, 0, .pwDB, _
                                 .pwDB_fc, .pwDB_fa1, .zs, EFirr.Firr_None)  '20131202LC
          '--- 20140424LC (fine)
        End If
      '--- 20150504LC (inizio)
      ElseIf .gruppo00 = 6 Then
        wa1.c0v_z = .n0.c0v_z
        '--- 20160524LC (inizio) - per riattivazioni al 100% pu� succedere che c0v_z=0
        'If (OPZ_INT_NO_AE_AB_AI = 0 And .n0.c0v_z <> 0) Then
          'iwb = t * (1 + P_DUECENTO) + t
          iwb = 0
          .wb(iwb).vg2 = .n0.c0v_z
        'Else
        '  Call MsgBox("TODO TIscr2_MovPop_2-10a", , .mat)
        '  Stop
        'End If
        '--- 20160524LC (fine) - per riattivazioni al 100% pu� succedere che c0v_z=0
      '--- 20150504LC (fine)
      ElseIf .gruppo0 = 5 Then
        wa1.c0e_z = .n0.c0e_z
        '--- 20160524LC (inizio) - per riattivazioni al 100% pu� succedere che c0e_z=0
        'If (OPZ_INT_NO_AE_AB_AI = 0 And .n0.c0e_z <> 0) Then
          'iwb = t * (1 + P_DUECENTO) + t
          iwb = 0
          .wb(iwb).eg2 = .n0.c0e_z
        'Else
        '  Call MsgBox("TODO TIscr2_MovPop_2-10b", , .mat)
        '  Stop
        'End If
        '--- 20160524LC (fine) - per riattivazioni al 100% pu� succedere che c0e_z=0
      '--- 20140307LC (fine)  'riportare 20140111LC-24
      Else
        Call MsgBox("TODO TIscr2_MovPop_2-1a", , .mat)
        Stop
      End If
    Else  'If .anno1 <= .anno0 Then 'Iscritto esistente
      '--- 20150518LC (inizio) (spostato in alto)
      If .gruppo00 = 6 Then
        '--- 20160524LC (inizio) - per riattivazioni al 100% pu� succedere che c0v_z=0
        'If (OPZ_INT_NO_AE_AB_AI = 0 And .n0.c0v_z <> 0) Then
          iwb = t * (1 + P_DUECENTO) + t
          .wb(iwb).vg2 = .n0.c0v_z
        'Else
        '  Call MsgBox("TODO TIscr2_MovPop_2-10c", , .mat)
        '  Stop
        'End If
        '--- 20160524LC (fine) - per riattivazioni al 100% pu� succedere che c0v_z=0
      '--- 20150518LC (fine) (spostato in alto)
      ElseIf .gruppo0 = 1 Then
        If .n0.c0a_z = 0 Then
          Call MsgBox("TODO TIscr2_MovPop_2-2", , .mat)
          Stop
        Else
          wa1.c0a_z = .n0.c0a_z
        End If
      ElseIf .gruppo0 = 2 Then
        If .n0.c1cvo_z + .n0.c1cva_z + .n0.c1cvp_z + .n0.c1ca_z + .n0.c1cs_z + .n0.c1ct_z = 0 Then  '20131202LC
          Call MsgBox("TODO TIscr2_MovPop_2-3", , .mat)
          Stop
        ElseIf OPZ_INT_NO_PENSATT = 0 Then
          '--- 20131202LC (inizio)
          wa1.c1cvo_z = .n0.c1cvo_z
          wa1.c1cva_z = .n0.c1cva_z
          wa1.c1cvp_z = .n0.c1cvp_z
          '--- 20131202LC (fine)
          wa1.c1ca_z = .n0.c1ca_z
          wa1.c1cs_z = .n0.c1cs_z
          wa1.c1ct_z = .n0.c1ct_z
        Else
          Call MsgBox("TODO TIscr2_MovPop_2-4", , .mat)
          Stop
        End If
      ElseIf .gruppo0 = 3 Then
        If .n0.c1nvo_z + .n0.c1nva_z + .n0.c1nvp_z + .n0.c1na_z + .n0.c1ns_z + .n0.c1nt_z <> 0 Then  '20131202LC
          If .n0.c1b_z + .n0.c1i_z <> 0 Then
            Call MsgBox("TODO TIscr2_MovPop_2-5", , .mat)
            Stop
          Else
          '--- 20131202LC (inizio)
          wa1.c1nvo_z = .n0.c1nvo_z
          wa1.c1nva_z = .n0.c1nva_z
          wa1.c1nvp_z = .n0.c1nvp_z
          '--- 20131202LC (fine)
            wa1.c1na_z = .n0.c1na_z
            wa1.c1ns_z = .n0.c1ns_z
            wa1.c1nt_z = .n0.c1nt_z
          End If
        ElseIf .n0.c1b_z + .n0.c1i_z <> 0 Then
          If .n0.c1nvo_z + .n0.c1nva_z + .n0.c1nvp_z + .n0.c1na_z + .n0.c1ns_z + .n0.c1nt_z <> 0 Then  '20131202LC
            Call MsgBox("TODO TIscr2_MovPop_2-6", , .mat)
            Stop
          ElseIf OPZ_INT_NO_AE_AB_AI = 0 Then
            .wb(iwb).bg2 = .n0.c1b_z
            .wb(iwb).ig2 = .n0.c1i_z
          Else
            Call MsgBox("TODO TIscr2_MovPop_2-7", , .mat)
            Stop
          End If
        Else '.n0.c1na_z + .n0.c1nw_z + .n0.c1ns_z + .n0.c1nt_z + .n0.c1b_z + .n0.c1i_z = 0
          Call MsgBox("TODO TIscr2_MovPop_2-8", , .mat)
          Stop
        End If
      ElseIf .gruppo0 = 4 Then
        'If OPZ_INT_NO_SUPERST = 0 And (.s(.t).stato = ST_S) Then
        If OPZ_SUP_MODEL = 0 Then  '20140424LC
          '20080915LC (inizio)
          '--- Superstiti dei pensionati ---
          nSup = .nSup
          sup0 = .sup(0)
          .nSup = 1
          ii = 0
          For i = 0 To nSup - 1
            If i > 0 Then
              .sup(0) = .sup(i)
            End If
            Call TIscr2_MovPop_Superstiti4_0(dip, appl, i)  '20140424LC
            Call TIscr2_MovPop_4b1(dip, appl, tb1, .s(.t), -TB_MPN_VO, 4, .sup(0).y + .sup(0).c1, 0, .pwDB, _
                                   .pwDB_fc, .pwDB_fa1, .zs, EFirr.Firr_None)  '20131202LC
            ii = ii + 1
          Next i
          .nSup = nSup
          .sup(0) = sup0
          '20080915LC (fine)
        Else
          '--- 20140424LC (inizio)
          'call MsgBox("TODO TIscr2_MovPop_2-9", , .mat)
          'Stop
          ReDim appl.pfam4(OPZ_OMEGA)
          Call TIscr2_MovPop_Superstiti4_2(dip, appl, 1, 0, 1#, appl.pfam4) '20140424LC
          Call TIscr2_MovPop_4b1(dip, appl, tb1, .s(.t), -TB_MPN_VO, 4, .sup(0).y + .sup(0).c1, 0, .pwDB, _
                                 .pwDB_fc, .pwDB_fa1, .zs, EFirr.Firr_None)  '20131202LC
        '--- 20140424LC (fine)
        End If
      ElseIf .gruppo0 = 5 Then
        '--- 20160524LC (inizio) - per riattivazioni al 100% pu� succedere che c0e_z=0
        'If (OPZ_INT_NO_AE_AB_AI = 0 And .n0.c0e_z <> 0) Then
          iwb = t * (1 + P_DUECENTO) + t
          .wb(iwb).eg2 = .n0.c0e_z
        'Else
        '  Call MsgBox("TODO TIscr2_MovPop_2-10d", , .mat)
        '  Stop
        'End If
        '--- 20160524LC (fine) - per riattivazioni al 100% pu� succedere che c0e_z=0
      Else
        Call MsgBox("TODO TIscr2_MovPop_2-11", , .mat)
        Stop
      End If
    End If
  End With
End Sub


Public Sub TIscr2_MovPop_3(ByRef dip As TIscr2, ByRef appl As TAppl, wa0 As TWA, t1&, wb0 As TWB, iop&)
  '*********************
  'Test di pensionamento
  '*********************
  'iop: 0 attivi, 1: ex-attivi
  Dim te%
  Dim t&
  Dim h#, z#  '20130919LC
  Dim anz As TIscr2_Anz  '20111226LC
  Dim dAnz As TIscr2_Anz '20111226LC
  
  With dip
'If .t >= 2025 Then
'.t = .t
'End If
    t = .tau1
    'If iop = 0 And .gruppo00 <> 6 And .gruppo0 <> 5 Then
    If iop = 0 And .gruppo0 <> 6 And .gruppo0 <> 5 Then  '20150527LC
      If wa0.c0a_z <= AA0 Then
        Exit Sub
      End If
      '20111226LC (inizio)
      anz = .s(.t - 1).anz
      Call TIscr2_Anz_Dh(dAnz, dip, .t, appl.CoeffVar(), .coe0 * .rr_dhEff0)
      Call TIscr2_Anz_Add(anz, dAnz)
      '20111226LC (fine)
      '20111213LC
      'h = .s(.t).AnzTot + .coe0 * .rr_dhEff
      h = .s(.t).hs + .coe0 * .rr_dhEff0
If Math.Abs(anz.h - h) > AA0 Then
Call MsgBox("TODO-MovPop_3-a", , .mat)
Stop
End If
      If .tipoPensA = 0 Then
        Call TIscr2_MovPop_3a(dip, appl, t, wa0, h, False, .tipoPensA, .stipoPensA, etp.PDIR, .abbPensA)   '20160308LC  'ex 20120531LC
        If .tipoPensA <> 0 Then
          '--- 20130919LC (inizio)
          .atryPensA = .atryPensA + 1
          z = Rnd(1)
          If .atryPensA > OPZ_INT_PROPEN_ANNI Or z <= OPZ_INT_PROPEN_ALIQ Then
            .annoPensA = appl.t0 + .tau1
            '--- 20170724LC (inizio)
            If OPZ_RC_MODEL = 2 And .tipoPensA = TPENS_S Then
              .RcIt = 2
              .RcNa = .RcNa + wa0.c0a_z  '20170801LC
            End If
            '--- 20170724LC (fine)
          Else
            .tipoPensA = 0
            .stipoPensA = 0
            .annoPensA = 0
          End If
          '--- 20130919LC (fine)
        End If
      End If
    Else
      If wb0.vg2 + wb0.eg2 <= AA0 Then Exit Sub  '20150504LC
      '--- 20150504LC (inizio)
      If .gruppo00 = 6 Then 'bEA = 1 Then
        If 1 = 1 Then  'L'anzianit� del volontario coincide con quella dell'attivo
          anz = .s(.t - 1).anz
          Call TIscr2_Anz_Dh(dAnz, dip, .t, appl.CoeffVar(), .coe0 * .rr_dhEff0)
          Call TIscr2_Anz_Add(anz, dAnz)
          '20111226LC (fine)
          '20111213LC
          'h = .s(.t).AnzTot + .coe0 * .rr_dhEff
          h = .s(.t).hs + .coe0 * .rr_dhEff0
If Math.Abs(anz.h - h) > AA0 Then
Call MsgBox("TODO-MovPop_3-a", , .mat)
Stop
End If
        Else
          'Ricostruisce l'anzianit� al 31/12 dell'anno di calcolo
          '--- 20140307LC (inizio)  'riportare 20140111LC-24
          If .anno1 > .anno0 Then
            te = .t - (.t - .anno1 - t1)
            anz = .s(te - 0).anz
          Else
            anz = .s(.t - t).anz
          End If
          '--- 20140307LC (fine)  'riportare 20140111LC-24
        End If
      '--- 20150504LC (fine)
      '20111226LC (inizio)
      ElseIf .gruppo0 = 5 Then 'bEA = 1 Then
        'Ricostruisce l'anzianit� al 31/12 dell'anno di calcolo
        '--- 20140307LC (inizio)  'riportare 20140111LC-24
        If .anno1 > .anno0 Then
          te = .t - (.t - .anno1 - t1)
          anz = .s(te - 0).anz
        Else
          anz = .s(.t - t).anz
        End If
        '--- 20140307LC (fine)  'riportare 20140111LC-24
      ElseIf t1 > 0 Then 'bEA = 2
        'Ricostruisce l'anzianit� al 30/06 dell'anno di uscita
        '20120117LC (inizio)
        'te = .t - t + t1 - 1
        te = .t - (.t - .anno1 - t1)
        If .anno1 > .anno0 Then
          '20120117LC (inizio)
          anz = .s(te - 1).anz 'ex te
          '20120117LC (fine)
          If te >= .anno1 Then
            'Call TIscr2_Anz_Dh(dAnz, dip, .t, appl.CoeffVar(), 0.5)  'ex te
            Call TIscr2_Anz_Dh(dAnz, dip, te, appl.CoeffVar(), 0.5)  'ex te (penso sia stato ripristinato)  '20121226LC
            Call TIscr2_Anz_Add(anz, dAnz)
          Else
Call MsgBox("TODO TIscr_Aggiorna_MovPop_3 1", , .mat)
Stop
          End If
        Else
'20120130LC - TODOTODO: CHECK (3)
'call MsgBox("TODO")
'Stop
          anz = .s(te - 1).anz 'ex te
          'Call TIscr2_Anz_Dh(dAnz, dip, .t, appl.CoeffVar(), 0.5)  'ex te
          Call TIscr2_Anz_Dh(dAnz, dip, te, appl.CoeffVar(), 0.5)  'ex te (penso sia stato ripristinato)  '20121226LC
          Call TIscr2_Anz_Add(anz, dAnz)
        End If
        '20120117LC (fine)
      Else
        Call MsgBox("TODO-MovPop_3-b")
        Stop
      End If
      '20111226LC (fine)
      If .gruppo0 = 5 Then
        If t1 > 0 Then Exit Sub
        '--- 20140307LC (inizio)  'riportare 20140111LC-24
        If .anno1 > .anno0 Then
          h = TIscr2_MovPop1_hea(dip, t, 1#, 0#)
        Else
          h = TIscr2_MovPop1_hea(dip, t, 1#, 0#)
        End If
        '--- 20140307LC (fine)  'riportare 20140111LC-24
      Else
        h = TIscr2_MovPop1_hea(dip, t, t1, 0.5)
      End If
If Math.Abs(anz.h - h) > AA0 Then '20111230LC
  If 1 = 1 Then '20120109LC
    h = anz.h
  ElseIf Not (anz.h > 0 Or h > 0) Then
  ElseIf .anno1 > .anno0 Then
    If h < (.anno1 - .anno0) Then
    ElseIf Math.Abs(anz.h - h + (.anno1 - .anno0)) > AA0 Then
      Call MsgBox("TODO-MovPop_3-c1", , .mat)
      Stop
    End If
  Else
    Call MsgBox("TODO-MovPop_3-c2", , .mat)
    Stop
  End If
End If
      '--- 20150504LC (inizio)
      '20160411LC TODOTODO controllare che escludendo EA non cambia il risultato
      If wb0.tipoPensVA = 0 Or (.gruppo0 <> 6 And .gruppo0 <> 5) Then  '20150527LC
        '20160323LC (l'anzianit� dei volontari � come quella degli attivi)  'ex 20160308LC
        'Call TIscr2_MovPop_3a(dip, appl, t, wa0, h, True, wb0.tipoPensVA, wb0.stipoPensVA, etp.PDIR, wb0.abbPensVA)
        Call TIscr2_MovPop_3a(dip, appl, t, wa0, .s(.t).hs + .coe0 * .rr_dhEff0, True, wb0.tipoPensVA, _
            wb0.stipoPensVA, etp.PDIR, wb0.abbPensVA) '20160308LC
        If wb0.tipoPensVA <> 0 Then
          wb0.atryPensVA = wb0.atryPensVA + 1
          z = Rnd(1)
          If wb0.atryPensVA > OPZ_INT_PROPEN_ANNI Or z <= OPZ_INT_PROPEN_ALIQ Then
            wb0.annoPensVA = appl.t0 + .tau1
            '--- 20170724LC (inizio)
            If OPZ_RC_MODEL = 2 And wb0.annoPensVA = TPENS_S Then
              .RcIt = 2
              .RcNa = .RcNa + wb0.vg2  '20170801LC
            End If
            '--- 20170724LC (fine)
          Else
            wb0.tipoPensVA = 0
            wb0.stipoPensVA = 0
            wb0.annoPensVA = 0
          End If
        Else
          wb0.annoPensVA = 0
        End If
      End If
      '--- 20150504LC (fine)
      '20160411LC TODOTODO controllare che escludendo VA non cambia il risultato
      If wb0.tipoPensEA = 0 Or (.gruppo0 <> 5 And .gruppo0 <> 6) Then
        '20160308LC  'ex 20120531LC
        Call TIscr2_MovPop_3a(dip, appl, t, wa0, h, True, wb0.tipoPensEA, wb0.stipoPensEA, etp.PDIR, wb0.abbPensEA)
        If wb0.tipoPensEA <> 0 Then
          '--- 20130919LC (inizio)
          wb0.atryPensEA = wb0.atryPensEA + 1
          z = Rnd(1)
          If wb0.atryPensEA > OPZ_INT_PROPEN_ANNI Or z <= OPZ_INT_PROPEN_ALIQ Then
            wb0.annoPensEA = appl.t0 + .tau1
            '--- 20170801LC (inizio)  'ex 20170724LC
            'If OPZ_RC_MODEL = 2 And .gruppo00 = 5 And wb0.tipoPensEA = TPENS_S Then
            If OPZ_RC_MODEL = 2 And wb0.tipoPensEA = TPENS_S Then
              .RcIt = 2
              .RcNe = .RcNe + wb0.eg2  '20170801LC
            End If
            '--- 20170801LC (fine)  'ex 20170724LC
          Else
            wb0.tipoPensEA = 0
            wb0.stipoPensEA = 0
            wb0.annoPensEA = 0
          End If
          '--- 20130919LC (fine)
        '20080917LC (inizio)
        Else
          wb0.annoPensEA = 0
        End If
        '20080917LC (fine)
      End If
    End If
  End With
End Sub


Public Sub TIscr2_MovPop_3a(ByRef dip As TIscr2, ByRef appl As TAppl, ByRef t As Long, ByRef wa0 As TWA, _
                            ByRef h As Double, ByRef bEA As Boolean, ByRef TPens As Long, ByRef STPens As Long, _
                            ByRef bTipoPens As etp, ByRef abbPens As Double)
'*********************************************************************************************************************
'SCOPO
'  Test di pensionamento
'VERSIONI
'  20130321LC
'  20150307LC  riportata da prototipo offera ENAS
'  20160308LC  bTipoPens,abbPens
'*********************************************************************************************************************
  Dim bPensS As Boolean, bPensT As Boolean
  Dim Pvea35%, Pveo65%, Pveo30%, RcXmin%, TzXmin% '20120210LC
  Dim Pvep70 As Integer  '20160422LC
  Dim hp#, PRCTTLZ#, quota# '20120214LC
  
  With dip
If .t >= 2056 Then  '2035
  .t = .t
End If
    abbPens = 1#  '20160308LC
    hp = h + AA0
    If OPZ_MISTO_DEB_MAT = 1 And .bFalsoEA = True Then
      hp = hp + .t - .TipoElab
    End If
    quota = .xr + .coe0 + hp
    Pveo65 = .cv4.Pveo65
    Pveo30 = .cv4.Pveo30_orig
    Pvea35 = .cv4.Pvea35
    '--- 20160422LC (inizio)
    Pvep70 = .cv4.Pvep70
    If bEA = True And OPZ_SIL_PVEP75 <> 0 Then
      If Pvep70 < OPZ_SIL_PVEP75 Then
        Pvep70 = OPZ_SIL_PVEP75
      ElseIf Pvep70 < -OPZ_SIL_PVEP75 And dip.gruppo0 = 5 Then
        Pvep70 = -OPZ_SIL_PVEP75
      End If
    End If
    '--- 20160422LC (fine)
    RcXmin = IIf(.RcIt <> 1, .cv4.RcXmin, 99) '20170801LC  'ex 20170724LC (.RcIt = 0)
    TzXmin = .cv4.TzXmin
    PRCTTLZ = appl.CoeffVar(.t, ecv.cv_PrcPTattM + .sesso - 1) '20120214LC
    '--- Ha diritto alla pensione di vecchiaia ? ---
    If (hp >= Pveo30 And .xan + 1 >= Pveo65) And quota > .cv4.Pveo98 Then  '20130326LC
      TPens = TPENS_VO   '20150307LC
      STPens = TPens * 100 + 1
    '--- 20160323LC (inizio)
    ElseIf ((.xan + 1) >= Pvep70) Then  '20150422LC
      TPens = TPENS_VP
      STPens = TPens * 100 + 3
    '--- 20160323LC (fine)
    End If
    '--- Ha diritto alla pensione di vecchiaia anticipata ? ---
    '--- 20160308LC (inizio) 'ex 20160222LC (spostato in alto, ma ancora INIBITE)
    If 1 = 1 Then
      If TPens = 0 And wa0.c0a_z + wa0.c0v_z + wa0.c0e_z > AA0 Then  '20150504LC
        If (hp >= Pvea35 And .xan + 1 >= .cv4.Pvea58) And quota > OPZ_ART14_90 Then
          TPens = TPENS_VA
          STPens = TPens * 100 + 1
          '--- 20160308LC (inizio)
          If quota < .cv4.Pveo98 Then
            abbPens = (1 + OPZ_ART16_2) ^ (Int(quota) - .cv4.Pveo98)
          End If
          '--- 20160308LC (inizio)
        End If
      End If
    End If
    '--- 20160308LC (fine)
    '--- Ha diritto alla pensione contributiva o di totalizzazione ? ---
    If TPens = 0 Then
      '--- 20160222LC (inizio)
      If (hp >= Pveo30 And .xan + 1 >= Pveo65) Then
        'Matricola_Silente.xls 37527209 (in alternativa bloccare le p.contr. fino al 2023 in CV4)
      Else
      '--- 20160222LC (fine)
        '20170801LC  'ex 20170801LC  'ex 20170727LC
        'bPensS = .xan + 1 >= RcXmin And (bEA = True Or hp >= dmax(.cv4.RcHmin + 0, OPZ_EA_HMIN_MONT))
        'bPensS = (.xan + 1 >= RcXmin) And ((bEA = True And OPZ_RC_MODEL = 0) Or hp >= dmax(.cv4.RcHmin + 0, OPZ_EA_HMIN_MONT))
        'bPensS = (.xan + 1 >= RcXmin) And ((bEA = True And (OPZ_RC_MODEL = 0 Or OPZ_RC_MODEL = 2)) Or hp >= dmax(.cv4.RcHmin + 0, OPZ_EA_HMIN_MONT))  '20170801LC
        bPensS = .xan + 1 >= RcXmin And (bEA = True Or hp >= dmax(.cv4.RcHmin + 0, OPZ_EA_HMIN_MONT))  '20170802LC (ripristinato)
        bPensT = .xan + 1 >= TzXmin And hp >= dmax(.cv4.TzHmin + 0, OPZ_EA_HMIN_MONT) And bPensS = False
        '20160308LC
        'If (hp + .cv4.Pveo98 - Pveo65 >= Pveo30 And bEA = False) And ((.TipoElab = 0) Or .TipoElab > 1 And (.s(.t).bIsMisto = False Or OPZ_MOD_X75 = 0)) Then '20121022LC
        If 1 = 2 Then
          TPens = 0
        ElseIf bPensS = True Then
          If hp >= dmax(.cv4.RcHmin + 0, OPZ_EA_HMIN_MONT) Then  'mat: 30293000
            TPens = TPENS_S
            STPens = TPens * 100 + 1
          Else  'mat: 30272000, 30273000, 30275000, 30276000, 30282000
            TPens = -TPENS_S
            STPens = TPens * 100 + 2
          End If
        ElseIf bPensT = True Then
          TPens = TPENS_T
          STPens = TPens * 100 + 1
        End If
      End If
      '--- 20160308LC (inizio)
      If TPens > 0 Then
        If bTipoPens = PIND Then  'Art.23.3
          If hp < Pveo30 Then
            abbPens = (1 + OPZ_ART16_2) ^ (Int(hp) - Pveo30)
          End If
        Else
          If quota < .cv4.Pveo98 Then
            abbPens = (1 + OPZ_ART16_2) ^ (Int(quota) - .cv4.Pveo98)
          End If
        End If
      End If
      '--- 20160308LC (inizio)
    End If
  End With
End Sub


Public Sub TIscr2_MovPop_4(ByRef dip As TIscr2, ByRef s0 As TIscr2_Stato, ByRef s1 As TIscr2_Stato, ByRef appl As TAppl, _
                           ByRef tb1() As Double, ByRef t&, ByRef wa0 As TWA, ByRef wa1 As TWA)
'20080811LC
'20140414LC
'20150608LC  vg2sm2
  Dim bFig As Boolean   '20131123LC
  Dim ic As Byte  '20160414LC
  Dim t01 As Integer
  Dim te% '20120114LC
  Dim i&, ijk&, iwb&, iwbm&, iwc&, iwcm&, j&, t1&, t1a&, t1b&, x&, ZERO& '20121028LC
  Dim iQPD&  '20150604LC
  Dim iTS&, iQ& '20080908LC
  Dim h0#, h1#, fact#, fact0#, fact1#, fraz#, om#, pens#, pens1#, y#, yd#  '20130216LC
  Dim nMandati2#  '20150608LC
  Dim wb0 As TWB '20121113LC
  Dim RestContrT#
  Dim cvSogg0Min#, cvSogg1Al#, cvAss1Al#, cvMat0Min#, y0#  '20150609LC
  Dim cvSogg1MaxNS#  '20160414LC
  Dim cvSogg1MaxS#  '20160414LC
  Dim omV0#()  '20150512LC
  Dim omV1#()  '20150512LC
  
  
  ZERO = 0#
  With dip
    If appl.bProb_Superst = True Then  '20121109LC
      Call TIscr2_MovPop_Sup1b(dip, appl, tb1, 0, 0, 1)   '20080919LC
    End If
    iQ = .iQ + 1
    iTS = .iTS + 1
    '--- 20150609LC (inizio)
    iQPD = BT_QPD
    If .catPens = CAT_PENS_INA Then
      If OPZ_INT_NO_AE_AB_AI = 0 Then
        iQPD = BT_QBD
      End If
    ElseIf .catPens = CAT_PENS_INV Then
      If OPZ_INT_NO_AE_AB_AI = 0 Then
        iQPD = BT_QID
      End If
    End If
    '--- 20150609LC (fine)
    '****************
    'Montanti all'1/1
    '****************
    RestContrT = 0#
    ReDim omV0(eom.om_tot)  '20150522LC
    'ReDim omV1(eom.om_tot)
    If .gruppo0 = 1 Or .gruppo0 = -2 Or .gruppo0 = 5 Or .gruppo00 = 6 Then  '20150504LC  'ex 2012103LC (ottimizzazione)
      '--- 20150504LC (inizio)
      'If .gruppo00 = 6 Then
      If .gruppo0 = 6 Then  '20150527LC
        If 1 = 1 Then  'Come l'attivo
          'h0 = dip.s(.t).anz.h  '20131123LC
          h0 = dip.s(.t).anz.h + AA0  '20140310LC  'riportare 20140111LC-27 (non dava problemi, solo per coerenza)
          bFig = TIscr2_bFig(dip, h0)  '20131123LC
          '20160124LC  'ex 20150512LC  'ex 20131123LC  'ex 20121127LC
          om = TIscr2_Montante(dip, appl, .t - 1, -1, 1, False, bFig, 0#, omV1())
          RestContrT = wa0.c0a_z * om
          '--- 20150512LC (inizio)
          For i = 0 To UBound(omV0)  '20150522LC
            omV0(i) = omV0(i) + wa0.c0a_z * omV1(i)
          Next i
          '--- 20150512LC (fine)
        Else
          te = .t - (.t - .anno1 - t1)
          h0 = .s(te).anz.h + 1E-30  '20131123LC
          bFig = TIscr2_bFig(dip, h0)  '20131123LC
          iwbm = (t - 1) * (1 + P_DUECENTO) + 0
          '20160124LC  'ex 20150512LC  'ex 20131123LC  'ex 20121127LC
          om = TIscr2_Montante(dip, appl, .t - 1, te - 0, 1, False, bFig, 0#, omV1())
          RestContrT = .wb(iwbm).vg2 * om
          '--- 20150512LC (inizio)
          For i = 0 To UBound(omV0)  '20150522LC
            omV0(i) = omV0(i) + .wb(iwbm).vg2 * omV1(i)
          Next i
          '--- 20150512LC (fine)
        End If
      '--- 20150504LC (fine)
      '20120117LC (inizio)
      ElseIf .gruppo0 = 5 Then
        te = .t - (.t - .anno1 - t1)
        h0 = .s(te).anz.h + 1E-30  '20131123LC
        bFig = TIscr2_bFig(dip, h0)  '20131123LC
        '20160124LC  'ex 20150512LC  'ex 20131123LC  'ex 20121127LC
        om = TIscr2_Montante(dip, appl, .t - 1, te - 0, 1, False, bFig, 0#, omV1())
        RestContrT = .wb(iwbm).eg2 * om
        iwbm = (t - 1) * (1 + P_DUECENTO) + 0
        '--- 20150512LC (inizio)
        For i = 0 To UBound(omV0)  '20150522LC
          omV0(i) = omV0(i) + .wb(iwbm).eg2 * omV1(i)
        Next i
        '--- 20150512LC (fine)
      ElseIf .gruppo0 = 1 Then
        If wa0.c0a_z > AA0 Then
          'h0 = dip.s(.t).anz.h  '20131123LC
          h0 = dip.s(.t).anz.h + AA0  '20140310LC  'riportare 20140111LC-27 (non dava problemi, solo per coerenza)
          bFig = TIscr2_bFig(dip, h0)  '20131123LC
          '20160124LC  'ex 20150512LC  'ex 20131123LC  'ex 20121127LC
          om = TIscr2_Montante(dip, appl, .t - 1, -1, 1, False, bFig, 0#, omV1())
          RestContrT = wa0.c0a_z * om
          '--- 20150512LC (inizio)
          For i = 0 To UBound(omV0)  '20150522LC
            omV0(i) = omV0(i) + wa0.c0a_z * omV1(i)
          Next i
          '--- 20150512LC (fine)
        '--- 20160611LC (inizio)
        Else
          'If Abs(OPZ_INT_COMP) = COMP_ENAS Then
            y = wa0.c1cvo_z + wa0.c1cva_z + wa0.c1cvp_z + wa0.c1ca_z + wa0.c1cs_z + wa0.c1ct_z
            If y > 0# Then
              om = TIscr2_Montante(dip, appl, .t - 1, -1, 1, False, bFig, 0#, omV1())
              RestContrT = y * om  '20160612LC
              For i = 0 To UBound(omV0)
                If omV1(i) <> 0 Then
                  If i = eom.om_pens_att Or i = eom.om_misto_int Then
                    omV0(i) = omV0(i) + y * omV1(i)
                  Else
                    Call MsgBox("TODO-TIscr2_MovPop_4-3", , dip.mat)
                    Stop
                  End If
                End If
              Next i
            End If
          'End If
        '--- 20160611LC (fine)
        End If
        '--- 20121113LC (inizio)
        If OPZ_INT_NO_AE_AB_AI = 0 Then
          t1a = 1
          t1b = t - (.anno1 - .anno0) * 1 - 1
          iwbm = (t - 1) * (1 + P_DUECENTO) + t1a
          iwb = t * (1 + P_DUECENTO) + t1a
          For t1 = t1a To t1b 'TIscr2_MovPop_4-1
            'iwbm = (t - 1) * (1 + P_DUECENTO) + t1
            'iwb = t * (1 + P_DUECENTO) + t1
            '--- 20150504LC (inizio)
            If .wb(iwbm).vg2 > AA0 Then
              'h0 = dip.s(.t).anz.h + AA0
              'bFig = TIscr2_bFig(dip, h0)
              te = .t - (.t - .anno1 - t1)
              om = TIscr2_Montante_AV(dip, appl.CoeffVar(), .t, te, False, .wb(iwbm), omV1())  '20150512LC
              RestContrT = RestContrT + .wb(iwbm).vg2 * om
              '--- 20150512LC (inizio)
              For i = 0 To UBound(omV0)  '20150522LC
                If i <> eom.om_misto_int Then  '20160611LC
                  omV0(i) = omV0(i) + .wb(iwbm).vg2 * omV1(i)
                Else
                End If
              Next i
              '--- 20150512LC (fine)
            End If
            '--- 20150504LC (fine)
            If .wb(iwbm).eg2 > AA0 Then
              te = .t - (.t - .anno1 - t1)
              'h0 = .s(te).anz.h + 0#  '20131123LC
              h0 = .s(te - 1).anz.h + AA5 '20140310LC  'riportare 20140111LC-27
              bFig = TIscr2_bFig(dip, h0)  '20131123LC
              '20160124LC  'ex 20150512LC  'ex 20131123LC  'ex 20121127LC
              om = TIscr2_Montante(dip, appl, .t - 1, te - 1, 1, False, bFig, 0#, omV1())
              RestContrT = RestContrT + .wb(iwbm).eg2 * om
              '--- 20150512LC (inizio)
              For i = 0 To UBound(omV0)  '20150522LC
                If i <> eom.om_misto_int Then  '20160611LC
                  omV0(i) = omV0(i) + .wb(iwbm).eg2 * omV1(i)
                Else
                End If
              Next i
              '--- 20150512LC (fine)
            End If
            '---
            iwbm = iwbm + 1
            iwb = iwb + 1
          Next t1
        End If
        '--- 20121113LC (fine)
      End If
      '20120117LC (fine)
    '--- 20160611LC (inizio)
    ElseIf .gruppo0 = 2 Then
      y = wa0.c1cvo_z + wa0.c1cva_z + wa0.c1cvp_z + wa0.c1ca_z + wa0.c1cs_z + wa0.c1ct_z
      If y > 0# Then
        om = TIscr2_Montante(dip, appl, .t - 1, -1, 1, False, bFig, 0#, omV1())
        RestContrT = y * om
        For i = 0 To UBound(omV0)
          If omV1(i) <> 0 Then
            If i = eom.om_pens_att Or i = eom.om_misto_int Then
              omV0(i) = omV0(i) + y * omV1(i)
              omV0(i) = omV0(i) + wa0.c0a_z * omV1(i)
            Else
              Call MsgBox("TODO-TIscr2_MovPop_4-4", , dip.mat)
              Stop
            End If
          End If
        Next i
      End If
    '--- 20160611LC (fine)
    End If
'--- CONSOLIDAMENTO 1 (inizio) ---
    '--- 20160611LC (inizio)  spostato in basso per comprendere anche i pensionati attivi
    If .gruppo0 = 1 Or Abs(.gruppo0) = 2 Or .gruppo0 = 5 Or .gruppo00 = 6 Then  '20150504LC  'ex 2012103LC (ottimizzazione)
      If OPZ_TB = 1 Then
        ijk = appl.nSize2 * iTS + appl.nSize3 * iQ + appl.nSize4 * (t + appl.t0 - 1960) '20121114LC '2
      Else
        'ijk = .iSQ + appl.nSize4 * (t + appl.t0 - 1960) '20121114LC '2
        ijk = .iSQT '2
      End If
      tb1(ijk + TB_OMRC) = tb1(ijk + TB_OMRC) + RestContrT
      '--- 20150522LC (inizio)  (montanti parziali non salvati nel vecchio programma)
      For i = 0 To eom.om_misto_int
        tb1(ijk + TB_OMRC_0 + i) = tb1(ijk + TB_OMRC_0 + i) + omV0(i)
      Next i
    End If
    '--- 20150522LC (fine)
    '--- 20160611LC (fine)  spostato in basso per comprendere anche i pensionati attivi
'--- CONSOLIDAMENTO 1 (fine) ---
    '*******************************************
    'Test di pensionamento dell'attivo nell'anno
    '*******************************************
    .PminBase = appl.CoeffVar(.t, ecv.cv_PMin)  '20130609LC  'ex 20121230LC (spostata in alto, per aSol) '20120515LC (unificata)
    If wa0.c0a_z + wa0.c0v_z + wa0.c0e_z > AA0 Then  '--- '20150504LC  'ex 20121031LC (ottimizzato)
      Call TIscr2_PensioniUnitarie2(dip, s1, appl, appl.CoeffRiv(), appl.CoeffVar(), .t, .t, s1.pens1a1, s1.pens1a2)  '20150506LC
    End If
    '---
    If wa0.c0a_z > AA0 Then
      '--- 20121113LC (inizio)
      'iwbm = (t - 1) * (1 + P_DUECENTO) + t
      'Call TIscr2_MovPop_3(dip, appl, .wa(t - 1), t, .wb(iwbm), 0)
      Call TIscr2_MovPop_3(dip, appl, .wa(t - 1), t, wb0, 0)
      '--- 20121113LC (fine)
      If .annoPensA = 0 Then
        fraz = 1
      ElseIf .t = .annoPensA Then
        fraz = .coe0
      Else
        Call MsgBox("Situazione inattesa 1 in TIscr2_MovPop_4", vbExclamation, .mat)
        Stop
      End If
      '20150526LC (inizio)  'ex 20080907LC
      '***************************
      'Valori unitari per l'attivo
      '***************************
      Call TIscr2_PensDirInd1(dip, .wb(iwbm), .wb(iwb), appl, s1.pens1a1, s1.pens1a2, fraz, t, 0, etp.PIND, 0) '20150504LC  'ex 20120619LC
      Call TIscr2_PensDirInd1(dip, .wb(iwbm), .wb(iwb), appl, s1.pens1a1, s1.pens1a2, fraz, t, 0, etp.PDIR, 0)  '20150504LC  'ex 20120619LC
      '20150526LC (fine)
    End If
    '**************************
    'Probabilit� di transizione
    '**************************
    '--- 20130216LC (inizio)
    If OPZ_INT_ABBQ = 0 Then
      fact0 = TIscr2_AbbQ(appl.BDA, .t - .anno0 - 1, .xr, .iQ)
      fact1 = fact0
    Else
      fact0 = TIscr2_AbbQ(appl.BDA, .t - .anno0 - 1, Int(.xr), .iQ)
      fact1 = TIscr2_AbbQ(appl.BDA, .t - .anno0 - 1, Int(.xr + 1), .iQ)
    End If
    '--- 20130216LC (fine)
    '--- 20150609LC (inizio)
    .qav2 = 0#
    .qae2 = 0#
    .qab2 = 0#
    .qai2 = 0#
    .qaw2 = 0#
    '--- 20150609LC (fine)
    '--- 20121031LC (inizio) spostato in alto
    If (s1.stato = ST_PAT) Or (s1.stato = ST_PNA) Then
      .qpad = InterpLF(appl.LL1, iQPD, .sesso, .Qualifica, .xr, fact0, fact1)   '20130216LC
'If .qpad <> 0.01 Then
'fact = fact
'End If
      .qpnd = .qpad
If .qpnd <> 0 Then
y = y
End If
      .qad1 = .qpad  'InterpLF(appl.LL1, BT_QAD, .sesso, .Qualifica, .xr, fact0, fact1)  '20130216LC
      '--- 201050504LC (inizio)
      If wb0.annoPensVA = 0 Then
        .qvd1 = InterpLF(appl.LL1, BT_QAD, .sesso, .Qualifica, .xr, fact0, fact1)  '20130216LC
      Else
        .qvd1 = .qpad  'InterpLF(appl.LL1, BT_QED, .sesso, .Qualifica, .xr, fact0, fact1)  '20130216LC
      End If
      '--- 20150504LC (fine)
      '--- 20121221LC (inizio)
      If wb0.annoPensEA = 0 Then
        .qed1 = InterpLF(appl.LL1, BT_QED, .sesso, .Qualifica, .xr, fact0, fact1)  '20130216LC
      Else
        .qed1 = .qpad  'InterpLF(appl.LL1, BT_QED, .sesso, .Qualifica, .xr, fact0, fact1)  '20130216LC
      End If
      '--- 20121221LC (fine)
      .qbd = InterpLF(appl.LL1, BT_QBD, .sesso, .Qualifica, .xr, fact0, fact1)  '20130216LC
      .qid = InterpLF(appl.LL1, BT_QID, .sesso, .Qualifica, .xr, fact0, fact1)  '20130216LC
    Else
      '--- 20150505LC (inizio)
      If ((s1.stato = ST_A) And (.xan + 1 >= .cv4.Pveo98)) Or (wa0.c0a_z <= AA0) Or ((.annoPensA) > 0 And (.t >= .annoPensA)) Then
      ElseIf wa0.c0a_z <= AA0 Or (t = .tau0 And .anno1 > .anno0) Or .gruppo00 = 6 Then  '20150528LC
      Else
        '--- 20160323LC (inizio)
Dim te0%
Dim te1%
Dim h#
Dim h5#
        te1 = .t - 1
        te0 = .t - 1 - .cv4.Pind2
        If te0 < .annoAss Then
          te0 = .annoAss
        End If
        h5 = .s(te1).hs - .s(te0).hs + AA0
        h = .s(.t - 1).hs + 0.5
        If (h < .cv4.Pinv5 Or h5 < OPZ_ART19_3) Then
          .qav2 = 0
        ElseIf Left(LCase(appl.parm(P_LAV)), 2) = "qh" Then  '20150520LC  '20160324LC
          .qav2 = InterpL(appl.LL1, BT_QAV, .sesso, .Qualifica, s1.hs)
        Else
          .qav2 = InterpL(appl.LL1, BT_QAV, .sesso, .Qualifica, .xr)
        End If
        '--- 20160323LC (fine)
        '--- 20121031LC (inizio)
        If OPZ_RND_QAV <> 0 Then
          y = Rnd(1)
          If .qav2 > y Then
            .qav2 = 1#
          Else
            .qav2 = 0#
          End If
        End If
      End If
      '--- 20150505LC (fine)
      If ((s1.stato = ST_A) And (.xan + 1 >= .cv4.Pveo98)) Or (wa0.c0a_z <= AA0) Or ((.annoPensA) > 0 And (.t >= .annoPensA)) Then
      '--- 20121031LC (fine
'If s1.stato = ST_A And .xan + 1 >= .cv4.Pveo98 And .mat > 7590 Then
'.mat = .mat 'TOCHECK-1 '2430,5080,6320,7340,7590
'End If
      ElseIf wa0.c0a_z <= AA0 Or (t = .tau0 And .anno1 > .anno0) Or .gruppo00 = 5 Then  '20150518LC
      '20120110LC (inizio)
      '20120329LC
      'ElseIf OPZ_MISTO_REDD_PENS = 2 And bIsMisto  Then
      ElseIf OPZ_MISTO_DEB_MAT = 1 And s1.bIsMisto = True Then '20120515LC
        .qae2 = 1#
        .bFalsoEA = True
      '20120110LC (fine)
      Else
        '20091014LC (inizio)
        'If Abs(OPZ_INT_COMP) = COMP_ENAS Or Left(LCase(appl.parm(P_LAE)), 2) = "qh" Then  '20150414LC
        If Left(LCase(appl.parm(P_LAE)), 2) = "qh" Then  '20150520LC
          .qae2 = InterpL(appl.LL1, BT_QAE, .sesso, .Qualifica, s1.hs)
        Else
          .qae2 = InterpL(appl.LL1, BT_QAE, .sesso, .Qualifica, .xr)
        End If
        '20091014LC (fine)
'.qae2 = 0.01 / (1 - 0.01 * (.t - appl.t0 - 1))  '20131123LC
        '20081010LC (inizio)
        y = OPZ_EA_ABBQ_REDDMIN * appl.CoeffVar(.t, ecv.cv_Prd_Ts1a) / appl.CoeffVar(.anno0, ecv.cv_Prd_Ts1a)
        'If .sm(.anno1) >= y Then
        'If .s(.anno1).sm >= y And y > 0# Then  '20091014LC
        'If .s(.t).sm >= y And y > 0# Then  '20121128LC
        If .s(.t).sm >= y And y > 0# And .qae2 < 1 Then '20121226LC
          .qae2 = .qae2 * OPZ_EA_ABBQ_PERC
        End If
        '20081010LC (fine)
        '--- 20121031LC (inizio)
        If OPZ_RND_QAE <> 0 Then
          y = Rnd(1)
          If .qae2 > y Then
            .qae2 = 1#
          Else
            .qae2 = 0#
          End If
        End If
        '--- 20121031LC (fine)
      End If
      '---
      If wa0.c0a_z <= AA0 Or (t = .tau0 And .anno1 > .anno0) Then
      '20080918LC (inizio)
      ElseIf ((s1.stato = ST_A) And (.xan + 1 >= .cv4.Pveo98)) Or (wa0.c0a_z <= AA0) Or ((.annoPensA > 0) And (.t >= .annoPensA)) Then
        .qab2 = 0
        .qai2 = 0
        .qaw2 = 0
      '20080918LC (fine)
      Else
        .qab2 = InterpL(appl.LL1, BT_QAB, .sesso, .Qualifica, .xr)
        .qai2 = InterpL(appl.LL1, BT_QAI, .sesso, .Qualifica, .xr)
        .qaw2 = InterpL(appl.LL1, BT_QAW, .sesso, .Qualifica, .xr)
        '--- 20140422LC-02 (inizio)
        If .qaw2 > AA0 Then
          y = OPZ_EA_ABBQ_REDDMIN * appl.CoeffVar(.t, ecv.cv_Prd_Ts1a) / appl.CoeffVar(.anno0, ecv.cv_Prd_Ts1a)
          If .s(.t).sm >= y And y > 0# And .qaw2 < 1 Then '20121226LC
            .qaw2 = .qaw2 * OPZ_EA_ABBQ_PERC
          End If
        End If
        '--- 20140422LC-02 (fine)
        '--- 20121031LC (inizio)
        If OPZ_RND_QAB <> 0 Then
          y = Rnd(1)
          If .qab2 > y Then
            .qab2 = 1
          Else
            .qab2 = 0
          End If
        End If
        '---
        If OPZ_RND_QAI <> 0 Then
          y = Rnd(1)
          If .qai2 > y Then
            .qai2 = 1
          Else
            .qai2 = 0
          End If
        End If
        '---
        If OPZ_RND_QAW <> 0 Then
          y = Rnd(1)
          If .qaw2 > y Then
            .qaw2 = 1
          Else
            .qaw2 = 0
          End If
        End If
        '--- 20121031LC (fine)
      End If
      '---
      If (t = .tau0 And .anno1 > .anno0) Then
        .qad1 = 0#
        .qvd1 = 0#  '20150504LC
        .qed1 = 0#
        .qbd = 0#
        .qid = 0#
        .qpad = 0#
        .qpnd = 0#
      Else
        '20120506LC (inizio)
        .qad1 = InterpLF(appl.LL1, BT_QAD, .sesso, .Qualifica, .xr, fact0, fact1)  '20130216LC
        .qvd1 = .qad1                                                              '20150505LC
        .qed1 = InterpLF(appl.LL1, BT_QED, .sesso, .Qualifica, .xr, fact0, fact1)  '20130216LC
        .qbd = InterpLF(appl.LL1, BT_QBD, .sesso, .Qualifica, .xr, fact0, fact1)   '20130216LC
        .qid = InterpLF(appl.LL1, BT_QID, .sesso, .Qualifica, .xr, fact0, fact1)   '20130216LC
        .qpad = InterpLF(appl.LL1, iQPD, .sesso, .Qualifica, .xr, fact0, fact1)  '20130216LC
        .qpnd = .qpad
        '20120506LC (fine)
        '20120109LC (fine)
        '--- 20121031LC (inizio)
        If OPZ_RND_QAD <> 0 Then
          y = Rnd(1)
          If .qad1 > y Then
            .qad1 = 1
          Else
            .qad1 = 0
          End If
        End If
        '--- 20150504LC (inizio)
        If OPZ_RND_QAD <> 0 Then
          y = Rnd(1)
          If .qvd1 > y Then
            .qvd1 = 1
          Else
            .qvd1 = 0
          End If
        End If
        '--- 20150504LC (fine)
        If OPZ_RND_QAD <> 0 Then
          y = Rnd(1)
          If .qed1 > y Then
            .qed1 = 1
          Else
            .qed1 = 0
          End If
        End If
        '---
        If OPZ_RND_QBD <> 0 Then
          y = Rnd(1)
          If .qbd > y Then
            .qbd = 1
          Else
            .qbd = 0
          End If
        End If
        '---
        If OPZ_RND_QID <> 0 Then
          y = Rnd(1)
          If .qid > y Then
            .qid = 1
          Else
            .qid = 0
          End If
        End If
        '---
        If OPZ_RND_QPD <> 0 Then
          y = Rnd(1)
          If .qpad > y Then
            .qpad = 1
          Else
            .qpad = 0
          End If
          If .qpnd > y Then
            .qpnd = 1
          Else
            .qpnd = 0
          End If
        End If
        '--- 20121031LC (fine)
        '20081012LC (inizio)
        If .qad1 > 1 Then
          .qad1 = 1
        End If
        yd = .qae2 + .qab2 + .qai2 + .qaw2
        If yd > 1 - .qad1 Then
'call MsgBox("TODO-MovPop_4-1a", , .mat)
'Stop
          '20121031LC (inizio) 'TODOTODOTODO
          If .qae2 > 1 - .qad1 Then
            .qae2 = 1 - .qad1
            .qab2 = 0
            .qai2 = 0
            .qaw2 = 0
          ElseIf .qab2 > 1 - .qad1 Then
            .qae2 = 0
            .qab2 = 1 - .qad1
            .qai2 = 0
            .qaw2 = 0
          ElseIf .qai2 > 1 - .qad1 Then
            .qae2 = 0
            .qab2 = 0
            .qai2 = 1 - .qad1
            .qaw2 = 0
          ElseIf .qaw2 > 1 - .qad1 Then
            .qae2 = 0
            .qab2 = 0
            .qai2 = 0
            .qaw2 = 1 - .qad1
          Else
          '20120110LC (fine)
            yd = (1 - .qad1) / yd
            .qae2 = .qae2 * yd
            .qab2 = .qab2 * yd
            .qai2 = .qai2 * yd
            .qaw2 = .qaw2 * yd
          End If
          '20121031LC (fine) 'TODOTODOTODO
        End If
        If .qvd1 > 1 Then  '20150504LC
Call MsgBox("TODO-MovPop_4-1b", , .mat)
Stop
          .qvd1 = 1
        End If
        If .qed1 > 1 Then
Call MsgBox("TODO-MovPop_4-1c", , .mat)
Stop
          .qed1 = 1
        End If
        '20081012LC (fine)
      End If
    End If
    '########
    'MovPop_5
    '########
Dim FRAZ_PC#, FRAZ_PNA#, FRAZ_PNV#
    '.z1(t - 1).z.ysg = .z1(t).z.ysg
    '--- 20121205LC (inizio)
    If 1 = 2 Then  '20121207LC
      'For t1 = 0 To t - (.anno1 - .anno0) * 1 - 1  'TIscr2_MovPop_4-2
        'iwcm = ((t - 1) * (1 + P_DUECENTO) + t1) * TWC_SIZE
        'iwc = (t * (1 + P_DUECENTO) + t1) * TWC_SIZE
        iwcm = ((t - 1) * (1 + P_DUECENTO) + (t - 1)) * TWC_SIZE
        iwc = (t * (1 + P_DUECENTO) + t) * TWC_SIZE
        For j = 0 To 6
          '.wc(iwcm + j) = .wc(iwc + j)
          .wc(iwc + j) = .wc(iwcm + j)
        Next j
      'Next t1
    End If
    '--- 20121205LC (fine)
    If 1 = 1 Then '20121209LC
      wa0.sj0_z = wa1.sj0_z
      wa0.sj1_z = wa1.sj1_z
      wa0.sj2_z = wa1.sj2_z
      wa0.sj3_z = wa1.sj3_z
      wa0.sj4_z = wa1.sj4_z
      wa0.sj5_z = wa1.sj5_z
      wa0.sj6_z = wa1.sj6_z
      wa0.sj7_z = wa1.sj7_z
      wa0.sj8_z = wa1.sj8_z
    End If
    If wa1.sd_z > AA0 Then '20121204LC
      wa0.sd_z = wa1.sd_z
    End If
    
    'Mod MDG
'    wa1.pensTot = wa1.s1_zp
'    wa1.numMedPens = wa1.s1_zm
'    wa0.pensTot = wa1.pensTot
'    wa0.numMedPens = wa1.numMedPens
'    wa1.eta = wa1.eta + 1
'    wa0.eta = wa0.eta + 1
'    wa1.anno = wa1.anno + 1
'    wa0.anno = wa0.anno + 1
    
    '20080918LC (inizio)
    If (t = .tau0) And (.anno1 > .anno0) Then 'primo anno Nuovo iscritto
    Else
      wa1 = wa0
      If appl.bProb_EA_IV_IB = True Then '20121113LC
        iwbm = (t - 1) * (1 + P_DUECENTO)
        iwb = t * (1 + P_DUECENTO)
        For t1 = 0 To t - (.anno1 - .anno0) * 1 - 1  'TIscr2_MovPop_4-3
          'iwbm = (t - 1) * (1 + P_DUECENTO) + t1
          'iwb = t * (1 + P_DUECENTO) + t1
          .wb(iwb) = .wb(iwbm)
          iwbm = iwbm + 1
          iwb = iwb + 1
        Next t1
      End If
    End If
    '20080918LC (fine)
    '**********************
    'Usciti per altre cause
    '**********************
    If wa0.c0a_z > 0 Then
      yd = wa0.c0a_z * .qaw2 * fraz
      If yd > AA0 Then
        wa1.c0a_z = wa1.c0a_z - yd
        wa1.w1b_z = wa1.w1b_z + yd
      End If
    End If
    '***********************
    'Pensionati di inabilit�
    '***********************
    wa1.c1b_z = 0
    If t > 0 And wa0.c0a_z > 0 And .qab2 > AA0 Then
      iwb = .tau1 * (1 + P_DUECENTO) + (.tau1 - (.anno1 - .anno0) * 1)
      Call TIscr2_MovPop_4a(dip, appl, tb1, .wa(t - 1), .wa(t), t, t, .wb(iwb), .wb(iwb), fraz, 5)  '20150504LC
    End If
    '************************
    'Pensionati di invalidit�
    '************************
    wa1.c1i_z = 0
    If t > 0 And wa0.c0a_z > 0 And .qai2 > AA0 Then
      iwb = .tau1 * (1 + P_DUECENTO) + (.tau1 - (.anno1 - .anno0) * 1)
      Call TIscr2_MovPop_4a(dip, appl, tb1, .wa(t - 1), .wa(t), t, t, .wb(iwb), .wb(iwb), fraz, 6) '20150504LC
    End If
    '--- 20150506LC (inizio)
    '*********
    'Volontari
    '*********
    wa1.c0v_z = 0#
    If appl.bProb_EA_IV_IB = True Then
      '--- 20150526LC (inizio)
      '--- 20160414LC (inizio)  'ex 20150526LC
      ic = 0
      If dip.gruppo00 = 6 Then
        t01 = .t + 0
      Else
        t01 = .t + 1
      End If
      cvSogg0Min = appl.CoeffVar(t01, ecv.cv_Sogg0Min + ic)
      cvSogg1MaxNS = appl.CoeffVar(t01, ecv.cv_Sogg1Max + ic)
      If OPZ_MASSIMALE_2011 = 1 And t01 > 2011 Then
        cvSogg1MaxS = appl.CoeffVar(2011, ecv.cv_Sogg1Max + ic)
      Else
        cvSogg1MaxS = cvSogg1MaxNS
      End If
      cvSogg1Al = appl.CoeffVar(t01, ecv.cv_Sogg1Al + ic)
      cvAss1Al = appl.CoeffVar(t01, ecv.cv_Ass1Al + ic)
      cvMat0Min = appl.CoeffVar(t01, ecv.cv_Mat0Min + ic) '20150609LC
      '--- 20160414LC (fine)  'ex 20150526LC
      j = TIscr2_Mont_Prd(eom.om_misto_obbl)
      t1a = IIf(.gruppo00 = 6, 0, 1)
      t1b = IIf(.gruppo00 = 6, 0, t - (.anno1 - .anno0) * 1)
      iwbm = (t - 1) * (1 + P_DUECENTO) + t1b
      iwb = t * (1 + P_DUECENTO) + t1b
      For t1 = t1b To t1a Step -1
        If t1 = t1b And t > 0 And wa0.c0a_z > 0 Then
          '***********************************
          'Costruzione della nuova generazione
          '***********************************
          .wb(iwb).vg2 = 0
          If .qav2 > AA0 Then
            yd = wa0.c0a_z * .qav2 * fraz
            '--- 20160222LC (inizio) servono 3 redditi anche non consecutivi
            If yd > AA0 Then
              'dip.mat=2401520 ha 3 redditi non consecutivi
              'dip.mat = 30292998 - dip.s(dip.t - 2) out of range  '20160128LC
              y = 0
              i = 0
              If dip.t - dip.annoAss + 1 >= 3 Then
                For te = dip.t To dip.annoAss Step -1
                  If dip.s(te).sm > AA0 Then
                    y = y + dip.s(te).sm2s  '20160414LC
                    i = i + 1
                    If i = 3 Then Exit For
                  End If
                Next te
                If i = 3 Then
                  y = y / i
                Else
                  y = 0
                End If
              End If
              If y <= AA0 Then
                yd = 0
              End If
            End If
            '---
            If yd > AA0 Then
              '--- 20160414LC (inizio)  'ex 20150506LC
              With .wb(iwb)
                .vg2 = yd
                '.vg2sm = ARRV((dip.s(dip.t).sm + dip.s(dip.t - 1).sm + dip.s(dip.t - 2).sm) / 3#)
                .vg2sm = y
                nMandati2 = 1#
                .vg2sm2ns = dmin(.vg2sm, cvSogg1MaxNS * nMandati2)
                .vg2sm2s = dmin(.vg2sm, cvSogg1MaxS * nMandati2)
                y0 = .vg2sm2ns
                If y0 > cvSogg1MaxNS * nMandati2 Then
                  y0 = cvSogg1MaxNS * nMandati2
                End If
                If cvSogg1Al + cvAss1Al <= AA0 Then  '20150511LC
                Else
                  y0 = ARRV(y0 * (cvSogg1Al + cvAss1Al))
                  If y0 < cvSogg0Min * nMandati2 Then
                    y0 = ARRV(cvSogg0Min * nMandati2)
                    '.vg2sm2ns = ARRV(cvSogg0Min * nMandati2 / (cvSogg1Al + cvAss1Al))  (commentato)
                  End If
                End If
                .vg2cSogg = ARRV(y0 * cvSogg1Al / (cvSogg1Al + cvAss1Al))
                .vg2cAss = ARRV(y0 - .vg2cSogg)
                .vg2cMat = cvMat0Min  '20150609LC
                .vg2rcV = dip.om_rcV(eom.om_misto_obbl)
                .vg2rcV = .vg2rcV + (s1.c_sogg1a + .vg2cSogg) / appl.CoeffVar(dip.t + 1, j)
                Call TIscr2_PensioniUnitarie2(dip, s1, appl, appl.CoeffRiv(), appl.CoeffVar(), dip.t, dip.t, .vg2p1, .vg2p2)  '20150506LC
              End With
              '--- 20160414LC (fine)  'ex 20150506LC
              wa1.c0a_z = wa1.c0a_z - yd
              '--- 20160609LC (inizio)  'FIRR  'adattato da 41I 20150118LC
              Call TIscr2_MovPop_Firr(dip, appl, yd, EFirr.Firr_V)
              '--- 20160609LC (fine)  'FIRR  'adattato da 41I 20150118LC
            End If
            '--- 20160222LC (fine)
          End If
        ElseIf .wb(iwbm).vg2 > AA0 Then
          '****************************************
          'Smontamento delle generazioni precedenti
          '****************************************
          iwbm = (t - 1) * (1 + P_DUECENTO) + t1
          '--- 20150608LC (inizio)  ex 20150506LC
          With .wb(iwb)
            '--- 20160414LC (inizio)
            .vg2sm = dip.wb(iwbm).vg2sm
            nMandati2 = 1#
            .vg2sm2ns = dmin(.vg2sm, cvSogg1MaxNS * nMandati2)
            .vg2sm2s = dmin(.vg2sm, cvSogg1MaxS * nMandati2)
            y0 = .vg2sm2ns
            If y0 > cvSogg1MaxNS * nMandati2 Then
              y0 = cvSogg1MaxNS * nMandati2
            End If
            If cvSogg1Al + cvAss1Al <= AA0 Then  '20150511LC
            Else
              y0 = ARRV(y0 * (cvSogg1Al + cvAss1Al))
              If y0 < cvSogg0Min * nMandati2 Then
                y0 = ARRV(cvSogg0Min * nMandati2)
                '.vg2sm2ns = ARRV(cvSogg0Min * nMandati2 / (cvSogg1Al + cvAss1Al))  (commentato)
              End If
            End If
            .vg2cSogg = ARRV(y0 * cvSogg1Al / (cvSogg1Al + cvAss1Al))
            .vg2cAss = ARRV(y0 - .vg2cSogg)
            .vg2cMat = cvMat0Min  '20150609LC
            '--- 20160414LC (fine)
            '--- 20150526LC (inizio)
            .vg2rcV = dip.wb(iwbm).vg2rcV
            If dip.gruppo00 = 6 Then
              .vg2rcV = .vg2rcV + .vg2cSogg / appl.CoeffVar(dip.t + 0, j)
            Else
              .vg2rcV = .vg2rcV + .vg2cSogg / appl.CoeffVar(dip.t + 1, j)
            End If
            '--- 20150526LC (fine)
            Call TIscr2_PensioniUnitarie2(dip, s1, appl, appl.CoeffRiv(), appl.CoeffVar(), dip.t, dip.t - t1b + t1, .vg2p1, .vg2p2) '20150506LC
          End With
          '--- 20150608LC (fine)
          '--- Test di pensionamento del volontario nell'anno ---
          Call TIscr2_MovPop_3(dip, appl, .wa(t - 1), t1, .wb(iwbm), 1)
          If .wb(iwbm).annoPensVA = 0 Then  '20140111LC-12
            fraz = 1#
          ElseIf .t = .wb(iwbm).annoPensVA Then  '20140111LC-12
            fraz = .coe0
          Else
'call MsgBox("Situazione inattesa 2 in TIscr2_MovPop_4", vbExclamation, .mat)
'Stop
          End If
          '20080917LC (fine) - spostato
          '************************************************************
          'Crea un nuovo pensionato indiretto da contribente volontario
          '************************************************************
          Call TIscr2_MovPop_4a(dip, appl, tb1, .wa(t - 1), .wa(t), t, t1, .wb(iwbm), .wb(iwb), fraz, -3) '20150504LC  '229,85
          '***********************************************************
          'Crea un nuovo pensionato diretto da contribuente volontario
          '***********************************************************
          If .wb(iwbm).annoPensVA = .t Then  '20140111LC-12  'ex 20080914LC
            Call TIscr2_MovPop_4a(dip, appl, tb1, .wa(t - 1), .wa(t), t, t1, .wb(iwbm), .wb(iwb), fraz, -4) '20150504LC  '90,82!!!
          End If
        End If
        wa1.c0v_z = wa1.c0v_z + .wb(iwb).vg2
        iwbm = iwbm - 1
        iwb = iwb - 1
      Next t1
    End If
    '--- 20150506LC (fine)
    '*******************
    'Ex attivi / silenti
    '*******************
    wa1.c0e_z = 0#
    If appl.bProb_EA_IV_IB = True Then '20121113LC
      '20081015LC
      'OTTIMIZZABILE: 317,11 rec/sec  'TIscr2_MovPop_4-4
      t1a = IIf(.gruppo0 = 5, 0, 1)
      t1b = IIf(.gruppo0 = 5, 0, t - (.anno1 - .anno0) * 1)
      iwbm = (t - 1) * (1 + P_DUECENTO) + t1b
      iwb = t * (1 + P_DUECENTO) + t1b
      For t1 = t1b To t1a Step -1  'TIscr2_MovPop_4-4
        'If t1 = t - (.anno1 - .anno0) * 1 And t > 0 And wa0.c0a_z > 0 Then
        If t1 = t1b And t > 0 And wa0.c0a_z > 0 Then
          '***********************************
          'Costruzione della nuova generazione
          '***********************************
          .wb(iwb).eg2 = 0
          If .qae2 > AA0 Then
            yd = wa0.c0a_z * .qae2 * fraz
            .wb(iwb).eg2 = yd
            wa1.c0a_z = wa1.c0a_z - yd
            '--- 20160609LC (inizio)  'FIRR  'adattato da 41I 20150118LC
            If yd > AA0 Then
              Call TIscr2_MovPop_Firr(dip, appl, yd, EFirr.Firr_E)
            End If
            '--- 20160609LC (fine)  'FIRR  'adattato da 41I 20150118LC
          End If
        ElseIf .wb(iwbm).eg2 > AA0 Then
          '****************************************
          'Smontamento delle generazioni precedenti
          '****************************************
          '20080917LC (inizio) - spostato
          '--- Test di pensionamento dell'ex-attivo nell'anno ---
          iwbm = (t - 1) * (1 + P_DUECENTO) + t1
          Call TIscr2_MovPop_3(dip, appl, .wa(t - 1), t1, .wb(iwbm), 1) '125,21!!!
          If .wb(iwbm).annoPensEA = 0 Then  '20140111LC-12
            fraz = 1#
          ElseIf .t = .wb(iwbm).annoPensEA Then  '20140111LC-12
            fraz = .coe0
          Else
'call MsgBox("Situazione inattesa 2 in TIscr2_MovPop_4", vbExclamation, .mat)
'Stop
          End If
          '20080917LC (fine) - spostato
          '*********************************************************
          'Crea un nuovo pensionato indiretto da ex-attivo / silente
          '*********************************************************
          Call TIscr2_MovPop_4a(dip, appl, tb1, .wa(t - 1), .wa(t), t, t1, .wb(iwbm), .wb(iwb), fraz, 3) '20150504LC  '229,85
          '*******************************************************
          'Crea un nuovo pensionato diretto da ex-attivo / silente
          '*******************************************************
          If .wb(iwbm).annoPensEA = .t Then  '20140111LC-12  'ex 20080914LC
            Call TIscr2_MovPop_4a(dip, appl, tb1, .wa(t - 1), .wa(t), t, t1, .wb(iwbm), .wb(iwb), fraz, 4) '20150504LC  '90,82!!!
          End If
        End If
        wa1.c0e_z = wa1.c0e_z + .wb(iwb).eg2
        iwbm = iwbm - 1
        iwb = iwb - 1
      Next t1
    End If
    '******
    'Attivi
    '******
    If wa1.c0a_z > AA0 Then
      If .annoPensA = 0 Then
        fraz = 1
      ElseIf .t = .annoPensA Then
        fraz = .coe0
      Else
        Call MsgBox("Situazione inattesa 1 in TIscr2_MovPop_4", vbExclamation, .mat)
        Stop
      End If
      '********************************************
      'Crea un nuovo pensionato indiretto da attivo
      '********************************************
      '20121113LC
      'Call TIscr2_MovPop_4a(dip, appl, .wa(t - 1), .wa(t), t, .wb(0), .wb(0), fraz, 1)
      Call TIscr2_MovPop_4a(dip, appl, tb1, .wa(t - 1), .wa(t), t, t, wb0, wb0, fraz, 1) '20150504LC
      '******************************************
      'Crea un nuovo pensionato diretto da attivo
      '******************************************
      If .annoPensA = .t Then '20080915LC
        '20121113LC
        'Call TIscr2_MovPop_4a(dip, appl, .wa(t - 1), .wa(t), t, .wb(0), .wb(0), fraz, 2)
        Call TIscr2_MovPop_4a(dip, appl, tb1, .wa(t - 1), .wa(t), t, t, wb0, wb0, fraz, 2) '20150504LC
      End If
    End If
    '******************
    'Muove i pensionati
    '******************
    Call TIscr2_MovPop_4c(dip, appl, s0, s1, tb1, t)  '20140414LC
    '***************************************
    'Pensionati di inabilit� e di invalidit�
    '***************************************
    wa1.c1b_z = 0
    wa1.c1i_z = 0
    If appl.bProb_EA_IV_IB = True Then '20121113LC
      t1b = IIf(.gruppo0 = 1, t - (.anno1 - .anno0) * 1 - 1, 0)
      t1a = IIf(.gruppo0 = 1, 1, 0)
      iwbm = (t - 1) * (1 + P_DUECENTO) + t1b
      iwb = t * (1 + P_DUECENTO) + t1b
      For t1 = t1b To t1a Step -1   'TIscr2_MovPop_4-5
        '--- Pensionati di inabilit�
        If .wb(iwbm).bg2 > AA0 Then
          yd = .wb(iwbm).bg2 * .qbd '20121029LC (TODOTODOTODO)
          If yd > 0 Then
            .wb(iwb).bg2 = .wb(iwb).bg2 - yd
            wa1.c1i_z = wa1.c1i_z + .wb(iwb).bg2
          End If
        End If
        '--- Pensionati di invalidit�
        If .wb(iwbm).ig2 > AA0 Then
          yd = .wb(iwbm).ig2 * .qid '20121029LC (TODOTODOTODO)
          If yd > 0 Then
            .wb(iwb).ig2 = .wb(iwb).ig2 - yd
            wa1.c1i_z = wa1.c1i_z + .wb(iwb).ig2
          End If
        End If
        '---
        iwbm = iwb - 1
        iwb = iwb - 1
      Next t1
    End If
    '************************
    'Pensionati di anziantit�
    '************************
    If wa0.c1ca_z > AA0 Then
      yd = wa0.c1ca_z * .qpad
      If yd > 0 Then
        wa1.c1ca_z = wa1.c1ca_z - yd
      End If
    End If
    '---
    If wa0.c1na_z > AA0 Then
      yd = wa0.c1na_z * .qpnd
      If yd > 0 Then
        wa1.c1na_z = wa1.c1na_z - yd
      End If
    End If
    '--- 20131202LC (inizio)
    '*********************************
    'Pensionati di vecchiaia ordinaria
    '*********************************
    If wa0.c1cvo_z > AA0 Then
      yd = wa0.c1cvo_z * .qpad
      If yd > 0 Then
        wa1.c1cvo_z = wa1.c1cvo_z - yd
      End If
    End If
    '---
    If wa0.c1nvo_z > AA0 Then
      yd = wa0.c1nvo_z * .qpnd
      If yd > 0 Then
        wa1.c1nvo_z = wa1.c1nvo_z - yd
      End If
    End If
    '**********************************
    'Pensionati di vecchiaia anticipata
    '**********************************
    If wa0.c1cva_z > AA0 Then
      yd = wa0.c1cva_z * .qpad
      If yd > 0 Then
        wa1.c1cva_z = wa1.c1cva_z - yd
      End If
    End If
    '---
    If wa0.c1nva_z > AA0 Then
      yd = wa0.c1nva_z * .qpnd
      If yd > 0 Then
        wa1.c1nva_z = wa1.c1nva_z - yd
      End If
    End If
    '***********************************
    'Pensionati di vecchiaia posticipata
    '***********************************
    If wa0.c1cvp_z > AA0 Then
      yd = wa0.c1cvp_z * .qpad
      If yd > 0 Then
        wa1.c1cvp_z = wa1.c1cvp_z - yd
      End If
    End If
    '---
    If wa0.c1nvp_z > AA0 Then
      yd = wa0.c1nvp_z * .qpnd
      If yd > 0 Then
        wa1.c1nvp_z = wa1.c1nvp_z - yd
      End If
    End If
    '--- 20131202LC (fine)
    '*************************
    'Pensionati p.contributive
    '*************************
    If wa0.c1cs_z > AA0 Then
      yd = wa0.c1cs_z * .qpad
      If yd > 0 Then
        wa1.c1cs_z = wa1.c1cs_z - yd
      End If
    End If
    '---
    If wa0.c1ns_z > AA0 Then
      yd = wa0.c1ns_z * .qpnd
      If yd > 0 Then
        wa1.c1ns_z = wa1.c1ns_z - yd
      End If
    End If
    '****************************
    'Pensionati di totalizzazione
    '****************************
    If wa0.c1ct_z > AA0 Then
      yd = wa0.c1ct_z * .qpad
      If yd > 0 Then
        wa1.c1ct_z = wa1.c1ct_z - yd
      End If
    End If
    '---
    If wa0.c1nt_z > AA0 Then
      yd = wa0.c1nt_z * .qpnd
      If yd > 0 Then
        wa1.c1nt_z = wa1.c1nt_z - yd
      End If
    End If
    '--- 20120115LC (inizio)
    'If s1.stato <> ST_PAT Then
    If (s1.stato = ST_A) Then
      If wa0.c1cvo_z + wa0.c1cva_z + wa0.c1cvp_z + wa0.c1ca_z + wa0.c1cs_z + wa0.c1ct_z > AA0 Then  '20131202LC
Call MsgBox("TODO-MovPop_4-2", , .mat)
Stop
      ElseIf wa1.c1cvo_z + wa1.c1cva_z + wa1.c1cvp_z + wa1.c1ca_z + wa1.c1cs_z + wa1.c1ct_z > AA0 Then  '20131202LC
        Call TZD_Test_C(wa1)
        s1.stato = ST_PAT  ':=
        s1.hq = -1  '20140703LC  'riportare 20140111LC-23
      End If
    End If
    '--- 20120115LC (fine)
    If appl.bProb_Superst = True Then  '20121109LC
      Call TIscr2_MovPop_Sup1b(dip, appl, tb1, 0, 0, 3) '20080919LC
    End If
  End With
End Sub


Public Sub TIscr2_MovPop_4a(ByRef dip As TIscr2, ByRef appl As TAppl, ByRef tb1() As Double, ByRef wa0 As TWA, _
                            ByRef wa1 As TWA, ByRef t&, ByRef t1&, ByRef wb0 As TWB, ByRef wb1 As TWB, ByRef fraz#, ByRef iop&)
'****************************************************************************************************
'  Crea i nuovi pensionati
'  20080912LC
'  20150504LC  t(ENAS)
'****************************************************************************************************
  Dim bFig As Boolean
  Dim te&  '20150504LC
  Dim FRAZ_PC#, FRAZ_PNC#, h0#, h0old#, h1#, h2#, om#, pens1a#, pens1a1#, pens1a2#, PRCTTLZ#, xp#, yd# '20120214LC
  Dim cp0#, cp1#  '20140312LC
  Dim omV#()  '20150512LC
  
  '--- 20140312LC (inizio)
  cp0 = OPZ_PENS_C0
  cp1 = 1 - cp0
  '--- 20140312LC (fine)
  With dip
    xp = .xr + fraz * IIf(9 = 9, 1, 0)
    PRCTTLZ = appl.CoeffVar(.t, ecv.cv_PrcPTattM + .sesso - 1) '20120214LC
    '******************************
    'Pensionati indiretti da attivi
    '******************************
    If iop = 1 Then
      yd = wa0.c0a_z * .qad1 * fraz
      If yd > AA0 Then
        '--- 20121116LC (inizio)
        wa1.c0a_z = wa1.c0a_z - yd
        '--- 20140111LC-16 (inizio) (mat=16470)
        pens1a1 = .s(.t).pens1a1
        pens1a2 = .s(.t).pens1a2
        '--- 20140111LC-16 (fine)
        '--- 20140111LC-15 (inizio)
        Call TIscr2_PensDirInd1(dip, wb0, wb1, appl, pens1a1, pens1a2, fraz, .tau1, 0, etp.PIND, 0) '20150504LC
        '--- 20140111LC-15 (fine)
        If .a_tpind = A1_RC Or appl.bProb_Superst = False Then
          '--- Popolazione  ---
          wa1.w3b_z = wa1.w3b_z + yd  'deceduti senza eredi, in quanto non si restituiscono i contributi
          '--- Entrate ---
          '--- Uscite ---
            If OPZ_RC_DEC_SENZA_SUP > AA0 Then  'TODO3
            bFig = TIscr2_bFig(dip, 0#)  '20131123LC
            '20160124LC  'ex 20150512LC  'ex 20131123LC  'ex 20121127LC
            om = TIscr2_Montante(dip, appl, .t, -1, 1, False, bFig, 1 * 0#, omV())
            wa1.w3b_zp = yd * ARRV(om * .cv4.RcPrcCon) * OPZ_RC_DEC_SENZA_SUP
            wa1.w3b_zm = (wa1.w3b_z - wa0.w3b_z) * OPZ_RC_W
            If wa1.w3b_zm > 0 Then
              wa1.w3b_p = wa1.w3b_zp / wa1.w3b_zm
            Else
              wa1.w3b_p = 0
            End If
          End If
          '--- 20160614LC (inizio)  'FIRR  'adattato da 41I 20150118LC
          Call TIscr2_MovPop_Firr(dip, appl, yd, EFirr.Firr_S)  '1 di 8
          '--- 20160614LC (fine)  'FIRR  'adattato da 41I 20150118LC
        Else
          Call TIscr2_MovPop_4b1(dip, appl, tb1, .s(.t), -TB_MPN_A, 1, xp, .a_hind, .a_pind, _
                                 .a_pind_fc, .a_pind_fa1, yd, EFirr.Firr_A)  '20131202LC  'ex 20121115LC
        End If
        '--- 20121116LC (fine)
      End If
    '****************************
    'Pensionati diretti da attivi
    '****************************
    ElseIf iop = 2 Then
      If .annoPensA = .t Then
        '--- Pensionati superstiti dal pensionamento al 31/12 ---
        yd = wa0.c0a_z * .qad1 * (1 - fraz)
        If yd > AA0 Then
          wa1.c0a_z = wa1.c0a_z - yd
          '--- 20121116LC (inizio)
          If .a_tprev = A1_RC Or appl.bProb_Superst = False Then
            '--- Popolazione  ---
            wa1.w3b_z = wa1.w3b_z + yd  'deceduti senza eredi, in quanto non si restituiscono i contributi
            '--- Entrate ---
            '--- Uscite ---
            If OPZ_RC_DEC_SENZA_SUP > AA0 Then  'TODO3
              bFig = TIscr2_bFig(dip, 0#)  '20131123LC
              '20160124LC  'ex 20150512LC  'ex 20131123LC  'ex 20121127LC
              om = TIscr2_Montante(dip, appl, .t, -1, 1, False, True, 1 * 0#, omV())
              wa1.w3b_zp = yd * ARRV(om * .cv4.RcPrcCon) * OPZ_RC_DEC_SENZA_SUP
              wa1.w3b_zm = (wa1.w3b_z - wa0.w3b_z) * OPZ_RC_W
              If wa1.w3b_zm > 0 Then
                wa1.w3b_p = wa1.w3b_zp / wa1.w3b_zm
              Else
                wa1.w3b_p = 0
              End If
            End If
            '--- 20160614LC (inizio)  'FIRR  'adattato da 41I 20150118LC
            Call TIscr2_MovPop_Firr(dip, appl, yd, EFirr.Firr_S)  '2 di 8
            '--- 20160614LC (fine)  'FIRR  'adattato da 41I 20150118LC
          Else
            Call TIscr2_MovPop_4b1(dip, appl, tb1, .s(.t), -TB_MPN_A, 1, xp, .a_hrev, .a_prev, _
                                   .a_prev_fc, .a_prev_fa1, yd, EFirr.Firr_A)  '20131202LC  'ex 20121115LC
          End If
          '--- 20121116LC (fine)
        End If
        '--- Pensionati diretti ---
        yd = wa1.c0a_z
        If yd > AA0 Then
          '20120221LC (inizio)
          If .tipoPensA >= TPENS_VO And .tipoPensA <= TPENS_VP Then  '20131129LC
            FRAZ_PC = appl.CoeffVar(.t, ecv.cv_PrcPVattM + .sesso - 1)
          ElseIf .tipoPensA = TPENS_AO Then  '20131129LC
            '20120601LC (inizio)
            If .bModArt26 = 0 Then
'20131129LC
'call MsgBox("TODO TIscr2_MovPop_4a 1", , .mat)
'Stop
              FRAZ_PC = appl.CoeffVar(.t, ecv.cv_PrcPAattM + .sesso - 1)
            Else
              '22991, 24975, 25001
              FRAZ_PC = 0
            End If
            '20120601LC (fine)
          '20120502LC (inizio)
          ElseIf Math.Abs(.tipoPensA) = TPENS_S Then
            If .tipoPensA = TPENS_S Then
              FRAZ_PC = appl.CoeffVar(.t, ecv.cv_PrcPCattM + .sesso - 1)
            Else
              FRAZ_PC = 0
            End If
          '20120502LC (fine)
          ElseIf .tipoPensA = TPENS_T Then
            FRAZ_PC = appl.CoeffVar(.t, ecv.cv_PrcPTattM + .sesso - 1)
          Else
            FRAZ_PC = 0
          End If
          If FRAZ_PC > AA0 Then
            '--- 20131202LC (inizio)
            If .tipoPensA = TPENS_VO Then
              wa1.c1cvo_z = wa1.c1cvo_z + FRAZ_PC * yd
              Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPC_VO, .stipoPensA + 0, 1, xp, .a_hrev, .a_prev, _
                                     .a_prev_fc, .a_prev_fa1, FRAZ_PC * yd, EFirr.Firr_A)   '20131202LC  'ex 20121115LC
            ElseIf .tipoPensA = TPENS_VA Then
              wa1.c1cva_z = wa1.c1cva_z + FRAZ_PC * yd
              Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPC_VA, .stipoPensA + 0, 1, xp, .a_hrev, .a_prev, _
                                     .a_prev_fc, .a_prev_fa1, FRAZ_PC * yd, EFirr.Firr_A)  '20131202LC  'ex 20121115LC
            ElseIf .tipoPensA = TPENS_VP Then
              wa1.c1cvp_z = wa1.c1cvp_z + FRAZ_PC * yd
              Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPC_VP, .stipoPensA + 0, 1, xp, .a_hrev, .a_prev, _
                                     .a_prev_fc, .a_prev_fa1, FRAZ_PC * yd, EFirr.Firr_A)  '20131202LC  'ex 20121115LC
            '--- 20131202LC (fine)
            ElseIf .tipoPensA = TPENS_AO Then  '20131129LC
'20131129LC
'call MsgBox("TODO TIscr2_MovPop_4a 2", , .mat)
'Stop
              wa1.c1ca_z = wa1.c1ca_z + FRAZ_PC * yd
              Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPC_A, .stipoPensA + 0, 1, xp, .a_hrev, .a_prev, _
                                     .a_prev_fc, .a_prev_fa1, FRAZ_PC * yd, EFirr.Firr_A)  '20131202LC  'ex 20121115LC
            '20120502LC (inizio)
            ElseIf Math.Abs(.tipoPensA) = TPENS_S Then
              If .tipoPensA = TPENS_S Then
                wa1.c1cs_z = wa1.c1cs_z + FRAZ_PC * yd
                Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPC_S, .stipoPensA + 0, 1, xp, .a_hrev, .a_prev, _
                                       .a_prev_fc, .a_prev_fa1, FRAZ_PC * yd, EFirr.Firr_A)  '20131202LC  'ex 20121115LC
              Else
                '.z1(t).z.yw1b = .z1(t).z.yw1b + FRAZ_PC * yd
                wa1.w1b_z = wa1.w1b_z + FRAZ_PC * yd
              End If
            '20120502LC (fine)
            ElseIf .tipoPensA = TPENS_T Then
              wa1.c1ct_z = wa1.c1ct_z + FRAZ_PC * yd
              '20120502LC
              'Call TIscr2_MovPop_4b2(dip, appl, TB_MPN_T, 1, xp, .a_hrev, .a_prev, FRAZ_PC * yd)
              Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPC_T, .stipoPensA + 0, 1, xp, .a_hrev, .a_prev, _
                                     .a_prev_fc, .a_prev_fa1, FRAZ_PC * yd, EFirr.Firr_A)  '20131202LC  'ex 20121115LC
            Else
Call MsgBox("TODO-TIscr2_MovPop_4a-1", , .mat)
Stop
            End If
Call TZD_Test_C(wa1) '20120115LC
          End If
          '---
          FRAZ_PNC = 1 - FRAZ_PC
          If FRAZ_PNC > AA0 Then
            '--- 20131202LC (inizio)
            If .tipoPensA = TPENS_VO Then
              wa1.c1nvo_z = wa1.c1nvo_z + FRAZ_PNC * yd
              wa1.c1nvo_p = .a_prev
              wa1.c1nvo_zm = (wa0.c1nvo_z * cp0 + wa1.c1nvo_z * cp1)  '20140312LC
              wa1.c1nvo_zp = wa1.c1nvo_zm * wa1.c1nvo_p
              Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPN_VO, .stipoPensA + 0, 1, xp, .a_hrev, .a_prev, _
                                     .a_prev_fc, .a_prev_fa1, FRAZ_PNC * yd, EFirr.Firr_A)  '20131202LC  'ex 20121115LC
            ElseIf .tipoPensA = TPENS_VA Then
              wa1.c1nva_z = wa1.c1nva_z + FRAZ_PNC * yd
              wa1.c1nva_p = .a_prev
              wa1.c1nva_zm = (wa0.c1nva_z * cp0 + wa1.c1nva_z * cp1)  '20140312LC
              wa1.c1nva_zp = wa1.c1nva_zm * wa1.c1nva_p
              Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPN_VA, .stipoPensA + 0, 1, xp, .a_hrev, .a_prev, _
                                     .a_prev_fc, .a_prev_fa1, FRAZ_PNC * yd, EFirr.Firr_A)  '20131202LC  'ex 20121115LC
            ElseIf .tipoPensA = TPENS_VP Then
              wa1.c1nvp_z = wa1.c1nvp_z + FRAZ_PNC * yd
              wa1.c1nvp_p = .a_prev
              wa1.c1nvp_zm = (wa0.c1nvp_z * cp0 + wa1.c1nvp_z * cp1)  '20140312LC
              wa1.c1nvp_zp = wa1.c1nvp_zm * wa1.c1nvp_p
              'Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPN_VO, .stipoPensA + 0, 1, xp, .a_hrev, .a_prev, .a_prev_fc, .a_prev_fa1, FRAZ_PNC * yd)  '20131202LC  'ex 20121115LC
              Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPN_VP, .stipoPensA + 0, 1, xp, .a_hrev, .a_prev, _
                                     .a_prev_fc, .a_prev_fa1, FRAZ_PNC * yd, EFirr.Firr_A)  '20131202LC  'ex 20121115LC
            '--- 20131202LC (fine)
            ElseIf .tipoPensA = TPENS_AO Then  '20131129LC
              wa1.c1na_z = wa1.c1na_z + FRAZ_PNC * yd
              wa1.c1na_p = .a_prev
              wa1.c1na_zm = (wa0.c1na_z * cp0 + wa1.c1na_z * cp1)  '20140312LC
              wa1.c1na_zp = wa1.c1na_zm * wa1.c1na_p
              Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPN_A, .stipoPensA + 0, 1, xp, .a_hrev, .a_prev, _
                                     .a_prev_fc, .a_prev_fa1, FRAZ_PNC * yd, EFirr.Firr_A)  '20131202LC  'ex 20121115LC
            '20120502LC (inizio)
            ElseIf Math.Abs(.tipoPensA) = TPENS_S Then
              If .tipoPensA = TPENS_S Then
                wa1.c1ns_z = wa1.c1ns_z + FRAZ_PNC * yd
                wa1.c1ns_p = .a_prev
                wa1.c1ns_zm = (wa0.c1ns_z * cp0 + wa1.c1ns_z * cp1)  '20140312LC
                wa1.c1ns_zp = wa1.c1ns_zm * wa1.c1ns_p
                Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPN_S, .stipoPensA + 0, 1, xp, .a_hrev, .a_prev, _
                                       .a_prev_fc, .a_prev_fa1, FRAZ_PNC * yd, EFirr.Firr_A)  '20131202LC  'ex 20121115LC
              Else
Call MsgBox("TODO-TIscr2_MovPop_4a-2", , .mat)
Stop
                '.z1(t).z.yw1b = .z1(t).z.yw1b + FRAZ_PNC * yd
                wa1.w1b_z = wa1.w1b_z + FRAZ_PNC * yd
              End If
            '20120502LC (fine)
            ElseIf .tipoPensA = TPENS_T Then
              wa1.c1nt_z = wa1.c1nt_z + FRAZ_PNC * yd
              wa1.c1nt_p = .a_prev
              wa1.c1nt_zm = (wa0.c1nt_z * cp0 + wa1.c1nt_z * cp1)  '20140312LC
              wa1.c1nt_zp = wa1.c1nt_zm * wa1.c1nt_p
              Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPN_T, .stipoPensA + 0, 1, xp, .a_hrev, .a_prev, _
                                     .a_prev_fc, .a_prev_fa1, FRAZ_PNC * yd, EFirr.Firr_A)  '20131202LC  'ex 20121115LC
            Else
Call MsgBox("TODO-TIscr2_MovPop_4a-3", , .mat)
Stop
            End If
          End If
          '20120221LC (fine)
          wa1.c0a_z = wa1.c0a_z - yd
        End If
      End If
    '--- 20150504LC (inizio)
    '**********************************************
    'Pensionati indiretti da contribuenti volontari
    '**********************************************
    ElseIf iop = -3 Then
      yd = wb0.vg2 * .qvd1 * fraz
      If yd > AA0 Then
        wb1.vg2 = wb1.vg2 - yd
        '--- 20150506LC (inizio)
        pens1a1 = wb1.vg2p1
        pens1a2 = wb1.vg2p2
        '--- 20150506LC (fine)
        Call TIscr2_PensDirInd1(dip, wb0, wb1, appl, pens1a1, pens1a2, fraz, .tau1, t1, etp.PIND, 1) '20150504LC
        '--- 20121116LC (inizio)
        If .va_tpind = A1_RC Or appl.bProb_Superst = False Then
          '--- Popolazione  ---
          wa1.w3b_z = wa1.w3b_z + yd  'deceduti senza eredi, in quanto non si restituiscono i contributi
          '--- Entrate ---
          '--- Uscite ---
          If OPZ_RC_DEC_SENZA_SUP > AA0 Then  'TODO3
            bFig = TIscr2_bFig(dip, 0#)  '20131123LC
            '20160124LC  'ex 20150512LC  'ex 20131123LC  'ex 20121127LC
            om = TIscr2_Montante(dip, appl, .t, -1, 1, False, True, 1 * 0#, omV())
            wa1.w3b_zp = yd * ARRV(om * .cv4.RcPrcCon) * OPZ_RC_DEC_SENZA_SUP
            wa1.w3b_zm = (wa1.w3b_z - wa0.w3b_z) * OPZ_RC_W
            If wa1.w3b_zm > 0 Then
              wa1.w3b_p = wa1.w3b_zp / wa1.w3b_zm
            Else
              wa1.w3b_p = 0
            End If
          End If
          '--- 20160614LC (inizio)  'FIRR  'adattato da 41I 20150118LC
          Call TIscr2_MovPop_Firr(dip, appl, yd, EFirr.Firr_S)  '3 di 8
          '--- 20160614LC (fine)  'FIRR  'adattato da 41I 20150118LC
        Else
          'mat=4390000 5->1->6 dipGen=5/237 (TODOTODO: controllare)
          Call TIscr2_MovPop_4b1(dip, appl, tb1, .s(.t), -TB_MPN_A, 5, xp, .va_hind, .va_pind, _
                                 .va_pind_fc, .va_pind_fa1, yd, EFirr.Firr_V)  '20131202LC  'ex 20121115LC
        End If
        '--- 20121116LC (fine)
      End If
    '********************************************
    'Pensionati diretti da contribuenti volontari
    '********************************************
    ElseIf iop = -4 Then
      If wb0.annoPensVA = .t Then
        '--- 20150506LC (inizio)
        pens1a1 = wb1.vg2p1
        pens1a2 = wb1.vg2p2
        '--- 20150506LC (fine)
        '--- 20150504LC (fine)
        Call TIscr2_PensDirInd1(dip, wb0, wb1, appl, pens1a1, pens1a2, fraz, .tau1, t1, etp.PDIR, 1) '20150504LC
        yd = wb0.vg2 * .qvd1 * (1 - fraz)
        If yd > AA0 Then
          wb1.vg2 = wb1.vg2 - yd
          '--- 20121116LC (inizio)
          If .va_tprev = A1_RC Or appl.bProb_Superst = False Then
            '--- Popolazione  ---
            wa1.w3b_z = wa1.w3b_z + yd  'deceduti senza eredi, in quanto non si restituiscono i contributi
            '--- Entrate ---
            '--- Uscite ---
            If OPZ_RC_DEC_SENZA_SUP > AA0 Then  'TODO3
              bFig = TIscr2_bFig(dip, 0#)  '20131123LC
              '20160124LC  'ex 20150512LC  'ex 20131123LC  'ex 20121127LC
              om = TIscr2_Montante(dip, appl, .t, -1, 1, False, bFig, 1 * 0#, omV())
              wa1.w3b_zp = yd * ARRV(om * .cv4.RcPrcCon) * OPZ_RC_DEC_SENZA_SUP
              wa1.w3b_zm = (wa1.w3b_z - wa0.w3b_z) * OPZ_RC_W
              If wa1.w3b_zm > 0 Then
                wa1.w3b_p = wa1.w3b_zp / wa1.w3b_zm
              Else
                wa1.w3b_p = 0
              End If
            End If
            '--- 20160614LC (inizio)  'FIRR  'adattato da 41I 20150118LC
            Call TIscr2_MovPop_Firr(dip, appl, yd, EFirr.Firr_S)  '4 di 8
            '--- 20160614LC (fine)  'FIRR  'adattato da 41I 20150118LC
          Else
            Call TIscr2_MovPop_4b1(dip, appl, tb1, .s(.t), -TB_MPN_A, 5, xp, .va_hrev, .va_prev, _
                                   .va_prev_fc, .va_prev_fa1, yd, EFirr.Firr_V)  '20131202LC  'ex 20121115LC
          End If
          '--- 20121116LC (fine)
        End If
        yd = wb1.vg2
        If yd > AA0 Then
          '20120221LC (inizio)
          If wb0.tipoPensVA >= TPENS_VO And wb0.tipoPensVA <= TPENS_VP Then  '20131129LC
            FRAZ_PC = 0 '.PRC_PV_ATT
          ElseIf wb0.tipoPensVA = TPENS_AO Then  '20131129LC
            FRAZ_PC = 0 '.PRC_PA_ATT
          '20120502LC (inizio)
          ElseIf Math.Abs(wb0.tipoPensVA) = TPENS_S Then
            If wb0.tipoPensVA = TPENS_S Then
              FRAZ_PC = 0 '.PRC_PV_ATT
            Else
              FRAZ_PC = 0 '.PRC_PV_ATT
            End If
          '20120502LC (fine)
          ElseIf wb0.tipoPensVA = TPENS_T Then
            FRAZ_PC = 0 '.PRC_PV_ATT
          Else
Call MsgBox("TODO-TIscr2_MovPop_4a-4", , .mat)
Stop
          End If
          If FRAZ_PC > AA0 Then
            '20120502LC (inizio)
            '--- 20131202LC (inizio)
            If wb0.tipoPensVA = TPENS_VO Then
              wa1.c1cvo_z = wa1.c1cvo_z + FRAZ_PC * yd
              Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPC_VO, wb0.stipoPensVA + 0, 1, xp, .va_hrev, .va_prev, _
                                     .va_prev_fc, .va_prev_fa1, FRAZ_PC * yd, EFirr.Firr_V)  '20131202LC  'ex 20121230LC
            ElseIf wb0.tipoPensVA = TPENS_VA Then
              wa1.c1cva_z = wa1.c1cva_z + FRAZ_PC * yd
              Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPC_VA, wb0.stipoPensVA + 0, 1, xp, .va_hrev, .va_prev, _
                                     .va_prev_fc, .va_prev_fa1, FRAZ_PC * yd, EFirr.Firr_V)  '20131202LC  'ex 20121230LC
            ElseIf wb0.tipoPensVA = TPENS_VP Then
              wa1.c1cvp_z = wa1.c1cvp_z + FRAZ_PC * yd
              Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPC_VP, wb0.stipoPensVA + 0, 1, xp, .va_hrev, .va_prev, _
                                     .va_prev_fc, .va_prev_fa1, FRAZ_PC * yd, EFirr.Firr_V)  '20131202LC  'ex 20121230LC
            '--- 20131202LC (fine)
            ElseIf wb0.tipoPensVA = TPENS_AO Then  '20131129LC
Call MsgBox("TODO-TIscr2_MovPop_4a 2", , .mat)
Stop
              wa1.c1ca_z = wa1.c1ca_z + FRAZ_PC * yd
              Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPC_A, wb0.stipoPensVA + 0, 1, xp, .va_hrev, .va_prev, _
                                     .va_prev_fc, .va_prev_fa1, FRAZ_PC * yd, EFirr.Firr_V)  '20131202LC  'ex 20121230LC
            ElseIf Math.Abs(wb0.tipoPensVA) = TPENS_S Then
              If wb0.tipoPensVA = TPENS_S Then
                wa1.c1cs_z = wa1.c1cs_z + FRAZ_PC * yd
                Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPC_S, wb0.stipoPensVA + 0, 1, xp, .va_hrev, .va_prev, _
                                       .va_prev_fc, .va_prev_fa1, FRAZ_PC * yd, EFirr.Firr_V)  '20131202LC  'ex 20121230LC
              Else
Call MsgBox("TODO-TIscr2_MovPop_4a-5", , .mat)
Stop
                '.z1(t).z.yw1b = .z1(t).z.yw1b + FRAZ_PC * yd
                wa1.w1b_z = wa1.w1b_z + FRAZ_PC * yd
              End If
            ElseIf wb0.tipoPensVA = TPENS_T Then
              wa1.c1ct_z = wa1.c1ct_z + FRAZ_PC * yd
              Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPC_T, wb0.stipoPensVA + 0, 1, xp, .va_hrev, .va_prev, _
                                     .va_prev_fc, .va_prev_fa1, FRAZ_PC * yd, EFirr.Firr_V)  '20131202LC  'ex 20121230LC
            End If
            '20120502LC (fine)
Call TZD_Test_C(wa1)  '20120115LC
          End If
          '---
          FRAZ_PNC = 1 - FRAZ_PC
          If FRAZ_PNC > AA0 Then
            '--- 20131202LC (inizio)
            If wb0.tipoPensVA = TPENS_VO Then  '20131129LC
              wa1.c1nvo_z = wa1.c1nvo_z + FRAZ_PNC * yd
              wa1.c1nvo_p = .va_prev
              wa1.c1nvo_zm = (wa0.c1nvo_z * cp0 + wa1.c1nvo_z * cp1)  '20140312LC
              wa1.c1nvo_zp = wa1.c1nvo_zm * wa1.c1nvo_p
              Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPN_VO, wb0.stipoPensVA + 0, 5, xp, .va_hrev, .va_prev, _
                                     .va_prev_fc, .va_prev_fa1, FRAZ_PNC * yd, EFirr.Firr_V)  '20131202LC  'ex 20121230LC
            ElseIf wb0.tipoPensVA = TPENS_VA Then  '20131129LC
              wa1.c1nva_z = wa1.c1nva_z + FRAZ_PNC * yd
              wa1.c1nva_p = .va_prev
              wa1.c1nva_zm = (wa0.c1nva_z * cp0 + wa1.c1nva_z * cp1)  '20140312LC
              wa1.c1nva_zp = wa1.c1nva_zm * wa1.c1nva_p
              Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPN_VA, wb0.stipoPensVA + 0, 5, xp, .va_hrev, .va_prev, _
                                     .va_prev_fc, .va_prev_fa1, FRAZ_PNC * yd, EFirr.Firr_V)  '20131202LC  'ex 20121230LC
            ElseIf wb0.tipoPensVA = TPENS_VP Then  '20131129LC
              wa1.c1nvp_z = wa1.c1nvp_z + FRAZ_PNC * yd
              wa1.c1nvp_p = .va_prev
              wa1.c1nvp_zm = (wa0.c1nvp_z * cp0 + wa1.c1nvp_z * cp1)  '20140312LC
              wa1.c1nvp_zp = wa1.c1nvp_zm * wa1.c1nvp_p
              Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPN_VP, wb0.stipoPensVA + 0, 5, xp, .va_hrev, .va_prev, _
                                     .va_prev_fc, .va_prev_fa1, FRAZ_PNC * yd, EFirr.Firr_V)  '20131202LC  'ex 20121230LC
            '--- 20131202LC (fine)
            ElseIf wb0.tipoPensVA = TPENS_AO Then  '20131129LC
              wa1.c1na_z = wa1.c1na_z + FRAZ_PNC * yd
              wa1.c1na_p = .va_prev
              wa1.c1na_zm = (wa0.c1na_z * cp0 + wa1.c1na_z * cp1)  '20140312LC
              wa1.c1na_zp = wa1.c1na_zm * wa1.c1na_p
              Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPN_A, wb0.stipoPensVA + 0, 5, xp, .va_hrev, .va_prev, _
                                     .va_prev_fc, .va_prev_fa1, FRAZ_PNC * yd, EFirr.Firr_V)  '20131202LC  'ex 20121230LC
            '20120502LC (inizio)
            ElseIf Math.Abs(wb0.tipoPensVA) = TPENS_S Then
              If wb0.tipoPensVA = TPENS_S Then
                wa1.c1ns_z = wa1.c1ns_z + FRAZ_PNC * yd
                wa1.c1ns_p = .va_prev
                wa1.c1ns_zm = (wa0.c1ns_z * cp0 + wa1.c1ns_z * cp1)  '20140312LC
                wa1.c1ns_zp = wa1.c1ns_zm * wa1.c1ns_p
                Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPN_S, wb0.stipoPensVA + 0, 5, xp, .va_hrev, .va_prev, _
                                       .va_prev_fc, .va_prev_fa1, FRAZ_PNC * yd, EFirr.Firr_V)  '20131202LC  'ex 20121230LC
              Else
                '.z1(t).z.yw1b = .z1(t).z.yw1b + FRAZ_PNC * yd
                wa1.w1b_z = wa1.w1b_z + FRAZ_PNC * yd
              End If
            '20120502LC (fine)
            ElseIf wb0.tipoPensVA = TPENS_T Then
              wa1.c1nt_z = wa1.c1nt_z + FRAZ_PNC * yd
              wa1.c1nt_p = .va_prev
              wa1.c1nt_zm = (wa0.c1nt_z * cp0 + wa1.c1nt_z * cp1)  '20140312LC
              wa1.c1nt_zp = wa1.c1nt_zm * wa1.c1nt_p
              Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPN_T, wb0.stipoPensVA + 0, 5, xp, .va_hrev, .va_prev, _
                                     .va_prev_fc, .va_prev_fa1, FRAZ_PNC * yd, EFirr.Firr_V)  '20131202LC  'ex 20121230LC
            ElseIf .xan + 1 >= .cv4.Pveo98 Then '.st = "A"
Call MsgBox("TODO-TIscr2_MovPop_4a-6", , .mat)
Stop
              yd = wb0.vg2 * .qvd1 * .coe0
              wa1.w3b_z = wa1.w3b_z + yd
              wa1.w2b_z = wa1.w2b_z + wb0.vg2 - yd
            ElseIf 1 = 2 And .xan + 1 < .cv4.Pveo98 Then '.st = "C" 'TODO?
Call MsgBox("TODO-TIscr2_MovPop_4a-6a", , .mat)
Stop
              yd = wb0.vg2 * .qvd1
              'If iind = A1_RC Then
                wa1.w3b_z = wa1.w3b_z + yd
              'End If
              wb1.vg2 = wb1.vg2 - yd
            End If
          End If
          '20120221LC (fine)
          '.z1(t).z.yvg2(t1) = 0
          wb1.vg2 = 0
        End If
      End If
    '--- 20150504LC (fine)
    '*******************************************
    'Pensionati indiretti da ex attivi / silenti
    '*******************************************
    ElseIf iop = 3 Then
      yd = wb0.eg2 * .qed1 * fraz
      If yd > AA0 Then
        wb1.eg2 = wb1.eg2 - yd
        '--- 20150504LC (inizio)
        te = .t - t + t1
        If te < appl.t0 + 1 Then
          te = appl.t0 + 1
        End If
        '--- 20160128LC (inizio)  2440240 genera -20200 che non ha reddito nel 2013+1
        If te < dip.annoAss Then
          te = dip.annoAss
        End If
        '--- 20160128LC (fine)
        pens1a1 = .s(te).pens1a1 '* 1.03 ^ (.t - te)
        pens1a2 = .s(te).pens1a2 '* 1.03 ^ (.t - te)
        '--- 20150504LC (fine)
        Call TIscr2_PensDirInd1(dip, wb0, wb1, appl, pens1a1, pens1a2, fraz, .tau1, t1, etp.PIND, 2) '20150504LC  '213,74!!! '20120619LC
        '--- 20160420LC (inizio)
        If .ea_pind <= OPZ_INT_TOLL_PENS And .ea_tpind <> A1_RC Then
          .ea_tpind = A1_RC  '304541(2023)
        End If
        '--- 20160420LC (fine)
        '--- 20121116LC (inizio)
        If .ea_tpind = A1_RC Or appl.bProb_Superst = False Then
          '--- Popolazione  ---
          wa1.w3b_z = wa1.w3b_z + yd  'deceduti senza eredi, in quanto non si restituiscono i contributi
          '--- Entrate ---
          '--- Uscite ---
          If OPZ_RC_DEC_SENZA_SUP > AA0 Then  'TODO3
            bFig = TIscr2_bFig(dip, 0#)  '20131123LC
            '20160124LC  'ex 20150512LC  'ex 20131123LC  'ex 20121127LC
            om = TIscr2_Montante(dip, appl, .t, -1, 1, False, True, 1 * 0#, omV())
            wa1.w3b_zp = yd * ARRV(om * .cv4.RcPrcCon) * OPZ_RC_DEC_SENZA_SUP
            wa1.w3b_zm = (wa1.w3b_z - wa0.w3b_z) * OPZ_RC_W
            If wa1.w3b_zm > 0 Then
              wa1.w3b_p = wa1.w3b_zp / wa1.w3b_zm
            Else
              wa1.w3b_p = 0
            End If
          End If
          '--- 20160614LC (inizio)  'FIRR  'adattato da 41I 20150118LC
          Call TIscr2_MovPop_Firr(dip, appl, yd, EFirr.Firr_S)  '5 di 8
          '--- 20160614LC (fine)  'FIRR  'adattato da 41I 20150118LC
        Else
          Call TIscr2_MovPop_4b1(dip, appl, tb1, .s(.t), -TB_MPN_A, 5, xp, .ea_hind, .ea_pind, _
                                 .ea_pind_fc, .ea_pind_fa1, yd, EFirr.Firr_E)  '20131202LC  'ex 20121115LC
        End If
        '--- 20121116LC (fine)
      End If
    '*****************************************
    'Pensionati diretti da ex attivi / silenti
    '*****************************************
    ElseIf iop = 4 Then
      If wb0.annoPensEA = .t Then
        '--- 20150504LC (inizio)
        te = .t - t + t1
        If te < appl.t0 + 1 Then
          te = appl.t0 + 1
        End If
        '--- 20160128LC (inizio)  2440240 genera -20200 che non ha reddito nel 2013+1
        If te < dip.annoAss Then
          te = dip.annoAss
        End If
        '--- 20160128LC (fine)
        pens1a1 = .s(te).pens1a1 '* 1.03 ^ (.t - te)
        pens1a2 = .s(te).pens1a2 '* 1.03 ^ (.t - te)
        '--- 20150504LC (fine)
        '--- 20150504LC (fine)
        Call TIscr2_PensDirInd1(dip, wb0, wb1, appl, pens1a1, pens1a2, fraz, .tau1, t1, etp.PDIR, 2) '20150504LC '20120619LC
        '--- 20161126LC (inizio)  spostata in alto  'ex 20160411LC
        If .ea_prev <= OPZ_INT_TOLL_PENS Then
          wb0.tipoPensEA = -TPENS_S  '3181000(5,2018) 3213000(5,2015) 3231000(5,2019) | x fraz<1 540000(5,2038)
          .ea_tprev = A1_RC  '20161124LC-2
        End If
        '--- 20161126LC (fine)  spostata in alto  'ex 20160411LC
        yd = wb0.eg2 * .qed1 * (1 - fraz)
        If yd > AA0 Then
          wb1.eg2 = wb1.eg2 - yd
          '--- 20121116LC (inizio)
          If .ea_tprev = A1_RC Or appl.bProb_Superst = False Then
            '--- Popolazione  ---
            wa1.w3b_z = wa1.w3b_z + yd  'deceduti senza eredi, in quanto non si restituiscono i contributi
            '--- Entrate ---
            '--- Uscite ---
            If OPZ_RC_DEC_SENZA_SUP > AA0 Then  'TODO3
              bFig = TIscr2_bFig(dip, 0#)  '20131123LC
              '20160124LC  'ex 20150512LC  'ex 20131123LC  'ex 20121127LC
              om = TIscr2_Montante(dip, appl, .t, -1, 1, False, bFig, 1 * 0#, omV())
              wa1.w3b_zp = yd * ARRV(om * .cv4.RcPrcCon) * OPZ_RC_DEC_SENZA_SUP
              wa1.w3b_zm = (wa1.w3b_z - wa0.w3b_z) * OPZ_RC_W
              If wa1.w3b_zm > 0 Then
                wa1.w3b_p = wa1.w3b_zp / wa1.w3b_zm
              Else
                wa1.w3b_p = 0
              End If
            End If
            '--- 20160614LC (inizio)  'FIRR  'adattato da 41I 20150118LC
            Call TIscr2_MovPop_Firr(dip, appl, yd, EFirr.Firr_S)  '6 di 8
            '--- 20160614LC (fine)  'FIRR  'adattato da 41I 20150118LC
          Else
            Call TIscr2_MovPop_4b1(dip, appl, tb1, .s(.t), -TB_MPN_A, 5, xp, .ea_hrev, .ea_prev, _
                                   .ea_prev_fc, .ea_prev_fa1, yd, EFirr.Firr_E)  '20131202LC  'ex 20121115LC
          End If
          '--- 20121116LC (fine)
        End If
        yd = wb1.eg2
        If yd > AA0 Then
          '--- 20160411LC (inizio)
          If .ea_prev <= OPZ_INT_TOLL_PENS Then  '20160420LC
            wb0.tipoPensEA = -TPENS_S  '3181000(5,2018) 3213000(5,2015) 3231000(5,2019)
          End If
          '--- 20160411LC (fine)
          '20120221LC (inizio)
          If wb0.tipoPensEA >= TPENS_VO And wb0.tipoPensEA <= TPENS_VP Then  '20131129LC
            FRAZ_PC = 0 '.PRC_PV_ATT
          ElseIf wb0.tipoPensEA = TPENS_AO Then  '20131129LC
            FRAZ_PC = 0 '.PRC_PA_ATT
          '20120502LC (inizio)
          ElseIf Math.Abs(wb0.tipoPensEA) = TPENS_S Then
            If wb0.tipoPensEA = TPENS_S Then
              FRAZ_PC = 0 '.PRC_PV_ATT
            Else
              FRAZ_PC = 0 '.PRC_PV_ATT
            End If
          '20120502LC (fine)
          ElseIf wb0.tipoPensEA = TPENS_T Then
            FRAZ_PC = 0 '.PRC_PV_ATT
          Else
Call MsgBox("TODO-TIscr2_MovPop_4a-4", , .mat)
Stop
          End If
          If FRAZ_PC > AA0 Then
            '20120502LC (inizio)
            '--- 20131202LC (inizio)
            If wb0.tipoPensEA = TPENS_VO Then
              wa1.c1cvo_z = wa1.c1cvo_z + FRAZ_PC * yd
              Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPC_VO, wb0.stipoPensEA + 0, 1, xp, .ea_hrev, .ea_prev, _
                                     .ea_prev_fc, .ea_prev_fa1, FRAZ_PC * yd, EFirr.Firr_E)  '20131202LC  'ex 20121230LC
            ElseIf wb0.tipoPensEA = TPENS_VA Then
              wa1.c1cva_z = wa1.c1cva_z + FRAZ_PC * yd
              Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPC_VA, wb0.stipoPensEA + 0, 1, xp, .ea_hrev, .ea_prev, _
                                     .ea_prev_fc, .ea_prev_fa1, FRAZ_PC * yd, EFirr.Firr_E)  '20131202LC  'ex 20121230LC
            ElseIf wb0.tipoPensEA = TPENS_VP Then
              wa1.c1cvp_z = wa1.c1cvp_z + FRAZ_PC * yd
              Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPC_VP, wb0.stipoPensEA + 0, 1, xp, .ea_hrev, .ea_prev, _
                                     .ea_prev_fc, .ea_prev_fa1, FRAZ_PC * yd, EFirr.Firr_E)  '20131202LC  'ex 20121230LC
            '--- 20131202LC (fine)
            ElseIf wb0.tipoPensEA = TPENS_AO Then  '20131129LC
Call MsgBox("TODO-TIscr2_MovPop_4a 2", , .mat)
Stop
              wa1.c1ca_z = wa1.c1ca_z + FRAZ_PC * yd
              Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPC_A, wb0.stipoPensEA + 0, 1, xp, .ea_hrev, .ea_prev, _
                                     .ea_prev_fc, .ea_prev_fa1, FRAZ_PC * yd, EFirr.Firr_E)  '20131202LC  'ex 20121230LC
            ElseIf Math.Abs(wb0.tipoPensEA) = TPENS_S Then
              If wb0.tipoPensEA = TPENS_S Then
                wa1.c1cs_z = wa1.c1cs_z + FRAZ_PC * yd
                Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPC_S, wb0.stipoPensEA + 0, 1, xp, .ea_hrev, .ea_prev, _
                                       .ea_prev_fc, .ea_prev_fa1, FRAZ_PC * yd, EFirr.Firr_E)  '20131202LC  'ex 20121230LC
              Else
Call MsgBox("TODO-TIscr2_MovPop_4a-5", , .mat)
Stop
                '.z1(t).z.yw1b = .z1(t).z.yw1b + FRAZ_PC * yd
                wa1.w1b_z = wa1.w1b_z + FRAZ_PC * yd
              End If
            ElseIf wb0.tipoPensEA = TPENS_T Then
              wa1.c1ct_z = wa1.c1ct_z + FRAZ_PC * yd
              Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPC_T, wb0.stipoPensEA + 0, 1, xp, .ea_hrev, .ea_prev, _
                                     .ea_prev_fc, .ea_prev_fa1, FRAZ_PC * yd, EFirr.Firr_E)  '20131202LC  'ex 20121230LC
            End If
            '20120502LC (fine)
Call TZD_Test_C(wa1)  '20120115LC
          End If
          '---
          FRAZ_PNC = 1 - FRAZ_PC
          If FRAZ_PNC > AA0 Then
            '--- 20131202LC (inizio)
            If wb0.tipoPensEA = TPENS_VO Then  '20131129LC
              wa1.c1nvo_z = wa1.c1nvo_z + FRAZ_PNC * yd
              wa1.c1nvo_p = .ea_prev
              wa1.c1nvo_zm = (wa0.c1nvo_z * cp0 + wa1.c1nvo_z * cp1)  '20140312LC
              wa1.c1nvo_zp = wa1.c1nvo_zm * wa1.c1nvo_p
              Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPN_VO, wb0.stipoPensEA + 0, 5, xp, .ea_hrev, .ea_prev, _
                                     .ea_prev_fc, .ea_prev_fa1, FRAZ_PNC * yd, EFirr.Firr_E)  '20131202LC  'ex 20121230LC
            ElseIf wb0.tipoPensEA = TPENS_VA Then  '20131129LC
              wa1.c1nva_z = wa1.c1nva_z + FRAZ_PNC * yd
              wa1.c1nva_p = .ea_prev
              wa1.c1nva_zm = (wa0.c1nva_z * cp0 + wa1.c1nva_z * cp1)  '20140312LC
              wa1.c1nva_zp = wa1.c1nva_zm * wa1.c1nva_p
              Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPN_VA, wb0.stipoPensEA + 0, 5, xp, .ea_hrev, .ea_prev, _
                                     .ea_prev_fc, .ea_prev_fa1, FRAZ_PNC * yd, EFirr.Firr_E)  '20131202LC  'ex 20121230LC
            ElseIf wb0.tipoPensEA = TPENS_VP Then  '20131129LC
              wa1.c1nvp_z = wa1.c1nvp_z + FRAZ_PNC * yd
              wa1.c1nvp_p = .ea_prev
              wa1.c1nvp_zm = (wa0.c1nvp_z * cp0 + wa1.c1nvp_z * cp1)  '20140312LC
              wa1.c1nvp_zp = wa1.c1nvp_zm * wa1.c1nvp_p
              Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPN_VP, wb0.stipoPensEA + 0, 5, xp, .ea_hrev, .ea_prev, _
                                     .ea_prev_fc, .ea_prev_fa1, FRAZ_PNC * yd, EFirr.Firr_E)  '20131202LC  'ex 20121230LC
            '--- 20131202LC (fine)
            ElseIf wb0.tipoPensEA = TPENS_AO Then  '20131129LC
              wa1.c1na_z = wa1.c1na_z + FRAZ_PNC * yd
              wa1.c1na_p = .ea_prev
              wa1.c1na_zm = (wa0.c1na_z * cp0 + wa1.c1na_z * cp1)  '20140312LC
              wa1.c1na_zp = wa1.c1na_zm * wa1.c1na_p
              Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPN_A, wb0.stipoPensEA + 0, 5, xp, .ea_hrev, .ea_prev, _
                                     .ea_prev_fc, .ea_prev_fa1, FRAZ_PNC * yd, EFirr.Firr_E)  '20131202LC  'ex 20121230LC
            '20120502LC (inizio)
            ElseIf Math.Abs(wb0.tipoPensEA) = TPENS_S Then
              If wb0.tipoPensEA = TPENS_S Then
                wa1.c1ns_z = wa1.c1ns_z + FRAZ_PNC * yd
                wa1.c1ns_p = .ea_prev
                wa1.c1ns_zm = (wa0.c1ns_z * cp0 + wa1.c1ns_z * cp1)  '20140312LC
                wa1.c1ns_zp = wa1.c1ns_zm * wa1.c1ns_p
                Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPN_S, wb0.stipoPensEA + 0, 5, xp, .ea_hrev, .ea_prev, _
                                       .ea_prev_fc, .ea_prev_fa1, FRAZ_PNC * yd, EFirr.Firr_E)  '20131202LC  'ex 20121230LC
              Else
                '.z1(t).z.yw1b = .z1(t).z.yw1b + FRAZ_PNC * yd
                wa1.w1b_z = wa1.w1b_z + FRAZ_PNC * yd
              End If
            '20120502LC (fine)
            ElseIf wb0.tipoPensEA = TPENS_T Then
              wa1.c1nt_z = wa1.c1nt_z + FRAZ_PNC * yd
              wa1.c1nt_p = .ea_prev
              wa1.c1nt_zm = (wa0.c1nt_z * cp0 + wa1.c1nt_z * cp1)  '20140312LC
              wa1.c1nt_zp = wa1.c1nt_zm * wa1.c1nt_p
              Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPN_T, wb0.stipoPensEA + 0, 5, xp, .ea_hrev, .ea_prev, _
                                     .ea_prev_fc, .ea_prev_fa1, FRAZ_PNC * yd, EFirr.Firr_E)  '20131202LC  'ex 20121230LC
            ElseIf .xan + 1 >= .cv4.Pveo98 Then '.st = "A"
Call MsgBox("TODO-TIscr2_MovPop_4a-6", , .mat)
Stop
              yd = wb0.eg2 * .qed1 * .coe0
              wa1.w3b_z = wa1.w3b_z + yd
              wa1.w2b_z = wa1.w2b_z + wb0.eg2 - yd
            ElseIf 1 = 2 And .xan + 1 < .cv4.Pveo98 Then '.st = "C" 'TODO?
Call MsgBox("TODO-TIscr2_MovPop_4a-6a", , .mat)
Stop
              yd = wb0.eg2 * .qed1
              'If iind = A1_RC Then
                wa1.w3b_z = wa1.w3b_z + yd
              'End If
              wb1.eg2 = wb1.eg2 - yd
            End If
          End If
          '20120221LC (fine)
          '.z1(t).z.yeg2(t1) = 0
          wb1.eg2 = 0
        End If
      End If
    '*****************************************
    'Pensionati diretti di inabilit� da attivi
    '*****************************************
    ElseIf iop = 5 Then
      wb1.bg2 = 0 '20121029LC (TODOTODOTODO)
      yd = wa0.c0a_z * .qab2 * fraz
      If yd > AA0 Then
        '20120619LC (inizio)
        Call TIscr2_PensDirInd1(dip, wb0, wb1, appl, .s(.t).pens1a1, .s(.t).pens1a2, fraz, .tau1, 0, etp.PINAB, 0) '20150504LC
        If .a_tpind = A1_PB1 Or (.a_tpind >= A1_PA1 And .a_tpind <= A1_PST) Then  '20161126LC
          '--- Popolazione  ---
          wb1.bg2 = yd
          wa1.c0a_z = wa1.c0a_z - yd
          wa1.c1b_z = wa1.c1b_z + wb1.bg2
          '--- Entrate ---
          '--- Uscite ---
          '--- 20181121LC-3 (inizio)
          'Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPN_B, 1, 1, xp, .s(.t).AnzTot+ 0.5 * .rr_dhEff * fraz, _
          '                       .a_pind, .a_pind_fc, .a_pind_fa1, yd, EFirr.Firr_A)  '20131202LC  'ex 20131202LC
          Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPN_B, 1, 1, xp, .s(.t).anz.h + 0.5 * .rr_dhEff * fraz, _
                                 .a_pind, .a_pind_fc, .a_pind_fa1, yd, EFirr.Firr_A)  '20131202LC  'ex 20131202LC
          '--- 20181121LC-3 (fine)
        ElseIf .a_tpind = A1_RC Then
          '--- Popolazione  ---
          wa1.w1b_z = wa1.w1b_z + yd
          wa1.c0a_z = wa1.c0a_z - yd
          '--- Entrate ---
          '--- Uscite ---
          'h0 = dip.s(.t).anz.h + 0.5  '20131123LC
          h0 = dip.s(.t).anz.h + AA5   '20140310LC  'riportare 20140111LC-27 (non dava problemi, solo per coerenza)
          bFig = TIscr2_bFig(dip, h0)  '20131123LC
          '20160124LC  'ex 20150512LC  'ex 20131123LC  'ex 20121127LC
          om = TIscr2_Montante(dip, appl, .t, -1, 1, False, bFig, 1 * 0#, omV())
          wa1.w1b_zp = yd * ARRV(om * .cv4.RcPrcCon) * OPZ_RC_W   '20120210LC
          wa1.w1b_zm = (wa1.w1b_z - wa0.w1b_z) * OPZ_RC_W
          If wa1.w1b_zm > 0 Then
            wa1.w1b_p = wa1.w1b_zp / wa1.w1b_zm
          Else
            wa1.w1b_p = 0
          End If
          '--- 20160614LC (inizio)  'FIRR  'adattato da 41I 20150118LC
          Call TIscr2_MovPop_Firr(dip, appl, yd, EFirr.Firr_E)  '7 di 8
          '--- 20160614LC (fine)  'FIRR  'adattato da 41I 20150118LC
        Else
          Call MsgBox("TODO-TIscr2_MovPop_4a-7", , .mat)
          Stop
        End If
        '20120619LC (inizio)
      End If
    '******************************************
    'Pensionati diretti di invalidit� da attivi
    '******************************************
    ElseIf iop = 6 Then
      wb1.ig2 = 0 '20121029LC (TODOTODOTODO)
      yd = wa0.c0a_z * .qai2 * fraz
      If yd > AA0 Then
        '20120619LC (inizio)
        Call TIscr2_PensDirInd1(dip, wb0, wb1, appl, .s(.t).pens1a1, .s(.t).pens1a2, fraz, .tau1, 0, etp.PINV, 0) '20150504LC
        If .a_tpind = A1_PI1 Or (.a_tpind >= A1_PA1 And .a_tpind <= A1_PST) Then  '20161126LC
          '--- Popolazione  ---
          wb1.ig2 = yd
          wa1.c0a_z = wa1.c0a_z - yd
          wa1.c1i_z = wa1.c1i_z + wb1.ig2
          '--- Entrate ---
          '--- Uscite ---
          '20150522LC (ripristinato) (c'era una istr. TEMPTEMP)
          'Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPC_VO, 1, 1, xp, .s(.t).AnzTot + 0.5 * .rr_dhEff * fraz, .a_pind, .a_pind_fc, .a_pind_fa1, yd)  '20131202LC  'ex 20121115LC
          '--- 20181121LC-3 (inizio)
          'Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPN_I, 1, 1, xp, .s(.t).AnzTot + 0.5 * .rr_dhEff * fraz, _
          '                       .a_pind, .a_pind_fc, .a_pind_fa1, yd, EFirr.Firr_A)  '20131202LC  'ex 20121115LC
          Call TIscr2_MovPop_4b2(dip, appl, .s(.t), TB_MPN_I, 1, 1, xp, .s(.t).anz.h + 0.5 * .rr_dhEff * fraz, _
                                 .a_pind, .a_pind_fc, .a_pind_fa1, yd, EFirr.Firr_A)  '20131202LC  'ex 20121115LC
          '--- 20181121LC-3 (fine)
        ElseIf .a_tpind = A1_RC Then
          '--- Popolazione  ---
          wa1.w1b_z = wa1.w1b_z + yd
          wa1.c0a_z = wa1.c0a_z - yd
          '--- Entrate ---
          '--- Uscite ---
          'h0 = dip.s(.t).anz.h + 0.5  '20131123LC
          h0 = dip.s(.t).anz.h + AA5   '20140310LC  'riportare 20140111LC-27 (non dava problemi, solo per coerenza)
          bFig = TIscr2_bFig(dip, h0) '20131123LC
          '20160124LC  'ex 20150512LC  'ex 20131123LC  'ex 20121127LC
          om = TIscr2_Montante(dip, appl, .t, -1, 1, False, bFig, 1 * 0#, omV())
          wa1.w1b_zp = yd * ARRV(om * .cv4.RcPrcCon) * OPZ_RC_W   '20120210LC
          wa1.w1b_zm = (wa1.w1b_z - wa0.w1b_z) * OPZ_RC_W
          If wa1.w1b_zm > 0 Then
            wa1.w1b_p = wa1.w1b_zp / wa1.w1b_zm
          Else
            wa1.w1b_p = 0
          End If
          '--- 20160614LC (inizio)  'FIRR  'adattato da 41I 20150118LC
          Call TIscr2_MovPop_Firr(dip, appl, yd, EFirr.Firr_E)  '8 di 8
          '--- 20160614LC (fine)  'FIRR  'adattato da 41I 20150118LC
        Else
          Call MsgBox("TODO-TIscr2_MovPop_4a-8", , .mat)
          Stop
        End If
        '20120619LC (fine)
      End If
    End If
  End With
End Sub


Public Sub TIscr2_MovPop_4b1(ByRef dip As TIscr2, ByRef appl As TAppl, ByRef tb1() As Double, _
                             ByRef s1 As TIscr2_Stato, ByRef tp%, ByRef gr0%, ByRef x0#, ByRef h0#, _
                             ByVal pens#, ByVal pens_fc#, ByVal pens_fa1#, ByRef yd#, ByVal bFirr As EFirr)
'*********************************************************************************************************************
'SCOPO
'  Crea un nuovo pensionato indiretto
'VERSIONI
'  20121115LC
'  20130101LC  byval pens,pens_fc,pens_fa1
'  20131202LC
'  20150521LC  tb1
'  20160609LC  bFirr
'*********************************************************************************************************************
  Dim t&
  Dim aSol#, ax#, cSol#, pens_dummy#  '20130102LC
  Dim lp As TIscr2pens
  
'--- 20160420LC (inizio)  'ex 20160411LC
If pens <= OPZ_INT_TOLL_PENS Then
  If gr0 >= 2 And gr0 <= 4 Then
  Else
    Call MsgBox("TODO-TIscr2_MovPop_4b1", , dip.mat)
    Stop
  End If
End If
'--- 20160420LC (fine)
  If yd <= AA0 Then
    Exit Sub
  End If
  '--- 20160312LC (inizio)
  If pens > 0 And OPZ_INT_PENSIONE > 0 Then
    pens = OPZ_INT_PENSIONE
    pens_fc = 0#
    pens_fa1 = 0#
  End If
  '--- 20160312LC (fine)
  With dip
    t = .tau1
    If tp >= 0 Then
      Call MsgBox("Situazione inattesa 1 in TIscr2_MovPop_4b1", , .mat)
      Stop
    End If
    With lp
      .tp = tp
      '--- 20121031LC (inizio) spostato
      '--- 20140325LC (inizio) 'ex 20131122LC 'ex 20121031LC (spostato)
      If .tp = TB_MPC_VO Then
        .kzm = TB_SPC_VO
        .kzp = TB_MPC_VO
      ElseIf .tp = TB_MPC_VA Then
        .kzm = TB_SPC_VA
        .kzp = TB_MPC_VA
      ElseIf .tp = TB_MPC_VP Then
        .kzm = TB_SPC_VP
        .kzp = TB_MPC_VP
      ElseIf .tp = TB_MPC_A Then
        .kzm = TB_SPC_A
        .kzp = TB_MPC_A
      ElseIf .tp = TB_MPC_S Then
        .kzm = TB_SPC_S
        .kzp = TB_MPC_S
      ElseIf .tp = TB_MPC_T Then
        .kzm = TB_SPC_T
        .kzp = TB_MPC_T
      '--- 20140325LC (fine)
      ElseIf .tp = TB_MPN_VO Then
        .kzm = TB_SPN_VO
        .kzp = TB_MPN_VO
      ElseIf .tp = TB_MPN_VA Then
        .kzm = TB_SPN_VA
        .kzp = TB_MPN_VA
      ElseIf .tp = TB_MPN_VP Then
        .kzm = TB_SPN_VP
        .kzp = TB_MPN_VP
      '--- 20131202LC (fine)
      ElseIf .tp = TB_MPN_A Then
        .kzm = TB_SPN_A
        .kzp = TB_MPN_A
      ElseIf .tp = TB_MPN_S Then
        .kzm = TB_SPN_S
        .kzp = TB_MPN_S
      ElseIf .tp = TB_MPN_T Then
        .kzm = TB_SPN_T
        .kzp = TB_MPN_T
      ElseIf .tp = TB_MPN_B Then
        .kzm = TB_SPN_B
        .kzp = TB_MPN_B
      ElseIf .tp = TB_MPN_I Then
        .kzm = TB_SPN_I
        .kzp = TB_MPN_I
      ElseIf .tp < 0 Then 'Indiretti
        '--- 20140325LC (inizio)
        '.kzm = TB_SPN_A
        '.kzp = TB_MPN_A
        .kzm = TB_SPS
        .kzp = TB_MPS
        '--- 20140325LC (fine)
      Else
        Call MsgBox("TODO 4b1")
        Stop
      End If
      '--- 20131122LC (fine)
      .gr0 = gr0
      .t0 = dip.t
      .x0 = x0
      '--- 20181121LC-1 (inizio)
      .h0a = h0
      .h0b = h0
      '--- 20181121LC-1 (fine)
      ReDim .tb(0)
      With .tb(0)
        .p = pens
        '--- 20131202LC (inizio)
        'If OPZ_INT_ASSIST_MODEL >= 1 Then
          .fa1 = pens_fa1
          .fa2 = 0#
          If dip.Pfig > 0 Then
            If pens <= 0 Then  'mat=184130 ha pens=0 (EA, art.20/40)
              Call MsgBox("TODO TIscr2_MovPop_4b1 4", , dip.mat)
              Stop
            End If
            .fa2 = dip.Pfig / pens
            If .fa2 > pens_fc Then
              Call MsgBox("TODO TIscr2_MovPop_4b1 5", , dip.mat)
              Stop
            End If
          Else
            .fa2 = 0
          End If
        'End If
        '--- 20131202LC (fine)
        .z = yd
        .zm = yd * (1 - OPZ_PENS_C0) '20140312LC
        '--- 20161116LC (inizio)
        If OPZ_PENS_C0 = 1 And OPZ_PENS_SP0 > 0 Then
          If dip.gruppo00 = 1 Or dip.gruppo00 = 5 Or dip.gruppo00 = 6 Then
            .zm = yd * OPZ_PENS_SP0
          End If
        End If
        '--- 20161116LC (fine)
        .zp = .zm * .p
        '--- 20121115LC (inizio)  'ex 20120514LC
        .fc = pens_fc
        .zcs = 0#
        aSol = TIScr2_aSol(dip, s1, lp, .p, .fc, 1)  '20150410LC  'ex 20121230LC
        If aSol > AA0 Then
          cSol = ARRV(.p * aSol)
          .zcs = .zm * cSol
        End If
        '--- 20121115LC (fine)
        '--- Superstiti iniziali ---
        If dip.gruppo0 = 4 Then
          Call TIscr2_MovPop_Sup4(dip, appl, appl.pfam4, tb1, 0, .z, .p)  '20140425LC
        '--- Superstiti del pensionato indiretto ---
        ElseIf appl.bProb_Superst = True Then  '20121109LC
If t <> dip.t - appl.t0 Then
Call MsgBox("TODO TIscr2_MovPop_4b1", , dip.mat)
Stop
End If
          Call TIscr2_MovPop_Sup1b(dip, appl, tb1, .z, .p, 2) '20080919LC
        ElseIf yd > 0 Then
          dip.wa(t).w3b_z = dip.wa(t).w3b_z + yd
        End If
      End With
    End With
    '--- 20160609LC (inizio)  'FIRR  'adattato da 41I 20150118LC
    Call TIscr2_MovPop_Firr_Sup(dip, appl, yd, bFirr)
    '--- 20160609LC (fine)  'FIRR  'adattato da 41I 20150118LC
  End With
End Sub


Public Sub TIscr2_MovPop_4b2(ByRef dip As TIscr2, ByRef appl As TAppl, ByRef s1 As TIscr2_Stato, ByRef tp%, _
                             ByRef stp%, ByRef gr0%, ByRef x0#, ByRef h0#, ByVal pens#, ByVal pens_fc#, _
                             ByVal pens_fa1#, ByRef yd#, ByVal bFirr As EFirr)
'*********************************************************************************************************************
'SCOPO
'  Crea un nuovo pensionato diretto
'VERSIONI
'  20121115LC
'  20130101LC  byval pens,pens_fc,pens_fa1
'  20131202LC
'  20160609LC  bFirr
'*********************************************************************************************************************
  Const sep As String = ";"  '20160420LC  'ex 20130609LC
  Dim bScriviFile As Byte    '20140424LC
  Dim napa%  '20150410LC
  Dim j&  '20130221LC
  Dim aSol#, ax#, cSol#, pens_dummy#, y#  '20130102LC
  Dim s$  '20130609LC
  
'20160420LC (inizio)  'ex 20160411LC
If pens <= OPZ_INT_TOLL_PENS Then
  If gr0 >= 2 And gr0 <= 4 Then
  Else
    Call MsgBox("TODO-TIscr2_MovPop_4b2", , dip.mat)
    Stop
  End If
End If
'20160420LC (fine)
  With dip
    If yd <= AA0 Then
      Exit Sub
    End If
    '--- 20140424LC (inizio)  'ex 20140422LC
    bScriviFile = 0
    '--- 20160312LC (inizio)
    If pens > 0 And OPZ_INT_PENSIONE > 0 Then
      pens = ARRV(OPZ_INT_PENSIONE * (1 + appl.CoeffVar(dip.t, ecv.cv_Tp1Al)))
      pens_fc = 0#
      pens_fa1 = 0#
    End If
    '--- 20160312LC (fine)
    '--- 20130604LC (inizio)
    If OPZ_INT_PMIN_DEBUG >= 1 Then
      If OPZ_INT_PMIN_DEBUG >= 2 And .bPmin <= 5 Then
        Exit Sub
      Else
        bScriviFile = 2
      End If
    '--- 20160420LC (inizio)
    ElseIf OPZ_INT_PENS_DEBUG = 1 Then
      bScriviFile = 3
    '--- 20160420LC (fine)
    End If
    '--- 20130604LC (fine)
    '--- 20140424LC (inizio)
    If bScriviFile > 0 Then
      '--- 20160420LC (inizio)
      If bScriviFile = 3 Then
        s = .mat & sep & .t & sep & x0 & sep & h0 & sep & tp & sep & stp & sep & gr0 & sep & yd & sep & pens
        s = s & sep & pens_fc & sep & .Pens1r & sep & .Pens1C & sep & .ax & sep & .ax0
        s = Replace(s, ".", ",")
        Print #99, s
      Else
      '--- 20160420LC (fine)
        s = .mat & sep & .t & sep & x0 & sep & h0 & sep & tp & sep & stp & sep & gr0 & sep & yd & sep & pens
        s = s & sep & pens_fc & sep & .Pens1r & sep & .Pens1C & sep & .ax & sep & .ax0 & sep & .bPmin & sep & .nAbbArt25
        s = s & sep & .nRMPmin & sep & .RMPmin & sep & .PminBase & sep & .PMinAbb & sep & .PMinCalc & sep & .PSost
        s = Replace(s, ".", ",")
        Print #99, s
        If bScriviFile = 2 Then
          Exit Sub
        End If
      End If
    End If
    '--- 20140424LC (fine)
    If tp <= 0 Then
      Call MsgBox("Situazione inattesa 1 in TIscr2_MovPop_4b2", , .mat)
      Stop
    End If
    ReDim Preserve .lp(.nPens)
    With .lp(.nPens)
      .tp = tp
      '--- 20140325LC (inizio) 'ex 20131122LC  'ex 20121031LC (spostato)
      If .tp = TB_MPC_VO Then
        .kzm = TB_SPC_VO
        .kzp = TB_MPC_VO
      ElseIf .tp = TB_MPC_VA Then
        .kzm = TB_SPC_VA
        .kzp = TB_MPC_VA
      ElseIf .tp = TB_MPC_VP Then
        .kzm = TB_SPC_VP
        .kzp = TB_MPC_VP
      ElseIf .tp = TB_MPC_A Then
        .kzm = TB_SPC_A
        .kzp = TB_MPC_A
      ElseIf .tp = TB_MPC_S Then
        .kzm = TB_SPC_S
        .kzp = TB_MPC_S
      ElseIf .tp = TB_MPC_T Then
        .kzm = TB_SPC_T
        .kzp = TB_MPC_T
      '--- 20140325LC (fine)
      ElseIf .tp = TB_MPN_VO Then
        .kzm = TB_SPN_VO
        .kzp = TB_MPN_VO
      ElseIf .tp = TB_MPN_VA Then
        .kzm = TB_SPN_VA
        .kzp = TB_MPN_VA
      ElseIf .tp = TB_MPN_VP Then
        .kzm = TB_SPN_VP
        .kzp = TB_MPN_VP
      '--- 20131202LC (fine)
      ElseIf .tp = TB_MPN_A Then
        .kzm = TB_SPN_A
        .kzp = TB_MPN_A
      ElseIf .tp = TB_MPN_S Then
        .kzm = TB_SPN_S
        .kzp = TB_MPN_S
      ElseIf .tp = TB_MPN_T Then
        .kzm = TB_SPN_T
        .kzp = TB_MPN_T
      ElseIf .tp = TB_MPN_B Then
        .kzm = TB_SPN_B
        .kzp = TB_MPN_B
      ElseIf .tp = TB_MPN_I Then
        .kzm = TB_SPN_I
        .kzp = TB_MPN_I
      ElseIf .tp < 0 Then 'Indiretti
Call MsgBox("TODO")
Stop
        .kzm = TB_SPN_A
        .kzp = TB_MPN_A
      Else
        Call MsgBox("TODO 4b2")
        Stop
      End If
      '--- 20131122LC (fine)
      .stp = stp '20120531LC
If .stp = 0 Then
'call MsgBox("TODO"), , dip.mat)
'Stop
End If
      '--- 20160609LC (inizio)
      If bFirr <> EFirr.Firr_A Then
        bFirr = EFirr.Firr_None
      End If
      '--- 20160609LC (fine)
      .gr0 = gr0
      .t0 = dip.t
      '20080918LC (inizio)
      If .gr0 = 2 Then
        .t1 = dip.t - s1.tapa
      '--- 20140307LC (inizio)  'riportare 20140111LC-24
      ElseIf .gr0 = 3 Or .gr0 = 4 Then
        .t1 = dip.t
      '--- 20140307LC (fine)  'riportare 20140111LC-24
      ElseIf (.tp >= TB_MPC_VO And .tp <= TB_MPC_T) Then  '20140325LC
        .t1 = dip.t
        '--- 20150609LC (inizio)
        'dip.PensAUS = dip.t  'pericoloso (vedi Aggiorna_Redd1)
        napa = appl.CoeffVar(dip.t, ecv.cv_SpAnni)
        If napa < 72 - dip.t + Year(dip.DNasc) Then
          napa = 72 - dip.t + Year(dip.DNasc)
        End If
        '--- 20150609LC (fine)
        '--- 20150410LC (inizio)
        If Int(dip.xr + 1) + napa > appl.CoeffVar(dip.t, ecv.cv_SpEtaMax) Then  '20150522LC (TODOTODO: il programma precedente non testava sm1)
          s1.napa = 0
        Else
          's1.napa = IIf(dip.t < 2004 And dip.t < 2004, 2, appl.parm(P_cvSpAnni))
          s1.napa = napa
        End If
        '--- 20150410LC (fine)
        s1.tapa = 0
        s1.bPensAtt = (s1.bPensAtt = True) Or (s1.napa > 0) '20121009LC
        dip.s(dip.t).om_rcV(om_pens_att) = 0 '20120117LC
        '--- 20160609LC (inizio)
        dip.FirrU = dip.om_rcU(om_misto_int)
        dip.FirrV = dip.om_rcV(om_misto_int)
        '--- 20160609LC (fine)
        ReDim dip.om_rcU(eom.om_max)  '20131122LC  'ex 20120114LC
        ReDim dip.om_rcV(eom.om_max)  '20131122LC  'ex 20120114LC
        '--- 20160609LC (inizio)
        If s1.napa > 0 Then  '20160611LC
          bFirr = EFirr.Firr_None
        End If
        dip.om_rcU(om_misto_int) = dip.FirrU
        dip.om_rcV(om_misto_int) = dip.FirrV
        '--- 20160609LC (fine)
      Else
        .t1 = 0
      End If
      '20080918LC (fine)
      .x0 = x0
      '--- 20181121LC-1 (inizio)
      .h0a = h0
      If .tp >= TB_MPC_VO And .tp <= TB_MPC_T Then
        .h0b = dip.s(dip.t).anz.h
      Else
        .h0b = h0
      End If
      '--- 20181121LC-1 (fine)
      'ReDim .tb(120 - Int(.x0))
      ReDim .tb(OPZ_OMEGA + 1 - Int(.x0))
      With .tb(0)
        .p = pens
        '--- 20131202LC (inizio)
        'If OPZ_INT_ASSIST_MODEL >= 1 Then
          .fa1 = pens_fa1
          '---
          .fa2 = 0#
          If dip.Pfig > 0 Then
            If pens <= 0 Then  'mat=184130 ha pens=0 (EA, art.20/40)
              Call MsgBox("TODO TIscr2_MovPop_4b2 4", , dip.mat)
              Stop
            End If
            .fa2 = dip.Pfig / pens
            If .fa2 > pens_fc Then
              Call MsgBox("TODO TIscr2_MovPop_4b2 5", , dip.mat)
              Stop
            End If
          Else
            .fa2 = 0
          End If
        'End If
        '--- 20131202LC (fine)
        .z = yd
        .zm = yd * (1 - OPZ_PENS_C0) '20140312LC
        '--- 20161116LC (inizio)
        If OPZ_PENS_C0 = 1 And OPZ_PENS_SP0 > 0 Then
          If dip.gruppo00 = 1 Or dip.gruppo00 = 5 Or dip.gruppo00 = 6 Then
            .zm = yd * OPZ_PENS_SP0
          End If
        End If
        '--- 20161116LC (fine)
        .zp = .zm * .p
        '--- 20121115LC (inizio)  'ex 20120514LC
        .fc = pens_fc
        .zcs = 0#
        aSol = TIScr2_aSol(dip, s1, dip.lp(dip.nPens), .p, .fc, 0)  '20150410LC  'ex 20121230LC  'ex 20120601LC
        If aSol > AA0 Then
          cSol = ARRV(.p * aSol)
          .zcs = .zm * cSol
        End If
        '--- 20121115LC (fine)
      End With
    End With
    '--- 20160609LC (inizio)  'FIRR  'adattato da 41I 20150118LC
    If bFirr <> EFirr.Firr_None Then
      Call TIscr2_MovPop_Firr(dip, appl, .lp(.nPens).tb(0).z, bFirr)
    End If
    '--- 20160609LC (fine)  'FIRR  'adattato da 41I 20150118LC
    .nPens = .nPens + 1
  End With
  '20120601LC (inizio)
  If OPZ_DEBUG = 1 Then
    Dim fo&
    fo = FreeFile
    Open "afp_debug.txt" For Append As #fo
    With dip
      Print #fo, .mat;
      With .lp(.nPens - 1)
        Print #fo, ";"; .tp;
        Print #fo, ";"; .stp;
        Print #fo, ";"; .gr0;
        Print #fo, ";"; .t0;
        Print #fo, ";"; .t1;
        Print #fo, ";"; .x0;
        Print #fo, ";"; .h0a;  '20181121LC-1
        With .tb(0)
          Print #fo, ";"; .p;
          Print #fo, ";"; .z;
          Print #fo, ";"; .zcs;
          Print #fo, ";"; .zm;
          Print #fo, ";"; .zp;
        End With
      End With
      Print #fo, ";"; .nPens
    End With
    Close #fo
  End If
  '20120601LC (fine)
End Sub


Public Sub TIscr2_MovPop_4c(ByRef dip As TIscr2, ByRef appl As TAppl, s0 As TIscr2_Stato, s1 As TIscr2_Stato, tb1#(), t&)
'****************************************************************************************************
'Muove i pensionati
'20080918LC
'20140414LC
'20150410LC
'****************************************************************************************************
  Dim bContrA As Boolean  '20140414LC
  Dim bPensAttNonAtt As Boolean  '20140325LC
  'Dim anno&, i&, ia&, ijk&, ip&, jp&, jj&, kpa&  '20150322LC  'ex 20140319LC-03
  Dim ip&, ijk&, jp&
  Dim aSol#, cSol#, dPens#, y1#, y2#, yd#  '20140325LC
  Dim MefX&  '20181108LC
  Dim hOut#  '20181121LC-1
  
  With dip
    '--- 20181108LC (inizio) CONSOLIDAMENTO MEF 2A
    If OPZ_DBXMEF <> 0 Then
      MefX = .anno - Year(.DNasc)
    End If
    '--- 20181108LC (fine) CONSOLIDAMENTO MEF 2A
    '--- 20140414LC (inizio)
    If s0.stato = ST_A Then
      bContrA = True
    Else
      bContrA = False
    End If
    '--- 20140414LC (fine)
    For ip = 0 To .nPens - 1
      With .lp(ip)
If .tp <= 0 Then
Call MsgBox("Situazione inattesa 1 in TIscr2_MovPop_4c", , dip.mat)
Stop
End If
'.tp > 0 'Diretti
        jp = dip.t - .t0
        If jp <= UBound(.tb, 1) Then
          '20080918LC (inizio)
          If jp > 0 Then
            .tb(jp).fc = .tb(jp - 1).fc  '20121222LC
            '--- 20131122LC (inizio)
            'If OPZ_INT_ASSIST_MODEL >= 1 Then
              .tb(jp).fa1 = .tb(jp - 1).fa1
              .tb(jp).fa2 = .tb(jp - 1).fa2  '20131126LC
            'End If
            '--- 20131122LC (fine)
            '--- 20150410LC (inizio)
            dPens = TIscr2_MovPop_4c_dPens_Calcola(dip.lp(ip), dip, appl, s0, s1, tb1(), t)
            'If (OPZ_PENS_C0 = 1 And Math.Abs(OPZ_INT_COMP) = COMP_ENAS) Then
            If OPZ_PENS_MODEL = 1 Then  '20161126LC
              .tb(jp).p = .tb(jp - 1).p  'rivalutazine a fine anno
            Else
              '20120213LC
              '.tb(jp).p = .tb(jp - 1).p * (1 + appl.parm(P_cvTp1Al))
              '.tb(jp).p = .tb(jp - 1).p * (1 + appl.CoeffVar(.t, ecv.cv_Tp1Al))
              '.tb(jp).p = TIscr2_Pens1(appl.cv(.t), .tb(jp - 1).p)
              .tb(jp).p = TIscr2_Pens(appl.CoeffVar, dip.t, .tb(jp - 1).p)
              Call TIscr2_MovPop_4c_dPens_Aggiorna(dip.lp(ip), dip, appl, s0, s1, tb1(), t, dPens)
            End If
            '--- 20150410LC (fine)
            '20080918LC (fine)
          End If  '//If jp > 0 Then
          '--- Popolazione  ---
          If jp <= 0 Then
            yd = 0#
          Else
            If .tp = TB_MPN_B Then
              yd = .tb(jp - 1).z * dip.qbd 'TODOTODOTODO
            ElseIf .tp = TB_MPN_I Then
              yd = .tb(jp - 1).z * dip.qid
            Else
              yd = .tb(jp - 1).z * dip.qpnd
            End If
            .tb(jp).z = .tb(jp - 1).z
            If yd > AA0 Then
              .tb(jp).z = .tb(jp).z - yd
            End If
          End If
          '--- Spesa pensionistica
          If jp > 0 Then
            .tb(jp).zm = (.tb(jp - 1).z + .tb(jp).z) * 0.5
            .tb(jp).zp = .tb(jp).zm * .tb(jp).p
            '20120514LC (inizio)
            .tb(jp).zcs = 0#
            aSol = TIScr2_aSol(dip, s1, dip.lp(ip), .tb(jp).p, .tb(jp).fc, 0)  '20150410LC  'ex 20121230LC  'ex 20121115LC  'ex 20120601LC
            If aSol > AA0 Then
              cSol = ARRV(.tb(jp).p * aSol)
              .tb(jp).zcs = .tb(jp).zm * cSol
            End If
            '20120514LC (fine)
          End If
          '--- Uscite ---
'--- CONSOLIDAMENTO 2 (inizio) ---
          If OPZ_TB = 1 Then
            ijk = appl.nSize2 * (dip.iTS + 1) + appl.nSize3 * (dip.iQ + 1) + appl.nSize4 * (t + appl.t0 - 1960) '20121114LC '3
          Else
            'ijk = dip.iSQ + appl.nSize4 * (t + appl.t0 - 1960) '20121114LC '3
            ijk = dip.iSQT '3
          End If
          '--- 20140321LC (inizio)  'ex 20140321LC
          If .tp >= TB_MPC_VO And .tp <= TB_MPC_T And s1.napa = 0 Then
            y1 = .tb(jp).zp * (1 - OPZ_PENS_C0)
            tb1(ijk + .kzm) = tb1(ijk + .kzm) + .tb(jp).zm * (1 - OPZ_PENS_C0)
            tb1(ijk + .kzp) = tb1(ijk + .kzp) + y1
            tb1(ijk + TB_PASS1) = tb1(ijk + TB_PASS1) + y1 * .tb(jp).fa1
            tb1(ijk + TB_PASS2) = tb1(ijk + TB_PASS2) + y1 * .tb(jp).fa2
            '--- 20140414LC (inizio)
            If 1 = 1 Or bContrA = False Then
              tb1(ijk + TB_PA_ECSP) = tb1(ijk + TB_PA_ECSP) + .tb(jp).zcs * (1 - OPZ_PENS_C0)
            Else
              tb1(ijk + TB_AA_ECSP) = tb1(ijk + TB_AA_ECSP) + .tb(jp).zcs * (1 - OPZ_PENS_C0)
            End If
            '--- 20140414LC (fine)
            .tp = .tp + TB_ZPN_VO - TB_ZPC_VO
            .kzm = .kzm + TB_ZPN_VO - TB_ZPC_VO
            .kzp = .kzp + TB_ZPN_VO - TB_ZPC_VO
            y2 = .tb(jp).zp * OPZ_PENS_C0
            tb1(ijk + .kzm) = tb1(ijk + .kzm) + .tb(jp).zm * OPZ_PENS_C0
            tb1(ijk + .kzp) = tb1(ijk + .kzp) + y2
            tb1(ijk + TB_PASS1) = tb1(ijk + TB_PASS1) + y2 * .tb(jp).fa1
            tb1(ijk + TB_PASS2) = tb1(ijk + TB_PASS2) + y2 * .tb(jp).fa2
            '--- 20140414LC (inizio)
            If 1 = 1 Or bContrA = False Then
              tb1(ijk + TB_PA_ECSP) = tb1(ijk + TB_PA_ECSP) + .tb(jp).zcs * OPZ_PENS_C0
            Else
              tb1(ijk + TB_AA_ECSP) = tb1(ijk + TB_AA_ECSP) + .tb(jp).zcs * OPZ_PENS_C0
            End If
            '--- 20140414LC (fine)
            '--- 20181108LC (inizio) CONSOLIDAMENTO MEF 2B
            If OPZ_DBXMEF <> 0 Then
              '--- 20181121LC-1 (inizio)  hOut
              .h0b = .h0b + 0.5  'ex 20151128LC (.h0 = .h0 + 0.5)
              hOut = .h0b - AA0
              Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_pa_pn, EMef.Mef_PosFin, t, .tb(jp).z, OPZ_DBXMEF) 'MEF31
              Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_pa_pn, EMef.Mef_PosAnz, t, .tb(jp).z * hOut, OPZ_DBXMEF)  'MEF31
              Call MEF2(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_pa_pa, t, y1, .tb(jp).z * .tb(jp).p)  'MEF21
              Call MEF2(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_pa_pn, t, y2, .tb(jp).z * .tb(jp).p)  'MEF31
              '--- 20181121LC-1 (fine)  hOut
            End If
            '--- 20181108LC (fine) CONSOLIDAMENTO MEF 2B
          Else
            tb1(ijk + .kzm) = tb1(ijk + .kzm) + .tb(jp).zm
            tb1(ijk + .kzp) = tb1(ijk + .kzp) + .tb(jp).zp
            tb1(ijk + TB_PASS1) = tb1(ijk + TB_PASS1) + .tb(jp).zp * .tb(jp).fa1
            tb1(ijk + TB_PASS2) = tb1(ijk + TB_PASS2) + .tb(jp).zp * .tb(jp).fa2  '20131126LC
            '--- 20140414LC (inizio)
            If 1 = 1 Or bContrA = False Then
              tb1(ijk + TB_PA_ECSP) = tb1(ijk + TB_PA_ECSP) + .tb(jp).zcs
            Else
              tb1(ijk + TB_AA_ECSP) = tb1(ijk + TB_AA_ECSP) + .tb(jp).zcs '20120514LC
            End If
            '--- 20140414LC (fine)
            '--- 20181108LC (inizio) CONSOLIDAMENTO MEF 2C
            If OPZ_DBXMEF <> 0 Then
              '--- 20151127LC-02 (inizio)
              If (.tp >= TB_MPC_VO And .tp <= TB_MPC_U) Then  '20151126LC
                '--- 20181121LC-1 (inizio)  hOut
                .h0b = .h0b + 1
                hOut = .h0b - AA0
                If .t0 = dip.anno Then
                  Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_a_pa, EMef.Mef_PosFin, t, .tb(jp).z, OPZ_DBXMEF)          'MEF21
                  Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_a_pa, EMef.Mef_PosAnz, t, .tb(jp).z * hOut, OPZ_DBXMEF) 'MEF21
                  Call MEF2(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_a_pa, t, .tb(jp).zp, .tb(jp).z * .tb(jp).p)   'MEF21
                Else
                  '.h0 = .h0 + 1  'commentata
                  Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_pa_pa, EMef.Mef_PosFin, t, .tb(jp).z, OPZ_DBXMEF)         'MEF21
                  Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_pa_pa, EMef.Mef_PosAnz, t, .tb(jp).z * hOut, OPZ_DBXMEF) 'MEF21
                  Call MEF2(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_pa_pa, t, .tb(jp).zp, .tb(jp).z * .tb(jp).p)  'MEF21
                End If
                '--- 20181121LC-1 (fine)  hOut
              '--- 20151127LC-02 (fine)
              Else
                '--- 20181121LC-1 (inizio)  hOut
                hOut = .h0b - AA0
                If .kzm = TB_SPN_B Then
                  If .t0 = dip.anno Then
                    Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_a_pb, EMef.Mef_PosFin, t, .tb(jp).z, OPZ_DBXMEF)          'MEF32
                    Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_a_pb, EMef.Mef_PosAnz, t, .tb(jp).z * hOut, OPZ_DBXMEF)    'MEF32
                    Call MEF2(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_a_pb, t, .tb(jp).zp, .tb(jp).z * .tb(jp).p)   'MEF32
                  Else
                    Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_pb_pb, EMef.Mef_PosFin, t, .tb(jp).z, OPZ_DBXMEF)         'MEF32
                    Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_pb_pb, EMef.Mef_PosAnz, t, .tb(jp).z * hOut, OPZ_DBXMEF)   'MEF32
                    Call MEF2(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_pb_pb, t, .tb(jp).zp, .tb(jp).z * .tb(jp).p)  'MEF32
                  End If
                ElseIf .kzm = TB_SPN_I Then
                  If .t0 = dip.anno Then
                    Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_a_pi, EMef.Mef_PosFin, t, .tb(jp).z, OPZ_DBXMEF)          'MEF33
                    Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_a_pi, EMef.Mef_PosAnz, t, .tb(jp).z * hOut, OPZ_DBXMEF)    'MEF33
                    Call MEF2(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_a_pi, t, .tb(jp).zp, .tb(jp).z * .tb(jp).p)   'MEF33
                  Else
                    Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_pi_pi, EMef.Mef_PosFin, t, .tb(jp).z, OPZ_DBXMEF)         'MEF33
                    Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_pi_pi, EMef.Mef_PosAnz, t, .tb(jp).z * hOut, OPZ_DBXMEF)   'MEF33
                    Call MEF2(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_pi_pi, t, .tb(jp).zp, .tb(jp).z * .tb(jp).p)  'MEF33
                  End If
                Else
                  If .t0 = dip.anno Then
                    Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_a_pn, EMef.Mef_PosFin, t, .tb(jp).z, OPZ_DBXMEF)          'MEF31
                    Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_a_pn, EMef.Mef_PosAnz, t, .tb(jp).z * hOut, OPZ_DBXMEF)    'MEF31
                    Call MEF2(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_a_pn, t, .tb(jp).zp, .tb(jp).z * .tb(jp).p)   'MEF31
                  Else
                    Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_pn_pn, EMef.Mef_PosFin, t, .tb(jp).z, OPZ_DBXMEF)         'MEF31
                    Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_pn_pn, EMef.Mef_PosAnz, t, .tb(jp).z * hOut, OPZ_DBXMEF)   'MEF31
                    Call MEF2(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_pn_pn, t, .tb(jp).zp, .tb(jp).z * .tb(jp).p)  'MEF31
                  End If
                End If
                '--- 20181121LC-1 (fine)  hOut
              End If
            End If
            '--- 20181108LC (fine) CONSOLIDAMENTO MEF 2C
          End If
'--- CONSOLIDAMENTO 2 (fine) ---
          '--- 20140325LC (fine)
          '--- Superstiti dei pensionati ---
          If appl.bProb_Superst = True Then  '20121109LC
            If jp = 1 And 1 = 2 Then  '20130304LC
              'Call TIscr2_MovPop_Sup1(dip, appl, t, yd, .tb(jp).p)
              '201111230LC - TODOTODO: aggiungere dPens?
              Call TIscr2_MovPop_Sup1b(dip, appl, tb1, yd, .tb(jp).p, 2)  '20080919LC
            Else
              'Call TIscr2_MovPop_Sup1(dip, appl, t, yd, .tb(jp).p + dpens)
              Call TIscr2_MovPop_Sup1b(dip, appl, tb1, yd, .tb(jp).p + dPens, 2)  '20080919LC
            End If
          ElseIf yd > AA0 Then '20121115LC
            dip.wa(t).w3b_z = dip.wa(t).w3b_z + yd
          End If
          '--- 20160609LC (inizio)  'FIRR  'adattato da 41I 20150118LC
          If s1.napa > 0 Then
            Call TIscr2_MovPop_Firr_Sup(dip, appl, yd, EFirr.Firr_PA)
          End If
          '--- 20160609LC (fine)  'FIRR  'adattato da 41I 20150118LC
          '--- 20150410LC (inizio)
          'If (OPZ_PENS_C0 = 1 And Math.Abs(OPZ_INT_COMP) = COMP_ENAS) And jp > 0 Then
          If OPZ_PENS_MODEL = 1 And jp > 0 Then  '20161126LC
            .tb(jp).p = TIscr2_Pens(appl.CoeffVar, dip.t, .tb(jp - 1).p)
            Call TIscr2_MovPop_4c_dPens_Aggiorna(dip.lp(ip), dip, appl, s0, s1, tb1(), t, dPens)
          End If
          '--- 20150410LC (fine)
        ElseIf jp - 1 = UBound(.tb, 1) Then
          yd = .tb(jp - 1).z
          dip.wa(t).w3b_z = dip.wa(t).w3b_z + yd
        Else
          yd = 0#
        End If
        '20080918LC (fine)
      End With  '//With .lp(ip)
    Next ip  '//For ip = 0 To .nPens - 1
    '#######################################################################
  End With
End Sub


Private Function TIscr2_MovPop_4c_dPens_Calcola(tp As TIscr2pens, dip As TIscr2, ByRef appl As TAppl, s0 As TIscr2_Stato, s1 As TIscr2_Stato, tb1#(), t&) As Double
'20150410LC  nuova (da TIscr2_MovPop_4c)
  Dim anno&, i&, ia&, jj&, kpa&
  Dim ax#, dPens#, fact#, pens1a#, pens1a1#, pens1a2#, xp#, y1a#, y1s#, ya#, ys#
    
  With tp
    dPens = 0#
    xp = dip.xr + dip.coe0 * IIf(9 = 9, 1, 0) '20111230LC
    If .gr0 = 2 Or (.tp >= TB_MPC_VO And .tp <= TB_MPC_T) Then  '20140325LC
      If s1.napa > 0 Then
        s1.tapa = dip.t - .t1
          '--- 20121224LC (inizio)
'20150512LC (commentato)
'If appl.parm(P_cvSpPrcRiv) <> 1 Then
'call MsgBox("TODO TIscr_Aggiorna_MovPop_4c 1", , dip.mat)
'Stop
'End If
        ya = 0#
        ys = 0#
        y1a = 0#
        y1s = 0#
        'N.B. Il montante si calcola rivalutando (A:acconto, S:saldo)
        'A(T-4)*[1+R(T)]*[1+R(T-1)]*[1+R(T-2)] +S(T-4)*[1+R(T)]*[1+R(T-1)]
        'A(T-3)*[1+R(T)]*[1+R(T-1)]            +S(T-3)*[1+R(T)]
        'A(T-2)*[1+R(T)]                       +S(T-2)
        'A(T-1)                                +S(T-1)
        'A(T-0)                                +S(T-0)
        '--- 20150322LC (inizio)
        i = eom.om_pens_att
        jj = TIscr2_Mont_Prd(i + 0)
        y1a = dip.om_rcV(i) * appl.CoeffVar(dip.t, jj)
        y1s = 0#
        '--- 20150322LC (fine)
        '--- 20140404LC (inizio) 20140319LC-03
        If OPZ_MISTO_CCR_MODEL >= 2 Then
          kpa = 1 + lmin(t, lmax(Year(dip.DNasc) - 1948, 0))
        ElseIf OPZ_MISTO_CCR_MODEL >= 1 Then   '20140404LC
          kpa = 1 + lmax(Year(dip.DNasc) - 1948, 0)
        Else
          kpa = t
        End If
        ax = AX1(appl.CoeffPA(), dip.sesso, xp, kpa)
'--- 20150421LC (inizio) TEMPTEMP
'If Math.Abs(OPZ_INT_COMP) = COMP_ENAS Then
'  If ax <> modEneaSwa.CCR(dip.t, xp, 1) / 100# Then
'    call MsgBox("TODO")
'    Stop
'  End If
'End If
'--- 20150421LC (fine)
        '--- 20140404LC  (fine)
        dPens = ARRV((y1a + y1s) * ax)
        '--- 20121224LC (fine)
      End If
    End If
  End With
  '--- 20160312LC (inizio)
  If dPens > 0 And OPZ_INT_INCRPENS > 0 Then
    dPens = OPZ_INT_INCRPENS
  End If
  '--- 20160312LC (fine)
  TIscr2_MovPop_4c_dPens_Calcola = dPens
End Function


Private Sub TIscr2_MovPop_4c_dPens_Aggiorna(ByRef tp As TIscr2pens, ByRef dip As TIscr2, ByRef appl As TAppl, _
                                            ByRef s0 As TIscr2_Stato, ByRef s1 As TIscr2_Stato, ByRef tb1#(), _
                                            ByRef t&, ByRef dPens#)
'20150410LC  nuova (da TIscr2_MovPop_4c)
  Dim jp&, napa&  '20150410LC
  
  With tp
    jp = dip.t - .t0
    If .gr0 = 2 Or (.tp >= TB_MPC_VO And .tp <= TB_MPC_T) Then  '20140325LC
      If s1.napa > 0 Then
        '20150615LC
        'If (s1.tapa >= s1.napa) Or s1.sm <= AA0 Then  '20140307LC  'riportare 20140111LC-24
        If (s1.tapa >= s1.napa) Then   '20140307LC  'riportare 20140111LC-24
          .t1 = .t1 + s1.napa
          .tb(jp).p = .tb(jp).p + dPens
          '.tb(jp).fc = .tb(jp).fc * (.tb(jp).p - dPens) / .tb(jp).p  '20121222LC
          If .tb(jp).p > 0 Then  '20130105LC
            '--- 20131122LC inizio)
            'If OPZ_INT_ASSIST_MODEL >= 1 Then
              .tb(jp).fa1 = (.tb(jp).fa1 * (.tb(jp).p - dPens)) / .tb(jp).p
              .tb(jp).fa2 = (.tb(jp).fa2 * (.tb(jp).p - dPens)) / .tb(jp).p  '20131126LC
            'End If
            '--- 20131122LC fine)
            .tb(jp).fc = (.tb(jp).fc * (.tb(jp).p - dPens) + dPens) / .tb(jp).p '20121226LC
          End If
          '--- 20150410LC (inizio)
          dPens = 0#  '20150522LC (ripristinato, era stato eliminato)
          '20140111LC-3 (mat=16470,18523)
          'If .xan + 1 + s1.napa > appl.parm(P_cvSpEtaMax) Then
          '--- 20150612LC (inizio)
          'If Int(dip.xr + 1) + s1.napa > appl.CoeffVar(dip.t, ecv.cv_SpEtaMax) Or s1.sm <= AA0 Then  '20140307LC  'riportare 20140111LC-24
          '--- 20150609LC (inizio)
          'dip.PensAUS = dip.t  'pericoloso (vedi Aggiorna_Redd1)
          napa = appl.CoeffVar(dip.t, ecv.cv_SpAnni)
          If napa < 72 - dip.t + Year(dip.DNasc) Then
            napa = 72 - dip.t + Year(dip.DNasc)
          End If
          '--- 20150609LC (fine)
          If Int(dip.xr + 1) + napa > appl.CoeffVar(dip.t, ecv.cv_SpEtaMax) Or s1.sm <= AA0 Then  '20140307LC  'riportare 20140111LC-24
            s1.napa = 0
            .t1 = 0
            '--- 20160609LC (inizio)  'FIRR  'adattato da 41I 20150118LC
            Call TIscr2_MovPop_Firr(dip, appl, tp.tb(jp).zm, EFirr.Firr_PA)
            '--- 20160609LC (fine)  'FIRR  'adattato da 41I 20150118LC
            'z0z.yz(TWA_c1nw) = z0z.yz(TWA_c1nw) + z0z.n.c1c
            'z0z.n.c1c = 0
          'ElseIf .t0 < 2004 And .t < 2004 Then
          '  s1.napa = 2
          Else
          '  s1.napa = appl.parm(P_cvSpAnni)
            s1.napa = napa
          End If
          '--- 20150612LC (fine)
          '--- 20150410LC (fine)
          s1.tapa = 0
          s1.bPensAtt = (s1.bPensAtt = True) Or (s1.napa > 0) '20121009LC
          s1.om_rcV(om_pens_att) = 0 '20120117LC
          ReDim dip.om_rcU(eom.om_max)  '20131122LC  'ex 20120114LC
          ReDim dip.om_rcV(eom.om_max)  '20131122LC  'ex 20120114LC
          '--- 20160609LC (inizio)
          'bFirr = EFirr.Firr_None
          dip.om_rcU(om_misto_int) = dip.FirrU
          dip.om_rcV(om_misto_int) = dip.FirrV
          '--- 20160609LC (fine)
        End If
      End If
    End If
  End With
End Sub


Public Sub TIscr2_MovPop_9(ByRef dip As TIscr2, ByRef appl As TAppl, ByRef glo As TGlobale, s1 As TIscr2_Stato, ByRef tb1() As Double, t&, wa1 As TWA, wb() As TWB, wc() As Double)
'20080617LC
  Dim ij&, ijk&, iQ&, iTS&, iwb&, iwc&, j&, t1&, t2&, l& '20121028LC
  Dim y#, fact#
  Dim c0ea_z As Double  '20160222LC
  '--- 20181108LC (inizio)
  Dim MefX&
  Dim hOut As Double
  '--- 20181108LC (fine)
  
  '--- 20181108LC (inizio) CONSOLIDAMENTO MEF 3A
  If OPZ_DBXMEF <> 0 Then
    MefX = dip.anno - Year(dip.DNasc)
  End If
  '--- 20181108LC (fine) CONSOLIDAMENTO MEF 3A
  With wa1
    .c0v_z = 0#  '20150504LC
    .c0e_z = 0#
      c0ea_z = 0#  '20160222LC
    .c1b_z = 0#
    .c1i_z = 0#
    '--- 20121209LC (inizio)
    If dip.gruppo0 = 4 Then
      If t = 0 Then
        dip.zs1 = .sj0_z
        .sd_z = 0
      Else
        .sd_z = dip.zs1 - .sj0_z
      End If
    '--- 20121209LC (fine)
    Else '20080915LC
      .sj0_z = 0
      .sj1_z = 0
      .sj2_z = 0
      .sj3_z = 0
      .sj4_z = 0
      .sj5_z = 0
      .sj6_z = 0
      .sj7_z = 0
      .sj8_z = 0
      '--- 20121113LC (inizio)
      If appl.bProb_EA_IV_IB = True Then
        iwb = t * (1 + P_DUECENTO) + t
        .c0e_dh = 0  '20181121LC-2
        For t1 = t To 0 Step -1
          'iwb = t * (1 + P_DUECENTO) + t1
          .c0v_z = .c0v_z + wb(iwb).vg2  '20150504LC
          .c0e_z = .c0e_z + wb(iwb).eg2
          '--- 20181121LC-2 (inizio)
          If t1 > 0 Then
            .c0e_dh = .c0e_dh + wb(iwb).eg2 * (t - t1 + 0.5)
          End If
          '--- 20181121LC-2 (fine)
          '--- 20160222LC (inizio)
          If OPZ_OUT_SIL_ATT > 0 Then
            If dip.gruppo0 <> 5 Then
              If t - t1 < OPZ_OUT_SIL_ATT Then
                c0ea_z = c0ea_z + wb(iwb).eg2
              End If
            Else
              If t1 = 0 And wb(iwb).eg2 > 0 Then
                For j = 0 To OPZ_OUT_SIL_ATT
                  If LBound(dip.s) <= dip.t - j Then  '20160306LC (errore non intercettao sempre in W7)
                    If dip.s(dip.t - j).sm > 0 Then
                      c0ea_z = c0ea_z + wb(iwb).eg2
                      Exit For
                    End If
                  End If
                Next j
              End If
            End If
          End If
          '--- 20160222LC (fine)
          .c1b_z = .c1b_z + wb(iwb).bg2
          .c1i_z = .c1i_z + wb(iwb).ig2
          iwb = iwb - 1
        Next t1
        '--- 20181121LC-2 (inizio)
        If .c0e_z > 0 Then
          .c0e_dh = .c0e_dh / .c0e_z
        Else
          .c0e_dh = 0
        End If
        '--- 20181121LC-2 (fine)
      End If
      '--- 20121113LC (fine)
      '--- 20150612LC-2 (inizio)
      With dip
        If (.PensAUS = .t + 1) And (.gruppo0 = 5 Or .gruppo0 = 6) Then
          If .gruppo00 = 5 Then
            .wa(.tau1 - 0).c0a_z = .wa(.tau1 - 0).c0e_z
            .wa(.tau1 - 0).c0e_z = 0
            .wb((.tau1 - 0) * (1 + P_DUECENTO) + 0).eg2 = 0
          Else
            .wa(.tau1 - 0).c0a_z = .wa(.tau1 - 0).c0v_z
            .wa(.tau1 - 0).c0v_z = 0
            .wb((.tau1 - 0) * (1 + P_DUECENTO) + 0).vg2 = 0
          End If
          '.rr_dhEff0 = 1
          '.rr_dhEff = 1
        End If
      End With
      '--- 20150612LC-2 (fine)
      '--- 20121206LC (inizio)
      If appl.bProb_Superst = True Then
'Dim sj0_z#
        iwc = (t * (1 + P_DUECENTO) + t) * TWC_SIZE
        For t2 = t To 0 Step -1
If iwc <> (t2 * (1 + P_DUECENTO) + t) * TWC_SIZE Then
Call MsgBox("TODO TIscr2_MovPop_9-1")
Stop
End If
          .sj0_z = .sj0_z + wc(iwc)
          For j = 1 To 6 '6
            y = wc(iwc + j)
            If y <> 0 Then
              Select Case j
              Case 1
                fact = 1
                .sj1_z = .sj1_z + y
              Case 2
                fact = 2
                .sj2_z = .sj2_z + y
              Case 3
                fact = 3
                .sj3_z = .sj3_z + y
              Case 4
                fact = 1
                .sj4_z = .sj4_z + y
              Case 5
                fact = 2
                .sj5_z = .sj5_z + y
              Case 6
                fact = 3
                .sj6_z = .sj6_z + y
              End Select
              '.sj0_z = .sj0_z + y
'sj0_z = sj0_z + y
              .sj7_z = .sj7_z + y * fact  '20121206LC
            End If
          Next j
          iwc = iwc - (1 + P_DUECENTO) * TWC_SIZE
        Next t2
        '--- 20121206LC (fine)
      End If
    End If
    '20080416LC (inizio)
    .totAtt_z = TZD_Tot(wa1)
    .tot_z = .totAtt_z + .sj0_z + .w1b_z + .w2b_z + .w3b_z + .sd_z '+ .z1(t).yd  '20121028LC
'If math.Abs(10.1859782420998 - .tot_z) > AA0 Then
'y = y
'End If
    '20080416LC (fine)
    '**************
    'Consolidamento
    '**************
    '20080617LC (inizio)
'--- CONSOLIDAMENTO 3 (inizio) ---
    iQ = dip.iQ + 1
    iTS = dip.iTS + 1
    If OPZ_TB = 1 Then
      ijk = appl.nSize2 * iTS + appl.nSize3 * iQ + appl.nSize4 * (t + appl.t0 - 1960) '20121114LC '1
    Else
      'ijk = .iSQ + appl.nSize4 * (t + appl.t0 - 1960) '20121114LC '1
      ijk = dip.iSQT '1
    End If
    tb1(ijk + TB_ZTOT) = tb1(ijk + TB_ZTOT) + .tot_z
    tb1(ijk + TB_ZAA) = tb1(ijk + TB_ZAA) + .c0a_z + c0ea_z  '20160222LC
    tb1(ijk + TB_ZVA) = tb1(ijk + TB_ZVA) + .c0v_z
    tb1(ijk + TB_ZEA) = tb1(ijk + TB_ZEA) + .c0e_z - c0ea_z  '20160222LC
    tb1(ijk + TB_ZPN_B) = tb1(ijk + TB_ZPN_B) + .c1b_z
    tb1(ijk + TB_ZPN_I) = tb1(ijk + TB_ZPN_I) + .c1i_z
    '20120115LC (inizio) - controllare se qui va bene
    If .c1cvo_z + .c1cva_z + .c1cvp_z + .c1ca_z + .c1cs_z + .c1ct_z > AA0 Then  '20131202LC
      If s1.napa > 0 Then
        '--- 20131202LC (inizio)
        tb1(ijk + TB_ZPC_VO) = tb1(ijk + TB_ZPC_VO) + .c1cvo_z
        tb1(ijk + TB_ZPC_VA) = tb1(ijk + TB_ZPC_VA) + .c1cva_z
        tb1(ijk + TB_ZPC_VP) = tb1(ijk + TB_ZPC_VP) + .c1cvp_z
        '--- 20131202LC (fine)
        tb1(ijk + TB_ZPC_A) = tb1(ijk + TB_ZPC_A) + .c1ca_z
        tb1(ijk + TB_ZPC_S) = tb1(ijk + TB_ZPC_S) + .c1cs_z
        tb1(ijk + TB_ZPC_T) = tb1(ijk + TB_ZPC_T) + .c1ct_z
      Else
        '--- 20131202LC (inizio)
        .c1nvo_z = .c1nvo_z + .c1cvo_z
        .c1nva_z = .c1nva_z + .c1cva_z
        .c1nvp_z = .c1nvp_z + .c1cvp_z
        .c1cvo_z = 0
        .c1cva_z = 0
        .c1cvp_z = 0
        '--- 20131202LC (fine)
        .c1na_z = .c1na_z + .c1ca_z
        .c1ns_z = .c1ns_z + .c1cs_z
        .c1nt_z = .c1nt_z + .c1ct_z
        .c1ca_z = 0
        .c1cs_z = 0
        .c1ct_z = 0
      End If
    End If
    '20120115LC (fine)
    '--- 20131202LC (inizio)
    tb1(ijk + TB_ZPN_VO) = tb1(ijk + TB_ZPN_VO) + .c1nvo_z
    tb1(ijk + TB_ZPN_VA) = tb1(ijk + TB_ZPN_VA) + .c1nva_z
    tb1(ijk + TB_ZPN_VP) = tb1(ijk + TB_ZPN_VP) + .c1nvp_z
    '--- 20131202LC (fine)
    tb1(ijk + TB_ZPN_A) = tb1(ijk + TB_ZPN_A) + .c1na_z
    tb1(ijk + TB_ZPN_S) = tb1(ijk + TB_ZPN_S) + .c1ns_z
    tb1(ijk + TB_ZPN_T) = tb1(ijk + TB_ZPN_T) + .c1nt_z
    'tb1(ijk + TB_ZD) = tb1(ijk + TB_ZD) + .z1(t).yd    '20121028LC (commentata)
    tb1(ijk + TB_ZD_S) = tb1(ijk + TB_ZD_S) + .sd_z '.ysd
    tb1(ijk + TB_ZW_1) = tb1(ijk + TB_ZW_1) + .w1b_z '.yw1b
    tb1(ijk + TB_ZW_2) = tb1(ijk + TB_ZW_2) + .w2b_z '.yw2b
    tb1(ijk + TB_ZW_3) = tb1(ijk + TB_ZW_3) + .w3b_z '.yw3b
    'If .gruppo0 <> 4 Then '20080915LC
      tb1(ijk + TB_ZPS_1) = tb1(ijk + TB_ZPS_1) + .sj0_z  '.ysj(0)
'tb1(ijk + TB_ZFAM1) = tb1(ijk + TB_ZFAM1) + .sj0_z  '.ysj(0)
      tb1(ijk + TB_ZPS_2) = tb1(ijk + TB_ZPS_2) + .sj7_z  '.ysj(7)
'tb1(ijk + TB_ZFAM) = tb1(ijk + TB_ZFAM) + .sj1_z + .sj4_z + 2 * (.sj2_z + .sj5_z) + 3 * (.sj3_z + .sj6_z)
      tb1(ijk + TB_ZPS_V) = tb1(ijk + TB_ZPS_V) + .sj1_z    '.ysj(1)
      tb1(ijk + TB_ZPS_VO) = tb1(ijk + TB_ZPS_VO) + .sj2_z  '.ysj(2)
      tb1(ijk + TB_ZPS_VOO) = tb1(ijk + TB_ZPS_VOO) + .sj3_z '.ysj(3)
      tb1(ijk + TB_ZPS_O) = tb1(ijk + TB_ZPS_O) + .sj4_z    '.ysj(4)
      tb1(ijk + TB_ZPS_OO) = tb1(ijk + TB_ZPS_OO) + .sj5_z  '.ysj(5)
      tb1(ijk + TB_ZPS_OOO) = tb1(ijk + TB_ZPS_OOO) + .sj6_z '.ysj(6)
      'tb1(ijk + TB_ZFAM) = tb1(ijk + TB_ZFAM) + .z1(t).ysj(1) + .z1(t).ysj(4) + 2 * (.ysj(2) + .z1(t).ysj(5)) + 3 * (.ysj(3) + .z1(t).ysj(6))
    'End If
    '20080617LC (fine)
'--- CONSOLIDAMENTO 3 (fine) ---
    '--- 20181108LC (inizio) CONSOLIDAMENTO MEF 3B
    If OPZ_DBXMEF <> 0 Then
      y = .w00_mef
      .w00_mef = .w1b_z + .w2b_z + .w3b_z + .sd_z
      Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_z_z, EMef.Mef_PosFin, t, y, OPZ_DBXMEF)           'MEF00
      Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_a_z, EMef.Mef_PosFin, t, .w00_mef - y, OPZ_DBXMEF) 'MEF00
      hOut = s1.anz.h
      If dip.anno1 > dip.anno0 And hOut < 1# Then
        hOut = 0#
      Else
        hOut = hOut - AA0
      End If
      If .c0a_z + .c0v_z > 0 Then
        If dip.anno0 = dip.anno1 Or dip.t > dip.anno1 Then
          Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_a_a, EMef.Mef_PosFin, t, .c0a_z + .c0v_z, OPZ_DBXMEF)               'MEF10
          Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_a_a, EMef.Mef_PosAnz, t, (.c0a_z + .c0v_z) * hOut, OPZ_DBXMEF)  'MEF10
        Else
          Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_n_a, EMef.Mef_PosFin, t, .c0a_z + .c0v_z, OPZ_DBXMEF)               'MEF10
          Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_n_a, EMef.Mef_PosAnz, t, (.c0a_z + .c0v_z) * hOut, OPZ_DBXMEF)  'MEF10
        End If
      End If
      '---
      Dim nSilenti As Double
      nSilenti = .c0e_z * dip.nRes
      If nSilenti > 0 Then
        hOut = hOut - .c0e_dh  '20181121LC-2
        If dip.anno0 = dip.anno1 Or dip.t > dip.anno1 Then
          Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_s_s, EMef.Mef_PosFin, t, nSilenti, OPZ_DBXMEF)                 'MEF40
          Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_s_s, EMef.Mef_PosAnz, t, nSilenti * hOut, OPZ_DBXMEF)    'MEF40
        Else
          Call MsgBox("TODO - Non sono previsti nuovi ingressi di silenti")
          Stop
          Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_n_a, EMef.Mef_PosFin, t, nSilenti, OPZ_DBXMEF)                 'MEF40
          Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_n_a, EMef.Mef_PosAnz, t, nSilenti * hOut, OPZ_DBXMEF)    'MEF40
        End If
      End If
    End If
    '--- 20181108LC (fine) CONSOLIDAMENTO MEF 3B
  End With
End Sub


Public Sub TIscr2_MovPop_Contributi(ByRef dip As TIscr2, ByRef appl As TAppl, ByRef tb1() As Double, t&, wa0 As TWA, wa1 As TWA, s0 As TIscr2_Stato, s1 As TIscr2_Stato, iop&)
'****************************************************************************************************
'SCOPO
'  Consolida i contributi
'VERSIONI
'  20150608LC  tutta (generazioni volontari)
'****************************************************************************************************
  Dim bContrA As Boolean
  Dim i&, ijk&, ijk1&, iQ&, iTS&
  Dim dya#, ya0#, ya1#
  '--- 20150608LC (inizio)
  Dim iwb&, iwbm&, t1&, t1a&, t1b&
  Dim frazA#, frazPA#, yv0#, yv1#, yya0#, yya1#
  Dim yIrpef#, yIrpef2#, yIva#
  Dim yEcs01#, yEcs1#, yEcs2#, yEcs3#, yEcsr#
  Dim yEcs02#
  Dim yEci0#, yEci1#, yEci2#, yEci3#, yEcir#
  Dim yEca0#, yEca1#, yEcar#
  Dim yEmat#
  Dim yEfi0#, yEfi1#, yEfs0#, yEfs1#
  Dim yEai0#, yEai1#
  Dim yEtot#
  '--- 20150608LC (fine)
  '--- 20150612LC (inizio)
  Dim za0#, za1#
  Dim zza0#, zza1#
  '--- 20150612LC (fine)
  Dim MefX&  '20181108LC
  
  With dip
    '--- 20181108LC (inizio) CONSOLIDAMENTO MEF 4A
    If OPZ_DBXMEF <> 0 Then
      MefX = .t - Year(.DNasc)
    End If
    '--- 20181108LC (fine) CONSOLIDAMENTO MEF 4A
'--- CONSOLIDAMENTO 4 (inizio) ---
    bContrA = (s0.stato = ST_A)
    iQ = .iQ + 1
    iTS = .iTS + 1
    ijk = appl.nSize2 * iTS + appl.nSize3 * iQ + appl.nSize4 * (t + appl.t0 - 1960) '20121114LC
    If .t - 1 < .anno1 Then
      If t > 0 And .anno1 > .anno0 Then
        '--- Consolidiamo i contributi dei nuovi iscritti nel primo anno (6 mesi)
        ya0 = wa0.c0a_z
        ya1 = wa1.c0a_z
        yya0 = ya0
        yya1 = ya1
        '--- 20150612LC (inizio)
        za0 = ya0
        za1 = ya1
        zza0 = za0
        zza1 = za1
        '--- 20150612LC (fine)
If yya0 <> 0 Then
  MsgBox "TODO - TIscr2_MovPop_Contributi 1"
  Stop
End If
        '---
        yIrpef = ya1 * dmax(.irpefNI, 0)
        yIrpef2 = ya1 * dmax(.irpefNI, 0)  'TODO: irpefNI potrebbe essere maggiore del massimale
        yIva = ya1 * .ivaNI
If (bContrA = False) Xor s0.stato = ST_PAT Then
  If s0.stato <> ST_PNA Then
    MsgBox "TODO - TIscr2_MovPop_Contributi 2"
    Stop
  ElseIf yIrpef > AA0 Then
    MsgBox "TODO - TIscr2_MovPop_Contributi 3"
    Stop
  End If
End If
        '---
        yEcs01 = ya1 * s1.c_sogg0a1
        yEcs02 = ya1 * s1.c_sogg0a2
        yEci0 = ya1 * s1.c_int0a
        yEca0 = ya1 * s1.c_ass0a
        yEfs0 = ya1 * s1.c_sogg_fig0
        yEai0 = ya1 * s1.c_int_ass0
        yEfi0 = ya1 * s1.c_int_fig0
        yEmat = ya1 * s1.c_mat
        '---
        yEtot = yEcs01 + yEcs02 + yEci0
        '---
        If s0.stato = ST_PAT Then
        Else
        End If
        '---
        If bContrA = False Then
          '--- 1) Popolazione
          'tb1(ijk + TB_PA_ZETOT) = tb1(ijk + TB_PA_ZETOT) + zza0  '20150612LC  'ex yya0
          '--- 2) Irpef e Iva/FIRR
          tb1(ijk + TB_PA_IRPEF) = tb1(ijk + TB_PA_IRPEF) + yIrpef
          tb1(ijk + TB_PA_IRPEF2) = tb1(ijk + TB_PA_IRPEF2) + yIrpef2
          tb1(ijk + TB_PA_IVA) = tb1(ijk + TB_PA_IVA) + yIva
          '--- 3) Contributi soggettivi
          tb1(ijk + TB_PA_ECS01) = tb1(ijk + TB_PA_ECS01) + yEcs01
          '--- 4) Contributi volontari
          tb1(ijk + TB_PA_ECS02) = tb1(ijk + TB_PA_ECS02) + yEcs02
          '--- 5) Contributi integrativi
          tb1(ijk + TB_PA_ECI0) = tb1(ijk + TB_PA_ECI0) + yEci0
          '--- 6) Contributi assistenziali/solidariet� attivi
          tb1(ijk + TB_PA_ECA0) = tb1(ijk + TB_PA_ECA0) + yEca0
          '--- 7) Contributi di maternit�
          tb1(ijk + TB_PA_EMAT) = tb1(ijk + TB_PA_EMAT) + yEmat
          '--- 8) Contributi figurativi
          tb1(ijk + TB_PA_EFS0) = tb1(ijk + TB_PA_EFS0) + yEfs0
          tb1(ijk + TB_PA_EFI0) = tb1(ijk + TB_PA_EFI0) + yEfi0
          '--- 9) Contributi non retrocessi
          tb1(ijk + TB_PA_EAI0) = tb1(ijk + TB_PA_EAI0) + yEai0
          '--- 10) Contributi totali
          tb1(ijk + TB_PA_ETOT) = tb1(ijk + TB_PA_ETOT) + yEtot
        Else
          '--- 1) Popolazione
          'tb1(ijk + TB_AA_ZETOT) = tb1(ijk + TB_AA_ZETOT) + zza0  '20150612LC  'ex yya0
          '--- 2) Irpef e Iva/FIRR
          tb1(ijk + TB_AA_IRPEF) = tb1(ijk + TB_AA_IRPEF) + yIrpef
          tb1(ijk + TB_AA_IRPEF2) = tb1(ijk + TB_AA_IRPEF2) + yIrpef2
          tb1(ijk + TB_AA_IVA) = tb1(ijk + TB_AA_IVA) + yIva
          '--- 3) Contributi soggettivi
          tb1(ijk + TB_AA_ECS01) = tb1(ijk + TB_AA_ECS01) + yEcs01
          '--- 4) Contributi volontari
          tb1(ijk + TB_AA_ECS02) = tb1(ijk + TB_AA_ECS02) + yEcs02
          '--- 5) Contributi integrativi
          tb1(ijk + TB_AA_ECI0) = tb1(ijk + TB_AA_ECI0) + yEci0
          '--- 6) Contributi assistenziali/solidariet� attivi
          tb1(ijk + TB_AA_ECA0) = tb1(ijk + TB_AA_ECA0) + yEca0
          '--- 7) Contributi di maternit�
          tb1(ijk + TB_AA_EMAT) = tb1(ijk + TB_AA_EMAT) + yEmat
          '--- 8) Contributi figurativi
          tb1(ijk + TB_AA_EFS0) = tb1(ijk + TB_AA_EFS0) + yEfs0
          tb1(ijk + TB_AA_EFI0) = tb1(ijk + TB_AA_EFI0) + yEfi0
          '--- 9) Contributi non retrocessi
          tb1(ijk + TB_AA_EAI0) = tb1(ijk + TB_AA_EAI0) + yEai0
          '--- 10) Contributi totali
          tb1(ijk + TB_AA_ETOT) = tb1(ijk + TB_AA_ETOT) + yEtot
        End If
        '--- 20181108LC (inizio) CONSOLIDAMENTO MEF 4B
        If OPZ_DBXMEF <> 0 Then
          yIrpef = ya1 * dmax(.irpefNI, 0)
          yIrpef2 = ya1 * dmax(.irpefNI, 0)  'TODO: irpefNI potrebbe essere maggiore del massimale
          yIva = ya1 * .ivaNI
          If bContrA = True Then
            Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_n_a, EMef.Mef_PosRedd, t, yIrpef, OPZ_DBXMEF)  'MEF10
            Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_n_a, EMef.Mef_PosContr, t, yEtot, OPZ_DBXMEF)   'MEF10
          Else
            Call MsgBox("TODO - TIscr2_MovPop_Contributi - mat=" & dip.mat)
            Stop
            Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_pa_pa, EMef.Mef_PosRedd, t, yIrpef, OPZ_DBXMEF)   'MEF21
            Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_pa_pa, EMef.Mef_PosContr, t, yEtot, OPZ_DBXMEF)   'MEF21
          End If
        End If
        '--- 20181108LC (fine) CONSOLIDAMENTO MEF 4B
      End If
      Exit Sub
    End If
    If (.gruppo0 = 1) Or (.gruppo0 = 6) Then  '20150609LC
      i = 1
    ElseIf (.gruppo0 = 5) Then
      i = 1
    ElseIf (.gruppo0 = 2) And s0.napa > 0 Then
      i = 2
    Else
      i = 0
    End If
    If i > 0 Then
      '--- 1) Popolazione
      If iop = 1 Then
        Call TZD_Test_C(wa0)
        If (.gruppo0 = 6) And 1 = 1 Then  '20150612LC (solo attivi nella popolazione)
          ya0 = wa0.c0v_z + wa0.c1cvo_z + wa0.c1cva_z + wa0.c1cvp_z + wa0.c1ca_z + wa0.c1cs_z + wa0.c1ct_z
          ya1 = wa1.c0v_z + wa1.c1cvo_z + wa1.c1cva_z + wa1.c1cvp_z + wa1.c1ca_z + wa1.c1cs_z + wa1.c1ct_z
          '--- 20150612LC (inizio)
          za0 = 0#
          za1 = 0#
          '--- 20150612LC (fine)
        Else
          ya0 = wa0.c0a_z + wa0.c1cvo_z + wa0.c1cva_z + wa0.c1cvp_z + wa0.c1ca_z + wa0.c1cs_z + wa0.c1ct_z
          ya1 = wa1.c0a_z + wa1.c1cvo_z + wa1.c1cva_z + wa1.c1cvp_z + wa1.c1ca_z + wa1.c1cs_z + wa1.c1ct_z
          '--- 20150612LC (inizio)
          za0 = ya0
          za1 = ya1
          '--- 20150612LC (fine)
        End If
      Else
        ya0 = .n
        ya1 = ya0
      End If
      yya0 = ya0
      yya1 = ya1
      '--- 20150612LC (inizio)
      zza0 = za0
      zza1 = za1
      '--- 20150612LC (fine)
      '--- 2) Irpef e Iva/FIRR
      yIrpef = ya0 * dmax(s1.sm, 0)
      yIrpef2 = ya0 * dmax(s1.sm2ns, 0)
      yIva = ya0 * s1.iva
If (bContrA = False) Xor s0.stato = ST_PAT Then
  If s0.stato <> ST_PNA Then
    MsgBox "TODO - TIscr2_MovPop_Contributi 5"
    Stop
  ElseIf yIrpef > AA0 Then
    MsgBox "TODO - TIscr2_MovPop_Contributi 6"
    Stop
  End If
End If
      '--- 3) Contributi soggettivi
      yEcs01 = ya0 * s1.c_sogg0a1
      '--- 4) Contributi volontari
      yEcs02 = ya0 * s1.c_sogg0a2
      '--- 5) Contributi integrativi
      yEmat = ya0 * s1.c_mat
      '--- 6) Contributi assistenziali/solidariet� attivi
      yEca0 = ya0 * s1.c_ass0a
      '--- 7) Contributi di maternit�
      yEci0 = ya0 * s1.c_int0a
      '--- 8) Contributi figurativi
      yEfs0 = ya0 * s1.c_sogg_fig0
      yEfi0 = ya0 * s1.c_int_fig0
      '--- 9) Contributi non retrocessi
      yEai0 = ya0 * s1.c_int_ass0
      If (iop = 1) Or (iop = 0 And .t > .annoAss) Then
        '--- 3) Contributi soggettivi
        yEcs1 = ya0 * s0.c_sogg1a
        yEcs2 = ya0 * s0.c_sogg2a
        yEcs3 = ya0 * s0.c_sogg3a
        '--- 4) Contributi volontari
        '--- 5) Contributi integrativi
        yEci1 = ya0 * s0.c_int1a
        yEci2 = ya0 * s0.c_int2a
        yEci3 = ya0 * s0.c_int3a
        '--- 6) Contributi assistenziali/solidariet� attivi
        yEca1 = ya0 * s0.c_ass1a
        '--- 7) Contributi di maternit�
        '--- 8) Contributi figurativi
        yEfs1 = ya0 * s0.c_sogg_fig1
        yEfi1 = ya0 * s0.c_int_fig1
        '--- 9) Contributi non retrocessi
        yEai1 = ya0 * s0.c_int_ass1
      End If
      '---
      If OPZ_INT_NO_AE_AB_AI = 0 Then
        t1a = 1
        t1b = t - (.anno1 - .anno0) * 1 - 0  '-1 in TIscr2_MovPop_4-1
        iwbm = (t - 1) * (1 + P_DUECENTO) + t1a
        iwb = t * (1 + P_DUECENTO) + t1a
        For t1 = t1a To t1b
          'iwbm = (t - 1) * (1 + P_DUECENTO) + t1
          'iwb = t * (1 + P_DUECENTO) + t1
          yv0 = .wb(iwbm).vg2
          yv1 = .wb(iwb).vg2
          '--- 1) Popolazione
          yya0 = yya0 + yv0
          yya1 = yya1 + yv1
          '--- 20150612LC (inizio)
          'zza0 = zza0 + yv0
          'zza1 = zza1 + yv1
          '--- 20150612LC (fine)
          If yv0 > AA0 Then
            '--- 2) Irpef e Iva/FIRR
            '--- 20150615LC (inizio)
            'yIrpef = yIrpef + yv0 * dmax(dip.wb(iwbm).vg2sm, 0)
            'yIrpef2 = yIrpef2 + yv0 * dmax(dip.wb(iwbm).vg2sm2, 0)
            'yIva = yIva
            '--- 20150615LC (fine)
            '--- 3) Contributi soggettivi
            '--- 4) Contributi volontari
            yEcs02 = yEcs02 + yv0 * dip.wb(iwbm).vg2cSogg
            '--- 5) Contributi integrativi
            '--- 6) Contributi assistenziali/solidariet� attivi
            yEca0 = yEca0 + yv0 * dip.wb(iwbm).vg2cAss
            '--- 7) Contributi di maternit�
            yEmat = yEmat + yv0 * dip.wb(iwbm).vg2cMat
            '--- 8) Contributi figurativi
            '--- 9) Contributi non retrocessi
            If (iop = 1) Or (iop = 0 And .t > .annoAss) Then  'Non ci sono saldi per i volontari
              '--- 3) Contributi soggettivi
              '--- 4) Contributi volontari
              '--- 5) Contributi integrativi
              '--- 6) Contributi assistenziali/solidariet� attivi
              '--- 7) Contributi di maternit�
              '--- 8) Contributi figurativi
              '--- 9) Contributi non retrocessi
            End If
          End If
          '---
          iwbm = iwbm + 1
          iwb = iwb + 1
        Next t1
      End If
      '--- 10) Contributi totali
      yEtot = yEcs01 + yEcs02 + yEci0
      If (iop = 1) Or (iop = 0 And .t > .annoAss) Then
        '--- 20160612LC (inizio)
        yEtot = yEtot + yEcs1 + yEcs2 + yEcs3
        '--- 20160612LC (fine)
      End If
      '---
      If bContrA = True Then
        '--- 1) Popolazione
        tb1(ijk + TB_AA_ZETOT) = tb1(ijk + TB_AA_ZETOT) + zza0  '20150612LC  'ex yya0
        '--- 2) Irpef e Iva/FIRR
        tb1(ijk + TB_AA_IRPEF) = tb1(ijk + TB_AA_IRPEF) + yIrpef
        tb1(ijk + TB_AA_IRPEF2) = tb1(ijk + TB_AA_IRPEF2) + yIrpef2
        tb1(ijk + TB_AA_IVA) = tb1(ijk + TB_AA_IVA) + yIva
        '--- 3) Contributi soggettivi
        tb1(ijk + TB_AA_ECS01) = tb1(ijk + TB_AA_ECS01) + yEcs01
        '--- 4) Contributi volontari
        tb1(ijk + TB_AA_ECS02) = tb1(ijk + TB_AA_ECS02) + yEcs02
        '--- 5) Contributi integrativi
        tb1(ijk + TB_AA_ECI0) = tb1(ijk + TB_AA_ECI0) + yEci0
        '--- 6) Contributi assistenziali/solidariet� attivi
        tb1(ijk + TB_AA_ECA0) = tb1(ijk + TB_AA_ECA0) + yEca0
        '--- 7) Contributi di maternit�
        tb1(ijk + TB_AA_EMAT) = tb1(ijk + TB_AA_EMAT) + yEmat
        '--- 8) Contributi figurativi
        tb1(ijk + TB_AA_EFS0) = tb1(ijk + TB_AA_EFS0) + yEfs0
        tb1(ijk + TB_AA_EFI0) = tb1(ijk + TB_AA_EFI0) + yEfi0
        '--- 9) Contributi non retrocessi
        tb1(ijk + TB_AA_EAI0) = tb1(ijk + TB_AA_EAI0) + yEai0
        '---
        If (iop = 1) Or (iop = 0 And .t > .annoAss) Then
          '--- 3) Contributi soggettivi
          tb1(ijk + TB_AA_ECS1) = tb1(ijk + TB_AA_ECS1) + yEcs1
          tb1(ijk + TB_AA_ECS2) = tb1(ijk + TB_AA_ECS2) + yEcs2
          tb1(ijk + TB_AA_ECS3) = tb1(ijk + TB_AA_ECS3) + yEcs3
          '--- 4) Contributi volontari
          '--- 5) Contributi integrativi
          tb1(ijk + TB_AA_ECI1) = tb1(ijk + TB_AA_ECI1) + yEci1
          tb1(ijk + TB_AA_ECI2) = tb1(ijk + TB_AA_ECI2) + yEci2
          tb1(ijk + TB_AA_ECI3) = tb1(ijk + TB_AA_ECI3) + yEci3
          '--- 6) Contributi assistenziali/solidariet� attivi
          tb1(ijk + TB_AA_ECA1) = tb1(ijk + TB_AA_ECA1) + yEca1
          '--- 7) Contributi di maternit�
          '--- 8) Contributi figurativi
          tb1(ijk + TB_AA_EFS1) = tb1(ijk + TB_AA_EFS1) + yEfs1
          tb1(ijk + TB_AA_EFI1) = tb1(ijk + TB_AA_EFI1) + yEfi1
          '--- 9) Contributi non retrocessi
          tb1(ijk + TB_AA_EAI1) = tb1(ijk + TB_AA_EAI1) + yEai1
        End If
        '--- 10) Contributi totali
        tb1(ijk + TB_AA_ETOT) = tb1(ijk + TB_AA_ETOT) + yEtot
      Else
        '--- 1) Popolazione
        tb1(ijk + TB_PA_ZETOT) = tb1(ijk + TB_PA_ZETOT) + zza0  '20150612LC  'ex yya0
        '--- 2) Irpef e Iva/FIRR
        tb1(ijk + TB_PA_IRPEF) = tb1(ijk + TB_PA_IRPEF) + yIrpef
        tb1(ijk + TB_PA_IRPEF2) = tb1(ijk + TB_PA_IRPEF2) + yIrpef2
        tb1(ijk + TB_PA_IVA) = tb1(ijk + TB_PA_IVA) + yIva
        '--- 3) Contributi soggettivi
        tb1(ijk + TB_PA_ECS01) = tb1(ijk + TB_PA_ECS01) + yEcs01
        '--- 4) Contributi volontari
        tb1(ijk + TB_PA_ECS02) = tb1(ijk + TB_PA_ECS02) + yEcs02
        '--- 5) Contributi integrativi
        tb1(ijk + TB_PA_ECI0) = tb1(ijk + TB_PA_ECI0) + yEci0
        '--- 6) Contributi assistenziali/solidariet� attivi
        tb1(ijk + TB_PA_ECA0) = tb1(ijk + TB_PA_ECA0) + yEca0
        '--- 7) Contributi di maternit�
        tb1(ijk + TB_PA_EMAT) = tb1(ijk + TB_PA_EMAT) + yEmat
        '--- 8) Contributi figurativi
        tb1(ijk + TB_PA_EFS0) = tb1(ijk + TB_PA_EFS0) + yEfs0
        tb1(ijk + TB_PA_EFI0) = tb1(ijk + TB_PA_EFI0) + yEfi0
        '--- 9) Contributi non retrocessi
        tb1(ijk + TB_PA_EAI0) = tb1(ijk + TB_PA_EAI0) + yEai0
        '---
        If (iop = 1) Or (iop = 0 And .t > .annoAss) Then
          '--- 3) Contributi soggettivi
          tb1(ijk + TB_PA_ECS1) = tb1(ijk + TB_PA_ECS1) + yEcs1
          tb1(ijk + TB_PA_ECS2) = tb1(ijk + TB_PA_ECS2) + yEcs2
          tb1(ijk + TB_PA_ECS3) = tb1(ijk + TB_PA_ECS3) + yEcs3
          '--- 4) Contributi volontari
          '--- 5) Contributi integrativi
          tb1(ijk + TB_PA_ECI1) = tb1(ijk + TB_PA_ECI1) + yEci1
          tb1(ijk + TB_PA_ECI2) = tb1(ijk + TB_PA_ECI2) + yEci2
          tb1(ijk + TB_PA_ECI3) = tb1(ijk + TB_PA_ECI3) + yEci3
          '--- 6) Contributi assistenziali/solidariet� attivi
          tb1(ijk + TB_PA_ECA1) = tb1(ijk + TB_PA_ECA1) + yEca1
          '--- 7) Contributi di maternit�
          '--- 8) Contributi figurativi
          tb1(ijk + TB_PA_EFS1) = tb1(ijk + TB_PA_EFS1) + yEfs1
          tb1(ijk + TB_PA_EFI1) = tb1(ijk + TB_PA_EFI1) + yEfi1
          '--- 9) Contributi non retrocessi
          tb1(ijk + TB_PA_EAI1) = tb1(ijk + TB_PA_EAI1) + yEai1
        End If
        '--- 10) Contributi totali
        tb1(ijk + TB_PA_ETOT) = tb1(ijk + TB_PA_ETOT) + yEtot
      End If
      '--- 20181108LC (inizio) CONSOLIDAMENTO MEF 4C
      If OPZ_DBXMEF <> 0 Then
        If s1.stato = ST_A Then
          If ya0 <= 0 Then
            If yIrpef <> 0 Or yEtot <> 0 Then
              MsgBox "TODO"
              Stop
            End If
          ElseIf ya0 = ya1 Then
            Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_a_a, EMef.Mef_PosRedd, t, yIrpef, OPZ_DBXMEF) 'MEF10
            Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_a_a, EMef.Mef_PosContr, t, yEtot, OPZ_DBXMEF) 'MEF10
          ElseIf ya1 = 0 Then
            Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_a_z, EMef.Mef_PosRedd, t, yIrpef, OPZ_DBXMEF) 'MEF10->MEF00
            Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_a_z, EMef.Mef_PosContr, t, yEtot, OPZ_DBXMEF) 'MEF10->MEF00
          Else
            Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_a_a, EMef.Mef_PosRedd, t, yIrpef / ya0 * ya1, OPZ_DBXMEF) 'MEF10
            Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_a_a, EMef.Mef_PosContr, t, yEtot / ya0 * ya1, OPZ_DBXMEF) 'MEF10
            Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_a_z, EMef.Mef_PosRedd, t, yIrpef / ya0 * (ya0 - ya1), OPZ_DBXMEF) 'MEF10->MEF00
            Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_a_z, EMef.Mef_PosContr, t, yEtot / ya0 * (ya0 - ya1), OPZ_DBXMEF) 'MEF10->MEF00
          End If
        ElseIf s0.stato = ST_A Then
          If ya0 <= 0 Then
            If yIrpef <> 0 Or yEtot <> 0 Then
              MsgBox "TODO"
              Stop
            End If
          ElseIf ya0 = ya1 Then
            Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_a_pa, EMef.Mef_PosRedd, t, yIrpef, OPZ_DBXMEF)  'MEF10->MEF21
            Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_a_pa, EMef.Mef_PosContr, t, yEtot, OPZ_DBXMEF)  'MEF10->MEF21
          ElseIf ya1 = 0 Then
            Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_a_z, EMef.Mef_PosRedd, t, yIrpef, OPZ_DBXMEF)   'MEF10->MEF21
            Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_a_z, EMef.Mef_PosContr, t, yEtot, OPZ_DBXMEF)   'MEF10->MEF21
          Else
            Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_a_pa, EMef.Mef_PosRedd, t, yIrpef / ya0 * ya1, OPZ_DBXMEF) 'MEF10->MEF21
            Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_a_pa, EMef.Mef_PosContr, t, yEtot / ya0 * ya1, OPZ_DBXMEF) 'MEF10->MEF21
            Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_a_z, EMef.Mef_PosRedd, t, yIrpef / ya0 * (ya0 - ya1), OPZ_DBXMEF) 'MEF10->MEF21
            Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_a_z, EMef.Mef_PosContr, t, yEtot / ya0 * (ya0 - ya1), OPZ_DBXMEF) 'MEF10->MEF21
          End If
        Else
          If ya0 <= 0 Then
            If yIrpef <> 0 Or yEtot <> 0 Then
              ''MsgBox "TODO"
              ''Stop
            End If
          ElseIf ya0 = ya1 Then
            Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_pa_pa, EMef.Mef_PosRedd, t, yIrpef, OPZ_DBXMEF) 'MEF21
            Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_pa_pa, EMef.Mef_PosContr, t, yEtot, OPZ_DBXMEF) 'MEF21
          ElseIf ya1 = 0 Then
            Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_pa_z, EMef.Mef_PosRedd, t, yIrpef, OPZ_DBXMEF) 'MEF21->MEF00
            Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_pa_z, EMef.Mef_PosContr, t, yEtot, OPZ_DBXMEF) 'MEF21->MEF00
          Else
            Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_pa_pa, EMef.Mef_PosRedd, t, yIrpef / ya0 * ya1, OPZ_DBXMEF) 'MEF21
            Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_pa_pa, EMef.Mef_PosContr, t, yEtot / ya0 * ya1, OPZ_DBXMEF) 'MEF21
            Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_pa_z, EMef.Mef_PosRedd, t, yIrpef / ya0 * (ya0 - ya1), OPZ_DBXMEF) 'MEF21->MEF00
            Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_pa_z, EMef.Mef_PosContr, t, yEtot / ya0 * (ya0 - ya1), OPZ_DBXMEF) 'MEF21->MEF00
          End If
        End If
      End If
      '--- 20181108LC (fine) CONSOLIDAMENTO MEF 4C
      dya = ya0 - ya1
      If dya > 0# Then
        ijk1 = appl.nSize2 * iTS + appl.nSize3 * iQ + appl.nSize4 * (t + 1 + appl.t0 - 1960) '20121114LC
        '--- 1) Popolazione
        '--- 2) Irpef e Iva/FIRR
        '--- 3) Contributi soggettivi
        yEcsr = dya * (s1.c_sogg1a + s1.c_sogg2a + s1.c_sogg3a)
        '--- 4) Contributi volontari
        '--- 5) Contributi integrativi
        '--- 20160609LC (inizio)
        'I contributi FIrr nell'anno di cessazione del mandato non vengono versati a Enasarco
        yEcir = 0#
        '--- 20160609LC (fine)
        '--- 6) Contributi assistenziali/solidariet� attivi
        yEcar = dya * s1.c_ass1a
        '--- 7) Contributi di maternit�
        '--- 8) Contributi figurativi
        yEfs1 = dya * s1.c_sogg_fig1
        yEfi1 = dya * s1.c_int_fig1
        '--- 9) Contributi non retrocessi
        yEai1 = dya * s1.c_int_ass1
        '--- 10) Contributi totali
        yEtot = yEcsr + yEcir
        '---
        If 1 = 1 Then
          '--- 1) Popolazione
          '--- 2) Irpef e Iva/FIRR
          '--- 3) Contributi soggettivi
          '--- 4) Contributi volontari
          '--- 5) Contributi integrativi
          '--- 6) Contributi assistenziali/solidariet� attivi
          '--- 7) Contributi di maternit�
          '--- 8) Contributi figurativi
          '--- 9) Contributi non retrocessi
          '--- 10) Contributi totali
        End If
        '---
        If bContrA = False Then
          frazA = 0#
          frazPA = 1#
        ElseIf bContrA = True And s1.stato = ST_A Then
          frazA = 1#
          frazPA = 0#
        Else
          frazA = ya0 / dya
          frazPA = -ya1 / dya
        End If
        '---
        If frazA > 0 Then
          '--- 1) Popolazione
          '--- 2) Irpef e Iva/FIRR
          '--- 3) Contributi soggettivi
          tb1(ijk1 + TB_AA_ECSR) = tb1(ijk1 + TB_AA_ECSR) + frazA * yEcsr
          '--- 4) Contributi volontari
          '--- 5) Contributi integrativi
          tb1(ijk1 + TB_AA_ECIR) = tb1(ijk1 + TB_AA_ECIR) + frazA * yEcir
          '--- 6) Contributi assistenziali/solidariet� attivi
          tb1(ijk1 + TB_AA_ECAR) = tb1(ijk1 + TB_AA_ECAR) + frazA * yEcar
          '--- 7) Contributi di maternit�
          '--- 8) Contributi figurativi
          tb1(ijk1 + TB_AA_EFS1) = tb1(ijk1 + TB_AA_EFS1) + frazA * yEfs1
          tb1(ijk1 + TB_AA_EFI1) = tb1(ijk1 + TB_AA_EFI1) + frazA * yEfi1
          '--- 9) Contributi non retrocessi
          tb1(ijk1 + TB_AA_EAI1) = tb1(ijk1 + TB_AA_EAI1) + frazA * yEai1
          '--- 10) Contributi totali
          tb1(ijk1 + TB_AA_ETOT) = tb1(ijk1 + TB_AA_ETOT) + frazA * yEtot
        End If
        '---
        If frazPA > 0 Then
          '--- 1) Popolazione
          '--- 2) Irpef e Iva/FIRR
          '--- 3) Contributi soggettivi
          tb1(ijk1 + TB_PA_ECSR) = tb1(ijk1 + TB_PA_ECSR) + frazPA * yEcsr
          '--- 4) Contributi volontari
          '--- 5) Contributi integrativi
          tb1(ijk1 + TB_PA_ECIR) = tb1(ijk1 + TB_PA_ECIR) + frazPA * yEcir
          '--- 6) Contributi assistenziali/solidariet� attivi
          tb1(ijk1 + TB_PA_ECAR) = tb1(ijk1 + TB_PA_ECAR) + frazPA * yEcar
          '--- 7) Contributi di maternit�
          '--- 8) Contributi figurativi
          tb1(ijk1 + TB_PA_EFS1) = tb1(ijk1 + TB_PA_EFS1) + frazPA * yEfs1
          tb1(ijk1 + TB_PA_EFI1) = tb1(ijk1 + TB_PA_EFI1) + frazPA * yEfi1
          '--- 9) Contributi non retrocessi
          tb1(ijk1 + TB_PA_EAI1) = tb1(ijk1 + TB_PA_EAI1) + frazPA * yEai1
          '--- 10) Contributi totali
          tb1(ijk1 + TB_PA_ETOT) = tb1(ijk1 + TB_PA_ETOT) + frazPA * yEtot
        End If
      End If
      '--- 20150608LC (fine)
      '--- 20181108LC (inizio) CONSOLIDAMENTO MEF 4D
      If OPZ_DBXMEF <> 0 And yEtot > AA0 Then
        Call MEF1(appl.tbMEF(), dip.sesso, MefX + 1, EMef.Mef_pa_pa, EMef.Mef_PosContr, t + 1, frazPA * yEtot, OPZ_DBXMEF) 'MEF21
        Call MEF1(appl.tbMEF(), dip.sesso, MefX + 1, EMef.Mef_a_a, EMef.Mef_PosContr, t + 1, frazA * yEtot, OPZ_DBXMEF) 'MEF10
      End If
      '--- 20181108LC (fine) CONSOLIDAMENTO MEF 4D
'--- CONSOLIDAMENTO 4 (fine) ---
    End If
  End With
End Sub


Public Sub TIscr2_MovPop_Firr(ByRef dip As TIscr2, ByRef appl As TAppl, ByRef yd As Double, ByRef bFirr As EFirr)
'*********************************************************************************************************************
'SCOPO
'  Calcola il Firr
'VERSIONI
'  20160609LC  creazione
'*********************************************************************************************************************
  Dim anno As Integer
  Dim ijk As Long
  Dim yFirr As Double
  
  If yd > AA0 And bFirr <> EFirr.Firr_None Then
    'se A o PA t+1 perch� lo si paga in t+1, altrimenti t
    anno = IIf(bFirr = Firr_A Or bFirr = Firr_PA, dip.t + 1, dip.t)
    yFirr = ARRV(dip.om_rcV(om_misto_int) * appl.CoeffVar(anno, ecv.cv_Prd_Trmc6))
    If yFirr > AA0 Then
      '--- Uscite ---
      ijk = dip.iSQ + appl.nSize4 * (anno - 1960)
      'appl.tb1(ijk + kzm) = appl.tb1(ijk + kzm) + yd  '.tb(jp).zm
      '--- 201606123C (inizio)
      If bFirr = Firr_NS Then
        appl.tb1(ijk + TB_FIRR_SSUP) = appl.tb1(ijk + TB_FIRR_SSUP) + yd * yFirr  '.tb(jp).zp
      Else
        appl.tb1(ijk + TB_FIRR_USC) = appl.tb1(ijk + TB_FIRR_USC) + yd * yFirr  '.tb(jp).zp
      End If
      If bFirr = Firr_E Or bFirr = Firr_V Or bFirr = Firr_S Or bFirr = Firr_NS Then
        appl.tb1(ijk + TB_OMRC_6) = appl.tb1(ijk + TB_OMRC_6) - yd * yFirr
      End If
      '--- 20160613LC (fine)
      'tb1(ijk + TB_PASS1) = tb1(ijk + TB_PASS1) + .tb(jp).zp * .tb(jp).fa1
      'tb1(ijk + TB_PASS2) = tb1(ijk + TB_PASS2) + .tb(jp).zp * .tb(jp).fa2  '20131126LC
    End If
  End If
End Sub


Public Sub TIscr2_MovPop_Firr_Sup(ByRef dip As TIscr2, ByRef appl As TAppl, ByRef yd As Double, ByRef bFirr As EFirr)
'*********************************************************************************************************************
'SCOPO
'  Calcola il Firr per i superstiti
'VERSIONI
'  20160613LC  creazione
'*********************************************************************************************************************
  Dim ipft&
  Dim iQ&
  Dim tau&
  Dim x#
  Dim yn#
  Dim ys#
  
  If bFirr = EFirr.Firr_A Or bFirr = EFirr.Firr_PA Then
    x = Int(dip.xr + AA5)  '55
    tau = dip.t - appl.t0 - 1
    If tau > P_PFAM_MAX_TAU Then  '20121212LC
      tau = P_PFAM_MAX_TAU        '20121212LC
    End If
    If x <= OPZ_INT_PF_XMAX Then
      iQ = IIf(OPZ_INT_SUP_4 = 1, 0, 2 * (dip.Qualifica - 1)) + dip.sesso  '20121212LC
      ipft = ((iQ - 1) * (1 + OPZ_INT_PF_XMAX - OPZ_INT_PF_XMIN) + (x - OPZ_INT_PF_XMIN)) * (1 + OPZ_OMEGA)
      yn = yd * (1 - appl.pfamD(tau, ipft).sg(0))
    Else
      yn = yd
    End If
    ys = yd - yn
    Call TIscr2_MovPop_Firr(dip, appl, ys, EFirr.Firr_S)
    Call TIscr2_MovPop_Firr(dip, appl, yn, EFirr.Firr_NS)
  End If
End Sub


Private Sub TIscr2_MovPop_New(ByRef dip As TIscr2, ByRef appl As TAppl, ByRef glo As TGlobale, ByRef tb1() As Double, _
                              ByRef fr&, ByRef iop&)
'20140609LC  fr
'20150521LC  iop
  Dim grSudd&, ijk&, t&  '20140609LC '20121207C
  Dim y0#, y1#, y2#, y3#, y4# '20140516LC  'ex 20121224LC
  Dim MefX&  '20181108LC
  
  With dip
  

    
    '--- 20121114LC (inizio) - portato in ReadMDB
    'ReDim .wa((1 + P_DUECENTO) - 1)
    'If OPZ_INT_NO_AE_AB_AI = 0 Then '20121113LC
    '  ReDim .wb((1 + P_DUECENTO) * (1 + P_DUECENTO) - 1)
    'End If
    'ReDim .wc((1 + P_DUECENTO) * (1 + P_DUECENTO) * TWC_SIZE - 1)  '20121204LC
    '--- 20121114LC (fine)
    '**********************
    'Prima inizializzazione
    '**********************
    Call TIscr2_MovPop_0(dip, appl, .s(.t))
    '*******************************
    'Ciclo temporale di elaborazione
    '*******************************
    '--- 20160312LC (inizio)
    OPZ_INT_PENSIONE = OPZ_DBG_PENSIONE
    OPZ_INT_INCRPENS = OPZ_DBG_INCRPENS
    '--- 20160312LC (fine)
    OPZ_INT_TOLL_PENS = OPZ_TOLL_PENS + AA0 '20160420LC
    For t = .tau0 To .AnniElab2
      .tau1 = t
      .iSQT = .iSQ + appl.nSize4 * (t + appl.t0 - 1960)  '20121114LC
      .t = appl.t0 + t
'If .t >= 2035 Then
'  .t = .t
'End If
      .anno = appl.t0 + t
      .cv4 = appl.cv4(.anno, .sesso - 1)
      '**************************
      'Inizializzazione del passo
      '**************************
      If .t - 1 < LBound(.s) Then
        .t0 = .t
      Else
        .t0 = .t - 1
      End If
      '--- 20150528LC-2 (inizio)
      If (.PensAUS = .t) And (.gruppo0 = 5 Or .gruppo0 = 6) Then
        .gruppo0 = 1
        If 1 = 1 Then  '20150612LC-2  (gi� fatto in MovPop9)
        ElseIf .gruppo00 = 5 Then
          .wa(.tau1 - 1).c0a_z = .wa(.tau1 - 1).c0e_z
          .wa(.tau1 - 1).c0e_z = 0
          .wb((.tau1 - 1) * (1 + P_DUECENTO) + 0).eg2 = 0
        Else
          .wa(.tau1 - 1).c0a_z = .wa(.tau1 - 1).c0v_z
          .wa(.tau1 - 1).c0v_z = 0
          .wb((.tau1 - 1) * (1 + P_DUECENTO) + 0).vg2 = 0
        End If
        .rr_dhEff0 = 1
        .rr_dhEff = 1
      End If
      '--- 20150528LC-2 (fine)
      Call TIscr2_MovPop_1(dip, appl, glo, tb1, t, .s(.t0), .s(.t), iop)  '20150521LC
      '--- 20140609LC (inizio)
      If fr > 0 Then
        If Math.Abs(appl.t0 + 0 + OPZ_SUDDIVISIONI_DT - .t) <= 0 Then
          If .s(.t).sm < 0 Then
            grSudd = 0
          ElseIf .gruppo0 = 1 Then
            If .s(.t0).napa > 0 And .s(.t0).bPensAtt = True Then
              grSudd = 2
            Else
              grSudd = 1
            End If
          ElseIf .gruppo0 = 2 Then
            If .s(.t0).napa > 0 Then 'And .s(.t0).bPensAtt = True Then
              grSudd = 2
            'ElseIf .s(.t0).napa <= 0 And .s(.t0).bPensAtt = True Then
            '  grSudd = 0
            Else
              'call MsgBox("TODO - TIscr2_MovPop_New", , .mat)
              'Stop
              grSudd = 0
            End If
          Else
            grSudd = 0
          End If
          '---
          If grSudd > 0 Then
            'Write #fr, .t, .mat), Int(.xr), .gruppo0, grSudd, .s(.t).sm, (.s(.t).sm / appl.CoeffVar(.t, ecv.cv_Sogg2Max)), ReddSudd(.s(.t).sm, appl.CoeffVar(.t, ecv.cv_Sogg2Max))
            Print #fr, .mat; ";"; Int(.s(.t).sm); ";"; ReddSudd(.s(.t).sm, appl.CoeffVar(.t, ecv.cv_Sogg2Max))
          'Else
          '  Write #fr, .mat), .t, Int(.xr), .gruppo0, .s(.t0).bPensAtt, .s(.t0).napa, .s(.t).sm, .s(.t).sm
          End If
        End If
      End If
      '--- 20140609LC (fine)
      '--- 20121208LC (inizio)
'--- CONSOLIDAMENTO 9 (inizio) ---
      If (.xan > OPZ_OMEGA And Math.Abs(dip.wa(t).sj0_z) <= AA0) Or _
          Math.Abs(dip.wa(t).tot_z - dip.wa(t).sd_z) <= AA0 Then
        ijk = .iSQT '+ TB_ZTOT
        '--- 20140516LC (inizio) 'ex 20121224LC
        y0 = .wa(t).tot_z
        y1 = .wa(t).w1b_z
        y2 = .wa(t).w2b_z
        y3 = .wa(t).w3b_z  '20121224LC
        y4 = .wa(t).sd_z
        '--- 20140516LC (fine)
        '--- 20181108LC (inizio) CONSOLIDAMENTO MEF 9A
        If OPZ_DBXMEF <> 0 Then
          MefX = .anno - Year(.DNasc)
        End If
        '--- 20181108LC (fine) CONSOLIDAMENTO MEF 9A
        Do While t < .AnniElab2
          t = t + 1
          ijk = ijk + appl.nSize4
          '--- 20140516LC (inizio) 'ex 20121224LC
          tb1(ijk + TB_ZTOT) = tb1(ijk + TB_ZTOT) + y0
          tb1(ijk + TB_ZW_1) = tb1(ijk + TB_ZW_1) + y1
          tb1(ijk + TB_ZW_2) = tb1(ijk + TB_ZW_2) + y2
          tb1(ijk + TB_ZW_3) = tb1(ijk + TB_ZW_3) + y3
          tb1(ijk + TB_ZD_S) = tb1(ijk + TB_ZD_S) + y4
          '--- 20140516LC (fine)
          '--- 20181108LC (inizio) CONSOLIDAMENTO MEF 9B
          If OPZ_DBXMEF <> 0 Then
            MefX = MefX + 1
            Call MEF1(appl.tbMEF(), dip.sesso, MefX, EMef.Mef_z_z, EMef.Mef_PosFin, t, y1 + y2 + y3 + y4, OPZ_DBXMEF)   'MEF00
          End If
          '--- 20181108LC (fine) CONSOLIDAMENTO MEF 9B
        Loop
        Exit For
      End If
'--- CONSOLIDAMENTO 9 (fine) ---
      '--- 20121208LC (fine)
      '--- 20160312LC (inizio)
      If OPZ_INT_PENSIONE > 0 Then
        OPZ_INT_PENSIONE = ARRV(OPZ_INT_PENSIONE * (1 + appl.CoeffVar(dip.t, ecv.cv_Tp1Al)))
      End If
      If OPZ_INT_INCRPENS > 0 Then
        OPZ_INT_INCRPENS = ARRV(OPZ_INT_INCRPENS * (1 + appl.CoeffVar(dip.t, ecv.cv_Tp1Al)))
      End If
      '--- 20160312LC (fine)
      OPZ_INT_TOLL_PENS = OPZ_INT_TOLL_PENS * (1 + appl.CoeffVar(dip.t, ecv.cv_Tev))  '20160420LC
    Next t
  End With
End Sub


Private Sub TIscr2_MovPop_Sup1b(ByRef dip As TIscr2, ByRef appl As TAppl, ByRef tb1() As Double, ByRef yd1#, ByRef pens#, ByRef iop&)
'ex 20121204LC
'20130302LC (suddivisa)
  If OPZ_ABB_QM_SUP <= 99 Then
    Call TIscr2_MovPop_Sup1b_D(dip, appl, tb1, yd1, pens, iop)
  Else
    Call TIscr2_MovPop_Sup1b_L(dip, appl, tb1, yd1, pens, iop)
  End If
End Sub


Private Sub TIscr2_MovPop_Sup1b_D(ByRef dip As TIscr2, ByRef appl As TAppl, ByRef tb1() As Double, ByRef yd1#, ByRef pens#, ByRef iop&)
'ex 20121204LC
'20130302LC (rinominata)
  Dim ijk&, ipf&, ipft&, iTS&, iQ&, iwc&, j&, t&, tau&, t2&, x& '20121204LC
  Dim dy#, fact#, pens1#, y#, y0#, y2#  '20121210LC
  Static s_y#, s_yp#

  Dim idx As Long
  Dim var_V As Double     'Numero di coniugi soli
  Dim var_VO As Double    'Coniugi 1 orfano
  Dim var_VOO As Double   'Coniugi 2 orfani
  Dim var_O As Double     'Orfano
  Dim var_OO As Double    '2 Orfani
  Dim var_OOO As Double   '3 Orfani
  Dim a, b, aliqMed As Double
  idx = 0
  Dim MefS&, MefY&  '20181108LC
  Dim MefV#         '20181108LC

  If iop = 1 Then 'Inizializzazione
    s_y = 0
    s_yp = 0
  ElseIf iop = 2 Then
    If yd1 < AA0 Then Exit Sub
    s_y = s_y + yd1
    s_yp = s_yp + yd1 * pens
  ElseIf iop = 3 Then 'Finalizzazione
    If s_y < AA0 Then Exit Sub
    With dip
      t = .tau1
      'iQ = .iQ + 1
      iQ = IIf(OPZ_INT_SUP_4 = 1, 0, 2 * (dip.Qualifica - 1)) + dip.sesso  '20121212LC
      iTS = .iTS + 1
      '--- Superstiti
      x = Int(.xr + AA5)
      '--- 20121210LC (inizio)
      fact = appl.CoeffVar(appl.t0 + t, ecv.cv_Av)
      If fact < 0 Or fact > 1 Then fact = 1
      '--- 20121210LC (fine)
If TMP_TEST = 0 Or (TMP_TEST = 1 And x = 58) Then
      '--- 20121204LC (inizio)
      tau = .t - appl.t0 - 1
      If tau > P_PFAM_MAX_TAU Then  '20121212LC
        tau = P_PFAM_MAX_TAU        '20121212LC
      End If
      If x <= OPZ_INT_PF_XMAX Then
        'ipf = ((.sesso - 1) * (1 + OPZ_INT_PF_XMAX - OPZ_INT_PF_XMIN) + (x - OPZ_INT_PF_XMIN)) * (1 + OPZ_OMEGA)
        ipf = ((iQ - 1) * (1 + OPZ_INT_PF_XMAX - OPZ_INT_PF_XMIN) + (x - OPZ_INT_PF_XMIN)) * (1 + OPZ_OMEGA)
      '--- 20121204LC (fine)
        ipft = ipf
        y = s_y * (1 - appl.pfamD(tau, ipft).sg(0))
        '20121204LC
        'wa1.w3b_z = wa1.w3b_z + y
        .wa(t).w3b_z = .wa(t).w3b_z + y
''''.wa(t + 1).sd_z = wa1.sd_z  '20121115LC
        '--- Popolazione ---
        y2 = 0
        For j = 0 To 6  '20121204LC
          dy = 0#  '20121204LC
          iwc = (t * (1 + P_DUECENTO) + t) * TWC_SIZE + j '20121204LC
          ipft = ipf
          For t2 = t To appl.nMax2
            'iwc = (t * (1 + P_DUECENTO) + t2) * TWC_SIZE + j '20121204LC
            'ipft = ipf + t2 - t
            '--- Popolazione ---
            y0 = y2
            If t2 = t Then
              y2 = s_y * appl.pfamD(tau, ipft).sg(j)
              '.wc(iwc) = .wc(iwc) + y2    '20121204LC
              .wc(iwc) = y2  '20121207LC
            ElseIf t2 - t > OPZ_OMEGA Then '20081118LC
              If j = 0 Then
                .wa(t2).sd_z = .wa(t2).sd_z + dy
              Else
                Exit For
              End If
            Else
              y2 = s_y * appl.pfamD(tau, ipft).sg(j)
              '--- 20121204LC (inizio)
              '.wc(iwc) = .wc(iwc) + y2
              .wc(iwc) = y2  '20121207LC
              If j = 0 Then
                'dy = y0 - y2
                dy = dy + y0 - y2
                'wa1.sd_z = wa1.sd_z + dy
                .wa(t2).sd_z = .wa(t2).sd_z + dy
              End If
              '--- 20121204LC (fine)
            End If
            If j > 0 Then '20121204LC
              If y2 <= AA0 Then
                If y0 > AA0 Then
                  Exit For
                End If
              End If
            End If
            '---
            iwc = iwc + TWC_SIZE  '20121204LC
            ipft = ipft + 1
          Next t2
        Next j
        '--- Spesa pensioni indirette ---
        pens1 = s_yp / s_y
        ijk = dip.iSQT '4 '20121028LC
        ipft = ipf
        For t2 = t To appl.nMax2
          'ipft = ipf + t2 - t
          '---
          .wa(t2).s1_zp = pens1 * s_y * appl.pfamD(tau, ipft).sg(8)
          .wa(t2).s1_zm = s_y * appl.pfamD(tau, ipft).sg(0)
          If t2 = t Then
            '--- 20150610LC (inizio) ex 20140312LC
            .wa(t2).s1_zp = .wa(t2).s1_zp * 0.5
            .wa(t2).s1_zm = .wa(t2).s1_zm * 0.5
            '--- 20150610LC (fine)
          ElseIf appl.pfamD(tau, ipft - 1).sg(8) <= AA0 Then
            Exit For
          Else
            .wa(t2).s1_zp = (.wa(t2).s1_zp + pens1 * s_y * appl.pfamD(tau, ipft - 1).sg(8)) * 0.5
            .wa(t2).s1_zm = (.wa(t2).s1_zm + s_y * appl.pfamD(tau, ipft - 1).sg(0)) * 0.5
          End If
          '---
          'If OPZ_TB = 1 Then
          '  ijk = appl.nSize2 * iTS + appl.nSize3 * iQ + appl.nSize4 * (t2 + appl.t0 - 1960) '20121114LC   '4
          'Else
          '  'ijk = dip.iSQ + appl.nSize4 * (t2 + appl.t0 - 1960) '20121114LC '4
          'End If
          '---
'--- CONSOLIDAMENTO 5(inizio) ---
          tb1(ijk + TB_MPS) = tb1(ijk + TB_MPS) + .wa(t2).s1_zp   '.z1(t2).zp.ys1
          tb1(ijk + TB_SPS) = tb1(ijk + TB_SPS) + .wa(t2).s1_zm '.z1(t2).zm.ys1
'--- CONSOLIDAMENTO 5 (fine) ---
          '--- 20121207LC (inizio)  'ex 20120213LC
          
          '--- 20181108LC (inizio) CONSOLIDAMENTO MEF 5A
          If OPZ_DBXMEF <> 0 Then  'MEF34
            If t2 = t Then
              If Math.Abs(OPZ_DBXMEF) = 1 Then
                MefS = 3 - dip.sesso
                '--- 20171024LC (inizio)
                If dip.anno - Year(dip.DNasc) <= OPZ_OMEGA Then
                  MefY = appl.pf(dip.anno - Year(dip.DNasc), dip.sesso, epf.pf_yv)
                Else
                  MefY = appl.pf(OPZ_OMEGA, dip.sesso, epf.pf_yv)
                End If
                '--- 20171024LC (fine)
                '--- 20181120LC (inizio)
                If MefY = 0 Then
                  If dip.anno - Year(dip.DNasc) <= OPZ_OMEGA Then
                    MefY = dip.anno - Year(dip.DNasc)
                  Else
                    MefY = OPZ_OMEGA
                  End If
                End If
                '--- 20181120LC (fine)
              Else
                MefS = dip.sesso
                MefY = dip.anno - Year(dip.DNasc)
              End If
            End If
            MefV = s_y * appl.pfamD(tau, ipft).sg(IIf(Math.Abs(OPZ_DBXMEF) <= 2, 7, 0))
            If t2 = t Then
              Call MEF1(appl.tbMEF(), MefS, MefY + t2 - t, EMef.Mef_pn_ps, EMef.Mef_PosFin, t2, MefV, OPZ_DBXMEF)
              Call MEF2(appl.tbMEF(), MefS, MefY + t2 - t, EMef.Mef_pn_ps, t2, .wa(t2).s1_zp, pens1 * s_y * appl.pfamD(tau, ipft).sg(8))
            Else
              Call MEF1(appl.tbMEF(), MefS, MefY + t2 - t, EMef.Mef_ps_ps, EMef.Mef_PosFin, t2, MefV, OPZ_DBXMEF)
              Call MEF2(appl.tbMEF(), MefS, MefY + t2 - t, EMef.Mef_ps_ps, t2, .wa(t2).s1_zp, pens1 * s_y * appl.pfamD(tau, ipft).sg(8))
            End If
          End If
          '--- 20181108LC (inizio) CONSOLIDAMENTO MEF 5A
          '--- CONSOLIDAMENTO 5 (fine) ---
          
          If OPZ_PEREQ = 1 Then
            If OPZ_INT_SUP_RP = 0 Then
              pens1 = TIscr2_Pens(appl.CoeffVar, .t + 1 + t2 - t, pens1)
            ElseIf OPZ_INT_SUP_RP = 2 Then 'Mod MDG
            
              'Indice per recuperare i dati nel MegaVettore
              idx = appl.nSize2 * iTS + appl.nSize3 * (.iQ + 1) + appl.nSize4 * (t2 - 1 + appl.t0 - 1960)
              
              'Nuclueo superstite - sviluppo foglio deceduti
              var_V = tb1(idx + TB_ZPS_V)
              var_VO = tb1(idx + TB_ZPS_VO)
              var_VOO = tb1(idx + TB_ZPS_VOO)
              var_O = tb1(idx + TB_ZPS_O)
              var_OO = tb1(idx + TB_ZPS_OO)
              var_OOO = tb1(idx + TB_ZPS_OOO)
            
            
              a = ((var_V * appl.CoeffVar(.t, ecv.cv_Av)) + (var_VO * appl.CoeffVar(.t, ecv.cv_Avo)) + (var_VOO * appl.CoeffVar(.t, ecv.cv_Avoo)) + _
                        (var_O * appl.CoeffVar(.t, ecv.cv_Ao)) + (var_OO * appl.CoeffVar(.t, ecv.cv_Aoo)) + (var_OOO * appl.CoeffVar(.t, ecv.cv_Aooo)))
              b = (var_V + var_VO + var_VOO + var_O + var_OO + var_OOO)
              If a = 0 Or b = 0 Then
                aliqMed = 0
              Else
                aliqMed = a / b
              End If
              If aliqMed <= 0 Then
                aliqMed = 1
              End If
                
              pens1 = TIscr2_Pens(appl.CoeffVar, .t + 1 + t2 - t, pens1 * aliqMed) / aliqMed
              vecAliqMed(t2) = aliqMed
            Else
              pens1 = TIscr2_Pens(appl.CoeffVar, .t + 1 + t2 - t, pens1 * fact) / fact  '20121210LC
            End If
          Else
            If OPZ_INT_SUP_RP = 0 Then
              pens1 = TIscr2_Pens(appl.CoeffVar, .t + 1 + t2 - t, pens1)
            Else
              pens1 = TIscr2_Pens(appl.CoeffVar, .t + 1 + t2 - t, pens1 * fact) / fact  '20121210LC
            End If
          End If
          


          '--- 20121207LC (fine)
          ijk = ijk + appl.nSize4 '4
          ipft = ipft + 1
        Next t2
      '--- 20160411LC (inizio)
      Else
        y = s_y
        .wa(t).w3b_z = .wa(t).w3b_z + y
      '--- 20160411LC (fine)
      End If
End If
    End With
  End If
End Sub


Private Sub TIscr2_MovPop_Sup1b_L(ByRef dip As TIscr2, ByRef appl As TAppl, ByRef tb1() As Double, ByRef yd1#, ByRef pens#, ByRef iop&)
'20130302LC (nuova)
  Const ZW = 0.000001  '20130302LC
  Dim ijk&, ipf&, ipft&, iTS&, iQ&, iwc&, j&, t&, tau&, t2&, x& '20121204LC
  Dim dy#, fact#, pens1#, y#, y0#, y2#  '20121210LC
  Static s_y#, s_yp#

  Dim idx As Long
  Dim var_V As Double     'Numero di coniugi soli
  Dim var_VO As Double    'Coniugi 1 orfano
  Dim var_VOO As Double   'Coniugi 2 orfani
  Dim var_O As Double     'Orfano
  Dim var_OO As Double    '2 Orfani
  Dim var_OOO As Double   '3 Orfani
  Dim a, b, aliqMed As Double
  idx = 0
  
  Dim MefS&, MefY&  '20181108LC
  Dim MefV#         '20181108LC

  If iop = 1 Then 'Inizializzazione
    s_y = 0
    s_yp = 0
  ElseIf iop = 2 Then
    If yd1 < AA0 Then Exit Sub
    s_y = s_y + yd1
    s_yp = s_yp + yd1 * pens
  ElseIf iop = 3 Then 'Finalizzazione
    If s_y < AA0 Then Exit Sub
    With dip
      t = .tau1
      'iQ = .iQ + 1
      iQ = IIf(OPZ_INT_SUP_4 = 1, 0, 2 * (dip.Qualifica - 1)) + dip.sesso  '20121212LC
      iTS = .iTS + 1
      '--- Superstiti
      x = Int(.xr + AA5)
      '--- 20121210LC (inizio)
      fact = appl.CoeffVar(appl.t0 + t, ecv.cv_Av)
      If fact < 0 Or fact > 1 Then fact = 1
      '--- 20121210LC (fine)
If TMP_TEST = 0 Or (TMP_TEST = 1 And x = 58) Then
      '--- 20121204LC (inizio)
      tau = .t - appl.t0 - 1
      If tau > P_PFAM_MAX_TAU Then  '20121212LC
        tau = P_PFAM_MAX_TAU        '20121212LC
      End If
      If x <= OPZ_INT_PF_XMAX Then
        'ipf = ((.sesso - 1) * (1 + OPZ_INT_PF_XMAX - OPZ_INT_PF_XMIN) + (x - OPZ_INT_PF_XMIN)) * (1 + OPZ_OMEGA)
        ipf = ((iQ - 1) * (1 + OPZ_INT_PF_XMAX - OPZ_INT_PF_XMIN) + (x - OPZ_INT_PF_XMIN)) * (1 + OPZ_OMEGA)
      '--- 20121204LC (fine)
        ipft = ipf
        y = s_y * (1 - ZW * appl.pfamL(tau, ipft).sg(0))
        '20121204LC
        'wa1.w3b_z = wa1.w3b_z + y
        .wa(t).w3b_z = .wa(t).w3b_z + y
''''.wa(t + 1).sd_z = wa1.sd_z  '20121115LC
        '--- Popolazione ---
        y2 = 0
        For j = 0 To 6  '20121204LC
          dy = 0#  '20121204LC
          iwc = (t * (1 + P_DUECENTO) + t) * TWC_SIZE + j '20121204LC
          ipft = ipf
          For t2 = t To appl.nMax2
            'iwc = (t * (1 + P_DUECENTO) + t2) * TWC_SIZE + j '20121204LC
            'ipft = ipf + t2 - t
            '--- Popolazione ---
            y0 = y2
            If t2 = t Then
              y2 = s_y * ZW * appl.pfamL(tau, ipft).sg(j)
              '.wc(iwc) = .wc(iwc) + y2    '20121204LC
              .wc(iwc) = y2  '20121207LC
            ElseIf t2 - t > OPZ_OMEGA Then '20081118LC
              If j = 0 Then
                .wa(t2).sd_z = .wa(t2).sd_z + dy
              Else
                Exit For
              End If
            Else
              y2 = s_y * ZW * appl.pfamL(tau, ipft).sg(j)
              '--- 20121204LC (inizio)
              '.wc(iwc) = .wc(iwc) + y2
              .wc(iwc) = y2  '20121207LC
              If j = 0 Then
                'dy = y0 - y2
                dy = dy + y0 - y2
                'wa1.sd_z = wa1.sd_z + dy
                .wa(t2).sd_z = .wa(t2).sd_z + dy
              End If
              '--- 20121204LC (fine)
            End If
            If j > 0 Then '20121204LC
              If y2 <= AA0 Then
                If y0 > AA0 Then
                  Exit For
                End If
              End If
            End If
            '---
            iwc = iwc + TWC_SIZE  '20121204LC
            ipft = ipft + 1
          Next t2
        Next j
        '--- Spesa pensioni indirette ---
        pens1 = s_yp / s_y
        ijk = dip.iSQT '4 '20121028LC
        ipft = ipf
        For t2 = t To appl.nMax2
          'ipft = ipf + t2 - t
          '---
          .wa(t2).s1_zp = pens1 * s_y * ZW * appl.pfamL(tau, ipft).sg(8)
          .wa(t2).s1_zm = s_y * ZW * appl.pfamL(tau, ipft).sg(0)
          If t2 = t Then
            '--- 20150610LC (inizio) ex 20140312LC
            .wa(t2).s1_zp = .wa(t2).s1_zp * 0.5
            .wa(t2).s1_zm = .wa(t2).s1_zm * 0.5
            '--- 20150610LC (fine)
          ElseIf ZW * appl.pfamL(tau, ipft - 1).sg(8) <= AA0 Then
            Exit For
          Else
            .wa(t2).s1_zp = (.wa(t2).s1_zp + pens1 * s_y * ZW * appl.pfamL(tau, ipft - 1).sg(8)) * 0.5
            .wa(t2).s1_zm = (.wa(t2).s1_zm + s_y * ZW * appl.pfamL(tau, ipft - 1).sg(0)) * 0.5
          End If
          '---
          'If OPZ_TB = 1 Then
          '  ijk = appl.nSize2 * iTS + appl.nSize3 * iQ + appl.nSize4 * (t2 + appl.t0 - 1960) '20121114LC   '4
          'Else
          '  'ijk = dip.iSQ + appl.nSize4 * (t2 + appl.t0 - 1960) '20121114LC '4
          'End If
          '---
'--- CONSOLIDAMENTO 6 (fine) ---
          tb1(ijk + TB_MPS) = tb1(ijk + TB_MPS) + .wa(t2).s1_zp   '.z1(t2).zp.ys1
          tb1(ijk + TB_SPS) = tb1(ijk + TB_SPS) + .wa(t2).s1_zm '.z1(t2).zm.ys1
'--- CONSOLIDAMENTO 6 (fine) ---
          '--- 20121207LC (inizio)  'ex 20120213LC
          '--- 20181108LC (inizio) CONSOLIDAMENTO MEF 6A
          If OPZ_DBXMEF <> 0 Then  'MEF34
            If t2 = t Then
              If Math.Abs(OPZ_DBXMEF) = 1 Then
                MefS = 3 - dip.sesso
                '--- 20171024LC (inizio)
                If dip.anno - Year(dip.DNasc) <= OPZ_OMEGA Then
                  MefY = appl.pf(dip.anno - Year(dip.DNasc), dip.sesso, epf.pf_yv)
                Else
                  MefY = appl.pf(OPZ_OMEGA, dip.sesso, epf.pf_yv)
                End If
                '--- 20171024LC (fine)
                '--- 20181120LC (inizio)
                If MefY = 0 Then
                  If dip.anno - Year(dip.DNasc) <= OPZ_OMEGA Then
                    MefY = dip.anno - Year(dip.DNasc)
                  Else
                    MefY = OPZ_OMEGA
                  End If
                End If
                '--- 20181120LC (fine)
              Else
                MefS = dip.sesso
                MefY = dip.anno - Year(dip.DNasc)
              End If
            End If
            MefV = s_y * ZW * appl.pfamD(tau, ipft).sg(IIf(Math.Abs(OPZ_DBXMEF) <= 2, 7, 0))
            If t2 = t Then
              Call MEF1(appl.tbMEF(), MefS, MefY + t2 - t, EMef.Mef_pn_ps, EMef.Mef_PosFin, t2, MefV, OPZ_DBXMEF)
              Call MEF2(appl.tbMEF(), MefS, MefY + t2 - t, EMef.Mef_pn_ps, t2, .wa(t2).s1_zp, pens1 * s_y * ZW * appl.pfamL(tau, ipft).sg(8))
            Else
              Call MEF1(appl.tbMEF(), MefS, MefY + t2 - t, EMef.Mef_ps_ps, EMef.Mef_PosFin, t2, MefV, OPZ_DBXMEF)
              Call MEF2(appl.tbMEF(), MefS, MefY + t2 - t, EMef.Mef_ps_ps, t2, .wa(t2).s1_zp, pens1 * s_y * ZW * appl.pfamL(tau, ipft).sg(8))
            End If
          End If
          '--- 20181108LC (fine) CONSOLIDAMENTO MEF 6A
          '--- CONSOLIDAMENTO 6 (fine) ---
          
          If OPZ_PEREQ = 1 Then
            If OPZ_INT_SUP_RP = 0 Then
              pens1 = TIscr2_Pens(appl.CoeffVar, .t + 1 + t2 - t, pens1)
            ElseIf OPZ_INT_SUP_RP = 2 Then 'Mod MDG
            
              'Indice per recuperare i dati nel MegaVettore
              idx = appl.nSize2 * iTS + appl.nSize3 * (.iQ + 1) + appl.nSize4 * (t2 - 1 + appl.t0 - 1960)
              
              'Nuclueo superstite - sviluppo foglio deceduti
              var_V = tb1(idx + TB_ZPS_V)
              var_VO = tb1(idx + TB_ZPS_VO)
              var_VOO = tb1(idx + TB_ZPS_VOO)
              var_O = tb1(idx + TB_ZPS_O)
              var_OO = tb1(idx + TB_ZPS_OO)
              var_OOO = tb1(idx + TB_ZPS_OOO)
              
              
              a = ((var_V * appl.CoeffVar(.t, ecv.cv_Av)) + (var_VO * appl.CoeffVar(.t, ecv.cv_Avo)) + (var_VOO * appl.CoeffVar(.t, ecv.cv_Avoo)) + _
                  (var_O * appl.CoeffVar(.t, ecv.cv_Ao)) + (var_OO * appl.CoeffVar(.t, ecv.cv_Aoo)) + (var_OOO * appl.CoeffVar(.t, ecv.cv_Aooo)))
              b = (var_V + var_VO + var_VOO + var_O + var_OO + var_OOO)
              If a = 0 Or b = 0 Then
                aliqMed = 0
              Else
                aliqMed = a / b
              End If
              If aliqMed <= 0 Then
                aliqMed = 1
              End If
              
              pens1 = TIscr2_Pens(appl.CoeffVar, .t + 1 + t2 - t, pens1 * aliqMed) / aliqMed
              vecAliqMed(t2) = aliqMed
            Else
              pens1 = TIscr2_Pens(appl.CoeffVar, .t + 1 + t2 - t, pens1 * fact) / fact  '20121210LC
            End If
          Else
            If OPZ_INT_SUP_RP = 0 Then
              pens1 = TIscr2_Pens(appl.CoeffVar, .t + 1 + t2 - t, pens1)
            Else
              pens1 = TIscr2_Pens(appl.CoeffVar, .t + 1 + t2 - t, pens1 * fact) / fact  '20121210LC
            End If
          End If
          

          ijk = ijk + appl.nSize4 '4
          ipft = ipft + 1
        Next t2
      '--- 20160411LC (inizio)
      Else
        y = s_y
        .wa(t).w3b_z = .wa(t).w3b_z + y
      '--- 20160411LC (fine)
      End If
End If
    End With
  End If
End Sub


'20140425LC (pfan4) '20080915LC
Private Sub TIscr2_MovPop_Sup4(ByRef dip As TIscr2, ByRef appl As TAppl, pfam4() As TWFD, tb1#(), t&, yd#, pens#)
  Dim ijk&, iTS&, iQ&, j&, t2&, tau&, x& '20121204LC
  Dim fact#, pens1#  '20121210LC
  
  Dim idx As Long
  Dim var_V As Double     'Numero di coniugi soli
  Dim var_VO As Double    'Coniugi 1 orfano
  Dim var_VOO As Double   'Coniugi 2 orfani
  Dim var_O As Double     'Orfano
  Dim var_OO As Double    '2 Orfani
  Dim var_OOO As Double   '3 Orfani
  Dim a, b, aliqMed As Double
  idx = 0
  Dim MefS&, MefY&  '20181108LC
  Dim MefV#         '20181108LC
  
  If yd < AA0 Then Exit Sub
  With dip
    x = Int(.xr + AA5)
    '--- 20121210LC (inizio)
    fact = .sup(0).rev
    If fact <= 0 Or fact > 1 Then fact = 1
    '--- 20121210LC (fine)
    'iSex = .sesso  '20121204LC (commentato)
    'iQ = .iQ + 1
    iQ = IIf(OPZ_INT_SUP_4 = 1, 0, 2 * (dip.Qualifica - 1)) + dip.sesso  '20121212LC
    iTS = .iTS + 1
      '--- Spesa pensioni indirette ---
    pens1 = pens
    ijk = .iSQT '4 '20121028LC
    '--- 20121203LC (inizio)
    tau = .t - appl.t0
    If tau <> 0 Then
      Call MsgBox("TODO TIscr2_MovPop_Sup4-1")
      Stop
    End If
    '--- 20121203LC (fine)
    For t2 = t To appl.nMax2
      .wa(t2).sj0_z = .wa(t2).sj0_z + yd * pfam4(t2 - t).sg(0)
      .wa(t2).sj1_z = .wa(t2).sj1_z + yd * pfam4(t2 - t).sg(1)
      .wa(t2).sj2_z = .wa(t2).sj2_z + yd * pfam4(t2 - t).sg(2)
      .wa(t2).sj3_z = .wa(t2).sj3_z + yd * pfam4(t2 - t).sg(3)
      .wa(t2).sj4_z = .wa(t2).sj4_z + yd * pfam4(t2 - t).sg(4)
      .wa(t2).sj5_z = .wa(t2).sj5_z + yd * pfam4(t2 - t).sg(5)
      .wa(t2).sj6_z = .wa(t2).sj6_z + yd * pfam4(t2 - t).sg(6)
      .wa(t2).sj7_z = .wa(t2).sj7_z + yd * pfam4(t2 - t).sg(7)
      .wa(t2).sj8_z = .wa(t2).sj8_z + yd * pfam4(t2 - t).sg(8)
      '---
      .wa(t2).s1_zp = pens1 * yd * pfam4(t2 - t).sg(8)
      .wa(t2).s1_zm = yd * pfam4(t2 - t).sg(0)
      If t2 = t Then
        '20121204LC: TODO controllare (qui le 2 istruzioni non erano commentate, nel vecchio programma si)
        '.wa( t2).s1_zp = .wa( t2).s1_zp * 0.5
        '.wa( t2).s1_zm = .wa( t2).s1_zm * 0.5
      ElseIf pfam4(t2 - t - 1).sg(8) <= AA0 Then
        Exit For
      Else
        .wa(t2).s1_zp = (.wa(t2).s1_zp + pens1 * yd * pfam4(t2 - t - 1).sg(8)) * 0.5
        .wa(t2).s1_zm = (.wa(t2).s1_zm + yd * pfam4(t2 - t - 1).sg(0)) * 0.5
      End If
'--- CONSOLIDAMENTO 7 (inizio) ---
      If OPZ_TB = 1 Then
        ijk = appl.nSize2 * iTS + appl.nSize3 * iQ + appl.nSize4 * (t2 + appl.t0 - 1960)  '20121114LC
      Else
        'ijk = .iSQ + appl.nSize4 * ( t2 + appl.t0 - 1960) '20121114LC
        'ijk = .iSQT
      End If
      tb1(ijk + TB_MPS) = tb1(ijk + TB_MPS) + .wa(t2).s1_zp   '20121028LC  '.z1( t2).zp.ys1
      tb1(ijk + TB_SPS) = tb1(ijk + TB_SPS) + .wa(t2).s1_zm  '20121028LC  '.z1( t2).zm.ys1
      ijk = ijk + appl.nSize4
'--- CONSOLIDAMENTO 7 (fine) ---
      '--- 20121207LC (inizio)  'ex 20120213LC
      '--- 20181108LC (inizio) CONSOLIDAMENTO MEF 7A
      If OPZ_DBXMEF <> 0 Then  'MEF34
        If t2 = t Then
          If Math.Abs(OPZ_DBXMEF) = 1 Then
            If dip.gruppo0 = 4 And dip.nSup > 0 Then
              MefS = dip.sup(0).s
              MefY = dip.anno - Year(dip.sup(0).DNasc)
            Else
              MefS = 3 - dip.sesso
              If appl.bProb_Superst = True Then
                MefY = appl.pf(dip.anno - Year(dip.DNasc), dip.sesso, epf.pf_yv)
              Else
                MefY = dip.anno - Year(dip.DNasc)
              End If
            End If
          Else
            MefS = dip.sesso
            MefY = dip.anno - Year(dip.DNasc)
          End If
        End If
        MefV = yd * pfam4(t2 - t).sg(IIf(Math.Abs(OPZ_DBXMEF) <= 2, 7, 0))
        If t2 = t Then
          Call MEF1(appl.tbMEF(), MefS, MefY + t2 - t, EMef.Mef_pn_ps, EMef.Mef_PosFin, t2, MefV, OPZ_DBXMEF)
          Call MEF2(appl.tbMEF(), MefS, MefY + t2 - t, EMef.Mef_pn_ps, t2, .wa(t2).s1_zp, pens1 * yd * pfam4(t2 - t).sg(8))
        Else
          Call MEF1(appl.tbMEF(), MefS, MefY + t2 - t, EMef.Mef_ps_ps, EMef.Mef_PosFin, t2, MefV, OPZ_DBXMEF)
          Call MEF2(appl.tbMEF(), MefS, MefY + t2 - t, EMef.Mef_ps_ps, t2, .wa(t2).s1_zp, pens1 * yd * pfam4(t2 - t).sg(8))
        End If
      End If
      '--- 20181108LC (fine) CONSOLIDAMENTO MEF 7A
'--- CONSOLIDAMENTO 7 (fine) ---

      If OPZ_PEREQ = 1 Then
        If OPZ_INT_SUP_RP = 0 Then
          pens1 = TIscr2_Pens(appl.CoeffVar, .t + 1 + t2 - t, pens1)
        ElseIf OPZ_INT_SUP_RP = 2 Then 'Mod MDG
            
          'Indice per recuperare i dati nel MegaVettore
          idx = appl.nSize2 * iTS + appl.nSize3 * (.iQ + 1) + appl.nSize4 * (t2 - 1 + appl.t0 - 1960)
          
          'Nuclueo superstite - sviluppo foglio deceduti
          var_V = tb1(idx + TB_ZPS_V)
          var_VO = tb1(idx + TB_ZPS_VO)
          var_VOO = tb1(idx + TB_ZPS_VOO)
          var_O = tb1(idx + TB_ZPS_O)
          var_OO = tb1(idx + TB_ZPS_OO)
          var_OOO = tb1(idx + TB_ZPS_OOO)
          
          
          a = ((var_V * appl.CoeffVar(.t, ecv.cv_Av)) + (var_VO * appl.CoeffVar(.t, ecv.cv_Avo)) + (var_VOO * appl.CoeffVar(.t, ecv.cv_Avoo)) + _
                    (var_O * appl.CoeffVar(.t, ecv.cv_Ao)) + (var_OO * appl.CoeffVar(.t, ecv.cv_Aoo)) + (var_OOO * appl.CoeffVar(.t, ecv.cv_Aooo)))
          b = (var_V + var_VO + var_VOO + var_O + var_OO + var_OOO)
          If a = 0 Or b = 0 Then
            aliqMed = 0
          Else
            aliqMed = a / b
          End If
          If aliqMed <= 0 Then
            aliqMed = 1
          End If
              
          pens1 = TIscr2_Pens(appl.CoeffVar, .t + 1 + t2 - t, pens1 * aliqMed) / aliqMed
          vecAliqMed(t2) = aliqMed
        Else
          pens1 = TIscr2_Pens(appl.CoeffVar, .t + 1 + t2 - t, pens1 * fact) / fact  '20121210LC
        End If
      Else
        If OPZ_INT_SUP_RP = 0 Then
          pens1 = TIscr2_Pens(appl.CoeffVar, .t + 1 + t2 - t, pens1)
        Else
          pens1 = TIscr2_Pens(appl.CoeffVar, .t + 1 + t2 - t, pens1 * fact) / fact  '20121210LC
        End If
      End If
      
      

    Next t2
  End With
End Sub


Private Sub TIscr2_MovPop1_dh(ByRef dip As TIscr2)
  '*********************************
  'Calcola l'incremento di anzianit�
  '*********************************
  With dip
    'If .n0.c0e_z > AA0  Then
    If .gruppo0 = 5 Then  '20150526LC
      .rr_dhEff = 0
    'ElseIf Not (.TipoElab = 0 Or .TipoElab > .t) Then '20111209LC
    ElseIf .TipoElab = 1 Then '20111214LC
      .rr_dhEff = 1 '20120523LC
    Else
      .rr_dhEff = 1
    End If
    '20111213LC (inizio)
    If .rr_dhEff0 = 0 And .rr_dhEff > 0 Then
      .rr_dhEff0 = .rr_dhEff
    End If
    '20111213LC (fine)
  End With
End Sub


Private Function TIscr2_MovPop1_hea(ByRef dip As TIscr2, t&, t1&, fraz#) As Double
  '******************************************
  'Calcola l'anzianit� per il nuovo ex-attivo
  '******************************************
  'per dip.gruppo = 5 t1=1,fraz=0
  Dim dhea#
  
  With dip
    dhea = (t - t1 - fraz) * .rr_dhMax
    '20111219LC (inizio)
    If 9 = 9 And .anno1 > .anno0 Then '20111230LC
      dhea = dhea - (.anno1 - .anno0)
    End If
    '20111219LC (fine)
    'non si sa mai
    If dhea < 0 Then dhea = 0
    TIscr2_MovPop1_hea = .s(.t).AnzTot - dhea
  End With
End Function


Public Function TIscr2_Pens(ByRef CoeffVar() As Double, ByRef anno As Integer, ByVal p0 As Double) As Double
'*********************************************************************************************************************
'VERSIONI
'  20120213LC  da C4
'  20161112LC  tutta, 5 scaglioni
'*********************************************************************************************************************
  Dim p#, p1#, p2#, p3#, p4#, p5#, riv# '20161112LC

  p = p0
  If OPZ_MOD_REG_3 = 0 Or anno < OPZ_MOD_REG_3_ANNO Then
    '--- 20161114LC (inizio)
    riv = p * CoeffVar(anno, ecv.cv_Tp1Al)
    p = p + riv
    '--- 20161113LC (fine)
  Else
    If OPZ_MOD_REG_3_PERC_ENAS > 0 And OPZ_MOD_REG_3_PERC_ENAS < 1 Then
      p = p0 / OPZ_MOD_REG_3_PERC_ENAS
    End If
    
    p1 = CoeffVar(anno, ecv.cv_Tp1Max)
    If p <= p1 Then
      riv = p * CoeffVar(anno, ecv.cv_Tp1Al)
    Else
      riv = p1 * CoeffVar(anno, ecv.cv_Tp1Al)
      p2 = CoeffVar(anno, ecv.cv_Tp2Max)
      If p <= p2 Then
        riv = riv + (p - p1) * CoeffVar(anno, ecv.cv_Tp2Al)  '20170406LC (mancava riv +)
      Else
        riv = riv + (p2 - p1) * CoeffVar(anno, ecv.cv_Tp2Al)
        p3 = CoeffVar(anno, ecv.cv_Tp3Max)
        If p <= p3 Then
          riv = riv + (p - p2) * CoeffVar(anno, ecv.cv_Tp3Al)
        Else
          riv = riv + (p3 - p2) * CoeffVar(anno, ecv.cv_Tp3Al)
          p4 = CoeffVar(anno, ecv.cv_Tp4Max)
          If p <= p4 Then
            riv = riv + (p - p3) * CoeffVar(anno, ecv.cv_Tp4Al)
          Else
            riv = riv + (p4 - p3) * CoeffVar(anno, ecv.cv_Tp4Al)
            p5 = CoeffVar(anno, ecv.cv_Tp5Max)
            If p <= p5 Then
              riv = riv + (p - p4) * CoeffVar(anno, ecv.cv_Tp5Al)
            Else
              riv = riv + (p5 - p4) * CoeffVar(anno, ecv.cv_Tp5Al)
            End If
          End If
        End If
      End If
    End If
    p = p + riv
    If OPZ_MOD_REG_3 <> 0 And anno >= OPZ_MOD_REG_3_ANNO And OPZ_MOD_REG_3_PERC_ENAS > 0 And OPZ_MOD_REG_3_PERC_ENAS < 1 Then
      p = p * OPZ_MOD_REG_3_PERC_ENAS
    End If
  End If
  '--- 20161112LC (fine)
ExitPoint:
  TIscr2_Pens = ARRV(p)  '20121203LC
End Function


Public Sub TIscr2_PensDirInd1(ByRef dip As TIscr2, wb0 As TWB, wb1 As TWB, ByRef appl As TAppl, pens1a1#, pens1a2#, fraz#, t&, t1&, bTipoPens As etp, iop&)
'  20140111LC-12
'  20121103LC (tutta)
'  20150504LC  iop
'  20150526LC  wb1
  Dim bVA As Byte  '20150504LC
  Dim bEA As Byte
  Dim itp%, te%, j%  '20130221LC
  Dim kpa&  '20140319LC-02
  Dim ax#, fraz1#, pens_dummy#, pens1#, pens1_fa1#, pens1_fc#, xp#, y#  '20131202LC  'ex 20130609LC (ex Pens1c#, Pens1r#)
  Dim anz As TIscr2_Anz
  Dim dAnz As TIscr2_Anz
  
  With dip
    fraz1 = fraz * IIf(bTipoPens = etp.PDIR, 1, 0.5) '20120619LC
    xp = .xr + fraz1 * IIf(9 = 9, 1, 0)
    '--- bEA ---
    If iop = 2 And .gruppo0 = 5 Then '20150504LC
      bEA = 1
      anz = .s(.t - t).anz
    ElseIf iop = 2 And t1 > 0 Then '20150504LC
      bEA = 2
      'Ricostruisce l'anzianit� al 30/06 dell'anno di uscita
      te = .t - (.t - .anno1 - t1)
If .anno1 > .anno0 And te < .anno1 Then
Call MsgBox("TODO TIscr2_PensDirInd-1")
Stop
End If
      anz = .s(te - 1).anz 'ex te
      'Call TIscr2_Anz_Dh(dAnz, dip, .t, appl.CoeffVar(), 0.5)  'ex te
      Call TIscr2_Anz_Dh(dAnz, dip, te, appl.CoeffVar(), 0.5)  'ex te (penso sia stato ripristinato)  '20121226LC
      Call TIscr2_Anz_Add(anz, dAnz)
    ElseIf iop = 2 Then
Call MsgBox("TODO TIscr2_PensDirInd-1a")
Stop
    Else
      '--- 20150504LC (inizio)
      If iop = 1 And .gruppo00 = 6 Then
        bVA = 1
      ElseIf iop = 1 And t1 > 0 Then
        bVA = 2
      ElseIf iop = 1 Then
Call MsgBox("TODO TIscr2_PensDirInd-1b")
Stop
      Else
        bVA = 0
      End If
      '--- 20150504LC (fine)
      bEA = 0
      anz = .s(.t - 1).anz
      Call TIscr2_Anz_Dh(dAnz, dip, .t, appl.CoeffVar(), fraz1 * .rr_dhEff)
      Call TIscr2_Anz_Add(anz, dAnz)
    End If
    '--- 20121115LC (inizio)  'ex 20120221LC
    pens1 = TIscr2_PensMista_New(dip, wb0, wb1, appl, t, t1, .t, bVA, bEA, fraz, pens1a1, pens1a2, anz, bTipoPens, itp)   '20150504LC  '20130609LC  'ex 20121115LC
    If pens1 > AA0 Then
      pens1_fc = .Pens1C / pens1
      pens1_fa1 = 1 - dip.Pcalc / pens1 '20131202LC
If pens1_fa1 < 0 Then
Call MsgBox("TODO TIscr2_PensDirInd-4")
Stop
End If
    Else
      pens1 = 0
      pens1_fc = 0
      pens1_fa1 = 0
    End If
    '--- 20160411LC (inizio)
    If pens1 <= AA0 Then  '3181000(5,2018) 3213000(5,2015) 3231000(5,2019)
      itp = A1_RC
    Else
      '300000(5,2045) 319000(5,2042) 460000(5,2015)
    End If
    '--- 20160411LC (fine)
    If bEA = 0 Then
      If bTipoPens <> etp.PDIR Then  'Pensione indiretta, di inabilit�, o di invalidit�
        If bVA = 0 Then  '20150514LC
          .a_tpind = itp
          .a_hind = anz.h
          .a_pind = pens1
          .a_pind_fc = pens1_fc
          .a_pind_fa1 = pens1_fa1  '20131202LC
        Else  '20150504LC
          .va_tpind = itp
          .va_hind = anz.h
          .va_pind = pens1
          .va_pind_fc = pens1_fc
          .va_pind_fa1 = pens1_fa1  '20131202LC
        End If
      ElseIf bTipoPens = etp.PDIR Then 'Pensione diretta
        If bVA = 0 Then  '20150514LC
          .a_tprev = itp
          .a_hrev = anz.h
          .a_prev = pens1
          .a_prev_fc = pens1_fc
          .a_prev_fa1 = pens1_fa1  '20131202LC
        Else  '20150504LC
          .va_tprev = itp
          .va_hrev = anz.h
          .va_prev = pens1
          .va_prev_fc = pens1_fc
          .va_prev_fa1 = pens1_fa1  '20131202LC
        End If
      End If
    Else
      If bTipoPens = PIND Then 'Pensione indiretta
        If 1 = 2 Then  '20150514LC
          .va_tpind = itp
          .va_hind = anz.h
          .va_pind = pens1
          .va_pind_fc = pens1_fc
          .va_pind_fa1 = pens1_fa1  '20131202LC
        End If
        If 1 = 1 Then
          .ea_tpind = itp
          .ea_hind = anz.h
          .ea_pind = pens1
          .ea_pind_fc = pens1_fc
          .ea_pind_fa1 = pens1_fa1  '20131202LC
        End If
      ElseIf bTipoPens = PINAB Or bTipoPens = PINV Then 'Pensione indiretta
        Call MsgBox("TODO TIscr2_PensDirInd-5")
        Stop
      ElseIf bTipoPens = etp.PDIR Then 'Pensione diretta
        If 1 = 2 Then  '20150514LC
          .va_tprev = itp
          .va_hrev = anz.h
          .va_prev = pens1
          .va_prev_fc = pens1_fc
          .va_prev_fa1 = pens1_fa1  '20131202LC
        End If
        If 1 = 1 Then
          .ea_tprev = itp
          .ea_hrev = anz.h
          .ea_prev = pens1
          .ea_prev_fc = pens1_fc
          .ea_prev_fa1 = pens1_fa1  '20131202LC
        End If
      End If
    End If
    '--- 20121115LC (fine)
  End With
End Sub


Private Function TIscr2_PensMista_New(ByRef dip As TIscr2, ByRef wb0 As TWB, ByRef wb1 As TWB, ByRef appl As TAppl, _
                                      ByRef t As Long, ByRef t1 As Long, ByRef anno As Integer, ByRef bVA As Byte, _
                                      ByRef bEA As Byte, ByRef fraz As Double, ByRef pens1a1 As Double, _
                                      ByRef pens1a2 As Double, ByRef anz As TIscr2_Anz, ByRef bTipoPens As etp, _
                                      ByRef itp As Integer) As Double
'*********************************************************************************************************************
'  20130310LC  'riportare 20140111LC-26: per pensioni indirette TIscr2_Montante(...,fraz1) e non fraz, mat=27360
'  20131123LC
'  20140111LC-12
'  20150504LC  bVA
'  20150526LC  wb1
'*********************************************************************************************************************
  Dim bFig As Boolean  '20131123LC
  Dim bPensRetr As Boolean
  Dim ipmin%, te%, te0%, te1%
  Dim fo&, tp&, stp&, kpa&  '20140319LC-02
  Dim fraz1#, h0#, h0old#, h1#, h2#, hmax#, om#, omFig#, omSost#, pens1#, Pens1rr#, xp#  '20131126LC
  Dim h5 As Double  '20160309LC
  Dim PMIN_MODEL As Integer  '20140404LC
  Dim annoPens As Integer  '20150504LC
  Dim abbPens As Double  '20160308LC
  Dim omV#()  '20150512LC

  With dip
    '--- 20140404LC (inizio)
    PMIN_MODEL = OPZ_MISTO_PMIN_MODEL
    If PMIN_MODEL = 4 Then
      If .bModPmin = 0 Then
        PMIN_MODEL = 2
      Else
        PMIN_MODEL = 3
      End If
    End If
    '--- 20140404LC (fine)
    fraz1 = fraz * IIf(bTipoPens = etp.PDIR, 1, 0.5) '20120619LC
    xp = .xr + fraz1 * IIf(9 = 9, 1, 0)
    '*****************************
    'Calcolo della pensione minima
    '*****************************
    '--- Pensione minima (nAbbArt25,PminAbb)
    .PMinAbb = .PminBase
    '--- 20150322LC (inizio)
    'Nessuna pensione minima
    '--- 20150322LC (fine)
    '****************************
    'Calcolo della pensione mista
    '****************************
    '--- Tipo pensione (tp, stp)
    If bEA = 0 Then
      '--- 20150504LC (inizio)
      If bVA <> 0 Then
        tp = wb0.tipoPensVA
        annoPens = wb0.annoPensVA
      Else
        tp = .tipoPensA
        annoPens = .annoPensA
      End If
      '--- 20150504LC (inizio)
      If bTipoPens = etp.PIND Then 'Pensione indiretta
        If tp <> 0 And .t > annoPens Then
          Call MsgBox("TODO-TIscr2_PensMista_New-1b", , .mat)
          Stop
        Else
          '--- 20160309LC (inizio)
          te1 = .t - 1
          te0 = .t - 1 - .cv4.Pind2
          If te0 < .annoAss Then
            te0 = .annoAss
          End If
          h5 = .s(te1).hf - .s(te0).hf + AA0
          If anz.h >= .cv4.Pind2 And h5 >= OPZ_ART23_1 Then  '20160420LC
          '--- 20160309LC (fine)
            tp = TPENS_VA  '20131129LC  'TODOTODO
            stp = tp * 100 + 2
          Else
            Call TIscr2_MovPop_3a(dip, appl, t, .wa(t - 1), anz.h, False, tp, stp, bTipoPens, abbPens) '20160308LC
          End If
        End If
      ElseIf bTipoPens = etp.PINAB Then 'Pensione di inabilit�
        If tp <> 0 Then
          Call MsgBox("TODO-TIscr2_PensMista_New-1c", , .mat)
          Stop
        Else
          '--- 20160309LC (inizio)
          te1 = .t - 1
          te0 = .t - 1 - .cv4.Pinab2
          If te0 < .annoAss Then
            te0 = .annoAss
          End If
          h5 = .s(te1).hf - .s(te0).hf + AA0
          If anz.h >= .cv4.Pinab2 And h5 >= OPZ_ART20_1 Then
          '--- 20160309LC (fine)
            tp = TPENS_B
            stp = tp * 100 + 2
          Else
            Call TIscr2_MovPop_3a(dip, appl, t, .wa(t - 1), anz.h, False, tp, stp, bTipoPens, abbPens) '20160308LC
          End If
        End If
      ElseIf bTipoPens = etp.PINV Then 'Pensione di invalidit�
        If tp <> 0 Then
          Call MsgBox("TODO-TIscr2_PensMista_New-1d", , .mat)
          Stop
        Else
          '--- 20160309LC (inizio)
          te1 = .t - 1
          te0 = .t - 1 - .cv4.Pinv5
          If te0 < .annoAss Then
            te0 = .annoAss
          End If
          h5 = .s(te1).hf - .s(te0).hf + AA0
          If anz.h >= .cv4.Pinv5 And h5 >= OPZ_ART19_3 Then
          '--- 20160309LC (fine)
            tp = TPENS_I
            stp = tp * 100 + 2
          Else
            Call TIscr2_MovPop_3a(dip, appl, t, .wa(t - 1), anz.h, False, tp, stp, bTipoPens, abbPens) '20160308LC
          End If
        End If
      End If
    Else
      tp = wb0.tipoPensEA
      annoPens = wb0.annoPensEA  '20150504LC
      If bTipoPens = etp.PIND Then 'Pensione indiretta
        If tp <> 0 And .t > annoPens Then
          Call MsgBox("TODO-TIscr2_PensMista_New-2a", , .mat)
          Stop
        '--- 20140111LC-14 (inizio) (mat=760)
        Else  '20160309LC
          'Gi� calcolato
        End If
      ElseIf bTipoPens = etp.PINAB Then 'Pensione di inabilit�
        Call MsgBox("TODO-TIscr2_PensMista_New-2b", , .mat)
        Stop
      ElseIf bTipoPens = etp.PINV Then 'Pensione di invalidit�
        Call MsgBox("TODO-TIscr2_PensMista_New-2c", , .mat)
        Stop
      End If
    End If
    '--- bPensRetr, itp  ->  bPmin, PminCalc
    bPensRetr = False
    If .TipoElab <> 1 Then
      Select Case tp
      '--- 20131129LC (inizio)
      Case TPENS_VO
        itp = A1_PV1
        bPensRetr = True
      Case TPENS_VA
        itp = A1_PV1
        bPensRetr = True
        If bTipoPens = etp.PDIR And OPZ_INT_PMIN_PANZ <> 1 Then  '20131129LC  'mat 26220,27530,69040
          .bPmin = 0
          .PMinCalc = 0#
        End If
      Case TPENS_VP
        itp = A1_PV1
        bPensRetr = True
        If bTipoPens = etp.PDIR And OPZ_INT_PMIN_PANZ = 0 Then '20131129LC  'mat 105390,105500,106870
          'Gi� testato anz.h<hmax, in tal caso .PminCalc=0
          If .PMinCalc <> 0# Then
            Call MsgBox("TODO-TIscr2_PensMista_New-3a", , .mat)
            Stop
          End If
          .bPmin = 0
          .PMinCalc = 0#
        End If
      Case TPENS_AO
        itp = A1_PA1
        bPensRetr = True
        If bTipoPens = etp.PDIR And OPZ_INT_PMIN_PANZ <> 1 Then  'mat = 28106, 28653, 28679
          .bPmin = 0
          .PMinCalc = 0#  'mat = 28106, 28653, 28679
        End If
      '--- 20131129LC (fine)
      Case TPENS_S
        itp = A1_PS1
      Case TPENS_T
        itp = A1_PT1
      Case TPENS_B
        itp = A1_PB1
        bPensRetr = True
      Case TPENS_I
        itp = A1_PI1
        bPensRetr = True
      Case Else
        itp = A1_RC
      End Select
    Else  'If .TipoElab = 1 Then
      .bPmin = 0
      .PMinCalc = 0#
      Select Case tp
      Case TPENS_VO, TPENS_VA, TPENS_VP  '20131129LC
        itp = A1_PV1
      Case TPENS_AO  '20131129LC
        itp = A1_PA1
      Case TPENS_S
        itp = A1_PS1
      Case TPENS_T
        itp = A1_PT1
      Case TPENS_B
        itp = A1_PB1
      Case TPENS_I
        itp = A1_PI1
      Case Else
        itp = A1_RC
      End Select
    End If
    '--- Pensione mista - parte contributiva ---
    '--- 20140404LC (inizio)  '20140319LC-02
    If Math.Abs(OPZ_MISTO_CCR_MODEL) >= 2 Then
      kpa = 1 + lmin(t, lmax(Year(dip.DNasc) - 1948, 0))
    ElseIf Math.Abs(OPZ_MISTO_CCR_MODEL) >= 1 Then
      kpa = 1 + lmax(Year(dip.DNasc) - 1948, 0)
    Else
      kpa = t
    End If
    .ax = AX1(appl.CoeffPA(), .sesso, xp, kpa)
    If OPZ_PENS_ANZ_MODEL = 2 Then
      .ax0 = AX1(appl.CoeffPA(), .sesso, .cv4.Pveo65 + 0, kpa)
    End If
'--- 20150421LC (inizio) TEMPTEMP
'If Math.Abs(OPZ_INT_COMP) = COMP_ENAS Then
'  If .ax <> modEneaSwa.CCR(dip.t, xp, 1) / 100# Then
'    call MsgBox("TODO")
'    Stop
'  ElseIf .ax0 <> modEneaSwa.CCR(dip.t, .cv4.Pveo65 + 0, 1) / 100# Then
'    call MsgBox("TODO")
'    Stop
'  End If
'End If
'--- 20150421LC (fine)
    '--- 20140404LC (fine)
    omFig = 0 '20131126LC
    If bEA = 0 Then
      '20150506LC (inizio)
      If bVA <> 0 Then   '20150518LC-2 (ripristinata)
        '20150526LC (inizio)
        If dip.gruppo00 = 6 Then
          om = TIscr2_Montante_AV(dip, appl.CoeffVar(), anno, -1, bPensRetr, wb1, omV())
          omSost = TIscr2_Montante_AV(dip, appl.CoeffVar(), anno, -1, False, wb1, omV())
        Else
          om = TIscr2_Montante_AV(dip, appl.CoeffVar(), anno, -1, bPensRetr, wb0, omV())
          omSost = TIscr2_Montante_AV(dip, appl.CoeffVar(), anno, -1, False, wb0, omV())
        End If
'omSost = omSost
      Else
      '20150506LC (fine)
        'h0 = .s(.t).anz.h + fraz1  '20131123LC
        h0 = .s(.t).anz.h + fraz1 + AA0  '20140310LC  'riportare 20140111LC-27 (non dava problemi, solo per coerenza)
        bFig = TIscr2_bFig(dip, h0)  '20131123LC
'If .t >= 2035 Then  '2035
'  .t = .t
'End If
        '20160124LC  'ex 20150512LC  'ex 20131123LC  'ex 20121127LC
        om = TIscr2_Montante(dip, appl, anno, -1, 3, bPensRetr, bFig, fraz1, omV())
        '--- 20131126LC (inizio)
        'If OPZ_INT_ASSIST_MODEL >= 1 Then
          If bFig = True Then
            '20160124LC  'ex 20150512LC  'ex 20131123LC  'ex 20121127LC
            omFig = om - TIscr2_Montante(dip, appl, anno, -1, 3, bPensRetr, False, fraz1, omV())
          End If
        'End If
        '--- 20131126LC (fine)
        If bPensRetr = True Then
          'bFig = TIscr2_bFig(dip, .s(.t).anz.h + fraz1) '20131123LC
          bFig = TIscr2_bFig(dip, h0)  '20140310LC  'riportare 20140111LC-27 (non dava problemi, solo per coerenza)
          '20160124LC  'ex 20150512LC  'ex 20131123LC  'ex 20121127LC
          omSost = TIscr2_Montante(dip, appl, anno, -1, 3, False, bFig, fraz1, omV())
        Else
          omSost = om
        End If
      End If
    Else
      '--- 20140310LC (inizio)  'riportare 20140111LC-27
      te = .t - (.t - .anno1 - t1)
      If bEA = 1 Then
If t1 <> 0 Then
Call MsgBox("TODO")
Stop
End If
        h0 = .s(te).anz.h + 1E-30  '20131123LC
      Else
        h0 = .s(te - 1).anz.h + AA5
      End If
      '--- 20140310LC (fine)  'riportare 20140111LC-27
      bFig = TIscr2_bFig(dip, h0)  '20131123LC
      '20160124LC  'ex 20150512LC  'ex 20131123LC  'ex 20121127LC
      om = TIscr2_Montante(dip, appl, anno, te, 3, bPensRetr, bFig, fraz1, omV())
      '--- 20131126LC (inizio)
      'If OPZ_INT_ASSIST_MODEL >= 1 Then
        If bFig = True Then
          '20160124LC  'ex 20150512LC  'ex 20131123LC  'ex 20121127LC
          omFig = om - TIscr2_Montante(dip, appl, anno, te, 3, bPensRetr, bFig, fraz1, omV())
        End If
      'End If
      '--- 20131126LC (fine)
      If bPensRetr = True Then
        bFig = TIscr2_bFig(dip, h0)  '20131123LC
        '20160124LC  'ex 20150512LC  'ex 20131123LC  'ex 20121127LC
        omSost = TIscr2_Montante(dip, appl, anno, te, 3, False, bFig, fraz1, omV())
      Else
        omSost = om
      End If
    End If
    '---
    If tp = -TPENS_S Then
      .Pens1C = 0#
    ElseIf itp = A1_RC Then
      'TODO3: .Pens1c = om
      .Pens1C = 0# 'om  '20120515LC
    ElseIf OPZ_MISTO_RETRIB_MIN = 1 And .s(.t).bIsMisto = True Then  '20131123LC (ex OPZ_MISTO_MODEL=3)  'ex 20120515LC
      'TODO3: .Pens1r e no .Pens1c
      .Pens1C = .PMinAbb * dmax(0, dmin(anz.h_contr_ante + anz.h_contr_post, .cv4.Pind30 + 0)) / .cv4.Pind30
    Else
      .Pens1C = ARRV(om * .ax)
    End If
    '--- 20160308LC (inizio)
    If bEA = 2 Then
      abbPens = wb0.abbPensEA
    ElseIf bEA = 1 Then
      abbPens = wb0.abbPensVA
    Else
      abbPens = dip.abbPensA
    End If
    If abbPens > 0 And abbPens < 1 Then
      omSost = omSost * abbPens
    ElseIf abbPens = 1 Then
    Else
    End If
    .PSost = ARRV(omSost * .ax)
    '--- 20160308LC (fine)
    .Pfig = ARRV(omFig * .ax)
    '--- Pensione mista - parte retributiva ---
    If bPensRetr = False Then
      .Pens1C = .PSost
      .Pens1r = 0#
    Else
      If bTipoPens = etp.PIND Then  'Pensione indiretta
        .Pens1r = pens1a1 * anz.h_retr_ante + pens1a2 * anz.h_retr_post
      ElseIf bTipoPens = etp.PINAB Or bTipoPens = etp.PINV Then  'Pensione di inabilit� o invalidit�
        h0old = anz.h_retr_ante + anz.h_retr_post
        If h0old >= AA0 Then
          h0 = dmin(h0old + .cv4.Pinab10, .cv4.Pinab35 + 0)
          h1 = anz.h_retr_ante * h0 / h0old
          h2 = anz.h_retr_post * h0 / h0old
          .Pens1r = pens1a1 * h1 + pens1a2 * h2
          If bTipoPens = etp.PINV Then
            .Pens1r = .Pens1r * .cv4.Pinv70
          End If
        Else  '20131123LC  Mat=101201
'call MsgBox("TODO-TIscr2_PensMista_New-3b", , .mat)
'Stop
          .Pens1C = .PSost
          .Pens1r = 0#
        End If
      ElseIf bTipoPens = etp.PDIR Then  'Pensione diretta
        '--- 20160308LC (inizio)
        If bEA = 2 Then
          abbPens = wb0.abbPensEA
        ElseIf bEA = 1 Then
          abbPens = wb0.abbPensVA
        Else
          abbPens = dip.abbPensA
        End If
        If abbPens > 0 And abbPens < 1 Then
          omSost = omSost * abbPens
        ElseIf abbPens = 1 Then
        Else
        End If
        .Pens1r = PensNew(.mat, pens1a1, pens1a2, anz, anz.h_retr_ante, anz.h_retr_post, annoPens = .t, _
                          xp, .ax0, .ax, abbPens) '02160308LC  'ex 20150504LC  'ex 20120302LC
        '--- 20160308LC (fine)
      End If
      .Pens1r = ARRV(.Pens1r)
    End If
    '--- 20131202LC (inizio)
    .Pcalc = .Pens1r + .Pens1C
    If ipmin = 3 And bPensRetr = True Then  'Perch� bPensRetr=T ??
      If (bTipoPens <> etp.PDIR) Then
        If (.Pcalc < .PMinCalc) And ((.PSost <= .PMinCalc) Or OPZ_MISTO_PMIN_RESTMC = 0) Then
          .bPmin = .bPmin + 30
          .Pens1C = ARRV(.PMinCalc - .Pens1r)  'mat = 27495, 27791, 28106
        ElseIf (.Pcalc < .PMinAbb) And (.PMinAbb <= .PSost) And (OPZ_MISTO_PMIN_RESTMC = 1) Then
          .bPmin = .bPmin + 40
          .Pens1C = ARRV(.PMinAbb - .Pens1r)  'mat = 27791, 183049, 183997
        ElseIf (.Pcalc < .PSost) And (.PSost < .PMinAbb) And (OPZ_MISTO_PMIN_RESTMC = 1) Then
          .bPmin = .bPmin + 50
          .Pens1C = ARRV(.PSost - .Pens1r)  'mat = 27791, 311909, 316693
        ElseIf (.Pcalc < .PSost) And (OPZ_MISTO_PMIN_RESTMC >= 2) Then
          .bPmin = .bPmin + 60
          .Pens1C = .PSost  'mat = 27791, 183049, 183997
          .Pens1r = 0#
        End If
      Else
        If (.Pcalc < .PMinCalc) And ((.PSost <= .PMinCalc) Or OPZ_MISTO_PMIN_RESTMC = 0) Then
          .bPmin = .bPmin + 70
          .Pens1C = ARRV(.PMinCalc - .Pens1r)  'mat = 27495, 29229, 29558
        ElseIf (.Pcalc < .PMinAbb) And (.PMinAbb <= .PSost) And (OPZ_MISTO_PMIN_RESTMC = 1) Then
          .bPmin = .bPmin + 80
          .Pens1C = ARRV(.PMinAbb - .Pens1r)  'mat = 183049, 183997, 186635
        ElseIf (.Pcalc < .PSost) And (.PSost < .PMinAbb) And (OPZ_MISTO_PMIN_RESTMC = 1) Then
          .bPmin = .bPmin + 90
          .Pens1C = ARRV(.PSost - .Pens1r)  'mat = 101533, 101900, 104351
        ElseIf (.Pcalc < .PSost) And (OPZ_MISTO_PMIN_RESTMC >= 2) Then
          .bPmin = .bPmin + 100
          .Pens1C = .PSost  'mat = 27791, 101533, 101900
          .Pens1r = 0#
        End If
      End If
    End If
    '--- 20131202LC (fine)
    TIscr2_PensMista_New = .Pens1C + .Pens1r
  End With
End Function


Private Sub TIscr2_PensioniUnitarie1(ByRef dip As TIscr2, s1 As TIscr2_Stato, ByRef appl As TAppl, CoeffRiv#(), CoeffVar#(), anno%, annoRid%, nMigliori0%, n%, p1#, p2#)
'****************************************************************************************************
'SCOPO
'  Calcola la pensione per 1 anno di anzianit�
'VERSIONI
'  20111213LC
'  20140706LC
'  20150703LC  ex TIscr2_PensioniUnitarie
'****************************************************************************************************
  Dim nMigliori%
  Dim RiscReddMin#, y# '20140707LC
  Dim temp#()
  
  With dip
'20130610LC
'If anno = 2038 Then
'anno = anno
'End If
    nMigliori = TIscr2_LexPensRivaluta(.s(), CoeffRiv(), CoeffVar(), anno, annoRid, temp(), n)
    '20121009LC (inizio)
    If OPZ_MOD_ART_17_4 <> 0 And s1.bIsMisto = True And nMigliori0 = 22 And n = 27 And nMigliori < n Then
'La media reddituale anteriore alla maturazione del diritto a pensione � determinata esclusivamente sui redditi anteriori al 2013.
'Se il numero dei redditi professionali anteriori al 2013 � inferiore a 27 (v. comma 3), ai fini del calcolo della media � escluso un reddito
'(meno favorevole) per ogni cinque anni di anzianit� contributiva maturata anteriormente al 01/01/2013, fino ad un massimo di quattro redditi.
       '20130610LC
      '17.4 - Ai fini del computo della media reddituale utile per il calcolo della quota retributiva sono presi a riferimento
      'esclusivamente i redditi fino all�anno 2012, anche se il numero � inferiore a quello indicato nel comma precedente.
      'Nel caso in cui il numero di redditi professionali dichiarati sia inferiore a quello indicato nel comma precedente, la
      'media reddituale � computata escludendo un reddito ogni cinque anni di anzianit� maturata fino ad un massimo di quattro.
      If Int(s1.AnzTot) >= 20 Then
'If anno = 2013 Then
'.mat = .mat '8881 (p1:31.0552->32.534), 16477, 18650, 19583, 20162  325234
'End If
        nMigliori = nMigliori - 4
      ElseIf Int(s1.AnzTot) >= 15 Then
'If anno = 2013 Then
'.mat = .mat '16717 (p1:166.1562->197.3106), 17678, 20077, 24754, 24793
'End If
        nMigliori = nMigliori - 3
      ElseIf Int(s1.AnzTot) >= 10 Then
'If anno = 2013 Then
'.mat = .mat '16470 (p1:24.7654->30.2688), 20151, 20338, 20787, 23001
'End If
        nMigliori = nMigliori - 2
      ElseIf Int(s1.AnzTot) >= 5 Then
'If anno = 2013 Then
'.mat = .mat '20712 (p1:725.2486->761.511), 22084, 22110, 23642, 23684
'End If
        nMigliori = nMigliori - 1
      Else
'If anno = 2013 Then
'.mat = .mat '22052 (p1:84.079<->84.079), 22814, 26448, 26526, 26707'
'End If
        nMigliori = nMigliori
      End If
    Else
      If nMigliori > nMigliori0 Then
        nMigliori = nMigliori0
      End If
    End If
    '20121009LC (fine)
    Call VecSort(temp(), n)
    .qaS = ARRV(VecMedia(temp, nMigliori))
    '--- 20140707LC (inizio)
    p2 = LexPensAbbattimento(CoeffVar, .qaS, anno, anno)
'If bRiscReddMin > 0 Then
'  Open "c:\aaa.txt" For Append As #1
'  Print #1, .mat;
'  Print #1, ";"; .t;
'  Print #1, ";"; .qaS;
'  Print #1, ";"; p2;
'  Print #1, ";"; y;
'  Print #1, ";"; LexPensAbbattimento(CoeffVar, y, anno, anno);
'  Print #1, ";"; RiscReddMin;
'  Print #1, ";"; .s(2012).AnzTot;
'  Print #1, ";"; .s(2012).AnzRis
'  Close #1
'End If
    p1 = p2
    If s1.bIsProRata = True Then '20111213LC
      'call MsgBox("TODO-9a", , .mat)
      'Stop
      p1 = LexPensAbbattimento(CoeffVar, .qaS, OPZ_PRORATA_ANNO - 1, anno)
    End If
    '--- 20140707LC (fine)
  End With
End Sub


Private Sub TIscr2_PensioniUnitarie2(ByRef dip As TIscr2, s1 As TIscr2_Stato, ByRef appl As TAppl, CoeffRiv#(), CoeffVar#(), anno%, annoRid%, p1#, p2#)
'****************************************************************************************************
'SCOPO
'  Calcola la pensione ENAS per 1 anno di anzianit�
'VERSIONI
'  20130326LC  nuova in prototipo offerta ENAS
'  20150307LC  riportata da prototipo offerta ENAS
'  20150322LC  aggiornata
'  20150413LC  aggiornata
'  20150506LC  aggiornata per volontari
'****************************************************************************************************
  On Error GoTo GestioneErrori
  Dim ir&, nr&, nk&
  Dim sm#
  Dim y0#, y1#
  Dim yy#()
  Dim yk#()
  
  Dim annoBack As Integer
  Dim idx As Integer
  Dim idxMed As Integer
  Dim sumMed As Long
  Dim sumN As Integer
  Dim vecMedie() As valTriennio
  ReDim vecMedie(7)
  
  
  With dip

    If (s1.anz.h_retr_ante > AA0) Or (s1.bIsProRata = True And s1.anz.h_retr_post > AA0) Then
        
        If .t = 2015 Then
            Debug.Print "2021"
        End If
    
      '--- Recupera gli ultimi 15 redditi - Per Quota B
      ReDim yy(14)
      nr = 0
      For ir = .t - 0 To .annoAss Step -1
        If anno = annoRid Or ir <= annoRid Then
          sm = .s(ir).sm2s
        Else
          sm = .s(annoRid).sm3s
        End If
        If sm > AA0 Then
          '--- 20130612LC (inizio)
          yy(nr) = sm
          If .gruppo0 > 2 Then
          ElseIf ir = .t - 0 And anno = annoRid Then
            yy(nr) = ARRV(sm * OPZ_FRAZ_COMP)  '20160222LC  'ex 0.75
          End If
          '--- 20130612LC (fine)
          nr = nr + 1
          If nr >= 15 Then
            Exit For
          End If
        End If
      Next ir
      
      'Condizione per decidere se eseguire il codice di Lodo oppure quello modificato da Marco
      If OPZ_QUOTA_A = 1 Then
        '--- Recupera gli ultimi 10 redditi - Per Quota A Mod MDG 20.09.2018
        nk = 0
        annoBack = .t - Year(.dIscrFondo)
'        If annoBack < 1997 Then
'          annoBack = 1997
'        End If
        ReDim yk(annoBack)
              
        For ir = .t - 0 To Year(.dIscrFondo) Step -1
          If anno = annoRid Or ir <= annoRid Then
            sm = .s(ir).sm2s
          Else
            sm = .s(annoRid).sm3s
          End If
            yk(nk) = sm
            If .gruppo0 > 2 Then
            ElseIf ir = .t - 0 And anno = annoRid Then
              yk(nk) = ARRV(sm * OPZ_FRAZ_COMP)
            End If
            nk = nk + 1
            If nk >= (anno - .annoAss + 1) Then
              Exit For
            End If
        Next ir
        
        '--- Quota A
        p1 = 0#
        If s1.anz.h_retr_ante > AA0 Then
          y0 = 0
          
          '--- Salvo i trienni consecutivi disponibili
          idxMed = 0
          For idx = 0 To UBound(yk) Step 1
            If yk(idx) > AA0 Then
              If idx >= 10 Then
                Exit For
              End If
              If vecMedie(idxMed).anno1 = 0 Then
                vecMedie(idxMed).anno1 = yk(idx)
              ElseIf vecMedie(idxMed).anno2 = 0 Then
                vecMedie(idxMed).anno2 = yk(idx)
              ElseIf vecMedie(idxMed).anno3 = 0 Then
                vecMedie(idxMed).anno3 = yk(idx)
                If idxMed = 7 Then
                  Exit For
                End If
                idxMed = idxMed + 1
                idx = idx - 2
              End If
            Else
              vecMedie(idxMed).anno1 = 0
              vecMedie(idxMed).anno2 = 0
              vecMedie(idxMed).anno3 = 0
            End If
          Next idx
          
          'Controllo che l'ultima terzina sia completa, altrimenti la elimino
          If vecMedie(idxMed).anno1 = 0 Or vecMedie(idxMed).anno2 = 0 Or vecMedie(idxMed).anno3 = 0 Then
            vecMedie(idxMed).anno1 = 0
            vecMedie(idxMed).anno2 = 0
            vecMedie(idxMed).anno3 = 0
          End If
          
          '---- Calcolo le medie
          sumMed = 0
          For idx = 0 To UBound(vecMedie) - 1
          
            sumMed = vecMedie(idx).anno1 '+ vecMedie(idx).anno2 + vecMedie(idx).anno3
            sumMed = sumMed + vecMedie(idx).anno2
            sumMed = sumMed + vecMedie(idx).anno3
            
            If y0 < sumMed Then
              y0 = sumMed
            End If
          
          Next idx
          
          '--- In questo caso nel decennio non ho un triennio consecutivo.
          '--- Parto dall'ultimo anno e uso le 2 provvigioni relative gli anni immediatamente precedenti
          sumN = 0
          If yk(1) = 0 Or yk(2) = 0 And idxMed < 1 Then
            
            For idx = 0 To UBound(yk)
              If yk(idx) > AA0 Then
                y0 = y0 + yk(idx)
                sumN = sumN + 1
              End If
              
              If sumN = 3 Then
                Exit For
              End If
            Next idx
          End If
          
          y1 = ARRV(modEneaSwa.PensA(y0# / 3#, s1.anz.h_retr_ante))
          p1 = ARRV(y1 - modEneaSwa.PensA_Detr(y1))
          p1 = p1 / s1.anz.h_retr_ante
        End If
      Else
        '--- Quota A
        p1 = 0#
        If s1.anz.h_retr_ante > AA0 Then
          y0 = 0
          For ir = 0 To 7
            y1 = yy(ir) + yy(ir + 1) + yy(ir + 2)
            If y0 < y1 Then
              y0 = y1
            End If
            If yy(ir + 2) <= AA0 Then
              Exit For
            End If
          Next ir
          y1 = ARRV(modEneaSwa.PensA(y0# / 3#, s1.anz.h_retr_ante))
          p1 = ARRV(y1 - modEneaSwa.PensA_Detr(y1))
          p1 = p1 / s1.anz.h_retr_ante
        End If
      End If
      
      '--- Quota B
      p2 = p1
      If s1.bIsProRata = True And s1.anz.h_retr_post > AA0 Then
        y0 = 0
        nr = 0
        For ir = 0 To 14
          If yy(ir) <= AA0 Then
            Exit For
          End If
          y0 = y0 + yy(ir)
          nr = nr + 1
        Next ir
        '--- 20150413LC-2 (inizio)
        If nr > 0 Then
          p2 = ARRV(modEneaSwa.PensB(y0 / nr, s1.anz.h_retr_post))
          p2 = p2 / s1.anz.h_retr_post
        Else
          p2 = 0#
        End If
        '--- 20150413LC-2 (fine)
      End If
    End If
  End With
  
  Exit Sub
  
GestioneErrori:
  MsgBox "Errore in PensioniUnitarie2: " & vbCrLf & _
        " Error Source: " & Err.Source & " " & vbCrLf & _
        " Error Description: " & Err.Description & vbCrLf & _
        " Error Line: " & Erl
End Sub


'20140603LC  'ex 20120331LC
Private Function TIscr2_TS_set(ByVal iTS As Byte, iGruppo As Byte, iSex&, iTit&, iTC&, DNasc As Date, ih%, t0%, Irpef#, ReddMin#, REDDMAX#) As Byte
  Dim x%
  If ON_ERR_EH = True Then On Error GoTo ErrorHandler
  
  If OPZ_SUDDIVISIONI_MODEL = 0 Then
    iTS = 0
  ElseIf OPZ_SUDDIVISIONI_MODEL >= 0 Then
    iTS = iTS
  ElseIf OPZ_SUDDIVISIONI_MODEL = -1 Then 'gruppo
    iTS = iGruppo - 1
  ElseIf OPZ_SUDDIVISIONI_MODEL = -2 Then 'gruppo e sesso
    iTS = iGruppo + 5 * (iSex - 1) - 1
  ElseIf OPZ_SUDDIVISIONI_MODEL = -3 Then 'gruppo e titolo
    iTS = iGruppo + 5 * (iTit - 1) - 1
  ElseIf OPZ_SUDDIVISIONI_MODEL = -4 Then 'sesso
    iTS = iSex - 1
  ElseIf OPZ_SUDDIVISIONI_MODEL = -5 Then 'sesso e titolo
    iTS = iSex + 2 * (iTit - 1) - 1
  ElseIf OPZ_SUDDIVISIONI_MODEL = -6 Then 'titolo
    iTS = iTit - 1
  ElseIf OPZ_SUDDIVISIONI_MODEL = -7 Then 'classi di et� quinquennali
    x = t0 - Year(DNasc) + OPZ_SUDDIVISIONI_DT  '20140603LC
    iTS = Int((x - 30) / 5) + 1
    If iTS < 0 Then
      iTS = 0
    ElseIf iTS > 9 Then
      iTS = 9
    End If
  ElseIf OPZ_SUDDIVISIONI_MODEL >= -10 And OPZ_SUDDIVISIONI_MODEL <= -8 Then 'classi di et� decennali
    x = t0 - Year(DNasc) + OPZ_SUDDIVISIONI_DT  '20140603LC
    iTS = Int((x - 35) / 10) + 1
    If iTS < 0 Then
      iTS = 0
    ElseIf iTS > 4 Then
      iTS = 4
    End If
    If OPZ_SUDDIVISIONI_MODEL = -9 Then 'classi di et� decennali e sesso
      iTS = iTS + 5 * (iSex - 1)
    ElseIf OPZ_SUDDIVISIONI_MODEL = -10 Then 'classi di et� decennali e titolo
      iTS = iTS + 5 * (iTit - 1)
    End If
  ElseIf OPZ_SUDDIVISIONI_MODEL >= -13 And OPZ_SUDDIVISIONI_MODEL <= -11 Then 'classi di reddito
    Select Case Irpef
    Case Is < ReddMin
      iTS = 0
    Case Is < ReddMin + (REDDMAX - ReddMin) / 3#
      iTS = 1
    Case Is < ReddMin + (REDDMAX - ReddMin) * 2# / 3#
      iTS = 2
    Case Is < REDDMAX
      iTS = 3
    Case Else
      iTS = 4
    End Select
    If OPZ_SUDDIVISIONI_MODEL = -12 Then 'classi di reddito e sesso
      iTS = iTS + 5 * (iSex - 1)
    ElseIf OPZ_SUDDIVISIONI_MODEL = -13 Then 'classi di reddito e titolo
      iTS = iTS + 5 * (iTit - 1)
    End If
  '20120331LC (inizio)
  ElseIf OPZ_SUDDIVISIONI_MODEL = -14 Then 'classi di et� annuali 25-65
    x = (t0 - Year(DNasc)) - 25 + OPZ_SUDDIVISIONI_DT  '20140603LC
    If x < 0 Then
      x = 0 'ubound(OPZ_INT_SUDDIVISIONI_nomi)
    ElseIf x >= OPZ_INT_SUDDIVISIONI_N Then
      x = OPZ_INT_SUDDIVISIONI_N - 1
    End If
    iTS = x
  ElseIf OPZ_SUDDIVISIONI_MODEL = -15 Then 'classi di anzianit� annuali
    x = ih
    If x < 0 Then
      x = 0
    ElseIf x >= OPZ_INT_SUDDIVISIONI_N Then
      x = OPZ_INT_SUDDIVISIONI_N - 1
    End If
    iTS = x
  '20120331LC (fine)
  Else
    iTS = 0
  End If
  If iTS < 0 Or iTS > OPZ_INT_SUDDIVISIONI_N Then
    iTS = 0
  End If
  
ExitPoint:
  On Error GoTo 0
  TIscr2_TS_set = iTS
  Exit Function
  
ErrorHandler:
  iTS = 1
  GoTo ExitPoint
End Function


Public Function TIscr2_xa#(ByRef dip As TIscr2)
  TIscr2_xa = dip.xan '+ dip.c1
End Function


Private Function TIscr2_ReadMdb(ByRef dip As TIscr2, ByRef dia As TIscr2Aux, ByRef appl As TAppl) As String
'20150413LC  bRiatt
'20150418LC  bRiatt (eliminato)
'20150525LC  aggiunto dia, tolto ixdb&(), gr1Mat, iRec&
  Dim bTipo As Byte, ss As Byte
  Dim anno%, ih% '20120331LC
  Dim napa%  '20150410LC
  Dim i&, ia&, j&, j0&, j1&, k&, k0&, mi&, t1&  '20150525LC
  Dim ijk As Long  '20160611LC
  Dim hh&  '20140706LC
  Dim y#, y1#, y2#, ys#, z#
  Dim dInizCR As Date  '20170309LC
  Dim SQL$, szErr$, szDElab$
  Dim s$  '20140317LC
  Dim supTemp As TIscrSup2
  Dim dip0 As TIscr2  '20150525LC
  If ON_ERR_EH = True Then On Error GoTo ErrorHandler
On Error GoTo 0

  szErr = ""
  dip = dip0  '20150525LC
  With dip
    '--- 20121114LC (inizio)
    ReDim .wa((1 + P_DUECENTO) - 1)
    If OPZ_INT_NO_AE_AB_AI = 0 Then '20121113LC
      ReDim .wb((1 + P_DUECENTO) * (1 + P_DUECENTO) - 1)
    End If
    ReDim .wc((1 + P_DUECENTO) * (1 + P_DUECENTO) * TWC_SIZE - 1)  '20121204LC
    .annoIniz = appl.t0
    .dElab = "31/12/" & appl.t0
    szDElab = Format(.dElab, "yyyymm")
    .anno0 = appl.parm(P_btAnno)
    '--- 20121114LC (fine)
    '--- iErr As Long    'iErr
    '--- mat As Long     'IdDip
    .mat = dia.mat
    '--- gr As Byte      'IdAz
    .gruppo0 = dia.gr
    .gruppo00 = .gruppo0
    If (.gruppo0 = 2) And (OPZ_INT_NO_PENSATT = 1) Then
      .gruppo0 = 3
    End If
    If .gruppo0 = 0 Then
      szErr = "Record senza gruppo"
      GoTo ExitPoint
    End If
    '--- 20150612LC (inizio)
    .PensAnno = dia.PensAnno
    If .PensAnno = 0 Then
      '--- 20150612LC (inizio)
      .PensAnno = appl.t0 + 1
      '--- 20150612LC (fine)
    End If
    '--- 20150612LC (fine)
    '--- 20150527LC (inizio)
    If .gruppo0 = 3 Or .gruppo0 = 4 Then
      .PensAUS = 0
    Else
      .PensAUS = dia.PensAUS
      If (.gruppo0 = 2) And (.PensAUS = 0) Then  '20150906LC
        .PensAUS = .PensAnno  '20150612LC
      End If
    End If
    '--- 20150527LC (fine)
    '--- sex As Byte     'Sesso
    .sesso = dia.sex
    If .sesso < 1 Or .sesso > 2 Then
      .sesso = 1
    End If
    '--- tit As Byte     'Categ
    .Qualifica = dia.tit
    'If .gruppo00 = 6 And 1 = 2 Then '20150612LC
    If .gruppo00 = 6 Then  '20160411LC
      .Qualifica = 1
    ElseIf .Qualifica < 1 Or .Qualifica > 2 Then
      .Qualifica = 1  '20160411LC (ex 2)
    End If
    '---
    .iQ = (.Qualifica - 1) * 2 + .sesso - 1 '20080617LC
    .iSQ = appl.nSize2 * (.iTS + 1) + appl.nSize3 * (.iQ + 1)  '20121028LC
    '--- DNasc As Date   'DNasc
    .DNasc = dia.DNasc
    '--- DIscr As Date   'DIscrFondo
    .dIscrFondo = dia.DIscr
      .dAssunz = .dIscrFondo
      .annoAss = Year(.dAssunz)
      If .annoAss < 1961 Then .annoAss = 1961
    .anno1 = .annoAss
    If .anno1 < .anno0 Then .anno1 = .anno0
    .bNuovoEntrato = .anno1 > .anno0 '20121105LC
    '--- dCess As Date   'DCessaz
    '--- h As Double     'hmf
    ih = Int(dia.h + AA0) '20140703LC
    '--- ha As Double    'hmfa
    '--- hb As Double    'hmfb
    '--- c1 As Byte      'c1
    .Carriera = dia.c1
    .iTC = .Carriera '20080305LC
    '--- c2 As Byte      'c2
    If appl.CoeffVar(appl.t0, ecv.cv_Sogg1Al) > AA0 Then
      .iTS = TIscr2_TS_set(dia.c2, .gruppo0, .sesso, .Qualifica, .Carriera, .DNasc, ih, appl.t0, dia.sm(0), appl.CoeffVar(appl.t0, ecv.cv_Sogg0Min) / appl.CoeffVar(appl.t0, ecv.cv_Sogg1Al), appl.CoeffVar(appl.t0, ecv.cv_Sogg2Max)) 'anTS,IrpefT0
    Else
      .iTS = TIscr2_TS_set(dia.c2, .gruppo0, .sesso, .Qualifica, .Carriera, .DNasc, ih, appl.t0, dia.sm(0), appl.CoeffVar(appl.t0, ecv.cv_Sogg0Min) / 10000000000#, appl.CoeffVar(appl.t0, ecv.cv_Sogg2Max)) 'anTS,IrpefT0
    End If
    '--- n As Double     'n
    .n = dia.n
    .nRes = .n
    '--- 20150413LC (inizio)
    If .n <= 0 Then
      .n = 0  '20150525LC  'ex 1
      .nRes = .n
    End If
    '--- nMan As Double  'Mandati
    If .Qualifica <= 0 Then
      .nMandati1 = 0
    ElseIf .Qualifica = 1 Then
      .nMandati1 = 1
    Else
      .nMandati1 = dia.nMan
      If .nMandati1 < 2 And 1 = 2 Then '20150612LC
        .nMandati1 = 2
      End If
    End If
    '--- smNI As Double  'Mandati
    '--- sm() As Double
    '--- om() As Double
    '--- cmNI As Double  'Mandati
    .cmNI = dia.cmNI
    '--- cm() As Double
    '--- ivaNI As Double  'Mandati
    '--- iva() As Double
    '--- inteNI As Double  'Mandati
    '--- inte() As Double
    '--- PensTipo As Byte
    .catPens = dia.PensTipo
    '--- PensAnno  As Integer
    .PensAnno = dia.PensAnno  '20150612LC
    '--- PensAUS As Integer
    '--- Pensione As Double
    '--- nSup  As Byte
    '--- sup() As TIscrSupAux
    '***********************
    'Lettura dati anagrafici
    '***********************
    '--- 20150527LC (inizio)
    .irpefNI = dia.smNI
    .ivaNI = dia.smNI
    '--- 20150527LC (inizio)
    '20111226LC (fine)
    .DCan = dia.dCess
      '--- 20140111LC-18 (inizio) (mat=25000)
      'If .dCan = 0 Then
      If .DCan = 0 Or .gruppo0 = 1 Or .gruppo0 = 2 Then  '20140703LC  'riportare 20140111LC-24
        .DCan = "31/12/9999"
      End If
      '--- 20140111LC-18 (fine)
    '*****************************************
    'Anno di cessazione dei contributi ridotti
    '*****************************************
    '20120112LC (inizio)
    '--- 20170309LC (inizio)  'ex 20170307LC  'ex 20161112LC
    .annoInizCR = 0
    .annoFineCR = 0  '9999
    If OPZ_MOD_REG_1A <> 0 Then '20170220LC
      If .gruppo00 = 1 Then
        dInizCR = .dAssunz
        anno = Year(dInizCR)
      ElseIf .gruppo00 = 5 And .PensAUS > 0 Then
        dInizCR = "01/01/" & .PensAUS
        anno = Year(dInizCR)
        If anno <= .annoUC + 3 Then
          anno = 0
        End If
      Else
        anno = 0
      End If
      ia = anno - appl.t0
      If ia >= 0 Then
        If appl.cvR1(ia).cvR1Anni > 0 Then
          If dInizCR < DateAdd("yyyy", appl.cvR1(ia).cvR1EtaMax + 1, .DNasc) Then
            .annoInizCR = anno
            .annoFineCR = anno + appl.cvR1(ia).cvR1Anni
          End If
        End If
      End If
    End If
    '--- 20170309LC (inizio)  'ex 20170307LC  'ex 20161112LC
    '**************************
    'Inizializzazione strutture
    '**************************
    t1 = appl.t0: If t1 < .annoAss Then t1 = .annoAss
    ReDim .s(.annoAss To .annoIniz + P_DUECENTO)
    ReDim .om_rcU(eom.om_max)  '20131122LC  'ex 20120114LC
    ReDim .om_rcV(eom.om_max)  '20131122LC  'ex 20120114LC
    '--- CV1 ---
    .AnniElab1 = appl.parm(P_btAnniEl1)
    .AnniElab2 = .AnniElab1 + OPZ_ELAB_CODA
    .TipoElab = appl.parm(P_TipoElab)
    '--- CV2 ---
    '20120214LC (inizio)
    'If .sesso = 1 Then
    '  .PRC_PV_ATT = appl.parm(P_cvPrcPVattM)
    '  .PRC_PA_ATT = appl.parm(P_cvPrcPAattM)
    '  .PRC_PC_ATT = appl.parm(P_cvPrcPCattM)
    '  .PRCTTLZ = appl.parm(P_cvPrcPTattM)
    'Else
    '  .PRC_PV_ATT = appl.parm(P_cvPrcPVattF)
    '  .PRC_PA_ATT = appl.parm(P_cvPrcPAattF)
    '  .PRC_PC_ATT = appl.parm(P_cvPrcPCattF)
    '  .PRCTTLZ = appl.parm(P_cvPrcPTattF)
    'End If
    '20120214LC (fine)
    '--- CV3 ---
    .ZMAX = appl.parm(P_ZMAX)
    '---
    .qm = 0
    .xan = appl.t0 - Year(.DNasc)
    .xr = .xan + 1 - (Data360(.DNasc) - Data360("1/1/" & Year(.DNasc)) + 1) / 360
    '.xr = appl.t0 + 1 - Data360(.dNasc) / 360
    '--- 20150322LC (fine)
    If OPZ_PENS_C0 = 1 Then '20161126LC
      .coe0 = 1#  '12 / 12
    Else
      .coe0 = Month(.DNasc) / 12
    End If
    '--- 20150322LC (fine)
    .coe1 = 1 - .coe0
    .c1 = .xr - Int(.xr)
    .c0 = 1 - .c1
    .doe1 = Month(.dIscrFondo) / 12
    .doe0 = 1 - .doe0
    bTipo = 0
    .pwDB = 0
    .pwDB_fc = 0  '20121115LC
    .pwDB_fa1 = 0  '20131202LC
    .nSup = 0
    Erase .sup
    '*********************************
    'Determina il tipo di contribuente
    '*********************************
    .t = .annoAss
    .s(.t).gruppo1 = .gruppo0
    .s(.t).napa = 0
    .s(.t).tapa = 0
    .s(.t).bPensAtt = False
    .s(.t).it = 0
    '---
    If .gruppo0 = 4 Then
      .nSup = dia.nSup
      If .nSup > 3 Then
        .nSup = 3
      End If
      ReDim .sup(.nSup - 1)
      For j = 0 To .nSup - 1
        .sup(j).mat = dia.sup(j).mat
        '--- 201340317LC (fine)
        '--- 20140111LC-10 (inizio) mat=403396 sup=423396 ha data <0 non NULL
        .sup(j).bDNascSupNull = dia.sup(j).bDNascSupNull
        '--- 20140111LC-10 (fine)
        .sup(j).DNasc = dia.sup(j).DNasc
        .sup(j).s = dia.sup(j).s
        If .sup(j).s <> 2 Then
          .sup(j).s = 1
        End If
        .sup(j).tipGen = dia.sup(j).tipGen
        .sup(j).rev = dia.sup(j).rev
        If .sup(j).bDNascSupNull = 0 Then  '20140111LC-10
          .sup(j).y = appl.t0 - Year(.sup(j).DNasc)
          .sup(j).c0 = (Data360(.sup(j).DNasc) Mod 360) / 360#
          '--- 20150522LC  'ex 20140111LC-10 (inizio) (mat=5532, 500539)
          If .sup(j).c0 <= AA0 Then
            .sup(j).c0 = 1 + .sup(j).c0
          End If
          '--- 20150522LC (fine)
          .sup(j).c1 = 1 - .sup(j).c0
          If .sup(j).tipGen <> 4 Or (.sup(j).y >= appl.parm(P_ZMAX)) Then   '20140425LC
            .sup(j).tipSup = 1
            .sup(j).ymax = OPZ_OMEGA
          Else
            .sup(j).tipSup = 4
            .sup(j).ymax = appl.parm(P_ZMAX)
          End If
          If j = 0 Then
            'L'interpolazione va fatta sull'et� del 1� superstite
            'non del dante causa
          End If
        ElseIf j = 0 And .sup(j).tipGen <> 4 Then
          'Superstite fino alla morte, senza data di nascita
          'Usiamo quella del dante causa
          .sup(j).y = .xan
          .sup(j).ymax = OPZ_OMEGA
          .sup(j).c0 = .c0
          .sup(j).c1 = .c1
        Else
          'Superstite temporaneo, senza data di nascita
          'Diamogli 26 anni
          .sup(j).y = appl.parm(P_ZMAX)
          .sup(j).ymax = appl.parm(P_ZMAX)
          .sup(j).c0 = 0.5
          .sup(j).c1 = 0.5
        End If
      Next j
      If .nSup = 3 Then
        If .sup(1).rev = 0 And .sup(2).rev = 0 Then
          .nSup = 1
        ElseIf .sup(2).rev = 0 Then
          .nSup = 2
        ElseIf .sup(2).rev = 0 Then
          .sup(1) = .sup(2)
          .nSup = 2
        End If
      ElseIf .nSup = 2 Then
        If .sup(1).rev = 0 Then
          .nSup = 1
        End If
      End If
      If .nSup - 1 <> UBound(.sup) Then
        ReDim Preserve .sup(.nSup - 1)
      End If
      bTipo = 1
      .pwDB = dia.Pensione
      .pwDB_fc = 0  '20121115LC
      .pwDB_fa1 = 0  '20131202LC
      .s(.t).it = 0
      .s(.t).stato = ST_S  ':=
      .s(.t).caus = CONTR_S_0
    ElseIf .gruppo0 = 3 Then
      bTipo = 1
      If .catPens = CAT_PENS_VEC Then
        i = CONTR_PW_0
      ElseIf .catPens = CAT_PENS_SOS Then
        i = CONTR_PS_0
      ElseIf .catPens = CAT_PENS_TOT Then
        i = CONTR_PT_0
      ElseIf .catPens = CAT_PENS_INA Then
        '--- 20121113LC (inizio)
        If OPZ_INT_NO_AE_AB_AI = 0 Then
          i = CONTR_PB_0
        Else
          i = CONTR_PW_0
        End If
        '--- 20121113LC (fine)
      ElseIf .catPens = CAT_PENS_INV Then
        '--- 20121113LC (inizio)
        If OPZ_INT_NO_AE_AB_AI = 0 Then
          i = CONTR_PI_0
        Else
          i = CONTR_PW_0
        End If
        '--- 20121113LC (fine)
      Else
        i = CONTR_PA_0
      End If
      .pwDB = dia.Pensione
      .pwDB_fc = 0  '20121115LC
      .pwDB_fa1 = 0  '20131202LC
      .s(.t).stato = ST_PNA  ':=
      .s(.t).caus = i
      '--- 20140307LC (inizio)  'riportare 20140111LC-24
      If .annoAss <= appl.t0 Then  'iscritti
        .annoPensA = dia.PensAnno
        If .annoPensA = 0 Then
          .annoPensA = appl.t0
        End If
      Else  'nuovi iscritti
        .annoPensA = .annoAss
      End If
      '--- 20140307LC (fine)
    ElseIf .gruppo0 = 2 Then
      bTipo = 2
      .pwDB = dia.Pensione
      .pwDB_fc = 0  '20121115LC
      .pwDB_fa1 = 0  '20131202LC
      '--- 20150612LC (inizio)  'ex 20150609LC  'ex 20150410LC
      napa = appl.CoeffVar(dip.PensAUS, ecv.cv_SpAnni)
      If napa < 72 - dip.PensAUS + 1 + Year(dip.DNasc) Then
        napa = 72 - dip.PensAUS + 1 + Year(dip.DNasc)
      End If
      '--- 20150612LC (fine)
      '--- 20140307LC (inizio)  'riportare 20140111LC-24
      If .annoAss <= appl.t0 Then  'iscritti
        y = dip.PensAUS  '20150612LC  'ex dia.PensAUS=0
        '--- 20140111LC-8 (inizio) (mat=661)
        '.s(.t).napa = appl.parm(P_cvSpAnni)
        .s(.t).napa = napa
        '--- 20140425LC (inizio)
        If .s(.t).napa <= 0 Then
          .s(.t).tapa = 0
        '--- 20140425LC (fine)
        '--- 20150612LC (inizio)
        Else
          y = appl.t0 + 1 - .PensAUS
          If y < .s(.t).napa Then
            .s(.t).tapa = y
          Else
            .s(.t).tapa = napa  '20160419LC-3  (mat=3266600)
          End If
        End If
        '--- 20150612LC (fine)
      Else  'nuovi iscritti
        'Gli si fa cominciare il quinquennio
        '.s(.t).NAPA = appl.parm(P_cvSpAnni)
        .s(.t).napa = napa
        'If Int(.xr + .annoAss - appl.t0) + appl.parm(P_cvSpAnni) > appl.parm(P_cvSpEtaMax) Or .s(.t).sm <= AA0 Then
        '  .s(.t).napa = 0
        'End If
        .s(.t).tapa = 0
      End If
      '--- 20140307LC (fine)  'riportare 20140111LC-24
      .s(.t).bPensAtt = True
      .s(.t).it = 2
      .s(.t).stato = ST_PAT  ':=
      .s(.t).caus = IIf(y >= 2004, CONTR_PW_5, CONTR_PW_2)
      '--- 20140307LC (inizio)  'riportare 20140111LC-24
      If .annoAss <= appl.t0 Then  'iscritti
        .annoPensA = dia.PensAnno
        If .annoPensA = 0 Then
          .annoPensA = appl.t0
        End If
      Else  'nuovi iscritti
        .annoPensA = .annoAss
      End If
      '--- 20140307LC (fine)
    ElseIf .gruppo0 = 1 Or .gruppo0 = 5 Or .gruppo0 = 6 Then
      '--- StatoAttivo --- 20111230LC (spostato da gosub StatoAttivo)
      .s(.t).it = 1
      .s(.t).stato = ST_A  ':=
      '--- 20150504LC (inizio)
      If .gruppo00 = 6 Then
        .s(.t).caus = CONTR_VA
      '--- 20150504LC (fine)
      ElseIf .gruppo0 = 5 Then
        .s(.t).caus = CONTR_EA
      Else
        .s(.t).caus = CONTR_AA
      End If
    End If
    '20120220LC (inizio) 'ex 20120113LC 'prima legge irpef/ifa poi anzianit�
    '***************
    'Leggi_IRPEF_IVA
    '***************
    '--- 20150521LC (inizio)
    If .gruppo0 = 5 Or .gruppo0 = 6 Then
      ReDim appl.tb2(appl.nSize1 - 1)
    End If
    '--- 20150521LC (fine)
    .t0 = .annoAss
    For .t = .annoAss To .annoIniz
      .s(.t) = .s(.t0)
      If .gruppo0 = 1 Or .gruppo0 = 2 Or .gruppo0 = 5 Or .gruppo0 = 6 Then  '20150527LC  'ex 20140307LC  'riportare 20140111LC-24
        Call TIscr2_Aggiorna_Redd0(dip, .s(.t0), .s(.t), appl, appl.CoeffVar(), dia)  '20150525LC  'ex 20150421LC
        Call TIscr2_Aggiorna_Stato(dip, .s(.t0), .s(.t), appl.CoeffVar())
'If .t = .annoIniz Then
'.t = .t
'End If
        Call TIscr2_Aggiorna_Contr0(dip, .s(.t0), .s(.t), appl, appl.CoeffVar(), dia)
        '--- 20150521LC (inizio)
        If .gruppo0 = 5 Or .gruppo0 = 6 Then
          Call TIscr2_MovPop_Contributi(dip, appl, appl.tb2, .t - appl.t0, dip.wa(0), dip.wa(0), .s(.t0), .s(.t), 0) '20121114LC
        Else
          Call TIscr2_MovPop_Contributi(dip, appl, appl.tb1, .t - appl.t0, dip.wa(0), dip.wa(0), .s(.t0), .s(.t), 0) '20121114LC
        End If
        '--- 20150521LC (fine)
      End If
      .t0 = .t
      '--- 20150609LC (inizio)  'ex 20150609LC
      If 1 = 1 Then
        .annoUC = dia.annoUC
      ElseIf .s(.t).c_tot > 0 Then
        If .t <= Year(dip.DCan) Then
          .annoUC = .t
        End If
      End If
      '--- 20150612LC (fine)
    Next .t
    '--- 20150518LC (inizio)  'ex 20150413LC
    If OPZ_INT_MONT_CALC > 0 Then
      ijk = .iSQ + appl.nSize4 * (.t0 - 1960)  '20160611LC
      '--- 20150612LC (inizio)
      'i = .s(.annoIniz).ico
      'i = om_misto_obbl
      If .gruppo0 >= 2 And .gruppo0 <= 4 Then
        i = eom.om_pens_att
      Else
        i = eom.om_misto_obbl
      End If
      j = TIscr2_Mont_Prd(i + 0)
      .om_rcU(i) = 0
      .om_rcV(i) = dia.om0 / appl.CoeffVar(.anno0, j)
      '--- 20150612LC (fine)
      '--- 20150616LC (inizio)  'commentata
      'If Math.Abs(OPZ_INT_COMP) = COMP_ENAS Then  '20150526LC
      '  .om_rcV(i) = .om_rcV(i) + dia.cmNI / appl.CoeffVar(.anno0 + 1, j)
      'End If
      '--- 20150616LC (fine)
      'For i = eom.om_min To eom.om_max
        .s(.anno1).om_rcU(i) = .om_rcU(i)
        .s(.anno1).om_rcV(i) = .om_rcV(i)
      'Next i
      appl.tb1(ijk + TB_OMRC_0 + i) = appl.tb1(ijk + TB_OMRC_0 + i) + dia.om0   '20160611LC
      appl.tb1(ijk + TB_OMRC) = appl.tb1(ijk + TB_OMRC) + dia.om0               '20160611LC
      '--- 20160606LC (inizio)
      i = om_misto_int
      j = TIscr2_Mont_Prd(i + 0)
      .om_rcU(i) = 0
      .om_rcV(i) = dia.qm0 / appl.CoeffVar(.anno0, j)
      .s(.anno1).om_rcU(i) = .om_rcU(i)
      .s(.anno1).om_rcV(i) = .om_rcV(i)
      appl.tb1(ijk + TB_OMRC_0 + i) = appl.tb1(ijk + TB_OMRC_0 + i) + dia.qm0  '20160611LC
      '--- 20160606LC (fine)
    End If
    '--- 20150518LC (fine)
    '***********************
    'Lettura delle anzianit�
    '***********************
.t = -1
    '--- 20150518LC (inizio)
    If .annoAss <= appl.t0 Or 1 = 1 Then
      '--- 20150410LC (inizio)
      .t = .anno1  '.annoIniz
      If .annoAss <= appl.t0 Then
        .d1 = dia.h + AA0 - Int(dia.h + AA0) '20140703LC  '(rsMat!anAnzTot Mod 1000) / 360
      Else
        .d1 = (Data360("31/12/" & Year(.dAssunz)) - Data360(.dAssunz) + 1) / 360
      End If
      With .s(.t)
        With .anz
          .h0_retr_ante = dia.ha
          .h0_retr_post = 0#
          '---
          .h1_retr_ante = 0#
          .h1_retr_post = dia.hb
          '---
          .h_retr_ante = .h0_retr_ante + .h1_retr_ante
          .h_retr_post = .h0_retr_post + .h1_retr_post
          .h_retr = .h_retr_ante + .h_retr_post
          '---
          .h2_contr_ante = 0#
          .h2_contr_post = 0#
          '---
          .h3_contr_ante = 0#
          .h3_contr_post = dip.d1 + ih - .h_retr
'If dip.annoAss > appl.t0 Then
'.h3_contr_post = .h3_contr_post - (dip.annoAss - appl.t0)
'End If
          '---
          .h_contr_ante = .h2_contr_ante + .h3_contr_ante
          .h_contr_post = .h2_contr_post + .h3_contr_post
          .h_contr = .h_contr_ante + .h_contr_post
          '---
          .h_ante = .h_retr_ante + .h_contr_ante
          .h_post = .h_retr_post + .h_contr_post
          .h = .h_ante + .h_post
        End With
        .h_retr_ante1 = .anz.h_retr_ante
        .h_retr_post1 = .anz.h_retr_post
        .h_contr_ante1 = 0#
        .h_contr_post1 = .anz.h - .h_retr_ante1 - .h_retr_post1
      End With
      '--- 20150410LC (fine)
    ElseIf .annoAss <= appl.t0 Then
      .d1 = dia.h + AA0 - Int(dia.h + AA0)  '20140703LC  '(rsMat!anAnzTot Mod 1000) / 360
      y = .d1 + ih '20120331LC 'Int(rsMat!anAnzTot / 1000 + AA0)
      y2 = y
      For .t = .annoIniz To .annoAss - 1 Step -1
        If .t < .annoAss Then
          Call MsgBox("TODO-ReadMdb-d", , .mat)
          Stop
        ElseIf .t = .annoAss Then
          y1 = y
        Else
          y1 = dmin(1, y)
        End If
        Call TIscr2_Anz_Dh(.s(.t).anz, dip, .t, appl.CoeffVar(), y1)
        y = y - y1
        If y <= AA0 Then
          'For .t = .t + 1 To .annoIniz
          Do
            .t = .t + 1
            Call TIscr2_Anz_Add(.s(.t).anz, .s(.t - 1).anz)
          Loop While .t <= .annoIniz
          'Next .t
          Exit For
        End If
      Next .t
      '---
      .t = .annoIniz
      .s(.t).h_retr_ante1 = .d1 + ih '20120331LC 'Int(rsMat!anAnzTot / 1000 + AA0)
      .s(.t).h_retr_post1 = 0#
      .s(.t).h_contr_ante1 = 0#
      .s(.t).h_contr_post1 = 0#
If Abs(y2 - .s(.annoIniz).anz.h) > AA0 Then
Call MsgBox("TODO-ReadMdb-e1", , .mat)
Stop
ElseIf Abs(.s(.t).h_retr_ante1 - .s(.annoIniz).anz.h) > AA0 Then
Call MsgBox("TODO-ReadMdb-e2", , .mat)
Stop
End If
    Else 'Nuovi iscritti
'call MsgBox("TODO-ReadMdb-f", , .mat)
'Stop
      .d1 = (Data360("31/12/" & Year(.dAssunz)) - Data360(.dAssunz) + 1) / 360
      .t = .annoAss
      .s(.t).h_retr_ante1 = .d1 + appl.t0 - Year(.dAssunz)
      .s(.t).h_retr_post1 = 0#
      .s(.t).h_contr_ante1 = 0#
      .s(.t).h_contr_post1 = 0#
    End If
    '--- 20150518LC (fine)
    .d0 = 1 - .d1
    .s(.t).AnzTot = .s(.t).h_retr_ante1 + .s(.t).h_retr_post1
    .s(.t).hs = .s(.t).AnzTot + .s(.t).h_contr_ante1 + .s(.t).h_contr_post1
    .s(.t).hf = .s(.t).hs
    '20120220LC (fine) 'ex 20120113LC
    '--- 20140307LC (inizio)  'riportare 20140111LC-24
    If .gruppo0 = 2 Or .gruppo0 = 3 Or .gruppo0 = 4 Then
      .s(.t).hq = appl.t0 - .annoPensA
    Else
      .s(.t).hq = .s(.t).hs
    End If
    '--- 20140307LC (fine)
    '--- 20160309LC (inizio)
    For anno = .t - 1 To .annoAss Step -1
      If .s(anno + 1).hs - 1 > AA0 Then
        If .s(anno).c_sogg0a1 > AA0 Then
          .s(anno).hs = .s(anno + 1).hs - 1
          .s(anno).hf = .s(anno).hs
        Else
          .s(anno).hs = .s(anno + 1).hs
          .s(anno).hf = .s(anno).hs
        End If
      Else
        Exit For
      End If
    Next anno
    '--- 20160309LC (fine)
    '************
    'Fine lettura
    '************
    If szErr <> "" Then GoTo ExitPoint
    '*************************************************
    'Inizializza il modello di ricongiunzione/riscatto
    '*************************************************
    If .gruppo0 <> 1 Then
      .rr_dhMax = 0
    Else
      .rr_dhMax = 1
    End If
    '20121010LC (inizio) 'ex 20120601LC
    '****************************************
    'Test applicabilit� modifiche articolo 26
    '****************************************
    .bModArt26 = 0
    If OPZ_MOD_ART26_MODEL > 0 Then
      If .gruppo0 = 1 Then
        y1 = .xr + 2010 - appl.t0
        y2 = .s(.t).AnzTot + 2010 - appl.t0
        If OPZ_MOD_ART26_MODEL = 2 Then
          y1 = y1 + AA0
          y2 = y2 + AA0
          z = (Data360("31/12/2010") - Data360("05/03/2010")) / 360#
          y1 = y1 - z
          y2 = y2 - z
        End If
        If y1 >= 55 And y2 >= 30 Then
          .bModArt26 = 1 'adesso 1 indica quelli "esonerati" dall'art. 26
        End If
      End If
    End If
    '20121010LC (fine)
    '--- 201040404LC (inizio)
    '*********************************
    'Test applicabilit� modifiche Pmin
    '*********************************
    .bModPmin = 0
    If OPZ_MISTO_PMIN_MODEL = 4 Then
        y1 = .xr + 2012 - appl.t0
        y2 = .s(.t).AnzTot + 2012 - appl.t0
        If y1 >= 55 And y2 >= 20 Then
          .bModPmin = 1
        End If
    End If
    '--- 201040404LC (fine)
    If .anno1 > .anno0 Then
      .tau0 = .anno1 - .anno0
    Else
      .tau0 = 0
    End If
  End With
  
ExitPoint:
  With dip
    On Error GoTo 0
    TIscr2_ReadMdb = szErr
  End With
  Exit Function

ErrorHandler:
  szErr = "Errore " & Err.Number & " (" & Hex(Err.Number) & ") nel modulo " & Err.Source & vbCrLf & Err.Description  '20161112LC
  GoTo ExitPoint
End Function


Public Sub TIscr2_Anz_Add(anz As TIscr2_Anz, dAnz As TIscr2_Anz)
  With anz
    If dAnz.h > AA0 Then
      If dAnz.h_ante > AA0 Then
        .h0_retr_ante = .h0_retr_ante + dAnz.h0_retr_ante
        .h1_retr_ante = .h1_retr_ante + dAnz.h1_retr_ante
         .h_retr_ante = .h0_retr_ante + .h1_retr_ante
        .h2_contr_ante = .h2_contr_ante + dAnz.h2_contr_ante
        .h3_contr_ante = .h3_contr_ante + dAnz.h3_contr_ante
         .h_contr_ante = .h2_contr_ante + .h3_contr_ante
      End If
      If dAnz.h_post > AA0 Then
        .h0_retr_post = .h0_retr_post + dAnz.h0_retr_post
        .h1_retr_post = .h1_retr_post + dAnz.h1_retr_post
         .h_retr_post = .h0_retr_post + .h1_retr_post
        .h2_contr_post = .h2_contr_post + dAnz.h2_contr_post
        .h3_contr_post = .h3_contr_post + dAnz.h3_contr_post
         .h_contr_post = .h2_contr_post + .h3_contr_post
      End If
      '--- 20150307LC (inizio) riportato da prototipo offerta ENAS 20130326LC
      .h_retr_ante = .h0_retr_ante + .h1_retr_ante
      .h_contr_ante = .h2_contr_ante + .h3_contr_ante
      .h_retr_post = .h0_retr_post + .h1_retr_post
      .h_contr_post = .h2_contr_post + .h3_contr_post
      '--- 20150307LC (fine)
       .h_retr = .h_retr_ante + .h_retr_post
      .h_contr = .h_contr_ante + .h_contr_post
      .h_ante = .h_retr_ante + .h_contr_ante
      .h_post = .h_retr_post + .h_contr_post
      .h = .h_ante + .h_post
    End If
  End With
End Sub


Public Sub TIscr2_Anz_Dh(anz As TIscr2_Anz, dip As TIscr2, anno%, CoeffVar#(), fraz#)
  Dim anz0 As TIscr2_Anz
  
  anz = anz0
  With anz
    '--- 20150322LC (inizio)
    If anno = 1998 And fraz > 0.25 - AA0 Then
      .h0_retr_ante = fraz - 0.25
      .h1_retr_post = .h1_retr_post + 0.25
      .h_ante = fraz - 0.25
      .h_contr = 0.25
    Else
    '--- 20150322LC (fine)
      Select Case dip.s(anno).ipr
      Case 0
        Select Case dip.s(anno).ico
        Case eom.om_2001
          .h0_retr_ante = fraz
        Case eom.om_2002
          .h1_retr_ante = fraz
        Case eom.om_art25
          '--- 20140111LC-6 (inizio) 838065
          '.h2_contr_ante = fraz
          If fraz <= 1# Then
            'mat=827807  h=3.1 di cui hc=0,1 nel 2009 ed ha=3 dal 2010 al 2012
            .h2_contr_ante = fraz
          Else 'mat=838065, 683304
            .h2_contr_ante = 1
            .h1_retr_ante = fraz - 1
          End If
          '--- 20140111LC-6 (fine)
        Case Else
          .h3_contr_ante = fraz 'eom.om_misto_obbl
        End Select
        .h_ante = fraz
      Case 1
        Select Case dip.s(anno).ico
        Case eom.om_2001
          .h0_retr_post = fraz
        Case eom.om_2002
          .h1_retr_post = fraz
        Case eom.om_art25:
          '--- 20140111LC-6 (inizio)
          '.h2_contr_post = fraz
          If fraz <= 1# Then
            .h2_contr_post = fraz
          Else
            .h2_contr_post = 1
            .h1_retr_post = fraz - 1
          End If
          '--- 20140111LC-6 (fine)
        Case Else
          .h3_contr_post = fraz  'eom.om_misto_obbl
        End Select
        .h_post = fraz
      Case Else
        Call MsgBox("TODO-TIscr2_Anz_Dh-a")
        Stop
      End Select
    End If  '201550322LC
    .h = fraz
  End With
End Sub


'20121230LC (tutta)
Public Function TIScr2_aSol(ByRef dip As TIscr2, s1 As TIscr2_Stato, lp As TIscr2pens, pens#, fc#, iop&) As Double
  'Articolo 29 bis � Contributo di solidariet� a carico dei pensionati (17)
  '1.   Ai trattamenti pensionistici erogati negli anni 2012-2013 � applicato un contributo di solidariet� nella
  '     misura dell'1% conformemente a quanto previsto dall�articolo 24, comma 24, Decreto Legge 6 dicembre 2011,
  '     n. 201, convertito in legge, con modificazioni, dalla Legge 22 dicembre 2011, n. 214.
  '(17) Disposizione aggiunta con delibera C.d.A. n. 73/2012 approvata con nota del Ministero del Lavoro e delle
  '     Politiche Sociali prot. 36/0016415/MA004.A007 del 9/11/2012.
  
  Dim y#
  
  y = 0#
  With dip
    If dip.t >= 2012 And dip.t < 2012 + OPZ_MISTO_SOGG_SOL_ANNI Then
      y = OPZ_MISTO_SOGG_SOL_P1
    End If
  End With
  TIScr2_aSol = y
End Function


Public Sub TZD_Test_C(wa As TWA)
  Dim y#
  
  With wa
    y = .c1cs_z + .c1ct_z
    If (.c1ca_z > AA0 And (.c1cvo_z + .c1cva_z + .c1cvp_z) > AA0) Or (.c1ca_z > AA0 And y > AA0) Or ((.c1cvo_z + .c1cva_z + .c1cvp_z) > AA0 And y > AA0) Then  '20131202LC
      Call MsgBox("TZD_Test_C")
      Stop
    End If
  End With
End Sub


Public Function TZD_Tot(wa As TWA) As Double
  TZD_Tot = TZD_Tot0(wa) + TZD_Tot1(wa)
End Function


Private Function TZD_Tot0(wa As TWA) As Double
  With wa
    TZD_Tot0 = .c0a_z + .c0v_z + .c0e_z  '20150504LC
  End With
End Function


Private Function TZD_Tot1(wa As TWA) As Double
  With wa
    TZD_Tot1 = .c1b_z + .c1i_z + _
               .c1cvo_z + .c1cva_z + .c1cvp_z + .c1ca_z + .c1cs_z + .c1ct_z + _
               .c1nvo_z + .c1nva_z + .c1nvp_z + .c1na_z + .c1ns_z + .c1nt_z  '20131202LC
  End With
End Function


'#############################################################
'20120114LC
Public Sub TIscr2_Aggiorna_Contr_Check(ByRef dip As TIscr2, s1 As TIscr2_Stato, s2 As TIscr2_Stato)
  Dim iErr&
  
  iErr = 0
  If Math.Abs(s1.c_sogg0a1 - s2.c_sogg0a1) > 0.25 Then iErr = iErr + 1 '20120514LC
  If Math.Abs(s1.c_sogg0a2 - s2.c_sogg0a2) > 0.25 Then iErr = iErr + 1 '20120514LC
  If Math.Abs(s1.c_sogg1a + s1.c_sogg2a - s2.c_sogg1a - s2.c_sogg2a) > 0.25 Then iErr = iErr + 2
  If Math.Abs(s1.c_sogg3a - s2.c_sogg3a) > AA0 Then iErr = iErr + 8
  '---
  If Math.Abs(s1.c_int0a - s2.c_int0a) > 0.25 Then iErr = iErr + 32
  If Math.Abs(s1.c_ass0a - s2.c_ass0a) > 0.25 Then iErr = iErr + 32
  '--- 20150512LC (inizio)
  If Math.Abs(s1.c_int1a - s2.c_int1a) > 0.25 Then iErr = iErr + 128
  If Math.Abs(s1.c_int2a - s2.c_int2a) > 0.25 Then iErr = iErr + 256
  If Math.Abs(s1.c_int3a - s2.c_int3a) > 0.25 Then iErr = iErr + 512
  If s1.c_mat <> s2.c_mat Then iErr = iErr + 1024
  If Math.Abs(s1.c_tot - s2.c_tot) > 0.011 Then iErr = iErr + 2048
  '--- 20150512LC (fine)
  If iErr <> 0 Then
    Call MsgBox("TODO-TIscr2_Aggiorna_Contr_Check " & iErr, , dip.mat)
    Stop
  End If
End Sub


Public Sub TIscr2_Aggiorna_Contr0(ByRef dip As TIscr2, ByRef s0 As TIscr2_Stato, ByRef s1 As TIscr2_Stato, ByRef appl As TAppl, ByRef CoeffVar#(), ByRef tia As TIscr2Aux)
'20150512LC  Vol->Fac
'20150525LC  tia
  Dim ia&
  Dim aFac#, cSogg#, cFac#, cInte#, cInteR#, cAssi#
  
  With dip
    ia = appl.t0 - .t
    cSogg = tia.cm(ia)
    '---
    If cSogg <= 1 Then
      cSogg = 0
    End If
    '---
    cFac = 0#
    If s1.bIsMisto = True Then
'call MsgBox("TODO")  '20150307LC (commentato) 'ex 20130304LC
'Stop
      If .gruppo0 = 1 Then
        aFac = OPZ_MISTO_SOGG_VOL_A
      ElseIf .gruppo0 = 2 Then
        aFac = OPZ_MISTO_SOGG_VOL_P
      End If
      If aFac > AA0 Then
        cFac = ARRV(s1.sm2ns * aFac)
      End If
    End If
    '---
    If ON_ERR_RN2 Then On Error Resume Next
    cInte = 0#
    cInteR = 0#
    '---
    cAssi = 0#
    '---
    Call TIscr2_Aggiorna_Contr1(dip, appl, s0, s1, CoeffVar(), True, cSogg, cFac, cInte, cInteR, cAssi)
    '********
    'Montante
    '********
    '20120227LC (obsoleto, e poi AnzTot viene calcolata dopo)
    'If .gruppo0 = 5 And s1.AnzTot < OPZ_EA_HMIN_MONT Then
    'If OPZ_RC_INIZ_AZZERA = 0 Then
    If OPZ_INT_MONT_CALC = 0 Then  '20150518LC
      If (s1.stato = ST_PAT) Then
        If .t > .annoIniz - s1.tapa Then
          Call TIscr2_Aggiorna_Mont(dip, s0, s1, CoeffVar(), .t, False, True)
        End If
      Else
        Call TIscr2_Aggiorna_Mont(dip, s0, s1, CoeffVar(), .t, True, True)  '20111211LC
      End If
    End If
  End With
End Sub


Public Sub TIscr2_Aggiorna_Contr1(ByRef dip As TIscr2, ByRef appl As TAppl, s0 As TIscr2_Stato, s1 As TIscr2_Stato, CoeffVar#(), bDb As Boolean, ByVal cSogg#, ByVal cFac#, ByVal cInte#, ByVal cInteR#, ByVal cAssi#)
'****************************************************************************************************
'SCOPO
'  Calcola i contributi dopo l'anno bilancio
'VERSIONI
'  20130321LC  nuova per prototipo offerta ENAS
'  20150307LC  riportata da prototipo offerta ENAS
'  20150512LC  Vol->Fac
'****************************************************************************************************
  Dim ia As Integer  '20170307LC
  Dim ic&
  Dim ib As Integer  '20170307LC
  Dim fact#, nMandati2#, y0#, y1#  '20150612LC
  Dim cvSogg0Min#, cvSogg1Al#, cvSogg1MaxNS#, cvSogg1MaxS#, cvAss1Al#  '20150612LC  'ex 20150409LC
  Dim cvSogg1AlR#, cvAss1AlR#
  Dim cvSogg0Min1 As Double  '20161116LC-2
  Dim cvInt0Min#, cvInt1Al#, cvInt1Max#, cvInt2Al#, cvInt2Max#, cvInt3Al#, y2#, y3#  '20160606LC
  Dim temp As Double  '20170307LC
  Dim yIva#  '20160620LC
  
  With dip
    '****************
    'Inizializzazione
    '****************
    '20160124LC
    'fact = IIf(bDb = True, 1#, 0.75)  '20150612LC
    fact = IIf(bDb = True And OPZ_INT_MONT_CALC = 1, 1#, OPZ_FRAZ_COMP)  '20160222LC  'ex 0.75
    ic = IIf(dip.Qualifica <= 1, 0, 1)
    '--- 20150322LC (inizio)
    cvSogg0Min = CoeffVar(.t, ecv.cv_Sogg0Min + ic)
    cvSogg0Min1 = cvSogg0Min  '20161116LC-2
    '--- 20150615LC (inizio)
    cvSogg1MaxNS = CoeffVar(.t, ecv.cv_Sogg1Max + ic)
    If OPZ_MASSIMALE_2011 = 1 And .t > 2011 Then
      cvSogg1MaxS = CoeffVar(2011, ecv.cv_Sogg1Max + ic)
    Else
      cvSogg1MaxS = cvSogg1MaxNS
    End If
    '--- 20150615LC (fine)
    cvSogg1Al = CoeffVar(.t, ecv.cv_Sogg1Al + ic)
    cvAss1Al = CoeffVar(.t, ecv.cv_Ass1Al + ic)
    
'    'Mod MDG
'    cvSogg1AlR = CoeffVar(.t, ecv.cv_Sogg1AlR + ic)
'    cvAss1AlR = CoeffVar(.t, ecv.cv_Ass1AlR + ic)
    
    '--- 20170307LC (inizio)
    'Agevolazioni contributive per gli iscritti con et� all�iscrizione non superiore ai 25 anni: per gli agenti che
    'si iscriveranno a far tempo dall�1.1.2018 con et� all�iscrizione al massimo pari a 25 anni, per il triennio
    'successivo all�iscrizione, sar� previsto il versamento della met� del contributo minimo di cui al comma 4
    'dell�art. 5 del Regolamento Istituzionale; per gli agenti con iscrizione effettuata nel 2017, detta
    'disposizione riguarder� esclusivamente il biennio 2018-2019.
    If OPZ_MOD_REG_1A <> 0 Then
      If .t >= .annoInizCR And .t < .annoFineCR Then  '20170309LC
        '--- indice inizio contribuzione ridotta
        ia = .annoInizCR - appl.t0
        If ia >= 0 Then
          '--- antidurata con contribuzione ridotta
          ib = .t - .annoInizCR
          If ib >= 0 And ib <= appl.cvR1(ia).cvR1Anni - 1 Then
            ib = ib + ib + ic
            With appl.cvR1(ia).r1a(ib)
              If OPZ_MOD_REG_1A = 1 Then
                temp = .cvR1Sogg0Min
                If temp <= 0 Then  '20170309LC
                  temp = 1 + temp
                End If
                cvSogg0Min1 = cvSogg0Min * temp
              ElseIf OPZ_MOD_REG_1A = 2 Then
                temp = .cvR1Sogg0Min
                If temp <= 0 Then  '20170309LC
                  temp = 1 + temp
                End If
                cvSogg0Min1 = cvSogg0Min * temp
                cvSogg0Min = cvSogg0Min1
              Else
                temp = .cvR1Sogg0Min
                If temp <= 0 Then  '20170309LC
                  temp = 1 + temp
                End If
                cvSogg0Min1 = cvSogg0Min * temp
                cvSogg0Min = cvSogg0Min1
                '---
                temp = .cvR1Sogg1Al
                If temp <= 0 Then  '20170309LC
                  cvSogg1Al = cvSogg1Al + temp
                Else
                  cvSogg1Al = temp
                End If
                '---
                temp = .cvR1Sogg1Max
                If temp <= 0 Then  '20170309LC
                  temp = 1 + temp
                End If
                cvSogg1MaxNS = cvSogg1MaxNS * temp
                '---
                If OPZ_MASSIMALE_2011 = 1 And dip.t > 2011 Then
                  cvSogg1MaxS = CoeffVar(2011, ecv.cv_Sogg1Max + ic)
                  If cvSogg1MaxS > cvSogg1MaxNS Then
                    cvSogg1MaxS = cvSogg1MaxNS
                  End If
                Else
                  cvSogg1MaxS = cvSogg1MaxNS
                End If
              End If
            End With
          Else
            Call MsgBox("TODO - OPZ_MOD_REG_1")
            Stop
          End If
        End If
      End If
    End If
    '--- 20170307LC (fine)
    '--- 20160606LC (inizio)
    cvInt0Min = CoeffVar(.t, ecv.cv_Int0Min + ic)
    cvInt1Al = CoeffVar(.t, ecv.cv_Int1Al + ic)
    cvInt1Max = CoeffVar(.t, ecv.cv_Int1Max + ic)
    cvInt2Al = CoeffVar(.t, ecv.cv_Int2Al + ic)
    cvInt2Max = CoeffVar(.t, IIf(ic = 0, ecv.cv_Int2Max, ecv.cv_Int2MaxR))  '20160609LC
    cvInt3Al = CoeffVar(.t, ecv.cv_Int3Al + ic)
    '--- 20160606LC (fine)
    '*************************
    'Correzione del soggettivo
    '*************************
    '--- 20150410LC (inizio)
    If bDb = True Then
      s1.sm2ns = s1.sm
      s1.sm2s = s1.sm   '20150615LC
      s1.sm3ns = s1.sm  '20150506LC
      s1.sm3s = s1.sm   '20150615LC
    '--- 20150504LC (inizio)  'ex 20150410LC
    ElseIf s1.sm <= AA0 Then
      s1.sm2ns = 0#
      s1.sm2s = 0#    '20150615LC
      cSogg = 0#
    Else
    '--- 20150504LC (fine)
      '--- 20150518LC (inizio)
      If s1.nMandati2 = 0 Then
        If s0.nMandati2 > 0 Then
          s1.nMandati2 = s0.nMandati2
        ElseIf dip.Qualifica <= 0 Then
          s1.nMandati2 = 0
        ElseIf dip.Qualifica = 1 Then
          s1.nMandati2 = 1
        Else
          s1.nMandati2 = dip.nMandati1
          If s1.nMandati2 < 2 Then
            s1.nMandati2 = 2
          End If
        End If
      End If
      '--- 20150518LC (fine)
      '--- 20150612LC (inizio)
      If dip.gruppo0 <= 2 Then
        nMandati2 = dmax(1#, s1.nMandati2)
      ElseIf dip.gruppo0 = 6 Then
        nMandati2 = 1
      Else
Call MsgBox("TODO")
Stop
        nMandati2 = dmax(1#, s1.nMandati2)
      End If
      '--- 20150612LC (fine)
      s1.sm2ns = dmin(s1.sm, cvSogg1MaxNS * nMandati2)  '20150615LC
      
      'Mod MDG
      If OPZ_STERIL = 1 Then
        If p_dia.tit = 0 Then
            p_dia.tit = 1
        End If
        
        If p_dia.tit = 1 Then
          s1.sm2s = dmin(s1.sm, 3735.05 / (cvSogg1Al + cvAss1Al))
        Else
          s1.sm2s = dmin(s1.sm, (2134.35 * nMandati2) / (cvSogg1Al + cvAss1Al)) '(cvSogg1AlR + cvAss1AlR))
        End If
      Else
        s1.sm2s = dmin(s1.sm, cvSogg1MaxS * nMandati2)  '20150615LC
      End If
      
      y0 = s1.sm2ns
    '--- 20150410LC (fine)
      If y0 > cvSogg1MaxNS * nMandati2 Then   '20150615LC
        y0 = cvSogg1MaxNS * nMandati2         '20150615LC
      End If
      If cvSogg1Al + cvAss1Al <= AA0 Then  '20150511LC
      Else
        y0 = ARRV(y0 * (cvSogg1Al + cvAss1Al))
        If (y0 > cvSogg0Min * nMandati2 And OPZ_MOD_REG_1A = 2) Then  '20170220LC
          y0 = ARRV(cvSogg0Min1 * nMandati2)
        ElseIf y0 < cvSogg0Min * nMandati2 Then
          y0 = ARRV(cvSogg0Min1 * nMandati2)  '20161116LC-2  'ex cvSogg0Min
        End If
        's1.sm2ns = ARRV(cvSogg0Min * nMandati2 / (cvSogg1Al + cvAss1Al))  '20160323LC (commentato)
      End If
      cSogg = y0
      '--- 20150609LC (inizio)  'ex 20150506LC
      'If .t - 2 >= dip.annoAss Then
      If .t >= Year(dip.DCan) Then
        's1.sm3 = ARRV((s1.sm2 + dip.s(.t - 1).sm2 + dip.s(.t - 2).sm2) / 3#)
        s1.sm3ns = s1.sm2ns
        s1.sm3s = s1.sm2s  '20150615LC
      Else
        s1.sm3ns = 0#
        s1.sm3s = 0#  '20150615LC
      End If
      '--- 20150609LC (fine)  'ex 20150506LC
    End If
    '*********************************************
    'Contributo assistenziale prima del soggettivo
    '*********************************************
    If .t < 2004 Or cSogg <= AA0 Or (cvSogg1Al + cvAss1Al) <= AA0 Then  '20150511LC
      s1.c_ass0a = 0#
      s1.c_ass1a = 0#
    Else
      y0 = ARRV(cSogg * cvSogg1Al / (cvSogg1Al + cvAss1Al))
      y1 = ARRV(cSogg - y0)
      cSogg = y0
      'If (.gruppo00 = 6 And .t >= .annoInizCV) Or (s1.bAzzeraReddito > 0) Then
      If (.gruppo0 = 6) Or (s1.bAzzeraReddito > 0) Then   '20150527LC
        s1.c_ass0a = y1
        s1.c_ass1a = 0#
      ElseIf .t > .annoAss Then
    '--- 20150504LC (fine)
        s1.c_ass0a = ARRV(y1 * fact)  '20150612LC  'ex 0.75
        s1.c_ass1a = ARRV(y1 - s1.c_ass0a)
      ElseIf .t = .annoAss Then
        '--- 20160307LC (inizio)
        If Math.Abs(OPZ_FRAZ_COMP - 0.75) <= AA0 Then
          s1.c_ass0a = ARRV(y1 - y1 / (5# - Trimestre(.dAssunz)))
          s1.c_ass1a = ARRV(y1 - s1.c_ass0a)
        Else
          s1.c_ass0a = y1
          s1.c_ass1a = 0#
        End If
        '--- 20160307LC (fine)
      Else
        s1.c_ass0a = 0#
        s1.c_ass1a = 0#
      End If
    End If
    '**********************************
    'Contributo soggettivo e volontario
    '**********************************
    '--- 20150609LC (inizio)  ex 20150504LC
    s1.c_sogg0a1 = 0#
    s1.c_sogg0a2 = 0#
    s1.c_sogg1a = 0#
    s1.c_sogg2a = 0#
    s1.c_sogg3a = 0#
    If cSogg <= AA0 Then
    ElseIf (dip.gruppo0 = 6) Then   '20150609LC
      s1.c_sogg0a2 = cSogg
    ElseIf (s1.bAzzeraReddito > 0) Or (cvSogg1Al + cvAss1Al) <= AA0 Then  '20150511LC
      s1.c_sogg0a1 = cSogg
    ElseIf .t > .annoAss Then
      s1.c_sogg0a1 = ARRV(fact * cSogg)  '20150612LC  'ex 0.75
      s1.c_sogg1a = ARRV(cSogg - s1.c_sogg0a1)
    ElseIf .t = .annoAss Then
      '--- 20160307LC (inizio)
      If Math.Abs(OPZ_FRAZ_COMP - 0.75) <= AA0 Then
        s1.c_sogg0a1 = ARRV(cSogg - cSogg / (5# - Trimestre(.dAssunz)))
        s1.c_sogg1a = ARRV(cSogg - s1.c_sogg0a1)
      Else
        s1.c_sogg0a1 = cSogg
        s1.c_sogg1a = 0#
      End If
      '--- 20160307LC (fine)
    End If
    '--- 20150609LC (fine)
    '--- 20150616LC (inizio)
    If bDb = True And .t = appl.t0 And .gruppo0 <= 2 Then
      y0 = ARRV(.cmNI * cvSogg1Al / (cvSogg1Al + cvAss1Al))
      y1 = ARRV(.cmNI - y0)
      s1.c_sogg1a = ARRV(s1.c_sogg1a + y0)
      s1.c_ass1a = ARRV(s1.c_ass1a + y1)
    End If
    '--- 20150616LC (fine)
    '********************************************
    'Contributo assistenziale dopo del soggettivo
    '********************************************
    's1.c_ass0a = 0
    's1.c_ass1a = 0
    '*****************************
    'Contributo integrativo / FIRR
    '*****************************
    '--- 20160606LC (inizio)
    y0 = 0#
    y1 = 0#
    y2 = 0#
    y3 = 0#
    '--- 20160610LC (inizio)
    s1.iva = 0#
    If (bDb = False Or dip.t = appl.t0) And (dip.gruppo00 = 1 Or dip.gruppo00 = 2) Then
      If dip.t = appl.t0 Then
        s1.iva = dip.irpefNI
      Else
        s1.iva = s1.sm
      End If
      If dip.t = Year(dip.dIscrFondo) Then
        fact = (13 - Month(dip.dIscrFondo)) / 12#
        cvInt1Max = cvInt1Max * fact
        cvInt2Max = cvInt2Max * fact
      End If
    ElseIf (bDb = False And dip.t >= dip.PensAUS And dip.PensAUS > 0) And (dip.gruppo00 = 5 Or dip.gruppo00 = 6) Then
      'If dip.t = dip.PensAUS Then
      '  s1.iva = dip.irpefNI
      'Else
        s1.iva = s1.sm
      'End If
      If dip.t = dip.PensAUS Then
        fact = 6 / 12
        cvInt1Max = cvInt1Max * fact
        cvInt2Max = cvInt2Max * fact
      End If
    End If
    If s1.iva > AA0 Then
    '---  20160610LC (fine)
    '---  20160620LC (inizio)
      If dip.nMandati1 > 1 Then
        yIva = ARRV(s1.iva / dip.nMandati1)
      Else
        yIva = s1.iva
      End If
      If yIva < cvInt1Max Then
        y1 = ARRV(cvInt1Al * yIva)
      ElseIf yIva < cvInt2Max Then
        y1 = ARRV(cvInt1Al * cvInt1Max)
        y2 = ARRV(cvInt2Al * (yIva - cvInt1Max))
      Else
        y1 = ARRV(cvInt1Al * cvInt1Max)
        y2 = ARRV(cvInt2Al * (cvInt2Max - cvInt1Max))
        y3 = ARRV(cvInt3Al * (yIva - cvInt2Max))
      End If
      If dip.nMandati1 > 1 Then
        y1 = y1 * dip.nMandati1
        y2 = y2 * dip.nMandati1
        y3 = y3 * dip.nMandati1
      End If
    '--- 20160620LC (fine)
    End If
    s1.c_int0a = y0
    s1.c_int1a = y1
    s1.c_int2a = y2
    s1.c_int3a = y3
    '--- 20160606LC (fine)
    '************************
    'Contributo per maternit�
    '************************
    s1.c_mat = 0
    '*********************************
    'Contributi integrativi retrocessi
    '*********************************
    s1.c_int_retr0 = 0
    s1.c_int_retr1 = 0
    s1.c_int_ass0 = 0
    s1.c_int_ass1 = 0
    '***********************************
    'Contributi figurativi per i giovani
    '***********************************
    s1.c_sogg_fig0 = 0
    s1.c_sogg_fig1 = 0
    s1.c_int_fig0 = 0
    s1.c_int_fig1 = 0
    '*****************
    'Contributi totali
    '*****************
    '20150512LC
    s1.c_tot = s1.c_sogg0a1 + s1.c_sogg0a2 + s1.c_sogg1a + s1.c_sogg2a + s1.c_sogg3a _
               + s1.c_int0a + s1.c_int1a + s1.c_int2a + s1.c_int3a + 0 * (s1.c_ass0a + s1.c_ass1a + s1.c_mat)
    '########
    'Montante
    '########
    If bDb = False Then
      Call TIscr2_Aggiorna_Mont(dip, s0, s1, CoeffVar(), .t, True, True) '20120118LC
    End If
  End With
End Sub



'20131126LC (tutta)
Private Sub z_TIscr2_Contr_Ass(ByRef dip As TIscr2, s1 As TIscr2_Stato, cvAss0Min#, cvAss1Al#, cvSogg2Max#, bDb As Boolean, cSogg#, cAssi#, cy0#, cy1#)
  Dim y0#, y1#
  
  With dip
    If bDb = True Then
      cy0 = 0#
      cy1 = 0#
      If .t >= 2010 Then '20121118LC
        cy0 = cvAss0Min
        '20120114LC (inizio)
        If (s1.stato = ST_PAT) Then
          If .t = .annoIniz - s1.tapa Then 'Da lettura DB
            'cy0 = ARRV(cy0 * .coe0)
            cy0 = 0
          ElseIf s1.it = 2 And .t = .annoPensA Then 'Da sviluppo
            '20120115LC (inizio)
            cy0 = cy0   '* 1
            '20120115LC (fine)
          ElseIf .t > .annoIniz - s1.tapa Then
            cy0 = 0
          ElseIf s1.it = 2 And .t <> .annoPensA Then
            'call MsgBox("TODO-TIscr2_Aggiorna_Contr0a-4", , .mat)
            'Stop
          End If
        End If
        '20120114LC (fine)
        '---
        '20111230LC (inizio)
        If .t >= .annoIniz Then
          'If OPZ_EA_AZZERA_ass0 <> 0 And .gruppo0 = 5 And Year(.dCan) <= .t And .dCan > 0 Then
          If .gruppo0 = 5 And Year(.DCan) <= .t And .DCan > 0 Then
            cy0 = 0
          End If
        End If
        '20111230LC (fine)
        '---
        If .t = .annoAss And cy0 > 0 Then
          If .t <= 2001 Then 'arrotonda a 1000 lire
            cy0 = ARRV(ARR5(cy0 * s1.mi / 12 * 1.93627, 1) / 1.93627)
          Else 'arrotonda a 0,50 euro
            cy0 = ARR5(cy0 * s1.mi / 12 * 2, 1) / 2
          End If
        End If
        '--- 20121118LC (inizio)
        'If s1.iva <= 0 And .gruppo0 = 5 And Year(.dCan) <= .t And .dCan > 0 Then
        '  cy0 = 0
        '  cy1 = 0
        If cAssi > AA0 Then
          cy1 = cAssi - cy0 'IIf(rsMat("ass" & .t).Value = -1, 0, -rsMat("ass" & .t).Value) - cy0
          If cy1 < 0 Then
            cy1 = 0
            cy0 = cAssi
          End If
        Else
          cy1 = ARRV(cvAss1Al * dmin(s1.sm, cvSogg2Max) - cy0)
          If cy1 < AA0 Then
            cy1 = 0
          End If
        End If
        '---
        cAssi = cy0 + cy1
        '--- 20121204LC (inizio)
        If cAssi > cSogg Then 'Non dovrebbe mai succedere
          cAssi = 0
          cy0 = 0
          cy1 = 0
        End If
        '--- 20121204LC (fine)
        cSogg = ARRV(cSogg - cAssi)
        '--- 20121118LC (fine)
      End If
    ElseIf bDb = False Then
      If s1.it = 0 Then
        cy0 = 0
        cy1 = 0
      Else
        y0 = cvAss0Min
        '20120114LC (inizio)
        If (s1.stato = ST_PAT) Then
          If OPZ_PENS_ATT_MODEL = 1 And .t >= OPZ_PENS_ATT_ANNO Then
            y0 = ARRV(cvAss0Min * OPZ_PENS_ATT_FRAZ) '20121119LC
          ElseIf .t = .annoIniz - s1.tapa Then 'Da lettura DB
            'y0 = ARRV(y0 * .coe0)
            y0 = 0#
          ElseIf s1.it = 2 And .t = .annoPensA Then 'Da sviluppo
            y0 = y0   '* 1  '20120115LC
          ElseIf .t > .annoIniz - s1.tapa Then
            y0 = 0
          ElseIf s1.it = 2 And .t <> .annoPensA Then
            'call MsgBox("TODO-TIscr2_Aggiorna_Contr0a-4", , .mat)
            'Stop
          End If
        End If
        '20120114LC (fine)
        If .t >= .annoIniz Then
          'If OPZ_EA_AZZERA_INT0 <> 0 And .gruppo0 = 5 And Year(.dCan) <= .t And .dCan > 0 Then
          If .gruppo0 = 5 And Year(.DCan) <= .t And .DCan > 0 Then '20121114LC
            y0 = 0
          End If
        End If
        If .t = .annoAss And y0 > 0 Then
          If .t <= 2001 Then 'arrotonda a 1000 lire
            y0 = ARRV(ARR5(y0 * s1.mi / 12 * 1.93627, 1) / 1.93627)
          Else 'arrotonda a 0,50 euro
            y0 = ARR5(y0 * s1.mi / 12 * 2, 1) / 2
          End If
        End If
        '20120403LC
        'y1 = ARRV(cvAss1Al * s1.iva) - y0
        y1 = ARRV(cvAss1Al * dmin(s1.sm, s1.sm2ns)) - y0
        If y1 < 0 Then
          y1 = 0
        End If
        If .t = .annoAss And 3 = 4 Then
          y0 = ARRV(y0 * s1.mi / 12)
          y1 = ARRV(y1 * s1.mi / 12)
        End If
        cy0 = y0
        cy1 = y1
        's1.c_assRa = 0
      End If
    End If
  End With
End Sub


'20131126LC (tutta)
Private Sub z_TIscr2_Contr_Fig(ByRef dip As TIscr2, s0 As TIscr2_Stato, s1 As TIscr2_Stato, CoeffVar#(), bDb As Boolean, _
                               cSogg#, cInte#, cInteR#, cy0#, cy1#, cy2#, cy3#)
  With dip
    If s1.ic = 1 And s1.ALR > 0# And OPZ_MISTO_AGEV_GIOV >= 1 Then  '20131123LC (ex OPZ_MISTO_MODEL=2)
      If OPZ_MISTO_AGEV_GIOV >= 2 Then
        Dim c_sogg0a1#, c_sogg1a#, c_sogg2a#, c_sogg3a#
        Dim c_int0a#, c_int1a#, c_int2a#, c_int3a#  '20150512LC
        
        Call z_TIscr2_Contr_Sogg(dip, s0, s1, _
                                 CoeffVar(.t, ecv.cv_Sogg0Min), CoeffVar(.t, ecv.cv_Sogg1Al), _
                                 CoeffVar(.t, ecv.cv_Sogg1Max), CoeffVar(.t, ecv.cv_Sogg2Al), _
                                 CoeffVar(.t, ecv.cv_Sogg2Max), CoeffVar(.t, ecv.cv_Sogg3Al), _
                                 bDb, cSogg, c_sogg0a1, c_sogg1a, c_sogg2a, c_sogg3a)
        cy0 = (c_sogg0a1 - s1.c_sogg0a1)
        cy1 = (c_sogg1a - s1.c_sogg1a)
        '--- 20140111LC-5 (inizio)
        If cy1 < 0 Then  'mat=400487
          cy0 = cy0 + cy1
          cy1 = 0
        End If
If cy0 < -AA0 Then
Call MsgBox("TODO z_TIscr2_Contr_Fig-1", , .mat)
Stop
End If
        '--- 20140111LC-5 (fine)
        Call z_TIscr2_Contr_Int(dip, s1, _
                                CoeffVar(.t, ecv.cv_Int0Min), CoeffVar(.t, ecv.cv_Int1Al), CoeffVar(.t, ecv.cv_Int1Max), _
                                bDb, cInte, cInteR, c_int0a, c_int1a, c_int2a, c_int3a)  '20150512LC
        cy2 = (c_int0a - s1.c_int0a) * s1.ALR
        cy3 = (c_int1a - s1.c_int1a) * s1.ALR
        If cy3 < 0 Then
          cy2 = cy2 + cy3
          cy3 = 0
        End If
        If cy2 < -AA0 Then
'--- 20140111LC-5 (inizio) mai successo, ma non si sa mai...
Call MsgBox("TODO z_TIscr2_Contr_Fig-2", , .mat)
Stop
'--- 20140111LC-5 (fine)
          cy2 = 0
        End If
      Else
        cy0 = s1.c_sogg0a1
        cy1 = s1.c_sogg1a
        cy2 = s1.c_int0a * s1.ALR
        cy3 = s1.c_int1a * s1.ALR
      End If
    Else
      cy0 = 0#
      cy1 = 0#
      cy2 = 0#
      cy3 = 0#
    End If
  End With
End Sub


Private Sub z_TIscr2_Contr_Int(ByRef dip As TIscr2, s1 As TIscr2_Stato, cvInt0Min#, cvInt1Al#, cvInt1Max#, bDb As Boolean, cInte#, cInteR#, cy0#, cy1#, cy2#, cy3#)
'20131126LC (tutta)
'20150512LC  y3
  Dim y0#, y1#, y1R#, y2#, y3#
  
  With dip
    If bDb = True Then
      If .t >= 1982 Then
        If .t < .annoIniz - 5 Then
          cy0 = 0#
          '20121018LC (inizio)
          cy1 = 0# '20120521LC
          cy2 = 0#
          cy3 = 0#
          '20121018LC (fine)
          's1.c_intRa = 0
        Else
          cy0 = cvInt0Min
          '20120114LC (inizio)
          If (s1.stato = ST_PAT) Then
            If .t = .annoIniz - s1.tapa Then 'Da lettura DB
              'cy0 = ARRV(cy0 * .coe0)
              cy0 = 0
            ElseIf s1.it = 2 And .t = .annoPensA Then 'Da sviluppo
              '20120115LC (inizio)
              cy0 = cy0   '* 1
              '20120115LC (fine)
            ElseIf .t > .annoIniz - s1.tapa Then
              cy0 = 0
            ElseIf s1.it = 2 And .t <> .annoPensA Then
              'call MsgBox("TODO-TIscr2_Aggiorna_Contr0a-2", , .mat)
              'Stop
            End If
          End If
          '20120114LC (fine)
          '20111230LC (inizio)
          If .t >= .annoIniz Then
            'If OPZ_EA_AZZERA_INT0 <> 0 And .gruppo0 = 5 And Year(.dCan) <= .t And .dCan > 0 Then
            If .gruppo0 = 5 And Year(.DCan) <= .t And .DCan > 0 Then '20121114LC
              cy0 = 0
            End If
          End If
          '20111230LC (fine)
          '---
          If .t = .annoAss And cy0 > 0 Then
            If .t <= 2001 Then 'arrotonda a 1000 lire
              cy0 = ARRV(ARR5(cy0 * s1.mi / 12 * 1.93627, 1) / 1.93627)
            Else 'arrotonda a 0,50 euro
              cy0 = ARR5(cy0 * s1.mi / 12 * 2, 1) / 2
            End If
          End If
          '---
          '20121018LC (inizio)
          cy1 = cInteR - cy0
          If cy1 < AA0 Then
            cy1 = 0#
            cy2 = 0#
            cy3 = 0#
            cy0 = cInte
          Else
            cy2 = cInte - cInteR
            cy3 = 0#
If cy2 < 0 Then
Call MsgBox("TODO-TIscr2_Aggiorna_Contr0a-3", , .mat)
Stop
End If
          End If
          '--- 20121119LC (inizio)
          If s1.bIsMisto = False Then
            cy2 = cy1 + cy2 + cy3
            cy1 = 0#
            cy3 = 0#
          End If
          '--- 20121119LC (fine)
          If s1.iva <= 0 And .gruppo0 = 5 And Year(.DCan) <= .t And .DCan > 0 Then
            cy0 = 0
            '20121018LC (inizio)
            cy1 = 0# '20120521LC
            cy2 = 0#
            cy3 = 0#
            '20121018LC (fine)
            's1.c_intRa = 0
          End If
        End If
      Else
        cy0 = 0#
        '20121018LC (inizio)
        cy1 = 0# '20120521LC
        cy2 = 0#
        cy3 = 0#
        '20121018LC (fine)
        's1.c_intRa = 0
      End If
    ElseIf bDb = False Then
      If s1.it = 0 Then
        cy0 = 0#
        cy1 = 0# '20120521LC
        cy2 = 0#
        cy3 = 0#
      Else
        y0 = cvInt0Min
        '20120114LC (inizio)
        If (s1.stato = ST_PAT) Then
          '--- 20130324LC (inizio)
          If OPZ_PENS_ATT_MODEL = 1 And .t >= OPZ_PENS_ATT_ANNO Then
            y0 = ARRV(cvInt0Min * OPZ_PENS_ATT_FRAZ)
          '--- 20130324LC (fine)
          ElseIf .t = .annoIniz - s1.tapa Then 'Da lettura DB
            'y0 = ARRV(y0 * .coe0)
            y0 = 0#
          ElseIf s1.it = 2 And .annoAss > .annoIniz Then  '20140307LC  'riportare 20140111LC  'nuovi iscritti
            y0 = 0#
          ElseIf s1.it = 2 And .t = .annoPensA Then 'Da sviluppo
            '20120115LC (inizio)
            y0 = y0   '* 1  'si riferisce alla fase precedente da attivo
            '20120115LC (fine)
          ElseIf .t > .annoIniz - s1.tapa Then
            y0 = 0#
          ElseIf s1.it = 2 And .t <> .annoPensA Then
            Call MsgBox("TODO-TIscr2_Aggiorna_Contr0a-2", , .mat)
            Stop
          End If
        End If
        '20120114LC (fine)
        '20111230LC (inizio)
        If .t >= .annoIniz Then
          'If OPZ_EA_AZZERA_INT0 <> 0 And .gruppo0 = 5 And Year(.dCan) <= .t And .dCan > 0 Then
          If .gruppo0 = 5 And Year(.DCan) <= .t And .DCan > 0 Then '20121114LC
            y0 = 0#
          End If
        End If
        '20111230LC (fine)
        If .t = .annoAss And y0 > AA0 Then  '20121107LC (sbloccato)
          If .t <= 2001 Then 'arrotonda a 1000 lire
            y0 = ARRV(ARR5(y0 * s1.mi / 12 * 1.93627, 1) / 1.93627)
          Else 'arrotonda a 0,50 euro
            y0 = ARR5(y0 * s1.mi / 12 * 2, 1) / 2
          End If
        End If
        '20121019LC (inizio)  'ex 20120521LC
        'y1 = ARRV(cvInt1Al * s1.iva) - y0
        If s1.iva < 0 Then 'IVA=-1
          y1 = 0#
          y1R = 0#
        Else
          y1 = dmax(0#, ARRV(cvInt1Al * s1.iva) - y0)
          y1R = dmax(0#, ARRV(cvInt1Al * dmin(s1.iva, cvInt1Max)) - y0)
        End If
        If .t = .annoAss And 3 = 4 Then
          y0 = ARRV(y0 * s1.mi / 12)
          y1 = ARRV(y1 * s1.mi / 12)
          y1R = ARRV(y1R * s1.mi / 12)
        End If
        cy0 = y0
        cy1 = y1R
        cy2 = y1 - y1R
        cy3 = 0#
        '20121019LC (fine)
      End If
    End If
  End With
End Sub


Private Sub z_TIscr2_Contr_IntRetr(ByRef dip As TIscr2, s1 As TIscr2_Stato, bDb As Boolean, cy0#, cy1#, cy2#, cy3#)
'20131126LC (tutta)
  With dip
    If s1.ALR > 0# Then
      cy0 = s1.c_int0a * s1.ALR
      cy1 = s1.c_int1a * s1.ALR
    Else
      cy0 = 0#
      cy1 = 0#
    End If
    cy2 = s1.c_int0a - cy0
    cy3 = s1.c_int1a - cy1
  End With
End Sub


'20131126LC (tutta)
Private Sub z_TIscr2_Contr_Mat(ByRef dip As TIscr2, s1 As TIscr2_Stato, cvMat0Min0#, bDb As Boolean, cy0#)
  'Dim y0#, y1#, y2#, y3#
  
  With dip
    cy0 = 0#
    If bDb = True Then
      If .t < OPZ_INT_CV_AMIN Then
        cy0 = 0
      Else
        cy0 = cvMat0Min0
        If s1.sm <= 0 And .gruppo0 = 5 And Year(.DCan) <= .t And .DCan > 0 Then
          cy0 = 0
        ElseIf .t = .annoAss Then
          If .t <= 2001 Then 'arrotonda a 1000 lire
            cy0 = ARRV(ARR5(cy0 * s1.mi / 12 * 1.93627, 1) / 1.93627)
          Else 'arrotonda a 0,50 euro
            cy0 = ARR5(cy0 * s1.mi / 12 * 2, 1) / 2
          End If
        End If
      End If
    ElseIf bDb = False Then
      If .t < OPZ_INT_CV_AMIN Then
        cy0 = 0
      Else
        cy0 = cvMat0Min0
        If s1.sm <= 0 And .gruppo0 = 5 And Year(.DCan) <= .t And .DCan > 0 Then
          cy0 = 0
        ElseIf .t = .annoAss Then
          If .mat < 0 And .t = .annoAss And cy0 > AA0 Then '20121107LC (sbloccato)
            If .t <= 2001 Then 'arrotonda a 1000 lire
              cy0 = ARRV(ARR5(cy0 * s1.mi / 12 * 1.93627, 1) / 1.93627)
            Else 'arrotonda a 0,50 euro
              cy0 = ARR5(cy0 * s1.mi / 12 * 2, 1) / 2
            End If
          End If
        End If
      End If
    End If
  End With
End Sub


'20131126LC (tutta)
Private Sub z_TIscr2_Contr_Sogg(ByRef dip As TIscr2, s0 As TIscr2_Stato, s1 As TIscr2_Stato, cvSogg0Min#, cvSogg1Al#, cvSogg1Max#, cvSogg2Al#, cvSogg2Max#, cvSogg3Al#, bDb As Boolean, cSogg#, cy0#, cy1#, cy2#, cy3#)
  Dim y0#, y1#, y2#, y3#
  
  With dip
    If bDb = True Then
      If .t < 1982 Then
        cy0 = cSogg '20120514LC
        cy1 = 0#
        cy2 = 0#
        cy3 = 0#
        's1.c_soggRa = 0#
      ElseIf .t >= 1982 Then '20111230LC 'i = appl.t0 - .t: if i >= 0 And i <= 24 Then
        cy0 = cvSogg0Min '20120514LC
        '--- 20140111LC-9 (inizio)  'copiato dalla routine per l'integrativo (mat=185)
        If (s1.stato = ST_PAT) Then
          If .t = .annoIniz - s1.tapa Then 'Da lettura DB
            'cy0 = ARRV(cy0 * .coe0)
            cy0 = 0#
          ElseIf s1.it = 2 And .t = .annoPensA Then 'Da sviluppo
            '20120115LC (inizio)
            cy0 = cy0   '* 1
            '20120115LC (fine)
          ElseIf .t > .annoIniz - s1.tapa Then
            cy0 = 0
          ElseIf s1.it = 2 And .t <> .annoPensA Then
            'call MsgBox("TODO-TIscr2_Aggiorna_Contr0a-2", , .mat)
            'Stop
          End If
        End If
        '--- 20140111LC-9 (fine)
        If .t = .annoAss Then
          If .t <= 2001 Then 'arrotonda a 1000 lire
            cy0 = ARRV(ARR5(cy0 * s1.mi / 12 * 1.93627, 1) / 1.93627) '20120514LC
          Else 'arrotonda a 0,50 euro
            cy0 = ARR5(cy0 * s1.mi / 12 * 2, 1) / 2 '20120514LC
          End If
        '20120114LC (inizio)
        ElseIf (s1.stato = ST_PAT) Then
          '20120302LC (inizio)
          If OPZ_PENS_ATT_MODEL = 1 And .t >= OPZ_PENS_ATT_ANNO Then
            'cy0 = cvSogg0Min '20120514LC
            cy0 = ARRV(cvSogg0Min * OPZ_PENS_ATT_FRAZ) '20121008LC
          ElseIf .t = .annoIniz - s1.tapa Then 'Da lettura DB
            cy0 = cy0   '* 1 '20120514LC
          '20120302LC (fine)
          ElseIf (s1.it = 2) And (.t = .annoPensA) Then 'Da sviluppo
            '20120115LC (inizio)
            cy0 = cy0   '* 1 '20120514LC
            '20120115LC (fine)
          ElseIf .t > .annoIniz - s1.tapa Then
            cy0 = 0 '20120514LC
          ElseIf s1.it = 2 And .t <> .annoPensA Then
            'call MsgBox("TODO-TIscr2_Aggiorna_Contr0a-1", , .mat)
            'Stop
          End If
        '20120114LC (fine)
        '20120115LC (fine)
        ElseIf (s1.stato = ST_PNA) Then
          cy0 = 0 '20120514LC
        End If
        '20120115LC (fine)
        '---
        'zb = ARRV(cvSogg2Al * cvSogg2Max)
        '20120506LC (inizio)
        cy1 = ARRV(cvSogg1Al * cvSogg1Max)
        If cy1 < cy0 Then '20120514LC
          'cy1 = cvSogg0Min
          cy1 = cy0  '20140111LC-11  'ex cvSogg0Min (forse sono sempre uguali?)
        End If
        cy2 = 0
        cy3 = 0
        If cSogg <= cy1 Then
          cy1 = cSogg
        Else
          cy2 = ARRV(cvSogg2Al * (cvSogg2Max - cvSogg1Max))
          If cSogg <= cy1 + cy2 Then
            cy2 = ARRV(cSogg - cy1)
          Else
            cy3 = ARRV(cSogg - cy1 - cy2)
          End If
        End If
        If cy1 < 0 Then
          cy1 = 0
        End If
        If cy1 > cy0 Then '20120514LC
          cy1 = cy1 - cy0 '20120514LC
        Else
          cy0 = cy1 '20120514LC
          cy1 = 0
          If 1 = 2 Then '20120528LC
            cy2 = 0
            cy3 = 0
          End If
        End If
        '20120506LC (fine)
        'If .t = .annoIniz And OPZ_EA_AZZERA_SOGG1_INIZ <> 0 Then
        If .t = .annoIniz And 1 = 2 Then '20121114LC
          'Ex_attivi, azzera il saldo dell'ultimo soggettivo (inesigibile)
          cy1 = 0
          cy2 = 0
          cy3 = 0
        End If
      End If
    ElseIf bDb = False Then
      If s1.it = 0 Or .gruppo0 = 5 Then
        cy0 = 0 '20120514LC
        cy1 = 0
        cy2 = 0
        cy3 = 0
      Else
        y0 = cvSogg0Min '20120514LC
        If .t = .annoAss And y0 > AA0 Then  '20121107LC (sbloccato)
          If .t <= 2001 Then 'arrotonda a 1000 lire
            y0 = ARRV(ARR5(y0 * s1.mi / 12 * 1.93627, 1) / 1.93627) '20120514LC
          Else 'arrotonda a 0,50 euro
            y0 = ARR5(y0 * s1.mi / 12 * 2, 1) / 2 '20120514LC
          End If
        '20120115LC (inizio)
        ElseIf (s1.stato = ST_PAT) Then
          '20120302LC (inizio)
          If OPZ_PENS_ATT_MODEL = 1 And .t >= OPZ_PENS_ATT_ANNO Then
            y0 = ARRV(cvSogg0Min * OPZ_PENS_ATT_FRAZ) '20121008LC  'ex 20120514LC
          ElseIf .t = .annoIniz - s1.tapa Then 'Da lettura DB
            y0 = y0   '* 1 '20120514LC
          ElseIf (s1.it = 2) And (.t = .annoPensA) Then 'Da sviluppo
            y0 = y0   '* 1 '20120514LC
          ElseIf .t > .annoIniz - s1.tapa Then
            y0 = 0 '20120514LC
          ElseIf s1.it = 2 And .t <> .annoPensA Then
            Call MsgBox("TODO-z_TIscr2_Contr-Sogg-1", , .mat)
            Stop
          End If
        '20120115LC (fine)
        '---
        ElseIf (s1.stato = ST_PNA) Then
          y0 = 0 '20120514LC
        End If
        '20120114LC (fine)
        y1 = 0#
        y2 = 0#
        y3 = 0#
        If s1.sm <= 0 Then '20121019LC (pleonastico, serve per evidenziare SM=-1)
        ElseIf s1.sm * cvSogg1Al < y0 Then
        ElseIf s1.sm < cvSogg1Max Then
          y1 = ARRV(cvSogg1Al * s1.sm - y0)
          If y1 < 0 Then
            y1 = 0#
          End If
        ElseIf s1.sm < cvSogg2Max Then
          y1 = ARRV(cvSogg1Al * cvSogg1Max - y0)
          y2 = ARRV(cvSogg2Al * (s1.sm - cvSogg1Max))
          'y2 = ARR5(cvSogg2Al * s1.sm - y1 - y0)
        Else
          y1 = ARRV(cvSogg1Al * cvSogg1Max - y0)
          y2 = ARRV(cvSogg2Al * (cvSogg2Max - cvSogg1Max))
          'y2 = ARRV(cvSogg2Al * cvSogg2Max - y1 - y0)
          y3 = ARRV(cvSogg3Al * (s1.sm - cvSogg2Max))
        End If
If y1 < 0 Then
Call MsgBox("TODO-TIscr2_Aggiorna_Contr-2", , .mat)
Stop
End If
        If .t = .annoAss And 3 = 4 Then
          y0 = ARRV(y0 * s1.mi / 12)
          y1 = ARRV(y1 * s1.mi / 12)
          y2 = ARRV(y2 * s1.mi / 12)
          y3 = ARRV(y3 * s1.mi / 12)
        End If
        '--- 20131015LC (inizio)
        If OPZ_INT_REGMIN_MODEL > 1 Then
          If .t >= 2014 And .t <= 2018 And .gruppo0 = 1 And (s1.stato = ST_A) Then
            If y1 = 0 Then
'If y0 < ARRV(s1.sm * cvSogg1Al) Then
'call MsgBox("TODO TIscr2_Aggiorna_Contr1 1", , .mat)
'Stop
'End If
              If OPZ_INT_REGMIN_MODEL = 1 Then
                y0 = ARRV(s1.sm * cvSogg1Al)
              ElseIf OPZ_INT_REGMIN_MODEL = 2 Then
                y0 = ARRV(s0.sm * cvSogg1Al)
              End If
            End If
          End If
        End If
        '--- 20131015LC (fine)
        cy0 = y0 '20120514LC
        cy1 = y1
        cy2 = y2
        cy3 = y3
      End If
    End If
  End With
End Sub


Private Sub z_TIscr2_Contr_Fac(ByRef dip As TIscr2, s1 As TIscr2_Stato, bDb As Boolean, cFac#, cy0#)
'20131126LC (tutta)
'20150512LC  Vol->Fac
  Dim aFac#
  
  With dip
    aFac = 0#
    cy0 = 0#
    If bDb = True Then
      cy0 = cFac
    ElseIf bDb = False Then
      If s1.bIsMisto = True Then
        '--- 20130304LC (inizio)
        'If .gruppo0 = 1 Then
        If s1.it = 1 Then
          aFac = OPZ_MISTO_SOGG_VOL_A
        'ElseIf .gruppo0 = 2 Then
        ElseIf s1.it = 2 Then
          aFac = OPZ_MISTO_SOGG_VOL_P
        End If
        '--- 20130304LC (fine)
        If aFac > AA0 Then
          cy0 = ARRV(s1.sm2ns * aFac)
        End If
      End If
    End If
  End With
End Sub


Public Sub TIscr2_MovPop_Superstiti4_0(ByRef dip As TIscr2, ByRef appl As TAppl, iSup&)
'ex 20080915LC
'20130216LC (tutta)
  Dim iQ&, iSex0&, iro&, irv&, jQ&, nSup&, t&, tau&, z&, z0&, z1&, z2&  '20130216LC  (tolto: x)
  Dim c0#, c1#, fact#, prob#, prod#, q#, ro#, rv#, y0#, y1#         '20130220LC
  
  'c0 = 0.5
  c0 = dip.sup(0).c0
  c1 = 1# - c0  '20130220LC
  '******************
  'Muove i superstiti
  '******************
  With appl
    If dip.zs <= AA0 Then
      Exit Sub
    End If
    '20080917LC
    '--- 20140422LC (inizio)
    prob = 1#  'dip.zs
    '--- 20140422LC (fine)
    '--- 20121204LC (inizio)
    'iSex = dip.sesso
    z0 = dip.sup(0).y
'20081002LC - mat=400383 ha 115anni
If z0 > OPZ_OMEGA Then z0 = OPZ_OMEGA
    iSex0 = dip.sup(0).s
    'iQ = dip.iQ + 1
    iQ = IIf(OPZ_INT_SUP_4 = 1, 0, 2 * (dip.Qualifica - 1)) + dip.sesso  '20121212LC
    'jQ = 2 * (dip.Qualifica - 1) + iSex0 '- 1 + 1
    jQ = IIf(OPZ_INT_SUP_4 = 1, 0, 2 * (dip.Qualifica - 1)) + iSex0      '20121212LC
    '--- 20121204LC (fine)
    'ReDim .pfam4(0, iQ To iQ, OPZ_OMEGA) As TWF  '20121204LC (� solo un commento, non servono i primi due indici)
    ReDim .pfam4(OPZ_OMEGA)
    tau = 0  '20121204LC (pleonastico, solo per ricordare l'importanza di TAU per la mortalit� variabile)
    '--- Gruppi 1-3 ---
    'If z0 >= .parm(P_ZMAX) Then
    If dip.sup(0).ymax > .parm(P_ZMAX) Then
      If dip.nSup = 2 Then
        z1 = dip.sup(1).y
      ElseIf dip.nSup = 3 Then
        z1 = dip.sup(2).y
        z2 = dip.sup(1).y
      End If
      If z0 <= OPZ_OMEGA Then
        For t = 0 To appl.nMax2 + OPZ_ELAB_CODA
          If z0 > OPZ_OMEGA Then
            Exit For
          '--- 20130220LC (inizio)
          Else
            y0 = InterpL(appl.LL1, BT_QSD, iSex0, dip.Qualifica, z0 - 1)
            If y0 >= AA9 Then
              Exit For
            End If
            y1 = InterpL(appl.LL1, BT_QSD, iSex0, dip.Qualifica, z0 - 0)
            If t = 0 Then
              prod = 1#
            Else
              '--- 20140422LC (inizio)  prima skippava il caso OPZ_ABB_QM_SUP <= 0
              If OPZ_ABB_QM_SUP > 0 Then
                If OPZ_INT_ABBQ = 0 Then
                  fact = TIscr2_AbbQ(appl.BDA, tau + t - 1, z0 - c0, jQ - 1)
                  y0 = y0 * fact
                  y1 = y1 * fact
                Else
                  y0 = y0 * TIscr2_AbbQ(appl.BDA, tau + t - 1, z0 - 1, jQ - 1)
                  If y1 < AA9 Then
                    y1 = y1 * TIscr2_AbbQ(appl.BDA, tau + t - 1, z0 - 0, jQ - 1)
                  End If
                End If
                'q = InterpL(appl.LL1, BT_QSD, iSex0, dip.Qualifica, z0 - 1) * TIscr2_AbbQ(appl.BDA, tau + t - 1, z0 - 1, jQ - 1) * c0 + _
                '    InterpL(appl.LL1, BT_QSD, iSex0, dip.Qualifica, z0 - 0) * TIscr2_AbbQ(appl.BDA, tau + t - 1, z0 - 0, jQ - 1) * (1 - c0)
              End If
              '--- 20140422LC (fine)
              q = (y0 * c0 + y1 * c1)
              prod = prod * (1 - q)
            End If
          End If
          '--- 20130220LC (fine)
          '--- Gruppo 1 ---
          If dip.nSup = 1 Then
            irv = 1: rv = dip.sup(0).rev
            iro = 0: ro = 0
            .pfam4(t).sg(irv) = .pfam4(t).sg(irv) + prob * prod
            .pfam4(t).sg(0) = .pfam4(t).sg(0) + prob * prod
            .pfam4(t).sg(7) = .pfam4(t).sg(7) + prob * prod
            .pfam4(t).sg(8) = .pfam4(t).sg(8) + prob * prod * rv
          '--- Gruppo 2 ---
          ElseIf dip.nSup = 2 Then
            If z1 < appl.parm(P_ZMAX) Then
              irv = 2: rv = dip.sup(0).rev + dip.sup(1).rev
              iro = 4: ro = dip.sup(0).rev
              nSup = 2
            Else
              irv = 1: rv = dip.sup(0).rev
              iro = 0: ro = 0
              nSup = 1
            End If
            .pfam4(t).sg(irv) = .pfam4(t).sg(irv) + prob * prod
            If iro > 0 Then
              .pfam4(t).sg(iro) = .pfam4(t).sg(iro) + prob * (1 - prod)
              .pfam4(t).sg(0) = .pfam4(t).sg(0) + prob
              .pfam4(t).sg(7) = .pfam4(t).sg(7) + prob * (1 + prod)
              .pfam4(t).sg(8) = .pfam4(t).sg(8) + prob * (prod * rv + (1 - prod) * ro)
            Else
              .pfam4(t).sg(0) = .pfam4(t).sg(0) + prob * prod
              .pfam4(t).sg(7) = .pfam4(t).sg(7) + prob * prod
              .pfam4(t).sg(8) = .pfam4(t).sg(8) + prob * prod * rv
            End If
            z1 = z1 + 1
          '--- Gruppo 3 ---
          ElseIf dip.nSup = 3 Then
            If z2 < appl.parm(P_ZMAX) Then
              irv = 3: rv = dip.sup(0).rev + dip.sup(2).rev + dip.sup(1).rev
              iro = 5: ro = dip.sup(0).rev + dip.sup(2).rev
              nSup = 3
            ElseIf z1 < appl.parm(P_ZMAX) Then
              irv = 2: rv = dip.sup(0).rev + dip.sup(2).rev
              iro = 4: ro = dip.sup(0).rev
              nSup = 2
            Else
              irv = 1: rv = dip.sup(0).rev
              iro = 0: ro = 0
              nSup = 1
            End If
            .pfam4(t).sg(irv) = .pfam4(t).sg(irv) + prob * prod
            If iro > 0 Then
              .pfam4(t).sg(iro) = .pfam4(t).sg(iro) + prob * (1 - prod)
              .pfam4(t).sg(0) = .pfam4(t).sg(0) + prob
              .pfam4(t).sg(7) = .pfam4(t).sg(7) + prob * (nSup - 1 + prod)
              .pfam4(t).sg(8) = .pfam4(t).sg(8) + prob * (prod * rv + (1 - prod) * ro)
            Else
              .pfam4(t).sg(0) = .pfam4(t).sg(0) + prob * prod
              .pfam4(t).sg(7) = .pfam4(t).sg(7) + prob * prod
              .pfam4(t).sg(8) = .pfam4(t).sg(8) + prob * prod * rv
            End If
            z2 = z2 + 1
            z1 = z1 + 1
          End If
          z0 = z0 + 1
        Next t
      End If
    '--- Gruppi 4-6 ---
    Else
      If dip.nSup = 2 Then
        z1 = dip.sup(1).y
      ElseIf dip.nSup = 3 Then
        z1 = dip.sup(1).y
        z2 = dip.sup(2).y
      End If
      '--- Gruppo 4 ---
      For t = 0 To appl.nMax2 + OPZ_ELAB_CODA
        If dip.nSup = 1 Then
          If z0 < appl.parm(P_ZMAX) Then
            iro = 4: ro = dip.sup(0).rev
          Else
            Exit For
          End If
          .pfam4(t).sg(iro) = .pfam4(t).sg(iro) + prob
          .pfam4(t).sg(0) = .pfam4(t).sg(0) + prob
          .pfam4(t).sg(7) = .pfam4(t).sg(7) + prob
          .pfam4(t).sg(8) = .pfam4(t).sg(8) + prob * ro
          z0 = z0 + 1
        ElseIf dip.nSup = 2 Then
          If z1 < appl.parm(P_ZMAX) Then
            iro = 5: ro = dip.sup(0).rev + dip.sup(1).rev
            nSup = 2
          ElseIf z0 < appl.parm(P_ZMAX) Then
            iro = 4: ro = dip.sup(0).rev
            nSup = 1
          Else
            Exit For
          End If
          .pfam4(t).sg(iro) = .pfam4(t).sg(iro) + prob
          .pfam4(t).sg(0) = .pfam4(t).sg(0) + prob
          .pfam4(t).sg(7) = .pfam4(t).sg(7) + prob * nSup '20080915LC
          .pfam4(t).sg(8) = .pfam4(t).sg(8) + prob * ro
          z1 = z1 + 1
          z0 = z0 + 1
        ElseIf dip.nSup = 3 Then
          If z2 < appl.parm(P_ZMAX) Then
            iro = 6: ro = dip.sup(0).rev + dip.sup(1).rev + dip.sup(2).rev
            nSup = 3
          ElseIf z1 < appl.parm(P_ZMAX) Then
            iro = 5: ro = dip.sup(0).rev + dip.sup(1).rev
            nSup = 2
          ElseIf z0 < appl.parm(P_ZMAX) Then
            iro = 4: ro = dip.sup(0).rev
            nSup = 1
          Else
            Exit For
          End If
          .pfam4(t).sg(iro) = .pfam4(t).sg(iro) + prob
          .pfam4(t).sg(0) = .pfam4(t).sg(0) + prob
          .pfam4(t).sg(7) = .pfam4(t).sg(7) + prob * nSup '20080915LC
          .pfam4(t).sg(8) = .pfam4(t).sg(8) + prob * ro
          z0 = z0 + 1
          z1 = z1 + 1
          z2 = z2 + 1
        End If
      Next t
    End If
  End With
End Sub


'20140425LC (tutta)
Public Sub TIscr2_MovPop_Superstiti4_1(ByRef dip As TIscr2, ByRef appl As TAppl, iop&, tau&, prob#, ZMAX&, pfam4() As TWFD)
  '******************
  'Muove i superstiti
  '******************
  Dim bSoloOrfani As Boolean
  Dim t&, z&, z0&, z1&, z2&
  Dim iSex0&, iSex1&, iSex2&
  Dim jQ0&, jQ1&, jQ2&
  Dim c0#, c1#, fact#, q#, y0#, y1#
  Dim p0#, p1#, p2#
  Dim p000#, p001#, p010#, p011#, p100#, p101#, p110#, p111#
  Dim av#, Avo#, Avoo#, Ao#, Aoo#, Aooo#
  Dim Bv#, Bvo#, Bvoo#, Bo#, Boo#, Booo#
  
  With appl
    c0 = dip.sup(0).c0
    c1 = 1# - c0
    If dip.zs <= AA0 Then
      Exit Sub
    End If
    '--- Sesso, et�
    iSex0 = dip.sup(0).s
    z0 = dip.sup(0).y
    If z0 > OPZ_OMEGA Then
      z0 = OPZ_OMEGA
    End If
    iSex1 = 1
    z1 = 0
    iSex1 = 2
    z2 = 0
    If dip.nSup >= 2 Then
      iSex1 = dip.sup(1).s
      z1 = dip.sup(1).y
      If dip.sup(1).tipSup = 1 Then
        If z1 > OPZ_OMEGA Then
          z1 = OPZ_OMEGA
        End If
      End If
      If dip.nSup >= 3 Then
        iSex2 = dip.sup(2).s
        z2 = dip.sup(2).y
        If dip.sup(2).tipSup = 1 Then
          If z2 > OPZ_OMEGA Then
            z2 = OPZ_OMEGA
          End If
        End If
        If z1 < z2 Then
          z = iSex1
          iSex1 = iSex2
          iSex2 = z
          z = z1
          z1 = z2
          z2 = z
        End If
      End If
    End If
    '---
    jQ0 = IIf(OPZ_INT_SUP_4 = 1, 0, 2 * (dip.Qualifica - 1)) + iSex0
    jQ1 = IIf(OPZ_INT_SUP_4 = 1, 0, 2 * (dip.Qualifica - 1)) + iSex1
    jQ2 = IIf(OPZ_INT_SUP_4 = 1, 0, 2 * (dip.Qualifica - 1)) + iSex2
    '---
    bSoloOrfani = True
    If dip.sup(0).tipSup = 1 Then
      bSoloOrfani = False
    ElseIf dip.nSup >= 2 Then
      If dip.sup(1).tipSup = 1 Then
        bSoloOrfani = False
      ElseIf dip.nSup >= 3 Then
        If dip.sup(2).tipSup = 1 Then
          bSoloOrfani = False
        End If
      End If
    End If
    '--- Reversibilit�
    If iop = 0 Then
      av = appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Av)
      Avo = appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Avo)
      Avoo = appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Avoo)
      Ao = appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Ao)
      Aoo = appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Aoo)
      Aooo = appl.CoeffVar(appl.t0 + 1 + tau, ecv.cv_Aooo)
    Else
      av = dip.sup(0).rev
      Avo = 0
      Avoo = 0
      If dip.nSup >= 2 Then
        Avo = dip.sup(1).rev
      End If
      If dip.nSup >= 3 Then
        Avoo = dip.sup(2).rev
      End If
      If av < Avo Then
        y0 = av
        av = Avo
        Avo = y0
      End If
      If av < Avoo Then
        y0 = av
        av = Avoo
        Avoo = y0
      End If
      If Avo < Avoo Then
        y0 = Avo
        Avo = Avoo
        Avoo = y0
      End If
      Avo = Avo + av
      Avoo = Avoo + Avo
      '---
      Ao = av
      Aoo = Avo
      Aooo = Avoo
    End If
    '---
    For t = 0 To appl.nMax2 + OPZ_ELAB_CODA
'If t = 21 Then
't = t
'End If
      If t = 0 Then
        p0 = IIf(dip.nSup >= 1, 1#, 0#)
        p1 = IIf(dip.nSup >= 2, 1#, 0#)
        p2 = IIf(dip.nSup >= 3, 1#, 0#)
      Else
        '--- Eliminazione del 1� superstite
        If dip.sup(0).tipSup = 1 Then
          '--- da mettere in routine (inizio)
          y0 = InterpL(appl.LL1, BT_QSD, iSex0, dip.Qualifica, z0 - 1)
          y1 = InterpL(appl.LL1, BT_QSD, iSex0, dip.Qualifica, z0 - 0)
          If OPZ_ABB_QM_SUP > 0 Then
            If OPZ_INT_ABBQ = 0 Then
              fact = TIscr2_AbbQ(appl.BDA, tau + t - 1, z0 - c0, jQ0 - 1)
              If y0 < AA9 Then
                y0 = y0 * fact
              End If
              If y1 < AA9 Then
                y1 = y1 * fact
              End If
            Else
              If y0 < AA9 Then
                y0 = y0 * TIscr2_AbbQ(appl.BDA, tau + t - 1, z0 - 1, jQ0 - 1)
              End If
              If y1 < AA9 Then
                y1 = y1 * TIscr2_AbbQ(appl.BDA, tau + t - 1, z0 - 0, jQ0 - 1)
              End If
            End If
          End If
          q = (y0 * c0 + y1 * c1)
          '--- da mettere in routine (fine)
          p0 = p0 * (1 - q)
        ElseIf z0 >= ZMAX Then
          p0 = 0#
        End If
        '--- Eliminazione del 2� superstite
        If dip.nSup >= 2 Then
          If dip.sup(1).tipSup = 1 Then
            '--- da mettere in routine (inizio)
            '--- 20160312LC (inizio)  iSex0->iSex1
            y0 = InterpL(appl.LL1, BT_QSD, iSex1, dip.Qualifica, z1 - 1)
            y1 = InterpL(appl.LL1, BT_QSD, iSex1, dip.Qualifica, z1 - 0)
            '--- 20160312LC (fine)
            If OPZ_ABB_QM_SUP > 0 Then
              If OPZ_INT_ABBQ = 0 Then
                fact = TIscr2_AbbQ(appl.BDA, tau + t - 1, z2 - c0, jQ1 - 1)
                If y0 < AA9 Then
                  y0 = y0 * fact
                End If
                If y1 < AA9 Then
                  y1 = y1 * fact
                End If
              Else
                If y0 < AA9 Then
                  y0 = y0 * TIscr2_AbbQ(appl.BDA, tau + t - 1, z1 - 1, jQ1 - 1)
                End If
                If y1 < AA9 Then
                  y1 = y1 * TIscr2_AbbQ(appl.BDA, tau + t - 1, z1 - 0, jQ1 - 1)
                End If
              End If
            End If
            q = (y0 * c0 + y1 * c1)
            '--- da mettere in routine (fine)
            p1 = p1 * (1 - q)
          ElseIf z1 >= ZMAX Then
            p1 = 0#
          End If
        End If
        '--- Eliminazione del 3� superstite
        If dip.nSup >= 3 Then
          If dip.sup(2).tipSup = 1 Then
            '--- da mettere in routine (inizio)
            '--- 20160312LC (inizio)  iSex0->iSex1
            y0 = InterpL(appl.LL1, BT_QSD, iSex2, dip.Qualifica, z2 - 1)
            y1 = InterpL(appl.LL1, BT_QSD, iSex2, dip.Qualifica, z2 - 0)
            '--- 20160312LC (fine)
            If OPZ_ABB_QM_SUP > 0 Then
              If OPZ_INT_ABBQ = 0 Then
                fact = TIscr2_AbbQ(appl.BDA, tau + t - 1, z2 - c0, jQ2 - 1)
                If y0 < AA9 Then
                  y0 = y0 * fact
                End If
                If y1 < AA9 Then
                  y1 = y1 * fact
                End If
              Else
                If y0 < AA9 Then
                  y0 = y0 * TIscr2_AbbQ(appl.BDA, tau + t - 1, z2 - 1, jQ2 - 1)
                End If
                If y1 < AA9 Then
                  y1 = y1 * TIscr2_AbbQ(appl.BDA, tau + t - 1, z2 - 0, jQ2 - 1)
                End If
              End If
            End If
            q = (y0 * c0 + y1 * c1)
            '--- da mettere in routine (fine)
            p2 = p2 * (1 - q)
          ElseIf z2 >= ZMAX Then
            p2 = 0#
          End If
        End If
      End If
      '--- Fine se non ci sono superstiti
      If p0 <= AA0 And p1 <= AA0 And p2 <= AA0 Then
        Exit For
      Else
        p111 = p0 * p1 * p2
        p110 = p0 * p1 * (1 - p2)
        p101 = p0 * (1 - p1) * p2
        p100 = p0 * (1 - p1) * (1 - p2)
        p011 = (1 - p0) * p1 * p2
        p010 = (1 - p0) * p1 * (1 - p2)
        p001 = (1 - p0) * (1 - p1) * p2
        p000 = (1 - p0) * (1 - p1) * (1 - p2)
If Math.Abs(p000 + p001 + p010 + p011 + p100 + p101 + p110 + p111 - 1) > AA0 Then
Call MsgBox("TODO")
Stop
End If
        If bSoloOrfani = True Then
          Bo = prob * (p001 + p010 + p100)
          Boo = prob * (p011 + p101 + p110)
          Booo = prob * p111
          pfam4(t).sg(4) = pfam4(t).sg(4) + Bo
          pfam4(t).sg(5) = pfam4(t).sg(5) + Boo
          pfam4(t).sg(6) = pfam4(t).sg(6) + Booo
          pfam4(t).sg(7) = pfam4(t).sg(7) + 1 * Bo + 2 * Boo + 3 * Booo
          pfam4(t).sg(8) = pfam4(t).sg(8) + Ao * Bo + Aoo * Boo + Aooo * Booo
        Else
          Bv = prob * p100
          Bvo = prob * (p110 + p101)
          Bvoo = prob * p111
          Bo = prob * (p001 + p010)
          Boo = pfam4(t).sg(5) + prob * p011
          pfam4(t).sg(1) = pfam4(t).sg(1) + Bv
          pfam4(t).sg(2) = pfam4(t).sg(2) + Bvo
          pfam4(t).sg(3) = pfam4(t).sg(3) + Bvoo
          pfam4(t).sg(4) = pfam4(t).sg(4) + Bo
          pfam4(t).sg(5) = pfam4(t).sg(5) + Boo
          pfam4(t).sg(7) = pfam4(t).sg(7) + 1 * (Bv + Bo) + 2 * (Bvo + Boo) + 3 * Bvoo
          pfam4(t).sg(8) = pfam4(t).sg(8) + av * Bv + Avo * Bvo + Avoo * Bvoo + Ao * Bo + Aoo * Boo
        End If
        pfam4(t).sg(0) = pfam4(t).sg(0) + prob * (1 - p000)
      End If
      z0 = z0 + 1
      z1 = z1 + 1
      z2 = z2 + 1
    Next t
  End With
End Sub


'20140425LC (tutta)
Public Sub TIscr2_MovPop_Superstiti4_2(ByRef dip As TIscr2, ByRef appl As TAppl, iop&, tau&, prob#, pfam4() As TWFD)
  'Modifica per gli orfani superstiti se OPZ_PENS_C0<>0,5
  'Se OPZ_PENS=1,0 e Z=ZMAX-1, l'orfano esce a met� anno e prende P/2, per prendere l'altro P/2 occorre fare la media con ZMAX+1
  'in pratica esce con ZMAX-0,5 anni, facendo la media col caso ZMAX+1 esce in media a ZMAX
  Dim j&, nMax3&, t&, ZMAX&
  Dim c0#, c1#
  Dim pfam0() As TWFD
  Dim pfam1() As TWFD

  ZMAX& = appl.parm(P_ZMAX)
  If 1 = 2 Or Math.Abs(OPZ_PENS_C0 - 0.5) <= AA0 Or (OPZ_PENS_C0 < 0.5 - AA0 And ZMAX < 1) Then
    Call TIscr2_MovPop_Superstiti4_1(dip, appl, iop, tau, prob, ZMAX, pfam4())
  Else
    nMax3 = UBound(pfam4, 1)
    ReDim pfam0(0 To nMax3)
    ReDim pfam1(0 To nMax3)
    If OPZ_PENS_C0 > 0.5 Then
      c1 = OPZ_PENS_C0 - 0.5  '0,5 se OPZ_PENS_C0=1,0  1,0 se OPZ_PENS_C0=0.5+eps
      c0 = 1 - c1             '0,5 se OPZ_PENS_C0=1,0  0,0 se OPZ_PENS_C0=0.5+eps
      Call TIscr2_MovPop_Superstiti4_1(dip, appl, iop, tau, prob, ZMAX, pfam0())
      Call TIscr2_MovPop_Superstiti4_1(dip, appl, iop, tau, prob, ZMAX + 1, pfam1())
    Else  'If OPZ_PENS_C0 < 0.5 Then
      c0 = 0.5 - OPZ_PENS_C0  '0,5 se OPZ_PENS_C0=0,0  0,0 se OPZ_PENS_C0=0.5-eps
      c1 = 1 - c0             '0,5 se OPZ_PENS_C0=0,0  1,0 se OPZ_PENS_C0=0.5-eps
      Call TIscr2_MovPop_Superstiti4_1(dip, appl, iop, tau, prob, ZMAX - 1, pfam0())
      Call TIscr2_MovPop_Superstiti4_1(dip, appl, iop, tau, prob, ZMAX, pfam1())
    End If
    For t = 0 To nMax3
      For j = 0 To 8
        pfam4(t).sg(j) = pfam4(t).sg(j) + pfam0(t).sg(j) * c0 + pfam1(t).sg(j) * c1
      Next j
    Next t
  End If
End Sub


'20140609LC
Public Function ReddSudd(reddT As Double, reddLimT As Double) As Byte
'****************************************************************************
'SCOPO
'  Attribuire la classe di reddito
'PARAMETRI DI INPUT
'  reddT:    reddito all'annoT
'  reddLimT: reddito limite all'annoT
'VALORE RESITUITO
'  Classe di reddito (0 se < 0#; 1: se 0#; 2+:scaglione di ampiezza REDD_STEP
'VERSIONE
'  20140609LC - prima implementazione
'****************************************************************************
  Const REDD_MAX As Double = 120000#
  Const REDD_STEP As Double = 15000#
  Dim redd0 As Double
  Dim y As Integer
  
  If reddLimT <= AA0 Then
    y = 1
  Else
    redd0 = Int(Int(reddT) / reddLimT * REDD_MAX)
    If redd0 < 0# Then
      y = 0
    ElseIf redd0 < REDD_MAX Then
      y = 1 + Int(redd0 / REDD_STEP)
    Else
      y = 1 + Int(REDD_MAX / REDD_STEP)
    End If
  End If
  ReddSudd = y
End Function


Private Sub TIscr2Aux_Init(ByRef tia As TIscr2Aux, ByRef AMIN%, ByRef AMAX%, ByRef maxOm As Byte, ByRef maxSup As Byte)
'20150525LC
  Dim ia&, ib&
  
  With tia
    .iErr = 0
    .mat = 0
    .gr = 0
    .sex = 0
    .tit = 0
    .DNasc = DATA_ZERO
    .DIscr = DATA_ZERO
    .dCess = DATA_ZERO
    .c1 = 0#
    .c2 = 0#
    .h = 0#
    .ha = 0#
    .hb = 0#
    .n = 0#
    .nMan = 0#
    '---
    .smNI = 0#
    ReDim .sm(AMAX - AMIN)
    For ia = 0 To AMAX - AMIN
      .sm(ia) = 0#
    Next ia
    '--- 20150612LC (inizio)
    'ReDim .om(maxOm)
    'For ib = 0 To maxOm
    '  .om(ib) = 0#
    'Next ib
    .om0 = 0#
    .qm0 = 0#
    '--- 20150612LC (fine)
    .cmNI = 0#
    ReDim .cm(AMAX - AMIN)
    For ia = 0 To AMAX - AMIN
      .cm(ia) = 0#
    Next ia
    '---
    .ivaNI = 0#
    ReDim .iva(5)
    For ia = 0 To 5
      .iva(ia) = 0#
    Next ia
    '---
    .inteNI = 0#
    ReDim .inte(5)
    For ia = 0 To 5
      .inte(ia) = 0#
    Next ia
    .PensTipo = 0
    .PensAnno = 0
    .PensAUS = 0
    .Pensione = 0#
    .nSup = 0
    ReDim .sup(maxSup)
    For ib = 0 To maxSup
      With .sup(ib)
        .mat = 0
        .DNasc = DATA_ZERO
        .s = 0
        .tipGen = 0
        .rev = 0#
        .bDNascSupNull = False
      End With
    Next ib
  End With
End Sub


Private Function TIscr2Aux_ReadMdb(ByRef dia As TIscr2Aux, ByRef appl As TAppl, ByRef ixdb&(), ByRef gr1Mat As Variant, ByRef iRec&) As String
'20150525LC  creazione
  Dim anno%
  Dim i&, ia&, ib&
  Dim y#
  Dim s$, sErr$
  'Dim tia As TIscr2Aux

  sErr = ""
  Call TIscr2Aux_Init(dia, 1961, appl.t0, eom.om_last, 2)
  '---
  With dia
    '--- iErr As Long    'iErr
    '--- mat As Long     'IdDip
    .mat = gr1Mat(ixdb(RS_mat), iRec)                'rsMat("mat").Value
    '--- gr As Byte      'IdAz
    .gr = ff0(gr1Mat(ixdb(RS_anGruppo), iRec))       'rsMat("anGruppo").Value
    '--- sex As Byte     'Sesso
    s = Left(UCase("" & Trim(gr1Mat(ixdb(RS_anSex), iRec))), 1)  '20150413LC-2
    If s = "M" Or s = "1" Then
      .sex = 1
    ElseIf s = "F" Or s = "2" Then
      .sex = 2
    Else
      .sex = 0
    End If
    '--- tit As Byte     'Categ
    s = Left(UCase("" & Trim(gr1Mat(ixdb(RS_anTit), iRec))), 1)
    If s = "A" Or s = "1" Then
      .tit = 1
    ElseIf s = "I" Or s = "2" Then
      .tit = 2
    Else
      .tit = 0
    End If
    '--- DNasc As Date   'DNasc
    .DNasc = DataNasc(ff0(gr1Mat(ixdb(RS_anDNasc), iRec)), OPZ_INT_DNASC)  '20130211LC  'ff0(rsMat!anDNasc)
    '--- DIscr As Date   'DIscrFondo
    .DIscr = ff0(gr1Mat(ixdb(RS_anIscInD), iRec)) 'ff0(rsMat!anIscInD)
    '--- dCess As Date   'DCessaz
    .dCess = ff0(gr1Mat(ixdb(RS_anCanFinD), iRec)) 'ff0(rsMat!anCanFinD)
    '--- c1 As Byte      'c1
    .c1 = ff0(gr1Mat(ixdb(RS_anTC), iRec)) 'ff0(rsMat("anTC").Value)
    '--- c2 As Byte      'c2
    '--- h As Double     'hmf
    .h = ff0(gr1Mat(ixdb(RS_anAnzTot), iRec))
      .h = Int(.h / 1000) + (.h Mod 1000) / 360#
    '--- ha As Double    'hmfa
    .ha = ff0(gr1Mat(ixdb(RS_anAnzA), iRec))
      .ha = Int(.ha / 1000) + (.ha Mod 1000) / 360#
    '--- hb As Double    'hmfb
    .hb = ff0(gr1Mat(ixdb(RS_anAnzB), iRec))
      .hb = Int(.hb / 1000) + (.hb Mod 1000) / 360#
    '--- n As Double     'n
    .n = ff0(gr1Mat(ixdb(RS_anNIngr), iRec))         'ff0(rsMat!anNIngr)
    '--- nMan As Double  'Mandati
    .nMan = ff0(gr1Mat(ixdb(RS_anMandati), iRec))
    '--- 20150612LC (inizio)  Math.Abs
    '--- smNI As Double  'Mandati
    .smNI = Math.Abs(ff0(gr1Mat(ixdb(RS_IrpefNI), iRec)))  'ff0(rsMat!irpefNI)
    '--- sm() As Double
    .annoUC = 0
    For anno = appl.t0 To 1961 Step -1
      ia = appl.t0 - anno
      y = ff0(gr1Mat(ixdb(RS_IRPEF + (anno - 1961) * 4), iRec))  'ffm1(rsMat("irpef" & .t).Value)
      .sm(ia) = Math.Abs(y)
      If y > 0# And .annoUC = 0 Then
        .annoUC = anno
      End If
    Next anno
    '--- om() As Double
    If OPZ_INT_MONT_CALC > 0 Then
      '--- 20150612LC (inizio)
      'If .gr >= 2 And .gr <= 4 Then
      '  i = om_pens_att
      'Else
      '  i = eom.om_misto_obbl
      'End If
      .om0 = ff0(gr1Mat(ixdb(RS_IvaNI), iRec))
      .qm0 = ff0(gr1Mat(ixdb(RS_IntNI), iRec))
      '--- 20150612LC (fine)
    End If
    '--- cmNI As Double  'Mandati
    If OPZ_INT_MONT_CALC > 0 Then
      .cmNI = Math.Abs(ff0(gr1Mat(ixdb(RS_SoggNI), iRec)))
    End If
    '--- cm() As Double
    For anno = appl.t0 To 1961 Step -1
      ia = appl.t0 - anno
      .cm(ia) = Math.Abs(ff0(gr1Mat(ixdb(RS_SOGG + (anno - 1961) * 4), iRec)))  'ffm1(rsMat("sogg" & .t).Value)
    Next anno
    '--- ivaNI As Double  'Mandati
    '--- iva() As Double
    '--- inteNI As Double  'Mandati
    '--- inte() As Double
    '--- 20150612LC (fine(
    '--- PensTipo As Byte
    '--- 20150604LC (inizio)
    '101 VECCHIAIA ORDINARIA
    '102 VECCHIAIA ANTICIPATA
    '103 TRASF.TA DA INVAL./INAB. A VECCHIAIA ORDINARIA
    '104 TRASF.TA DA INVAL./INAB. A VECCHIAIA ANTICIPATA
    '*105 TRASFORMATA DA INVALIDITA' AD INABILITA'
    '*106 TRASFORMATA DA INABILITA'� A INVALIDITA'
    '*107 INABILITA '
    '*108 INVALIDITA ' PARZIALE
    '*109 SUPERSTITI INDIRETTA
    '*110 SUPERSTITI REVERSIBILE DA VECCHIAIA
    '*111 SUPERSTITI REVERSIBILE DA INVALIDITA'/INABILITA'
'Private Const CAT_PENS_VEC = 1 '1 Pensione di vecchiaia
''--- 20140404LC (inizio)
'Private Const CAT_PENS_SOS = 19  '-1 '1 Pensione sostitutiva
'Private Const CAT_PENS_TOT = 20  '-2 '1 Pensione di totalizzazione
''--- 20140404LC (fine)
'Private Const CAT_PENS_ANZ = 5 '2 Pensione di anzianit�
'Private Const CAT_PENS_REV = 2 '3 Pensione di reversibilit�
'Private Const CAT_PENS_SUP = 3 '4 Pensione ai superstiti
'Private Const CAT_PENS_INA = 6 '5 Pensione di inabilit�
'Private Const CAT_PENS_INV = 4 '6 Pensione di invalidit�
'Private Const CAT_PENS_ALT = 7 '7 Pensione di altro tipo
    .PensTipo = ff0(gr1Mat(ixdb(RS_anCatPens), iRec)) 'ff0(rsMat!anCatPens)
    If .PensTipo = 105 Or .PensTipo = 107 Then
      .PensTipo = CAT_PENS_INA
    ElseIf .PensTipo = 106 Or .PensTipo = 108 Then
      .PensTipo = CAT_PENS_INV
    ElseIf .PensTipo = 109 Then
      .PensTipo = CAT_PENS_SUP
    ElseIf .PensTipo >= 110 Then
      .PensTipo = CAT_PENS_REV
    ElseIf .PensTipo = 102 Or .PensTipo = 104 Then
      .PensTipo = CAT_PENS_VEC
    Else
      .PensTipo = CAT_PENS_VEC
    End If
    '--- 20150604LC (fine)
    '--- PensAnno  As Integer
    .PensAnno = ff0(gr1Mat(ixdb(RS_anPenADcr), iRec))
    '--- PensAUS As Integer
    .PensAUS = ff0(gr1Mat(ixdb(RS_anPensAUS), iRec)) 'ff0(rsMat!anPensAUS)
    '--- Pensione As Double
    .Pensione = ff0(gr1Mat(ixdb(RS_anPens), iRec)) 'ff0(rsMat!anPens)
    '--- nSup  As Byte
    .nSup = ff0(gr1Mat(ixdb(RS_anSupN), iRec)) 'rsMat!anSupN
    '--- sup() As TIscrSupAux
    For ib = 0 To .nSup - 1
      With .sup(ib)
        .mat = -(ib + 1)
        .DNasc = DataNasc(ff0(gr1Mat(ixdb(RS_anSDNasc + ib * 5), iRec)), OPZ_INT_DNASC)  '20130211LC  'rsMat("anS" & ib + 1 & "dnasc")
        .s = gr1Mat(ixdb(RS_anSSex + ib * 5), iRec)       'rsMat("anS" & ib + 1 & "sex")
        If .s <> 2 Then
          .s = 1
        End If
        .tipGen = ff0(gr1Mat(ixdb(RS_anSCcod + ib * 5), iRec))  '20150413LC-2  'rsMat("anS" & ib + 1 & "cod")
        '--- 20160312LC (inizio)
        If .tipGen = 64 Then
          .tipGen = 4
        Else
          .tipGen = 5
        End If
        '--- 20160312LC (fine)
        .rev = gr1Mat(ixdb(RS_anSRev + ib * 5), iRec)     'rsMat("anS" & ib + 1 & "rev")
        .bDNascSupNull = IIf(VarType(gr1Mat(ixdb(RS_anSDNasc + ib * 5), iRec)) = vbNull, 1, 0)
      End With
    Next ib
  End With
  TIscr2Aux_ReadMdb = sErr
End Function

