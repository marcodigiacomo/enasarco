VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmElab 
   Caption         =   "Form1"
   ClientHeight    =   6120
   ClientLeft      =   1500
   ClientTop       =   1875
   ClientWidth     =   10905
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6120
   ScaleWidth      =   10905
   Begin VB.Frame Frame1 
      Caption         =   "2 Tassi ed altre stime"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5595
      Index           =   2
      Left            =   60
      TabIndex        =   38
      Top             =   360
      Width           =   10755
      Begin VB.Frame Frame1 
         Caption         =   "2.3 Tassi di rivalutazione di redditi e volumi d'affari"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1575
         Index           =   23
         Left            =   5160
         TabIndex        =   64
         Top             =   420
         Width           =   5055
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   36
            Left            =   3360
            TabIndex        =   74
            Top             =   1140
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   37
            Left            =   4200
            TabIndex        =   75
            Top             =   1140
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   34
            Left            =   3360
            TabIndex        =   71
            Top             =   780
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   32
            Left            =   3360
            TabIndex        =   68
            ToolTipText     =   "30"
            Top             =   420
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   35
            Left            =   4200
            TabIndex        =   72
            Top             =   780
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   33
            Left            =   4200
            TabIndex        =   69
            ToolTipText     =   "31"
            Top             =   420
            Width           =   735
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Nuovi iscritti"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   36
            Left            =   2250
            TabIndex        =   73
            Top             =   1140
            Width           =   1065
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Attivi"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   32
            Left            =   2880
            TabIndex        =   67
            Top             =   420
            Width           =   420
         End
         Begin VB.Label Label1 
            Alignment       =   2  'Center
            Caption         =   "vol.affari"
            Height          =   255
            Index           =   33
            Left            =   4140
            TabIndex        =   66
            Top             =   240
            Width           =   795
         End
         Begin VB.Label Label1 
            Alignment       =   2  'Center
            Caption         =   "redditi"
            Height          =   255
            Index           =   35
            Left            =   3420
            TabIndex        =   65
            Top             =   240
            Width           =   615
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Pensionati attivi"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   34
            Left            =   1905
            TabIndex        =   70
            Top             =   780
            Width           =   1395
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "2.5 Probabilit� di prosecuzione attivit�"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1815
         Index           =   25
         Left            =   5160
         TabIndex        =   96
         Top             =   3660
         Width           =   5055
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   49
            Left            =   4200
            TabIndex        =   107
            Top             =   1020
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   48
            Left            =   3420
            TabIndex        =   106
            ToolTipText     =   "Percentuale di pensionati che proseguono nell'attivit�"
            Top             =   1020
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   47
            Left            =   4200
            TabIndex        =   104
            Top             =   660
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   46
            Left            =   3420
            TabIndex        =   103
            ToolTipText     =   "Percentuale di pensionati che proseguono nell'attivit�"
            Top             =   660
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   50
            Left            =   3420
            TabIndex        =   109
            Top             =   1380
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   45
            Left            =   4200
            TabIndex        =   101
            Top             =   300
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   44
            Left            =   3420
            TabIndex        =   100
            ToolTipText     =   "Percentuale di pensionati che proseguono nell'attivit�"
            Top             =   300
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   51
            Left            =   4200
            TabIndex        =   110
            Top             =   1380
            Width           =   735
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Fraz. pensionati Contributivi"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   48
            Left            =   900
            TabIndex        =   105
            Top             =   1020
            Width           =   2430
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Fraz, pensionati di Anzianit�"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   46
            Left            =   855
            TabIndex        =   102
            Top             =   660
            Width           =   2475
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Fraz. pensionati di Totalizzazione"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   50
            Left            =   405
            TabIndex        =   108
            Top             =   1380
            Width           =   2955
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Fraz. pensionati di Vecchiaia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   44
            Left            =   765
            TabIndex        =   99
            Top             =   300
            Width           =   2565
         End
         Begin VB.Label Label1 
            Alignment       =   2  'Center
            Caption         =   "maschi"
            Height          =   255
            Index           =   47
            Left            =   3480
            TabIndex        =   97
            Top             =   120
            Width           =   615
         End
         Begin VB.Label Label1 
            Alignment       =   2  'Center
            Caption         =   "femmine"
            Height          =   255
            Index           =   45
            Left            =   4140
            TabIndex        =   98
            Top             =   120
            Width           =   795
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "2.4 Rivalutazione delle pensioni"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1515
         Index           =   24
         Left            =   5160
         TabIndex        =   76
         Top             =   2040
         Width           =   5055
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   139
            Left            =   4215
            TabIndex        =   92
            ToolTipText     =   "32"
            Top             =   360
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   138
            Left            =   3135
            TabIndex        =   91
            Text            =   "999.999,99"
            Top             =   360
            Width           =   1095
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   140
            Left            =   3135
            TabIndex        =   94
            Top             =   720
            Width           =   1095
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   141
            Left            =   4215
            TabIndex        =   95
            Top             =   720
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   43
            Left            =   1560
            TabIndex        =   87
            Top             =   1080
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   41
            Left            =   1560
            TabIndex        =   84
            Top             =   720
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   42
            Left            =   480
            TabIndex        =   86
            Top             =   1080
            Width           =   1095
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   40
            Left            =   480
            TabIndex        =   83
            Top             =   720
            Width           =   1095
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   38
            Left            =   480
            TabIndex        =   80
            Text            =   "999.999,99"
            Top             =   360
            Width           =   1095
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   39
            Left            =   1560
            TabIndex        =   81
            ToolTipText     =   "32"
            Top             =   360
            Width           =   735
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "sc.4"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   138
            Left            =   2745
            TabIndex        =   90
            Top             =   360
            Width           =   360
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "sc.5"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   140
            Left            =   2745
            TabIndex        =   93
            Top             =   720
            Width           =   360
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Massimale"
            Height          =   195
            Index           =   139
            Left            =   3315
            TabIndex        =   88
            Top             =   180
            Width           =   735
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Tasso"
            Height          =   195
            Index           =   141
            Left            =   4335
            TabIndex        =   89
            Top             =   180
            Width           =   435
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Tasso"
            Height          =   195
            Index           =   41
            Left            =   1680
            TabIndex        =   78
            Top             =   180
            Width           =   435
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Massimale"
            Height          =   195
            Index           =   39
            Left            =   660
            TabIndex        =   77
            Top             =   180
            Width           =   735
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "sc.3"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   42
            Left            =   105
            TabIndex        =   85
            Top             =   1080
            Width           =   360
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "sc.2"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   40
            Left            =   105
            TabIndex        =   82
            Top             =   720
            Width           =   360
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "sc.1"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   38
            Left            =   105
            TabIndex        =   79
            Top             =   360
            Width           =   360
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "2.1 Tassi di valutazione e rivalutazione"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1875
         Index           =   21
         Left            =   420
         TabIndex        =   40
         Top             =   420
         Width           =   4575
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   24
            Left            =   3720
            TabIndex        =   48
            ToolTipText     =   "35"
            Top             =   1440
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   23
            Left            =   3720
            TabIndex        =   46
            ToolTipText     =   "35"
            Top             =   1080
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   22
            Left            =   3720
            TabIndex        =   44
            ToolTipText     =   "35"
            Top             =   720
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   21
            Left            =   3720
            TabIndex        =   42
            ToolTipText     =   "Tasso di valutazione"
            Top             =   360
            Width           =   735
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Reddito iniziale dei nuovi iscritti"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   24
            Left            =   885
            TabIndex        =   47
            Top             =   1440
            Width           =   2775
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Inflazione"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   23
            Left            =   2820
            TabIndex        =   45
            Top             =   1080
            Width           =   840
            WordWrap        =   -1  'True
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "*Valutazione"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   21
            Left            =   2580
            TabIndex        =   41
            Top             =   360
            Width           =   1125
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "*Rendimento finanziario"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   22
            Left            =   1560
            TabIndex        =   43
            ToolTipText     =   "Tasso di rendimento della gestione finanziaria "
            Top             =   720
            Width           =   2115
         End
      End
      Begin VB.CheckBox Check1 
         Alignment       =   1  'Right Justify
         Caption         =   "Lettura parametri da database (esclusi dati contrassegnati con *)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   20
         Left            =   4560
         TabIndex        =   39
         Top             =   120
         Value           =   2  'Grayed
         Width           =   6075
      End
      Begin VB.Frame Frame1 
         Caption         =   "2.2 Tassi di rivalutazione dei montanti contributivi"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3015
         Index           =   22
         Left            =   420
         TabIndex        =   49
         Top             =   2400
         Width           =   4575
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   26
            Left            =   3720
            TabIndex        =   53
            Top             =   780
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   27
            Left            =   3720
            TabIndex        =   55
            Top             =   1140
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   28
            Left            =   3720
            TabIndex        =   57
            Top             =   1500
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   30
            Left            =   3720
            TabIndex        =   61
            Top             =   2220
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   25
            Left            =   3720
            TabIndex        =   51
            Top             =   420
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   29
            Left            =   3720
            TabIndex        =   59
            Top             =   1860
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   31
            Left            =   3720
            TabIndex        =   63
            Top             =   2580
            Width           =   735
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Metodo misto - contr integr. retrocessi"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   31
            Left            =   330
            TabIndex        =   62
            Top             =   2580
            Width           =   3315
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Metodo misto - contributi facoltativi"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   30
            Left            =   615
            TabIndex        =   60
            Top             =   2220
            Width           =   3045
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Metodo misto - contributi obbligatori"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   29
            Left            =   480
            TabIndex        =   58
            Top             =   1860
            Width           =   3165
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Contributi per supplementi di pensione"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   28
            Left            =   270
            TabIndex        =   56
            Top             =   1500
            Width           =   3405
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Contributi soggetti ad art. 25"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   27
            Left            =   1215
            TabIndex        =   54
            Top             =   1140
            Width           =   2445
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Contributi dal 2002"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   26
            Left            =   2040
            TabIndex        =   52
            Top             =   780
            Width           =   1620
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Contributi fino al 2001"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   25
            Left            =   1800
            TabIndex        =   50
            Top             =   420
            Width           =   1860
         End
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "1 Impostazione parametri di valutazione"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5595
      Index           =   1
      Left            =   60
      TabIndex        =   1
      Top             =   360
      Width           =   10775
      Begin VB.TextBox txtIter 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   4980
         MaxLength       =   2
         TabIndex        =   364
         Text            =   "99"
         ToolTipText     =   "Bilancio Tecnico - Anni di valutazione"
         Top             =   660
         Width           =   315
      End
      Begin VB.CheckBox Check1 
         Alignment       =   1  'Right Justify
         Caption         =   "Lettura parametri da database"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   7620
         TabIndex        =   3
         Top             =   600
         Value           =   2  'Grayed
         Visible         =   0   'False
         Width           =   3015
      End
      Begin VB.Frame Frame1 
         Caption         =   "1.1 Bilancio tecnico"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4335
         Index           =   11
         Left            =   120
         TabIndex        =   7
         Top             =   1080
         Width           =   5175
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   7
            Left            =   3300
            TabIndex        =   18
            ToolTipText     =   "Bilancio Tecnico - Uscite per prestazioni assistenziali"
            Top             =   2280
            Width           =   1695
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   360
            Index           =   4
            Left            =   3780
            Locked          =   -1  'True
            MaxLength       =   3
            TabIndex        =   12
            Text            =   "999"
            ToolTipText     =   "Bilancio Tecnico - Anni di valutazione"
            Top             =   660
            Width           =   435
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   6
            Left            =   3300
            TabIndex        =   16
            ToolTipText     =   "Bilancio Tecnico - Spese di amministrazione"
            Top             =   1800
            Width           =   1695
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   11
            Left            =   4260
            TabIndex        =   26
            ToolTipText     =   "Bilancio Tecnico - Incremento annuo contributi da persone fisiche"
            Top             =   3780
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   10
            Left            =   3000
            TabIndex        =   25
            ToolTipText     =   "Bilancio Tecnico - Contributi iniziali da persone fisiche"
            Top             =   3780
            Width           =   1155
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   9
            Left            =   4260
            TabIndex        =   23
            ToolTipText     =   "Bilancio Tecnico - Incremento annuo contributi da Societ� di ingegneria"
            Top             =   3300
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   8
            Left            =   3000
            TabIndex        =   22
            ToolTipText     =   "Bilancio Tecnico - Contributi iniziali da Societ� di ingegneria"
            Top             =   3300
            Width           =   1215
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   5
            Left            =   3300
            TabIndex        =   14
            ToolTipText     =   "Bilancio Tecnico - Patrimonio iniziale"
            Top             =   1320
            Width           =   1695
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   3
            Left            =   3300
            MaxLength       =   3
            TabIndex        =   11
            Text            =   "999"
            ToolTipText     =   "Bilancio Tecnico - Anni di valutazione"
            Top             =   660
            Width           =   435
         End
         Begin VB.TextBox Text1 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   2
            Left            =   180
            TabIndex        =   9
            Text            =   "31/12/9999"
            ToolTipText     =   "Bilancio Tecnico - Anno di bilancio"
            Top             =   660
            Width           =   1095
         End
         Begin VB.Label Label1 
            Alignment       =   2  'Center
            AutoSize        =   -1  'True
            Caption         =   "Tasso di crescita"
            ForeColor       =   &H80000008&
            Height          =   390
            Index           =   1499
            Left            =   4320
            TabIndex        =   20
            Top             =   2880
            Width           =   675
            WordWrap        =   -1  'True
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Importo iniziale"
            ForeColor       =   &H80000008&
            Height          =   195
            Index           =   11
            Left            =   3060
            TabIndex        =   19
            Top             =   3060
            Width           =   1035
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Contrib. per prestaz. assist."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Index           =   7
            Left            =   -60
            TabIndex        =   17
            Top             =   2280
            Width           =   3345
            WordWrap        =   -1  'True
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Contrib. per spese amm."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Index           =   6
            Left            =   1035
            TabIndex        =   15
            Top             =   1800
            Width           =   2175
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Passivi persone fisiche"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Index           =   10
            Left            =   840
            TabIndex        =   24
            Top             =   3780
            Width           =   2070
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Passivi societ� d'ingegneria"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Index           =   8
            Left            =   360
            TabIndex        =   21
            Top             =   3300
            Width           =   2535
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Patrimonio iniziale"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Index           =   5
            Left            =   1560
            TabIndex        =   13
            Top             =   1320
            Width           =   1620
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Anni di valutazione"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Index           =   3
            Left            =   3300
            TabIndex        =   10
            Top             =   420
            Width           =   1665
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Data di calcolo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Index           =   2
            Left            =   180
            TabIndex        =   8
            Top             =   420
            Width           =   1350
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "1.2 Tipo di calcolo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1455
         Index           =   12
         Left            =   5400
         TabIndex        =   27
         Top             =   1080
         Width           =   5235
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   12
            Left            =   3360
            MaxLength       =   4
            TabIndex        =   31
            Text            =   "9999"
            Top             =   1020
            Width           =   555
         End
         Begin VB.OptionButton optTipoElab 
            Caption         =   "Metodo retributivo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   180
            TabIndex        =   28
            Top             =   300
            Value           =   -1  'True
            Width           =   1995
         End
         Begin VB.OptionButton optTipoElab 
            Caption         =   "Metodo contributivo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   180
            TabIndex        =   29
            Top             =   660
            Width           =   2115
         End
         Begin VB.OptionButton optTipoElab 
            Caption         =   "Metodo misto a partire dall'anno"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   180
            TabIndex        =   30
            Top             =   1020
            Width           =   3255
         End
      End
      Begin ComctlLib.ProgressBar ProgressBar1 
         Height          =   255
         Left            =   180
         TabIndex        =   2
         Top             =   300
         Visible         =   0   'False
         Width           =   10455
         _ExtentX        =   18441
         _ExtentY        =   450
         _Version        =   327682
         Appearance      =   1
      End
      Begin VB.Frame Frame1 
         Caption         =   "1.3 Modalit� di analisi"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2835
         Index           =   13
         Left            =   5400
         TabIndex        =   32
         Top             =   2580
         Width           =   5235
         Begin VB.TextBox txtCvNome 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   1320
            TabIndex        =   362
            ToolTipText     =   "Bilancio Tecnico - Anni di valutazione"
            Top             =   300
            Width           =   3795
         End
         Begin VB.CommandButton Command2 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   5
            Left            =   4680
            Picture         =   "AfpElab.frx":0000
            Style           =   1  'Graphical
            TabIndex        =   37
            Top             =   2280
            Width           =   465
         End
         Begin VB.TextBox Text1 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   14
            Left            =   1320
            TabIndex        =   36
            Top             =   2280
            Width           =   3300
         End
         Begin VB.TextBox Text1 
            Height          =   1455
            Index           =   13
            Left            =   1320
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   34
            Top             =   720
            Width           =   3795
         End
         Begin VB.Label lblCvNome 
            AutoSize        =   -1  'True
            Caption         =   "Tab.coeff.var."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   60
            TabIndex        =   361
            Top             =   300
            Width           =   1365
            WordWrap        =   -1  'True
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "File di output"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Index           =   14
            Left            =   120
            TabIndex        =   35
            Top             =   2280
            Width           =   1200
            WordWrap        =   -1  'True
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Query di estrazione dati"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   720
            Index           =   13
            Left            =   300
            TabIndex        =   33
            Top             =   840
            Width           =   975
            WordWrap        =   -1  'True
         End
      End
      Begin VB.ComboBox Combo1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   1
         Left            =   1860
         Style           =   2  'Dropdown List
         TabIndex        =   5
         ToolTipText     =   "Identificativo dei parametri di analisi"
         Top             =   660
         Width           =   1815
      End
      Begin VB.TextBox Text1 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   1
         Left            =   1860
         TabIndex        =   6
         ToolTipText     =   "N� minimo anni di anzianit� per pensione di vecchiaia con et� minima (Pvec30)"
         Top             =   660
         Width           =   1815
      End
      Begin VB.Label lblIter 
         AutoSize        =   -1  'True
         Caption         =   "Iterazione"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   4080
         TabIndex        =   363
         Top             =   660
         Width           =   975
         WordWrap        =   -1  'True
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Gruppo parametri"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Index           =   1
         Left            =   180
         TabIndex        =   4
         Top             =   660
         Width           =   1575
         WordWrap        =   -1  'True
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "5 Contribuzione"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5595
      Index           =   5
      Left            =   60
      TabIndex        =   217
      Top             =   360
      Width           =   10775
      Begin VB.Frame Frame1 
         Caption         =   "5.4 Assistenziale"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1095
         Index           =   54
         Left            =   5520
         TabIndex        =   272
         Top             =   3540
         Width           =   4335
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   182
            Left            =   2220
            TabIndex        =   279
            Top             =   660
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   180
            Left            =   2220
            TabIndex        =   276
            Top             =   300
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   183
            Left            =   3240
            TabIndex        =   280
            Top             =   660
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   181
            Left            =   3240
            TabIndex        =   277
            Top             =   300
            Width           =   915
         End
         Begin VB.Label Label1 
            Alignment       =   2  'Center
            Caption         =   "mono"
            Height          =   195
            Index           =   181
            Left            =   2220
            TabIndex        =   273
            Top             =   120
            Width           =   885
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Aliquota"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   182
            Left            =   1440
            TabIndex        =   278
            Top             =   660
            Width           =   735
         End
         Begin VB.Label Label1 
            Alignment       =   2  'Center
            Caption         =   "pluri"
            Height          =   195
            Index           =   183
            Left            =   3240
            TabIndex        =   274
            Top             =   120
            Width           =   885
         End
         Begin VB.Label Label1 
            Caption         =   "Minimo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   180
            Left            =   1500
            TabIndex        =   275
            Top             =   300
            Width           =   675
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "5.1 Contribuzione ridotta"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1635
         Index           =   51
         Left            =   900
         TabIndex        =   219
         Top             =   420
         Width           =   4395
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   152
            Left            =   3900
            MaxLength       =   2
            TabIndex        =   223
            Text            =   "3"
            Top             =   720
            Width           =   315
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   151
            Left            =   3900
            MaxLength       =   2
            TabIndex        =   221
            Text            =   "35"
            Top             =   360
            Width           =   315
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   153
            Left            =   3900
            MaxLength       =   1
            TabIndex        =   225
            Text            =   "3"
            Top             =   1080
            Width           =   315
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Et� massima per contribuzione ridotta"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   152
            Left            =   480
            TabIndex        =   222
            Top             =   720
            Width           =   3345
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Et� massima prima iscrizione"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   151
            Left            =   1200
            TabIndex        =   220
            Top             =   360
            Width           =   2610
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "N� max di anni di contribuzione ridotta (limite di reddito = massimale 1a aliquota)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   480
            Index           =   153
            Left            =   60
            TabIndex        =   224
            Top             =   1080
            Width           =   3750
            WordWrap        =   -1  'True
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "5.5. Altro"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   55
         Left            =   5520
         TabIndex        =   281
         Top             =   4680
         Width           =   4335
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   191
            Left            =   3240
            TabIndex        =   286
            Top             =   300
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   190
            Left            =   2220
            TabIndex        =   285
            Top             =   300
            Width           =   915
         End
         Begin VB.Label Label1 
            Alignment       =   2  'Center
            Caption         =   "pluri"
            Height          =   195
            Index           =   192
            Left            =   3300
            TabIndex        =   283
            Top             =   120
            Width           =   885
         End
         Begin VB.Label Label1 
            Alignment       =   2  'Center
            Caption         =   "mono"
            Height          =   195
            Index           =   191
            Left            =   2280
            TabIndex        =   282
            Top             =   120
            Width           =   795
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Maternit�"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   190
            Left            =   1320
            TabIndex        =   284
            Top             =   300
            Width           =   825
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "5.3 FIRR"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3075
         Index           =   53
         Left            =   5520
         TabIndex        =   249
         Top             =   420
         Width           =   4335
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   176
            Left            =   3255
            TabIndex        =   266
            Top             =   1740
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   178
            Left            =   3255
            TabIndex        =   269
            Top             =   2100
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   175
            Left            =   2235
            TabIndex        =   265
            Top             =   1740
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   177
            Left            =   2235
            TabIndex        =   268
            Top             =   2100
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   173
            Left            =   2220
            TabIndex        =   262
            Top             =   1380
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   171
            Left            =   2220
            TabIndex        =   259
            Top             =   1020
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   174
            Left            =   3240
            TabIndex        =   263
            Top             =   1380
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   172
            Left            =   3240
            TabIndex        =   260
            Top             =   1020
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   179
            Left            =   3240
            TabIndex        =   271
            Top             =   2580
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   168
            Left            =   3240
            TabIndex        =   254
            Top             =   300
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   170
            Left            =   3240
            TabIndex        =   257
            Top             =   660
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   167
            Left            =   2220
            TabIndex        =   253
            Top             =   300
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   169
            Left            =   2220
            TabIndex        =   256
            Top             =   660
            Width           =   915
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Massimale 2^ aliquota"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   175
            Left            =   180
            TabIndex        =   264
            Top             =   1740
            Width           =   2010
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "3^ aliquota"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   177
            Left            =   1215
            TabIndex        =   267
            Top             =   2100
            Width           =   975
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "2^ aliquota"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   173
            Left            =   1200
            TabIndex        =   261
            Top             =   1380
            Width           =   975
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Massimale 1^ aliquota"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   171
            Left            =   165
            TabIndex        =   258
            Top             =   1020
            Width           =   2010
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Volume d'affari minimo per ricono- scimento anno di anzianit� (art.25)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   480
            Index           =   179
            Left            =   60
            TabIndex        =   270
            Top             =   2460
            Width           =   3135
            WordWrap        =   -1  'True
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Minimo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   167
            Left            =   1530
            TabIndex        =   252
            Top             =   300
            Width           =   645
         End
         Begin VB.Label Label1 
            Alignment       =   2  'Center
            Caption         =   "pluri"
            Height          =   195
            Index           =   170
            Left            =   3240
            TabIndex        =   251
            Top             =   120
            Width           =   885
         End
         Begin VB.Label Label1 
            Alignment       =   2  'Center
            Caption         =   "mono"
            Height          =   195
            Index           =   168
            Left            =   2220
            TabIndex        =   250
            Top             =   120
            Width           =   885
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "1^ aliquota"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   169
            Left            =   1200
            TabIndex        =   255
            Top             =   660
            Width           =   975
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "5.2 Soggettivo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3075
         Index           =   52
         Left            =   900
         TabIndex        =   226
         Top             =   2340
         Width           =   4395
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   166
            Left            =   3300
            TabIndex        =   248
            Top             =   2580
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   160
            Left            =   2280
            TabIndex        =   239
            Top             =   1380
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   161
            Left            =   3300
            TabIndex        =   240
            Top             =   1380
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   158
            Left            =   2280
            TabIndex        =   236
            Top             =   1020
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   159
            Left            =   3300
            TabIndex        =   237
            Top             =   1020
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   163
            Left            =   3300
            TabIndex        =   243
            Top             =   1740
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   165
            Left            =   3300
            TabIndex        =   246
            Top             =   2100
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   155
            Left            =   3300
            TabIndex        =   231
            Top             =   300
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   157
            Left            =   3300
            TabIndex        =   234
            Top             =   660
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   162
            Left            =   2280
            TabIndex        =   242
            Top             =   1740
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   164
            Left            =   2280
            TabIndex        =   245
            Top             =   2100
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   154
            Left            =   2280
            TabIndex        =   230
            Top             =   300
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   156
            Left            =   2280
            TabIndex        =   233
            Top             =   660
            Width           =   915
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Reddito minimo per ricono- scimento anno di anzianit� (art.25)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   480
            Index           =   166
            Left            =   180
            TabIndex        =   247
            Top             =   2460
            Width           =   3075
            WordWrap        =   -1  'True
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "2^ aliquota"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   160
            Left            =   1200
            TabIndex        =   238
            Top             =   1380
            Width           =   975
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Massimale 1^ aliquota"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   158
            Left            =   240
            TabIndex        =   235
            Top             =   1020
            Width           =   2010
         End
         Begin VB.Label Label1 
            Alignment       =   2  'Center
            Caption         =   "pluri"
            Height          =   195
            Index           =   155
            Left            =   3300
            TabIndex        =   228
            Top             =   120
            Width           =   885
         End
         Begin VB.Label Label1 
            Alignment       =   2  'Center
            Caption         =   "mono"
            Height          =   195
            Index           =   157
            Left            =   2280
            TabIndex        =   227
            Top             =   120
            Width           =   885
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Massimale 2^ aliquota"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   162
            Left            =   240
            TabIndex        =   241
            Top             =   1740
            Width           =   2010
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "3^ aliquota"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   164
            Left            =   1200
            TabIndex        =   244
            Top             =   2100
            Width           =   975
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Minimo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   154
            Left            =   1560
            TabIndex        =   229
            Top             =   300
            Width           =   645
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "1^ aliquota"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   156
            Left            =   1200
            TabIndex        =   232
            Top             =   660
            Width           =   975
         End
      End
      Begin VB.CheckBox Check1 
         Alignment       =   1  'Right Justify
         Caption         =   "Lettura parametri da database"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   150
         Left            =   7620
         TabIndex        =   218
         Top             =   120
         Value           =   2  'Grayed
         Width           =   3015
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "4 Requisiti di accesso alle prestazioni"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5595
      Index           =   4
      Left            =   60
      TabIndex        =   150
      Top             =   360
      Width           =   10775
      Begin VB.Frame Frame1 
         Caption         =   "4.3 Pensione di vecchiaia posticipata"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1155
         Index           =   43
         Left            =   120
         TabIndex        =   169
         Top             =   3000
         Width           =   5175
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   111
            Left            =   2040
            MaxLength       =   2
            TabIndex        =   171
            Text            =   "99"
            ToolTipText     =   "Pensione di vecchiaia - Et� massima in attesa del raggiungimento dell'anzianita minima M"
            Top             =   300
            Width           =   315
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   112
            Left            =   2340
            MaxLength       =   2
            TabIndex        =   172
            Text            =   "99"
            ToolTipText     =   "Pensione di vecchiaia - Et� massima in attesa del raggiungimento dell'anzianita minima F"
            Top             =   300
            Width           =   315
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Et� pens.post. (m/f)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   111
            Left            =   300
            TabIndex        =   170
            Top             =   300
            Width           =   1710
         End
      End
      Begin VB.CheckBox Check1 
         Alignment       =   1  'Right Justify
         Caption         =   "Lettura parametri da database"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   100
         Left            =   7620
         TabIndex        =   151
         Top             =   120
         Value           =   2  'Grayed
         Width           =   3015
      End
      Begin VB.Frame Frame1 
         Caption         =   "4.2 Pensione di vecchiaia anticipata"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1155
         Index           =   42
         Left            =   120
         TabIndex        =   162
         Top             =   1740
         Width           =   5175
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   110
            Left            =   4740
            MaxLength       =   2
            TabIndex        =   168
            Text            =   "99"
            ToolTipText     =   "Pensione di anzianit� - Et� minima F"
            Top             =   300
            Width           =   315
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   109
            Left            =   4440
            MaxLength       =   2
            TabIndex        =   167
            Text            =   "99"
            ToolTipText     =   "Pensione di anzianit� - Et� minima M"
            Top             =   300
            Width           =   315
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   108
            Left            =   2340
            MaxLength       =   2
            TabIndex        =   165
            Text            =   "99"
            ToolTipText     =   "Pensione di anzianit� - Anzianit� minima per con vincolo di et� M"
            Top             =   300
            Width           =   315
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   107
            Left            =   2040
            MaxLength       =   2
            TabIndex        =   164
            Text            =   "99"
            ToolTipText     =   "Pensione di anzianit� - Anzianit� minima per con vincolo di et� M"
            Top             =   300
            Width           =   315
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Et� minima (m/f)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   109
            Left            =   2940
            TabIndex        =   166
            Top             =   300
            Width           =   1425
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Anz. minima (m/f)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   107
            Left            =   480
            TabIndex        =   163
            Top             =   300
            Width           =   1575
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "4.5 Pensione di invalidit�"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1155
         Index           =   45
         Left            =   5400
         TabIndex        =   183
         Top             =   480
         Width           =   5175
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   121
            Left            =   4440
            TabIndex        =   188
            ToolTipText     =   "Pensione di invalidit� - % della pensione di inabilit�"
            Top             =   300
            Width           =   615
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   120
            Left            =   2340
            MaxLength       =   2
            TabIndex        =   186
            Text            =   "99"
            ToolTipText     =   "Pensione di invalidit� - Anzianit� minima F"
            Top             =   300
            Width           =   315
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   119
            Left            =   2040
            MaxLength       =   2
            TabIndex        =   185
            Text            =   "99"
            ToolTipText     =   "Pensione di invalidit� - Anzianit� minima M"
            Top             =   300
            Width           =   315
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Fraz. pens. inab."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   121
            Left            =   2940
            TabIndex        =   187
            Top             =   300
            Width           =   1512
            WordWrap        =   -1  'True
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Anz. minima (m/f)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   119
            Left            =   480
            TabIndex        =   184
            Top             =   300
            Width           =   1500
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "4.4 Pensione di inabilit�"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1155
         Index           =   44
         Left            =   120
         TabIndex        =   173
         Top             =   4260
         Width           =   5175
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   116
            Left            =   2280
            MaxLength       =   2
            TabIndex        =   179
            Text            =   "99"
            ToolTipText     =   "Pensione di inabilit� - Anzianit� aggiuntiva riconosciuta F"
            Top             =   720
            Width           =   315
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   115
            Left            =   1980
            MaxLength       =   2
            TabIndex        =   178
            Text            =   "99"
            ToolTipText     =   "Pensione di inabilit� - Anzianit� aggiuntiva riconosciuta M"
            Top             =   720
            Width           =   315
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   118
            Left            =   4740
            MaxLength       =   2
            TabIndex        =   182
            Text            =   "99"
            ToolTipText     =   "Pensione di inabilit� - Anzianit� massima riconosciuta F"
            Top             =   720
            Width           =   315
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   117
            Left            =   4440
            MaxLength       =   2
            TabIndex        =   181
            Text            =   "99"
            ToolTipText     =   "Pensione di inabilit� - Anzianit� massima riconosciuta M"
            Top             =   720
            Width           =   315
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   114
            Left            =   2280
            MaxLength       =   2
            TabIndex        =   176
            Text            =   "99"
            ToolTipText     =   "Pensione di inabilit� - Anzianit� minima F"
            Top             =   300
            Width           =   315
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   113
            Left            =   1980
            MaxLength       =   2
            TabIndex        =   175
            Text            =   "99"
            ToolTipText     =   "Pensione di inabilit� - Anzianit� minima M"
            Top             =   300
            Width           =   315
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Anz.max.ricon. (m/f)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   117
            Left            =   2715
            TabIndex        =   180
            Top             =   720
            Width           =   1710
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Anz.agg.ricon. (m/f)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   115
            Left            =   240
            TabIndex        =   177
            Top             =   720
            Width           =   1695
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Anz. minima (m/f)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   113
            Left            =   420
            TabIndex        =   174
            Top             =   300
            Width           =   1575
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "4.8 Totalizzazione"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1155
         Index           =   48
         Left            =   5400
         TabIndex        =   208
         ToolTipText     =   "Anzianit� di servizio riconosciuta"
         Top             =   4260
         Width           =   5175
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   134
            Left            =   2340
            MaxLength       =   2
            TabIndex        =   211
            Text            =   "99"
            ToolTipText     =   "Pensione indiretta - Anzianit� minima F"
            Top             =   300
            Width           =   315
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   133
            Left            =   2040
            MaxLength       =   2
            TabIndex        =   210
            Text            =   "99"
            ToolTipText     =   "Pensione indiretta - Anzianit� minima M"
            Top             =   300
            Width           =   315
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   135
            Left            =   4440
            MaxLength       =   2
            TabIndex        =   213
            Text            =   "99"
            ToolTipText     =   "Totalizzazione - Et� minima M"
            Top             =   300
            Width           =   315
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   136
            Left            =   4740
            MaxLength       =   2
            TabIndex        =   214
            Text            =   "99"
            ToolTipText     =   "Totalizzazione - Et� minima F"
            Top             =   300
            Width           =   315
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   137
            Left            =   2040
            TabIndex        =   216
            ToolTipText     =   "Totalizzazione - Tasso minimo di rivalutazione"
            Top             =   720
            Width           =   735
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Anz. minima (m/f)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   133
            Left            =   480
            TabIndex        =   209
            Top             =   300
            Width           =   1575
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Et� minima (m/f)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   135
            Left            =   2940
            TabIndex        =   212
            Top             =   300
            Width           =   1425
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Rival. minima"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   137
            Left            =   780
            TabIndex        =   215
            Top             =   720
            Width           =   1200
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "4.1 Pensione di vecchiaia ordinaria"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1155
         Index           =   41
         Left            =   120
         TabIndex        =   152
         Top             =   480
         Width           =   5175
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   106
            Left            =   2340
            MaxLength       =   2
            TabIndex        =   161
            Text            =   "99"
            ToolTipText     =   "Pensione di anzianit� - Anzianit� minima senza vincolo di et� F"
            Top             =   660
            Width           =   315
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   105
            Left            =   2040
            MaxLength       =   2
            TabIndex        =   160
            Text            =   "99"
            ToolTipText     =   "Pensione di anzianit� - Anzianit� minima senza vincolo di et� M"
            Top             =   660
            Width           =   315
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   104
            Left            =   4740
            MaxLength       =   2
            TabIndex        =   158
            Text            =   "99"
            ToolTipText     =   "Pensione di vecchiaia - Et� minima F"
            Top             =   300
            Width           =   315
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   103
            Left            =   4440
            MaxLength       =   2
            TabIndex        =   157
            Text            =   "99"
            ToolTipText     =   "Pensione di vecchiaia - Et� minima M"
            Top             =   300
            Width           =   315
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   102
            Left            =   2340
            MaxLength       =   2
            TabIndex        =   155
            Text            =   "99"
            ToolTipText     =   "Pensione di vecchiaia - anzianit� minima F"
            Top             =   300
            Width           =   315
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   101
            Left            =   2040
            MaxLength       =   2
            TabIndex        =   154
            Text            =   "99"
            ToolTipText     =   "Pensione di vecchiaia - anzianit� minima M"
            Top             =   300
            Width           =   315
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Quota minima (m/f)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   105
            Left            =   300
            TabIndex        =   159
            Top             =   660
            Width           =   1665
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Et� minima (m/f)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   103
            Left            =   2940
            TabIndex        =   156
            Top             =   300
            Width           =   1425
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Anz. minima (m/f)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   101
            Left            =   480
            TabIndex        =   153
            Top             =   300
            Width           =   1575
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "4.6 Pensione indiretta"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1155
         Index           =   46
         Left            =   5400
         TabIndex        =   189
         Top             =   1740
         Width           =   5175
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   125
            Left            =   2340
            MaxLength       =   2
            TabIndex        =   195
            Text            =   "99"
            ToolTipText     =   "Pensione indiretta - Anzianit� minima di calcolo F"
            Top             =   720
            Width           =   315
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   127
            Left            =   4740
            MaxLength       =   2
            TabIndex        =   198
            Text            =   "99"
            ToolTipText     =   "Pensione indiretta - Anzianit� massima di calcolo F"
            Top             =   720
            Width           =   315
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   124
            Left            =   2040
            MaxLength       =   2
            TabIndex        =   194
            Text            =   "99"
            ToolTipText     =   "Pensione indiretta - Anzianit� minima di calcolo M"
            Top             =   720
            Width           =   315
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   126
            Left            =   4440
            MaxLength       =   2
            TabIndex        =   197
            Text            =   "99"
            ToolTipText     =   "Pensione indiretta - Anzianit� massima di calcolo M"
            Top             =   720
            Width           =   315
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   122
            Left            =   2040
            MaxLength       =   2
            TabIndex        =   191
            Text            =   "99"
            ToolTipText     =   "Pensione indiretta - Anzianit� minima M"
            Top             =   300
            Width           =   315
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   123
            Left            =   2340
            MaxLength       =   2
            TabIndex        =   192
            Text            =   "99"
            ToolTipText     =   "Pensione indiretta - Anzianit� minima F"
            Top             =   300
            Width           =   315
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Anz.max.calc. (m/f)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   126
            Left            =   2760
            TabIndex        =   196
            Top             =   720
            Width           =   1650
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Anz.min.calc. (m/f)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   124
            Left            =   360
            TabIndex        =   193
            Top             =   720
            Width           =   1590
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Anz. minima (m/f)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   122
            Left            =   480
            TabIndex        =   190
            Top             =   300
            Width           =   1575
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "4.7 Prestazione previdenziale contributiva"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1155
         Index           =   47
         Left            =   5400
         TabIndex        =   199
         Top             =   3000
         Width           =   5175
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   132
            Left            =   2040
            TabIndex        =   207
            ToolTipText     =   "Restituzione contributi - % di contributi restituibili"
            Top             =   720
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   131
            Left            =   4740
            MaxLength       =   2
            TabIndex        =   205
            ToolTipText     =   "Restituzione contributi - Et� minima F"
            Top             =   300
            Width           =   315
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   130
            Left            =   4440
            MaxLength       =   2
            TabIndex        =   204
            ToolTipText     =   "Restituzione contributi - Et� minima M"
            Top             =   300
            Width           =   315
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   129
            Left            =   2340
            MaxLength       =   2
            TabIndex        =   202
            ToolTipText     =   "Restituzione contributi - Anzianit� minima F"
            Top             =   300
            Width           =   315
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   128
            Left            =   2040
            MaxLength       =   2
            TabIndex        =   201
            ToolTipText     =   "Restituzione contributi - Anzianit� minima M"
            Top             =   300
            Width           =   315
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "% contributi restituibili"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   132
            Left            =   120
            TabIndex        =   206
            Top             =   780
            Width           =   1890
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Et� di uscita (m/f)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   130
            Left            =   2880
            TabIndex        =   203
            Top             =   300
            Width           =   1515
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Anz.minima (m/f)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   128
            Left            =   540
            TabIndex        =   200
            Top             =   300
            Width           =   1455
         End
      End
   End
   Begin MSComctlLib.TabStrip TabStrip1 
      Height          =   6075
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   10875
      _ExtentX        =   19182
      _ExtentY        =   10716
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   8
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Parametri"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Tassi e stime"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab3 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Basi tecniche"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab4 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Requisiti di accesso"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab5 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Contribuzione"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab6 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Prestazioni"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab7 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Nuovi iscritti"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab8 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Risultati"
            ImageVarType    =   2
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame Div7 
      Caption         =   "7 Nuovi iscritti"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5595
      Left            =   60
      TabIndex        =   340
      Top             =   360
      Width           =   10775
      Begin VB.Frame Div7_3 
         Caption         =   "7.3. Volontari riattivati"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Left            =   300
         TabIndex        =   354
         Top             =   4200
         Width           =   10155
         Begin VB.ComboBox cbxNvStat 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   1560
            Style           =   2  'Dropdown List
            TabIndex        =   356
            ToolTipText     =   "Tabella nouovi iscritti architetti M"
            Top             =   300
            Width           =   1455
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Base statistica"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   255
            Left            =   120
            TabIndex        =   355
            Top             =   300
            Width           =   1380
            WordWrap        =   -1  'True
         End
      End
      Begin VB.Frame Div7_2 
         Caption         =   "7.2. Silenti riattivati"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Left            =   300
         TabIndex        =   351
         Top             =   3120
         Width           =   10155
         Begin VB.ComboBox cbxNeStat 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   1560
            Style           =   2  'Dropdown List
            TabIndex        =   353
            ToolTipText     =   "Tabella nouovi iscritti architetti M"
            Top             =   300
            Width           =   1455
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Base statistica"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   254
            Left            =   60
            TabIndex        =   352
            Top             =   300
            Width           =   1380
            WordWrap        =   -1  'True
         End
      End
      Begin VB.Frame Div7_1 
         Caption         =   "7.1. nuovi iscritti attivi"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2235
         Left            =   300
         TabIndex        =   342
         Top             =   600
         Width           =   10155
         Begin VB.OptionButton optNI_0 
            Caption         =   "Non considerati"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   150
            TabIndex        =   343
            Top             =   300
            Width           =   1755
         End
         Begin VB.OptionButton optNI_1 
            Caption         =   "Letti dalla tabella degli iscritti"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   150
            TabIndex        =   344
            Top             =   660
            Width           =   3075
         End
         Begin VB.OptionButton optNI_2 
            Caption         =   "Generati per rimpiazzare gli attivi eliminati annualmente"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   150
            TabIndex        =   345
            Top             =   1020
            Width           =   5235
         End
         Begin VB.ComboBox cbxNiStat 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   1560
            Style           =   2  'Dropdown List
            TabIndex        =   348
            ToolTipText     =   "Tabella nouovi iscritti architetti M"
            Top             =   1740
            Width           =   1455
         End
         Begin VB.ComboBox cbxNip 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   8550
            Style           =   2  'Dropdown List
            TabIndex        =   350
            ToolTipText     =   "Tabella nouovi iscritti architetti M"
            Top             =   1740
            Width           =   1455
         End
         Begin VB.OptionButton optNI_3 
            Caption         =   "Generati per rimpiazzare attivi e pensionati attivi eliminati annualmente"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   150
            TabIndex        =   346
            Top             =   1380
            Width           =   6555
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Base statistica"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   253
            Left            =   60
            TabIndex        =   347
            Top             =   1740
            Width           =   1380
            WordWrap        =   -1  'True
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Nuovi iscritti puntuali"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   252
            Left            =   6660
            TabIndex        =   349
            Top             =   1740
            Width           =   1800
         End
      End
      Begin VB.CheckBox ckxcv7 
         Alignment       =   1  'Right Justify
         Caption         =   "Lettura parametri da database"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   7620
         TabIndex        =   341
         Top             =   120
         Value           =   2  'Grayed
         Visible         =   0   'False
         Width           =   3015
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "6 Misura delle prestazioni"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5595
      Index           =   6
      Left            =   60
      TabIndex        =   287
      Top             =   360
      Width           =   10775
      Begin VB.Frame Frame1 
         Caption         =   "6.2 Pensione"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3615
         Index           =   62
         Left            =   3900
         TabIndex        =   313
         Top             =   540
         Width           =   2955
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   215
            Left            =   1920
            TabIndex        =   315
            Top             =   600
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   216
            Left            =   2520
            MaxLength       =   2
            TabIndex        =   317
            ToolTipText     =   "Supplementi di pensione - anni di calcolo"
            Top             =   1020
            Width           =   315
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   217
            Left            =   2520
            MaxLength       =   2
            TabIndex        =   319
            ToolTipText     =   "Supplementi di pensione - anni di calcolo"
            Top             =   1440
            Width           =   315
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Pensione minima"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   215
            Left            =   300
            TabIndex        =   314
            Top             =   600
            Width           =   1545
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "n� migliori redditi"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   216
            Left            =   1020
            TabIndex        =   316
            Top             =   1020
            Width           =   1455
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "su n� totale di redditi"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   217
            Left            =   660
            TabIndex        =   318
            Top             =   1440
            Width           =   1770
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "6.4 Reversibilit� Superstiti"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1095
         Index           =   64
         Left            =   180
         TabIndex        =   327
         Top             =   4320
         Width           =   10395
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   226
            Left            =   8100
            TabIndex        =   337
            ToolTipText     =   "Reversibilit� 2 orfani"
            Top             =   540
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   225
            Left            =   6840
            TabIndex        =   335
            ToolTipText     =   "Reversibilit� orfani soli (53)"
            Top             =   540
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   227
            Left            =   9420
            TabIndex        =   339
            ToolTipText     =   "Reversibilit� 3 orfani"
            Top             =   540
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   224
            Left            =   4920
            TabIndex        =   333
            ToolTipText     =   "Reversibilit� vedovi + 2 orfani"
            Top             =   540
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   223
            Left            =   2880
            TabIndex        =   331
            ToolTipText     =   "Reversibilit� vedovi + 1 orfano"
            Top             =   540
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   222
            Left            =   780
            TabIndex        =   329
            ToolTipText     =   "Reversibilit� vedovi soli"
            Top             =   540
            Width           =   735
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "2 orfani soli"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   226
            Left            =   7860
            TabIndex        =   336
            ToolTipText     =   "(aoo) Reversibilit� 2 orfani"
            Top             =   300
            Width           =   1005
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Orfano solo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   225
            Left            =   6540
            TabIndex        =   334
            ToolTipText     =   "(ao) Reversibilit� orfani soli "
            Top             =   300
            Width           =   1035
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "3 orfani soli"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   227
            Left            =   9120
            TabIndex        =   338
            ToolTipText     =   "(aooo) Reversibilit� 3 orfani"
            Top             =   300
            Width           =   1005
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   " Vedovo/a e 1 orfano"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   223
            Left            =   1740
            TabIndex        =   330
            ToolTipText     =   "(avo) Reversibilit� vedovi + 1 orfano"
            Top             =   300
            Width           =   1875
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Vedovo/a solo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   222
            Left            =   180
            TabIndex        =   328
            ToolTipText     =   "(AV) Reversibilit� vedovi soli"
            Top             =   300
            Width           =   1335
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Vedovo/a e 2 orfani"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   224
            Left            =   3900
            TabIndex        =   332
            ToolTipText     =   "(avoo) Reversibilit� vedovi + 2 orfani (52)"
            Top             =   300
            Width           =   1755
         End
      End
      Begin VB.CheckBox Check1 
         Alignment       =   1  'Right Justify
         Caption         =   "Lettura parametri da database"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   200
         Left            =   7620
         TabIndex        =   288
         Top             =   120
         Value           =   2  'Grayed
         Width           =   3015
      End
      Begin VB.Frame Frame1 
         Caption         =   "6.1 Scaglioni IRPEF"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3615
         Index           =   61
         Left            =   180
         TabIndex        =   289
         Top             =   540
         Width           =   3555
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   214
            Left            =   2520
            TabIndex        =   312
            Text            =   "0"
            Top             =   3120
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   213
            Left            =   1500
            TabIndex        =   311
            Top             =   3120
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   212
            Left            =   2520
            TabIndex        =   309
            Text            =   "0"
            Top             =   2700
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   211
            Left            =   1500
            TabIndex        =   308
            Top             =   2700
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   201
            Left            =   1500
            TabIndex        =   293
            Top             =   600
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   203
            Left            =   1500
            TabIndex        =   296
            Top             =   1020
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   205
            Left            =   1500
            TabIndex        =   299
            Top             =   1440
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   207
            Left            =   1500
            TabIndex        =   302
            Top             =   1860
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   209
            Left            =   1500
            TabIndex        =   305
            Top             =   2280
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   210
            Left            =   2520
            TabIndex        =   306
            Text            =   "0"
            Top             =   2280
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   202
            Left            =   2520
            TabIndex        =   294
            Text            =   "0,0200"
            Top             =   600
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   204
            Left            =   2520
            TabIndex        =   297
            Text            =   "0,0171"
            Top             =   1020
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   206
            Left            =   2520
            TabIndex        =   300
            Text            =   "0,0143"
            Top             =   1440
            Width           =   915
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   208
            Left            =   2520
            TabIndex        =   303
            Text            =   "0,0114"
            Top             =   1860
            Width           =   915
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "(Scaglione G)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   213
            Left            =   240
            TabIndex        =   310
            Top             =   3120
            Width           =   1230
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "(Scaglione F)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   4
            Left            =   240
            TabIndex        =   307
            Top             =   2700
            Width           =   1200
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Tetto di reddito"
            Height          =   195
            Index           =   204
            Left            =   1380
            TabIndex        =   290
            Top             =   360
            Width           =   1125
            WordWrap        =   -1  'True
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Aliquota"
            Height          =   195
            Index           =   202
            Left            =   2700
            TabIndex        =   291
            Top             =   360
            Width           =   570
            WordWrap        =   -1  'True
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Scaglione A"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   201
            Left            =   300
            TabIndex        =   292
            Top             =   600
            Width           =   1095
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Scaglione B"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   203
            Left            =   300
            TabIndex        =   295
            Top             =   1020
            Width           =   1095
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Scaglione D"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   207
            Left            =   300
            TabIndex        =   301
            Top             =   1860
            Width           =   1110
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "(Scaglione E)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   209
            Left            =   240
            TabIndex        =   304
            Top             =   2280
            Width           =   1215
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Scaglione C"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   205
            Left            =   300
            TabIndex        =   298
            Top             =   1440
            Width           =   1095
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "6.3 Supplementi di pensione"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3615
         Index           =   63
         Left            =   7020
         TabIndex        =   320
         Top             =   540
         Width           =   3555
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   219
            Left            =   3120
            MaxLength       =   2
            TabIndex        =   324
            ToolTipText     =   "Supplementi di pensione - anni di calcolo"
            Top             =   1020
            Width           =   315
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   220
            Left            =   2640
            TabIndex        =   326
            ToolTipText     =   "Supplementi di pensione - % di contribuzione da considerare"
            Top             =   1380
            Width           =   795
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   218
            Left            =   3120
            MaxLength       =   2
            TabIndex        =   322
            ToolTipText     =   "Supplementi di pensione - anni di calcolo"
            Top             =   600
            Width           =   315
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Et� minima fine supplementi"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   219
            Left            =   540
            TabIndex        =   323
            Top             =   1020
            Width           =   2490
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "% di contributi da restituire"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   220
            Left            =   300
            TabIndex        =   325
            Top             =   1380
            Width           =   2310
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Anni di calcolo supplemento"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   218
            Left            =   540
            TabIndex        =   321
            Top             =   600
            Width           =   2520
         End
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "3 Basi tecniche"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5595
      Index           =   3
      Left            =   60
      TabIndex        =   111
      Top             =   360
      Width           =   10775
      Begin VB.Frame Frame1 
         Caption         =   "3.1 Probabilit� di morte"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1035
         Index           =   31
         Left            =   180
         TabIndex        =   113
         Top             =   480
         Width           =   10395
         Begin VB.ComboBox Combo1 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   71
            Left            =   120
            Style           =   2  'Dropdown List
            TabIndex        =   115
            ToolTipText     =   "Base demografica x passaggi da Attivi a Deceduti"
            Top             =   540
            Width           =   1575
         End
         Begin VB.ComboBox Combo1 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   76
            Left            =   8280
            Style           =   2  'Dropdown List
            TabIndex        =   123
            ToolTipText     =   "Base demografica x passaggi da Attivi a Deceduti"
            Top             =   540
            Width           =   1515
         End
         Begin VB.ComboBox Combo1 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   73
            Left            =   4140
            Style           =   2  'Dropdown List
            TabIndex        =   119
            ToolTipText     =   "Base demografica x passaggi da Attivi a Deceduti"
            Top             =   540
            Width           =   1575
         End
         Begin VB.ComboBox Combo1 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   75
            Left            =   6180
            Style           =   2  'Dropdown List
            TabIndex        =   121
            ToolTipText     =   "Base demografica x passaggi da Attivi a Deceduti"
            Top             =   540
            Width           =   1575
         End
         Begin VB.ComboBox Combo1 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   72
            Left            =   2160
            Style           =   2  'Dropdown List
            TabIndex        =   117
            ToolTipText     =   "Base demografica x passaggi da Attivi a Deceduti"
            Top             =   540
            Width           =   1575
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Attivi e volontari"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   71
            Left            =   180
            TabIndex        =   114
            Top             =   300
            Width           =   1395
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Pensionati"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   73
            Left            =   4140
            TabIndex        =   118
            Top             =   300
            Width           =   945
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Pensionati di invalidit�"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   76
            Left            =   8280
            TabIndex        =   122
            Top             =   300
            Width           =   1995
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Pensionati di inabilt�"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   75
            Left            =   6180
            TabIndex        =   120
            Top             =   300
            Width           =   1845
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Silenti"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   72
            Left            =   2160
            TabIndex        =   116
            Top             =   300
            Width           =   540
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "3.2 Altre probabilt�"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1020
         Index           =   32
         Left            =   240
         TabIndex        =   124
         Top             =   1740
         Width           =   10395
         Begin VB.ComboBox cbxQav 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   120
            Style           =   2  'Dropdown List
            TabIndex        =   359
            ToolTipText     =   "Base demografica x passaggi da Attivi a Deceduti"
            Top             =   540
            Width           =   1575
         End
         Begin VB.ComboBox Combo1 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   78
            Left            =   4080
            Style           =   2  'Dropdown List
            TabIndex        =   128
            ToolTipText     =   "Base demografica x passaggi da Attivi a Deceduti"
            Top             =   540
            Width           =   1575
         End
         Begin VB.ComboBox Combo1 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   79
            Left            =   6060
            Style           =   2  'Dropdown List
            TabIndex        =   130
            ToolTipText     =   "Base demografica x passaggi da Attivi a Deceduti"
            Top             =   540
            Width           =   1575
         End
         Begin VB.ComboBox cbxQae 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   2100
            Style           =   2  'Dropdown List
            TabIndex        =   126
            ToolTipText     =   "Base demografica x passaggi da Attivi a Deceduti"
            Top             =   540
            Width           =   1575
         End
         Begin VB.ComboBox Combo1 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   80
            Left            =   8160
            Style           =   2  'Dropdown List
            TabIndex        =   132
            ToolTipText     =   "Base demografica x passaggi da Attivi a Deceduti"
            Top             =   540
            Width           =   1575
         End
         Begin VB.Label lblQav 
            AutoSize        =   -1  'True
            Caption         =   "Volontari da attivi"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   120
            TabIndex        =   360
            Top             =   300
            Width           =   1530
         End
         Begin VB.Label lblQae 
            AutoSize        =   -1  'True
            Caption         =   "Silenti da attivi"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   2100
            TabIndex        =   125
            Top             =   300
            Width           =   1275
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Invalidit� degli attivi"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   79
            Left            =   6060
            TabIndex        =   129
            Top             =   300
            Width           =   1740
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Inabilit� degli attivi"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   78
            Left            =   4080
            TabIndex        =   127
            Top             =   300
            Width           =   1635
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Uscita per altre cause"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   80
            Left            =   8160
            TabIndex        =   131
            Top             =   300
            Width           =   1950
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "3.3 Linee reddituali"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1035
         Index           =   33
         Left            =   240
         TabIndex        =   133
         Top             =   3000
         Width           =   5835
         Begin VB.ComboBox Combo1 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   81
            Left            =   120
            Style           =   2  'Dropdown List
            TabIndex        =   135
            ToolTipText     =   "Base demografica x passaggi da Attivi a Deceduti"
            Top             =   540
            Width           =   1575
         End
         Begin VB.ComboBox Combo1 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   82
            Left            =   2100
            Style           =   2  'Dropdown List
            TabIndex        =   137
            ToolTipText     =   "Base demografica x passaggi da Attivi a Deceduti"
            Top             =   540
            Width           =   1575
         End
         Begin VB.ComboBox Combo1 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   83
            Left            =   4080
            Style           =   2  'Dropdown List
            TabIndex        =   139
            ToolTipText     =   "Base demografica x passaggi da Attivi a Deceduti"
            Top             =   540
            Width           =   1575
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Attivi"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   81
            Left            =   120
            TabIndex        =   134
            Top             =   300
            Width           =   420
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Pensionati attivi"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   82
            Left            =   2100
            TabIndex        =   136
            Top             =   300
            Width           =   1395
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Nuovi iscritti"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   83
            Left            =   4080
            TabIndex        =   138
            Top             =   300
            Width           =   1065
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "3.5 Supersiti"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1035
         Index           =   35
         Left            =   240
         TabIndex        =   143
         Top             =   4320
         Width           =   10335
         Begin VB.ComboBox Combo1 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   85
            Left            =   120
            Style           =   2  'Dropdown List
            TabIndex        =   145
            ToolTipText     =   "Base demografica x passaggi da Attivi a Deceduti"
            Top             =   540
            Width           =   1575
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   86
            Left            =   2880
            TabIndex        =   147
            ToolTipText     =   "Et� media della vedova di dante causa morto a 30 anni"
            Top             =   540
            Width           =   735
         End
         Begin VB.ComboBox Combo1 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   74
            Left            =   4140
            Style           =   2  'Dropdown List
            TabIndex        =   149
            ToolTipText     =   "Base demografica x passaggi da Attivi a Deceduti"
            Top             =   540
            Width           =   1575
         End
         Begin VB.Label Label1 
            Alignment       =   2  'Center
            AutoSize        =   -1  'True
            Caption         =   "Probabilit� di famiglia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   85
            Left            =   120
            TabIndex        =   144
            Top             =   300
            Width           =   1965
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Et� limite orfani"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   86
            Left            =   2280
            TabIndex        =   146
            Top             =   300
            Width           =   1350
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Mortalit� superstiti"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   74
            Left            =   4140
            TabIndex        =   148
            Top             =   300
            Width           =   1605
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "3.4 Conversione del montante in rendita"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1035
         Index           =   34
         Left            =   6300
         TabIndex        =   140
         Top             =   3000
         Width           =   4275
         Begin VB.ComboBox Combo1 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   84
            ItemData        =   "AfpElab.frx":0442
            Left            =   120
            List            =   "AfpElab.frx":0444
            Style           =   2  'Dropdown List
            TabIndex        =   142
            Top             =   540
            Width           =   1395
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Tav. coeff. conversione"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   84
            Left            =   60
            TabIndex        =   141
            Top             =   300
            Width           =   2085
         End
      End
      Begin VB.CheckBox Check1 
         Alignment       =   1  'Right Justify
         Caption         =   "Lettura parametri da database"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   70
         Left            =   7620
         TabIndex        =   112
         Top             =   120
         Value           =   2  'Grayed
         Visible         =   0   'False
         Width           =   3015
      End
   End
   Begin VB.Frame Div8 
      BackColor       =   &H8000000C&
      BorderStyle     =   0  'None
      Height          =   5595
      Left            =   60
      TabIndex        =   357
      Top             =   360
      Width           =   10775
      Begin RichTextLib.RichTextBox rtfOutput 
         Height          =   5475
         Left            =   60
         TabIndex        =   358
         Top             =   60
         Width           =   10635
         _ExtentX        =   18759
         _ExtentY        =   9657
         _Version        =   393217
         ScrollBars      =   3
         RightMargin     =   99999
         TextRTF         =   $"AfpElab.frx":0446
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
End
Attribute VB_Name = "frmElab"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
DefStr A-Z

'Private Const P_MODELLO = 0  '20130307LC (commentato)
'Private p_appl As TAppl
Public p_bLoading As Boolean
Public p_bDataChanged As Boolean
Public p_req As Collection  '20130307LC
Private tit$()
Private Lsm#()
Private tb#()


Private Sub Command1_Click(Index As Integer)
  Select Case Index
  Case 0
    Call Elabora(0)
  Case 1
    Unload Me
  End Select
End Sub


Private Sub Form_Unload(Cancel As Integer)
  If Screen.MousePointer = vbHourglass Then
    Cancel = True
    Exit Sub
  End If
End Sub


Private Sub optNI_0_Click()
  Call ParametriNI(0, Me) '20120319LC
  p_bDataChanged = True  '20140604LC
End Sub
Private Sub optNI_1_Click()
  Call ParametriNI(1, Me) '20120319LC
  p_bDataChanged = True  '20140604LC
End Sub
Private Sub optNI_2_Click()
  Call ParametriNI(2, Me) '20120319LC
  p_bDataChanged = True  '20140604LC
End Sub
Private Sub optNI_3_Click()
  Call ParametriNI(3, Me) '20120319LC
  p_bDataChanged = True  '20140604LC
End Sub



Private Sub optTipoElab_Click(Index As Integer)
  Call ParametriTipoElab(Index, Me)
  p_bDataChanged = True  '20140604LC
End Sub


'20080614LC
Private Sub TabStrip1_Click()
  If p_bLoading Then Exit Sub
  Call MdiAfp.p_afp.TabStrip1_Click(Me) '20121019LC
End Sub


Private Sub Text1_Change(Index As Integer)
  If p_bLoading Then Exit Sub
  p_bDataChanged = True
  '20120213LC (inizio)
  Select Case Index
  Case P_btAnniEl1
    '20121019LC
    'Text1(P_btAnniEl2).Text = Val(Text1(P_btAnniEl1).Text) + IIf(OPZ_ELAB_CODA = 0, 0, 100)
    Me.Text1(P_btAnniEl2).Text = Val(Me.Text1(P_btAnniEl1).Text) + IIf(MdiAfp.p_afp.OPZ_ELAB_CODA_Get = 0, 0, 100)
  Case P_cvTrmc1 '= 28
    If IsNumeric(Text1(P_cvTrmc1).Text) Then
      Text1(P_cvTrmc2).Text = Text1(P_cvTrmc1).Text
      Text1(P_cvTrmc3).Text = Text1(P_cvTrmc1).Text
      Text1(P_cvTrmc4).Text = Text1(P_cvTrmc1).Text
      Text1(P_cvTrmc5).Text = Text1(P_cvTrmc1).Text
      Text1(P_cvTrmc6).Text = Text1(P_cvTrmc1).Text
    End If
  Case P_cvTv '= 34  'Valutazione
    If IsNumeric(Text1(P_cvTv).Text) Then
      Text1(P_cvTi).Text = Text1(P_cvTi).Text
    End If
  Case P_cvTev '= 36 'Tasso di inflazione (elementi variabili)
    If IsNumeric(Text1(P_cvTev).Text) Then
      Text1(P_cvTrn).Text = Text1(P_cvTev).Text
      Text1(P_cvTs1a).Text = Text1(P_cvTev).Text
      Text1(P_cvTs2a).Text = Text1(P_cvTev).Text
      Text1(P_cvTs1p).Text = Text1(P_cvTev).Text
      Text1(P_cvTs2p).Text = Text1(P_cvTev).Text
      Text1(P_cvTs1n).Text = Text1(P_cvTev).Text
      Text1(P_cvTs2n).Text = Text1(P_cvTev).Text
      Text1(P_cvTp1Al).Text = Text1(P_cvTev).Text
    End If
  Case P_cvTp1Al
    If IsNumeric(Text1(P_cvTp1Al).Text) Then
      Text1(P_cvTp2Al).Text = Text1(P_cvTp1Al).Text
      Text1(P_cvTp3Al).Text = Text1(P_cvTp1Al).Text
    End If
  End Select
  '20120213LC (fine)
End Sub


Private Sub rtf1_DblClick(Index As Integer)
  Dim fo&
  Dim sz$
  Dim tim1#
  
  tim1 = Timer
  Select Case Index
  Case 0
    With MdiAfp.CommonDialog1
      If 1 = 2 Then  '20140312LC
        .Filter = "File di output|*.out|File di testo|*.prn;*.txt|Tutti i file|*.*"
        .FilterIndex = 1
        .ShowOpen
        fo = FreeFile
        If ON_ERR_EH = True Then On Error GoTo ErrorHandler
        If .FileName <> "" Then
          Open .FileName For Binary As #fo
          sz = Space(LOF(fo))
          Get #fo, , sz
          Me.rtfOutput.Text = sz
        Else
          Call MsgBox("Salvataggio del file annullato dall'utente", vbInformation)
        End If
      End If
    End With
  End Select
  
ExitPoint:
  Close #fo
  '20121019LC
  'glo.tAcc = glo.tAcc + Timer - tim1
  MdiAfp.p_afp.tAcc = MdiAfp.p_afp.tAcc + Timer - tim1
  On Error GoTo 0
  Exit Sub
  
ErrorHandler:
  Resume ExitPoint
End Sub


Private Sub Titolo()
  '20121019LC
  Caption = "ENAS" & MdiAfp.p_afp.Counter
End Sub


Public Sub FormParametriSalva()
  '20121019LC
  'Call TAppl_ParamSalva(p_appl, Me)
  Call MdiAfp.p_afp.ParamSalva(Me)
End Sub

Public Sub Stampa()
  Dim i1%
  
  i1 = Printer.Orientation
  With MdiAfp.CommonDialog1
    .Orientation = cdlLandscape '  Printer.Orientation
    .Flags = cdlPDReturnDC + cdlPDNoPageNums
    If Me.rtfOutput.SelLength = 0 Then
      .Flags = .Flags + cdlPDAllPages
    Else
      .Flags = .Flags + cdlPDSelection
    End If
    .CancelError = True
    If ON_ERR_RN = True Then On Error Resume Next
    .ShowPrinter
    If Err Then
      Call MsgBox("Impossibile stampare a causa del seguente errore:" & vbCrLf & Err.Description)
      Err.Clear
    Else
      Me.rtfOutput.SelPrint .hDC
      Printer.EndDoc
    End If
  End With
  Printer.Orientation = i1
End Sub


Sub Interrompi()
  '20121019LC
  'p_appl.bStop = True
  'DoEvents
  Call MdiAfp.p_afp.Interrompi
End Sub


'20120131LC (tutta)
Public Sub Check1_Click(Index As Integer)
  Dim b As Boolean
  
  b = Not (Check1(Index).Value = vbChecked)
  Select Case Index
  Case P_cv1
    '*******************
    'Frame PARAMETRI (1)
    '*******************
    If b = False Then
      Call MsgBox("TODO Check1_Click-1")
      Stop
    End If
  Case P_cv2
    '******************************
    'Frame TASSI ED ALTRE STIME (2)
    '******************************
    '--- 2.1 Tassi di valutazione e rivalutazione ---
    If OPZ_INT_CV21 = 1 Then  '20140605LC  'bCv25 = True
      Call TextBox_Set(Text1(P_cvTv), b)
      Call TextBox_Set(Text1(P_cvTi), b)
    End If
    Call TextBox_Set(Text1(P_cvTev), b)
    Call TextBox_Set(Text1(P_cvTrn), b)
    '--- 2.2 Tassi di rivalutazione dei montanti contributivi ---
    Call TextBox_Set(Text1(P_cvTrmc0), b)
    Call TextBox_Set(Text1(P_cvTrmc1), b)
    Call TextBox_Set(Text1(P_cvTrmc2), b)
    Call TextBox_Set(Text1(P_cvTrmc3), b)
    Call TextBox_Set(Text1(P_cvTrmc4), b)
    Call TextBox_Set(Text1(P_cvTrmc5), b)
    Call TextBox_Set(Text1(P_cvTrmc6), b)
    '--- 2.3 Tassi di rivalutazione di redditi e volumi d'affari ---
    Call TextBox_Set(Text1(P_cvTs1a), b)
    Call TextBox_Set(Text1(P_cvTs1p), b)
    Call TextBox_Set(Text1(P_cvTs1n), b)
    '--- 2.4 Rivalutazione delle pensioni ---
    Call TextBox_Set(Text1(P_cvTp1Max), b)
    Call TextBox_Set(Text1(P_cvTp1Al), b)
    Call TextBox_Set(Text1(P_cvTp2Max), b)
    Call TextBox_Set(Text1(P_cvTp2Al), b)
    Call TextBox_Set(Text1(P_cvTp3Max), b)
    Call TextBox_Set(Text1(P_cvTp3Al), b)
    '--- 20161112LC (inizio)
    Call TextBox_Set(Text1(P_cvTp4Max), b)
    Call TextBox_Set(Text1(P_cvTp4Al), b)
    Call TextBox_Set(Text1(P_cvTp5Max), b)
    Call TextBox_Set(Text1(P_cvTp5Al), b)
    '--- 20161112LC (fine)
    '--- 2.5 Altre stime ----
    If OPZ_INT_CV25 = 1 Then  '20140605LC  'bCv25 = True
      Call TextBox_Set(Text1(P_cvPrcPVattM), b)
      Call TextBox_Set(Text1(P_cvPrcPVattF), b)
      Call TextBox_Set(Text1(P_cvPrcPAattM), b)
      Call TextBox_Set(Text1(P_cvPrcPAattF), b)
      Call TextBox_Set(Text1(P_cvPrcPCattM), b)
      Call TextBox_Set(Text1(P_cvPrcPCattF), b)
    End If
  Case P_cv3
    '***********************
    'Frame BASI_TECNICHE (3)
    '***********************
    If b = False Then
      Call MsgBox("TODO Check1_Click-3")
      Stop
    End If
  Case P_cv4
    '******************************
    'Frame REQUISITI DI ACCESSO (4)
    '******************************
    '--- 4.1 Pensione di vecchiaia ordinaria ----
    Call TextBox_Set(Text1(P_cvPveo30M), b)
    Call TextBox_Set(Text1(P_cvPveo30F), b)
    Call TextBox_Set(Text1(P_cvPveo65M), b)
    Call TextBox_Set(Text1(P_cvPveo65F), b)
    Call TextBox_Set(Text1(P_cvPveo98M), b)
    Call TextBox_Set(Text1(P_cvPveo98F), b)
    '--- 4.2 Pensione di vecchiaia anticipata ----
    Call TextBox_Set(Text1(P_cvPvea35M), b)
    Call TextBox_Set(Text1(P_cvPvea35F), b)
    Call TextBox_Set(Text1(P_cvPvea58M), b)
    Call TextBox_Set(Text1(P_cvPvea58F), b)
    '--- 4.3 Pensione di vecchiaia posticipata ----
    Call TextBox_Set(Text1(P_cvPvep70M), b)
    Call TextBox_Set(Text1(P_cvPvep70F), b)
    '--- 4.4 Pensione di inabilit� ----
    Call TextBox_Set(Text1(P_cvPinab2M), b)
    Call TextBox_Set(Text1(P_cvPinab2F), b)
    Call TextBox_Set(Text1(P_cvPinab10M), b)
    Call TextBox_Set(Text1(P_cvPinab10F), b)
    Call TextBox_Set(Text1(P_cvPinab35M), b)
    Call TextBox_Set(Text1(P_cvPinab35F), b)
    '--- 4.5 Pensione di invalidit� ----
    Call TextBox_Set(Text1(P_cvPinv5M), b)
    Call TextBox_Set(Text1(P_cvPinv5F), b)
    Call TextBox_Set(Text1(P_cvPinv70), b)
    '--- 4.6 Pensione indiretta ----
    Call TextBox_Set(Text1(P_cvPind2M), b)
    Call TextBox_Set(Text1(P_cvPind2F), b)
    Call TextBox_Set(Text1(P_cvPind20M), b)
    Call TextBox_Set(Text1(P_cvPind20F), b)
    Call TextBox_Set(Text1(P_cvPind30M), b)
    Call TextBox_Set(Text1(P_cvPind30F), b)
    '--- 4.7 Restituzione dei contributi ----
    Call TextBox_Set(Text1(P_cvRcHminM), b)
    Call TextBox_Set(Text1(P_cvRcHminF), b)
    Call TextBox_Set(Text1(P_cvRcXminM), b)
    Call TextBox_Set(Text1(P_cvRcXminF), b)
    Call TextBox_Set(Text1(P_cvRcPrcCon), b)
    'Call TextBox_Set(Text1(P_cvRcPrcRiv), b)  '20150512LC (commentato)
    '--- 4.8 Totalizzazione ----
    Call TextBox_Set(Text1(P_cvTzHminM), b)
    Call TextBox_Set(Text1(P_cvTzHminF), b)
    Call TextBox_Set(Text1(P_cvTzXminM), b)
    Call TextBox_Set(Text1(P_cvTzXminF), b)
    Call TextBox_Set(Text1(P_cvTzRivMin), b)
  Case P_cv5
    '***********************
    'Frame CONTRIBUZIONE (5)
    '***********************
    '--- 5.1 Contribuzione ridotta ----
    Call TextBox_Set(Text1(P_cvEtaR), b)
    Call TextBox_Set(Text1(P_cvXmaxR), b)
    Call TextBox_Set(Text1(P_cvAnniR), b)
    '--- 5.2 Soggettivo ----
    Call TextBox_Set(Text1(P_cvSogg0Min), b)
    Call TextBox_Set(Text1(P_cvSogg0MinR), b)
    Call TextBox_Set(Text1(P_cvSogg1Al), b)
    Call TextBox_Set(Text1(P_cvSogg1AlR), b)
    Call TextBox_Set(Text1(P_cvSogg1Max), b)
    Call TextBox_Set(Text1(P_cvSogg1MaxR), b)
    '--- 5.3 Integrativo / FIRR ----
    Call TextBox_Set(Text1(P_cvInt0Min), b)
    Call TextBox_Set(Text1(P_cvInt0MinR), b)
    Call TextBox_Set(Text1(P_cvInt1Al), b)
    Call TextBox_Set(Text1(P_cvInt1AlR), b)
    Call TextBox_Set(Text1(P_cvInt1Max), b)
    Call TextBox_Set(Text1(P_cvInt1MaxR), b)
    Call TextBox_Set(Text1(P_cvInt2Al), b)
    Call TextBox_Set(Text1(P_cvInt2AlR), b)
    Call TextBox_Set(Text1(P_cvInt2Max), b)
    Call TextBox_Set(Text1(P_cvInt2MaxR), b)
    Call TextBox_Set(Text1(P_cvInt3Al), b)
    Call TextBox_Set(Text1(P_cvInt3AlR), b)
    '--- 5.4 Assistenziale ----
    Call TextBox_Set(Text1(P_cvAss0Min), b)
    Call TextBox_Set(Text1(P_cvAss0MinR), b)
    Call TextBox_Set(Text1(P_cvAss1Al), b)
    Call TextBox_Set(Text1(P_cvass1AlR), b)
    '--- 5.5 Altro ----
    Call TextBox_Set(Text1(P_cvMat0Min), b)
    Call TextBox_Set(Text1(P_cvMat0MinR), b)
  Case P_cv6
    '****************************
    'Frame MISURA PRESTAZIONI (6)
    '****************************
    '--- 6.1 Scaglioni IRPEF ----
    Call TextBox_Set(Text1(P_cvScA), b)
    Call TextBox_Set(Text1(P_cvAlA), b)
    Call TextBox_Set(Text1(P_cvScB), b)
    Call TextBox_Set(Text1(P_cvAlB), b)
    Call TextBox_Set(Text1(P_cvScC), b)
    Call TextBox_Set(Text1(P_cvAlC), b)
    Call TextBox_Set(Text1(P_cvScD), b)
    Call TextBox_Set(Text1(P_cvAlD), b)
    Call TextBox_Set(Text1(P_cvScE), b)
    Call TextBox_Set(Text1(P_cvAlE), b)
    Call TextBox_Set(Text1(P_cvScF), b)
    Call TextBox_Set(Text1(P_cvAlF), b)
    Call TextBox_Set(Text1(P_cvScG), b)
    Call TextBox_Set(Text1(P_cvAlG), b)
    '--- 6.2 Pensione ----
    Call TextBox_Set(Text1(P_cvPMin), b)
    Call TextBox_Set(Text1(P_cvNMR), b)
    Call TextBox_Set(Text1(P_cvNTR), b)
    '--- 6.3 Supplementi di pensione ----
    Call TextBox_Set(Text1(P_cvSpAnni), b)
    Call TextBox_Set(Text1(P_cvSpEtaMax), b)
    Call TextBox_Set(Text1(P_cvSpPrcCon), b)
    'Call TextBox_Set(Text1(P_cvSpPrcRiv), b)
    '--- 6.4 Reversibilit� superstiti ----
    Call TextBox_Set(Text1(P_cvAv), b)
    Call TextBox_Set(Text1(P_cvAvo), b)
    Call TextBox_Set(Text1(P_cvAvoo), b)
    Call TextBox_Set(Text1(P_cvAo), b)
    Call TextBox_Set(Text1(P_cvAoo), b)
    Call TextBox_Set(Text1(P_cvAooo), b)
    'Call TextBox_Set(Text1(P_AGEN), b)
    'Call TextBox_Set(Text1(P_AFRA), b)
  Case P_cv7
    '************************
    'Frame NUOVI ISCRITTI (7)
    '************************
    If b = False Then
      Call MsgBox("TODO Check1_Click-7")
      Stop
    End If
  End Select
End Sub


'--- 20150505LC (inizio)
Private Sub cbxQav_Click()
  If p_bLoading Then Exit Sub
  p_bDataChanged = True
End Sub
Private Sub cbxQae_Click()
  If p_bLoading Then Exit Sub
  p_bDataChanged = True
End Sub
'--- 20150505LC (fine)
Public Sub Combo1_Click(Index As Integer)
  Dim szErr$ '20120607LC
  
  szErr = ""
  Select Case Index
  Case P_ID
    p_bLoading = True
    '20121019LC (inizio)
    'glo.szGruppo = Combo1(Index).Text
    '20120607LC (inizio)
    'szErr = TAppl_ParamDbGet(p_appl, glo.db, glo.szGruppo, "I")
    'If szErr <> "" Then
    '  call MsgBox(szErr, vbExclamation)
    'Else
    '  Call TAppl_DatiPut(p_appl, Me)
    'End If
    ''201200607LC (fine)
    'Call TAppl_DatiPut(p_appl, Me)
    szErr = MdiAfp.p_afp.szGruppo_Let(Me.Combo1(Index).Text, p_req, Me)  '20130307LC
    If szErr <> "" Then
      Call MsgBox(szErr, vbExclamation)
    End If
    '20121019LC (fine)
    p_bLoading = False
    p_bDataChanged = False
  Case P_LAD
    If p_bLoading Then Exit Sub
    p_bDataChanged = True
    Combo1(P_LED).Text = Combo1(P_LAD).Text
  Case P_LPD
    If p_bLoading Then Exit Sub
    p_bDataChanged = True
    Combo1(P_LSD).Text = Combo1(P_LPD).Text
  Case P_LRA
    If p_bLoading Then Exit Sub
    p_bDataChanged = True
    Combo1(P_LRN).Text = Combo1(P_LRA).Text
  Case P_niP
    'Check1(P_niP).Enabled = cbxNip.ListIndex > 0
    '20120319LC (inizio)
    Check1(P_niP).Enabled = False 'cbxNip.ListIndex > 0
    Check1(P_niP).Value = IIf(UCase(Left(cbxNip.Text, 1)) = "A", vbChecked, vbUnchecked)
    '20120319LC (fine)
  '--- 20140604LC (inizio)
    p_bDataChanged = True
  Case P_niStat, P_neStat, P_nvStat '20150527LC
    p_bDataChanged = True
  '--- 20140604LC (fine)
  Case Else
    If p_bLoading Then Exit Sub
    p_bDataChanged = True
  End Select
End Sub


Public Sub Command2_Click(Index As Integer)
  With MdiAfp.CommonDialog1
    Select Case Index
    Case 5
      .FileName = Text1(P_FOUT)
      .Filter = "File di testo (*.txt)|*.txt|Tutti i file (*.*)|*.*"
      .FilterIndex = 1
      .ShowOpen
      Text1(P_FOUT) = .FileName
    End Select
  End With
End Sub


Private Sub Form_Load()
  Call MdiAfp.p_afp.frmElab_Load(p_req, Me) '20130307LC  'ex 20121019LC
End Sub


Public Sub Form_Resize()
  If WindowState = vbMinimized Then Exit Sub
  If ON_ERR_RN = True Then On Error Resume Next
  With TabStrip1
    .Move 0, 0, Me.ScaleWidth, Me.ScaleHeight
    'Picture1(0).Move .Left + 120, .Top + 360, .Width - 240, .Height - 480
    Me.Frame1(1).Move 60, 360, Me.ScaleWidth - 120, Me.ScaleHeight - 420
    Me.Frame1(2).Move 60, 360, Me.ScaleWidth - 120, Me.ScaleHeight - 420
    Me.Frame1(3).Move 60, 360, Me.ScaleWidth - 120, Me.ScaleHeight - 420
    Me.Frame1(4).Move 60, 360, Me.ScaleWidth - 120, Me.ScaleHeight - 420
    Me.Frame1(5).Move 60, 360, Me.ScaleWidth - 120, Me.ScaleHeight - 420
    Me.Frame1(6).Move 60, 360, Me.ScaleWidth - 120, Me.ScaleHeight - 420
    Me.Div7.Move 60, 360, Me.ScaleWidth - 120, Me.ScaleHeight - 420
    Me.Div8.Move 60, 360, Me.ScaleWidth - 120, Me.ScaleHeight - 420
    Me.rtfOutput.Move 60, 60, Me.Div8.Width - 120, Me.Div8.Height - 120
  End With
  On Error GoTo 0
End Sub


Public Sub Elabora(iop&)
  '20121019LC
  Call MdiAfp.p_afp.Elabora1(p_req, Me, iop)
End Sub



