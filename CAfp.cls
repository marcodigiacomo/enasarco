VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CAfp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'*** Modifiche ***
'1) Contributi attivi anni successivi: OK
'2) Anomalia anni succ x passaggio a volontario (troppo bassi): OK
'2a)         anni succ x passaggio a silente (andava bene?)
'3) prob. di morte per pass. a vol. o silente


Private p_glo As TGlobale
Private p_appl As TAppl
'// Supporto ASP (inizio)
Private Type TSession
  '--- Navigazione ---
  iErr As Long
  szErr As String
  szIop As String
  szAction As String
  '--- Utente ---
  utUser As String
  utID As Long
End Type
Private p_ts As TSession
'--- 20130308LC (inizio)
Private p_bMaskInit1 As Boolean
Private p_bMaskInit2 As Boolean
Private p_cbCCR$
Private p_cbLS$
Private p_cbNI$
Private p_cbNIP$
Private p_cbPF$
Private p_cbPrm$
Private p_cbQ$
'--- 20130308LC (fine)
'// Supporto ASP (fine)


Public Function Init(mi&, MdiAfp As Object)
  Dim sErr$ '20120927LC
  
  sErr = ""
  sErr = modAfpTGlo.TGlo_Init(p_glo, mi, MdiAfp)
  Init = sErr
End Function


'20130308LC (tutta)
Public Function MaskInit1(frm As Object) As String
  Dim sErr$, sGruppo$ '20130308LC
  
  sErr = ""
  Call TAppl_Init(p_appl, p_glo)
  If p_bMaskInit1 = False Then
    If p_glo.MJ = MI_VB Then
      sErr = modAfpServer.Param_MaskLoad_Prm(p_glo.MJ, p_glo.db, frm)
    ElseIf p_glo.MJ = MI_ASP Then
      sErr = modAfpServer.Param_MaskLoad_Prm(p_glo.MJ, p_glo.db, p_cbPrm)
    End If
'p_cbPrm = "<option value='base'>base</option><option value='z'>z</option><option value='2009_fm'>2009_fm</option><option value='z514c10'>z514c10</option>"  '20130306LC
    p_bMaskInit1 = True
  End If
  MaskInit1 = sErr
End Function


Public Function MaskInit2(frm As frmElab) As String
'20130308LC (tutta)
  Dim sErr$, sGruppo$ '20130308LC
  
  sErr = ""
  If p_bMaskInit2 = False Then
    If p_glo.MJ = MI_VB Then
      sErr = modAfpServer.Param_MaskLoad_CCR(p_glo.MJ, p_glo.db, frm)
      'If sErr = "" Then sErr = modAfpServer.Param_MaskLoad_IRR(p_glo.MJ, p_glo.db, frm)
      If sErr = "" Then sErr = modAfpServer.Param_MaskLoad_LS(p_glo.MJ, p_glo.db, frm)
      If sErr = "" Then sErr = modAfpServer.Param_MaskLoad_NI(p_glo.MJ, p_glo.db, frm)
      If sErr = "" Then sErr = modAfpServer.Param_MaskLoad_NIP(p_glo.MJ, p_glo.db, frm)
      If sErr = "" Then sErr = modAfpServer.Param_MaskLoad_PF(p_glo.MJ, p_glo.db, frm)
      If sErr = "" Then sErr = modAfpServer.Param_MaskLoad_Q(p_glo.MJ, p_glo.db, frm, "")
    ElseIf p_glo.MJ = MI_ASP Then
      sErr = modAfpServer.Param_MaskLoad_CCR(p_glo.MJ, p_glo.db, p_cbCCR)
      'If sErr = "" Then sErr = modAfpServer.Param_MaskLoad_IRR(p_glo.MJ, p_glo.db, p_cbIRR)
      If sErr = "" Then sErr = modAfpServer.Param_MaskLoad_LS(p_glo.MJ, p_glo.db, p_cbLS)
      If sErr = "" Then sErr = modAfpServer.Param_MaskLoad_NI(p_glo.MJ, p_glo.db, p_cbNI)
      If sErr = "" Then sErr = modAfpServer.Param_MaskLoad_NIP(p_glo.MJ, p_glo.db, p_cbNIP)
      If sErr = "" Then sErr = modAfpServer.Param_MaskLoad_PF(p_glo.MJ, p_glo.db, p_cbPF)
      If sErr = "" Then sErr = modAfpServer.Param_MaskLoad_Q(p_glo.MJ, p_glo.db, Nothing, p_cbQ)
    End If
'p_cbCCR = "<option value=''>--</option><option value='base'>base</option><option value='base2010'>base2010</option><option value='nist_var'>nist_var</option>"  '20130306LC
'p_cbLS = "<option value=''>--</option><option value='lsaa_09'>lsaa_09</option><option value='ls2010p'>ls2010p</option><option value='zzz'>zzz</option>"
'p_cbNI = "<option value=''>--</option><option value='niso10_b'>niso10_b</option>"
'p_cbNIP = "<option value=''>--</option><option value='a_2012a5'>a2012_a5</option>"
'p_cbPF = "<option value=''>--</option><option value='pf2_fe'>pf2_fe</option>"
'p_cbQ = "<option value=''>--</option><option value='q09aw'>q09aw</option><option value='q09inv'>q09inv</option><option value='q09x_att'>q09x_att</option><option value='q09x_pen'>q09x_pen</option><option value='q10x'>q10x</option><option value='rgs48b'>rgs48b</option><option value='zzz'>zzz</option>"  '20130306LC
    p_bMaskInit2 = True
  End If
  MaskInit2 = sErr
End Function


'20130307LC (tutta)
Public Property Get cbCCR() As String
  cbCCR = p_cbCCR
End Property


'20130307LC (tutta)
Public Property Get cbLS() As String
  cbLS = p_cbLS
End Property


'20130307LC (tutta)
Public Property Get cbNI() As String
  cbNI = p_cbNI
End Property


'20130307LC (tutta)
Public Property Get cbNIP() As String
  cbNIP = p_cbNIP
End Property


'20130307LC (tutta)
Public Property Get cbPF() As String
  cbPF = p_cbPF
End Property


'20130307LC (tutta)
Public Property Get cbPrm() As String
  cbPrm = p_cbPrm
End Property


'20130307LC (tutta)
Public Property Get cbQ() As String
  cbQ = p_cbQ
End Property


Public Sub TabStrip1_Click(frm As frmElab)
'ex 20080614LC
'20130307LC
  If frm.p_bLoading Then Exit Sub
  If frm.TabStrip1.SelectedItem.Index = F_NUOVI_ISCRITTI Then
    frm.Div7.ZOrder
  ElseIf frm.TabStrip1.SelectedItem.Index < F_RISULTATI Then
    frm.Frame1(frm.TabStrip1.SelectedItem.Index).ZOrder
  Else
    frm.rtfOutput.Text = p_appl.SuddivReport(frm.TabStrip1.SelectedItem.Index - F_RISULTATI)
    frm.Div8.ZOrder
  End If
End Sub


Public Property Get bDebug() As Byte
  bDebug = p_glo.bDebug
End Property
Public Property Let bDebug(x As Byte)
  p_glo.bDebug = x
End Property


Public Property Get Counter() As Long
  Counter = p_glo.Counter
End Property
Public Property Let Counter(x As Long)
  p_glo.Counter = x
End Property


Public Property Get nElab() As Long
  nElab = p_glo.nElab
End Property
Public Property Let nElab(x As Long)
  p_glo.nElab = x
End Property


Public Property Get OPZ_ELAB_CODA_Get() As String
  OPZ_ELAB_CODA_Get = OPZ_ELAB_CODA
End Property


Public Property Get tAcc() As Double
  tAcc = p_glo.tAcc
End Property
Public Property Let tAcc(x As Double)
  p_glo.tAcc = x
End Property


Public Property Get szGruppo() As String
  szGruppo = p_glo.szGruppo
End Property


'20130308LC   'ex 20121010LC
Public Function szGruppo_Let(x As String, req As Object, frm As Object) 'Supporto VB
  Dim szErr As String
  
  p_glo.szGruppo = x
  If p_glo.szGruppo = "" Then
    If p_glo.szGruppo = "" Then
      p_glo.szGruppo = "z514c10"
    End If
  End If
  szErr = TAppl_ParamDbGet(p_appl, p_glo, p_glo.db, p_glo.szGruppo, "I")
  If szErr = "" Then
    Call TAppl_DatiPut(p_appl, p_glo, req, frm)  '20130307LC
  Else
    p_glo.szGruppo = ""
  End If
  szGruppo_Let = szErr
End Function


Public Sub ParamSalva(frm As Object)
  Call TAppl_ParamSalva(p_appl, p_glo, frm)
End Sub


Public Sub Interrompi()
  p_appl.bStop = True
  DoEvents
End Sub


Public Sub frmElab_Load(req As Collection, frm As frmElab)  '20130307LC
  Dim fo&, iTS&
  Dim sErr$
  'Dim req As Collection  '20130307LC (commentato)
  
  '--- 20150522LC (inizio)
  '****************
  'Legge le opzioni
  '****************
  sErr = modAfpTGlo.TGlo_Opz_Init(p_glo)
  If sErr <> "" Then GoTo ExitPoint
'**************************
'Inizializza l'applicazione
'**************************
Call TAppl_Init(p_appl, p_glo)
ReDim p_appl.SuddivReport(OPZ_INT_SUDDIVISIONI_N)
ReDim p_appl.SuddivRipart(OPZ_INT_SUDDIVISIONI_N)
  '20121028LC (inizio)
  p_appl.nSize4 = (1 + TB_LAST)
  'appl.nSize3 = (1 + appl.Nmax2 + 1) * p_appl.nSize4
  p_appl.nSize3 = (1 + p_appl.nMax2 + 1 + p_appl.t0 - 1961) * p_appl.nSize4  '20121114LC
  p_appl.nSize2 = (1 + 4) * p_appl.nSize3
  p_appl.nSize1 = (1 + OPZ_INT_SUDDIVISIONI_N) * p_appl.nSize2
  ReDim p_appl.tb1(p_appl.nSize1 - 1)
  '20121028LC (fine)
  p_appl.idReport = p_glo.nElab
  '--- 20150522LC (fine)
  '20080614LC (inizio)
  '*********************
  'Inizializza la pagina
  '*********************
  '--- 20161111LC (inizio)
  frm.txtIter.Text = 0
  frm.txtCvNome.Text = LCase(OPZ_CV_NOME)
  '--- 20161111LC (fine)
  '**************************************
  'Aggiunge le schede per le suddivisioni
  '**************************************
  frm.p_bLoading = True
  If OPZ_INT_SUDDIVISIONI_N > 1 Then
    For iTS = 0 To OPZ_INT_SUDDIVISIONI_N - 1
      frm.TabStrip1.Tabs.Add
      frm.TabStrip1.Tabs.Item(frm.TabStrip1.Tabs.Count).Caption = OPZ_INT_SUDDIVISIONI_nomi(iTS)
    Next iTS
  End If
  frm.p_bLoading = False
  '20080614LC (fine)
  '--- 20150504LC (inizio)
  '*******************
  'Inizializza il form
  '*******************
  '--- 1.1 Bilancio tecnico ----
  frm.Label1(P_btPassSIV).Caption = "Passivi societ� di capitali"
  frm.Label1(P_btPassPFV).Caption = ""
  frm.Label1(P_btPassPFV).Visible = False
  frm.Text1(P_btPassPFV).Visible = False
  frm.Text1(P_btPassPFP).Visible = False
  '--- 2.1 Tassi di valutazione e rivalutazione ---
  '--- 2.2 Tassi di rivalutazione dei montanti contributivi ---
  '--- 20161115LC (inizio)  'ex 20161115LC  'ex 20150615LC
  frm.Label1(P_cvTrmc0).Caption = "Rapporto Contributi/Pensioni"              '25
  frm.Label1(P_cvTrmc0).Visible = True
  frm.Text1(P_cvTrmc0).Visible = True
  '---
  'frm.Label1(P_cvTrmc1).Caption = "Contributi quota B"                       '26
  frm.Label1(P_cvTrmc1).Visible = False
  frm.Text1(P_cvTrmc1).Visible = False
  '---
  'frm.Label1(P_cvTrmc2).Caption = "Contributi quota C"                       '27
  frm.Label1(P_cvTrmc2).Visible = False
  frm.Text1(P_cvTrmc2).Visible = False
  '---
  'frm.Label1(P_cvTrmc3).Caption = "Contributi per supplementi di pensione"   '28
  frm.Label1(P_cvTrmc3).Visible = False
  frm.Text1(P_cvTrmc3).Visible = False
  '---
  'frm.Label1(P_cvTrmc4).Caption = "Contributi volontari"                     '29
  frm.Label1(P_cvTrmc4).Caption = "Riv. contributi obbligatori"              '29
  frm.Label1(P_cvTrmc4).Top = frm.Label1(P_cvTrmc1).Top
  frm.Text1(P_cvTrmc4).Top = frm.Text1(P_cvTrmc1).Top
  '---
  frm.Label1(P_cvTrmc5).Caption = "Riv. contributi PIL quinquennale"         '30
  frm.Label1(P_cvTrmc5).Top = frm.Label1(P_cvTrmc2).Top
  frm.Text1(P_cvTrmc5).Top = frm.Text1(P_cvTrmc2).Top
  '---
  frm.Label1(P_cvTrmc6).Caption = "Riv. contributi FIRR"                     '31
  frm.Label1(P_cvTrmc6).Top = frm.Label1(P_cvTrmc3).Top
  frm.Text1(P_cvTrmc6).Top = frm.Text1(P_cvTrmc3).Top
  '--- 20161111LC (fine)  'ex 20150615LC
  '--- 2.3 Tassi di rivalutazione di redditi e volumi d'affari ---
  frm.Frame1(23).Caption = "2.3 Tassi di rivalutazione delle provvigioni"
  frm.Label1(P_cvTs2p).Left = frm.Label1(P_cvTs2p).Left + 4200 - 3360
  frm.Label1(P_cvTs1a).Left = frm.Label1(P_cvTs1a).Left + 4200 - 3360
  frm.Text1(P_cvTs1a).Left = frm.Text1(P_cvTs1a).Left + 4200 - 3360
  frm.Label1(P_cvTs1p).Left = frm.Label1(P_cvTs1p).Left + 4200 - 3360
  frm.Text1(P_cvTs1p).Left = frm.Text1(P_cvTs1p).Left + 4200 - 3360
  frm.Label1(P_cvTs1n).Left = frm.Label1(P_cvTs1n).Left + 4200 - 3360
  frm.Text1(P_cvTs1n).Left = frm.Text1(P_cvTs1n).Left + 4200 - 3360
  '---
  frm.Label1(P_cvTs2a).Visible = False
  frm.Text1(P_cvTs2a).Visible = False
  frm.Text1(P_cvTs2p).Visible = False
  frm.Text1(P_cvTs2n).Visible = False
  '--- 2.4 Rivalutazione delle pensioni ---
  '--- 2.5 Altre stime ----
  frm.Label1(P_cvPrcPTattM).Visible = False
  frm.Text1(P_cvPrcPTattM).Visible = False
  frm.Text1(P_cvPrcPTattF).Visible = False
  '--- 3.2 Altre probabilit� ----
  frm.lblQae.Caption = "Silenti da attivi"
  '--- 4.1 Pensione di vecchiaia ordinaria ----
  '--- 4.2 Pensione di vecchiaia anticipata ----
  '--- 4.3 Pensione di vecchiaia posticipata ----
  '--- 4.4 Pensione di inabilit� ----
  '--- 4.5 Pensione di invalidit� ----
  '--- 4.6 Pensione indiretta ----
  frm.Frame1(46).Caption = "4.6 Pensione ai superstiti"
  '--- 4.7 Restituzione dei contributi ----
  frm.Frame1(47).Caption = "4.7 Rendita contributiva"
  '--- 4.8 Totalizzazione ----
  frm.Frame1(48).Visible = False
  '--- 5.1 Contribuzione ridotta ----
  frm.Frame1(51).Visible = False
  '--- 5.2 Soggettivo ----
  'frm.Frame1(52).Top = frm.Frame1(51).Top
  frm.Label1(P_cvSogg1AlR).Caption = "mono"
  frm.Label1(P_cvSogg0MinR).Caption = "pluri"
  '--- 20150615LC (inizio)
  frm.Label1(P_cvSogg2Al).Visible = False
  frm.Text1(P_cvSogg2Al).Visible = False
  frm.Text1(P_cvSogg2AlR).Visible = False
  '---
  frm.Label1(P_cvSogg2Max).Visible = False
  frm.Text1(P_cvSogg2Max).Visible = False
  frm.Text1(P_cvSogg2MaxR).Visible = False
  '---
  frm.Label1(P_cvSogg3Al).Visible = False
  frm.Text1(P_cvSogg3Al).Visible = False
  frm.Text1(P_cvSogg3AlR).Visible = False
  '--- 20150615LC (fine)
  frm.Label1(P_cvSoggArt25).Visible = False
  frm.Text1(P_cvSoggArt25).Visible = False
  '--- 5.3 Integrativo / FIRR ----
  frm.Label1(P_cvAss0MinR).Caption = "mono"
  frm.Label1(P_cvass1AlR).Caption = "pluri"
  frm.Label1(P_cvIntArt25).Visible = False
  frm.Text1(P_cvIntArt25).Visible = False
  '--- 5.4 Assistenziale ----
  frm.Label1(P_cvInt0MinR + 0).Caption = "mono"  '20160422LC
  frm.Label1(P_cvInt0MinR + 2).Caption = "pluri"  '20160422LC
  '--- 5.5 Altro ----
  frm.Label1(P_cvMat0MinR).Caption = "mono"
  frm.Label1(P_cvMat0MinR + 1).Caption = "pluri"
  '--- 6.1 Scaglioni IRPEF ----
  frm.Frame1(61).Visible = False
  '--- 6.2 Pensione ----
  frm.Frame1(62).Visible = False
  '--- 6.3 Supplementi di pensione ----
  frm.Frame1(63).Left = frm.Frame1(61).Left
  '--- 6.4 Reversibilit� superstiti ----
  '--- 20150504LC (fine)
  '**************************
  'Legge le basi demografiche
  '**************************
  If p_glo.nElab < 1 Then
    '--- 20130307LC (inizio)
    sErr = modAfpServer.Param_MaskLoad_Prm(p_glo.MJ, p_glo.db, frm)
    If sErr = "" Then sErr = modAfpServer.Param_MaskLoad_CCR(p_glo.MJ, p_glo.db, frm)
    'If sErr = "" Then sErr = modAfpServer.Param_MaskLoad_IRR(p_glo.MJ, p_glo.db, frm)
    If sErr = "" Then sErr = modAfpServer.Param_MaskLoad_LS(p_glo.MJ, p_glo.db, frm)
    If sErr = "" Then sErr = modAfpServer.Param_MaskLoad_NI(p_glo.MJ, p_glo.db, frm)
    If sErr = "" Then sErr = modAfpServer.Param_MaskLoad_NIP(p_glo.MJ, p_glo.db, frm)
    If sErr = "" Then sErr = modAfpServer.Param_MaskLoad_PF(p_glo.MJ, p_glo.db, frm)
    If sErr = "" Then sErr = modAfpServer.Param_MaskLoad_Q(p_glo.MJ, p_glo.db, frm, "")
    '--- 20130307LC (fine)
    If sErr = "" Then
      p_glo.szGruppo = "base"
      'If glo.szGruppo = "" Then glo.szGruppo = "base"
      frm.Combo1(P_ID).Text = p_glo.szGruppo
      frm.Combo1(P_ID).Visible = True
      frm.Text1(P_ID).Visible = False
    End If
  Else
    frm.p_bLoading = True
    '20080825LC (inizio)
    'sErr = ParametriMaskLoad1(glo.db, Combo1(P_ID))
    '--- 20130307LC (inizio)
    'sErr = modAfpServer.Param_MaskLoad_Prm(p_glo.MJ, p_glo.db, frm)
    If sErr = "" Then sErr = modAfpServer.Param_MaskLoad_CCR(p_glo.MJ, p_glo.db, frm)
    'If sErr = "" Then sErr = modAfpServer.Param_MaskLoad_IRR(p_glo.MJ, p_glo.db, frm)
    If sErr = "" Then sErr = modAfpServer.Param_MaskLoad_LS(p_glo.MJ, p_glo.db, frm)
    If sErr = "" Then sErr = modAfpServer.Param_MaskLoad_NI(p_glo.MJ, p_glo.db, frm)
    If sErr = "" Then sErr = modAfpServer.Param_MaskLoad_NIP(p_glo.MJ, p_glo.db, frm)
    If sErr = "" Then sErr = modAfpServer.Param_MaskLoad_PF(p_glo.MJ, p_glo.db, frm)
    If sErr = "" Then sErr = modAfpServer.Param_MaskLoad_Q(p_glo.MJ, p_glo.db, frm, "")
    '--- 20130307LC (fine)
    If sErr = "" Then
      sErr = TAppl_ReportDbGet(p_appl, p_glo, p_appl.tb1, p_glo.db)
    End If
    If sErr = "" Then
      p_glo.szGruppo = p_appl.parm(P_ID)
      frm.Text1(P_ID).Text = p_glo.szGruppo
      frm.Text1(P_ID).Visible = True
      frm.Combo1(P_ID).Visible = False
      Call TAppl_DatiPut(p_appl, p_glo, req, frm)  '20130307LC
    End If
    frm.p_bLoading = False
    frm.p_bDataChanged = False
    '****************
    'Stampa il report
    '****************
    '--- 20161112LC (inizio)
    If sErr = "" Then
      sErr = TAppl_CreaReportt(p_appl, p_glo, p_appl.tb1, frm)
    End If
    '--- 20161112LC (fine)
    If sErr <> "" Then  '20170309LC
      Call MsgBox("Si � verificato il seguente errore:" & vbCrLf & vbCrLf & sErr)
    End If
    '20080825LC (fine)
  End If
  
ExitPoint:  '20150522LC
  If sErr = "" Then '20121021LC
    p_glo.Counter = p_glo.Counter + 1
    frm.Caption = "AFP" & p_glo.Counter
    ''''Call frm.Command1_Click(2)
    If p_glo.nElab < 1 Or sErr <> "" Then
      frm.TabStrip1.SelectedItem = frm.TabStrip1.Tabs(F_PARAM)
    Else
      frm.TabStrip1.SelectedItem = frm.TabStrip1.Tabs(F_RISULTATI)
      '20080825LC
      'rtf1(0).SetFocus
      frm.rtfOutput.SelStart = 0
    End If
    frm.WindowState = vbMaximized
  Else
    frm.Tag = sErr
  End If
End Sub


Public Function Elabora1(req As Object, frm As frmElab, iop&) As String '//Supporto VB
'20130307LC
'20150522LC  iop (0:AFP, 1:ENEA)
  Dim bElab As Boolean
  Dim nItRand&
  Dim s$, sFilename$, szErr$ '20130604LC
  'Dim req As Collection  '20130307LC (commentato)
  
  szErr = ""
  If Screen.MousePointer = vbHourglass Then Exit Function
  If p_glo.nElab <> 0 Then
    'p_glo.nElab = 0
  End If
  p_glo.tElab = Timer
  p_glo.dElabIn = Now
  p_glo.tAcc = 0
  p_glo.tElab = 0
  p_glo.tMax = 0
  p_glo.tMin = 0
  p_glo.tMed = 0
  Screen.MousePointer = vbHourglass
  nItRand = 1
  If nItRand < 1 Or nItRand > 100000 Then nItRand = 1
  ReDim p_appl.SuddivReport(OPZ_INT_SUDDIVISIONI_N)
  ReDim p_appl.SuddivRipart(OPZ_INT_SUDDIVISIONI_N)
  '--- 20140424LC (inizio)  'ex 20130609LC
  If OPZ_INT_PMIN_DEBUG <> 0 Then
    sFilename = "pmin_" & AFP_SVER1 & ".txt"
    Open sFilename For Output As #99
    s = "mat; t; x; h; tp; stp; gr0; yd; pens"
    s = s & "; prc_pc; Pretr; Pcontr; ax; ax0; bPmin; nAbbPmin"
    s = s & "; nRMPmin; RMPmin; PminBase; PminAbb; PminCalc; Psost"
    's = Replace(s, ";", vbTab)
    Print #99, s
  '--- 20160420LC (inizio)
  ElseIf OPZ_INT_PENS_DEBUG = 1 Then
    sFilename = "pens_debug.csv"
    Open sFilename For Output As #99
    s = "mat; t; x; h; tp; stp; gr0; yd; pens"
    s = s & "; prc_pc; Pretr; Pcontr; ax; ax0"
    's = Replace(s, ";", vbTab)
    Print #99, s
  '--- 20160420LC (fine)
  End If
  '--- 20140424LC (fine)
  '--- 20160306LC (inizio)  'ex 20150522LC (ENEA)
  'If iop = 0 Then
    If TAppl_Elab32(p_appl, p_glo, p_appl.tb1, nItRand, 3, req, frm) Then
      frm.TabStrip1.SelectedItem = frm.TabStrip1.Tabs(F_RISULTATI)
      'frm.rtf1(0).SetFocus
    End If
  'Else
  '  Call TAppl1_BilTecn(1, -1, 1, frm)
  '  frm.Div8.ZOrder
  'End If
  '--- 20160306LC (fine)  'ex 20150522LC (ENEA)
  '--- 20140424LC (inizio)  'ex 20130604LC
  If OPZ_INT_PMIN_DEBUG <> 0 Or OPZ_INT_PENS_DEBUG = 1 Then  '20160420LC
    Close #99
  End If
  '--- 20140424LC (fine)
  '20080907LC (inizio)
  If p_glo.rsDbg1.State = 1 Then p_glo.rsDbg1.Close
  If p_glo.rsDbg2.State = 1 Then p_glo.rsDbg2.Close
  '20080907LC (fine)
  Screen.MousePointer = vbDefault
ExitPoint:
  Elabora1 = szErr
End Function


Public Function Elabora2(req As Object, frm As Object) As String  '//Supporto ASP
  Dim bElab As Boolean
  Dim szErr$
  Dim nItRand&
  If ON_ERR_EH = True Then On Error GoTo ErrorHandler
  
  szErr = ""
  If Screen.MousePointer = vbHourglass Then Exit Function
  p_glo.tElab = Timer
  p_glo.dElabIn = Now
  p_glo.tAcc = 0
  p_glo.tElab = 0
  p_glo.tMax = 0
  p_glo.tMin = 0
  p_glo.tMed = 0
  Screen.MousePointer = vbHourglass
  nItRand = 1
  If nItRand < 1 Or nItRand > 100000 Then nItRand = 1
'Call TAppl_Init(p_appl)
'szErr = Me.szGruppo_Let(p_glo.szGruppo, req, frm)
  ReDim p_appl.SuddivReport(OPZ_INT_SUDDIVISIONI_N)
  ReDim p_appl.SuddivRipart(OPZ_INT_SUDDIVISIONI_N)
  '20101010LC (inizio)
  '--- 201300306LC (inizio)
  szErr = IIf(TAppl_Elab32(p_appl, p_glo, p_appl.tb1, nItRand, 3, req, frm) = True, "", "Errore durante l'elaborazione")
  '--- 201300306LC (fine)
  If p_glo.MJ = MI_VB Then
    If szErr = "*" Then
      Call MsgBox("Operazione annullata", vbInformation)
    ElseIf szErr <> "" Then
      Call MsgBox(szErr, vbExclamation)
    Else
      frm.TabStrip1.SelectedItem = frm.TabStrip1.Tabs(F_RISULTATI)
      'rtf1(0).SetFocus
    End If
    '20101010LC (fine)
    '20080907LC (inizio)
    If p_glo.rsDbg1.State = 1 Then p_glo.rsDbg1.Close
    If p_glo.rsDbg2.State = 1 Then p_glo.rsDbg2.Close
    '20080907LC (fine)
  End If
  
ExitPoint:
  On Error GoTo 0
  Screen.MousePointer = vbDefault
  Elabora2 = szErr
  Exit Function
  
ErrorHandler:
  szErr = "Errore " & Err.Number & " (" & Hex(Err.Number) & ") nel modulo " & Err.Source & vbCrLf & Err.Description  '20161112LC
  GoTo ExitPoint
End Function


Public Sub OpzioniReport(req As Object, iop&)  '20130307LC
  Call modAfpUtil0.OpzioniReport(p_glo, req, iop)
End Sub


'////////////
'Supporto ASP
'////////////
Public Sub Login(req As Object, Res As Object)  '20130307LC
  With p_ts
    .iErr = 0
    .szErr = ""
    If LCase(req("cUser")) = "system" And LCase(req("cPwd")) = "prova" Then
      .utUser = LCase(req("cUser"))
      .utID = 1
    Else
      .utUser = ""
      .utID = 0
      .iErr = -1
      .szErr = "Utente non riconosciuto"
    End If
    Set Res = New Collection
    Res.Add CStr(.iErr), "iErr"
    Res.Add .szErr, "szErr"
    Res.Add .utID, "utID"
  End With
End Sub


Public Property Get SuddivReport(i%) As String
  SuddivReport = p_appl.SuddivReport(i)
End Property


Public Function Test() As String
  Static z&
  
  z = z + 1
  Test = CStr(z)
End Function


'20170724LC
'Equivalenza tra A18 e A19

'Gruppo 1: Attivi
'OPZ_RC_MODEL = Qualunque
'  OPZ_RC_ADEC = 0 o comunque < 1961
'  OPZ_RC_FRAZ = 1


