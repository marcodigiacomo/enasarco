Attribute VB_Name = "modAfpUtil0"
Option Explicit

'317,11 rec/sec  'TIscr2_MovPop_4-4
'305,28 rec/sec  'TIscr2_MovPop_4-3
'170,19 rec/sec  'TIscr2_MovPop_4-1
'168,90 rec/sec  'TIscr2_MovPop_9-1
'168,54 rec/sec  'TIscr2_MovPop_4-2
'168,17 rec/sec  'TIscr2_MovPop_Sup4-1
'167,80 rec/sec  'TIscr2_MovPop_4-5
'167,26 rec/sec  'TIscr2_MovPop_4-6

'387,51 rec/sec  '5  '(For t1...)
'175,19 rec/sec  '4  'TIscr2_Montante
'174,79 rec/sec  '6b 'TIscr2_MovPop_4b2
'172,85 rec/sec  '6d 'TIscr2_MovPop_Sup1b
'172,65 rec/sec  '1  'TIscr2_Aggiorna_Contr
'171,13 rec/sec  '6c 'TIscr2_MovPop_4c
'164,95 rec/sec  '2  'TIscr2_Aggiorna_Mont
'161,70 rec/sec  '3  'TIscr2_Aggiorna_Redd
'163,91 rec/sec  '6a 'TIscr2_MovPop_4b1
'159,54 rec/sec  '6e 'TIscr2_MovPop_Contributi
'159,04 rec/sec  '   'Base
'167,99
Public Const TEST_VEL = 7 '0 in produzione '387,51 (For t1...)
Public Const DATA_ZERO As Date = "30/12/1899"

Public Const OPZ_TB = 2 '20121028LC

'Public Const OPZ_AL95 = 0.95 '20150322LC (commentata) 'ex 0.95 in produzione '20120118LC
Public Const OPZ_TEST = 0            '0 in produzione  '20120114LC
Public Const OPZ_NOARR = 1           '0 in produzione  '20111211LC

Public Const IVERSCOMPAT = 1  '0 nella versione finale
Public Const TMP_TEST = 0     '0 nella versione finale

Public Const K_HMIN = 0
Public Const K_HMAX = 60
Public Const K_XMIN = 20
Public Const K_XMAX = 80
Public Const MAX_TC = 1 '20080307LC
Public Const NAP = 25
Public Const NAPM1 = NAP - 1
Public Const percMAX = 1 '100
Public Const RMAX = 9999999.99  'RMAX = 999999.99
Public Const TASSOMAX = 1
'20080926LC
'Public Const omega = 110
'Public Const xMax = omega - 20
Public Const xpen = 65
Public Const ZMAX = 999999.99

Public Const BT_QAD = 0
Public Const BT_QED = 1
Public Const BT_QPD = 2
Public Const BT_QSD = 3
Public Const BT_QBD = 4
Public Const BT_QID = 5
Public Const BT_QAB = 6
Public Const BT_QAI = 7
Public Const BT_QAV = 12
Public Const BT_QAE = 8
Public Const BT_QAW = 9
Public Const BT_PGEN = 10
Public Const BT_PFRA = 11
'Public Const BT_LMF = 11
Public Const BT_MAX = 12
'--- 20161112LC (inizio)
Public Const xlPasteValues As Long = -4163
Public Const xlNone As Long = -4142
'--- 20161112LC (fine)


Public Function ARR5Z#(y#, z#)
'20111211LC
  If OPZ_NOARR = 0 Then
    ARR5Z = Int(AA5 + y * z) / z
  Else
    ARR5Z = y
  End If
End Function


Public Function AX1(CoeffPA#(), iSex&, ix#, it&)
'20080416LC
'20140319LC  tutta
'20150421LC  tutta
  Dim sex&, xi&, t&
  Dim dx#, dy#, x#, y#
  Dim mesi As Double  'Mesi interamente trascorsi
  Dim y0 As Double    'Coefficiente all'et� intera
  Dim y1 As Double    'Coefficiente all'et� intera+1
  
  '--- t
  t = it
  If OPZ_PENS_C0 = 1 Then  '20161126LC
    t = t + 1
  End If
  If t <= LBound(CoeffPA, 3) Then
    t = LBound(CoeffPA, 3)
  ElseIf t >= UBound(CoeffPA, 3) Then
    t = UBound(CoeffPA, 3)
  End If
  '--- sex
  sex = iSex - 1
  If sex <> 0 Then
    sex = 1
  End If
  '--- x
  '--- 20150512LC (inizio)
  x = ix
  If OPZ_PENS_C0 = 1 Then  '20161126LC
    x = x + 1 / 360
  End If
  '--- 20150512LC (fine)
  x = Int(AA0 + x * 12) / 12  '20111217LC
  If x <= LBound(CoeffPA, 2) Then
    x = LBound(CoeffPA, 2)
  ElseIf x >= UBound(CoeffPA, 2) Then
    x = UBound(CoeffPA, 2)
  End If
  '--- et� intera / mesi trascorsi
  xi = Int(x)
  dx = x - xi
  If Math.Abs(x - xi) <= AA0 Then  '--- mai fidarsi di un floating point esattamente uguale ad un intero
    mesi = 0
  Else  'mesi interamente trascorsi
    mesi = Int(x * 12 + AA0) - xi * 12  '--- idem c.s., +AA0 rende "sicuro" l'arrotondamento di floating point
  End If
  '--- Coefficiente di trasformazione all'et� intera
  y0 = CoeffPA(sex, xi, t)
  '--- Coefficiente di trasformazione all'et� intera+1
  If xi < UBound(CoeffPA, 2) Then
    y1 = CoeffPA(sex, xi + 1, t)
  Else
    'y1 = y0  '--- y1 non verr� usato, si commenta l'assegnazione per sottolineare questo fatto
    dx = 0#
    mesi = 0#
  End If
  '--- Restituzione del coefficiente, interpolato alla 3 cifra decimale, eeventualmente interpolato
  If mesi > AA0 Then
    dy = (y1 - y0 + AA0) / 12#
    '--- Si interpola dapprima sull'incremento mensile al 3� decimale (come espressamente scritto nel regolamento)
    dy = ARR5(dy, 1000#)
    y = ARR5(y0 + dy * mesi, 1000#) / 100#
  Else
    y = ARR5(y0, 1000#) / 100# '--- Non occorre interpolare, y1 non viene usato
  End If
  '---
  AX1 = y
End Function


Public Sub ComboSelect(c As Variant, x As Variant, iop%)
  Dim i%
  
  For i = 0 To c.ListCount - 1
    If iop = 0 Then
      If c.ItemData(i) = x Then
        c.ListIndex = i
        Exit Sub
      End If
    ElseIf iop = 1 Then
      If UCase(c.List(i)) = UCase(x) Then
        c.ListIndex = i
        Exit Sub
      End If
    End If
  Next i
  c.ListIndex = -1
End Sub


'20130211LC
Public Function DataNasc(d0 As Date, giorno&) As Date
  Dim d1 As Date
  
  If giorno < 1 Then
    d1 = d0
  Else
    d1 = "01/" & Format(d0, "mm/yyyy")
    If giorno > 28 Then
      d1 = DateAdd("m", 1, d1) - 1
    ElseIf giorno > 0 Then
      d1 = DateAdd("d", giorno - 1, d1)
    End If
  End If
  DataNasc = d1
End Function


Public Function dmax#(a As Double, b As Double)
  If a > b Then dmax = a Else dmax = b
End Function


Public Function dmin#(a As Double, b As Double)
  If a < b Then dmin = a Else dmin = b
End Function


'20080611LC - da PICO
Public Function dxRend&(iAgeShift&, iSesso&, anasc&)
  Dim dx&
  
  Select Case iAgeShift
  Case 1948, -1948
    Select Case iSesso
    Case 2
      Select Case anasc
      Case Is <= 1943: dx = 1
      Case Is <= 1950: dx = 0
      Case Is <= 1964: dx = -1
      Case Else
        If iAgeShift > 0 Then
          dx = -2
        Else
          dx = Int((anasc - 1964 - 1) / 12) - 2
        End If
      End Select
    Case Else
      Select Case anasc
      Case Is <= 1941: dx = 1
      Case Is <= 1951: dx = 0
      Case Is <= 1965: dx = -1
      Case Else
        If iAgeShift > 0 Then
          dx = -2
        Else
          dx = Int((anasc - 1964 - 1) / 12) - 2
        End If
      End Select
    End Select
    dx = dx + OPZ_ABB_QM_PRM_1
  Case 1955, -1955
    Select Case iSesso
    Case 2
      Select Case anasc
      Case Is <= 1927: dx = 3
      Case Is <= 1940: dx = 2
      Case Is <= 1949: dx = 1
      Case Is <= 1962: dx = 0
      Case Is <= 1972: dx = -1
      Case Else
        If iAgeShift > 0 Then
          dx = -2
        Else
          dx = Int((anasc - 1972 - 1) / 12) - 2
        End If
      End Select
    Case Else
      Select Case anasc
      Case Is <= 1925: dx = 3
      Case Is <= 1938: dx = 2
      Case Is <= 1947: dx = 1
      Case Is <= 1960: dx = 0
      Case Is <= 1970: dx = -1
      Case Else
        If iAgeShift > 0 Then
          dx = -2
        Else
          dx = Int((anasc - 1970 - 1) / 12) - 2
        End If
      End Select
      dx = dx + OPZ_ABB_QM_PRM_1
    End Select
  Case Else
    dx = 0
  End Select
  dxRend = dx
End Function


'20121209LC (obsoleto)
Public Sub z_ElaboraZCB(db As ADODB.Connection, tasso#, bZCB As Boolean, zcbNomeA$, zcbNomeV$, ZCB#())
  '******************************
  'Inizializzazione dei tassi zcb
  '******************************
  Const NZCB = 50
  Dim i%, it%, j%, n%, t%, x%, x0%, x1%
  Dim slope#, y0#, y1#
  Dim SQL$
  Dim rs As New ADODB.Recordset
  Dim y(NZCB, 2) As Double
  ReDim ZCB#(NZCB - 1, NZCB - 1)
  
  '************************************************************
  'Leggo i tassi swap in acquisto e vendita distinti per durata
  '************************************************************
  If bZCB Then
    For it = 1 To 2
      SQL = "SELECT irrN,irrTasso FROM ALM_IRR"
      SQL = SQL & " WHERE irrNome='" & UCase(IIf(it = 1, zcbNomeA, zcbNomeV)) & "'" '20121024LC
      SQL = SQL & " ORDER BY irrN"
      rs.Open SQL, db, adOpenForwardOnly, adLockReadOnly
      x1 = 0
      Do Until rs.EOF
        x0 = x1
        y0 = y1
        x1 = rs(0)
        y1 = rs(1)
        If x0 < 1 Then
          y(x1, it) = y1
        Else
          slope = (y1 - y0) / (x1 - x0)
          For x = x0 + 1 To x1
            If x > NZCB Then Exit Do
            y(x, it) = y(x - 1, it) + slope
          Next x
        End If
        rs.MoveNext
      Loop
      rs.Close
      If x < NZCB Then
        x0 = x1
        y0 = y1
        x1 = NZCB
        For x = x0 + 1 To x1
          y(x, it) = y(x - 1, it) + slope
        Next x
      End If
    Next it
  Else
    For x = 1 To NZCB
      y(x, 1) = tasso
      y(x, 2) = tasso
    Next x
  End If
  '***********************************
  'Costruzione della matrice dei tassi
  '***********************************
  For i = 0 To NZCB - 1
    ZCB(i, 0) = (y(i + 1, 1) + y(i + 1, 2)) / 2
    For j = 0 To i - 1
      ZCB(i, j + 1) = ((1 + ZCB(i, 0)) ^ (i + 1) / (1 + ZCB(j, 0)) ^ (j + 1)) ^ (1 / (i - j)) - 1
    Next j
  Next i
End Sub


Public Sub fclose(fh As Long)
  Close #fh
End Sub


Public Function fd$(ByVal x As Double, ByVal szFmt As String)
    Dim i As Long
  Dim n As Long
  Dim sz As String
  
  '*******************************
  'Trasforma la stringa di formato
  '*******************************
  sz = ""
  n = Len(szFmt)
  If Right(szFmt, 1) = "%" Then
    szFmt = Left(szFmt, n - 1)
    sz = "%"
  End If
  
  For i = 1 To n
    Select Case Mid$(szFmt, i, 1)
    Case "."
      If i < Len(szFmt) Then
        Mid(szFmt, i + 1, n - i) = String(n - i, "0")
      End If
      Exit For
    End Select
  Next i
  sz = Format(x, LTrim(szFmt)) & sz
  If Trim(sz) = "" Then
    sz = "0"
  Else
    i = InStr(sz, ",")
    If i > 0 Then
      Mid(sz, i, 1) = "."
      If i = 1 Then sz = "0" & sz
    End If
    i = InStr(sz, "A")
    If i > 0 Then Mid(sz, i, 1) = "%"
  End If
  
  If Len(sz) < n Then
    sz = Space(n - Len(sz)) & sz
  ElseIf Len(sz) > n Then
    sz = "%" & sz
  End If
  fd = sz
End Function


Public Function ffm1(rs As Variant) As Variant
  Dim y#  '20140125LC
  
  If VarType(rs) = vbNull Then
    ffm1 = -1#
  Else
    '--- 20140125LC (inizio)
    y = rs
    If y = -2# Or y = -3# Then
      y = -1#
    End If
    ffm1 = y
    '--- 20140125LC (fine)
  End If
End Function


Public Function fopen&(szFileName As String, szMode As String)
  Dim fh As Long
  
  fh = FreeFile
  Select Case LCase$(szMode)
  Case "r": Open szFileName For Input As #fh
  Case "w": Open szFileName For Output As #fh
  Case "a": Open szFileName For Append As #fh
  Case "rb": Open szFileName For Binary As #fh
  Case "wb"
    Open szFileName For Output As #fh
    Close #fh
    Open szFileName For Binary As #fh
  Case Else: fh = 0
  End Select
  fopen = fh
End Function


'20130604LC
Public Function imax(a As Integer, b As Integer) As Integer
  If a > b Then imax = a Else imax = b
End Function


'20130604LC
Public Function imin(a As Integer, b As Integer) As Integer
  If a < b Then imin = a Else imin = b
End Function


'20130211LC (tutta)  'ex 20080616LC
Public Function InterpL#(ta#(), iBT&, iSex&, iTit&, x#)
  Dim iQ%, x0%, x1%, XMAX%, XMIN%
  Dim y#, z0#, z1#

  iQ = (iTit - 1) * 2 + iSex - 1 '20080617LC
  x0 = Int(x)
  x1 = x0 + 1
  z1 = x - x0
  '20111217LC (inizio)
  If OPZ_INTERP_X = 0 Then
    z1 = Int(z1 * 360 + AA0) / 360#  'new, la parte decimale di X ha perso 1 o 2 decimali significativi
  ElseIf OPZ_INTERP_X = 1 Then
    z1 = 0
  ElseIf OPZ_INTERP_X = 2 Then
    z1 = AA9
  ElseIf OPZ_INTERP_X = 3 Then
    z1 = 0.5
  End If
  '20111217LC (fine)
  z0 = 1 - z1
  XMIN = LBound(ta, 1)
  XMAX = UBound(ta, 1)
  If x0 < XMIN Then
    y = ta(XMIN, iBT, iQ) * z1
  ElseIf x0 > XMAX Then
    y = ta(XMAX, iBT, iQ)
  ElseIf x1 > XMAX Then
    y = ta(x0, iBT, iQ)
  Else
    y = ta(x0, iBT, iQ) * z0 + ta(x1, iBT, iQ) * z1
  End If
  InterpL = y
End Function


'20130216LC (tutta)  'ex 20120506C
Public Function InterpLF(ta#(), iBT&, iSex&, iTit&, x#, fact0#, fact1#) As Double
  Dim dx#, y#, y0#, y1#
  
  dx = x - Int(x)
  If OPZ_INT_ABBQ = 0 Then
    y = InterpL(ta(), iBT, iSex, iTit, x)
    If y < AA9 Then
      y = y * fact0
    End If
  Else
    y0 = InterpL(ta(), iBT, iSex, iTit, Int(x))
    If y0 < AA9 Then
      y0 = y0 * fact0
    End If
    y1 = InterpL(ta(), iBT, iSex, iTit, Int(x + 1))
    If y1 < AA9 Then
      y1 = y1 * fact1
    End If
    y = y0 * (1 - dx) + y1 * dx
  End If
  InterpLF = y
End Function


Public Function InterpLSM#(ByVal h#, ByVal x#, Lsm#(), iLsm&, iSex&, iTit&, iTC&)
  Dim h0&, h1&, iQ&, x0&, x1&
  Dim csi#, eta#, y#
  
  iQ = (iTit - 1) * 2 + iSex - 1 '20080416LC
  If h < K_HMIN Then
    h = K_HMIN
  ElseIf h > K_HMAX - AA0 Then
    h = K_HMAX - 1
  End If
  If x < K_XMIN Then
    x = K_XMIN
  ElseIf x > K_XMAX - AA0 Then
    x = K_XMAX - 1
  End If
  h0 = Int(h)
  h1 = h0 + 1
  csi = h - h0
  x0 = Int(x)
  x1 = x0 + 1
  eta = x - x0
  '20111217LC (inizio)
  If OPZ_INTERP_H = 0 Then
  ElseIf OPZ_INTERP_H = 1 Then
    csi = 0
  ElseIf OPZ_INTERP_H = 2 Then
    csi = AA9
  ElseIf OPZ_INTERP_H = 3 Then
    csi = 0.5
  End If
  If OPZ_INTERP_X = 0 Then
  ElseIf OPZ_INTERP_X = 1 Then
    eta = 0
  ElseIf OPZ_INTERP_X = 2 Then
    eta = AA9
  ElseIf OPZ_INTERP_X = 3 Then
    eta = 0.5
  End If
  '20111217LC (fine)
  If x0 = K_XMAX Then
   y = Lsm(h0, x0, iLsm, iTC, iQ) * (1 - csi) _
     + Lsm(h1, x0, iLsm, iTC, iQ) * csi
  ElseIf h0 = K_HMAX Then
   y = Lsm(h0, x0, iLsm, iTC, iQ) * (1 - eta) _
     + Lsm(h0, x1, iLsm, iTC, iQ) * eta
  Else
   y = Lsm(h0, x0, iLsm, iTC, iQ) * (1 - csi) * (1 - eta) _
     + Lsm(h1, x0, iLsm, iTC, iQ) * csi * (1 - eta) _
     + Lsm(h1, x1, iLsm, iTC, iQ) * csi * eta _
     + Lsm(h0, x1, iLsm, iTC, iQ) * (1 - csi) * eta
  End If
  InterpLSM = y
End Function


Public Function lmax(a As Long, b As Long) As Long
'20130604LC
  If a > b Then lmax = a Else lmax = b
End Function


Public Function lmin(a As Long, b As Long) As Long
'20130604LC
  If a < b Then lmin = a Else lmin = b
End Function


Public Sub LeggiBD(db As ADODB.Connection, bdNome$, i%, LL#(), bMorte As Boolean)
'ex 20120210LC (tutta)
'20130211LC
  Dim bdType As Integer
  Dim iQ%, x%, XMAX%
  Dim SQL$
  Dim rs As New ADODB.Recordset
  
  '************************
  'Legge le basi per M ed F
  '************************
  If UCase(Left(bdNome, 1)) = "Q" Then
    bdType = 2
  Else
    bdType = 1
  End If
  'SQL = "SELECT bdX,bdLam,bdLaf,bdLim,bdLif"
  SQL = "SELECT bdX,bdL1m,bdL1f,bdL2m,bdL2f"  '20150528LC
  SQL = SQL & " FROM C_BD"  '20160419LC
  SQL = SQL & " WHERE bdNome='" & UCase(bdNome) & "'" '20121024LC
  SQL = SQL & " AND bdX>=0"
  SQL = SQL & " AND bdX<=" & IIf(bdType = 2, OPZ_OMEGA - 1, OPZ_OMEGA) '20130211LC
  SQL = SQL & " ORDER BY bdX ASC"
  rs.Open SQL, db, adOpenForwardOnly, adLockReadOnly
  '20120619LC (inizio) - TODO: da gestire
  'If rs.EOF = True Then
  '  call MsgBox("Nessun record trovato per la base demografica " & bdNome, vbCritical
  '  End
  'End If
  '20120619LC (fine)
  XMAX = 0  '20130211LC
  Do Until rs.EOF
    x = rs.Fields(0).Value
    If XMAX < x Then
      XMAX = x
    End If
    For iQ = 0 To 3
      LL(x, i, iQ) = rs.Fields(iQ + 1).Value
    Next iQ
    rs.MoveNext
  Loop
  rs.Close
  '*************************************
  'Trasforma le basi di tipo L in tipo Q
  '*************************************
  If bdType = 1 Then
    For iQ = 0 To 3
      For x = 0 To OPZ_OMEGA - 1
        If LL(x, i, iQ) > 0 Then
          LL(x, i, iQ) = 1 - LL(x + 1, i, iQ) / LL(x, i, iQ)
        '20080926LC (inizio)
        ElseIf bMorte = True Then
          LL(x, i, iQ) = 1#
        Else
          LL(x, i, iQ) = 0#
        End If
        '20080926LC (fine)
      Next x
      '--- 20130211LC (inizio)
      For x = OPZ_OMEGA To OPZ_OMEGAP1
        If bMorte = True Then
          LL(x, i, iQ) = 1#
        Else
          LL(x, i, iQ) = 0#
        End If
      Next x
      '--- 20130211LC (fine)
    Next iQ
  Else
    For iQ = 0 To 3
      For x = XMAX + 1 To OPZ_OMEGAP1  '20130122LC
        '20080926LC (inizio)
        If bMorte = True Then
          LL(x, i, iQ) = 1#
        Else
          LL(x, i, iQ) = 0#
        End If
        '20080926LC (fine)
      Next x
    Next iQ
  End If
End Sub


Public Function LeggiBDA(db As ADODB.Connection, bdaNome$, bdaAmin%, BDA#()) As String
'************************************
'Legge le basi demografiche variabili
'20130305LC (tutta)
'************************************
  Dim iQ%, omega%, t%, tMin%, tMax%, x%, XMAX%
  Dim y#
  Dim SQL$, szErr$
  Dim rs As New ADODB.Recordset
  
  szErr = ""
  omega = UBound(BDA, 2)
  tMax = -1
  XMAX = -1
  'SQL = "SELECT bdaA,bdaX,bdaCam,bdaCaf,bdaCim,bdaCif"
  SQL = "SELECT bdaA,bdaX,bdaC1m,bdaC1f,bdaC2m,bdaC2f"  '20150528LC
  SQL = SQL & " FROM C_BDA"
  SQL = SQL & " WHERE bdaNome='" & UCase(bdaNome) & "'" '20121024LC
  SQL = SQL & " AND bdaX>=0"
  SQL = SQL & " AND bdaX<" & omega
  SQL = SQL & " AND bdaA>=" & bdaAmin
  SQL = SQL & " AND bdaA<=" & bdaAmin + P_DUECENTO
  SQL = SQL & " ORDER BY bdaA ASC,bdaX ASC"
  rs.Open SQL, db, adOpenForwardOnly, adLockReadOnly
  If rs.EOF Then
    rs.Close
    szErr = "Nessun record trovato nella tabella C_BDA"
    For x = 0 To omega
      For iQ = 0 To 3
        For t = 0 To tMax
          BDA(t, x, iQ) = 1
        Next t
      Next iQ
    Next x
    GoTo ExitPoint
  Else
    tMin = rs.Fields(0).Value - bdaAmin  '20130305LC
    Do Until rs.EOF
      t = rs.Fields(0).Value - bdaAmin
      If tMax < t Then
        tMax = t
      End If
      x = rs.Fields(1).Value
      If XMAX < x Then
        XMAX = x
      End If
      For iQ = 0 To 3
        BDA(t, x, iQ) = rs.Fields(2 + iQ).Value
        If BDA(t, x, iQ) < 0 Then
          BDA(t, x, iQ) = 0
        ElseIf BDA(t, x, iQ) > 1 Then
          BDA(t, x, iQ) = 1
        End If
      Next iQ
      rs.MoveNext
    Loop
    rs.Close
    '***************
    'Completamento X
    '***************
    If XMAX >= omega Then
      XMAX = omega - 1
    End If
    For t = 0 To tMax
      For x = XMAX + 1 To omega
        For iQ = 0 To 3
          BDA(t, x, iQ) = 1
        Next iQ
      Next x
    Next t
    '*********************************
    'Normalizzazione e completamento T
    '*********************************
    For x = 0 To omega
      For iQ = 0 To 3
        y = BDA(tMin, x, iQ)  '20130305LC
        If y > 0 Then
          '---  20130305LC (inizio)
          '--- Estrapolazione iniziale
          For t = 0 To tMin - 1
            BDA(t, x, iQ) = 1#
          Next t
          '--- Normalizzazione
          BDA(tMin, x, iQ) = 1#
          For t = tMin + 1 To tMax
            BDA(t, x, iQ) = BDA(t, x, iQ) / y
          Next t
          '--- Estrapolazione iniziale
          '---  20130305LC (fine)
          If (OPZ_ABB_QM_PRM_1 >= 1 And OPZ_ABB_QM_PRM_1 <= 2) And tMax >= 1 Then 'Estrapolazione lineare
            If OPZ_ABB_QM_PRM_1 = 1 And tMax >= 1 Then 'Estrapolazione lineare
              y = BDA(tMax, x, iQ) - BDA(tMax - 1, x, iQ)
              For t = tMax + 1 To P_DUECENTO
                BDA(t, x, iQ) = BDA(t - 1, x, iQ) + y
              Next t
            ElseIf OPZ_ABB_QM_PRM_1 = 2 Then  'Estrapolazione esponenziale
              y = BDA(tMax, x, iQ) / BDA(tMax - 1, x, iQ)
              For t = tMax + 1 To P_DUECENTO
                BDA(t, x, iQ) = BDA(t - 1, x, iQ) * y
              Next t
            End If
            For t = tMax + 1 To P_DUECENTO
              If BDA(t, x, iQ) < 0# Then
                BDA(t, x, iQ) = 0#
              ElseIf BDA(t, x, iQ) > 1# Then
                BDA(t, x, iQ) = 1#
              End If
            Next t
          Else
            For t = tMax + 1 To P_DUECENTO
              BDA(t, x, iQ) = 1#
            Next t
          End If
        Else
          For t = 0 To tMax
            BDA(t, x, iQ) = 1#
          Next t
        End If
      Next iQ
    Next x
  End If
  
ExitPoint:
  LeggiBDA = szErr
End Function


Public Sub LeggiCoeffPA(annoIniz%, db As ADODB.Connection, cpaNome$, CoeffPA#())
'ex 20120302LC
'20140319LC (tutta)
  Dim iTipo%, sex%, t%, t0%, t1%, x%, x0%
  Dim SQL$
  Dim rs As New ADODB.Recordset
  
  SQL = "SELECT cpaAnno0,cpaAnno1,cpaX,cpaCoeffM,cpaCoeffF"
  SQL = SQL & " FROM C_CCR"  '20160419LC-2  'ex C_COE_PA
  SQL = SQL & " WHERE cpaNome='" & UCase(cpaNome) & "'" '20121024LC
  SQL = SQL & " ORDER BY cpaAnno0,cpaAnno1,cpaX"        '20121024LC
  Call rs.Open(SQL, db, adOpenKeyset, adLockReadOnly)
  If rs.EOF Then
  Else
    ReDim CoeffPA(0 To 1, 0 To OPZ_OMEGA, 0 To P_DUECENTO) As Double
    Do Until rs.EOF
      x = rs(2).Value
      If x >= 0 And x <= OPZ_OMEGA Then
        t0 = rs(0).Value - annoIniz
        If t0 < 0 Then
          t0 = 0
        End If
        t1 = rs(1).Value - annoIniz
        If t1 > P_DUECENTO Then
          t1 = P_DUECENTO
        End If
        For t = t0 To t1
          CoeffPA(0, x, t) = rs(3).Value
          CoeffPA(1, x, t) = rs(4).Value
        Next t
      End If
      rs.MoveNext
    Loop
  End If
  rs.Close
  '***************************
  'Completamento della tabella
  '***************************
  For sex = 0 To 1
    For t = 0 To P_DUECENTO
      For x0 = 0 To OPZ_OMEGA
        If CoeffPA(sex, x0, t) > 0 Then
          For x = 0 To x0 - 1
            CoeffPA(sex, x, t) = CoeffPA(sex, x0, t)
          Next x
          Exit For
        End If
      Next x0
      For x0 = OPZ_OMEGA To 0 Step -1
        If CoeffPA(sex, x0, t) > 0 Then
          For x = x0 + 1 To OPZ_OMEGA
            CoeffPA(sex, x, t) = CoeffPA(sex, x0, t)
          Next x
          Exit For
        End If
      Next x0
    Next t
  Next sex
End Sub


Public Sub LeggiCoeffRiv(db As ADODB.Connection, t0&, parm() As Variant, CoeffVar#(), CoeffRiv#())
'20080709LC
  'Non usata
End Sub


Public Function LeggiCoeffVar1(db As ADODB.Connection, t0%, CoeffVar#(), cv() As TCV, cv4() As TAppl_CV4) As String
'20120607LC (tutta)
  Dim bFoundTable As Boolean '20120607LC
  Dim anno%, anno1%, j%, j1%  '20160419LC-2 (tolto it)  'ex 20120607LC
  Dim y#
  Dim SQL$, szErr$, szTable$ '20120607LC
  Dim sTableCV As String  '20160419LC
  Dim rs As New ADODB.Recordset
  

  '*************************************************
  '1) Legge tutti i record disponibili su C_COE_VAR4
  '*************************************************
  '20120607LC (inizio)
  szErr = ""
  sTableCV = "C_CV" '20160419LC-2
  szTable = OPZ_CV_NOME
  '--- 20160419LC-2 (inizio) tolto it=1 to 2
  SQL = "SELECT *"
  SQL = SQL & " FROM " & sTableCV  '20160419LC
  SQL = SQL & " WHERE cvNome='" & UCase(szTable) & "'" '20121024LC
  SQL = SQL & " AND cvAnno<=" & t0 + P_DUECENTO  '20140320LC
  SQL = SQL & " ORDER BY cvAnno ASC"
  Call rs.Open(SQL, db, adOpenForwardOnly, adLockReadOnly)
  'If it = 1 Then
    OPZ_INT_CV_AMIN = rs.Fields("cvAnno").Value
    OPZ_INT_CV_AMAX = t0 + P_DUECENTO
    OPZ_INT_CV_DIM1 = 1 + OPZ_INT_CV_AMAX - OPZ_INT_CV_AMIN
    OPZ_INT_CV_DIM2 = 1 + ecv.cv_max
    ReDim CoeffVar(OPZ_INT_CV_AMIN To OPZ_INT_CV_AMAX, 0 To ecv.cv_max)
    ReDim cv(OPZ_INT_CV_AMIN To OPZ_INT_CV_AMAX) As TCV
    ReDim cv4(OPZ_INT_CV_AMIN To t0 + OPZ_INT_CV_AMAX, 0 To 1)
  'End If
  '--- 20160419LC-2 (fine) tolto it=1 to 2
  bFoundTable = False
  Do Until rs.EOF
    bFoundTable = True
'20120607LC (fine)
    anno = rs.Fields("cvAnno").Value
    '******************************
    'Frame TASSI ED ALTRE STIME (2)
    '******************************
    '--- 2.1 Tassi di valutazione e rivalutazione ---
    '--- 20140605LC (inizio)  tolto underscore
    If OPZ_INT_CV21 = 1 Then    'ex bCv25 = True
      CoeffVar(anno, ecv.cv_Tv) = rs.Fields("cvTv").Value  'ex 201211024LC (underscore)
      CoeffVar(anno, ecv.cv_Ti) = rs.Fields("cvTi").Value  'ex 201211024LC (underscore)
    End If
    '--- 20140605LC (fine)
    CoeffVar(anno, ecv.cv_Tev) = rs.Fields("cvTev").Value
    CoeffVar(anno, ecv.cv_Trn) = rs.Fields("cvTrn").Value
    '--- 2.2 Tassi di rivalutazione dei montanti contributivi ---
    '--- 20150609LC (inizio)
    CoeffVar(anno, ecv.cv_Trmc0) = rs.Fields("cvTrmc0").Value  '20161115LC
    CoeffVar(anno, ecv.cv_Trmc1) = rs.Fields("cvTrmc1").Value  '20180306LC
    CoeffVar(anno, ecv.cv_Trmc2) = rs.Fields("cvTrmc4").Value
    CoeffVar(anno, ecv.cv_Trmc3) = rs.Fields("cvTrmc4").Value
    CoeffVar(anno, ecv.cv_Trmc4) = rs.Fields("cvTrmc4").Value
    CoeffVar(anno, ecv.cv_Trmc5) = rs.Fields("cvTrmc5").Value  '20161111LC
    CoeffVar(anno, ecv.cv_Trmc6) = rs.Fields("cvTrmc6").Value
    '--- 20150609LC (fine)
    '--- 2.3 Tassi di rivalutazione di redditi e volumi d'affari ---
    CoeffVar(anno, ecv.cv_Ts1a) = rs.Fields("cvTs1a").Value
    CoeffVar(anno, ecv.cv_Ts1p) = rs.Fields("cvTs1p").Value
    CoeffVar(anno, ecv.cv_Ts1n) = rs.Fields("cvTs1n").Value
    '--- 20160606LC (inizio)
    CoeffVar(anno, ecv.cv_Ts2a) = CoeffVar(anno, ecv.cv_Ts1a)
    CoeffVar(anno, ecv.cv_Ts2p) = CoeffVar(anno, ecv.cv_Ts1p)
    CoeffVar(anno, ecv.cv_Ts2n) = CoeffVar(anno, ecv.cv_Ts1n)
    '--- 20160606LC (fine)
    '--- 2.4 Rivalutazione delle pensioni ---
    CoeffVar(anno, ecv.cv_Tp1Max) = rs.Fields("cvTp1Max").Value
    CoeffVar(anno, ecv.cv_Tp1Al) = rs.Fields("cvTp1Al").Value
    CoeffVar(anno, ecv.cv_Tp2Max) = rs.Fields("cvTp2Max").Value
    CoeffVar(anno, ecv.cv_Tp2Al) = rs.Fields("cvTp2Al").Value
    CoeffVar(anno, ecv.cv_Tp3Max) = rs.Fields("cvTp3Max").Value
    CoeffVar(anno, ecv.cv_Tp3Al) = rs.Fields("cvTp3Al").Value
    '--- 20161112LC (inizio) ---
    CoeffVar(anno, ecv.cv_Tp4Max) = ff0(rs.Fields("cvTp4Max").Value)
    CoeffVar(anno, ecv.cv_Tp4Al) = ff0(rs.Fields("cvTp4Al").Value)
    CoeffVar(anno, ecv.cv_Tp5Max) = ff0(rs.Fields("cvTp5Max").Value)
    CoeffVar(anno, ecv.cv_Tp5Al) = ff0(rs.Fields("cvTp5Al").Value)
    '--- 20161112LC (fine) ---
    '--- 2.5 Altre stime ----
    '--- 20140605LC (inizio)  tolto underscore
    If OPZ_INT_CV25 = 1 Then  '20140605LC  'ex 201211024LC (underscore)  'bCv25 = True
      CoeffVar(anno, ecv.cv_PrcPVattM) = rs.Fields("cvPrcPVattM").Value
      CoeffVar(anno, ecv.cv_PrcPVattF) = rs.Fields("cvPrcPVattF").Value
      CoeffVar(anno, ecv.cv_PrcPAattM) = rs.Fields("cvPrcPAattM").Value
      CoeffVar(anno, ecv.cv_PrcPAattF) = rs.Fields("cvPrcPAattF").Value
      CoeffVar(anno, ecv.cv_PrcPCattM) = rs.Fields("cvPrcPCattM").Value
      CoeffVar(anno, ecv.cv_PrcPCattF) = rs.Fields("cvPrcPCattF").Value
    End If
    '--- 20140605LC (fine)
    '******************************
    'Frame REQUISITI DI ACCESSO (4)
    '******************************
    '--- 20160419LC (inizio)
    '--- 4.1 Pensione di vecchiaia ordinaria ----
    CoeffVar(anno, ecv.cv_Pveo30M) = rs.Fields("cvPveo20M").Value
    CoeffVar(anno, ecv.cv_Pveo30F) = rs.Fields("cvPveo20F").Value
    CoeffVar(anno, ecv.cv_Pveo65M) = rs.Fields("cvPveo67M").Value
    CoeffVar(anno, ecv.cv_Pveo65F) = rs.Fields("cvPveo67F").Value
    CoeffVar(anno, ecv.cv_Pveo98M) = rs.Fields("cvPveo92M").Value
    CoeffVar(anno, ecv.cv_Pveo98F) = rs.Fields("cvPveo92F").Value
    '--- 4.2 Pensione di vecchiaia anticipata ----
    CoeffVar(anno, ecv.cv_Pvea35M) = rs.Fields("cvPvea20M").Value
    CoeffVar(anno, ecv.cv_Pvea35F) = rs.Fields("cvPvea20F").Value
    CoeffVar(anno, ecv.cv_Pvea58M) = rs.Fields("cvPvea65M").Value
    CoeffVar(anno, ecv.cv_Pvea58F) = rs.Fields("cvPvea65F").Value
    '--- 4.3 Pensione di vecchiaia posticipata ----
    CoeffVar(anno, ecv.cv_Pvep70M) = rs.Fields("cvPvep75M").Value
    CoeffVar(anno, ecv.cv_Pvep70F) = rs.Fields("cvPvep75F").Value
    '--- 4.4 Pensione di inabilit� ----
    CoeffVar(anno, ecv.cv_Pinab2M) = rs.Fields("cvPinab5M").Value
    CoeffVar(anno, ecv.cv_Pinab2F) = rs.Fields("cvPinab5F").Value
    CoeffVar(anno, ecv.cv_Pinab10M) = 0   'rs.Fields("cvPinab10M").Value
    CoeffVar(anno, ecv.cv_Pinab10F) = 0   'rs.Fields("cvPinab10F").Value
    CoeffVar(anno, ecv.cv_Pinab35M) = 99  'rs.Fields("cvPinab35M").Value
    CoeffVar(anno, ecv.cv_Pinab35F) = 99  'rs.Fields("cvPinab35F").Value
    '--- 4.5 Pensione di invalidit� ----
    CoeffVar(anno, ecv.cv_Pinv5M) = rs.Fields("cvPinv5M").Value
    CoeffVar(anno, ecv.cv_Pinv5F) = rs.Fields("cvPinv5F").Value
    CoeffVar(anno, ecv.cv_Pinv70) = rs.Fields("cvPinv70").Value
    '--- 4.6 Pensione indiretta ----
    CoeffVar(anno, ecv.cv_Pind2M) = rs.Fields("cvPind5M").Value
    CoeffVar(anno, ecv.cv_Pind2F) = rs.Fields("cvPind5F").Value
    CoeffVar(anno, ecv.cv_Pind20M) = rs.Fields("cvPind20M").Value
    CoeffVar(anno, ecv.cv_Pind20F) = rs.Fields("cvPind20F").Value
    CoeffVar(anno, ecv.cv_Pind30M) = 99 'rs.Fields("cvPind30M").Value
    CoeffVar(anno, ecv.cv_Pind30F) = 99 'rs.Fields("cvPind30F").Value
    '--- 4.7 Restituzione dei contributi ----
    CoeffVar(anno, ecv.cv_RcHminM) = rs.Fields("cvRcHminM").Value
    CoeffVar(anno, ecv.cv_RcHminF) = rs.Fields("cvRcHminF").Value
    CoeffVar(anno, ecv.cv_RcXminM) = rs.Fields("cvRcXminM").Value
    CoeffVar(anno, ecv.cv_RcXminF) = rs.Fields("cvRcXminF").Value
    CoeffVar(anno, ecv.cv_RcPrcCon) = 1  'rs.Fields("cvRcPrcCon").Value
    'CoeffVar(anno, ecv.cv_RcPrcRiv) = rs.Fields("cvRcPrcRiv").Value  '20150512LC (commentato)
    '--- 4.8 Totalizzazione ----
    '***********************
    'Frame CONTRIBUZIONE (5)
    '***********************
    '--- 5.1 Contribuzione ridotta ----
    '--- 5.2 Soggettivo ----
    CoeffVar(anno, ecv.cv_Sogg0Min) = rs.Fields("cvSogg0Min").Value
    CoeffVar(anno, ecv.cv_Sogg0MinR) = rs.Fields("cvSogg0MinR").Value
    CoeffVar(anno, ecv.cv_Sogg1Al) = rs.Fields("cvSogg1Al").Value
    CoeffVar(anno, ecv.cv_Sogg1AlR) = rs.Fields("cvSogg1AlR").Value
    CoeffVar(anno, ecv.cv_Sogg1Max) = rs.Fields("cvSogg1Max").Value
    CoeffVar(anno, ecv.cv_Sogg1MaxR) = rs.Fields("cvSogg1MaxR").Value
    '--- 5.3 Integrativo / FIRR ----
    CoeffVar(anno, ecv.cv_Int0Min) = rs.Fields("cvFirr0Min").Value
    CoeffVar(anno, ecv.cv_Int0MinR) = rs.Fields("cvFirr0MinR").Value
    CoeffVar(anno, ecv.cv_Int1Al) = rs.Fields("cvFirr1Al").Value
    CoeffVar(anno, ecv.cv_Int1AlR) = rs.Fields("cvFirr1AlR").Value
    CoeffVar(anno, ecv.cv_Int1Max) = rs.Fields("cvFirr1Max").Value
    CoeffVar(anno, ecv.cv_Int1MaxR) = rs.Fields("cvFirr1MaxR").Value
    CoeffVar(anno, ecv.cv_Int2Al) = rs.Fields("cvFirr2Al").Value
    CoeffVar(anno, ecv.cv_Int2AlR) = rs.Fields("cvFirr2AlR").Value
    'If Abs(OPZ_INT_COMP) = COMP_ENAS Then
      CoeffVar(anno, ecv.cv_Int2Max) = rs.Fields("cvFirr2Max").Value
      CoeffVar(anno, ecv.cv_Int2MaxR) = rs.Fields("cvFirr2MaxR").Value
      CoeffVar(anno, ecv.cv_Int3Al) = rs.Fields("cvFirr3Al").Value
      CoeffVar(anno, ecv.cv_Int3AlR) = rs.Fields("cvFirr3AlR").Value
    'Else
    '  CoeffVar(anno, ecv.cv_IntArt25) = ff0(rs.Fields("cvIntArt25").Value)
    'End If
    '--- 20160419LC (fine)
    '--- 5.4 Assistenziale ----
    CoeffVar(anno, ecv.cv_Ass0Min) = rs.Fields("cvAss0Min").Value
    CoeffVar(anno, ecv.cv_Ass0MinR) = rs.Fields("cvAss0MinR").Value
    CoeffVar(anno, ecv.cv_Ass1Al) = rs.Fields("cvAss1Al").Value
    CoeffVar(anno, ecv.cv_Ass1AlR) = rs.Fields("cvAss1AlR").Value
    '--- 5.5 Altro ----
    CoeffVar(anno, ecv.cv_Mat0Min) = rs.Fields("cvMat0Min").Value
    CoeffVar(anno, ecv.cv_Mat0MinR) = rs.Fields("cvMat0MinR").Value
    '****************************
    'Frame MISURA PRESTAZIONI (6)
    '****************************
    '--- 6.1 Scaglioni IRPEF ----
    '--- 6.2 Pensione ----
    '--- 6.3 Supplementi di pensione ----
    CoeffVar(anno, ecv.cv_SpAnni) = rs.Fields("cvSpAnni").Value
    CoeffVar(anno, ecv.cv_SpEtaMax) = rs.Fields("cvSpEtaMax").Value
    CoeffVar(anno, ecv.cv_SpPrcCon) = rs.Fields("cvSpPrcCon").Value
    'CoeffVar(anno, ecv.cv_SpPrcRiv) = rs.Fields("cvSpPrcRiv").Value  '20150512LC (commentato)
    '--- 6.4 Reversibilit� superstiti ----
    CoeffVar(anno, ecv.cv_Av) = rs.Fields("cvAv").Value
    CoeffVar(anno, ecv.cv_Avo) = rs.Fields("cvAvO").Value
    CoeffVar(anno, ecv.cv_Avoo) = rs.Fields("cvAvOO").Value
    CoeffVar(anno, ecv.cv_Ao) = rs.Fields("cvAo").Value
    CoeffVar(anno, ecv.cv_Aoo) = rs.Fields("cvAoo").Value
    CoeffVar(anno, ecv.cv_Aooo) = rs.Fields("cvAooo").Value
    Call rs.MoveNext
  Loop
  Call rs.Close
'20120607LC (inizio)
  If bFoundTable = False Then
    szErr = "Nessun record trovato in C_CV (OPZ_CV_NOME='" & UCase(szTable) & "')"  '20160419LC-2  'ex 20140320LC  'ex 20121024LC
  End If
  If szErr <> "" Then GoTo ExitPoint
  '20120607LC (fine)
  '**********************************************************
  '2) Completa la tabella C_COE_VAR4 a partire dal dati letti
  '**********************************************************
  'anno1 = parm(P_btAnno)
  anno1 = anno
  For anno = anno1 + 1 To OPZ_INT_CV_AMAX
    '******************************
    'Frame TASSI ED ALTRE STIME (2)
    '******************************
    '--- 2.1 Tassi di valutazione e rivalutazione ---
    '--- 20140605LC (inizio)
    If OPZ_INT_CV21 = 1 Then    'ex bCv25 = True
      CoeffVar(anno, ecv.cv_Tv) = CoeffVar(anno - 1, ecv.cv_Tv)
      CoeffVar(anno, ecv.cv_Ti) = CoeffVar(anno - 1, ecv.cv_Ti)
    End If
    '--- 20140605LC (fine)
    CoeffVar(anno, ecv.cv_Tev) = CoeffVar(anno - 1, ecv.cv_Tev)
    CoeffVar(anno, ecv.cv_Trn) = CoeffVar(anno - 1, ecv.cv_Trn)
    '--- 2.2 Tassi di rivalutazione dei montanti contributivi ---
    '--- 20150609LC (inizio)
    CoeffVar(anno, ecv.cv_Trmc0) = CoeffVar(anno - 1, ecv.cv_Trmc0)
    CoeffVar(anno, ecv.cv_Trmc1) = CoeffVar(anno - 1, ecv.cv_Trmc1)
    CoeffVar(anno, ecv.cv_Trmc2) = CoeffVar(anno - 1, ecv.cv_Trmc2)
    CoeffVar(anno, ecv.cv_Trmc3) = CoeffVar(anno - 1, ecv.cv_Trmc3)
    CoeffVar(anno, ecv.cv_Trmc4) = CoeffVar(anno - 1, ecv.cv_Trmc4)
    CoeffVar(anno, ecv.cv_Trmc5) = CoeffVar(anno - 1, ecv.cv_Trmc5)
    CoeffVar(anno, ecv.cv_Trmc6) = CoeffVar(anno - 1, ecv.cv_Trmc6)
    '--- 20150609LC (fine)
    '--- 2.3 Tassi di rivalutazione di redditi e volumi d'affari ---
    CoeffVar(anno, ecv.cv_Ts1a) = CoeffVar(anno - 1, ecv.cv_Ts1a)
    CoeffVar(anno, ecv.cv_Ts1p) = CoeffVar(anno - 1, ecv.cv_Ts1p)
    CoeffVar(anno, ecv.cv_Ts1n) = CoeffVar(anno - 1, ecv.cv_Ts1n)
    '20160606LC (commentata)
    'If Math.Abs(OPZ_INT_COMP) = COMP_ENAS Then
    'Else
      CoeffVar(anno, ecv.cv_Ts2a) = CoeffVar(anno - 1, ecv.cv_Ts2a)
      CoeffVar(anno, ecv.cv_Ts2p) = CoeffVar(anno - 1, ecv.cv_Ts2p)
      CoeffVar(anno, ecv.cv_Ts2n) = CoeffVar(anno - 1, ecv.cv_Ts2n)
    'End If
    '--- 2.4 Rivalutazione delle pensioni ---
    CoeffVar(anno, ecv.cv_Tp1Al) = CoeffVar(anno - 1, ecv.cv_Tp1Al)
    CoeffVar(anno, ecv.cv_Tp2Al) = CoeffVar(anno - 1, ecv.cv_Tp2Al)
    CoeffVar(anno, ecv.cv_Tp3Al) = CoeffVar(anno - 1, ecv.cv_Tp3Al)
    '--- 20161112LC (inizio)
    CoeffVar(anno, ecv.cv_Tp4Al) = CoeffVar(anno - 1, ecv.cv_Tp4Al)
    CoeffVar(anno, ecv.cv_Tp5Al) = CoeffVar(anno - 1, ecv.cv_Tp5Al)
    '--- 20161112LC (fine)
    '--- 2.5 Altre stime ----
    '--- 20140605LC (inizio)
    If OPZ_INT_CV25 = 1 Then    'ex bCv25 = True
      CoeffVar(anno, ecv.cv_PrcPVattM) = CoeffVar(anno - 1, ecv.cv_PrcPVattM)
      CoeffVar(anno, ecv.cv_PrcPVattF) = CoeffVar(anno - 1, ecv.cv_PrcPVattF)
      CoeffVar(anno, ecv.cv_PrcPAattM) = CoeffVar(anno - 1, ecv.cv_PrcPAattM)
      CoeffVar(anno, ecv.cv_PrcPAattF) = CoeffVar(anno - 1, ecv.cv_PrcPAattF)
      CoeffVar(anno, ecv.cv_PrcPCattM) = CoeffVar(anno - 1, ecv.cv_PrcPCattM)
      CoeffVar(anno, ecv.cv_PrcPCattF) = CoeffVar(anno - 1, ecv.cv_PrcPCattF)
      CoeffVar(anno, ecv.cv_PrcPTattM) = CoeffVar(anno - 1, ecv.cv_PrcPTattM)
      CoeffVar(anno, ecv.cv_PrcPTattF) = CoeffVar(anno - 1, ecv.cv_PrcPTattF)
    End If
    '--- 20140605LC (fine)
  Next anno
  '******************************
  'Frame TASSI ED ALTRE STIME (2)
  '******************************
  For j = ecv.cv_Tv To ecv.cv_Ts2n
    j1 = ecv.cv_Prd_Ts1a + j - ecv.cv_Ts1a
    CoeffVar(anno1 - 1, j1) = 1#
    For anno = anno1 To OPZ_INT_CV_AMAX
      CoeffVar(anno, j1) = CoeffVar(anno - 1, j1) * (1 + CoeffVar(anno, j))
    Next anno
    For anno = anno1 - 2 To OPZ_INT_CV_AMIN Step -1
      CoeffVar(anno, j1) = CoeffVar(anno + 1, j1) / (1 + CoeffVar(anno + 1, j))
    Next anno
  Next j
  '---
  For anno = anno1 + 1 To OPZ_INT_CV_AMAX
    y = CoeffVar(anno, ecv.cv_prd_Tev) / CoeffVar(anno1, ecv.cv_prd_Tev)
    '******************************
    'Frame TASSI ED ALTRE STIME (2)
    '******************************
    '--- 2.4 Rivalutazione delle pensioni ---
    CoeffVar(anno, ecv.cv_Tp1Max) = ARRV(CoeffVar(anno1, ecv.cv_Tp1Max) * y)
    CoeffVar(anno, ecv.cv_Tp2Max) = ARRV(CoeffVar(anno1, ecv.cv_Tp2Max) * y)
    CoeffVar(anno, ecv.cv_Tp3Max) = ARRV(CoeffVar(anno1, ecv.cv_Tp3Max) * y)
    '--- 20161112LC (inizio)
    CoeffVar(anno, ecv.cv_Tp4Max) = ARRV(CoeffVar(anno1, ecv.cv_Tp4Max) * y)
    CoeffVar(anno, ecv.cv_Tp5Max) = ARRV(CoeffVar(anno1, ecv.cv_Tp5Max) * y)
    '--- 20161112LC (fine)
    '******************************
    'Frame REQUISITI DI ACCESSO (4)
    '******************************
    '--- 4.1 Pensione di vecchiaia ordinaria ----
    CoeffVar(anno, ecv.cv_Pveo30M) = CoeffVar(anno - 1, ecv.cv_Pveo30M)
    CoeffVar(anno, ecv.cv_Pveo30F) = CoeffVar(anno - 1, ecv.cv_Pveo30F)
    CoeffVar(anno, ecv.cv_Pveo65M) = CoeffVar(anno - 1, ecv.cv_Pveo65M)
    CoeffVar(anno, ecv.cv_Pveo65F) = CoeffVar(anno - 1, ecv.cv_Pveo65F)
    CoeffVar(anno, ecv.cv_Pveo98M) = CoeffVar(anno - 1, ecv.cv_Pveo98M)
    CoeffVar(anno, ecv.cv_Pveo98F) = CoeffVar(anno - 1, ecv.cv_Pveo98F)
    '--- 4.2 Pensione di vecchiaia anticipata ----
    CoeffVar(anno, ecv.cv_Pvea35M) = CoeffVar(anno - 1, ecv.cv_Pvea35M)
    CoeffVar(anno, ecv.cv_Pvea35F) = CoeffVar(anno - 1, ecv.cv_Pvea35F)
    CoeffVar(anno, ecv.cv_Pvea58M) = CoeffVar(anno - 1, ecv.cv_Pvea58M)
    CoeffVar(anno, ecv.cv_Pvea58F) = CoeffVar(anno - 1, ecv.cv_Pvea58F)
    '--- 4.3 Pensione di vecchiaia posticipata ----
    CoeffVar(anno, ecv.cv_Pvep70M) = CoeffVar(anno - 1, ecv.cv_Pvep70M)
    CoeffVar(anno, ecv.cv_Pvep70F) = CoeffVar(anno - 1, ecv.cv_Pvep70F)
    '--- 4.4 Pensione di inabilit� ----
    CoeffVar(anno, ecv.cv_Pinab2M) = CoeffVar(anno - 1, ecv.cv_Pinab2M)
    CoeffVar(anno, ecv.cv_Pinab2F) = CoeffVar(anno - 1, ecv.cv_Pinab2F)
    CoeffVar(anno, ecv.cv_Pinab10M) = CoeffVar(anno - 1, ecv.cv_Pinab10M)
    CoeffVar(anno, ecv.cv_Pinab10F) = CoeffVar(anno - 1, ecv.cv_Pinab10F)
    CoeffVar(anno, ecv.cv_Pinab35M) = CoeffVar(anno - 1, ecv.cv_Pinab35M)
    CoeffVar(anno, ecv.cv_Pinab35F) = CoeffVar(anno - 1, ecv.cv_Pinab35F)
    '--- 4.5 Pensione di invalidit� ----
    CoeffVar(anno, ecv.cv_Pinv5M) = CoeffVar(anno - 1, ecv.cv_Pinv5M)
    CoeffVar(anno, ecv.cv_Pinv5F) = CoeffVar(anno - 1, ecv.cv_Pinv5F)
    CoeffVar(anno, ecv.cv_Pinv70) = CoeffVar(anno - 1, ecv.cv_Pinv70)
    '--- 4.6 Pensione indiretta ----
    CoeffVar(anno, ecv.cv_Pind2M) = CoeffVar(anno - 1, ecv.cv_Pind2M)
    CoeffVar(anno, ecv.cv_Pind2F) = CoeffVar(anno - 1, ecv.cv_Pind2F)
    CoeffVar(anno, ecv.cv_Pind20M) = CoeffVar(anno - 1, ecv.cv_Pind20M)
    CoeffVar(anno, ecv.cv_Pind20F) = CoeffVar(anno - 1, ecv.cv_Pind20F)
    CoeffVar(anno, ecv.cv_Pind30M) = CoeffVar(anno - 1, ecv.cv_Pind30M)
    CoeffVar(anno, ecv.cv_Pind30F) = CoeffVar(anno - 1, ecv.cv_Pind30F)
    '--- 4.7 Restituzione dei contributi ----
    CoeffVar(anno, ecv.cv_RcHminM) = CoeffVar(anno - 1, ecv.cv_RcHminM)
    CoeffVar(anno, ecv.cv_RcHminF) = CoeffVar(anno - 1, ecv.cv_RcHminF)
    CoeffVar(anno, ecv.cv_RcXminM) = CoeffVar(anno - 1, ecv.cv_RcXminM)
    CoeffVar(anno, ecv.cv_RcXminF) = CoeffVar(anno - 1, ecv.cv_RcXminF)
    CoeffVar(anno, ecv.cv_RcPrcCon) = CoeffVar(anno - 1, ecv.cv_RcPrcCon)
    'CoeffVar(anno, ecv.cv_RcPrcRiv) = CoeffVar(anno - 1, ecv.cv_RcPrcRiv)  '20150512LC (commentato)
    '--- 4.8 Totalizzazione ----
    '***********************
    'Frame CONTRIBUZIONE (5)
    '***********************
    '--- 5.1 Contribuzione ridotta ----
    '--- 5.2 Soggettivo ----
    CoeffVar(anno, ecv.cv_Sogg0Min) = ARRV(CoeffVar(anno1, ecv.cv_Sogg0Min) * y)
    CoeffVar(anno, ecv.cv_Sogg0MinR) = ARRV(CoeffVar(anno1, ecv.cv_Sogg0MinR) * y)
    CoeffVar(anno, ecv.cv_Sogg1Al) = CoeffVar(anno - 1, ecv.cv_Sogg1Al)
    CoeffVar(anno, ecv.cv_Sogg1AlR) = CoeffVar(anno - 1, ecv.cv_Sogg1AlR)
    CoeffVar(anno, ecv.cv_Sogg1Max) = ARRV(CoeffVar(anno1, ecv.cv_Sogg1Max) * y)
    CoeffVar(anno, ecv.cv_Sogg1MaxR) = ARRV(CoeffVar(anno1, ecv.cv_Sogg1MaxR) * y)
    '--- 5.3 Integrativo / FIRR ----
    CoeffVar(anno, ecv.cv_Int0Min) = ARRV(CoeffVar(anno1, ecv.cv_Int0Min) * y)
    CoeffVar(anno, ecv.cv_Int0MinR) = ARRV(CoeffVar(anno1, ecv.cv_Int0MinR) * y)
    CoeffVar(anno, ecv.cv_Int1Al) = CoeffVar(anno - 1, ecv.cv_Int1Al)
    CoeffVar(anno, ecv.cv_Int1AlR) = CoeffVar(anno - 1, ecv.cv_Int1AlR)
    CoeffVar(anno, ecv.cv_Int1Max) = ARRV(CoeffVar(anno1, ecv.cv_Int1Max) * y)
    CoeffVar(anno, ecv.cv_Int1MaxR) = ARRV(CoeffVar(anno1, ecv.cv_Int1MaxR) * y)
    CoeffVar(anno, ecv.cv_Int2Al) = CoeffVar(anno - 1, ecv.cv_Int2Al)
    CoeffVar(anno, ecv.cv_Int2AlR) = CoeffVar(anno - 1, ecv.cv_Int2AlR)
    CoeffVar(anno, ecv.cv_Int2Max) = ARRV(CoeffVar(anno1, ecv.cv_Int2Max) * y)
    CoeffVar(anno, ecv.cv_Int2MaxR) = ARRV(CoeffVar(anno1, ecv.cv_Int2MaxR) * y)
    CoeffVar(anno, ecv.cv_Int3Al) = CoeffVar(anno - 1, ecv.cv_Int3Al)
    CoeffVar(anno, ecv.cv_Int3AlR) = CoeffVar(anno - 1, ecv.cv_Int3AlR)
    '--- 5.4 Assistenziale ----
    CoeffVar(anno, ecv.cv_Ass0Min) = ARRV(CoeffVar(anno1, ecv.cv_Ass0Min) * y)
    CoeffVar(anno, ecv.cv_Ass0MinR) = ARRV(CoeffVar(anno1, ecv.cv_Ass0MinR) * y)
    CoeffVar(anno, ecv.cv_Ass1Al) = CoeffVar(anno - 1, ecv.cv_Ass1Al)
    CoeffVar(anno, ecv.cv_Ass1AlR) = CoeffVar(anno - 1, ecv.cv_Ass1AlR)
    '--- 5.5 Altro ----
    CoeffVar(anno, ecv.cv_Mat0Min) = ARRV(CoeffVar(anno1, ecv.cv_Mat0Min) * y)
    CoeffVar(anno, ecv.cv_Mat0MinR) = ARRV(CoeffVar(anno1, ecv.cv_Mat0MinR) * y)
    '****************************
    'Frame MISURA PRESTAZIONI (6)
    '****************************
    '--- 6.1 Scaglioni IRPEF ----
    '--- 6.2 Pensione ----
    '--- 6.3 Supplementi di pensione ----
    CoeffVar(anno, ecv.cv_SpAnni) = CoeffVar(anno - 1, ecv.cv_SpAnni)
    CoeffVar(anno, ecv.cv_SpEtaMax) = CoeffVar(anno - 1, ecv.cv_SpEtaMax)
    CoeffVar(anno, ecv.cv_SpPrcCon) = CoeffVar(anno - 1, ecv.cv_SpPrcCon)
    'CoeffVar(anno, ecv.cv_SpPrcRiv) = CoeffVar(anno - 1, ecv.cv_SpPrcRiv)  '20150512LC (commentato)
    '--- 6.4 Reversibilit� superstiti ----
    CoeffVar(anno, ecv.cv_Av) = CoeffVar(anno - 1, ecv.cv_Av)
    CoeffVar(anno, ecv.cv_Avo) = CoeffVar(anno - 1, ecv.cv_Avo)
    CoeffVar(anno, ecv.cv_Avoo) = CoeffVar(anno - 1, ecv.cv_Avoo)
    CoeffVar(anno, ecv.cv_Ao) = CoeffVar(anno - 1, ecv.cv_Ao)
    CoeffVar(anno, ecv.cv_Aoo) = CoeffVar(anno - 1, ecv.cv_Aoo)
    CoeffVar(anno, ecv.cv_Aooo) = CoeffVar(anno - 1, ecv.cv_Aooo)
  Next anno
ExitPoint:
  LeggiCoeffVar1 = szErr
End Function


Public Function LeggiCoeffVar2(parm() As Variant, CoeffVar#(), cv() As TCV, cv4() As TAppl_CV4) As String
'20120212LC (tutta)
  Dim anno%, anno1%, j%, j1%
  Dim y#
  Dim szErr$
  If ON_ERR_EH = True Then On Error GoTo ErrorHandler
  
  szErr = ""
  '***************************************************
  '3) Completa la tabella C_COE_VAR4 a partire da PARM
  '***************************************************
  anno1 = parm(P_btAnno)
  '******************************
  'Frame TASSI ED ALTRE STIME (2)
  '******************************
  If (VarType(parm(P_cv2)) <> 0 And parm(P_cv2) = False) Then
    For anno = anno1 To OPZ_INT_CV_AMAX
      '--- 2.1 Tassi di valutazione e rivalutazione ---
      If OPZ_INT_CV21 = 1 Then  '20140605LC  'bCv25 = True
        CoeffVar(anno, ecv.cv_Tv) = parm(P_cvTv)
        CoeffVar(anno, ecv.cv_Ti) = parm(P_cvTi)
      End If
      CoeffVar(anno, ecv.cv_Tev) = parm(P_cvTev)
      CoeffVar(anno, ecv.cv_Trn) = parm(P_cvTrn)
      '--- 2.2 Tassi di rivalutazione dei montanti contributivi ---
      '--- 20150609LC (inizio)
      CoeffVar(anno, ecv.cv_Trmc1) = parm(P_cvTrmc4)
      CoeffVar(anno, ecv.cv_Trmc2) = parm(P_cvTrmc4)
      CoeffVar(anno, ecv.cv_Trmc3) = parm(P_cvTrmc4)
      CoeffVar(anno, ecv.cv_Trmc4) = parm(P_cvTrmc4)
      CoeffVar(anno, ecv.cv_Trmc5) = parm(P_cvTrmc4)
      CoeffVar(anno, ecv.cv_Trmc6) = parm(P_cvTrmc6)
      '--- 20150609LC (fine)
      '--- 2.3 Tassi di rivalutazione di redditi e volumi d'affari ---
      CoeffVar(anno, ecv.cv_Ts1a) = parm(P_cvTs1a)
      CoeffVar(anno, ecv.cv_Ts1p) = parm(P_cvTs1p)
      CoeffVar(anno, ecv.cv_Ts1n) = parm(P_cvTs1n)
      '--- 2.4 Rivalutazione delle pensioni ---
      CoeffVar(anno, ecv.cv_Tp1Al) = parm(P_cvTp1Al)
      CoeffVar(anno, ecv.cv_Tp2Al) = parm(P_cvTp2Al)
      CoeffVar(anno, ecv.cv_Tp3Al) = parm(P_cvTp3Al)
      '--- 20161112LC (inizio)
      CoeffVar(anno, ecv.cv_Tp4Al) = parm(P_cvTp4Al)
      CoeffVar(anno, ecv.cv_Tp5Al) = parm(P_cvTp5Al)
      '--- 20161112LC (fine)
      '--- 2.5 Altre stime ----
      If OPZ_INT_CV25 = 1 Then  '20140605LC  'bCv25 = True
        CoeffVar(anno, ecv.cv_PrcPVattM) = parm(P_cvPrcPVattM)
        CoeffVar(anno, ecv.cv_PrcPVattF) = parm(P_cvPrcPVattF)
        CoeffVar(anno, ecv.cv_PrcPAattM) = parm(P_cvPrcPAattM)
        CoeffVar(anno, ecv.cv_PrcPAattF) = parm(P_cvPrcPAattF)
        CoeffVar(anno, ecv.cv_PrcPCattM) = parm(P_cvPrcPCattM)
        CoeffVar(anno, ecv.cv_PrcPCattF) = parm(P_cvPrcPCattF)
      End If
    Next anno
  Else
    For anno = OPZ_INT_CV_AMIN To OPZ_INT_CV_AMAX
      '--- 2.1 Tassi di valutazione e rivalutazione ---
      If OPZ_INT_CV21 = 0 Then  '20140605LC  'ex bCv25 = False Then '20121106LC - ciclo esteso da AMIN
        CoeffVar(anno, ecv.cv_Tv) = parm(P_cvTv)
        CoeffVar(anno, ecv.cv_Ti) = parm(P_cvTi)
      End If
      '--- 2.5 Altre stime ----
      If OPZ_INT_CV25 = 0 Then  '20140605LC  'ex bCv25 = False
        CoeffVar(anno, ecv.cv_PrcPVattM) = parm(P_cvPrcPVattM)
        CoeffVar(anno, ecv.cv_PrcPVattF) = parm(P_cvPrcPVattF)
        CoeffVar(anno, ecv.cv_PrcPAattM) = parm(P_cvPrcPAattM)
        CoeffVar(anno, ecv.cv_PrcPAattF) = parm(P_cvPrcPAattF)
        CoeffVar(anno, ecv.cv_PrcPCattM) = parm(P_cvPrcPCattM)
        CoeffVar(anno, ecv.cv_PrcPCattF) = parm(P_cvPrcPCattF)
      End If
    Next anno
  End If
  '******************************
  'Frame TASSI ED ALTRE STIME (2)
  '******************************
  For j = ecv.cv_Tv To ecv.cv_Ts2n
    j1 = ecv.cv_Prd_Ts1a + j - ecv.cv_Ts1a
    CoeffVar(anno1 - 1, j1) = 1#
    For anno = anno1 To OPZ_INT_CV_AMAX
      CoeffVar(anno, j1) = CoeffVar(anno - 1, j1) * (1 + CoeffVar(anno, j))
    Next anno
    For anno = anno1 - 2 To OPZ_INT_CV_AMIN Step -1
      CoeffVar(anno, j1) = CoeffVar(anno + 1, j1) / (1 + CoeffVar(anno + 1, j))
    Next anno
  Next j
  '******************************
  'Frame TASSI ED ALTRE STIME (2)
  '******************************
  If (VarType(parm(P_cv2)) <> 0 And parm(P_cv2) = False) Then
    For anno = anno1 To OPZ_INT_CV_AMAX
      y = CoeffVar(anno, ecv.cv_prd_Tev) / CoeffVar(anno1, ecv.cv_prd_Tev)
      '--- 2.4 Rivalutazione delle pensioni ---
      CoeffVar(anno, ecv.cv_Tp1Max) = ARRV(parm(P_cvTp1Max) * y)
      CoeffVar(anno, ecv.cv_Tp2Max) = ARRV(parm(P_cvTp2Max) * y)
      '--- 20161112LC (inizio)
      CoeffVar(anno, ecv.cv_Tp3Max) = ARRV(parm(P_cvTp3Max) * y)
      CoeffVar(anno, ecv.cv_Tp4Max) = ARRV(parm(P_cvTp4Max) * y)
      CoeffVar(anno, ecv.cv_Tp5Max) = ARRV(parm(P_cvTp5Max) * y)
      '--- 20161112LC (fine)
    Next anno
  End If
  '******************************
  'Frame REQUISITI DI ACCESSO (4)
  '******************************
  If (VarType(parm(P_cv4)) <> 0 And parm(P_cv4) = False) Then
    y = 1
    For anno = anno1 To OPZ_INT_CV_AMAX
      '--- 4.1 Pensione di vecchiaia ordinaria ----
      CoeffVar(anno, ecv.cv_Pveo30M) = parm(P_cvPveo30M)
      CoeffVar(anno, ecv.cv_Pveo30F) = parm(P_cvPveo30F)
      CoeffVar(anno, ecv.cv_Pveo65M) = parm(P_cvPveo65M)
      CoeffVar(anno, ecv.cv_Pveo65F) = parm(P_cvPveo65F)
      CoeffVar(anno, ecv.cv_Pveo98M) = parm(P_cvPveo98M)
      CoeffVar(anno, ecv.cv_Pveo98F) = parm(P_cvPveo98F)
      '--- 4.2 Pensione di vecchiaia anticipata ----
      CoeffVar(anno, ecv.cv_Pvea35M) = parm(P_cvPvea35M)
      CoeffVar(anno, ecv.cv_Pvea35F) = parm(P_cvPvea35F)
      CoeffVar(anno, ecv.cv_Pvea58M) = parm(P_cvPvea58M)
      CoeffVar(anno, ecv.cv_Pvea58F) = parm(P_cvPvea58F)
      '--- 4.2 Pensione di vecchiaia posticipata ----
      CoeffVar(anno, ecv.cv_Pvep70M) = parm(P_cvPvep70M)
      CoeffVar(anno, ecv.cv_Pvep70F) = parm(P_cvPvep70F)
      '--- 4.4 Pensione di inabilit� ----
      CoeffVar(anno, ecv.cv_Pinab2M) = parm(P_cvPinab2M)
      CoeffVar(anno, ecv.cv_Pinab2F) = parm(P_cvPinab2F)
      CoeffVar(anno, ecv.cv_Pinab10M) = parm(P_cvPinab10M)
      CoeffVar(anno, ecv.cv_Pinab10F) = parm(P_cvPinab10F)
      CoeffVar(anno, ecv.cv_Pinab35M) = parm(P_cvPinab35M)
      CoeffVar(anno, ecv.cv_Pinab35F) = parm(P_cvPinab35F)
      '--- 4.5 Pensione di invalidit� ----
      CoeffVar(anno, ecv.cv_Pinv5M) = parm(P_cvPinv5M)
      CoeffVar(anno, ecv.cv_Pinv5F) = parm(P_cvPinv5F)
      CoeffVar(anno, ecv.cv_Pinv70) = parm(P_cvPinv70)
      '--- 4.6 Pensione indiretta ----
      CoeffVar(anno, ecv.cv_Pind2M) = parm(P_cvPind2M)
      CoeffVar(anno, ecv.cv_Pind2F) = parm(P_cvPind2F)
      CoeffVar(anno, ecv.cv_Pind20M) = parm(P_cvPind20M)
      CoeffVar(anno, ecv.cv_Pind20F) = parm(P_cvPind20F)
      CoeffVar(anno, ecv.cv_Pind30M) = parm(P_cvPind30M)
      CoeffVar(anno, ecv.cv_Pind30F) = parm(P_cvPind30F)
      '--- 4.7 Restituzione dei contributi ----
      CoeffVar(anno, ecv.cv_RcHminM) = parm(P_cvRcHminM)
      CoeffVar(anno, ecv.cv_RcHminF) = parm(P_cvRcHminF)
      CoeffVar(anno, ecv.cv_RcXminM) = parm(P_cvRcXminM)
      CoeffVar(anno, ecv.cv_RcXminF) = parm(P_cvRcXminF)
      CoeffVar(anno, ecv.cv_RcPrcCon) = parm(P_cvRcPrcCon)
      'CoeffVar(anno, ecv.cv_RcPrcRiv) = parm(P_cvRcPrcRiv)  '20150512LC (commentato)
      '--- 4.8 Totalizzazione ----
      'y = y * (1 + parm(P_cvTev))
      y = y * (1 + CoeffVar(anno, ecv.cv_Tev))
    Next anno
  End If
  '***********************
  'Frame CONTRIBUZIONE (5)
  '***********************
  If (VarType(parm(P_cv5)) <> 0 And parm(P_cv5) = False) Then
    y = 1
    For anno = anno1 To OPZ_INT_CV_AMAX
      '--- 5.1 Contribuzione ridotta ----
      '--- 5.2 Soggettivo ----
      CoeffVar(anno, ecv.cv_Sogg0Min) = ARRV(parm(P_cvSogg0Min) * y)
      CoeffVar(anno, ecv.cv_Sogg0MinR) = ARRV(parm(P_cvSogg0MinR) * y)
      CoeffVar(anno, ecv.cv_Sogg1Al) = parm(P_cvSogg1Al)
      CoeffVar(anno, ecv.cv_Sogg1AlR) = parm(P_cvSogg1AlR)
      CoeffVar(anno, ecv.cv_Sogg1Max) = ARRV(parm(P_cvSogg1Max) * y)
      CoeffVar(anno, ecv.cv_Sogg1MaxR) = ARRV(parm(P_cvSogg1MaxR) * y)
      '--- 5.3 Integrativo / FIRR ----
      CoeffVar(anno, ecv.cv_Int0Min) = ARRV(parm(P_cvInt0Min) * y)
      CoeffVar(anno, ecv.cv_Int0MinR) = ARRV(parm(P_cvInt0MinR) * y)
      CoeffVar(anno, ecv.cv_Int1Al) = parm(P_cvInt1Al)
      CoeffVar(anno, ecv.cv_Int1AlR) = parm(P_cvInt1AlR)
      CoeffVar(anno, ecv.cv_Int1Max) = ARRV(parm(P_cvInt1Max) * y)
      CoeffVar(anno, ecv.cv_Int1MaxR) = ARRV(parm(P_cvInt1MaxR) * y)
      CoeffVar(anno, ecv.cv_Int2Al) = parm(P_cvInt2Al)
      CoeffVar(anno, ecv.cv_Int2AlR) = parm(P_cvInt2AlR)
      CoeffVar(anno, ecv.cv_Int2Max) = ARRV(parm(P_cvInt2Max) * y)
      CoeffVar(anno, ecv.cv_Int2MaxR) = ARRV(parm(P_cvInt2MaxR) * y)
      CoeffVar(anno, ecv.cv_Int3Al) = parm(P_cvInt3Al)
      CoeffVar(anno, ecv.cv_Int3AlR) = parm(P_cvInt3AlR)
      '--- 5.4 Assistenziale ----
      CoeffVar(anno, ecv.cv_Ass0Min) = ARRV(parm(P_cvAss0Min) * y)
      CoeffVar(anno, ecv.cv_Ass0MinR) = ARRV(parm(P_cvAss0MinR) * y)
      CoeffVar(anno, ecv.cv_Ass1Al) = parm(P_cvAss1Al)
      CoeffVar(anno, ecv.cv_Ass1AlR) = parm(P_cvAss1Al)
      '--- 5.5 Altro ----
      CoeffVar(anno, ecv.cv_Mat0Min) = ARRV(parm(P_cvMat0Min) * y)
      CoeffVar(anno, ecv.cv_Mat0MinR) = ARRV(parm(P_cvMat0MinR) * y)
      '---
      'y = y * (1 + parm(P_cvTev))
      y = y * (1 + CoeffVar(anno, ecv.cv_Tev))
    Next anno
  End If
  '****************************
  'Frame MISURA PRESTAZIONI (6)
  '****************************
  If (VarType(parm(P_cv6)) <> 0 And parm(P_cv6) = False) Then
    y = 1
    For anno = anno1 To OPZ_INT_CV_AMAX
      '--- 6.1 Scaglioni IRPEF ----
      '--- 6.2 Pensione ----
      '--- 6.3 Supplementi di pensione ----
      CoeffVar(anno, ecv.cv_SpAnni) = parm(P_cvSpAnni)
      CoeffVar(anno, ecv.cv_SpEtaMax) = parm(P_cvSpEtaMax)
      CoeffVar(anno, ecv.cv_SpPrcCon) = parm(P_cvSpPrcCon)
      'CoeffVar(anno, ecv.cv_SpPrcRiv) = parm(P_cvSpPrcRiv)
      '--- 6.4 Reversibilit� superstiti ----
      CoeffVar(anno, ecv.cv_Av) = parm(P_cvAv)
      CoeffVar(anno, ecv.cv_Avo) = parm(P_cvAvo)
      CoeffVar(anno, ecv.cv_Avoo) = parm(P_cvAvoo)
      CoeffVar(anno, ecv.cv_Ao) = parm(P_cvAo)
      CoeffVar(anno, ecv.cv_Aoo) = parm(P_cvAoo)
      CoeffVar(anno, ecv.cv_Aooo) = parm(P_cvAooo)
      '---
      'y = y * (1 + parm(P_cvTev))
      y = y * (1 + CoeffVar(anno, ecv.cv_Tev))
    Next anno
  End If
  '***********************
  '4) COMPLETAMENTI FINALI
  '***********************
  '******************************
  'Frame TASSI ED ALTRE STIME (2)
  '******************************
  '--- 20161111LC (inizio)
  If 1 = 1 Then
    Call LeggiCoeffVar3(CoeffVar(), anno1)
  Else
  '--- 20161111LC (fine)
    For j = ecv.cv_Tv To ecv.cv_Ts2n
      j1 = ecv.cv_Prd_Ts1a + j - ecv.cv_Ts1a
      CoeffVar(anno1 - 1, j1) = 1#
      For anno = anno1 To OPZ_INT_CV_AMAX
        CoeffVar(anno, j1) = CoeffVar(anno - 1, j1) * (1 + CoeffVar(anno, j))
      Next anno
      For anno = anno1 - 2 To OPZ_INT_CV_AMIN Step -1
        CoeffVar(anno, j1) = CoeffVar(anno + 1, j1) / (1 + CoeffVar(anno + 1, j))
      Next anno
    Next j
  End If
  '******************************
  'Frame REQUISITI DI ACCESSO (4)
  '******************************
  '--- 20120210LC (inizio)
  For anno = anno1 To anno1 + P_DUECENTO
    Call TAppl_CV4_Get(CoeffVar(), cv4, anno)
  Next anno
  '--- 20120210LC (fine)
  '--- 20121102LC (inizio)
  '*********************
  'Tentativo di usare CV
  '*********************
  For anno = OPZ_INT_CV_AMIN To OPZ_INT_CV_AMAX
    With cv(anno)
      .Tp1Max = CoeffVar(anno, ecv.cv_Tp1Max)
      .Tp1Al = CoeffVar(anno, ecv.cv_Tp1Al)
      .Tp2Max = CoeffVar(anno, ecv.cv_Tp2Max)
      .Tp2Al = CoeffVar(anno, ecv.cv_Tp2Al)
      .Tp3Max = CoeffVar(anno, ecv.cv_Tp3Max)
      .Tp3Al = CoeffVar(anno, ecv.cv_Tp3Al)
      '--- 20161112LC (inizio)
      .Tp4Max = CoeffVar(anno, ecv.cv_Tp4Max)
      .Tp4Al = CoeffVar(anno, ecv.cv_Tp4Al)
      .Tp5Max = CoeffVar(anno, ecv.cv_Tp5Max)
      .Tp5Al = CoeffVar(anno, ecv.cv_Tp5Al)
      '--- 20161112LC (fine)
    End With
  Next anno
  '--- 20121102LC (fine)
  
ExitPoint:
  On Error GoTo 0
  LeggiCoeffVar2 = szErr
  Exit Function
  
ErrorHandler:
  szErr = "Errore durante la lettura dei coefficienti variabili." & vbCrLf & "Si consiglia di non effettuare l'analisi con questo gruppo di parametri" 'Err.Description
  GoTo ExitPoint
End Function


Public Sub LeggiCoeffVar3(ByRef CoeffVar() As Double, ByRef anno1 As Integer)
'20161111LC  creazione
  Dim anno As Integer
  Dim j As Integer
  Dim j1 As Integer
  
  '******************************
  'Frame TASSI ED ALTRE STIME (2)
  '******************************
  For j = ecv.cv_Tv To ecv.cv_Ts2n
    j1 = ecv.cv_Prd_Ts1a + j - ecv.cv_Ts1a
    CoeffVar(anno1 - 1, j1) = 1#
    For anno = anno1 To OPZ_INT_CV_AMAX
      CoeffVar(anno, j1) = CoeffVar(anno - 1, j1) * (1 + CoeffVar(anno, j))
    Next anno
    For anno = anno1 - 2 To OPZ_INT_CV_AMIN Step -1
      CoeffVar(anno, j1) = CoeffVar(anno + 1, j1) / (1 + CoeffVar(anno + 1, j))
    Next anno
  Next j
End Sub


Public Function LeggiCoeffVar4(ByRef appl As TAppl, ByRef glo As TGlobale) As String
'20170307LC  nuova
  Dim ia As Integer
  Dim ib As Integer
  Dim anno As Integer
  Dim sErr As String
  Dim SQL As String
  Dim rs As ADODB.Recordset
  
  sErr = ""
  '---
  If OPZ_MOD_REG_1_CV <> "" Then
    SQL = "SELECT C_CV_R1.*"
    SQL = SQL & " FROM C_CV_R1"
    SQL = SQL & " WHERE cvR1Nome='" & OPZ_MOD_REG_1_CV & "'"
    SQL = SQL & " AND cvR1Anno>=" & appl.t0 + 1
    SQL = SQL & " AND cvR1Anno<=" & appl.t0 + P_DUECENTO
    Set rs = New ADODB.Recordset
    rs.CursorLocation = adUseClient
    Call rs.Open(SQL, glo.db, adOpenForwardOnly, adLockReadOnly)
    If rs.EOF = True Then
      sErr = "Coefficienti non trovati in C_CV_R1"
    Else
      ReDim appl.cvR1(P_DUECENTO)
      Do Until rs.EOF = True
        anno = rs.Fields("cvR1Anno").Value
        ia = anno - appl.t0
        With appl.cvR1(ia)
          .cvR1Anno = anno
          .cvR1Anni = rs.Fields("cvR1Anni").Value
          If .cvR1Anni > OPZ_MOD_REG_1_ANNI Then
            .cvR1Anni = OPZ_MOD_REG_1_ANNI
          End If
          .cvR1EtaMax = rs.Fields("cvR1EtaMax").Value
          ReDim .r1a(2 * .cvR1Anni - 1) 'OPZ_MOD_REG_1_ANNI-1
          For ib = 0 To .cvR1Anni - 1   'OPZ_MOD_REG_1_ANNI-1
            With .r1a(2 * ib)
              .cvR1Sogg0Min = rs.Fields("cvR1Sogg0Min" & ib).Value
              .cvR1Sogg1Al = rs.Fields("cvR1Sogg1Al" & ib).Value
              .cvR1Sogg1Max = rs.Fields("cvR1Sogg1Max" & ib).Value
            End With
            With .r1a(2 * ib + 1)
              .cvR1Sogg0Min = rs.Fields("cvR1Sogg0MinR" & ib).Value
              .cvR1Sogg1Al = rs.Fields("cvR1Sogg1AlR" & ib).Value
              .cvR1Sogg1Max = rs.Fields("cvR1Sogg1MaxR" & ib).Value
            End With
          Next ib
        End With
        Call rs.MoveNext
      Loop
    End If
    Call rs.Close
  ElseIf OPZ_MOD_REG_1A <> 0 Then
    sErr = "Nome dei coefficienti in C_CV_R1 non specificato"
  End If
  If sErr <> "" Then GoTo ExitPoint
  '---
  If OPZ_MOD_REG_2_CV <> "" Then
    Call MsgBox("TODO")
    Stop
  End If
  '---
  If OPZ_MOD_REG_3_CV <> "" Then
    Call MsgBox("TODO")
    Stop
  End If
  '---
  
ExitPoint:
  LeggiCoeffVar4 = sErr
End Function


Public Sub LeggiLS(db As ADODB.Connection, btNome$, iLS&, iTC&, LL#())
  Dim h%, h0%, h1%, x%, x0%, x1%
  Dim Sam#, Saf#, Sim#, Sif#
  Dim SQL$
  Dim rs As New ADODB.Recordset
  
  '************************
  'Legge le linee salariali
  '************************
  '20080305LC (inizio)
  'SQL = "SELECT lsH0,lsH1,lsX0,lsX1,lsSam,lsSaf,lsSim,lsSif"
  SQL = "SELECT lsH0,lsH1,lsX0,lsX1,lsS1m,lsS1f,lsS2m,lsS2f"  '20150528LC
  SQL = SQL & " FROM C_LS"  '20160419LC
  SQL = SQL & " WHERE lsNome='" & UCase(btNome) & "'" '20121024LC
  SQL = SQL & " AND lsTC=" & iTC
  '20080305LC (fine)
  Call rs.Open(SQL, db, adOpenForwardOnly, adLockReadOnly)
  Do Until rs.EOF
    h0 = rs(0).Value
    h1 = rs(1).Value
    If h0 > h1 Then
      h = h0
      h0 = h1
      h1 = h
    End If
    If h0 < K_HMIN Then
      h0 = K_HMIN
    End If
    If h1 > K_HMAX Then
      h1 = K_HMAX
    End If
    x0 = rs(2).Value
    x1 = rs(3).Value
    If x0 > x1 Then
      x = x0
      x0 = x1
      x1 = x
    End If
    If x0 < K_XMIN Then
      x0 = K_XMIN
    End If
    If x1 > K_XMAX Then
      x1 = K_XMAX
    End If
    Sam = rs(4).Value
    Saf = rs(5).Value
    Sim = rs(6).Value
    Sif = rs(7).Value
    For h = h0 To h1
      For x = x0 To x1
        '20080617LC (inizio)
        LL(h, x, iLS, iTC, 0) = Sam
        LL(h, x, iLS, iTC, 1) = Saf
        LL(h, x, iLS, iTC, 2) = Sim
        LL(h, x, iLS, iTC, 3) = Sif
        '20080617LC (fine)
      Next x
    Next h
    rs.MoveNext
  Loop
  rs.Close
End Sub


Public Function LeggiNE(ByRef db As ADODB.Connection, ByRef niNomeA As String, ByRef Lne() As Double) As String
'20150527LC
'20160323LC  tutta
'20170312LC  nuovo campo niT
  Dim bNiVer As Byte  '20170312LC
  Dim iTC As Byte  '20170312LC
  Dim j As Byte
  Dim k As Byte
  Dim tipo As Byte  '1:X, 2:H
  Dim t As Byte
  Dim xh As Byte
  Dim niNome As String
  Dim sErr As String
  Dim SQL As String
  Dim sz As String
  Dim v As Variant
  Dim rs As New ADODB.Recordset
  
  '****************
  'Inizializzazione
  '****************
  sErr = ""
  For t = 0 To 30
    For xh = 0 To 60  '20160323LC
      For j = 0 To 3
        For k = 0 To 3
          '--- 20170312LC (inizio)
          For iTC = 0 To MAX_TC
            Lne(t, xh, j, k, iTC) = 0#
          Next iTC
          '--- 20170312LC (fine)
        Next k
      Next j
    Next xh
  Next t
  niNome = UCase(niNomeA)
  If niNome = "" Then GoTo ExitPoint
  Select Case Left(niNome, 2)
  Case "SX", "VX"
    tipo = 1
  Case "SH", "VH"
    tipo = 2
  Case Else
    sErr = "Il nome delle probabilit� di riattivazione dei volontari deve cominciare per VX o VH"
    GoTo ExitPoint
  End Select
  '************
  'Lettura dati
  '************
  'v = Array("AM", "AF", "IM", "IF")
  v = Array("1M", "1F", "2M", "2F")  '20150528LC
  '--- 20170312LC (inizio)
  bNiVer = 0
  SQL = "SELECT TOP 1 *"
  SQL = SQL & " FROM C_NI"  '20160419LC
  Set rs = New ADODB.Recordset
  Call rs.Open(SQL, db, adOpenForwardOnly, adLockReadOnly)
  For j = 0 To rs.Fields.Count - 1
    If LCase(rs.Fields(j + 0).Name) = "nit" Then
      bNiVer = 1
      Exit For
    End If
  Next j
  Call rs.Close
  '--- 20170312LC (fine)
  SQL = "SELECT *"
  SQL = SQL & " FROM C_NI"  '20160419LC
  SQL = SQL & " WHERE niNome='" & niNome & "'" '20121024LC
  '--- 20170312LC (inizio)
  If bNiVer > 0 Then
    SQL = SQL & " AND niT>=" & 0 & " AND niT<=" & 30
    SQL = SQL & " AND niTC<=" & MAX_TC '20080307LC
  '--- 20170312LC (fine)
  Else
    SQL = SQL & " AND niTC>=" & 0 & " AND niTC<=" & 30
  End If
  '-- 20160323LC (inizio)
  If tipo = 1 Then
    SQL = SQL & " AND niX>=" & 15 & " AND niX<=" & 75
  Else
    SQL = SQL & " AND niH>=" & 0 & " AND niH<=" & 60
  End If
  '-- 20160323LC (fine)
  '--- 20170312LC (inizio)
  If bNiVer > 0 Then
    If tipo = 1 Then
      SQL = SQL & " ORDER BY niT ASC, niX ASC, niTC ASC"
    Else
      SQL = SQL & " ORDER BY niT ASC, niH ASC, niTC ASC"
    End If
  Else
    If tipo = 1 Then
      SQL = SQL & " ORDER BY niTC ASC, niX ASC"
    Else
      SQL = SQL & " ORDER BY niTC ASC, niH ASC"
    End If
  End If
  '--- 20170312LC (fine)
  Set rs = New ADODB.Recordset
  Call rs.Open(SQL, db, adOpenForwardOnly, adLockReadOnly)
  Do Until rs.EOF = True
    '-- 20160323LC (inizio)
    '--- 20170312LC (inizio)
    If bNiVer > 0 Then
      iTC = rs.Fields("niTC").Value
      t = rs.Fields("niT").Value
    Else
      iTC = 0
      t = rs.Fields("niTC").Value
    End If
    '--- 20170312LC (fine)
    If tipo = 1 Then
      xh = rs.Fields("niX").Value - 15
    Else
      xh = rs.Fields("niH").Value
    End If
    For j = 0 To 3
      '--- 20170312LC (inizio)
      Lne(t, xh, j, 0, iTC) = 0
      Lne(t, xh, j, 1, iTC) = rs.Fields("niProb" & v(j)).Value
      Lne(t, xh, j, 2, iTC) = rs.Fields("niProvv" & v(j)).Value  '20160420LC
      Lne(t, xh, j, 3, iTC) = 0  'rs.Fields("niIva" & v(j)).Value
      '--- 20170312LC (fine)
    Next j
    '-- 20160323LC (fine)
    Call rs.MoveNext
  Loop
  Call rs.Close
  
ExitPoint:
  On Error GoTo 0
  LeggiNE = sErr
End Function


Public Function LeggiNI(db As ADODB.Connection, niNome$, Lni#()) As String
'20080305LC
'20160523LC  sErr
  Dim iTC As Byte, j As Byte, k As Byte, x As Byte, XMIN As Byte, XMAX As Byte
  Dim h#
  Dim SQL$, sz$
  Dim sErr As String
  Dim v As Variant
  Dim rs As New ADODB.Recordset
  
  '****************
  'Inizializzazione
  '****************
  sErr = ""  '20160523LC
  XMIN = LBound(Lni, 1)
  XMAX = UBound(Lni, 1)
  For x = XMIN To XMAX
    For iTC = 0 To MAX_TC '20080307LC
      For j = 0 To 3
        For k = 0 To 3
          Lni(x, iTC, j, k) = 0
        Next k
      Next j
    Next iTC
  Next x
  '************
  'Lettura dati
  '************
  'v = Array("AM", "AF", "IM", "IF")
  v = Array("1M", "1F", "2M", "2F")
  SQL = "SELECT *"
  SQL = SQL & " FROM C_NI"  '20160419LC
  SQL = SQL & " WHERE niNome='" & UCase(niNome) & "'" '20121024LC
  SQL = SQL & " AND niX>=" & XMIN & " AND niX<=" & XMAX
  SQL = SQL & " AND niTC<=" & MAX_TC '20080307LC
  SQL = SQL & " ORDER BY niTC ASC, niX ASC"
  Call rs.Open(SQL, db, adOpenForwardOnly, adLockReadOnly)
  Do Until rs.EOF
    x = rs.Fields("niX").Value
    '20080305LC
    h = 0 'rs("niH").Value / 1000
    h = Int(h) + (h - Int(h)) * 1000 / 360
    iTC = rs.Fields("niTC").Value
    For j = 0 To 3
      Lni(x, iTC, j, 0) = h
      Lni(x, iTC, j, 1) = rs.Fields("niProb" & v(j)).Value
      Lni(x, iTC, j, 2) = rs.Fields("niProvv" & v(j)).Value  '20160420LC
      Lni(x, iTC, j, 3) = 0  'rs.Fields("NiIva" & v(j)).Value
      '--- 20160523LC (inizio)
      'Call MsgBox("TODO-TIscr2_MovPop_4b2", , dip.mat)
      If (Lni(x, iTC, j, 1) < 0) Or (Lni(x, iTC, j, 2) < 0) Or _
         (Lni(x, iTC, j, 1) > 0 And Lni(x, iTC, j, 2) = 0) Then
        sErr = "Errore di lettura della tabella C_NI" & vbCrLf & _
               "niTC=" & iTC & _
               "   niX=" & x & _
               "   niProb" & v(j) & "=" & Lni(x, iTC, j, 1) & _
               "   niProvv" & v(j) & "=" & Lni(x, iTC, j, 2) & vbCrLf & _
               "Valori negativi oppure probabilit� positiva e provvigioni nulle"
        GoTo ExitPoint
      End If
      '--- 20160523LC (fine)
    Next j
    Call rs.MoveNext
  Loop
  
ExitPoint:
  Call rs.Close
  LeggiNI = sErr '20160523LC
End Function


'20111208LC
Public Function LexPensAbbattimento(CoeffVar#(), z#, anno0%, anno%) As Double
  Dim i%
  Dim y#
  ReDim a#(6), l#(6) '20120220LC
  
  OPZ_INT_CV_AMAX = OPZ_INT_CV_AMAX
  For i = 0 To 6 '20120220LC
    If anno > OPZ_INT_CV_AMAX Then
      l(i) = CoeffVar(OPZ_INT_CV_AMAX, ecv.cv_ScA + 2 * i) * 1.02 ^ (anno - OPZ_INT_CV_AMAX)
    Else
      l(i) = CoeffVar(anno, ecv.cv_ScA + 2 * i)
    End If
    '20111208LC (inizio)
    'a(i) = CoeffVar(anno, ecv.cv_ScA + 2 * i + 1)
    If anno0 > OPZ_INT_CV_AMAX Then
      a(i) = CoeffVar(OPZ_INT_CV_AMAX, ecv.cv_ScA + 2 * i + 1)
    Else
      a(i) = CoeffVar(anno0, ecv.cv_ScA + 2 * i + 1)
    End If
    '20111208LC (fine)
  Next i
  Select Case z
  Case Is <= l(0)
    y = z * a(0) '0.02
  Case Is <= l(1)
    y = l(0) * a(0) + (z - l(0)) * a(1) '0.0171
  Case Is <= l(2)
    y = l(0) * a(0) + (l(1) - l(0)) * a(1) + (z - l(1)) * a(2) '0.0143
  Case Is <= l(3)
    y = l(0) * a(0) + (l(1) - l(0)) * a(1) + (l(2) - l(1)) * a(2) + (z - l(2)) * a(3) '0.0114
  Case Is <= l(4)
    y = l(0) * a(0) + (l(1) - l(0)) * a(1) + (l(2) - l(1)) * a(2) + (l(3) - l(2)) * a(3) + (z - l(3)) * a(4)
  '20120220LC (inizio)
  Case Is <= l(5)
    y = l(0) * a(0) + (l(1) - l(0)) * a(1) + (l(2) - l(1)) * a(2) + (l(3) - l(2)) * a(3) + (l(4) - l(3)) * a(4) + (z - l(4)) * a(5)
  Case Is <= l(5)
    y = l(0) * a(0) + (l(1) - l(0)) * a(1) + (l(2) - l(1)) * a(2) + (l(3) - l(2)) * a(3) + (l(4) - l(3)) * a(4) + (l(5) - l(4)) * a(5) + (z - l(5)) * a(6)
  Case Else
    y = l(0) * a(0) + (l(1) - l(0)) * a(1) + (l(2) - l(1)) * a(2) + (l(3) - l(2)) * a(3) + (l(4) - l(3)) * a(4) + (l(5) - l(4)) * a(5) + (l(6) - l(5)) * a(6)
  End Select
  '20120220LC (fine)
  'y = ARRV(y)
  LexPensAbbattimento# = y
End Function


'20111217LC (tutta)
Public Function mi&(DAss As Date, ByVal DCan As Date, anno%)
  Dim aAss%, aCan%, y%
  
  If DCan = 0 Then
    DCan = #12/31/1999#
  End If
  aAss = Year(DAss)
  aCan = Year(DCan)
  If anno > aAss Or aAss < 1990 Then
    If DCan > 0 And aCan = anno Then
      y = Month(DCan)
    Else
      y = 12
    End If
  Else
    If DCan > 0 And aCan = aAss Then
      y = Month(DCan) - Month(DAss) + 1
    Else
      y = 13 - Month(DAss)
    End If
  End If
  mi = y
End Function


Public Sub OpzioniReport(glo As TGlobale, req As Object, iop&)  '20130307LC
  Dim i%
  
  If iop = 0 Then
    Set req = New Collection
    For i = 0 To 12
      req.Add CStr(glo.rep_chk(i)), "k2" & i
    Next i
    If glo.rep_opFile(2) = True Then
      req.Add CStr(2), "r2"
    ElseIf glo.rep_opFile(1) Then
      req.Add CStr(1), "r2"
    Else
      req.Add CStr(0), "r2"
    End If
    '---
    'For i = 0 To 12
    '  frmRep.Check1(i).Value = glo.rep_chk(i)
    'Next i
    'For i = 0 To 2
    '  frmRep.Option1(i).Value = glo.rep_opFile(i)
    'Next i
  Else
    If iop <> 1 Then
      Set req = New Collection
      For i = 0 To 12
        req.Add CStr(1), "k2" & i
      Next i
      req.Add CStr(0), "r2"
    End If
    For i = 0 To 12
      glo.rep_chk(i) = req("k2" & i) 'frmRep.Check1(i).Value
    Next i
    For i = 0 To 2
      'glo.rep_opFile(i) = frmRep.Option1(i).Value
      glo.rep_opFile(i) = req("r2") = i
    Next i
  End If
End Sub


Public Function PensNew(ByRef mat As Long, ByRef pens1a1 As Double, ByRef pens1a2 As Double, _
                        ByRef anz As TIscr2_Anz, ByRef h1 As Double, ByRef h2 As Double, ByRef bPensA As Boolean, _
                        ByRef xr As Double, ByRef ax0 As Double, ByRef ax As Double, ByRef abbPens As Double) As Double
'*********************************************************************************************************************
'  20120302LC  tutta
'  20160308LC  x0 (ENAS)
'*********************************************************************************************************************
  Dim h#, pensProRata#
  Dim x#, y0#, y1#
  
  pensProRata = pens1a1 * h1 + pens1a2 * h2
  h = h1 + h2 + AA0 '20111209LC - Attenzione; .mat=704032 .t=2045 h=34.9999999999999
  x = xr + AA0 '307000
  If bPensA = True And h < OPZ_PENS_ANZ_HABB And OPZ_PENS_ANZ_HABB > 0 Then
If Int(x) <> Int(x + AA0) Then 'vedi 307000 senza AA0
Call MsgBox("TODO-PensNew-1", , mat)
Stop
End If
    '--- 20160308LC (inizio)
    If abbPens > 0 And abbPens < 1 Then
      pensProRata = pensProRata * abbPens
    End If
    '--- 20160308LC (fine)
  End If
  PensNew = pensProRata
End Function


Public Function PensNew_aux#(x%)
  Dim y#
  
  '20080502LC (inizio)
  If 1 = 2 Then '20080502LC (TOMASI)
    Select Case x
    Case Is <= 58
      y = 0.24
    Case 59
      y = 0.21
    Case 60
      y = 0.18
    Case 61
      y = 0.145
    Case 62
      y = 0.11
    Case 63
      y = 0.075
    Case 64
      y = 0.04
    Case Is >= 65
      y = 0
    End Select
    '20080502LC (fine)
  Else
    Select Case x
    Case Is <= 58
      y = 0.173
    Case 59
      y = 0.153
    Case 60
      y = 0.131
    Case 61
      y = 0.108
    Case 62
      y = 0.084
    Case 63
      y = 0.058
    Case 64
      y = 0.03
    Case Is >= 65
      y = 0
    End Select
  End If
  PensNew_aux = 1 - y
End Function


Public Function ValFmt$(x#, n%)
  Dim sz$
  
  sz = Format(x, "#,##0.00")
  If Len(sz) < n Then sz = Space(n - Len(sz)) & sz
  ValFmt = sz
End Function


Public Sub SetComboNI(v As Variant, cb As ComboBox, b As Boolean)
'20120203LC
  Call ComboBox_Set(cb, b)
  If b = True Then
    ComboSelect cb, v, 1
  Else
    cb.ListIndex = -1
  End If
End Sub


Public Sub SetTextNI(v As Variant, tb As TextBox, b As Boolean)
'20120203LC
  Call TextBox_Set(tb, b)
  tb.Text = IIf(b = True, v, "")
End Sub


Public Function SQL_Del$(DB_TYPE%)
  If DB_TYPE <= DB_ACCESS Then
    SQL_Del = "DELETE *"
  Else
    SQL_Del = "DELETE"
  End If
End Function


'20120213LC
Public Function Sql_Normalize(SQL$) As String
  Dim s1$, s2$
  
  s1 = LCase(Trim(SQL))
  's2 = Replace(s1, vbCrLf, " ")
  s2 = Replace(s1, Chr(13), " ")
  s2 = Replace(s2, Chr(10), " ")
  Do
    s1 = Replace(s2, "  ", " ")
    If s1 = s2 Then
      Exit Do
    End If
    s2 = s1
  Loop
  Sql_Normalize = s2
End Function


'20121209LC (tutta)
Public Function TassoZCB(CoeffVar#(), tau&, j&) As Double
  If tau < LBound(CoeffVar, 1) Then
    TassoZCB = CoeffVar(LBound(CoeffVar, 1), j)
  ElseIf tau > UBound(CoeffVar, 1) Then
    TassoZCB = CoeffVar(UBound(CoeffVar, 1), j)
  Else
    TassoZCB = CoeffVar(tau, j)
  End If
End Function


'20121209LC (obsoleto)
Public Function z_TassoZCB#(ZCB#(), tau%)
  Dim y#
  
  If tau - 1 < 0 Then
    y = ZCB(0, 0)
  ElseIf tau - 1 >= 50 Then
    y = ZCB(49, 49)
  Else
    y = ZCB(tau - 1, tau - 1)
  End If
  'If y > 0 Then y = (1 + y) ^ (1 / 12) - 1
  z_TassoZCB = y
End Function


Public Function VecMedia#(v#(), n%)
  Dim i%
  Dim y#
  
  For i = 0 To n - 1
    y = y + v(i)
  Next i
  If n > 0 Then
    VecMedia = y / n
  Else
    VecMedia = 0
  End If
End Function


Public Sub VecSort(v#(), n%)
  Dim i%, ii%, j%
  Dim y#
  
  For i = 0 To n - 1
    ii = i
    For j = i + 1 To n - 1
      If v(ii) < v(j) Then ii = j
    Next j
    y = v(i)
    v(i) = v(ii)
    v(ii) = y
  Next i
End Sub


Public Function verNum$(iop As Byte, ByVal sz$, XMIN#, XMAX#, a$)
  '************************************************************
  'Verifica la correttezza di un numero in una casella di testo
  '************************************************************
  'iop = 0    numero intero
  'iop <> 0   numero reale
  Dim bPerc As Boolean
  Dim x#
  
  If Right(sz, 1) = "%" Then
    sz = Left(sz, Len(sz) - 1)
    bPerc = True
  End If
  If Not IsNumeric(sz) Then
    verNum = a$ & " non � un numero"
  Else
    x = CDbl(sz)
    If iop = 0 And x <> Int(x) Then
      verNum = a$ & " non � un numero intero"
    ElseIf x < XMIN Then
      '20111217LC (inizio)
      If XMIN = XMAX Then
        verNum = a$ & " � diverso da valore prefissato (" & XMIN & ")"
      Else
      '20111217LC (fine)
        verNum = a$ & " � inferiore al minimo (" & XMIN & ")"
      End If
    ElseIf x > XMAX Then
      verNum = a$ & " � superiore al massimo (" & XMAX & ")"
    End If
  End If
End Function


Public Function xlsApri(ByRef xlsApp As Object, ByRef szXlsTemplate As String, ByRef szXlsOutput As String) As String
  Dim szErr$
  If ON_ERR_RN3 Then On Error Resume Next
  
  szErr = ""
  If xlsApp Is Nothing Then '20080614LC
    Set xlsApp = GetObject("Excel.Application")
    If Err.Number <> 0 Then
      Err.Clear
      Set xlsApp = CreateObject("Excel.Application")
      If Err.Number <> 0 Then
        szErr = "Errore di automazione OLE" & vbCrLf & Err.Description
        GoTo ExitPoint
      End If
    End If
  End If
  xlsApp.Workbooks.Open App.Path & "\" & szXlsTemplate
  If Err.Number <> 0 Then
    szErr = "Impossibile trovare o aprire il modello " & App.Path & "\" & szXlsTemplate
    GoTo ExitPoint
  End If
  xlsApp.DisplayAlerts = False
  xlsApp.ActiveWorkbook.SaveAs App.Path & "\" & szXlsOutput
  If Err.Number <> 0 Then
    szErr = "Impossibile salvare il file " & App.Path & "\" & szXlsOutput
    GoTo ExitPoint
  End If

ExitPoint:
  If szErr <> "" Then
    xlsApp.ActiveWorkbook.Close
    xlsApp.Quit
    Set xlsApp = Nothing
  End If
  On Error GoTo 0
  xlsApri = szErr
End Function


Public Function xlsChiudi$(xlsApp As Object, szNomeFile$)
  Dim szErr$
  If ON_ERR_RN Then On Error Resume Next
  
  szErr = ""
  xlsApp.ActiveWorkbook.SaveAs App.Path & "\" & szNomeFile
  xlsApp.ActiveWorkbook.Close
  xlsApp.Quit
  'Set xlsApp = Nothing '20080614LC
  szErr = Err.Description
  On Error GoTo 0
  xlsChiudi = szErr
End Function


Public Function xlsPaste(xlsApp As Object, ByVal sz$, ByRef sRif$) As String
'20161112LC  gestione errori
'20161113LC  torniamo all'uso di Cells
  Dim iop As Integer  '20160113LC (0:usa Cells; 1:usa PasteSpecial
  Dim i As Long
  Dim iRiga As Long
  Dim iRigaMin As Long
  Dim j As Long
  Dim jCol As Long
  Dim jColMin As Long
  Dim tim1 As Double
  Dim sErr As String  '20161112LC
  Dim v As Variant
  Dim v1 As Variant
  If ON_ERR_EH = True Then On Error GoTo ErrorHandler
  
  iop = OPZ_OUTPUT_XLS  '20170125LC (0:vecchio metodo; 1:veloce)
  sErr = ""
  '--- Modofica del testo
  If 1 = 1 Then
    sz = Replace(sz, ",", ".")
  Else
    i = 0
    Do
      i = InStr(i + 1, sz, ",")
      If i = 0 Then Exit Do
      Mid(sz, i, 1) = "."
    Loop
  End If
  '---
  If iop = 0 Then
    tim1 = Timer
    jColMin = Asc(UCase(Left(sRif, 1))) - 64
    Select Case UCase(Mid(sRif, 2, 1))
    Case "A" To "Z"
      jColMin = jCol * 26 + Asc(UCase(Mid(sRif, 2, 1))) - 64
      iRigaMin = Mid(sRif, 3)
    Case Else
      iRigaMin = Mid(sRif, 2)
    End Select
    v = Split(sz, vbCrLf)
    For i = 0 To UBound(v)
      If Trim(v(i)) <> "" Then
        v1 = Split(v(i), vbTab)
        iRiga = iRigaMin + i
        For j = 0 To UBound(v1)
          jCol = jColMin + j
          xlsApp.ActiveSheet.cells(iRiga, jCol) = v1(j)
        Next j
      End If
    Next i
    tim1 = Timer - tim1
tim1 = tim1
  Else
    Call Clipboard.Clear
    Call Clipboard.SetText(sz)
    'xlsApp.Range(sRif").Select  '20161112LC (era nelle routine chiamanti, commentato)
    'xlsApp.ActiveSheet.PasteSpecial  '20131020LC (ex .Paste)
    If 1 = 1 Then
      On Error Resume Next
      Call xlsApp.ActiveSheet.Range(sRif).PasteSpecial
      If Err.Number <> 0 Then
        Err.Clear
        On Error GoTo 0
        If ON_ERR_EH = True Then On Error GoTo ErrorHandler
        Call Clipboard.Clear
        Call Clipboard.SetText(sz)
        Call xlsApp.ActiveSheet.Range(sRif).PasteSpecial
        'Call MsgBox("TODO")
        'Stop
      End If
    Else
      Call xlsApp.ActiveSheet.Range(sRif).PasteSpecial( _
          Paste:=xlPasteValues, _
          Operation:=xlNone, _
          SkipBlanks:=False, _
          Transpose:=False)
      End If
    Call Clipboard.Clear
  End If
  
ExitPoint:
  On Error GoTo 0
  xlsPaste = sErr
  Exit Function
  
ErrorHandler:
  sErr = "Errore " & Err.Number & " (" & Hex(Err.Number) & ") nel modulo " & Err.Source & vbCrLf & Err.Description  '20161112LC
  GoTo ExitPoint
End Function


Public Function ymed#(y#, z#)
  If z > AA0 Then
    ymed = y / z
  Else
    ymed = 0
  End If
End Function
